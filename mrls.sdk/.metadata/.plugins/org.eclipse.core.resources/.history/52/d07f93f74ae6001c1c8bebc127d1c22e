#include "MRLS2D.h"

	XUartPs UartPs;
	XScuGic INTCInst;
	XAxiDma AxiDma;
	uint8_t part_mem = 0;
	volatile int Error;
	volatile int RxDone;
	uint32_t g_dnaPart1;
	uint32_t g_dnaPart2;
	bool neededStopDmaAndTransmitUbloxData = false;
	static tHMC769_Reg psCurrentHMC769_Reg;
	const uint32_t sizeOfWordTransmitDataDmaFromMemToPC = 2048;
	extern bool isReverseRawData ;
	extern int32_t reverseRaw[8192];
	extern int32_t* pReverseRaw;

	static void MRLS2D_ReadDNA();
	static void MRLS2D_waitSetTrigger();
	static void MRLS2D_waitResetTrigger();
	static void MRLS2D_Process_StopDmaAndTransmitUbloxData();
	static tHMC769_Reg readConfigHMC769();

	static struct sHMC769_Reg listHMC769_Reg[] =
		{{ "REG0-ID  Register   ",		0,		12,			0x23		},
		 { "REG1-RST Register   ",		0x01,	0x000002,	0x000002	},
		 { "REG2-REF Register ",		0x02,	0x000001,	0x000001	},
		 { "REG3-FreqIntegPart",		0x03,	0x00001D,	0x00001D	},
		 { "REG4-FreqFractPart",		0x04,	1048576,	1048576		},
		 { "REG5-SeedRegister",			0x05,	0x000000,	0x000000	},
		 { "REG7-LockDetReg",			0x07,	0x204865,	0x204865	},
		 { "REG8-AnalogEnReg",			0x08,	0x036FFF,	0x036FFF   	},
		 { "REG9-ChargePumpReg",		0x09,	0x003264,	0x003264   	},
		 { "REGA-ModulatStepReg",		0x0A,	83,		    0           },
		 { "REGB-PD Register",			0x0B,	0x01E071,	0x01E071   	},
		 { "REGC-ALTINT Register",		0x0C,	0x00001D,	0x00001D   	},
		 { "REGD-ALTFRACRegister",		0x0D,	7356576,	0   	    },
		 { "REGE-SPI TRIG Register",	0x0E,	0x000000,	0x000000 	},
		 { "REGF-GPO Register",			0x0F,	0x000001,	0x000001 	},
		 { "REG6-SD CFG Reg",			0x06,	0x001FBF,	0x001FBF 	}	};

struct sHMC769 HMC769 = { config_HMC_MAIN120, listHMC769_Reg, setRegHMC769};

void MRLS2D_Init(){

	UartPsIntrInit( &INTCInst, &UartPs, UART_DEVICE_ID, UART_INT_IRQ_ID);
	uartLiteInit();
	i2c_init();
	i2c_init_TEMP_PAMP();
	init_platform();
	MRLS2D_app();
}
int checkConfigHMC769(tFrequencyBandsHMC frequencyBands, tHMC769_Reg writeListHMC769_Reg,
														  tHMC769_Reg readListHMC769_Reg){

	for (size_t  numReg = HMC769_RST_Register_REG; numReg < NUM_REGISTERS_HMC769; numReg++){
		if ( frequencyBands == config_HMC_MAIN120){
			if( (writeListHMC769_Reg + numReg )->valueFor120MHz != ((readListHMC769_Reg + numReg) )->valueFor120MHz )
				return XST_FAILURE;
		}
		else{
			if( (writeListHMC769_Reg + numReg )->valueCalculated != ((readListHMC769_Reg + numReg) )->valueCalculated )
				return XST_FAILURE;
		}
	}
	return XST_SUCCESS;
}
void setRegHMC769( tFrequencyBandsHMC frequencyBands, tHMC769_Reg reg ){

	if( frequencyBands == config_HMC_MAIN120){
		HMC769_write(psCurrentHMC769_Reg->num,
				     psCurrentHMC769_Reg->valueFor120MHz);
	}
	else{
		HMC769_write(psCurrentHMC769_Reg->num,
				     psCurrentHMC769_Reg->valueCalculated);
	}
}
void HMC769_CalculateParam(uint16_t Fstart, uint16_t Fstop, uint16_t Tramp){

	static const uint32_t Fpd = 80;

	float Nstart = ( float )Fstart / ( float )(Fpd * 4);
	float Reg_3 = round( Nstart );
	float Reg_4 = ( Nstart - Reg_3) * 16777216;
	float Nstop = ( float )Fstop / ( float )(Fpd * 4);
	float Reg_C = trunc( Nstop );
	float Tref = 1 / ( float )Fpd;
	float Nstep =  ( float )Tramp / Tref;

	uint32_t Reg_A = round( (( Nstop - Nstart)/( float )Nstep ) * 16777216 );
	uint32_t Reg_D = Reg_4 + Reg_A * ( float )Nstep;

	listHMC769_Reg[9].valueCalculated  = Reg_A;
	listHMC769_Reg[12].valueCalculated = Reg_D;

		xil_printf("INFO: Calculated value REG A = %d, REG D = %d\r\n", Reg_A, Reg_D);
}
void HMC769_configIC2( tFrequencyBandsHMC frequencyBands ){

	tHMC769_Reg readListHMC769_Reg;


	if( frequencyBands == config_HMC_MAIN120){
		xil_printf("INFO: HMC769 will be configured is 120MHz\r\n");
	}
	if( frequencyBands == config_HMC_CUSTOM){
		xil_printf("INFO: HMC769 will be configured is custom FB\r\n");
	}

		for(uint8_t i = HMC769_RST_Register_REG; i < NUM_REGISTERS_HMC769; i++){
			psCurrentHMC769_Reg = &listHMC769_Reg[i];
			HMC769.pfConfigFunc( frequencyBands, psCurrentHMC769_Reg );
		}

		readListHMC769_Reg = readConfigHMC769();

		if ( checkConfigHMC769(frequencyBands, listHMC769_Reg, readListHMC769_Reg) != XST_SUCCESS ){
			xil_printf("ERROR: Check config err\r\n");
//			while(1);
		}
		else
			xil_printf("INFO: Check config successful comleted\r\n");
}

#define AVERAG_VAL       4

#define SHIFT_DIVIDE_VAL2 2
#define SHIFT_DIVIDE_VAL3 3
#define SHIFT_DIVIDE_VAL4 4

void AD9650_setAveragVal(uint8_t averagVal){

	Xil_Out32(XPAR_HIER_0_AVERAGEFFT_0_S00_AXI_BASEADDR,    averagVal); // Передаем в модуль усреднения значение усреднения
	Xil_Out32(XPAR_AD9650_0_S00_AXI_BASEADDR + 12,        ( averagVal << 16 | MAX_AMOUNT_AZIMUTH_FOR_SPEED_2_5_SEC ) );	// передаем в модуль

	// При усреднении 4 пакетов. Делимим сумму на 4 ( деление на 4 ~ сдвигу на 2, деление на 8 ~ сдвигу на 3 и т.д.)
	if(averagVal == 4)	Xil_Out32(XPAR_HIER_0_AVERAGEFFT_0_S00_AXI_BASEADDR + 4,  SHIFT_DIVIDE_VAL2);
	if(averagVal == 8)	Xil_Out32(XPAR_HIER_0_AVERAGEFFT_0_S00_AXI_BASEADDR + 4,  SHIFT_DIVIDE_VAL3);
	if(averagVal == 16)	Xil_Out32(XPAR_HIER_0_AVERAGEFFT_0_S00_AXI_BASEADDR + 4,  SHIFT_DIVIDE_VAL4);

	xil_printf("INFO: Set averag val = %d\r\n", averagVal);
}
void MRLS2D_app(){

	printf("<----MRLS_2D v%d.0 ------>\n\r", VERISON_MRLS2D);
	MRLS2D_ReadDNA();
	AD9650_setAveragVal( AVERAG_VAL8 );
//	SET_MAX_AMOUNT_AZIMUTH( MAX_AMOUNT_AZIMUTH_FOR_SPEED_2_5_SEC )   //Xil_Out32(XPAR_AD9650_0_S00_AXI_BASEADDR + 12,  ( 571 ));    ///283 - 2.5  571 - 5	// ������������ �������� ������������ ������������ ������ ������ �� ������������������������ �������������������� ���������������� �������������� ���������� ������������������ ���� ����
	HMC769_PWR_EN( EN_SPI_CEN |DIS_PAMP | DIS_ROW_PLL | DIS_LO_AMP );
	HMC769_PWR_EN( EN_SPI_CEN |DIS_PAMP | EN_ROW_PLL | EN_LO_AMP );
	AD9650_SetAdrData_andStartTransfer(AD9650_CHANNEL_INDEX_REG, 	0b11, 	WRITE);
	AD9650_SetAdrData_andStartTransfer(AD9650_CLOCK_REG, 			AD9650_DUTY_CYCLE_STABILIZE_BITREG, 	WRITE);
	AD9650_SetAdrData_andStartTransfer(AD9650_DEVICE_UPDATE_REG,	AD9650_SW_TRANSFER_BITREG, 	WRITE);
	AD9650_SetAdrData_andStartTransfer(AD9650_CHANNEL_INDEX_REG, 	0b11, 	WRITE);
	AD9650_SetAdrData_andStartTransfer(AD9650_SYNC_CONTROL_REG,  	0b011, 	WRITE);
	AD9650_SetAdrData_andStartTransfer(AD9650_DEVICE_UPDATE_REG, 	AD9650_SW_TRANSFER_BITREG, 	WRITE);
	AD9650_DIVIDE_CLOCK_10();
	HMC769_setAtten( ATTEN_VAL );
	AD9650_ADC_SetSwitch_AMP( NUM_AMP_RECIVER );     // 1, 2, 3, 4
	HMC769_SOFT_RESET
	HMC769_DISABLE_FROM_SPI
	HMC769_ENABLE_FROM_SPI
	HMC769_init();
	HMC769_configIC2( HMC769.currentfrequencyBands );
	HMC769_setShiftFront( g_shiftFront );
	MRLS2D_ampEn(4);   // 2, 3 , 4
	AD9650_SetAdrData_andStartTransfer(0x100, 0b000, 	WRITE);
	AD9650_SetAdrData_andStartTransfer(0xFF, 0b1, 		WRITE);
	PWDN_RESET
}
void MRLS2D_ampEn(u8 num_amp_in_receiver){

	if(num_amp_in_receiver  == 2)		Xil_Out32(XPAR_SWITCHMICROWAVEPATH_0_S00_AXI_BASEADDR + 8, 1);
	if(num_amp_in_receiver  == 3)		Xil_Out32(XPAR_SWITCHMICROWAVEPATH_0_S00_AXI_BASEADDR + 8, 3);
	if(num_amp_in_receiver  == 4)		Xil_Out32(XPAR_SWITCHMICROWAVEPATH_0_S00_AXI_BASEADDR + 8, 7);
}
void MRLS2D_TelemetryRead(){

	xil_printf("<<--------- Telemetry MRLS2D ----------->>\r\n");
		UBLOX_read_GPS( );
		UBLOX_readCompass( );
		TMP100_ReadTemADPart( );
		XSysMonInit(SYSMON_DEVICE_ID);
	xil_printf("<<-------------------------------------->>\r\n");
}
void MRLS2D_Net(){

	start_net();
	udpDebugPrintData();
}
void MRLS2D_Process(){

//	SendRawDataToPC();
	printTelemetry();
	MRLS2D_Process_StopDmaAndTransmitUbloxData();
	xemacif_input(echo_netif);
	transfer_data();
}
void MRLS2D_trigerState( bool state ){

	g_stateTriger = state ? true : false;

	int slv_reg7 = ( g_shiftFront  << 1 ) | g_stateTriger;
	Xil_Out32(XPAR_HMC769_0_S00_AXI_BASEADDR + 7*4, slv_reg7);

	MRLS2D_waitResetTrigger();
	MRLS2D_waitSetTrigger();

	if ( state )		xil_printf("INFO: trigRun\r\n");
	else         		xil_printf("INFO: trigStop\r\n");
}
void MRLS2D_DmaRxIntrHandler(void *Callback)
{
//	static uint64_t cnt_turnover = 0;
//	static uint32_t* mem_area_for_azimut[];


	u32 IrqStatus;
	int TimeOut;
	XAxiDma *AxiDmaInst = (XAxiDma *)Callback;
	/* Read pending interrupts */
	IrqStatus = XAxiDma_IntrGetIrq(AxiDmaInst, XAXIDMA_DEVICE_TO_DMA);
	/* Acknowledge pending interrupts */
	XAxiDma_IntrAckIrq(AxiDmaInst, IrqStatus, XAXIDMA_DEVICE_TO_DMA);
	/*
	 * If no interrupt is asserted, we do not do anything
	 */
	if (!(IrqStatus & XAXIDMA_IRQ_ALL_MASK)) {
		return;
	}

//	my_udp_send_RAWD(praw , sizeOfWordTransmitDataDmaFromMemToPC * sizeof( uint32_t ), &ip_addr_broadcast);


//	if( isReverseRawData ){
//
//		for(int i = 0 ; i < 8192; i++)
//			reverseRaw[8191-i] = raw[i];
//		my_udp_send_RAWD(pReverseRaw , sizeOfWordTransmitDataDmaFromMemToPC*4, &ip_addr_broadcast);
//	}
//
//	else{
//
//		my_udp_send_RAWD(praw , sizeOfWordTransmitDataDmaFromMemToPC*4, &ip_addr_broadcast);
//	}

//    if(mem_empty){
////
////
    	mem_empty = false;
////
////
//////		if(part_mem == 0){
//////			part_mem = 1;
////			praw = (int32_t *)RX_BUFFER_BASE4;			//MEMCPY(raw , (u32 *)0x80070000, FRAME_SIZE*4);
			XAxiDma_SimpleTransfer(&AxiDma,(UINTPTR) praw,	MAX_PKT_LEN, XAXIDMA_DEVICE_TO_DMA);
//////		}
//////		else{
//////			part_mem = 0;
//////			praw = (int32_t *)RX_BUFFER_BASE4;			//MEMCPY(raw ,  (u32 *)0x80080000, FRAME_SIZE*4);
//////			XAxiDma_SimpleTransfer(&AxiDma,(UINTPTR) praw,	MAX_PKT_LEN, XAXIDMA_DEVICE_TO_DMA);
//////		}
////
////
//////		my_udp_send_RAWD(praw , sizeOfWordTransmitDataDmaFromMemToPC * sizeof( uint32_t ), &ip_addr_broadcast);
////
//    }



	/*
	 * If error interrupt is asserted, raise error flag, reset the
	 * hardware to recover from the error, and return with no further
	 * processing.
	 */
	if ((IrqStatus & XAXIDMA_IRQ_ERROR_MASK)) {
		Error = 1;

		/* Reset could fail and hang
		 * NEED a way to handle this or do not call it??
		 */
		XAxiDma_Reset(AxiDmaInst);
		TimeOut = RESET_TIMEOUT_COUNTER;

		xil_printf( "ERR: DMA IrqStatus is not Done\r\n" );

		while (TimeOut) {
			if(XAxiDma_ResetIsDone(AxiDmaInst)) {
				break;
			}
			TimeOut -= 1;
		}
		return;
	}
	/*
	 * If completion interrupt is asserted, then set RxDone flag
	 */
	if ((IrqStatus & XAXIDMA_IRQ_IOC_MASK)) {
		RxDone = 1;
	}
}
static void MRLS2D_ReadDNA(){

	Xil_Out32(XPAR_HIER_1_AXI_DNA_0_S00_AXI_BASEADDR, 0x1);
	while( Xil_In32(XPAR_HIER_1_AXI_DNA_0_S00_AXI_BASEADDR + 4) != 1 ){}

	g_dnaPart1 = Xil_In32(XPAR_HIER_1_AXI_DNA_0_S00_AXI_BASEADDR + 8);
	g_dnaPart2 = Xil_In32(XPAR_HIER_1_AXI_DNA_0_S00_AXI_BASEADDR + 12);

	xil_printf("INFO: DnaMRLS2D=%x%x\r\n", g_dnaPart1, g_dnaPart2);
}
static void MRLS2D_waitSetTrigger(){

	    volatile unsigned int* hmcbaseaddr = (unsigned int*)XPAR_HMC769_0_S00_AXI_BASEADDR; // ������� ����� �������� IP ���� �� HMC769
	    volatile unsigned int* ip2mb_reg0 = hmcbaseaddr + 8; // ����� 8-�� �������� ��� ������ ������ �� IP ���� � �� HMC769
	    while ( ((*ip2mb_reg0) & 0x2) != 0x2) {
	    }

}
static void MRLS2D_waitResetTrigger(){

	    volatile unsigned int* hmcbaseaddr = (unsigned int*)XPAR_HMC769_0_S00_AXI_BASEADDR; // ������� ����� �������� IP ���� �� HMC769
	    volatile unsigned int* ip2mb_reg0 = hmcbaseaddr + 8; // ����� 8-�� �������� ��� ������ ������ �� IP ���� � �� HMC769
	    while ( ((*ip2mb_reg0) & 0x2) != 0x0) {
	    }

}
bool MRLS2D_readALMPamp(){

	    volatile unsigned int* hmcbaseaddr = (unsigned int*)XPAR_HMC769_0_S00_AXI_BASEADDR; // ������� ����� �������� IP ���� �� HMC769
	    volatile unsigned int* ip2mb_reg0 = hmcbaseaddr + 8; // ����� 8-�� �������� ��� ������ ������ �� IP ���� � �� HMC769

	    return (((*ip2mb_reg0) & 0x4) != 0) ? 1 : 0;
}
static void MRLS2D_Process_StopDmaAndTransmitUbloxData(){

	if( neededStopDmaAndTransmitUbloxData ){

		MRLS2D_trigerState( false );
			xil_printf("INFO: <<--------- Telemetry MRLS2D ----------->>\r\n");
			UBLOX_readCompass();
			UBLOX_read_GPS();
			mavLinkSendCompassAzimuthToPC();
			mavLinkSendGPSCoordinateToPC();
			mavLinkSendTemperatureToPC();
		MRLS2D_trigerState( true );

		neededStopDmaAndTransmitUbloxData = false;
	}
}
static tHMC769_Reg readConfigHMC769(){


//	HMC769_viewAllDataPLL();
	static struct sHMC769_Reg readListHMC769_Reg[NUM_REGISTERS_HMC769];

	if ( HMC769.currentfrequencyBands == config_HMC_MAIN120){
		for(uint8_t numReg = HMC769_RST_Register_REG; numReg < NUM_REGISTERS_HMC769; numReg++){
			psCurrentHMC769_Reg = &listHMC769_Reg[numReg];
			readListHMC769_Reg[numReg].valueFor120MHz = HMC769_read( psCurrentHMC769_Reg->num );
		}
	}
	else{
		for(uint8_t numReg = HMC769_RST_Register_REG; numReg < NUM_REGISTERS_HMC769; numReg++){
			psCurrentHMC769_Reg = &listHMC769_Reg[numReg];
			readListHMC769_Reg[numReg].valueCalculated = HMC769_read( psCurrentHMC769_Reg->num );
		}
	}

	for(uint8_t numReg = HMC769_RST_Register_REG; numReg < NUM_REGISTERS_HMC769; numReg++){
		psCurrentHMC769_Reg = &listHMC769_Reg[numReg];
		xil_printf( "%-50s %6x %x\r\n",   (psCurrentHMC769_Reg)->psNameRegHMC,
									  (readListHMC769_Reg + numReg)->valueFor120MHz ,
									  (readListHMC769_Reg + numReg)->valueCalculated );
	}


	return readListHMC769_Reg;
}
