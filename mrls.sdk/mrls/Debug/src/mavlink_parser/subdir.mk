################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/mavlink_parser/handlers.c \
../src/mavlink_parser/parser.c 

OBJS += \
./src/mavlink_parser/handlers.o \
./src/mavlink_parser/parser.o 

C_DEPS += \
./src/mavlink_parser/handlers.d \
./src/mavlink_parser/parser.d 


# Each subdirectory must supply rules for building sources it contributes
src/mavlink_parser/%.o: ../src/mavlink_parser/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: ARM v7 gcc compiler'
	arm-none-eabi-gcc -Wall -O0 -g3 -I../../mrls_bsp/ps7_cortexa9_0/include -I"/home/alexbtlab/source/mrls/mrls.sdk/mrls/src/btlink/src/BTLink" -I"/home/alexbtlab/source/mrls/mrls.sdk/mrls/src" -c -fmessage-length=0 -MT"$@" -mcpu=cortex-a9 -mfpu=vfpv3 -mfloat-abi=hard -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


