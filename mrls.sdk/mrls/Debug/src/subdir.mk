################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
LD_SRCS += \
../src/lscript.ld 

C_SRCS += \
../src/ADC_AD9650.c \
../src/MRLS2D.c \
../src/compass.c \
../src/echo.c \
../src/main.c \
../src/mavLinkMRLS.c \
../src/mrls_parameters.c \
../src/mrls_print.c \
../src/mrls_udp.c \
../src/platform.c \
../src/platform_zynq.c \
../src/tmp100.c \
../src/tmp1075.c \
../src/uartForDownPart.c \
../src/xadc.c 

OBJS += \
./src/ADC_AD9650.o \
./src/MRLS2D.o \
./src/compass.o \
./src/echo.o \
./src/main.o \
./src/mavLinkMRLS.o \
./src/mrls_parameters.o \
./src/mrls_print.o \
./src/mrls_udp.o \
./src/platform.o \
./src/platform_zynq.o \
./src/tmp100.o \
./src/tmp1075.o \
./src/uartForDownPart.o \
./src/xadc.o 

C_DEPS += \
./src/ADC_AD9650.d \
./src/MRLS2D.d \
./src/compass.d \
./src/echo.d \
./src/main.d \
./src/mavLinkMRLS.d \
./src/mrls_parameters.d \
./src/mrls_print.d \
./src/mrls_udp.d \
./src/platform.d \
./src/platform_zynq.d \
./src/tmp100.d \
./src/tmp1075.d \
./src/uartForDownPart.d \
./src/xadc.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: ARM v7 gcc compiler'
	arm-none-eabi-gcc -Wall -O0 -g3 -I../../mrls_bsp/ps7_cortexa9_0/include -I"/home/alexbtlab/source/mrls/mrls.sdk/mrls/src/btlink/src/BTLink" -I"/home/alexbtlab/source/mrls/mrls.sdk/mrls/src" -c -fmessage-length=0 -MT"$@" -mcpu=cortex-a9 -mfpu=vfpv3 -mfloat-abi=hard -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


