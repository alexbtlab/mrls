#include "ADC_AD9650.h"

uint32_t g_delayDco = 0;

void AD9650_DELAY_DCO(bool direction, uint32_t step){

	if( direction )
		g_delayDco += 1*step;
	else
		g_delayDco -= 1*step;

	xil_printf("CMD: delay DCO: %d\r\n", g_delayDco);

	AD9650_SetAdrData_andStartTransfer		(AD9650_CHANNEL_INDEX_REG, 0b111, 	WRITE);
		AD9650_SetAdrData_andStartTransfer	(AD9650_DELAY_DCO_REG, 	   g_delayDco, 		WRITE);				// 1 - 40MHz 3 - 20MHz 7 - 10MHz
	AD9650_SetAdrData_andStartTransfer		(AD9650_DEVICE_UPDATE_REG, AD9650_SW_TRANSFER_BITREG, 	WRITE);
}
