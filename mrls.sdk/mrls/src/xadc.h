#ifndef __XSYSMON_H_
#define __XSYSMON_H_

#include <stdio.h>
#include "xsysmon.h"
#include "math.h"

#define RawToVoltage(AdcData) \
	((((float)(AdcData)) * (1.0f)) / 65536.0f)

int XSysMonInit(u16 SysMonDeviceId);
int SysMonFractionToInt(float FloatNum);
float readTemperatureLDO();
float readTemperatureFPGA();
float readOutPowerPAMP();

#endif /* __XSYSMON_H_ */
