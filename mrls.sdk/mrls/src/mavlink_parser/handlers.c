#include "mrls_parameters.h"
#include "mavlink_parser.h"
//#include "net_mrls.h"
#include <string.h>
#include "ADC_AD9650.h"
#include "AD9650.h"
#include "HMC769.h"
#include "mrls_print.h"
#include "compass.h"
#include "mrls_udp.h"
#include "uartForDownPart.h"
#include "uartForDownPart.h"
#include "mavLinkMRLS.h"
#include "mrls_udp.h"

	#define MRLS_VERSION 2
	struct tcp_pcb *server_pcb;
	extern mavlink_message_t 	msgTx;
	struct Param params[MAX_NUM_PARAM+1];
	const char err1[] = "Packet error 1";
	const char err2[] = "Packet error 2";
	const char err3[] = "Packet error 3";
	const char err4[] = "Packet error 4";
	bool enable_transmit_data_to_PC = true;
	bool needed_send_param = false;
	extern volatile double lat;
	extern volatile double lon;
	extern bool gpsDataTransmitToPC;
	extern bool compasDataTransmitToPC;
	extern bool gpsDataTransmitToPC;
	extern uint32_t timeOutReadNMEA;
	extern bool neededPrintTelemetry;
	extern bool enable_transmit_RAWdata_to_PC;
	extern double angelDegree;
	extern XUartPs UartPs;
	extern uint8_t RecvBufferUartNULL[TEST_BUFFER_SIZE_UART];
	extern XAxiDma AxiDma;
	extern bool neededStopDmaAndTransmitUbloxData;
	static volatile mavlink_param_value_t param_value;
	mavlink_heartbeat_t heartbeat;
	void sendError(int errorNum);

void handler(mavlink_message_t* message, const ip_addr_t *addr, u16_t port){

    switch (message->msgid) {
    case MAVLINK_MSG_ID_HEARTBEAT:
        handler_heartbeat(message, addr, port);
        mrls_print("received HB\r\n");		        	break;


//    case MAVLINK_MSG_ID_PARAM_GET:
//        handler_param_get(message);             	break;

    case MAVLINK_MSG_ID_PARAM_SET:
        handler_PARAM_SET(message);				// RXAMP ATTEN PAMP
        break;
    case MAVLINK_MSG_ID_COMMAND_INT:
    	handler_COMMAND_INT(message);
        break;

    case MAVLINK_MSG_ID_PARAM_VALUE:
        handler_PARAM_GET(message);
        break;

    case MAVLINK_MSG_ID_PARAM_REQUEST_LIST:
        handler_PARAM_REQUEST_LIST(message);
        break;

//    case MAVLINK_MSG_ID_PARAM_REQUEST_GROUP:
//        handler_param_set(message);		        break;

//    case MAVLINK_MSG_ID_PARAM_REQUEST_READ:
//        handler_param_REQUEST_READ(message);        break;

    default:
        sendError(0);
        break;
    }
}

ip_addr_t *IPaddrUI;
u16_t portUI;

void handler_heartbeat(mavlink_message_t* message, const ip_addr_t *addr, u16_t port){

    mavlink_msg_heartbeat_decode(message, &heartbeat);

    xil_printf("handler_heartbeat ---%x,%x\r\n", heartbeat.state, heartbeat.version);

    if( heartbeat.state == 0){
    	*IPaddrUI = *addr;
    	portUI = port;

        xil_printf("UIport:%d\r\n", port);
        print_ip("UI IP: ", addr);
    }

}
int handler_PARAM_REQUEST_LIST(mavlink_message_t* message)
{ 
	mavlink_param_request_list_t param_request_list;
	// ��������������

	xil_printf("INFO: handler_PARAM_REQUEST_LIST\r\n");

	mavlink_msg_param_request_list_decode(message, &param_request_list);
    // ������������������ ������ ���� ������ ������������������
    if (param_request_list.target_system != CURRENT_SYSID)
        return -1;

    needed_send_param = true;

    return 1;
}
void zynq_reset (){

//	microblaze_disable_interrupts ();

	XUartPs_Recv(&UartPs, RecvBufferUartNULL, 128);

	GPS_Received_Done      = false;
	GPS_Received_Err = false;

	compasDataTransmitToPC = false;
	gpsDataTransmitToPC = false;

	XScuGic_Stop(&INTCInst);
    (* ((void (*) ()) (0x100000))) (); // ����������
}
int handler_PARAM_SET(mavlink_message_t* message){

	static mavlink_param_set_t param_set;


	xil_printf("INFO: handler_PARAM_SET\r\n");

	mavlink_msg_param_set_decode(message, &param_set);

	uint64_t value = param_set.param_value;

	// ������������������ ������ ���� ������ ������������������
	if (param_set.target_system != CURRENT_SYSID)
		return -1;

	switch (param_set.param_index)    // �������������� ���������������� ��������������
				{
					case 0:
//								mrls_print("INFO: Atten\n\r");
								printf("INFO: Atten=%d\r\n", (int)param_set.param_value);
								HMC769_setAtten( param_set.param_value );
								break;

					case 1: 	mrls_print("INFO: Pamp\n\r");
								UDP_printf("INFO: Pamp");
								if( param_set.param_value)	HMC769_PWR_EN( EN_SPI_CEN  |EN_PAMP  | EN_ROW_PLL | EN_LO_AMP );
								else						HMC769_PWR_EN( DIS_SPI_CEN |DIS_PAMP | EN_ROW_PLL| EN_LO_AMP );
								break;

					case 2: 	mrls_print("INFO: RXamp\n\r");
								MRLS2D_ampEn( param_set.param_value );
								AD9650_ADC_SetSwitch_AMP(param_set.param_value);
								break;

					case 3: 	xil_printf( "INFO: NumAzimuth :%d\n\r" , value );

								if ( value == 12)
									xil_printf( "OK: NumAzimuth :%d\n\r" , value );
								else
									xil_printf( "ERR: NumAzimuth :%d\n\r" , value );

								Xil_Out32(XPAR_AD9650_0_S00_AXI_BASEADDR + 12, value);
								break;
					case 4: 	xil_printf( "INFO: HMC769_SetShiftFront :%d\n\r" , param_set.param_value );

								HMC769_setShiftFront( param_set.param_value );
								break;
					case 6:
								if (  param_set.param_value ){
									xil_printf( "INFO: RunMotor\n\r");
									ADPART_runMotor();
									UDP_printf("INFO: RunMotor...");
//									zynq_reset();
								}
								else{
									xil_printf( "INFO: StopMotor\n\r");
									ADPART_stopMotor();
									UDP_printf("INFO: StopMotor");
								}


								break;
					case 7:
							    AD9650_DELAY_DCO( true, param_set.param_value );

								break;
					case 8:
								switch (param_set.param_value)    // �������������� ���������������� ��������������
										{
											case 0:
												MRLS2D_trigerState( false );
												AD9650_setAveragVal( AVERAG_VAL4 );
												ADPART_MotorSpeed( SPEED_MOTOR_1_25_SEC );
												MRLS2D_trigerState( true );
											break;
											case 1:
												MRLS2D_trigerState( false );
												AD9650_setAveragVal( AVERAG_VAL8 );
												ADPART_MotorSpeed( SPEED_MOTOR_2_5_SEC );
												MRLS2D_trigerState( true );
											break;
											case 2:
												MRLS2D_trigerState( false );
												AD9650_setAveragVal( AVERAG_VAL16 );
												ADPART_MotorSpeed( SPEED_MOTOR_5_SEC );
												MRLS2D_trigerState( true );
											break;
										}
					break;
					default : break;
				}


//	param_value.param_value = param_set.param_value;
//	param_value.param_index = param_set.param_index;
//	param_value.param_id    = param_set.param_id;




//	setParam(&param_value);
		return 1;
}
int handler_PARAM_GET(mavlink_message_t* message){

	mavlink_param_value_t param_value;
	char bufTx[128];
	xil_printf("INFO: handler_PARAM_GET\r\n");

	mavlink_msg_param_value_decode(message, &param_value);
	// ������������������ ������ ���� ������ ������������������
//	if (param_value.target_system != CURRENT_SYSID)
//		return -1;

//	param_value.param_value = param_set.param_value;
//	param_value.param_index = param_set.param_index;


	uint64_t data;
	getParam(&param_value, (uint64_t *)&data);
	xil_printf("INFO:%d\r\n", data);
	mavlink_message_t 	msgTx;

		mavlink_msg_param_value_encode(SYS_ID, COMPONENT_ID, &msgTx, &(params[(param_value.param_index) - 1].param));
		uint16_t len_val = mavlink_msg_to_send_buffer((uint8_t *)bufTx, &msgTx);
		my_udp_send(bufTx, len_val, &ip_addr_broadcast);

		return 1;
}

extern struct sHMC769 HMC769;

bool stateTriger = true;
bool isReverseRawData = false;

int handler_COMMAND_INT(mavlink_message_t* message){

	mavlink_command_int_t command_int;
//	uint16_t len_cmd_ack;
	xil_printf("INFO: handler_COMMAND_INT\r\n");

	mavlink_msg_command_int_decode(message, &command_int);
	if(command_int.target_system != CURRENT_SYSID)
			return -1;

	    switch (command_int.command)    // �������������� ���������������� ��������������
	    {
			case CUSTOM:	 switch (command_int.param1)    // �������������� ���������������� ��������������
		    				{
								case 0: 	xil_printf("CMD: Enable AMP\n\r");
									break;
								case 1:		xil_printf("CMD: Set ATTEN\n\r");
											xil_printf("CMD: ATTEN:%d\n\r", command_int.param2);

											HMC769_setAtten(command_int.param2);
									// �������������� �������������� ���������������� ��������������������
									break;
								case 2:		xil_printf("CMD: Enable TRIG\n\r");		// �������������� �������������� ���������������� ��������������������
											xil_printf("CMD: VAL_TRIG:%d\n\r", command_int.param2);
									break;
								case 3:		xil_printf("CMD: AMP\n\r");		// �������������� �������������� ���������������� ��������������������
											xil_printf("CMD: AMP_VAL:%d\n\r", command_int.param2);
											if(command_int.param2 <= 3)
												configReciever(RECIEVER_NUM_AMP, ON, command_int.param2);
									break;
								case 4:		xil_printf("CMD: frameSizeML\n\r");		// �������������� �������������� ���������������� ��������������������
									break;
								case 5: 	xil_printf("CMD: Set delayset\n\r");
											xil_printf("CMD: delayset:%d\n\r", command_int.param2);
//											AD9508_writeData(0x17, command_int.param2);
									break;
								case 6: 	xil_printf("CMD: Get version\n\r");
											xil_printf("CMD: version:%d\n\r", MRLS_VERSION);
									break;
								case 7: 	xil_printf("CMD: Enable TX data\n\r");
											if(command_int.param2)
												enable_transmit_data_to_PC = true;
											else
												enable_transmit_data_to_PC = false;
								break;
								case 8: 	xil_printf("CMD: Enable TX data\n\r");
											if(command_int.param2)
												enable_transmit_RAWdata_to_PC = true;
											else
												enable_transmit_RAWdata_to_PC = false;
								break;
								case 9:
									xil_printf("CMD: Request Compass Data\n\r");

//										MRLS2D_trigerState( false );
//										UBLOX_readCompass();
//										mavLinkSendCompassAzimuthToPC();         // Get Compass Azimuth
//										compasDataTransmitToPC = true;
//										MRLS2D_trigerState( true );
										neededStopDmaAndTransmitUbloxData = true;

									break;
								case 10:
									xil_printf("CMD: Request GPS Data\n\r");
									if ( true ){
//										MRLS2D_trigerState( false );


//										XScuGic_Enable(&INTCInst, UART_INT_IRQ_ID);

//										for(uint32_t i = 0 ; i < 1000000000; i++){}
//										UBLOX_read_GPS();
//										XScuGic_Disable(&INTCInst, UART_INT_IRQ_ID);
//										XScuGic_Disconnect(&INTCInst, XPAR_FABRIC_AXIDMA_0_VEC_ID);
//										mavLinkSendGPSCoordinateToPC();         // Get Compass Azimuth
//										for(uint32_t i = 0 ; i < 1000000000; i++){}

//										MRLS2D_trigerState( true );
									}
								break;
								case 11:
									neededPrintTelemetry = !neededPrintTelemetry;
									if (neededPrintTelemetry)
										xil_printf("CMD: Send telemetry Enable\n\r");
									else
										xil_printf("CMD: Send telemetry Disable\n\r");
								break;
								case 12:
										switch (command_int.param2)    // �������������� ���������������� ��������������
											{
												case 0:     MRLS2D_trigerState( false );
															xil_printf("CMD: Set frequency band 120 main Mhz HMC769 \n\r");
															HMC769_SOFT_RESET
															HMC769_DISABLE_FROM_SPI
															HMC769_ENABLE_FROM_SPI
															HMC769_init();
															HMC769.currentfrequencyBands = config_HMC_MAIN120;
															HMC769_configIC2( HMC769.currentfrequencyBands );
//															HMC769_configIC( config_HMC_MAIN60 );
															MRLS2D_trigerState( true );
												break;
												case 1:
															MRLS2D_trigerState( false );
															xil_printf("CMD: Set Custom frequency band HMC769\n\r");

															uint32_t Fstart = command_int.param3;
															uint32_t Fstop  = command_int.param4;
															uint32_t Tramp  = command_int.param5;
															HMC769_CalculateParam( Fstart, Fstop, Tramp);
															HMC769_SOFT_RESET
															HMC769_DISABLE_FROM_SPI
															HMC769_ENABLE_FROM_SPI
															HMC769_init();
															HMC769.currentfrequencyBands = config_HMC_CUSTOM;
															HMC769_configIC2( HMC769.currentfrequencyBands );
															MRLS2D_trigerState( true );//
//															HMC769_configIC( config_HMC_MAIN120 );
												break;

												default : break;
											}
								break;
								case 13:
									stateTriger = !stateTriger;
									MRLS2D_trigerState( stateTriger );
								break;
								case 14:
									MRLS2D_trigerState( false );
									isReverseRawData = !isReverseRawData;
									MRLS2D_trigerState( true );
								break;
								default : break;
		    				}






			break;
			case REBOOT:				break;			//send_cmdACK_mavLink();
//										mb_reset();
										break;
			case RESET_PARAM:	    	/*------------------*/	break;
			case BT_CMD_TYPE_ENUM_END:  /*------------------*/	break;
			default:   					sendError(0);			break;
	    }
	return 1;
}
void sendError(int errorNum) {

	mavlink_message_t msg;
    char error[128];
    char buf[MAVLINK_MAX_PACKET_LEN];
    switch (errorNum)
    {
    case 0:
        strncpy(error, err1, strlen(err1));
        break;
    default:
        break;
    }
    mavlink_msg_error_pack(0, 0, &msg, 0, 5526, error);
    int len = mavlink_msg_to_send_buffer((uint8_t *)buf, &msg);
    tcp_write(server_pcb, &buf, len, 1);
}
void mb_reset (){
    
//	microblaze_disable_interrupts ();
    (* ((void (*) ()) (0x80000000))) (); // ������������������������������
}
