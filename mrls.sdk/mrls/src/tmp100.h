
#ifndef __TMP100_H_
#define __TMP100_H_

#include "xil_types.h"
#include "xiicps.h"
#include "stdio.h"

#define IIC_READ_SIZE 128
#define IIC_ADPART_TMP100_SLAVE_ADDR		0x48
#define TMP100_TEMP_REG 0
#define TMP100_CONF_REG 1

float TMP100_ReadTemADPart(  );
void TMP100_setCONG_REG_RESOLUTION_12BIT(  );

extern XIicPs Iic;

#endif //  __TMP100_H_

