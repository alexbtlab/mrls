#include <mavLinkMRLS.h>



uint64_t timeToSendTeletry = 0;

void mavLinkSendCompassAzimuthToPC()
{
	char bufTx[128];
	mavlink_message_t 	msgTx;
	mavlink_param_value_t param_value;
	param_value.param_count = 3;
	param_value.param_value = angelDegree;


			mavlink_msg_param_value_encode(SYS_ID, COMPONENT_ID, &msgTx, &param_value);
			uint16_t len_val = mavlink_msg_to_send_buffer((uint8_t *)bufTx, &msgTx);
			my_udp_send(bufTx, len_val, &ip_addr_broadcast);

			compasDataTransmitToPC = true;
}
void mavLinkSendGPSCoordinateToPC()
{
//	if( GPS_Received_Done ){	// Если приняли валидные координаты
		char bufTx[128];
		mavlink_message_t 	msgTx;
		mavlink_param_value_t param_value;
		param_value.param_count = 4;
		param_value.param_value = (  ((uint64_t)(lon * 10000000)  ) |
									 ((uint64_t)(lat * 10000000) << 32)
								  );



				mavlink_msg_param_value_encode(SYS_ID, COMPONENT_ID, &msgTx, &param_value);
				uint16_t len_val = mavlink_msg_to_send_buffer((uint8_t *)bufTx, &msgTx);
				my_udp_send(bufTx, len_val, &ip_addr_broadcast);

				printf("INFO: lat == %f, lon == %f\r\n", lat, lon);

//		gpsDataTransmitToPC = true;
//	}
	if( GPS_Received_Err )
		gpsDataTransmitToPC = true;
}
void mavLinkSendTemperatureToPC(){

	XSysMon *SysMonInstPtr = &SysMonInst;
	u32 TempRawData;
	u16 Vaux_00_RawData;
	u32 Vaux_08_RawData;
	float TempFPGA;
	float Vaux_00_Data;
	float Vaux_08_Data;
	float TempLDO;

	char bufTx[128];

//			mavlink_message_t 	msgTx;
//			mavlink_param_value_t param_value;
//			param_value.param_count = 4;
//			param_value.param_value = (  ((uint64_t)(lon * 10000000)  ) |
//										 ((uint64_t)(lat * 10000000) << 32)
//									  );
//
//
//
//					mavlink_msg_param_value_encode(SYS_ID, COMPONENT_ID, &msgTx, &param_value);
//					uint16_t len_val = mavlink_msg_to_send_buffer((uint8_t *)bufTx, &msgTx);
//					my_udp_send(bufTx, len_val, &ip_addr_broadcast);

		XSysMon_GetStatus(SysMonInstPtr); /* Clear the old status */
		while ((XSysMon_GetStatus(SysMonInstPtr) & XSM_SR_EOS_MASK) !=
				XSM_SR_EOS_MASK);
	/////////////////////////////////////////////////////////////////////////////////////////////
		/*
		 * Read the on-chip Temperature Data (Current/Maximum/Minimum)
		 * from the ADC data registers.
		 */
		TempRawData = XSysMon_GetAdcData(SysMonInstPtr, XSM_CH_TEMP);
		TempFPGA = XSysMon_RawToTemperature(TempRawData);


		Vaux_00_RawData = XSysMon_GetAdcData(SysMonInstPtr, XSM_CH_AUX_MIN);
		Vaux_00_Data = RawToVoltage(Vaux_00_RawData);
		TempLDO = Vaux_00_Data * 100;

		Vaux_08_RawData = XSysMon_GetAdcData(SysMonInstPtr, XSM_CH_AUX_MIN + 8);
		Vaux_08_Data = RawToVoltage(Vaux_08_RawData);

		printf("INFO: The Current Temperature FPGA is %0d.%03d Centigrades.\r\n",
					(int)(TempFPGA), SysMonFractionToInt(TempFPGA));
		printf("INFO: The Current Temperature is LDO %0d.%03d Centigrades.\r\n",
					(int)(TempLDO), SysMonFractionToInt(TempLDO));
		printf("INFO: The Current OutPower is %0d.%03d dbm\r\n",
					(int)(Vaux_08_Data), SysMonFractionToInt(Vaux_08_Data));

		mavlink_message_t 	msgTx;
		mavlink_temp_raw_t temp_raw;

		temp_raw.time =  ((int)( TempFPGA * 100 )) |  (((int)(TempLDO*100)) << 16 )  ;

//		float tempADPart = TMP1075_ReadTemPAMP();
//
//		printf("INFO: The Current OutPower is %0d.%03d dbm\r\n",
//							(int)(Vaux_08_Data), SysMonFractionToInt(Vaux_08_Data));
//
//		temp_raw.reserv_1 =  (int)(tempADPart * 100) ;
//	    temp_raw.reserv_2 = ((int)(tempADPart * 100) & 0xFF00 ) >> 8;

		float tempPAMP = TMP1075_ReadTemPAMP();

		printf("INFO: The Current temp PAMP is %f C\r\n", tempPAMP);

		temp_raw.reserv_1 =  (int)(tempPAMP * 100) ;
	    temp_raw.reserv_2 = ((int)(tempPAMP * 100) & 0xFF00 ) >> 8;

		mavlink_msg_temp_raw_encode(SYS_ID, COMPONENT_ID, &msgTx, &temp_raw);
		uint16_t len_val = mavlink_msg_to_send_buffer((uint8_t *)bufTx, &msgTx);
		my_udp_send(bufTx, len_val, &ip_addr_broadcast);
}
void printTelemetry()
{

	mavlink_message_t 	msgTx;
	mavlink_heartbeat_t heartbeat;
	char bufTx[128];

	if ( neededPrintTelemetry ){
//		bool stateALMPamp = MRLS2D_readALMPamp();
//		if( stateALMPamp )
//			printf("INFO: ALM PAMP\r\n" );
		heartbeat.version = 12;
		heartbeat.state = 1;

		timeToSendTeletry++;
		if ( timeToSendTeletry > TIME_TO_SEND_TELEMETRY){
			timeToSendTeletry =  0;

//			mavlink_msg_heartbeat_encode(SYS_ID, COMPONENT_ID, &msgTx, &heartbeat);
//			uint16_t len_val = mavlink_msg_to_send_buffer((uint8_t *)bufTx, &msgTx);
//			my_udp_send(bufTx, len_val, &ip_addr_broadcast);

//			xil_printf("Send HB from Sashok to Artem !!!\r\n");
			mavLinkSendCompassAzimuthToPC();
			mavLinkSendGPSCoordinateToPC();
			mavLinkSendTemperatureToPC();

//			printf("INFO: Temperature FPGA   = %f\r\n",       readTemperatureFPGA() );
//			printf("INFO: Temperature LDO     = %f\r\n",      readTemperatureLDO() );
//			printf("INFO: Temperature PAMP   = %f\r\n",       TMP1075_ReadTemPAMP() );
////			printf("INFO: Temperature ADPart = %f\r\n",       TMP100_ReadTemADPart() );
//			printf("INFO: OutPower PAMP        = %f\r\n",     readOutPowerPAMP() );
		}
	}
}
