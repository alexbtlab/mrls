#pragma once
// MESSAGE CMD_ACK PACKING

#define MAVLINK_MSG_ID_CMD_ACK 5


typedef struct __mavlink_cmd_ack_t {
 uint32_t time; /*<  Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude of the number.*/
 uint8_t cmd; /*<    .*/
 uint8_t result; /*<   .*/
} mavlink_cmd_ack_t;

#define MAVLINK_MSG_ID_CMD_ACK_LEN 6
#define MAVLINK_MSG_ID_CMD_ACK_MIN_LEN 6
#define MAVLINK_MSG_ID_5_LEN 6
#define MAVLINK_MSG_ID_5_MIN_LEN 6

#define MAVLINK_MSG_ID_CMD_ACK_CRC 241
#define MAVLINK_MSG_ID_5_CRC 241



#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_CMD_ACK { \
    5, \
    "CMD_ACK", \
    3, \
    {  { "time", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_cmd_ack_t, time) }, \
         { "cmd", NULL, MAVLINK_TYPE_UINT8_T, 0, 4, offsetof(mavlink_cmd_ack_t, cmd) }, \
         { "result", NULL, MAVLINK_TYPE_UINT8_T, 0, 5, offsetof(mavlink_cmd_ack_t, result) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_CMD_ACK { \
    "CMD_ACK", \
    3, \
    {  { "time", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_cmd_ack_t, time) }, \
         { "cmd", NULL, MAVLINK_TYPE_UINT8_T, 0, 4, offsetof(mavlink_cmd_ack_t, cmd) }, \
         { "result", NULL, MAVLINK_TYPE_UINT8_T, 0, 5, offsetof(mavlink_cmd_ack_t, result) }, \
         } \
}
#endif

/**
 * @brief Pack a cmd_ack message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param time  Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude of the number.
 * @param cmd    .
 * @param result   .
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_cmd_ack_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint32_t time, uint8_t cmd, uint8_t result)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_CMD_ACK_LEN];
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_uint8_t(buf, 4, cmd);
    _mav_put_uint8_t(buf, 5, result);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_CMD_ACK_LEN);
#else
    mavlink_cmd_ack_t packet;
    packet.time = time;
    packet.cmd = cmd;
    packet.result = result;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_CMD_ACK_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_CMD_ACK;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_CMD_ACK_MIN_LEN, MAVLINK_MSG_ID_CMD_ACK_LEN, MAVLINK_MSG_ID_CMD_ACK_CRC);
}

/**
 * @brief Pack a cmd_ack message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param time  Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude of the number.
 * @param cmd    .
 * @param result   .
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_cmd_ack_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint32_t time,uint8_t cmd,uint8_t result)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_CMD_ACK_LEN];
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_uint8_t(buf, 4, cmd);
    _mav_put_uint8_t(buf, 5, result);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_CMD_ACK_LEN);
#else
    mavlink_cmd_ack_t packet;
    packet.time = time;
    packet.cmd = cmd;
    packet.result = result;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_CMD_ACK_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_CMD_ACK;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_CMD_ACK_MIN_LEN, MAVLINK_MSG_ID_CMD_ACK_LEN, MAVLINK_MSG_ID_CMD_ACK_CRC);
}

/**
 * @brief Encode a cmd_ack struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param cmd_ack C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_cmd_ack_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_cmd_ack_t* cmd_ack)
{
    return mavlink_msg_cmd_ack_pack(system_id, component_id, msg, cmd_ack->time, cmd_ack->cmd, cmd_ack->result);
}

/**
 * @brief Encode a cmd_ack struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param cmd_ack C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_cmd_ack_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_cmd_ack_t* cmd_ack)
{
    return mavlink_msg_cmd_ack_pack_chan(system_id, component_id, chan, msg, cmd_ack->time, cmd_ack->cmd, cmd_ack->result);
}

/**
 * @brief Send a cmd_ack message
 * @param chan MAVLink channel to send the message
 *
 * @param time  Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude of the number.
 * @param cmd    .
 * @param result   .
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_cmd_ack_send(mavlink_channel_t chan, uint32_t time, uint8_t cmd, uint8_t result)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_CMD_ACK_LEN];
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_uint8_t(buf, 4, cmd);
    _mav_put_uint8_t(buf, 5, result);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_CMD_ACK, buf, MAVLINK_MSG_ID_CMD_ACK_MIN_LEN, MAVLINK_MSG_ID_CMD_ACK_LEN, MAVLINK_MSG_ID_CMD_ACK_CRC);
#else
    mavlink_cmd_ack_t packet;
    packet.time = time;
    packet.cmd = cmd;
    packet.result = result;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_CMD_ACK, (const char *)&packet, MAVLINK_MSG_ID_CMD_ACK_MIN_LEN, MAVLINK_MSG_ID_CMD_ACK_LEN, MAVLINK_MSG_ID_CMD_ACK_CRC);
#endif
}

/**
 * @brief Send a cmd_ack message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_cmd_ack_send_struct(mavlink_channel_t chan, const mavlink_cmd_ack_t* cmd_ack)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_cmd_ack_send(chan, cmd_ack->time, cmd_ack->cmd, cmd_ack->result);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_CMD_ACK, (const char *)cmd_ack, MAVLINK_MSG_ID_CMD_ACK_MIN_LEN, MAVLINK_MSG_ID_CMD_ACK_LEN, MAVLINK_MSG_ID_CMD_ACK_CRC);
#endif
}

#if MAVLINK_MSG_ID_CMD_ACK_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_cmd_ack_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint32_t time, uint8_t cmd, uint8_t result)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_uint8_t(buf, 4, cmd);
    _mav_put_uint8_t(buf, 5, result);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_CMD_ACK, buf, MAVLINK_MSG_ID_CMD_ACK_MIN_LEN, MAVLINK_MSG_ID_CMD_ACK_LEN, MAVLINK_MSG_ID_CMD_ACK_CRC);
#else
    mavlink_cmd_ack_t *packet = (mavlink_cmd_ack_t *)msgbuf;
    packet->time = time;
    packet->cmd = cmd;
    packet->result = result;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_CMD_ACK, (const char *)packet, MAVLINK_MSG_ID_CMD_ACK_MIN_LEN, MAVLINK_MSG_ID_CMD_ACK_LEN, MAVLINK_MSG_ID_CMD_ACK_CRC);
#endif
}
#endif

#endif

// MESSAGE CMD_ACK UNPACKING


/**
 * @brief Get field time from cmd_ack message
 *
 * @return  Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude of the number.
 */
static inline uint32_t mavlink_msg_cmd_ack_get_time(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint32_t(msg,  0);
}

/**
 * @brief Get field cmd from cmd_ack message
 *
 * @return    .
 */
static inline uint8_t mavlink_msg_cmd_ack_get_cmd(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  4);
}

/**
 * @brief Get field result from cmd_ack message
 *
 * @return   .
 */
static inline uint8_t mavlink_msg_cmd_ack_get_result(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  5);
}

/**
 * @brief Decode a cmd_ack message into a struct
 *
 * @param msg The message to decode
 * @param cmd_ack C-struct to decode the message contents into
 */
static inline void mavlink_msg_cmd_ack_decode(const mavlink_message_t* msg, mavlink_cmd_ack_t* cmd_ack)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    cmd_ack->time = mavlink_msg_cmd_ack_get_time(msg);
    cmd_ack->cmd = mavlink_msg_cmd_ack_get_cmd(msg);
    cmd_ack->result = mavlink_msg_cmd_ack_get_result(msg);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_CMD_ACK_LEN? msg->len : MAVLINK_MSG_ID_CMD_ACK_LEN;
        memset(cmd_ack, 0, MAVLINK_MSG_ID_CMD_ACK_LEN);
    memcpy(cmd_ack, _MAV_PAYLOAD(msg), len);
#endif
}
