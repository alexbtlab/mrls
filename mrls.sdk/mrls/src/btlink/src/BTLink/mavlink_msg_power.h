#pragma once
// MESSAGE POWER PACKING

#define MAVLINK_MSG_ID_POWER 20


typedef struct __mavlink_power_t {
 uint32_t time; /*<  Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude of the number.*/
 int8_t value; /*<   .*/
} mavlink_power_t;

#define MAVLINK_MSG_ID_POWER_LEN 5
#define MAVLINK_MSG_ID_POWER_MIN_LEN 5
#define MAVLINK_MSG_ID_20_LEN 5
#define MAVLINK_MSG_ID_20_MIN_LEN 5

#define MAVLINK_MSG_ID_POWER_CRC 197
#define MAVLINK_MSG_ID_20_CRC 197



#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_POWER { \
    20, \
    "POWER", \
    2, \
    {  { "time", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_power_t, time) }, \
         { "value", NULL, MAVLINK_TYPE_INT8_T, 0, 4, offsetof(mavlink_power_t, value) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_POWER { \
    "POWER", \
    2, \
    {  { "time", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_power_t, time) }, \
         { "value", NULL, MAVLINK_TYPE_INT8_T, 0, 4, offsetof(mavlink_power_t, value) }, \
         } \
}
#endif

/**
 * @brief Pack a power message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param time  Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude of the number.
 * @param value   .
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_power_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint32_t time, int8_t value)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_POWER_LEN];
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_int8_t(buf, 4, value);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_POWER_LEN);
#else
    mavlink_power_t packet;
    packet.time = time;
    packet.value = value;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_POWER_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_POWER;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_POWER_MIN_LEN, MAVLINK_MSG_ID_POWER_LEN, MAVLINK_MSG_ID_POWER_CRC);
}

/**
 * @brief Pack a power message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param time  Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude of the number.
 * @param value   .
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_power_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint32_t time,int8_t value)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_POWER_LEN];
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_int8_t(buf, 4, value);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_POWER_LEN);
#else
    mavlink_power_t packet;
    packet.time = time;
    packet.value = value;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_POWER_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_POWER;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_POWER_MIN_LEN, MAVLINK_MSG_ID_POWER_LEN, MAVLINK_MSG_ID_POWER_CRC);
}

/**
 * @brief Encode a power struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param power C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_power_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_power_t* power)
{
    return mavlink_msg_power_pack(system_id, component_id, msg, power->time, power->value);
}

/**
 * @brief Encode a power struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param power C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_power_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_power_t* power)
{
    return mavlink_msg_power_pack_chan(system_id, component_id, chan, msg, power->time, power->value);
}

/**
 * @brief Send a power message
 * @param chan MAVLink channel to send the message
 *
 * @param time  Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude of the number.
 * @param value   .
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_power_send(mavlink_channel_t chan, uint32_t time, int8_t value)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_POWER_LEN];
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_int8_t(buf, 4, value);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_POWER, buf, MAVLINK_MSG_ID_POWER_MIN_LEN, MAVLINK_MSG_ID_POWER_LEN, MAVLINK_MSG_ID_POWER_CRC);
#else
    mavlink_power_t packet;
    packet.time = time;
    packet.value = value;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_POWER, (const char *)&packet, MAVLINK_MSG_ID_POWER_MIN_LEN, MAVLINK_MSG_ID_POWER_LEN, MAVLINK_MSG_ID_POWER_CRC);
#endif
}

/**
 * @brief Send a power message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_power_send_struct(mavlink_channel_t chan, const mavlink_power_t* power)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_power_send(chan, power->time, power->value);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_POWER, (const char *)power, MAVLINK_MSG_ID_POWER_MIN_LEN, MAVLINK_MSG_ID_POWER_LEN, MAVLINK_MSG_ID_POWER_CRC);
#endif
}

#if MAVLINK_MSG_ID_POWER_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_power_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint32_t time, int8_t value)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_int8_t(buf, 4, value);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_POWER, buf, MAVLINK_MSG_ID_POWER_MIN_LEN, MAVLINK_MSG_ID_POWER_LEN, MAVLINK_MSG_ID_POWER_CRC);
#else
    mavlink_power_t *packet = (mavlink_power_t *)msgbuf;
    packet->time = time;
    packet->value = value;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_POWER, (const char *)packet, MAVLINK_MSG_ID_POWER_MIN_LEN, MAVLINK_MSG_ID_POWER_LEN, MAVLINK_MSG_ID_POWER_CRC);
#endif
}
#endif

#endif

// MESSAGE POWER UNPACKING


/**
 * @brief Get field time from power message
 *
 * @return  Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude of the number.
 */
static inline uint32_t mavlink_msg_power_get_time(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint32_t(msg,  0);
}

/**
 * @brief Get field value from power message
 *
 * @return   .
 */
static inline int8_t mavlink_msg_power_get_value(const mavlink_message_t* msg)
{
    return _MAV_RETURN_int8_t(msg,  4);
}

/**
 * @brief Decode a power message into a struct
 *
 * @param msg The message to decode
 * @param power C-struct to decode the message contents into
 */
static inline void mavlink_msg_power_decode(const mavlink_message_t* msg, mavlink_power_t* power)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    power->time = mavlink_msg_power_get_time(msg);
    power->value = mavlink_msg_power_get_value(msg);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_POWER_LEN? msg->len : MAVLINK_MSG_ID_POWER_LEN;
        memset(power, 0, MAVLINK_MSG_ID_POWER_LEN);
    memcpy(power, _MAV_PAYLOAD(msg), len);
#endif
}
