#ifndef __COMPASS_H_
#define __COMPASS_H_

	#include <stdio.h>
	#include "xiicps.h"
	#include "math.h"
	#include "stdbool.h"
	#include "tmp100.h"

	#define HMC5883L_ADDRESS            0x1E // this device only has one address
	#define HMC5883L_DEFAULT_ADDRESS    (HMC5883L_ADDRESS<<1)
	#define HMC5883L_RA_CONFIG_A        0x00
	#define HMC5883L_RA_CONFIG_B        0x01
	#define HMC5883L_RA_MODE            0x02
	#define HMC5883L_RA_DATAX_H         0x03
	#define HMC5883L_RA_DATAX_L         0x04
	#define HMC5883L_RA_DATAY_H         0x05
	#define HMC5883L_RA_DATAY_L         0x06
	#define HMC5883L_RA_DATAZ_H         0x07
	#define HMC5883L_RA_DATAZ_L         0x08
	#define HMC5883L_RA_STATUS          0x09
	#define HMC5883L_RA_ID_A            0x0A
	#define HMC5883L_RA_ID_B            0x0B
	#define HMC5883L_RA_ID_C            0x0C
	#define HMC5883L_CRA_AVERAGE_BIT    6
	#define HMC5883L_CRA_AVERAGE_LENGTH 2
	#define HMC5883L_CRA_RATE_BIT       4
	#define HMC5883L_CRA_RATE_LENGTH    3
	#define HMC5883L_CRA_BIAS_BIT       1
	#define HMC5883L_CRA_BIAS_LENGTH    2
	#define HMC5883L_AVERAGING_1        0x00
	#define HMC5883L_AVERAGING_2        0x01
	#define HMC5883L_AVERAGING_4        0x02
	#define HMC5883L_AVERAGING_8        0x03
	#define HMC5883L_RATE_0P75          0x00
	#define HMC5883L_RATE_1P5           0x01
	#define HMC5883L_RATE_3             0x02
	#define HMC5883L_RATE_7P5           0x03
	#define HMC5883L_RATE_15            0x04
	#define HMC5883L_RATE_30            0x05
	#define HMC5883L_RATE_75            0x06
	#define HMC5883L_BIAS_NORMAL        0x00
	#define HMC5883L_BIAS_POSITIVE      0x01
	#define HMC5883L_BIAS_NEGATIVE      0x02
	#define HMC5883L_CRB_GAIN_BIT       7
	#define HMC5883L_CRB_GAIN_LENGTH    3
	#define HMC5883L_GAIN_1370          0x00
	#define HMC5883L_GAIN_1090          0x01
	#define HMC5883L_GAIN_820           0x02
	#define HMC5883L_GAIN_660           0x03
	#define HMC5883L_GAIN_440           0x04
	#define HMC5883L_GAIN_390           0x05
	#define HMC5883L_GAIN_330           0x06
	#define HMC5883L_GAIN_220           0x07
	#define HMC5883L_MODEREG_BIT        1
	#define HMC5883L_MODEREG_LENGTH     2
	#define HMC5883L_MODE_CONTINUOUS    0x00
	#define HMC5883L_MODE_SINGLE        0x01
	#define HMC5883L_MODE_IDLE          0x02
	#define HMC5883L_STATUS_LOCK_BIT    1
	#define HMC5883L_STATUS_READY_BIT   0
	#define TEST_BUFFER_SIZE 128
	#define IIC_DEVICE_ID		XPAR_XIICPS_0_DEVICE_ID
	#define IIC_SCLK_RATE		100000
	#define IIC_COMPASS_SLAVE_ADDR		0x1E << 0
	#define HMC5883l_Enable_A (0x10)
	#define HMC5883l_Enable_B (0x00)
	#define HMC5883l_MR (0x00)

	int i2c_init();
	void compasInit();
	void UBLOX_readCompass();

	int i2c_init_TEMP_PAMP();

	extern bool compasDataTransmitToPC;

#endif //  __MAIN_H_
