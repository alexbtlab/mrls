#include "mrls_parameters.h"

bool tmp_PAMP_val = false;

struct Param PAMP_state_Param;
struct Param RX_Num_Amp_Param;

void getterRXnumAMP(void *value) {}
void setParam(mavlink_param_value_t *newParam) {

	xil_printf("INFO_P: setParam\r\n");
	params[(newParam->param_index)].setter((uint64_t *)&(newParam->param_value));
}
void getParam(mavlink_param_value_t *Param, void* data){

	xil_printf("INFO_P: getParam->%d\r\n", (Param->param_index));
	params[(Param->param_index)].getter(data);
}
void getterPAMP_State(void *value) {}
void setterPAMP_State(void *value) {

	tmp_PAMP_val = (bool)*((bool *)value);
	xil_printf("INFO_P: setterPAMP->%d\r\n", tmp_PAMP_val);

	if(tmp_PAMP_val)
		configReciever(PAMP_state, 	ON, 0);
	else
		configReciever(PAMP_state, 	OFF, 0);
}
void setterRXnumAMP(void *value) {}
void addParam(struct Param* param) {
	params[param->param.param_index] = *param;		// Какой идекс имеет паратетр, в ту ячейку массива кладем параметр
}
void configReciever(param_t param, stateConfigReciever_t state, uint8_t data){

	volatile unsigned int* configRecieverbaseaddr = NULL;//(unsigned int*)XPAR_CONFIGRECIEVER_0_S00_AXI_BASEADDR;

	volatile unsigned int* slv_reg1 = configRecieverbaseaddr + 1;
	volatile unsigned int* slv_reg2 = configRecieverbaseaddr + 2;
	volatile unsigned int* slv_reg3 = configRecieverbaseaddr + 3;
	volatile unsigned int* slv_reg4 = configRecieverbaseaddr + 4;

	switch (param){
		case ATTEN: 						            		*slv_reg1 = data;										break;
		/*-------------------------------------------------------------------------------------------------------------------*/
		case PAMP_state:
		if (state == ON){	*slv_reg3  |=   0x1;
							PAMP_state_Param = (struct Param){{true,  PARAM_COUNT_TX_GROUP, 2, "PAMP_state", "TXGroup", PARAM_TYPE_BIT}, setterPAMP_State, getterPAMP_State};
		}
		else{				*slv_reg3  &=  ~0x1;
							PAMP_state_Param = (struct Param){{false, PARAM_COUNT_TX_GROUP, 2, "PAMP_state", "TXGroup", PARAM_TYPE_BIT}, setterPAMP_State, getterPAMP_State};
		}
		addParam(&PAMP_state_Param);
		break;
		/*-------------------------------------------------------------------------------------------------------------------*/
		case ADC_MODE_PDn: 		                        		*slv_reg2 = data; 	 	break;
		/*-------------------------------------------------------------------------------------------------------------------*/
		case ETH_REF_CLK_EN_and_RST: 		if (state == ON) 	*slv_reg3  |=   0x2;
											else				*slv_reg3  &=  ~0x2;	break;
		/*-------------------------------------------------------------------------------------------------------------------*/
		case RECIEVER_NUM_AMP: 	*slv_reg4 =  data;
								RX_Num_Amp_Param = (struct Param){{data, 1, 4, "RX_Num_AMP",  "RXGroup", PARAM_TYPE_UINT32}, setterRXnumAMP,	getterRXnumAMP};
								addParam(&RX_Num_Amp_Param);							break;
		/*-------------------------------------------------------------------------------------------------------------------*/
		default : break;
	}



}
