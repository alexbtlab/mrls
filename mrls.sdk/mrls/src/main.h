#ifndef __MAIN_H_
#define __MAIN_H_

	#include <stdio.h>
	#include "xgpiops.h"
	#include "xparameters.h"
	#include "assert.h"
	#include "netif/xadapter.h"
	#include "platform.h"
	#include "platform_config.h"
	#include "xil_printf.h"
	#include "lwip/tcp.h"
	#include "xil_cache.h"
	#include "lwip/dhcp.h"
	#include "xaxidma.h"
	#include "xstatus.h"
	#include "xil_io.h"
	#include "stdbool.h"
	#include "xscugic.h"
	#include "mrls_udp.h"
	#include "definition.h"
	#include "definition.h"
    #include "xparameters.h"
	#include "AD9650.h"
	#include "mrls_udp.h"
	#include "HMC5883.h"
	#include "uartForDownPart.h"
	#include "math.h"
//	#include "HMC769_2.h"
	#include "compass.h"
	#include "MRLS2D.h"

	/*--- MACROS PROJECT ---*/
	#define DMA_DEV_ID		XPAR_AXIDMA_0_DEVICE_ID
	#define RX_INTR_ID		XPAR_FABRIC_AXIDMA_0_VEC_ID
	#define INTC			XScuGic
	#define INTC_HANDLER	XScuGic_InterruptHandler

	#define XPAR_SWITCHMICROWAVEPATH_0_S00_AXI_BASEADDR 	XPAR_HIER_1_SWITCHMICROWAVEPATH_0_S00_AXI_BASEADDR
	#define XPAR_AVERAGEFFT_0_INTERRUPT_FRAME_INTR 			XPAR_FABRIC_HIER_0_AVERAGEFFT_0_INTERRUPT_FRAME_INTR
	#define PWDN_SET   Xil_Out32(XPAR_AD9650_0_S00_AXI_BASEADDR + 8 , 1 << 1);
	#define PWDN_RESET Xil_Out32(XPAR_AD9650_0_S00_AXI_BASEADDR + 8 , 0 << 1);
	#define AUTO_CALIBRATION_WAIT Xil_In32(XPAR_AD9650_0_S00_AXI_BASEADDR + 16) == 0
	#define SWEEP_VAL(val) 			Xil_Out32(XPAR_HMC769_0_S00_AXI_BASEADDR + 24 , val);
	#define RX_BUFFER_BASE			(XPAR_PS7_DDR_0_S_AXI_BASEADDR + 0x00300000)
	#define RX_BUFFER_BASE5			(XPAR_PS7_DDR_0_S_AXI_BASEADDR + 0x00500000)
	#define WAITING_FOR_TRANSMIT_UBLOX_DATA ( !( compasDataTransmitToPC && gpsDataTransmitToPC )  )

	void print_app_header();
	void tcp_fasttmr(void);
	void tcp_slowtmr(void);
	void FRAME_Intr_Handler(void *InstancePtr);
	void lwip_init();
	void RxIntrHandler(void *Callback);
	void SendRawDataToPC();
	void app(void);
	int start_application();
	int transfer_data();
	void Process_mrls();
	void trigerState( bool state );
	err_t dhcp_start(struct netif *netif);

	extern volatile size_t g_shiftFront;
	extern bool  g_stateTriger;
	extern bool mem_empty;
	extern int32_t raw[2048];
	extern int32_t* praw;
	extern XAxiDma AxiDma;
	extern volatile int dhcp_timoutcntr;
	extern volatile int TcpFastTmrFlag;
	extern volatile int TcpSlowTmrFlag;

#endif //  __MAIN_H_
