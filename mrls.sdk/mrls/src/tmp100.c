#include "tmp100.h"

static int TMP100_setADR(uint8_t adr );

#define TIMEOUT_WAIT_TMP100 100000000

int TMP100_waitIIC(){

	uint32_t timeOutWaitTMP100 = 0;

	while (XIicPs_BusIsBusy(&Iic)) {
		if ( timeOutWaitTMP100 > TIMEOUT_WAIT_TMP100){
			xil_printf("ERROR: TimeOut wait TMP100!!!!!!!!!!!!!!!!!!!!!!!!!!\r\n");
			XIicPs_Reset(&Iic);
			return XST_FAILURE;
		}
		timeOutWaitTMP100++;
	}
	return XST_SUCCESS;
}

float TMP100_ReadTemADPart(  ){

	uint8_t RecvBufferIIC[IIC_READ_SIZE];	/* Buffer for Receiving Data */

	if( TMP100_setADR( TMP100_TEMP_REG ) != XST_SUCCESS)
		return XST_FAILURE;

	if( TMP100_waitIIC() != XST_SUCCESS)
		return XST_FAILURE;

	XIicPs_MasterRecvPolled(&Iic, RecvBufferIIC,  2, IIC_ADPART_TMP100_SLAVE_ADDR);

	int dataI2C = ( RecvBufferIIC[0] << 4 ) | ( RecvBufferIIC[1] >> 4 );

	float tempADPart = ( (float) dataI2C )  / 22 ;
//	printf("INFO: The Current Temperature ADPart TMP100 = %2.2f\r\n",  tempADPart);

//	for( uint32_t i = 0 ; i < 100000000; i++);

	return tempADPart;
}

void TMP100_setCONG_REG_RESOLUTION_12BIT(  ){

	uint8_t dataTX[2];
		dataTX[0] = 1;
		dataTX[1] = 0xB0;

	if( TMP100_waitIIC() != XST_SUCCESS)	return;
    XIicPs_MasterSendPolled(&Iic, dataTX, 2, IIC_ADPART_TMP100_SLAVE_ADDR);
}
static int TMP100_setADR(uint8_t adr ){

	if( TMP100_waitIIC() != XST_SUCCESS)	return XST_FAILURE;
    XIicPs_MasterSendPolled(&Iic, &adr, 1, IIC_ADPART_TMP100_SLAVE_ADDR);

    return XST_SUCCESS;
}
