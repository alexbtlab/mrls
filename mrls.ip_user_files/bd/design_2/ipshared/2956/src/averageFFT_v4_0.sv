
`timescale 1 ns / 1 ps

	module averageFFT_v4_0 # 
	(
		// Parameters of Axi Slave Bus Interface S00_AXIS
		parameter integer C_S00_AXIS_TDATA_WIDTH	= 64,

		// Parameters of Axi Master Bus Interface M00_AXIS
		parameter integer C_M00_AXIS_TDATA_WIDTH	= 32,
		parameter integer C_M00_AXIS_START_COUNT	= 32,
		
		parameter integer C_S_AXI_ADDR_WIDTH	= 5,
		parameter integer C_S_AXI_DATA_WIDTH	= 32
	)
	(

        input wire  S_AXI_ACLK,
		// Global Reset Signal. This Signal is Active LOW
		input wire  S_AXI_ARESETN,
		// Write address (issued by master, acceped by Slave)
		input wire [C_S_AXI_ADDR_WIDTH-1 : 0] S_AXI_AWADDR,
		// Write channel Protection type. This signal indicates the
    		// privilege and security level of the transaction, and whether
    		// the transaction is a data access or an instruction access.
		input wire [2 : 0] S_AXI_AWPROT,
		// Write address valid. This signal indicates that the master signaling
    		// valid write address and control information.
		input wire  S_AXI_AWVALID,
		// Write address ready. This signal indicates that the slave is ready
    		// to accept an address and associated control signals.
		output wire  S_AXI_AWREADY,
		// Write data (issued by master, acceped by Slave) 
		input wire [C_S_AXI_DATA_WIDTH-1 : 0] S_AXI_WDATA,
		// Write strobes. This signal indicates which byte lanes hold
    		// valid data. There is one write strobe bit for each eight
    		// bits of the write data bus.    
		input wire [(C_S_AXI_DATA_WIDTH/8)-1 : 0] S_AXI_WSTRB,
		// Write valid. This signal indicates that valid write
    		// data and strobes are available.
		input wire  S_AXI_WVALID,
		// Write ready. This signal indicates that the slave
    		// can accept the write data.
		output wire  S_AXI_WREADY,
		// Write response. This signal indicates the status
    		// of the write transaction.
		output wire [1 : 0] S_AXI_BRESP,
		// Write response valid. This signal indicates that the channel
    		// is signaling a valid write response.
		output wire  S_AXI_BVALID,
		// Response ready. This signal indicates that the master
    		// can accept a write response.
		input wire  S_AXI_BREADY,
		// Read address (issued by master, acceped by Slave)
		input wire [C_S_AXI_ADDR_WIDTH-1 : 0] S_AXI_ARADDR,
		// Protection type. This signal indicates the privilege
    		// and security level of the transaction, and whether the
    		// transaction is a data access or an instruction access.
		input wire [2 : 0] S_AXI_ARPROT,
		// Read address valid. This signal indicates that the channel
    		// is signaling valid read address and control information.
		input wire  S_AXI_ARVALID,
		// Read address ready. This signal indicates that the slave is
    		// ready to accept an address and associated control signals.
		output wire  S_AXI_ARREADY,
		// Read data (issued by slave)
		output wire [C_S_AXI_DATA_WIDTH-1 : 0] S_AXI_RDATA,
		// Read response. This signal indicates the status of the
    		// read transfer.
		output wire [1 : 0] S_AXI_RRESP,
		// Read valid. This signal indicates that the channel is
    		// signaling the required read data.
		output wire  S_AXI_RVALID,
		// Read ready. This signal indicates that the master can
    		// accept the read data and response information.
		input wire  S_AXI_RREADY,
		
		output wire  s00_axis_tready,
		input wire [C_S00_AXIS_TDATA_WIDTH-1 : 0] s00_axis_tdata,
		input wire [(C_S00_AXIS_TDATA_WIDTH/8)-1 : 0] s00_axis_tstrb,
		input wire  s00_axis_tlast,
		input wire  s00_axis_tvalid,
		output wire  m00_axis_tvalid,
		output wire [C_M00_AXIS_TDATA_WIDTH-1 : 0] m00_axis_tdata,
		output wire [(C_M00_AXIS_TDATA_WIDTH/8)-1 : 0] m00_axis_tstrb,
		output wire  m00_axis_tlast,
		input wire  m00_axis_tready,
		
		input wire  allowed_clk,
        input wire  azimuth_0,		
		input  wire  m00_axis_aclk,
		input  wire  clk_10MHz,
		input  wire  m00_axis_aresetn
//		output wire [15:0] azimuth_out			
	);
	
//	wire S_AXI_ACLK;
	
    reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg0;
    reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg1;
    reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg2;
    reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg3;
    
    averageFFT_v4_0_S00_AXI averageFFT_v4_0_S00_AXI (.*);
	
//localparam averag_val = 8;     
//localparam divide_val = 3;     
wire[4:0] averag_val = slv_reg0;     
wire[3:0] divide_val = slv_reg1;
//wire[4:0] averag_val = 16;     
//wire[4:0] divide_val = 4;

localparam frame_size = 8192;     // ?????????? ?????? ?? ??????

wire [29:0] data_abs_1;     
wire [29:0] data_abs_2;     
//reg [4:0] azimut_r;
reg [4:0] fft_cnt;
reg [15:0] azimuth;
//reg [15:0] azimut8_r;
reg [63:0] RAM [0:8191];
reg [15:0] adr;
reg [15:0] adrRD;
reg [31:0] m00_axis_tdata_r;
reg [31:0] cnt100;
//reg [15:0] cnt_low_allowed_clk;
reg [15:0] cnt_high_allowed_clk;

assign m00_axis_tdata = m00_axis_tdata_r;
assign m00_axis_tstrb = 4'hF;
assign m00_axis_tlast  = ( fft_cnt == averag_val  ) ? s00_axis_tlast  : 0;
assign m00_axis_tvalid = ( fft_cnt == averag_val  ) ? s00_axis_tvalid : 0;
assign s00_axis_tready = m00_axis_tready;
assign data_abs_1[29:0] = (s00_axis_tdata[29] == 1) ? -s00_axis_tdata[29:0]  : s00_axis_tdata[29:0];
assign data_abs_2[29:0] = (s00_axis_tdata[61] == 1) ? -s00_axis_tdata[61:32] : s00_axis_tdata[61:32];
//assign azimuth_out = azimuth;	

always @ (posedge m00_axis_aclk) begin 
		        
     if(allowed_clk)                                     cnt100 <= cnt100 + 1;
     else                                                cnt100 <= 0;
	
     if(m00_axis_aresetn)  begin
                         if( s00_axis_tvalid) begin                    
                                   if(adr == frame_size - 1)      adr <= 0;
                                   else                           adr <= adr + 1;
                                   if(adrRD == 0)                 adrRD <= frame_size - 1;
                                   else                           adrRD <= adrRD - 1; 
                                   
                                   if(fft_cnt == 0) begin    // ???? ??????? ????? ?? ????????? ?????? ? ?????? 
//                                       if(adr == frame_size - 1)      adr <= 0;
//                                       else                           adr <= adr + 1;
                                       RAM[adr] <= data_abs_1 + data_abs_2;
                                       m00_axis_tdata_r <= 0;
                                   end
                                   else begin  
                                       
                                       if( fft_cnt == averag_val ) begin
//                                            if(adrRD == 0)       adrRD <= frame_size - 1;
//                                            else                 adrRD <= adrRD - 1;    
                                            
                                            if ( adrRD == frame_size - 1 )     m00_axis_tdata_r <= ( ( azimuth  << 16) ) | (2048 ) ;   // ???? ?????????????? averag_val ???????? ?? ?????????? ?????? ?? ?????? ? ????????(???????) ?? 2,4,8 ? ??
                                            else                               m00_axis_tdata_r <= (RAM[adrRD] >> divide_val); 

                                       end
                                       else begin
//                                           adrRD <= frame_size - 1;
//                                           if(adr == frame_size - 1)      adr <= 0;
//                                           else                           adr <= adr + 1;    
                                           RAM[adr] <= RAM[adr] + data_abs_1 + data_abs_2; // ???? ?? ??????? ????? ?? ????????? ???????? ?????? ? ?????????? (???????????? ?? ?????????? ??????) ???????
                                           m00_axis_tdata_r <= 0;
                                       end
                                   end
                                   
                        end
                        else begin  
                            adrRD <= frame_size - 1;
                            adr <= 0;                         
                        end
	end
	else begin
	   adrRD <= frame_size - 1;
	   adr <= 0;
	   m00_axis_tdata_r = 0;
	end
	
end

reg reset_azimuth;

always @ (posedge clk_10MHz) begin
    
    if ( m00_axis_aresetn ) begin
//                if ( !allowed_clk )                 cnt_low_allowed_clk  <= cnt_low_allowed_clk + 1;
//                else                                cnt_low_allowed_clk  <= 0;        
                if ( allowed_clk )                  cnt_high_allowed_clk <= cnt_high_allowed_clk + 1;
                else                                cnt_high_allowed_clk <= 0;
       
//                if( cnt_low_allowed_clk == 10 )     begin
//                    azimut_r <= azimut_r + 1;
//                    if ( azimut_r == averag_val ) begin
////                        azimut8_r <= azimut8_r + 1;
//                        azimut_r  <= 0;
//                    end
//                end 
                if( cnt_high_allowed_clk == 1000 )     begin
                    fft_cnt <= fft_cnt + 1;
                    if ( fft_cnt == averag_val ) begin
                        azimuth <= azimuth + 1;
                        fft_cnt  <= 0;
                    end
                end
                if ( azimuth_0 ) begin
//                    cnt_low_allowed_clk  <= 0;
                    cnt_high_allowed_clk <= 0;
//                    azimut_r             <= 0;
                    fft_cnt         <= 5'h1F;
                    azimuth        <= 0;
//                    azimut8_r            <= 0;
                end
    end
    else begin
//        cnt_low_allowed_clk     <= 0;
        cnt_high_allowed_clk    <= 0;
//        azimut_r                <= 0;
        fft_cnt            <= 5'h1F;
        azimuth           <= 0;
//        azimut8_r               <= 0;
        reset_azimuth           <= 0;
    end  
end
endmodule

