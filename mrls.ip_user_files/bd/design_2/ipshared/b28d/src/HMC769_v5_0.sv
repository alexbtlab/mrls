
interface spi_pll;
    logic sen, sck, mosi, cen, ld_sdo;      // Indicates if slave is ready to accept data
    modport master  (output sen, output sck, output mosi, output cen, input ld_sdo);
endinterface

module HMC769_v5_0 #
	(  
		// Parameters of Axi Slave Bus Interface S00_AXI
		parameter integer C_S00_AXI_DATA_WIDTH	= 32,
		parameter integer C_S00_AXI_ADDR_WIDTH	= 6
	)
	(	input wire  clk_10MHz,
	    input wire  clk_SPI_PLL,
		input wire  clkDCO_10MHz,
		input wire  s00_axi_aclk,
		input wire  s00_axi_aresetn,
		input wire [C_S00_AXI_ADDR_WIDTH-1 : 0] s00_axi_awaddr,
		input wire [2 : 0] s00_axi_awprot,
		input wire  s00_axi_awvalid,
		output wire  s00_axi_awready,
		input wire [C_S00_AXI_DATA_WIDTH-1 : 0] s00_axi_wdata,
		input wire [(C_S00_AXI_DATA_WIDTH/8)-1 : 0] s00_axi_wstrb,
		input wire  s00_axi_wvalid,
		output wire  s00_axi_wready,
		output wire [1 : 0] s00_axi_bresp,
		output wire  s00_axi_bvalid,
		input wire  s00_axi_bready,
		input wire [C_S00_AXI_ADDR_WIDTH-1 : 0] s00_axi_araddr,
		input wire [2 : 0] s00_axi_arprot,
		input wire  s00_axi_arvalid,
		output wire  s00_axi_arready,
		output wire [C_S00_AXI_DATA_WIDTH-1 : 0] s00_axi_rdata,
		output wire [1 : 0] s00_axi_rresp,
		output wire  s00_axi_rvalid,
		input wire  s00_axi_rready,
		
		input wire  azimut_0,
		spi_pll.master SPI_PLL,
		output wire  pll_trig,
		output wire [5:0] ATTEN,
		output wire PLL_POW_EN,
		output wire LOW_AMP_EN,
		output wire PAMP_EN,
        output wire start_adc_count,
        input wire PAMP_ALM_FPGA
	);

	
//	     wire [15:0] shift_front;
	
	     wire [32-1:0]	slv_reg0;
         wire [32-1:0]	slv_reg1;
         wire [32-1:0]	slv_reg2;
         wire [32-1:0]	slv_reg3;
         wire [32-1:0]	slv_reg4;
         wire [32-1:0]	slv_reg5;
         wire [32-1:0]	slv_reg6;
         wire [32-1:0]	slv_reg7;
        
         wire [32-1:0]	ip2mb_reg0;
         wire [32-1:0]	ip2mb_reg1;
         wire [32-1:0]	ip2mb_reg2;
         wire [32-1:0]	ip2mb_reg3;
         wire [32-1:0]	ip2mb_reg4;
         wire [32-1:0]	ip2mb_reg5;
         wire [32-1:0]	ip2mb_reg6;
         wire [32-1:0]	ip2mb_reg7;

top_PLL_control top_PLL_control_i(
    .clk_100MHz (s00_axi_aclk),
    .clk_10MHz(clk_10MHz),  
    .clk_SPI_PLL(clk_SPI_PLL),
    .clkDCO_10MHz(clkDCO_10MHz),
    .reset      (s00_axi_aresetn),
//    .shift_front(shift_front),
    .pll_sen    (SPI_PLL.sen),
    .pll_sck    (SPI_PLL.sck),
    .pll_mosi   (SPI_PLL.mosi),
    .pll_ld_sdo (SPI_PLL.ld_sdo),
    .pll_cen    (SPI_PLL.cen),
    .pll_trig   (pll_trig),
    .ATTEN      (ATTEN),
    .PLL_POW_EN     (PLL_POW_EN),
    .LOW_AMP_EN     (LOW_AMP_EN),
    .PAMP_EN        (PAMP_EN),
    .start_adc_count(start_adc_count),
    .slv_reg0(slv_reg0),
    .slv_reg1(slv_reg1),
    .slv_reg2(slv_reg2),
    .slv_reg3(slv_reg3),
    .slv_reg4(slv_reg4),
    .slv_reg5(slv_reg5),
    .slv_reg6(slv_reg6),
    .slv_reg7(slv_reg7),
    .ip2mb_reg0(ip2mb_reg0),
    .ip2mb_reg1(ip2mb_reg1),
    .ip2mb_reg2(ip2mb_reg2),
    .ip2mb_reg3(ip2mb_reg3),
    .ip2mb_reg4(ip2mb_reg4),
    .ip2mb_reg5(ip2mb_reg5),
    .ip2mb_reg6(ip2mb_reg6),
    .ip2mb_reg7(ip2mb_reg7),
    .azimut_0(azimut_0),
    .PAMP_ALM_FPGA(PAMP_ALM_FPGA)
);	
// Instantiation of Axi Bus Interface S00_AXI
	HMC769_v5_0_S00_AXI # ( 
		.C_S_AXI_DATA_WIDTH(C_S00_AXI_DATA_WIDTH),
		.C_S_AXI_ADDR_WIDTH(C_S00_AXI_ADDR_WIDTH)
	) HMC769_v5_0_S00_AXI_inst (
		.S_AXI_ACLK(s00_axi_aclk),
		.S_AXI_ARESETN(s00_axi_aresetn),
		.S_AXI_AWADDR(s00_axi_awaddr),
		.S_AXI_AWPROT(s00_axi_awprot),
		.S_AXI_AWVALID(s00_axi_awvalid),
		.S_AXI_AWREADY(s00_axi_awready),
		.S_AXI_WDATA(s00_axi_wdata),
		.S_AXI_WSTRB(s00_axi_wstrb),
		.S_AXI_WVALID(s00_axi_wvalid),
		.S_AXI_WREADY(s00_axi_wready),
		.S_AXI_BRESP(s00_axi_bresp),
		.S_AXI_BVALID(s00_axi_bvalid),
		.S_AXI_BREADY(s00_axi_bready),
		.S_AXI_ARADDR(s00_axi_araddr),
		.S_AXI_ARPROT(s00_axi_arprot),
		.S_AXI_ARVALID(s00_axi_arvalid),
		.S_AXI_ARREADY(s00_axi_arready),
		.S_AXI_RDATA(s00_axi_rdata),
		.S_AXI_RRESP(s00_axi_rresp),
		.S_AXI_RVALID(s00_axi_rvalid),
		.S_AXI_RREADY(s00_axi_rready),
		
	    .slv_reg0(slv_reg0),
		.slv_reg1(slv_reg1),
		.slv_reg2(slv_reg2),
		.slv_reg3(slv_reg3),
		.slv_reg4(slv_reg4),
		.slv_reg5(slv_reg5),
		.slv_reg6(slv_reg6),
		.slv_reg7(slv_reg7),
		.ip2mb_reg0(ip2mb_reg0),
		.ip2mb_reg1(ip2mb_reg1),
		.ip2mb_reg2(ip2mb_reg2),
		.ip2mb_reg3(ip2mb_reg3),
		.ip2mb_reg4(ip2mb_reg4),  
	    .ip2mb_reg5(ip2mb_reg5),
		.ip2mb_reg6(ip2mb_reg6),
		.ip2mb_reg7(ip2mb_reg7)   
	       
	);
endmodule