interface dco_or;
    logic dcoa, dcob, ora, orb;      // Indicates if slave is ready to accept data
    modport slave  (input dcoa, dcob, ora, orb);
endinterface 

module syncADCData_v1_0 (    
                            dco_or.slave DCO_OR, 
                            output [15:0] DATA_CH1_out,
                            output [15:0] DATA_CH2_out,
                            
                            input   [15:0] DATA_CH1_in,
                            input   [15:0] DATA_CH2_in,
//                            input   clkDCO_10MHz,
                            input   clk_10MHz,
                            input   clk_100MHz,
                            input   reset,
                            
                            output  DCOA
//                            output  DCOB
                                                   
                       );
                       
assign DCOA = DCO_OR.dcoa;
//assign DCOB = DCO_OR.dcob;

localparam valSet = 2;        
reg   [3:0]  count;
reg   [15:0] DATA_CH1_tmp;
reg   [15:0] DATA_CH2_tmp;
reg   [15:0] DATA_CH1_sync;
reg   [15:0] DATA_CH2_sync;

assign DATA_CH1_out = DATA_CH1_sync;
assign DATA_CH2_out = DATA_CH2_sync;

//assign DATA_CH1_out = DATA_CH1_in;
//assign DATA_CH2_out = DATA_CH2_in;

/*-------------------------------------------------------------------*/
always @ (posedge clk_100MHz ) begin
    if (~reset)    begin
        count <= 0;
        DATA_CH1_tmp <= 0;
        DATA_CH2_tmp <= 0;
    end 
    else begin
         if ( DCO_OR.dcoa )    begin
                count <= count + 1'b1;
                if ( count == valSet) begin
                    DATA_CH1_tmp <= DATA_CH1_in;
                    DATA_CH2_tmp <= DATA_CH2_in;
                end
                else begin
                    DATA_CH1_tmp <= DATA_CH1_tmp;
                    DATA_CH2_tmp <= DATA_CH2_tmp;
                end 
         end
         else begin
            count <= 0;
         end
    end
end 
/*-------------------------------------------------------------------*/
always @ (posedge clk_10MHz ) begin
    if (~reset)    begin
        DATA_CH1_sync <= 0;
        DATA_CH2_sync <= 0;
    end 
    else begin
       DATA_CH1_sync <= DATA_CH1_tmp;
       DATA_CH2_sync <= DATA_CH2_tmp;
    end
end  
/*-------------------------------------------------------------------*/
endmodule