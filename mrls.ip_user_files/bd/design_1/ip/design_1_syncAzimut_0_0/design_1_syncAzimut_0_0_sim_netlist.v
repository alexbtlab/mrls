// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
// Date        : Mon Feb 28 18:13:07 2022
// Host        : alexbtlab-System-Product-Name running 64-bit Ubuntu 20.04.3 LTS
// Command     : write_verilog -force -mode funcsim -rename_top design_1_syncAzimut_0_0 -prefix
//               design_1_syncAzimut_0_0_ design_1_syncAzimut_0_0_sim_netlist.v
// Design      : design_1_syncAzimut_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_syncAzimut_0_0,syncAzimut_v1_0,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "syncAzimut_v1_0,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module design_1_syncAzimut_0_0
   (sysclk_100,
    reset,
    async_azimut,
    sync_azimut);
  input sysclk_100;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 reset RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME reset, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input reset;
  input async_azimut;
  output sync_azimut;

  wire async_azimut;
  wire reset;
  wire sync_azimut;
  wire sysclk_100;

  design_1_syncAzimut_0_0_syncAzimut_v1_0 inst
       (.async_azimut(async_azimut),
        .reset(reset),
        .sync_azimut(sync_azimut),
        .sysclk_100(sysclk_100));
endmodule

module design_1_syncAzimut_0_0_syncAzimut_v1_0
   (sync_azimut,
    async_azimut,
    reset,
    sysclk_100);
  output sync_azimut;
  input async_azimut;
  input reset;
  input sysclk_100;

  wire async_azimut;
  wire \cnt_notrecieved_azimut[7]_i_1_n_0 ;
  wire \cnt_notrecieved_azimut[7]_i_3_n_0 ;
  wire [7:0]cnt_notrecieved_azimut_reg;
  wire \cnt_recieved_azimut[7]_i_1_n_0 ;
  wire \cnt_recieved_azimut[7]_i_3_n_0 ;
  wire [7:0]cnt_recieved_azimut_reg;
  wire [7:0]p_0_in;
  wire [7:0]p_0_in__0;
  wire reset;
  wire sync_azimut;
  wire sync_azimut_r_i_1_n_0;
  wire sync_azimut_r_i_2_n_0;
  wire sync_azimut_r_i_3_n_0;
  wire sync_azimut_r_i_4_n_0;
  wire sync_azimut_r_i_5_n_0;
  wire sysclk_100;

  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \cnt_notrecieved_azimut[0]_i_1 
       (.I0(cnt_notrecieved_azimut_reg[0]),
        .O(p_0_in[0]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \cnt_notrecieved_azimut[1]_i_1 
       (.I0(cnt_notrecieved_azimut_reg[0]),
        .I1(cnt_notrecieved_azimut_reg[1]),
        .O(p_0_in[1]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \cnt_notrecieved_azimut[2]_i_1 
       (.I0(cnt_notrecieved_azimut_reg[1]),
        .I1(cnt_notrecieved_azimut_reg[0]),
        .I2(cnt_notrecieved_azimut_reg[2]),
        .O(p_0_in[2]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \cnt_notrecieved_azimut[3]_i_1 
       (.I0(cnt_notrecieved_azimut_reg[2]),
        .I1(cnt_notrecieved_azimut_reg[0]),
        .I2(cnt_notrecieved_azimut_reg[1]),
        .I3(cnt_notrecieved_azimut_reg[3]),
        .O(p_0_in[3]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \cnt_notrecieved_azimut[4]_i_1 
       (.I0(cnt_notrecieved_azimut_reg[3]),
        .I1(cnt_notrecieved_azimut_reg[1]),
        .I2(cnt_notrecieved_azimut_reg[0]),
        .I3(cnt_notrecieved_azimut_reg[2]),
        .I4(cnt_notrecieved_azimut_reg[4]),
        .O(p_0_in[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \cnt_notrecieved_azimut[5]_i_1 
       (.I0(cnt_notrecieved_azimut_reg[4]),
        .I1(cnt_notrecieved_azimut_reg[2]),
        .I2(cnt_notrecieved_azimut_reg[0]),
        .I3(cnt_notrecieved_azimut_reg[1]),
        .I4(cnt_notrecieved_azimut_reg[3]),
        .I5(cnt_notrecieved_azimut_reg[5]),
        .O(p_0_in[5]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \cnt_notrecieved_azimut[6]_i_1 
       (.I0(\cnt_notrecieved_azimut[7]_i_3_n_0 ),
        .I1(cnt_notrecieved_azimut_reg[6]),
        .O(p_0_in[6]));
  LUT2 #(
    .INIT(4'hB)) 
    \cnt_notrecieved_azimut[7]_i_1 
       (.I0(async_azimut),
        .I1(reset),
        .O(\cnt_notrecieved_azimut[7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'hD2)) 
    \cnt_notrecieved_azimut[7]_i_2 
       (.I0(cnt_notrecieved_azimut_reg[6]),
        .I1(\cnt_notrecieved_azimut[7]_i_3_n_0 ),
        .I2(cnt_notrecieved_azimut_reg[7]),
        .O(p_0_in[7]));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \cnt_notrecieved_azimut[7]_i_3 
       (.I0(cnt_notrecieved_azimut_reg[4]),
        .I1(cnt_notrecieved_azimut_reg[2]),
        .I2(cnt_notrecieved_azimut_reg[0]),
        .I3(cnt_notrecieved_azimut_reg[1]),
        .I4(cnt_notrecieved_azimut_reg[3]),
        .I5(cnt_notrecieved_azimut_reg[5]),
        .O(\cnt_notrecieved_azimut[7]_i_3_n_0 ));
  FDRE \cnt_notrecieved_azimut_reg[0] 
       (.C(sysclk_100),
        .CE(1'b1),
        .D(p_0_in[0]),
        .Q(cnt_notrecieved_azimut_reg[0]),
        .R(\cnt_notrecieved_azimut[7]_i_1_n_0 ));
  FDRE \cnt_notrecieved_azimut_reg[1] 
       (.C(sysclk_100),
        .CE(1'b1),
        .D(p_0_in[1]),
        .Q(cnt_notrecieved_azimut_reg[1]),
        .R(\cnt_notrecieved_azimut[7]_i_1_n_0 ));
  FDRE \cnt_notrecieved_azimut_reg[2] 
       (.C(sysclk_100),
        .CE(1'b1),
        .D(p_0_in[2]),
        .Q(cnt_notrecieved_azimut_reg[2]),
        .R(\cnt_notrecieved_azimut[7]_i_1_n_0 ));
  FDRE \cnt_notrecieved_azimut_reg[3] 
       (.C(sysclk_100),
        .CE(1'b1),
        .D(p_0_in[3]),
        .Q(cnt_notrecieved_azimut_reg[3]),
        .R(\cnt_notrecieved_azimut[7]_i_1_n_0 ));
  FDRE \cnt_notrecieved_azimut_reg[4] 
       (.C(sysclk_100),
        .CE(1'b1),
        .D(p_0_in[4]),
        .Q(cnt_notrecieved_azimut_reg[4]),
        .R(\cnt_notrecieved_azimut[7]_i_1_n_0 ));
  FDRE \cnt_notrecieved_azimut_reg[5] 
       (.C(sysclk_100),
        .CE(1'b1),
        .D(p_0_in[5]),
        .Q(cnt_notrecieved_azimut_reg[5]),
        .R(\cnt_notrecieved_azimut[7]_i_1_n_0 ));
  FDRE \cnt_notrecieved_azimut_reg[6] 
       (.C(sysclk_100),
        .CE(1'b1),
        .D(p_0_in[6]),
        .Q(cnt_notrecieved_azimut_reg[6]),
        .R(\cnt_notrecieved_azimut[7]_i_1_n_0 ));
  FDRE \cnt_notrecieved_azimut_reg[7] 
       (.C(sysclk_100),
        .CE(1'b1),
        .D(p_0_in[7]),
        .Q(cnt_notrecieved_azimut_reg[7]),
        .R(\cnt_notrecieved_azimut[7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \cnt_recieved_azimut[0]_i_1 
       (.I0(cnt_recieved_azimut_reg[0]),
        .O(p_0_in__0[0]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \cnt_recieved_azimut[1]_i_1 
       (.I0(cnt_recieved_azimut_reg[0]),
        .I1(cnt_recieved_azimut_reg[1]),
        .O(p_0_in__0[1]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \cnt_recieved_azimut[2]_i_1 
       (.I0(cnt_recieved_azimut_reg[1]),
        .I1(cnt_recieved_azimut_reg[0]),
        .I2(cnt_recieved_azimut_reg[2]),
        .O(p_0_in__0[2]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \cnt_recieved_azimut[3]_i_1 
       (.I0(cnt_recieved_azimut_reg[2]),
        .I1(cnt_recieved_azimut_reg[0]),
        .I2(cnt_recieved_azimut_reg[1]),
        .I3(cnt_recieved_azimut_reg[3]),
        .O(p_0_in__0[3]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \cnt_recieved_azimut[4]_i_1 
       (.I0(cnt_recieved_azimut_reg[3]),
        .I1(cnt_recieved_azimut_reg[1]),
        .I2(cnt_recieved_azimut_reg[0]),
        .I3(cnt_recieved_azimut_reg[2]),
        .I4(cnt_recieved_azimut_reg[4]),
        .O(p_0_in__0[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \cnt_recieved_azimut[5]_i_1 
       (.I0(cnt_recieved_azimut_reg[4]),
        .I1(cnt_recieved_azimut_reg[2]),
        .I2(cnt_recieved_azimut_reg[0]),
        .I3(cnt_recieved_azimut_reg[1]),
        .I4(cnt_recieved_azimut_reg[3]),
        .I5(cnt_recieved_azimut_reg[5]),
        .O(p_0_in__0[5]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \cnt_recieved_azimut[6]_i_1 
       (.I0(\cnt_recieved_azimut[7]_i_3_n_0 ),
        .I1(cnt_recieved_azimut_reg[6]),
        .O(p_0_in__0[6]));
  LUT2 #(
    .INIT(4'h7)) 
    \cnt_recieved_azimut[7]_i_1 
       (.I0(reset),
        .I1(async_azimut),
        .O(\cnt_recieved_azimut[7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'hD2)) 
    \cnt_recieved_azimut[7]_i_2 
       (.I0(cnt_recieved_azimut_reg[6]),
        .I1(\cnt_recieved_azimut[7]_i_3_n_0 ),
        .I2(cnt_recieved_azimut_reg[7]),
        .O(p_0_in__0[7]));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \cnt_recieved_azimut[7]_i_3 
       (.I0(cnt_recieved_azimut_reg[4]),
        .I1(cnt_recieved_azimut_reg[2]),
        .I2(cnt_recieved_azimut_reg[0]),
        .I3(cnt_recieved_azimut_reg[1]),
        .I4(cnt_recieved_azimut_reg[3]),
        .I5(cnt_recieved_azimut_reg[5]),
        .O(\cnt_recieved_azimut[7]_i_3_n_0 ));
  FDRE \cnt_recieved_azimut_reg[0] 
       (.C(sysclk_100),
        .CE(1'b1),
        .D(p_0_in__0[0]),
        .Q(cnt_recieved_azimut_reg[0]),
        .R(\cnt_recieved_azimut[7]_i_1_n_0 ));
  FDRE \cnt_recieved_azimut_reg[1] 
       (.C(sysclk_100),
        .CE(1'b1),
        .D(p_0_in__0[1]),
        .Q(cnt_recieved_azimut_reg[1]),
        .R(\cnt_recieved_azimut[7]_i_1_n_0 ));
  FDRE \cnt_recieved_azimut_reg[2] 
       (.C(sysclk_100),
        .CE(1'b1),
        .D(p_0_in__0[2]),
        .Q(cnt_recieved_azimut_reg[2]),
        .R(\cnt_recieved_azimut[7]_i_1_n_0 ));
  FDRE \cnt_recieved_azimut_reg[3] 
       (.C(sysclk_100),
        .CE(1'b1),
        .D(p_0_in__0[3]),
        .Q(cnt_recieved_azimut_reg[3]),
        .R(\cnt_recieved_azimut[7]_i_1_n_0 ));
  FDRE \cnt_recieved_azimut_reg[4] 
       (.C(sysclk_100),
        .CE(1'b1),
        .D(p_0_in__0[4]),
        .Q(cnt_recieved_azimut_reg[4]),
        .R(\cnt_recieved_azimut[7]_i_1_n_0 ));
  FDRE \cnt_recieved_azimut_reg[5] 
       (.C(sysclk_100),
        .CE(1'b1),
        .D(p_0_in__0[5]),
        .Q(cnt_recieved_azimut_reg[5]),
        .R(\cnt_recieved_azimut[7]_i_1_n_0 ));
  FDRE \cnt_recieved_azimut_reg[6] 
       (.C(sysclk_100),
        .CE(1'b1),
        .D(p_0_in__0[6]),
        .Q(cnt_recieved_azimut_reg[6]),
        .R(\cnt_recieved_azimut[7]_i_1_n_0 ));
  FDRE \cnt_recieved_azimut_reg[7] 
       (.C(sysclk_100),
        .CE(1'b1),
        .D(p_0_in__0[7]),
        .Q(cnt_recieved_azimut_reg[7]),
        .R(\cnt_recieved_azimut[7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8FFF888888888888)) 
    sync_azimut_r_i_1
       (.I0(sync_azimut_r_i_2_n_0),
        .I1(sync_azimut_r_i_3_n_0),
        .I2(sync_azimut_r_i_4_n_0),
        .I3(sync_azimut_r_i_5_n_0),
        .I4(reset),
        .I5(sync_azimut),
        .O(sync_azimut_r_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    sync_azimut_r_i_2
       (.I0(cnt_recieved_azimut_reg[5]),
        .I1(cnt_recieved_azimut_reg[6]),
        .I2(cnt_recieved_azimut_reg[3]),
        .I3(cnt_recieved_azimut_reg[4]),
        .I4(cnt_recieved_azimut_reg[7]),
        .I5(async_azimut),
        .O(sync_azimut_r_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'h0400)) 
    sync_azimut_r_i_3
       (.I0(cnt_recieved_azimut_reg[0]),
        .I1(reset),
        .I2(cnt_recieved_azimut_reg[1]),
        .I3(cnt_recieved_azimut_reg[2]),
        .O(sync_azimut_r_i_3_n_0));
  LUT6 #(
    .INIT(64'h0000002000000000)) 
    sync_azimut_r_i_4
       (.I0(cnt_notrecieved_azimut_reg[5]),
        .I1(cnt_notrecieved_azimut_reg[4]),
        .I2(cnt_notrecieved_azimut_reg[2]),
        .I3(cnt_notrecieved_azimut_reg[3]),
        .I4(cnt_notrecieved_azimut_reg[7]),
        .I5(cnt_notrecieved_azimut_reg[6]),
        .O(sync_azimut_r_i_4_n_0));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'h01)) 
    sync_azimut_r_i_5
       (.I0(cnt_notrecieved_azimut_reg[1]),
        .I1(cnt_notrecieved_azimut_reg[0]),
        .I2(async_azimut),
        .O(sync_azimut_r_i_5_n_0));
  FDRE sync_azimut_r_reg
       (.C(sysclk_100),
        .CE(1'b1),
        .D(sync_azimut_r_i_1_n_0),
        .Q(sync_azimut),
        .R(1'b0));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
