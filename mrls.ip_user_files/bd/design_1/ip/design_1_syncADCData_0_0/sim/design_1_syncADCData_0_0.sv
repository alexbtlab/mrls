// (c) Copyright 1995-2022 Xilinx, Inc. All rights reserved.
// 
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and
// international copyright and other intellectual property
// laws.
// 
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
// 
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
// 
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES.
// 
// DO NOT MODIFY THIS FILE.


// IP VLNV: user.org:user:syncADCData:1.0
// IP Revision: 18

`timescale 1ns/1ps

(* DowngradeIPIdentifiedWarnings = "yes" *)
module design_1_syncADCData_0_0 (
  dco_or_dcoa,
  dco_or_dcob,
  dco_or_ora,
  dco_or_orb,
  reset,
  DCOA,
  DATA_CH1_out,
  DATA_CH2_out,
  clk_10MHz,
  clk_100MHz,
  DATA_CH1_in,
  DATA_CH2_in
);

(* X_INTERFACE_INFO = "user.org:interface:dco_or:1.0 DCO_OR dcoa" *)
input wire dco_or_dcoa;
(* X_INTERFACE_INFO = "user.org:interface:dco_or:1.0 DCO_OR dcob" *)
input wire dco_or_dcob;
(* X_INTERFACE_INFO = "user.org:interface:dco_or:1.0 DCO_OR ora" *)
input wire dco_or_ora;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME DCO_OR, SV_INTERFACE true" *)
(* X_INTERFACE_INFO = "user.org:interface:dco_or:1.0 DCO_OR orb" *)
input wire dco_or_orb;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME reset, POLARITY ACTIVE_LOW, INSERT_VIP 0" *)
(* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 reset RST" *)
input wire reset;
output wire DCOA;
output wire [15 : 0] DATA_CH1_out;
output wire [15 : 0] DATA_CH2_out;
input wire clk_10MHz;
input wire clk_100MHz;
input wire [15 : 0] DATA_CH1_in;
input wire [15 : 0] DATA_CH2_in;

dco_or DCO_OR();
assign DCO_OR.dcoa = dco_or_dcoa;
assign DCO_OR.dcob = dco_or_dcob;
assign DCO_OR.ora = dco_or_ora;
assign DCO_OR.orb = dco_or_orb;

  syncADCData_v1_0 inst (
    .DCO_OR(DCO_OR),
    .reset(reset),
    .DCOA(DCOA),
    .DATA_CH1_out(DATA_CH1_out),
    .DATA_CH2_out(DATA_CH2_out),
    .clk_10MHz(clk_10MHz),
    .clk_100MHz(clk_100MHz),
    .DATA_CH1_in(DATA_CH1_in),
    .DATA_CH2_in(DATA_CH2_in)
  );
endmodule
