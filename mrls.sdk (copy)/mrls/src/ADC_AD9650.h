#ifndef __ADC_AD9650_H_
#define __ADC_AD9650_H_

	#include <stdio.h>
	#include "stdbool.h"
	#include "AD9650.h"

	#define AD9650_DELAY_DCO_REG  0x17

	void AD9650_DELAY_DCO(bool direction, uint32_t step);

#endif //  __ADC_AD9650_H_
