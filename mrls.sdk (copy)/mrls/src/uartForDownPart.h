#ifndef __UARTFORDOWNPART_H_
#define __UARTFORDOWNPART_H_

	#include <stdio.h>
	#include "xuartlite.h"
	#include "xuartps.h"
	#include "stdbool.h"
	#include "xscugic.h"
	#include "mrls_udp.h"
	#include "xparameters.h"
	#include "MRLS2D.h"

	#define COMMAND_TO_ADPART__RUNMOTOR			1
	#define COMMAND_TO_ADPART__STOPMOTOR		0
	#define COMMAND_TO_ADPART__SPEED_1_25_SEC	6
	#define COMMAND_TO_ADPART__SPEED_2_5_SEC	7
	#define COMMAND_TO_ADPART__SPEED_5_SEC		8

	#define UARTLITE_IRPT_INTR	  	XPAR_FABRIC_UARTLITE_0_VEC_ID
	#define TEST_UART_BUFFER_SIZE	100
	#define INTC_DEVICE_ID			XPAR_SCUGIC_SINGLE_DEVICE_ID
	#define TEST_BUFFER_SIZE_UART 	128
	#define UARTLITE_DEVICE_ID		XPAR_UARTLITE_0_DEVICE_ID

	#define INTC				XScuGic
	#define INTC_HANDLER		XScuGic_InterruptHandler
	#define UART_INT_IRQ_ID		XPAR_XUARTPS_1_INTR
	#define BUFF_UART(pos)	(char *)&buff_UART[cntByte + pos]
	#define FIND_STR(a, b, c)		!( (buff_UART[cntByte] == a) && (buff_UART[cntByte + 1] == b) && (buff_UART[cntByte + 2] == c))
	#define SIZE_BUF_FROM_JEKA 128
	#define WAIT_FOR_RECEIVED_GPS  ( !GPS_Received_Done && !GPS_Received_Err)
	#define TIME_OUT_SEC_WAIT_VALID_STATUS_NMEA 60*5
	#define NMEA_GNRMC_STATUS_VALID_POSITION 17

	typedef enum{
		SPEED_MOTOR_1_25_SEC,
		SPEED_MOTOR_2_5_SEC,
		SPEED_MOTOR_5_SEC,
	}t_MotorSpeed;

	void UBLOX_read_GPS();
	int UartPsIntrInit(INTC *IntcInstPtr, XUartPs *UartInstPtr,	u16 DeviceId, u16 UartIntrId);
	int ADPART_runMotor();
	int ADPART_stopMotor();
	u32  XUartPs_ReceiveBuffer(XUartPs *InstancePtr);
	int uartLiteInit();
	void uartPSHandler(void *CallBackRef, u32 Event, unsigned int EventData);
	void UBLOX_read_GPS();
	int ADPART_MotorSpeed( t_MotorSpeed speed);

	extern XScuGic INTCInst;
	extern bool GPS_Received_Done;
	extern bool GPS_Received_Err;
	extern volatile size_t g_shiftFront;

#endif //  __UARTFORDOWNPART_H_
