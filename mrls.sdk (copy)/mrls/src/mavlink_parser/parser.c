#include "mavlink_parser/mavlink_parser.h"


mavlink_message_t rxMessage;
mavlink_status_t rxStatus;

void parser(const char* buffer, size_t len)
{

	uint32_t cnt = 0;
    mavlink_message_t message;
    mavlink_status_t status;
    for (int pos = 0; pos < len; pos++) {
        uint8_t res = mavlink_frame_char_buffer(&rxMessage, &rxStatus, (uint8_t)buffer[pos], &message, &status);
        switch (res) {

			case MAVLINK_FRAMING_INCOMPLETE:
				cnt++;
				//xil_printf("frameIncomplete = %d\r\n", cnt);
			break;
				case MAVLINK_FRAMING_OK:
						handler(&message);
				break;
					case MAVLINK_FRAMING_BAD_CRC:
					break;
						case MAVLINK_FRAMING_BAD_SIGNATURE:
						break;
        }
    }
}
