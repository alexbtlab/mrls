#include "mavlink.h"
#include "xil_io.h"
#include "mrls_print.h"

#define CURRENT_SYSID 55

void parser(const char* buffer, size_t len);
extern void handler(mavlink_message_t* message);
extern void handler_heartbeat(mavlink_message_t* message);
extern void handler_param_get(mavlink_message_t* message);
extern void handler_param_set(mavlink_message_t* message);
extern void handler_cmd(mavlink_message_t* message);
       int handler_PARAM_REQUEST_LIST(mavlink_message_t* message);
       int handler_PARAM_SET(mavlink_message_t* message);
       int handler_COMMAND_INT(mavlink_message_t* message);
       int handler_PARAM_GET(mavlink_message_t* message);
       int handler_param_REQUEST_READ(mavlink_message_t* message);
