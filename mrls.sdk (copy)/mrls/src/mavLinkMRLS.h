#ifndef __MAVLINK_MRLS_H_
#define __MAVLINK_MRLS_H_

#include "mavlink.h"
#include "mrls_parameters.h"
#include "xadc.h"
#include "tmp100.h"
#include "MRLS2D.h"

#define TIME_TO_SEND_TELEMETRY 10000000

extern XSysMon SysMonInst;
extern bool GPS_Received_Err;
extern bool gpsDataTransmitToPC;
extern bool GPS_Received_Done;
extern double angelDegree;
extern volatile double lat;
extern volatile double lon;
extern bool neededPrintTelemetry;
extern bool compasDataTransmitToPC;

void mavLinkSendCompassAzimuthToPC();
void mavLinkSendGPSCoordinateToPC();
void printTelemetry();
void mavLinkSendTemperatureToPC();

#endif /* __DEFINITION_H_ */
