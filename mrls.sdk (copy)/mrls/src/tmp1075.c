#include "tmp1075.h"

u8 SendBufferIIC[TEST_BUFFER_SIZE];

static void TMP1075_setADR(int adr );

int i2c_init_TEMP_PAMP(){

	int Status;
	XIicPs_Config *Config;
//			int Index;

			Config = XIicPs_LookupConfig(IIC_TEMP_PAMP_DEVICE_ID);
			if (NULL == Config) {
				return XST_FAILURE;
			}

			Status = XIicPs_CfgInitialize(&Iic_Temp_PAMP, Config, Config->BaseAddress);
			if (Status != XST_SUCCESS) {
				return XST_FAILURE;
			}

			Status = XIicPs_SelfTest(&Iic_Temp_PAMP);
			if (Status != XST_SUCCESS) {
				return XST_FAILURE;
			}

			XIicPs_SetSClk(&Iic_Temp_PAMP, IIC_SCLK_RATE);

//			for (Index = 0; Index < TEST_BUFFER_SIZE; Index++) {
//				SendBuffer[Index] = (Index % TEST_BUFFER_SIZE);
//				RecvBuffer[Index] = 0;
//			}
			SendBufferIIC[0] = 0x6;

			TMP1075_setCONG_REG_RESOLUTION_12BIT( );

			return XST_SUCCESS;
}

float TMP1075_ReadTemPAMP(  ){

		uint8_t RecvBufferIIC[IIC_READ_SIZE];	/* Buffer for Receiving Data */

		TMP1075_setADR( TMP1075_TEMP_REG );

		XIicPs_MasterRecvPolled(&Iic_Temp_PAMP, RecvBufferIIC,  2, IIC_TEMPPAMP_TMP1075_SLAVE_ADDR);

		int dataI2C = ( RecvBufferIIC[0] << 4 ) | ( RecvBufferIIC[1] >> 4 );

		float tempADPart = ( (float) dataI2C )  / 22 ;
//		printf("INFO: The Current Temperature PAMP TMP1075 = %2.2f\r\n",  tempADPart);

//		for( uint32_t i = 0 ; i < 100000000; i++);

		return tempADPart;
	}
void TMP1075_setCONG_REG_RESOLUTION_12BIT(  ){


	uint8_t dataTX[2];
		dataTX[0] = 1;
		dataTX[1] = 0xB0;

    XIicPs_MasterSendPolled(&Iic_Temp_PAMP, dataTX, 2, IIC_TEMPPAMP_TMP1075_SLAVE_ADDR);
    while (XIicPs_BusIsBusy(&Iic_Temp_PAMP)) {}
}
static void TMP1075_setADR(int adr ){


		uint8_t dataTX[2];
			dataTX[0] = adr;


	    XIicPs_MasterSendPolled(&Iic_Temp_PAMP, dataTX, 1, IIC_TEMPPAMP_TMP1075_SLAVE_ADDR);
	    while (XIicPs_BusIsBusy(&Iic_Temp_PAMP)) {}
}
