#include "compass.h"

double angelDegree;
uint8_t regHMC5883 = 0x3;
uint8_t   *  pregHMC5883 = &regHMC5883;
XIicPs Iic;
XIicPs Iic_Temp_PAMP;
uint8_t RegSettingA = HMC5883l_Enable_A;
uint8_t RegSettingB = HMC5883l_Enable_B;
uint8_t RegSettingMR = HMC5883l_MR;
u8 SendBufferIIC[TEST_BUFFER_SIZE];	/* Buffer for Transmitting Data */
u8 RecvBufferIIC[TEST_BUFFER_SIZE];	/* Buffer for Receiving Data */

int i2c_init(){

	int Status;
	XIicPs_Config *Config;
//			int Index;

			Config = XIicPs_LookupConfig(IIC_DEVICE_ID);
			if (NULL == Config) {
				return XST_FAILURE;
			}

			Status = XIicPs_CfgInitialize(&Iic, Config, Config->BaseAddress);
			if (Status != XST_SUCCESS) {
				return XST_FAILURE;
			}

			Status = XIicPs_SelfTest(&Iic);
			if (Status != XST_SUCCESS) {
				return XST_FAILURE;
			}

			XIicPs_SetSClk(&Iic, IIC_SCLK_RATE);

//			for (Index = 0; Index < TEST_BUFFER_SIZE; Index++) {
//				SendBuffer[Index] = (Index % TEST_BUFFER_SIZE);
//				RecvBuffer[Index] = 0;
//			}
			SendBufferIIC[0] = 0x6;

			TMP100_setCONG_REG_RESOLUTION_12BIT( );
			compasInit();


			return XST_SUCCESS;
}





void HMC_setRegA(){

	uint8_t dataTX[2];
	dataTX[0] = HMC5883L_RA_CONFIG_A;
	dataTX[1] = RegSettingA;

	    XIicPs_MasterSendPolled(&Iic, dataTX, 2, IIC_COMPASS_SLAVE_ADDR);
	    while (XIicPs_BusIsBusy(&Iic)) {}
}
void HMC_setRegB(){

	uint8_t dataTX[2];
	dataTX[0] = HMC5883L_RA_CONFIG_B;
	dataTX[1] = RegSettingB;

		XIicPs_MasterSendPolled(&Iic, dataTX, 2, IIC_COMPASS_SLAVE_ADDR);
		while (XIicPs_BusIsBusy(&Iic)) {}
}
void HMC_setRegMR(){

	uint8_t dataTX[2];
		dataTX[0] = HMC5883L_RA_MODE;
		dataTX[1] = RegSettingMR;

    XIicPs_MasterSendPolled(&Iic, dataTX, 2, IIC_COMPASS_SLAVE_ADDR);
    while (XIicPs_BusIsBusy(&Iic)) {}
}
void UBLOX_readCompass(){

	double angelRadian;

	compasDataTransmitToPC = false;

	regHMC5883 = 3;
		while (XIicPs_BusIsBusy(&Iic)) {  }
		XIicPs_MasterSendPolled(&Iic, pregHMC5883, 1, IIC_COMPASS_SLAVE_ADDR);

		while (XIicPs_BusIsBusy(&Iic)) {  }
		XIicPs_MasterRecvPolled(&Iic, RecvBufferIIC,  6, IIC_COMPASS_SLAVE_ADDR);


		int16_t X = ( RecvBufferIIC[0] << 8 ) | ( RecvBufferIIC[1] );
		int16_t Y = ( RecvBufferIIC[4] << 8 ) | ( RecvBufferIIC[5] );
		int16_t Z = ( RecvBufferIIC[2] << 8 ) | ( RecvBufferIIC[3] );

		angelRadian = atan2(X, Y);
	    angelDegree = ( angelRadian * 180 ) / 3.14;

	    angelDegree += 180.0;

	    printf("INFO:  angelDegree:%f   \r\n",  angelDegree);
		xil_printf("INFO: X:%4d   Y:%4d    Z:%4d  \r\n", X, Y, Z);

		for(uint32_t i = 0 ; i < 50000000; i++){}
}
void compasInit(){

	HMC_setRegA();
	HMC_setRegB();
	HMC_setRegMR();
}
