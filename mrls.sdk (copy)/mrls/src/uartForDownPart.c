#include "uartForDownPart.h"

	uint32_t timeOutReadNMEA = TIME_OUT_SEC_WAIT_VALID_STATUS_NMEA;
	uint32_t TotalReceivedCount =  0;
	uint8_t SendBufferUart[TEST_BUFFER_SIZE_UART];	/* Buffer for Transmitting Data */
	uint8_t RecvBufferUart[TEST_BUFFER_SIZE_UART];	/* Buffer for Receiving Data */
	uint8_t RecvBufferUartNULL[TEST_BUFFER_SIZE_UART];
	uint8_t volatile buff_UART[1024];
	uint8_t rxDataUart = 0;
	XUartLite UartLite;
	XUartPs UartPs;
	size_t rxSize;
	char rxUartData[128];
	char  end[128];
	char*  p_end = end;
	volatile double lat = 1;
	volatile double lon = 1;
	volatile uint32_t cntByte = 0;
	static volatile char lat_degrees_str[16] = {0,};
	static volatile char lat_minutes_str[16] = {0,};
	static volatile char lon_degrees_str[16] = {0,};
	static volatile char lon_minutes_str[16] = {0,};

int UartLiteSetupIntrSystem(INTC *IntcInstancePtr,	XUartLite *UartLiteInstPtr,	u16 UartLiteIntrId)
{
	int Status;




	XScuGic_Config *IntcConfig;

	/*
	 * Initialize the interrupt controller driver so that it is ready to
	 * use.
	 */
	IntcConfig = XScuGic_LookupConfig(INTC_DEVICE_ID);
	if (NULL == IntcConfig) {
		return XST_FAILURE;
	}

	Status = XScuGic_CfgInitialize(IntcInstancePtr, IntcConfig,
					IntcConfig->CpuBaseAddress);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}


	XScuGic_SetPriorityTriggerType(IntcInstancePtr, UartLiteIntrId,
					0xA0, 0x3);

	/*
	 * Connect the interrupt handler that will be called when an
	 * interrupt occurs for the device.
	 */
	Status = XScuGic_Connect(IntcInstancePtr, UartLiteIntrId,
				 (Xil_ExceptionHandler)XUartLite_InterruptHandler,
				 UartLiteInstPtr);
	if (Status != XST_SUCCESS) {
		return Status;
	}

	/*
	 * Enable the interrupt for the Timer device.
	 */
	XScuGic_Enable(IntcInstancePtr, UartLiteIntrId);


	/*
	 * Initialize the exception table.
	 */
	Xil_ExceptionInit();

	/*
	 * Register the interrupt controller handler with the exception table.
	 */
	Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_INT,
			(Xil_ExceptionHandler)INTC_HANDLER,
			IntcInstancePtr);

	/*
	 * Enable exceptions.
	 */
	Xil_ExceptionEnable();



	return XST_SUCCESS;
}
int uartLiteInit(){

	int Status;


		Status = XUartLite_Initialize(&UartLite, UARTLITE_DEVICE_ID);
		if (Status != XST_SUCCESS) {
			return XST_FAILURE;
		}

		Status = XUartLite_SelfTest(&UartLite);
		if (Status != XST_SUCCESS) {
			return XST_FAILURE;
		}

		return XST_SUCCESS;
}
int ADPART_runMotor(){

	unsigned int SentCount;
	SendBufferUart[0] = COMMAND_TO_ADPART__RUNMOTOR;

	SentCount = XUartLite_Send(&UartLite, SendBufferUart, 1);
	if (SentCount != TEST_BUFFER_SIZE_UART) {
		return XST_FAILURE;
	}
	return XST_SUCCESS;
}
int ADPART_stopMotor(){

	unsigned int SentCount;
	SendBufferUart[0] = COMMAND_TO_ADPART__STOPMOTOR;
	SentCount = XUartLite_Send(&UartLite, SendBufferUart, 1);
	if (SentCount != TEST_BUFFER_SIZE_UART) {
		return XST_FAILURE;
	}
	return XST_SUCCESS;
}
int ADPART_MotorSpeed( t_MotorSpeed speed){

	unsigned int SentCount;

	switch( speed ){
		case SPEED_MOTOR_1_25_SEC :	SendBufferUart[0] = COMMAND_TO_ADPART__SPEED_1_25_SEC;
			xil_printf( "INFO: MotorSpeed 1.25 sec\n\r");
			break;
		case SPEED_MOTOR_2_5_SEC :	SendBufferUart[0] = COMMAND_TO_ADPART__SPEED_2_5_SEC;
			xil_printf( "INFO: MotorSpeed 2.5 sec\n\r");
			break;
		case SPEED_MOTOR_5_SEC :	SendBufferUart[0] = COMMAND_TO_ADPART__SPEED_5_SEC;
			xil_printf( "INFO: MotorSpeed 5 sec\n\r");
			break;
	}

	SentCount = XUartLite_Send(&UartLite, SendBufferUart, 1);
	if (SentCount != TEST_BUFFER_SIZE_UART) {
		return XST_FAILURE;
	}
	return XST_SUCCESS;
}
int parse_data_from_UBLOX(){

	cntByte = 0;

	while (FIND_STR('R','M','C'))							// Ждем строку с GNRMC
	{
		if(cntByte != SIZE_BUF_FROM_JEKA)		cntByte++;	//
		else												// Если индекс символа в сторке далеко от начала строки, то нехуй там искать
		{
			memset((void *)buff_UART, 0, 1024);				// Чистим приемный буфер
			cntByte = 0;									// Обнуляем индекс для следующего поиска RMC
			return XST_FAILURE;
		};
	}
	// Нашли RMC. Далее необходимо искать признак валидных координат
	// Если нет признака валидных координат в течении 5 минут то ставим признак GPS_Received_Err
	// и продожаем работу (отключаем прерывания и работаем без координат)
	if ( (buff_UART[NMEA_GNRMC_STATUS_VALID_POSITION] != 'A') | (timeOutReadNMEA == 0) ){

		xil_printf("wait valid data GPS from UBLOX... %d\r\n", timeOutReadNMEA);
		if( timeOutReadNMEA == 0 ){							// TimeOut закончился. Хватит ждать. Надо работать без координат
			XScuGic_Disable(&INTCInst, UART_INT_IRQ_ID);	// Отключаем прерывания по UART.
			xil_printf("WARNING: timeOutReadNMEA\r\n");
			GPS_Received_Err = true;						// Ставим признак GPS_Received_Err.
		}
		else
			timeOutReadNMEA--;

		return XST_FAILURE;
	}
	xil_printf("INFO: %s\r\n", buff_UART);	// Выводим NMEA пакет с валидным признаком "А"
	XScuGic_Disable(&INTCInst, UART_INT_IRQ_ID);  // Отключаем прерывания по UART. Нашли признак валидности
//	XScuGic_Disconnect(&INTCInst, UART_INT_IRQ_ID);

	// Ищем данные в строке
		strncpy((char *)lat_degrees_str, 		BUFF_UART(16),			2);
		strncpy((char *)lat_minutes_str, 		BUFF_UART(18),			8);
		strncpy((char *)lon_degrees_str, 		BUFF_UART(30),			2);
		strncpy((char *)lon_minutes_str, 		BUFF_UART(32),			8);

		float lat_degrees = strtod(lat_degrees_str, &p_end);
		float lat_minutes = strtod(lat_minutes_str, &p_end)/60 ;
		float lon_minutes = strtod(lon_minutes_str, &p_end)/60;
		float lon_degrees = strtod(lon_degrees_str, &p_end);

		lon = lon_degrees + lon_minutes;
		lat = lat_degrees + lat_minutes;

		cntByte = 0;
		memset((void *)buff_UART, 0, 1024);

		GPS_Received_Done = true;

		return XST_SUCCESS;
}
void UBLOX_read_GPS(){

	GPS_Received_Err  = false;
	GPS_Received_Done = false;
	timeOutReadNMEA = TIME_OUT_SEC_WAIT_VALID_STATUS_NMEA;

	xil_printf("INFO: Start parse GPS Coordinate...\r\n");

	XScuGic_Enable(&INTCInst, UART_INT_IRQ_ID);
	while( WAIT_FOR_RECEIVED_GPS ){}

	if( GPS_Received_Done ){
		printf("INFO: lan == %f, lon == %f\r\n", lat, lon);
		printf("INFO: GPS Coordinate Received complete...\r\n");
	}
	else{
		printf("WARNING: data GPS is not valid. Check if the sky is clear\r\n");
	}

}
void uartPSHandler(void *CallBackRef, u32 Event, unsigned int EventData)
{



	//	/* All of the data has been sent */
		if (Event == XUARTPS_EVENT_SENT_DATA) {
	//		TotalSentCount = EventData;
		}

		/* All of the data has been received */
		if (Event == XUARTPS_EVENT_RECV_DATA) {
	//
	//		rxDataUart = RecvBufferUart[0];
	//		rxDataUart = XUartPs_Recv(&UartPs, RecvBufferUart, 1);
	//		xil_printf(RecvBufferUart);
	//		TotalReceivedCount = EventData;

			XUartPs_Recv(&UartPs, RecvBufferUart, 1);
			buff_UART[cntByte] = RecvBufferUart[0];	// ������������������ ���� ����������

				if((buff_UART[cntByte] == '\n'))		// Ждем конца строки
				{
					parse_data_from_UBLOX();			// Смотрим что за строка
				}
				else if( cntByte == TEST_BUFFER_SIZE_UART - 512 ){}
				else
					cntByte++;
		}

		/*
		 * Data was received, but not the expected number of bytes, a
		 * timeout just indicates the data stopped for 8 character times
		 */
		if (Event == XUARTPS_EVENT_RECV_TOUT) {
//			printf("INFO: XUARTPS_EVENT_RECV_TOUT\r\n");
			XUartPs_Recv(&UartPs, RecvBufferUartNULL, 1);
		}

		/*
		 * Data was received with an error, keep the data but determine
		 * what kind of errors occurred
		 */
		if (Event == XUARTPS_EVENT_RECV_ERROR) {
//			printf("INFO: XUARTPS_EVENT_RECV_ERROR\r\n");
			XUartPs_Recv(&UartPs, RecvBufferUartNULL, 128);
		}

		/*
		 * Data was received with an parity or frame or break error, keep the data
		 * but determine what kind of errors occurred. Specific to Zynq Ultrascale+
		 * MP.
		 */
		if (Event == XUARTPS_EVENT_PARE_FRAME_BRKE) {
			printf("INFO: XUARTPS_EVENT_PARE_FRAME_BRKE\r\n");
		}

		/*
		 * Data was received with an overrun error, keep the data but determine
		 * what kind of errors occurred. Specific to Zynq Ultrascale+ MP.
		 */
		if (Event == XUARTPS_EVENT_RECV_ORERR) {
			printf("INFO: XUARTPS_EVENT_RECV_ORERR\r\n");
		}
}
static int SetupInterruptSystem(INTC *IntcInstancePtr,
				XUartPs *UartInstancePtr,
				u16 UartIntrId)
{
//	int Status;
//
//	XScuGic_Config *IntcConfig; /* Config for interrupt controller */
//
//	/* Initialize the interrupt controller driver */
//	IntcConfig = XScuGic_LookupConfig(INTC_DEVICE_ID);
//	if (NULL == IntcConfig) {
//		return XST_FAILURE;
//	}
//
//	Status = XScuGic_CfgInitialize(IntcInstancePtr, IntcConfig,
//					IntcConfig->CpuBaseAddress);
//	if (Status != XST_SUCCESS) {
//		return XST_FAILURE;
//	}
//
//	/*
//	 * Connect the interrupt controller interrupt handler to the
//	 * hardware interrupt handling logic in the processor.
//	 */
//	Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_INT,
//				(Xil_ExceptionHandler) XScuGic_InterruptHandler,
//				IntcInstancePtr);
//
//
//	/*
//	 * Connect a device driver handler that will be called when an
//	 * interrupt for the device occurs, the device driver handler
//	 * performs the specific interrupt processing for the device
//	 */
//	Status = XScuGic_Connect(IntcInstancePtr, UartIntrId,
//				  (Xil_ExceptionHandler) XUartPs_InterruptHandler,
//				  (void *) UartInstancePtr);
//	if (Status != XST_SUCCESS) {
//		return XST_FAILURE;
//	}
//
//	/* Enable the interrupt for the device */
//	XScuGic_Enable(IntcInstancePtr, UartIntrId);
//
//	 Xil_ExceptionEnable();
//
//
	return XST_SUCCESS;
}
int UartPsIntrInit(INTC *IntcInstPtr, XUartPs *UartInstPtr,
			u16 DeviceId, u16 UartIntrId)
{
	int Status;
	XUartPs_Config *Config;
//	int Index;
//	u32 IntrMask;
//	int BadByteCount = 0;

	/*
	 * Initialize the UART driver so that it's ready to use
	 * Look up the configuration in the config table, then initialize it.
	 */
	Config = XUartPs_LookupConfig(DeviceId);
	if (NULL == Config) {
		return XST_FAILURE;
	}

	Status = XUartPs_CfgInitialize(UartInstPtr, Config, Config->BaseAddress);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	/* Check hardware build */
	Status = XUartPs_SelfTest(UartInstPtr);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	/*
	 * Connect the UART to the interrupt subsystem such that interrupts
	 * can occur. This function is application specific.
	 */
	Status = SetupInterruptSystem(IntcInstPtr, UartInstPtr, UartIntrId);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}



	return XST_SUCCESS;
}
