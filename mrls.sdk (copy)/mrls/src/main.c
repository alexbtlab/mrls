#include "main.h"

	bool GPS_Received_Done       = false;
	bool gpsDataTransmitToPC     = false;
	bool compasDataTransmitToPC  = false;
	bool GPS_Received_Err 		 = false;
	bool neededPrintTelemetry = false;

int main(){

	MRLS2D_Init();
	MRLS2D_TelemetryRead();
	MRLS2D_Net();

	xil_printf("INFO: waiting for UI will be request telemetry data from MRLS\r\n");

//	while ( WAITING_FOR_TRANSMIT_UBLOX_DATA )
//							xemacif_input(echo_netif); // send data once to cPC

	XAxiDma_SimpleTransfer(&AxiDma,(UINTPTR) praw,	MAX_PKT_LEN, XAXIDMA_DEVICE_TO_DMA);


	xil_printf("MRLS_2D battle mode started...\r\n");
	xil_printf("<<<---*** XAxiDma_SimpleTransfer. ProcessMRLS2DExecuting... ***--->>>\r\n");

	MRLS2D_trigerState( true );

	while(true)
		MRLS2D_Process();

	cleanup_platform();
	return 0;
}
void delay(u32 delayVal){
	while(delayVal--){}
}
