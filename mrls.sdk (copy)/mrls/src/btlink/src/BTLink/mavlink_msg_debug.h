#pragma once
// MESSAGE DEBUG PACKING

#define MAVLINK_MSG_ID_DEBUG 5


typedef struct __mavlink_debug_t {
 uint64_t param1; /*<  Parameter 1.*/
 uint64_t param2; /*<  Parameter 2.*/
 uint64_t param3; /*<  Parameter 3.*/
 uint64_t param4; /*<  Parameter 4.*/
 uint64_t param5; /*<  Parameter 5.*/
 uint64_t param6; /*<  Parameter 6.*/
 uint64_t param7; /*<  Parameter 7.*/
 uint64_t param8; /*<  Parameter 8.*/
 uint32_t time; /*<  Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude of the number.*/
 char text[128]; /*<  DEBUG TEXT.*/
} mavlink_debug_t;

#define MAVLINK_MSG_ID_DEBUG_LEN 196
#define MAVLINK_MSG_ID_DEBUG_MIN_LEN 196
#define MAVLINK_MSG_ID_5_LEN 196
#define MAVLINK_MSG_ID_5_MIN_LEN 196

#define MAVLINK_MSG_ID_DEBUG_CRC 149
#define MAVLINK_MSG_ID_5_CRC 149

#define MAVLINK_MSG_DEBUG_FIELD_TEXT_LEN 128

#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_DEBUG { \
    5, \
    "DEBUG", \
    10, \
    {  { "time", NULL, MAVLINK_TYPE_UINT32_T, 0, 64, offsetof(mavlink_debug_t, time) }, \
         { "text", NULL, MAVLINK_TYPE_CHAR, 128, 68, offsetof(mavlink_debug_t, text) }, \
         { "param1", NULL, MAVLINK_TYPE_UINT64_T, 0, 0, offsetof(mavlink_debug_t, param1) }, \
         { "param2", NULL, MAVLINK_TYPE_UINT64_T, 0, 8, offsetof(mavlink_debug_t, param2) }, \
         { "param3", NULL, MAVLINK_TYPE_UINT64_T, 0, 16, offsetof(mavlink_debug_t, param3) }, \
         { "param4", NULL, MAVLINK_TYPE_UINT64_T, 0, 24, offsetof(mavlink_debug_t, param4) }, \
         { "param5", NULL, MAVLINK_TYPE_UINT64_T, 0, 32, offsetof(mavlink_debug_t, param5) }, \
         { "param6", NULL, MAVLINK_TYPE_UINT64_T, 0, 40, offsetof(mavlink_debug_t, param6) }, \
         { "param7", NULL, MAVLINK_TYPE_UINT64_T, 0, 48, offsetof(mavlink_debug_t, param7) }, \
         { "param8", NULL, MAVLINK_TYPE_UINT64_T, 0, 56, offsetof(mavlink_debug_t, param8) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_DEBUG { \
    "DEBUG", \
    10, \
    {  { "time", NULL, MAVLINK_TYPE_UINT32_T, 0, 64, offsetof(mavlink_debug_t, time) }, \
         { "text", NULL, MAVLINK_TYPE_CHAR, 128, 68, offsetof(mavlink_debug_t, text) }, \
         { "param1", NULL, MAVLINK_TYPE_UINT64_T, 0, 0, offsetof(mavlink_debug_t, param1) }, \
         { "param2", NULL, MAVLINK_TYPE_UINT64_T, 0, 8, offsetof(mavlink_debug_t, param2) }, \
         { "param3", NULL, MAVLINK_TYPE_UINT64_T, 0, 16, offsetof(mavlink_debug_t, param3) }, \
         { "param4", NULL, MAVLINK_TYPE_UINT64_T, 0, 24, offsetof(mavlink_debug_t, param4) }, \
         { "param5", NULL, MAVLINK_TYPE_UINT64_T, 0, 32, offsetof(mavlink_debug_t, param5) }, \
         { "param6", NULL, MAVLINK_TYPE_UINT64_T, 0, 40, offsetof(mavlink_debug_t, param6) }, \
         { "param7", NULL, MAVLINK_TYPE_UINT64_T, 0, 48, offsetof(mavlink_debug_t, param7) }, \
         { "param8", NULL, MAVLINK_TYPE_UINT64_T, 0, 56, offsetof(mavlink_debug_t, param8) }, \
         } \
}
#endif

/**
 * @brief Pack a debug message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param time  Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude of the number.
 * @param text  DEBUG TEXT.
 * @param param1  Parameter 1.
 * @param param2  Parameter 2.
 * @param param3  Parameter 3.
 * @param param4  Parameter 4.
 * @param param5  Parameter 5.
 * @param param6  Parameter 6.
 * @param param7  Parameter 7.
 * @param param8  Parameter 8.
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_debug_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint32_t time, const char *text, uint64_t param1, uint64_t param2, uint64_t param3, uint64_t param4, uint64_t param5, uint64_t param6, uint64_t param7, uint64_t param8)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_DEBUG_LEN];
    _mav_put_uint64_t(buf, 0, param1);
    _mav_put_uint64_t(buf, 8, param2);
    _mav_put_uint64_t(buf, 16, param3);
    _mav_put_uint64_t(buf, 24, param4);
    _mav_put_uint64_t(buf, 32, param5);
    _mav_put_uint64_t(buf, 40, param6);
    _mav_put_uint64_t(buf, 48, param7);
    _mav_put_uint64_t(buf, 56, param8);
    _mav_put_uint32_t(buf, 64, time);
    _mav_put_char_array(buf, 68, text, 128);
        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_DEBUG_LEN);
#else
    mavlink_debug_t packet;
    packet.param1 = param1;
    packet.param2 = param2;
    packet.param3 = param3;
    packet.param4 = param4;
    packet.param5 = param5;
    packet.param6 = param6;
    packet.param7 = param7;
    packet.param8 = param8;
    packet.time = time;
    mav_array_memcpy(packet.text, text, sizeof(char)*128);
        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_DEBUG_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_DEBUG;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_DEBUG_MIN_LEN, MAVLINK_MSG_ID_DEBUG_LEN, MAVLINK_MSG_ID_DEBUG_CRC);
}

/**
 * @brief Pack a debug message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param time  Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude of the number.
 * @param text  DEBUG TEXT.
 * @param param1  Parameter 1.
 * @param param2  Parameter 2.
 * @param param3  Parameter 3.
 * @param param4  Parameter 4.
 * @param param5  Parameter 5.
 * @param param6  Parameter 6.
 * @param param7  Parameter 7.
 * @param param8  Parameter 8.
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_debug_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint32_t time,const char *text,uint64_t param1,uint64_t param2,uint64_t param3,uint64_t param4,uint64_t param5,uint64_t param6,uint64_t param7,uint64_t param8)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_DEBUG_LEN];
    _mav_put_uint64_t(buf, 0, param1);
    _mav_put_uint64_t(buf, 8, param2);
    _mav_put_uint64_t(buf, 16, param3);
    _mav_put_uint64_t(buf, 24, param4);
    _mav_put_uint64_t(buf, 32, param5);
    _mav_put_uint64_t(buf, 40, param6);
    _mav_put_uint64_t(buf, 48, param7);
    _mav_put_uint64_t(buf, 56, param8);
    _mav_put_uint32_t(buf, 64, time);
    _mav_put_char_array(buf, 68, text, 128);
        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_DEBUG_LEN);
#else
    mavlink_debug_t packet;
    packet.param1 = param1;
    packet.param2 = param2;
    packet.param3 = param3;
    packet.param4 = param4;
    packet.param5 = param5;
    packet.param6 = param6;
    packet.param7 = param7;
    packet.param8 = param8;
    packet.time = time;
    mav_array_memcpy(packet.text, text, sizeof(char)*128);
        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_DEBUG_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_DEBUG;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_DEBUG_MIN_LEN, MAVLINK_MSG_ID_DEBUG_LEN, MAVLINK_MSG_ID_DEBUG_CRC);
}

/**
 * @brief Encode a debug struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param debug C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_debug_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_debug_t* debug)
{
    return mavlink_msg_debug_pack(system_id, component_id, msg, debug->time, debug->text, debug->param1, debug->param2, debug->param3, debug->param4, debug->param5, debug->param6, debug->param7, debug->param8);
}

/**
 * @brief Encode a debug struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param debug C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_debug_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_debug_t* debug)
{
    return mavlink_msg_debug_pack_chan(system_id, component_id, chan, msg, debug->time, debug->text, debug->param1, debug->param2, debug->param3, debug->param4, debug->param5, debug->param6, debug->param7, debug->param8);
}

/**
 * @brief Send a debug message
 * @param chan MAVLink channel to send the message
 *
 * @param time  Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude of the number.
 * @param text  DEBUG TEXT.
 * @param param1  Parameter 1.
 * @param param2  Parameter 2.
 * @param param3  Parameter 3.
 * @param param4  Parameter 4.
 * @param param5  Parameter 5.
 * @param param6  Parameter 6.
 * @param param7  Parameter 7.
 * @param param8  Parameter 8.
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_debug_send(mavlink_channel_t chan, uint32_t time, const char *text, uint64_t param1, uint64_t param2, uint64_t param3, uint64_t param4, uint64_t param5, uint64_t param6, uint64_t param7, uint64_t param8)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_DEBUG_LEN];
    _mav_put_uint64_t(buf, 0, param1);
    _mav_put_uint64_t(buf, 8, param2);
    _mav_put_uint64_t(buf, 16, param3);
    _mav_put_uint64_t(buf, 24, param4);
    _mav_put_uint64_t(buf, 32, param5);
    _mav_put_uint64_t(buf, 40, param6);
    _mav_put_uint64_t(buf, 48, param7);
    _mav_put_uint64_t(buf, 56, param8);
    _mav_put_uint32_t(buf, 64, time);
    _mav_put_char_array(buf, 68, text, 128);
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DEBUG, buf, MAVLINK_MSG_ID_DEBUG_MIN_LEN, MAVLINK_MSG_ID_DEBUG_LEN, MAVLINK_MSG_ID_DEBUG_CRC);
#else
    mavlink_debug_t packet;
    packet.param1 = param1;
    packet.param2 = param2;
    packet.param3 = param3;
    packet.param4 = param4;
    packet.param5 = param5;
    packet.param6 = param6;
    packet.param7 = param7;
    packet.param8 = param8;
    packet.time = time;
    mav_array_memcpy(packet.text, text, sizeof(char)*128);
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DEBUG, (const char *)&packet, MAVLINK_MSG_ID_DEBUG_MIN_LEN, MAVLINK_MSG_ID_DEBUG_LEN, MAVLINK_MSG_ID_DEBUG_CRC);
#endif
}

/**
 * @brief Send a debug message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_debug_send_struct(mavlink_channel_t chan, const mavlink_debug_t* debug)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_debug_send(chan, debug->time, debug->text, debug->param1, debug->param2, debug->param3, debug->param4, debug->param5, debug->param6, debug->param7, debug->param8);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DEBUG, (const char *)debug, MAVLINK_MSG_ID_DEBUG_MIN_LEN, MAVLINK_MSG_ID_DEBUG_LEN, MAVLINK_MSG_ID_DEBUG_CRC);
#endif
}

#if MAVLINK_MSG_ID_DEBUG_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_debug_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint32_t time, const char *text, uint64_t param1, uint64_t param2, uint64_t param3, uint64_t param4, uint64_t param5, uint64_t param6, uint64_t param7, uint64_t param8)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_uint64_t(buf, 0, param1);
    _mav_put_uint64_t(buf, 8, param2);
    _mav_put_uint64_t(buf, 16, param3);
    _mav_put_uint64_t(buf, 24, param4);
    _mav_put_uint64_t(buf, 32, param5);
    _mav_put_uint64_t(buf, 40, param6);
    _mav_put_uint64_t(buf, 48, param7);
    _mav_put_uint64_t(buf, 56, param8);
    _mav_put_uint32_t(buf, 64, time);
    _mav_put_char_array(buf, 68, text, 128);
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DEBUG, buf, MAVLINK_MSG_ID_DEBUG_MIN_LEN, MAVLINK_MSG_ID_DEBUG_LEN, MAVLINK_MSG_ID_DEBUG_CRC);
#else
    mavlink_debug_t *packet = (mavlink_debug_t *)msgbuf;
    packet->param1 = param1;
    packet->param2 = param2;
    packet->param3 = param3;
    packet->param4 = param4;
    packet->param5 = param5;
    packet->param6 = param6;
    packet->param7 = param7;
    packet->param8 = param8;
    packet->time = time;
    mav_array_memcpy(packet->text, text, sizeof(char)*128);
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DEBUG, (const char *)packet, MAVLINK_MSG_ID_DEBUG_MIN_LEN, MAVLINK_MSG_ID_DEBUG_LEN, MAVLINK_MSG_ID_DEBUG_CRC);
#endif
}
#endif

#endif

// MESSAGE DEBUG UNPACKING


/**
 * @brief Get field time from debug message
 *
 * @return  Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude of the number.
 */
static inline uint32_t mavlink_msg_debug_get_time(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint32_t(msg,  64);
}

/**
 * @brief Get field text from debug message
 *
 * @return  DEBUG TEXT.
 */
static inline uint16_t mavlink_msg_debug_get_text(const mavlink_message_t* msg, char *text)
{
    return _MAV_RETURN_char_array(msg, text, 128,  68);
}

/**
 * @brief Get field param1 from debug message
 *
 * @return  Parameter 1.
 */
static inline uint64_t mavlink_msg_debug_get_param1(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint64_t(msg,  0);
}

/**
 * @brief Get field param2 from debug message
 *
 * @return  Parameter 2.
 */
static inline uint64_t mavlink_msg_debug_get_param2(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint64_t(msg,  8);
}

/**
 * @brief Get field param3 from debug message
 *
 * @return  Parameter 3.
 */
static inline uint64_t mavlink_msg_debug_get_param3(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint64_t(msg,  16);
}

/**
 * @brief Get field param4 from debug message
 *
 * @return  Parameter 4.
 */
static inline uint64_t mavlink_msg_debug_get_param4(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint64_t(msg,  24);
}

/**
 * @brief Get field param5 from debug message
 *
 * @return  Parameter 5.
 */
static inline uint64_t mavlink_msg_debug_get_param5(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint64_t(msg,  32);
}

/**
 * @brief Get field param6 from debug message
 *
 * @return  Parameter 6.
 */
static inline uint64_t mavlink_msg_debug_get_param6(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint64_t(msg,  40);
}

/**
 * @brief Get field param7 from debug message
 *
 * @return  Parameter 7.
 */
static inline uint64_t mavlink_msg_debug_get_param7(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint64_t(msg,  48);
}

/**
 * @brief Get field param8 from debug message
 *
 * @return  Parameter 8.
 */
static inline uint64_t mavlink_msg_debug_get_param8(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint64_t(msg,  56);
}

/**
 * @brief Decode a debug message into a struct
 *
 * @param msg The message to decode
 * @param debug C-struct to decode the message contents into
 */
static inline void mavlink_msg_debug_decode(const mavlink_message_t* msg, mavlink_debug_t* debug)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    debug->param1 = mavlink_msg_debug_get_param1(msg);
    debug->param2 = mavlink_msg_debug_get_param2(msg);
    debug->param3 = mavlink_msg_debug_get_param3(msg);
    debug->param4 = mavlink_msg_debug_get_param4(msg);
    debug->param5 = mavlink_msg_debug_get_param5(msg);
    debug->param6 = mavlink_msg_debug_get_param6(msg);
    debug->param7 = mavlink_msg_debug_get_param7(msg);
    debug->param8 = mavlink_msg_debug_get_param8(msg);
    debug->time = mavlink_msg_debug_get_time(msg);
    mavlink_msg_debug_get_text(msg, debug->text);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_DEBUG_LEN? msg->len : MAVLINK_MSG_ID_DEBUG_LEN;
        memset(debug, 0, MAVLINK_MSG_ID_DEBUG_LEN);
    memcpy(debug, _MAV_PAYLOAD(msg), len);
#endif
}
