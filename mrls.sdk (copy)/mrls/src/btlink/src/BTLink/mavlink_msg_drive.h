#pragma once
// MESSAGE DRIVE PACKING

#define MAVLINK_MSG_ID_DRIVE 19


typedef struct __mavlink_drive_t {
 uint32_t time; /*<  Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude of the number.*/
 int8_t speed; /*<  */
 int8_t temp; /*<  .*/
} mavlink_drive_t;

#define MAVLINK_MSG_ID_DRIVE_LEN 6
#define MAVLINK_MSG_ID_DRIVE_MIN_LEN 6
#define MAVLINK_MSG_ID_19_LEN 6
#define MAVLINK_MSG_ID_19_MIN_LEN 6

#define MAVLINK_MSG_ID_DRIVE_CRC 208
#define MAVLINK_MSG_ID_19_CRC 208



#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_DRIVE { \
    19, \
    "DRIVE", \
    3, \
    {  { "time", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_drive_t, time) }, \
         { "speed", NULL, MAVLINK_TYPE_INT8_T, 0, 4, offsetof(mavlink_drive_t, speed) }, \
         { "temp", NULL, MAVLINK_TYPE_INT8_T, 0, 5, offsetof(mavlink_drive_t, temp) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_DRIVE { \
    "DRIVE", \
    3, \
    {  { "time", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_drive_t, time) }, \
         { "speed", NULL, MAVLINK_TYPE_INT8_T, 0, 4, offsetof(mavlink_drive_t, speed) }, \
         { "temp", NULL, MAVLINK_TYPE_INT8_T, 0, 5, offsetof(mavlink_drive_t, temp) }, \
         } \
}
#endif

/**
 * @brief Pack a drive message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param time  Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude of the number.
 * @param speed  
 * @param temp  .
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_drive_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint32_t time, int8_t speed, int8_t temp)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_DRIVE_LEN];
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_int8_t(buf, 4, speed);
    _mav_put_int8_t(buf, 5, temp);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_DRIVE_LEN);
#else
    mavlink_drive_t packet;
    packet.time = time;
    packet.speed = speed;
    packet.temp = temp;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_DRIVE_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_DRIVE;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_DRIVE_MIN_LEN, MAVLINK_MSG_ID_DRIVE_LEN, MAVLINK_MSG_ID_DRIVE_CRC);
}

/**
 * @brief Pack a drive message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param time  Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude of the number.
 * @param speed  
 * @param temp  .
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_drive_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint32_t time,int8_t speed,int8_t temp)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_DRIVE_LEN];
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_int8_t(buf, 4, speed);
    _mav_put_int8_t(buf, 5, temp);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_DRIVE_LEN);
#else
    mavlink_drive_t packet;
    packet.time = time;
    packet.speed = speed;
    packet.temp = temp;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_DRIVE_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_DRIVE;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_DRIVE_MIN_LEN, MAVLINK_MSG_ID_DRIVE_LEN, MAVLINK_MSG_ID_DRIVE_CRC);
}

/**
 * @brief Encode a drive struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param drive C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_drive_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_drive_t* drive)
{
    return mavlink_msg_drive_pack(system_id, component_id, msg, drive->time, drive->speed, drive->temp);
}

/**
 * @brief Encode a drive struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param drive C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_drive_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_drive_t* drive)
{
    return mavlink_msg_drive_pack_chan(system_id, component_id, chan, msg, drive->time, drive->speed, drive->temp);
}

/**
 * @brief Send a drive message
 * @param chan MAVLink channel to send the message
 *
 * @param time  Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude of the number.
 * @param speed  
 * @param temp  .
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_drive_send(mavlink_channel_t chan, uint32_t time, int8_t speed, int8_t temp)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_DRIVE_LEN];
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_int8_t(buf, 4, speed);
    _mav_put_int8_t(buf, 5, temp);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DRIVE, buf, MAVLINK_MSG_ID_DRIVE_MIN_LEN, MAVLINK_MSG_ID_DRIVE_LEN, MAVLINK_MSG_ID_DRIVE_CRC);
#else
    mavlink_drive_t packet;
    packet.time = time;
    packet.speed = speed;
    packet.temp = temp;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DRIVE, (const char *)&packet, MAVLINK_MSG_ID_DRIVE_MIN_LEN, MAVLINK_MSG_ID_DRIVE_LEN, MAVLINK_MSG_ID_DRIVE_CRC);
#endif
}

/**
 * @brief Send a drive message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_drive_send_struct(mavlink_channel_t chan, const mavlink_drive_t* drive)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_drive_send(chan, drive->time, drive->speed, drive->temp);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DRIVE, (const char *)drive, MAVLINK_MSG_ID_DRIVE_MIN_LEN, MAVLINK_MSG_ID_DRIVE_LEN, MAVLINK_MSG_ID_DRIVE_CRC);
#endif
}

#if MAVLINK_MSG_ID_DRIVE_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_drive_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint32_t time, int8_t speed, int8_t temp)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_int8_t(buf, 4, speed);
    _mav_put_int8_t(buf, 5, temp);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DRIVE, buf, MAVLINK_MSG_ID_DRIVE_MIN_LEN, MAVLINK_MSG_ID_DRIVE_LEN, MAVLINK_MSG_ID_DRIVE_CRC);
#else
    mavlink_drive_t *packet = (mavlink_drive_t *)msgbuf;
    packet->time = time;
    packet->speed = speed;
    packet->temp = temp;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_DRIVE, (const char *)packet, MAVLINK_MSG_ID_DRIVE_MIN_LEN, MAVLINK_MSG_ID_DRIVE_LEN, MAVLINK_MSG_ID_DRIVE_CRC);
#endif
}
#endif

#endif

// MESSAGE DRIVE UNPACKING


/**
 * @brief Get field time from drive message
 *
 * @return  Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude of the number.
 */
static inline uint32_t mavlink_msg_drive_get_time(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint32_t(msg,  0);
}

/**
 * @brief Get field speed from drive message
 *
 * @return  
 */
static inline int8_t mavlink_msg_drive_get_speed(const mavlink_message_t* msg)
{
    return _MAV_RETURN_int8_t(msg,  4);
}

/**
 * @brief Get field temp from drive message
 *
 * @return  .
 */
static inline int8_t mavlink_msg_drive_get_temp(const mavlink_message_t* msg)
{
    return _MAV_RETURN_int8_t(msg,  5);
}

/**
 * @brief Decode a drive message into a struct
 *
 * @param msg The message to decode
 * @param drive C-struct to decode the message contents into
 */
static inline void mavlink_msg_drive_decode(const mavlink_message_t* msg, mavlink_drive_t* drive)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    drive->time = mavlink_msg_drive_get_time(msg);
    drive->speed = mavlink_msg_drive_get_speed(msg);
    drive->temp = mavlink_msg_drive_get_temp(msg);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_DRIVE_LEN? msg->len : MAVLINK_MSG_ID_DRIVE_LEN;
        memset(drive, 0, MAVLINK_MSG_ID_DRIVE_LEN);
    memcpy(drive, _MAV_PAYLOAD(msg), len);
#endif
}
