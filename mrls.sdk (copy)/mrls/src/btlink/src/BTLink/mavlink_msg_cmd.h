#pragma once
// MESSAGE CMD PACKING

#define MAVLINK_MSG_ID_CMD 4


typedef struct __mavlink_cmd_t {
 uint64_t value1; /*<    1.*/
 uint64_t value2; /*<    2.*/
 uint64_t value3; /*<    3.*/
 uint64_t value4; /*<    4.*/
 uint64_t value5; /*<    5.*/
 uint32_t time; /*<  Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude of the number.*/
 uint8_t target_system; /*<  System ID*/
 uint8_t target_component; /*<  Component ID*/
 uint8_t cmd; /*<   .*/
} mavlink_cmd_t;

#define MAVLINK_MSG_ID_CMD_LEN 47
#define MAVLINK_MSG_ID_CMD_MIN_LEN 47
#define MAVLINK_MSG_ID_4_LEN 47
#define MAVLINK_MSG_ID_4_MIN_LEN 47

#define MAVLINK_MSG_ID_CMD_CRC 105
#define MAVLINK_MSG_ID_4_CRC 105



#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_CMD { \
    4, \
    "CMD", \
    9, \
    {  { "time", NULL, MAVLINK_TYPE_UINT32_T, 0, 40, offsetof(mavlink_cmd_t, time) }, \
         { "target_system", NULL, MAVLINK_TYPE_UINT8_T, 0, 44, offsetof(mavlink_cmd_t, target_system) }, \
         { "target_component", NULL, MAVLINK_TYPE_UINT8_T, 0, 45, offsetof(mavlink_cmd_t, target_component) }, \
         { "cmd", NULL, MAVLINK_TYPE_UINT8_T, 0, 46, offsetof(mavlink_cmd_t, cmd) }, \
         { "value1", NULL, MAVLINK_TYPE_UINT64_T, 0, 0, offsetof(mavlink_cmd_t, value1) }, \
         { "value2", NULL, MAVLINK_TYPE_UINT64_T, 0, 8, offsetof(mavlink_cmd_t, value2) }, \
         { "value3", NULL, MAVLINK_TYPE_UINT64_T, 0, 16, offsetof(mavlink_cmd_t, value3) }, \
         { "value4", NULL, MAVLINK_TYPE_UINT64_T, 0, 24, offsetof(mavlink_cmd_t, value4) }, \
         { "value5", NULL, MAVLINK_TYPE_UINT64_T, 0, 32, offsetof(mavlink_cmd_t, value5) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_CMD { \
    "CMD", \
    9, \
    {  { "time", NULL, MAVLINK_TYPE_UINT32_T, 0, 40, offsetof(mavlink_cmd_t, time) }, \
         { "target_system", NULL, MAVLINK_TYPE_UINT8_T, 0, 44, offsetof(mavlink_cmd_t, target_system) }, \
         { "target_component", NULL, MAVLINK_TYPE_UINT8_T, 0, 45, offsetof(mavlink_cmd_t, target_component) }, \
         { "cmd", NULL, MAVLINK_TYPE_UINT8_T, 0, 46, offsetof(mavlink_cmd_t, cmd) }, \
         { "value1", NULL, MAVLINK_TYPE_UINT64_T, 0, 0, offsetof(mavlink_cmd_t, value1) }, \
         { "value2", NULL, MAVLINK_TYPE_UINT64_T, 0, 8, offsetof(mavlink_cmd_t, value2) }, \
         { "value3", NULL, MAVLINK_TYPE_UINT64_T, 0, 16, offsetof(mavlink_cmd_t, value3) }, \
         { "value4", NULL, MAVLINK_TYPE_UINT64_T, 0, 24, offsetof(mavlink_cmd_t, value4) }, \
         { "value5", NULL, MAVLINK_TYPE_UINT64_T, 0, 32, offsetof(mavlink_cmd_t, value5) }, \
         } \
}
#endif

/**
 * @brief Pack a cmd message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param time  Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude of the number.
 * @param target_system  System ID
 * @param target_component  Component ID
 * @param cmd   .
 * @param value1    1.
 * @param value2    2.
 * @param value3    3.
 * @param value4    4.
 * @param value5    5.
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_cmd_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint32_t time, uint8_t target_system, uint8_t target_component, uint8_t cmd, uint64_t value1, uint64_t value2, uint64_t value3, uint64_t value4, uint64_t value5)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_CMD_LEN];
    _mav_put_uint64_t(buf, 0, value1);
    _mav_put_uint64_t(buf, 8, value2);
    _mav_put_uint64_t(buf, 16, value3);
    _mav_put_uint64_t(buf, 24, value4);
    _mav_put_uint64_t(buf, 32, value5);
    _mav_put_uint32_t(buf, 40, time);
    _mav_put_uint8_t(buf, 44, target_system);
    _mav_put_uint8_t(buf, 45, target_component);
    _mav_put_uint8_t(buf, 46, cmd);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_CMD_LEN);
#else
    mavlink_cmd_t packet;
    packet.value1 = value1;
    packet.value2 = value2;
    packet.value3 = value3;
    packet.value4 = value4;
    packet.value5 = value5;
    packet.time = time;
    packet.target_system = target_system;
    packet.target_component = target_component;
    packet.cmd = cmd;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_CMD_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_CMD;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_CMD_MIN_LEN, MAVLINK_MSG_ID_CMD_LEN, MAVLINK_MSG_ID_CMD_CRC);
}

/**
 * @brief Pack a cmd message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param time  Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude of the number.
 * @param target_system  System ID
 * @param target_component  Component ID
 * @param cmd   .
 * @param value1    1.
 * @param value2    2.
 * @param value3    3.
 * @param value4    4.
 * @param value5    5.
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_cmd_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint32_t time,uint8_t target_system,uint8_t target_component,uint8_t cmd,uint64_t value1,uint64_t value2,uint64_t value3,uint64_t value4,uint64_t value5)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_CMD_LEN];
    _mav_put_uint64_t(buf, 0, value1);
    _mav_put_uint64_t(buf, 8, value2);
    _mav_put_uint64_t(buf, 16, value3);
    _mav_put_uint64_t(buf, 24, value4);
    _mav_put_uint64_t(buf, 32, value5);
    _mav_put_uint32_t(buf, 40, time);
    _mav_put_uint8_t(buf, 44, target_system);
    _mav_put_uint8_t(buf, 45, target_component);
    _mav_put_uint8_t(buf, 46, cmd);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_CMD_LEN);
#else
    mavlink_cmd_t packet;
    packet.value1 = value1;
    packet.value2 = value2;
    packet.value3 = value3;
    packet.value4 = value4;
    packet.value5 = value5;
    packet.time = time;
    packet.target_system = target_system;
    packet.target_component = target_component;
    packet.cmd = cmd;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_CMD_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_CMD;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_CMD_MIN_LEN, MAVLINK_MSG_ID_CMD_LEN, MAVLINK_MSG_ID_CMD_CRC);
}

/**
 * @brief Encode a cmd struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param cmd C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_cmd_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_cmd_t* cmd)
{
    return mavlink_msg_cmd_pack(system_id, component_id, msg, cmd->time, cmd->target_system, cmd->target_component, cmd->cmd, cmd->value1, cmd->value2, cmd->value3, cmd->value4, cmd->value5);
}

/**
 * @brief Encode a cmd struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param cmd C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_cmd_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_cmd_t* cmd)
{
    return mavlink_msg_cmd_pack_chan(system_id, component_id, chan, msg, cmd->time, cmd->target_system, cmd->target_component, cmd->cmd, cmd->value1, cmd->value2, cmd->value3, cmd->value4, cmd->value5);
}

/**
 * @brief Send a cmd message
 * @param chan MAVLink channel to send the message
 *
 * @param time  Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude of the number.
 * @param target_system  System ID
 * @param target_component  Component ID
 * @param cmd   .
 * @param value1    1.
 * @param value2    2.
 * @param value3    3.
 * @param value4    4.
 * @param value5    5.
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_cmd_send(mavlink_channel_t chan, uint32_t time, uint8_t target_system, uint8_t target_component, uint8_t cmd, uint64_t value1, uint64_t value2, uint64_t value3, uint64_t value4, uint64_t value5)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_CMD_LEN];
    _mav_put_uint64_t(buf, 0, value1);
    _mav_put_uint64_t(buf, 8, value2);
    _mav_put_uint64_t(buf, 16, value3);
    _mav_put_uint64_t(buf, 24, value4);
    _mav_put_uint64_t(buf, 32, value5);
    _mav_put_uint32_t(buf, 40, time);
    _mav_put_uint8_t(buf, 44, target_system);
    _mav_put_uint8_t(buf, 45, target_component);
    _mav_put_uint8_t(buf, 46, cmd);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_CMD, buf, MAVLINK_MSG_ID_CMD_MIN_LEN, MAVLINK_MSG_ID_CMD_LEN, MAVLINK_MSG_ID_CMD_CRC);
#else
    mavlink_cmd_t packet;
    packet.value1 = value1;
    packet.value2 = value2;
    packet.value3 = value3;
    packet.value4 = value4;
    packet.value5 = value5;
    packet.time = time;
    packet.target_system = target_system;
    packet.target_component = target_component;
    packet.cmd = cmd;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_CMD, (const char *)&packet, MAVLINK_MSG_ID_CMD_MIN_LEN, MAVLINK_MSG_ID_CMD_LEN, MAVLINK_MSG_ID_CMD_CRC);
#endif
}

/**
 * @brief Send a cmd message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_cmd_send_struct(mavlink_channel_t chan, const mavlink_cmd_t* cmd)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_cmd_send(chan, cmd->time, cmd->target_system, cmd->target_component, cmd->cmd, cmd->value1, cmd->value2, cmd->value3, cmd->value4, cmd->value5);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_CMD, (const char *)cmd, MAVLINK_MSG_ID_CMD_MIN_LEN, MAVLINK_MSG_ID_CMD_LEN, MAVLINK_MSG_ID_CMD_CRC);
#endif
}

#if MAVLINK_MSG_ID_CMD_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_cmd_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint32_t time, uint8_t target_system, uint8_t target_component, uint8_t cmd, uint64_t value1, uint64_t value2, uint64_t value3, uint64_t value4, uint64_t value5)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_uint64_t(buf, 0, value1);
    _mav_put_uint64_t(buf, 8, value2);
    _mav_put_uint64_t(buf, 16, value3);
    _mav_put_uint64_t(buf, 24, value4);
    _mav_put_uint64_t(buf, 32, value5);
    _mav_put_uint32_t(buf, 40, time);
    _mav_put_uint8_t(buf, 44, target_system);
    _mav_put_uint8_t(buf, 45, target_component);
    _mav_put_uint8_t(buf, 46, cmd);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_CMD, buf, MAVLINK_MSG_ID_CMD_MIN_LEN, MAVLINK_MSG_ID_CMD_LEN, MAVLINK_MSG_ID_CMD_CRC);
#else
    mavlink_cmd_t *packet = (mavlink_cmd_t *)msgbuf;
    packet->value1 = value1;
    packet->value2 = value2;
    packet->value3 = value3;
    packet->value4 = value4;
    packet->value5 = value5;
    packet->time = time;
    packet->target_system = target_system;
    packet->target_component = target_component;
    packet->cmd = cmd;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_CMD, (const char *)packet, MAVLINK_MSG_ID_CMD_MIN_LEN, MAVLINK_MSG_ID_CMD_LEN, MAVLINK_MSG_ID_CMD_CRC);
#endif
}
#endif

#endif

// MESSAGE CMD UNPACKING


/**
 * @brief Get field time from cmd message
 *
 * @return  Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude of the number.
 */
static inline uint32_t mavlink_msg_cmd_get_time(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint32_t(msg,  40);
}

/**
 * @brief Get field target_system from cmd message
 *
 * @return  System ID
 */
static inline uint8_t mavlink_msg_cmd_get_target_system(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  44);
}

/**
 * @brief Get field target_component from cmd message
 *
 * @return  Component ID
 */
static inline uint8_t mavlink_msg_cmd_get_target_component(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  45);
}

/**
 * @brief Get field cmd from cmd message
 *
 * @return   .
 */
static inline uint8_t mavlink_msg_cmd_get_cmd(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  46);
}

/**
 * @brief Get field value1 from cmd message
 *
 * @return    1.
 */
static inline uint64_t mavlink_msg_cmd_get_value1(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint64_t(msg,  0);
}

/**
 * @brief Get field value2 from cmd message
 *
 * @return    2.
 */
static inline uint64_t mavlink_msg_cmd_get_value2(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint64_t(msg,  8);
}

/**
 * @brief Get field value3 from cmd message
 *
 * @return    3.
 */
static inline uint64_t mavlink_msg_cmd_get_value3(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint64_t(msg,  16);
}

/**
 * @brief Get field value4 from cmd message
 *
 * @return    4.
 */
static inline uint64_t mavlink_msg_cmd_get_value4(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint64_t(msg,  24);
}

/**
 * @brief Get field value5 from cmd message
 *
 * @return    5.
 */
static inline uint64_t mavlink_msg_cmd_get_value5(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint64_t(msg,  32);
}

/**
 * @brief Decode a cmd message into a struct
 *
 * @param msg The message to decode
 * @param cmd C-struct to decode the message contents into
 */
static inline void mavlink_msg_cmd_decode(const mavlink_message_t* msg, mavlink_cmd_t* cmd)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    cmd->value1 = mavlink_msg_cmd_get_value1(msg);
    cmd->value2 = mavlink_msg_cmd_get_value2(msg);
    cmd->value3 = mavlink_msg_cmd_get_value3(msg);
    cmd->value4 = mavlink_msg_cmd_get_value4(msg);
    cmd->value5 = mavlink_msg_cmd_get_value5(msg);
    cmd->time = mavlink_msg_cmd_get_time(msg);
    cmd->target_system = mavlink_msg_cmd_get_target_system(msg);
    cmd->target_component = mavlink_msg_cmd_get_target_component(msg);
    cmd->cmd = mavlink_msg_cmd_get_cmd(msg);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_CMD_LEN? msg->len : MAVLINK_MSG_ID_CMD_LEN;
        memset(cmd, 0, MAVLINK_MSG_ID_CMD_LEN);
    memcpy(cmd, _MAV_PAYLOAD(msg), len);
#endif
}
