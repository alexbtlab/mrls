#pragma once
// MESSAGE COMMAND_INT PACKING

#define MAVLINK_MSG_ID_COMMAND_INT 6


typedef struct __mavlink_command_int_t {
 uint64_t param1; /*<  Parameter 1 (for the specific command).*/
 uint64_t param2; /*<  Parameter 2 (for the specific command).*/
 uint64_t param3; /*<  Parameter 3 (for the specific command).*/
 uint64_t param4; /*<  Parameter 4 (for the specific command).*/
 uint64_t param5; /*<  Parameter 5 (for the specific command).*/
 uint64_t param6; /*<  Parameter 6 (for the specific command).*/
 uint64_t param7; /*<  Parameter 7 (for the specific command).*/
 uint64_t param8; /*<  Parameter 8 (for the specific command).*/
 uint16_t command; /*<  The scheduled action for the mission item.*/
 uint8_t target_system; /*<  System ID*/
 uint8_t target_component; /*<  Component ID*/
} mavlink_command_int_t;

#define MAVLINK_MSG_ID_COMMAND_INT_LEN 68
#define MAVLINK_MSG_ID_COMMAND_INT_MIN_LEN 68
#define MAVLINK_MSG_ID_6_LEN 68
#define MAVLINK_MSG_ID_6_MIN_LEN 68

#define MAVLINK_MSG_ID_COMMAND_INT_CRC 225
#define MAVLINK_MSG_ID_6_CRC 225



#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_COMMAND_INT { \
    6, \
    "COMMAND_INT", \
    11, \
    {  { "target_system", NULL, MAVLINK_TYPE_UINT8_T, 0, 66, offsetof(mavlink_command_int_t, target_system) }, \
         { "target_component", NULL, MAVLINK_TYPE_UINT8_T, 0, 67, offsetof(mavlink_command_int_t, target_component) }, \
         { "command", NULL, MAVLINK_TYPE_UINT16_T, 0, 64, offsetof(mavlink_command_int_t, command) }, \
         { "param1", NULL, MAVLINK_TYPE_UINT64_T, 0, 0, offsetof(mavlink_command_int_t, param1) }, \
         { "param2", NULL, MAVLINK_TYPE_UINT64_T, 0, 8, offsetof(mavlink_command_int_t, param2) }, \
         { "param3", NULL, MAVLINK_TYPE_UINT64_T, 0, 16, offsetof(mavlink_command_int_t, param3) }, \
         { "param4", NULL, MAVLINK_TYPE_UINT64_T, 0, 24, offsetof(mavlink_command_int_t, param4) }, \
         { "param5", NULL, MAVLINK_TYPE_UINT64_T, 0, 32, offsetof(mavlink_command_int_t, param5) }, \
         { "param6", NULL, MAVLINK_TYPE_UINT64_T, 0, 40, offsetof(mavlink_command_int_t, param6) }, \
         { "param7", NULL, MAVLINK_TYPE_UINT64_T, 0, 48, offsetof(mavlink_command_int_t, param7) }, \
         { "param8", NULL, MAVLINK_TYPE_UINT64_T, 0, 56, offsetof(mavlink_command_int_t, param8) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_COMMAND_INT { \
    "COMMAND_INT", \
    11, \
    {  { "target_system", NULL, MAVLINK_TYPE_UINT8_T, 0, 66, offsetof(mavlink_command_int_t, target_system) }, \
         { "target_component", NULL, MAVLINK_TYPE_UINT8_T, 0, 67, offsetof(mavlink_command_int_t, target_component) }, \
         { "command", NULL, MAVLINK_TYPE_UINT16_T, 0, 64, offsetof(mavlink_command_int_t, command) }, \
         { "param1", NULL, MAVLINK_TYPE_UINT64_T, 0, 0, offsetof(mavlink_command_int_t, param1) }, \
         { "param2", NULL, MAVLINK_TYPE_UINT64_T, 0, 8, offsetof(mavlink_command_int_t, param2) }, \
         { "param3", NULL, MAVLINK_TYPE_UINT64_T, 0, 16, offsetof(mavlink_command_int_t, param3) }, \
         { "param4", NULL, MAVLINK_TYPE_UINT64_T, 0, 24, offsetof(mavlink_command_int_t, param4) }, \
         { "param5", NULL, MAVLINK_TYPE_UINT64_T, 0, 32, offsetof(mavlink_command_int_t, param5) }, \
         { "param6", NULL, MAVLINK_TYPE_UINT64_T, 0, 40, offsetof(mavlink_command_int_t, param6) }, \
         { "param7", NULL, MAVLINK_TYPE_UINT64_T, 0, 48, offsetof(mavlink_command_int_t, param7) }, \
         { "param8", NULL, MAVLINK_TYPE_UINT64_T, 0, 56, offsetof(mavlink_command_int_t, param8) }, \
         } \
}
#endif

/**
 * @brief Pack a command_int message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param target_system  System ID
 * @param target_component  Component ID
 * @param command  The scheduled action for the mission item.
 * @param param1  Parameter 1 (for the specific command).
 * @param param2  Parameter 2 (for the specific command).
 * @param param3  Parameter 3 (for the specific command).
 * @param param4  Parameter 4 (for the specific command).
 * @param param5  Parameter 5 (for the specific command).
 * @param param6  Parameter 6 (for the specific command).
 * @param param7  Parameter 7 (for the specific command).
 * @param param8  Parameter 8 (for the specific command).
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_command_int_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint8_t target_system, uint8_t target_component, uint16_t command, uint64_t param1, uint64_t param2, uint64_t param3, uint64_t param4, uint64_t param5, uint64_t param6, uint64_t param7, uint64_t param8)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_COMMAND_INT_LEN];
    _mav_put_uint64_t(buf, 0, param1);
    _mav_put_uint64_t(buf, 8, param2);
    _mav_put_uint64_t(buf, 16, param3);
    _mav_put_uint64_t(buf, 24, param4);
    _mav_put_uint64_t(buf, 32, param5);
    _mav_put_uint64_t(buf, 40, param6);
    _mav_put_uint64_t(buf, 48, param7);
    _mav_put_uint64_t(buf, 56, param8);
    _mav_put_uint16_t(buf, 64, command);
    _mav_put_uint8_t(buf, 66, target_system);
    _mav_put_uint8_t(buf, 67, target_component);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_COMMAND_INT_LEN);
#else
    mavlink_command_int_t packet;
    packet.param1 = param1;
    packet.param2 = param2;
    packet.param3 = param3;
    packet.param4 = param4;
    packet.param5 = param5;
    packet.param6 = param6;
    packet.param7 = param7;
    packet.param8 = param8;
    packet.command = command;
    packet.target_system = target_system;
    packet.target_component = target_component;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_COMMAND_INT_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_COMMAND_INT;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_COMMAND_INT_MIN_LEN, MAVLINK_MSG_ID_COMMAND_INT_LEN, MAVLINK_MSG_ID_COMMAND_INT_CRC);
}

/**
 * @brief Pack a command_int message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param target_system  System ID
 * @param target_component  Component ID
 * @param command  The scheduled action for the mission item.
 * @param param1  Parameter 1 (for the specific command).
 * @param param2  Parameter 2 (for the specific command).
 * @param param3  Parameter 3 (for the specific command).
 * @param param4  Parameter 4 (for the specific command).
 * @param param5  Parameter 5 (for the specific command).
 * @param param6  Parameter 6 (for the specific command).
 * @param param7  Parameter 7 (for the specific command).
 * @param param8  Parameter 8 (for the specific command).
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_command_int_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint8_t target_system,uint8_t target_component,uint16_t command,uint64_t param1,uint64_t param2,uint64_t param3,uint64_t param4,uint64_t param5,uint64_t param6,uint64_t param7,uint64_t param8)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_COMMAND_INT_LEN];
    _mav_put_uint64_t(buf, 0, param1);
    _mav_put_uint64_t(buf, 8, param2);
    _mav_put_uint64_t(buf, 16, param3);
    _mav_put_uint64_t(buf, 24, param4);
    _mav_put_uint64_t(buf, 32, param5);
    _mav_put_uint64_t(buf, 40, param6);
    _mav_put_uint64_t(buf, 48, param7);
    _mav_put_uint64_t(buf, 56, param8);
    _mav_put_uint16_t(buf, 64, command);
    _mav_put_uint8_t(buf, 66, target_system);
    _mav_put_uint8_t(buf, 67, target_component);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_COMMAND_INT_LEN);
#else
    mavlink_command_int_t packet;
    packet.param1 = param1;
    packet.param2 = param2;
    packet.param3 = param3;
    packet.param4 = param4;
    packet.param5 = param5;
    packet.param6 = param6;
    packet.param7 = param7;
    packet.param8 = param8;
    packet.command = command;
    packet.target_system = target_system;
    packet.target_component = target_component;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_COMMAND_INT_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_COMMAND_INT;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_COMMAND_INT_MIN_LEN, MAVLINK_MSG_ID_COMMAND_INT_LEN, MAVLINK_MSG_ID_COMMAND_INT_CRC);
}

/**
 * @brief Encode a command_int struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param command_int C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_command_int_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_command_int_t* command_int)
{
    return mavlink_msg_command_int_pack(system_id, component_id, msg, command_int->target_system, command_int->target_component, command_int->command, command_int->param1, command_int->param2, command_int->param3, command_int->param4, command_int->param5, command_int->param6, command_int->param7, command_int->param8);
}

/**
 * @brief Encode a command_int struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param command_int C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_command_int_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_command_int_t* command_int)
{
    return mavlink_msg_command_int_pack_chan(system_id, component_id, chan, msg, command_int->target_system, command_int->target_component, command_int->command, command_int->param1, command_int->param2, command_int->param3, command_int->param4, command_int->param5, command_int->param6, command_int->param7, command_int->param8);
}

/**
 * @brief Send a command_int message
 * @param chan MAVLink channel to send the message
 *
 * @param target_system  System ID
 * @param target_component  Component ID
 * @param command  The scheduled action for the mission item.
 * @param param1  Parameter 1 (for the specific command).
 * @param param2  Parameter 2 (for the specific command).
 * @param param3  Parameter 3 (for the specific command).
 * @param param4  Parameter 4 (for the specific command).
 * @param param5  Parameter 5 (for the specific command).
 * @param param6  Parameter 6 (for the specific command).
 * @param param7  Parameter 7 (for the specific command).
 * @param param8  Parameter 8 (for the specific command).
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_command_int_send(mavlink_channel_t chan, uint8_t target_system, uint8_t target_component, uint16_t command, uint64_t param1, uint64_t param2, uint64_t param3, uint64_t param4, uint64_t param5, uint64_t param6, uint64_t param7, uint64_t param8)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_COMMAND_INT_LEN];
    _mav_put_uint64_t(buf, 0, param1);
    _mav_put_uint64_t(buf, 8, param2);
    _mav_put_uint64_t(buf, 16, param3);
    _mav_put_uint64_t(buf, 24, param4);
    _mav_put_uint64_t(buf, 32, param5);
    _mav_put_uint64_t(buf, 40, param6);
    _mav_put_uint64_t(buf, 48, param7);
    _mav_put_uint64_t(buf, 56, param8);
    _mav_put_uint16_t(buf, 64, command);
    _mav_put_uint8_t(buf, 66, target_system);
    _mav_put_uint8_t(buf, 67, target_component);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_COMMAND_INT, buf, MAVLINK_MSG_ID_COMMAND_INT_MIN_LEN, MAVLINK_MSG_ID_COMMAND_INT_LEN, MAVLINK_MSG_ID_COMMAND_INT_CRC);
#else
    mavlink_command_int_t packet;
    packet.param1 = param1;
    packet.param2 = param2;
    packet.param3 = param3;
    packet.param4 = param4;
    packet.param5 = param5;
    packet.param6 = param6;
    packet.param7 = param7;
    packet.param8 = param8;
    packet.command = command;
    packet.target_system = target_system;
    packet.target_component = target_component;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_COMMAND_INT, (const char *)&packet, MAVLINK_MSG_ID_COMMAND_INT_MIN_LEN, MAVLINK_MSG_ID_COMMAND_INT_LEN, MAVLINK_MSG_ID_COMMAND_INT_CRC);
#endif
}

/**
 * @brief Send a command_int message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_command_int_send_struct(mavlink_channel_t chan, const mavlink_command_int_t* command_int)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_command_int_send(chan, command_int->target_system, command_int->target_component, command_int->command, command_int->param1, command_int->param2, command_int->param3, command_int->param4, command_int->param5, command_int->param6, command_int->param7, command_int->param8);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_COMMAND_INT, (const char *)command_int, MAVLINK_MSG_ID_COMMAND_INT_MIN_LEN, MAVLINK_MSG_ID_COMMAND_INT_LEN, MAVLINK_MSG_ID_COMMAND_INT_CRC);
#endif
}

#if MAVLINK_MSG_ID_COMMAND_INT_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_command_int_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint8_t target_system, uint8_t target_component, uint16_t command, uint64_t param1, uint64_t param2, uint64_t param3, uint64_t param4, uint64_t param5, uint64_t param6, uint64_t param7, uint64_t param8)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_uint64_t(buf, 0, param1);
    _mav_put_uint64_t(buf, 8, param2);
    _mav_put_uint64_t(buf, 16, param3);
    _mav_put_uint64_t(buf, 24, param4);
    _mav_put_uint64_t(buf, 32, param5);
    _mav_put_uint64_t(buf, 40, param6);
    _mav_put_uint64_t(buf, 48, param7);
    _mav_put_uint64_t(buf, 56, param8);
    _mav_put_uint16_t(buf, 64, command);
    _mav_put_uint8_t(buf, 66, target_system);
    _mav_put_uint8_t(buf, 67, target_component);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_COMMAND_INT, buf, MAVLINK_MSG_ID_COMMAND_INT_MIN_LEN, MAVLINK_MSG_ID_COMMAND_INT_LEN, MAVLINK_MSG_ID_COMMAND_INT_CRC);
#else
    mavlink_command_int_t *packet = (mavlink_command_int_t *)msgbuf;
    packet->param1 = param1;
    packet->param2 = param2;
    packet->param3 = param3;
    packet->param4 = param4;
    packet->param5 = param5;
    packet->param6 = param6;
    packet->param7 = param7;
    packet->param8 = param8;
    packet->command = command;
    packet->target_system = target_system;
    packet->target_component = target_component;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_COMMAND_INT, (const char *)packet, MAVLINK_MSG_ID_COMMAND_INT_MIN_LEN, MAVLINK_MSG_ID_COMMAND_INT_LEN, MAVLINK_MSG_ID_COMMAND_INT_CRC);
#endif
}
#endif

#endif

// MESSAGE COMMAND_INT UNPACKING


/**
 * @brief Get field target_system from command_int message
 *
 * @return  System ID
 */
static inline uint8_t mavlink_msg_command_int_get_target_system(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  66);
}

/**
 * @brief Get field target_component from command_int message
 *
 * @return  Component ID
 */
static inline uint8_t mavlink_msg_command_int_get_target_component(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  67);
}

/**
 * @brief Get field command from command_int message
 *
 * @return  The scheduled action for the mission item.
 */
static inline uint16_t mavlink_msg_command_int_get_command(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint16_t(msg,  64);
}

/**
 * @brief Get field param1 from command_int message
 *
 * @return  Parameter 1 (for the specific command).
 */
static inline uint64_t mavlink_msg_command_int_get_param1(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint64_t(msg,  0);
}

/**
 * @brief Get field param2 from command_int message
 *
 * @return  Parameter 2 (for the specific command).
 */
static inline uint64_t mavlink_msg_command_int_get_param2(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint64_t(msg,  8);
}

/**
 * @brief Get field param3 from command_int message
 *
 * @return  Parameter 3 (for the specific command).
 */
static inline uint64_t mavlink_msg_command_int_get_param3(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint64_t(msg,  16);
}

/**
 * @brief Get field param4 from command_int message
 *
 * @return  Parameter 4 (for the specific command).
 */
static inline uint64_t mavlink_msg_command_int_get_param4(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint64_t(msg,  24);
}

/**
 * @brief Get field param5 from command_int message
 *
 * @return  Parameter 5 (for the specific command).
 */
static inline uint64_t mavlink_msg_command_int_get_param5(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint64_t(msg,  32);
}

/**
 * @brief Get field param6 from command_int message
 *
 * @return  Parameter 6 (for the specific command).
 */
static inline uint64_t mavlink_msg_command_int_get_param6(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint64_t(msg,  40);
}

/**
 * @brief Get field param7 from command_int message
 *
 * @return  Parameter 7 (for the specific command).
 */
static inline uint64_t mavlink_msg_command_int_get_param7(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint64_t(msg,  48);
}

/**
 * @brief Get field param8 from command_int message
 *
 * @return  Parameter 8 (for the specific command).
 */
static inline uint64_t mavlink_msg_command_int_get_param8(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint64_t(msg,  56);
}

/**
 * @brief Decode a command_int message into a struct
 *
 * @param msg The message to decode
 * @param command_int C-struct to decode the message contents into
 */
static inline void mavlink_msg_command_int_decode(const mavlink_message_t* msg, mavlink_command_int_t* command_int)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    command_int->param1 = mavlink_msg_command_int_get_param1(msg);
    command_int->param2 = mavlink_msg_command_int_get_param2(msg);
    command_int->param3 = mavlink_msg_command_int_get_param3(msg);
    command_int->param4 = mavlink_msg_command_int_get_param4(msg);
    command_int->param5 = mavlink_msg_command_int_get_param5(msg);
    command_int->param6 = mavlink_msg_command_int_get_param6(msg);
    command_int->param7 = mavlink_msg_command_int_get_param7(msg);
    command_int->param8 = mavlink_msg_command_int_get_param8(msg);
    command_int->command = mavlink_msg_command_int_get_command(msg);
    command_int->target_system = mavlink_msg_command_int_get_target_system(msg);
    command_int->target_component = mavlink_msg_command_int_get_target_component(msg);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_COMMAND_INT_LEN? msg->len : MAVLINK_MSG_ID_COMMAND_INT_LEN;
        memset(command_int, 0, MAVLINK_MSG_ID_COMMAND_INT_LEN);
    memcpy(command_int, _MAV_PAYLOAD(msg), len);
#endif
}
