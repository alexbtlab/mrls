#pragma once
// MESSAGE TEXT PACKING

#define MAVLINK_MSG_ID_TEXT 4


typedef struct __mavlink_text_t {
 char text[255]; /*<  MSG TEXT.*/
} mavlink_text_t;

#define MAVLINK_MSG_ID_TEXT_LEN 255
#define MAVLINK_MSG_ID_TEXT_MIN_LEN 255
#define MAVLINK_MSG_ID_4_LEN 255
#define MAVLINK_MSG_ID_4_MIN_LEN 255

#define MAVLINK_MSG_ID_TEXT_CRC 28
#define MAVLINK_MSG_ID_4_CRC 28

#define MAVLINK_MSG_TEXT_FIELD_TEXT_LEN 255

#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_TEXT { \
    4, \
    "TEXT", \
    1, \
    {  { "text", NULL, MAVLINK_TYPE_CHAR, 255, 0, offsetof(mavlink_text_t, text) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_TEXT { \
    "TEXT", \
    1, \
    {  { "text", NULL, MAVLINK_TYPE_CHAR, 255, 0, offsetof(mavlink_text_t, text) }, \
         } \
}
#endif

/**
 * @brief Pack a text message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param text  MSG TEXT.
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_text_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               const char *text)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_TEXT_LEN];

    _mav_put_char_array(buf, 0, text, 255);
        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_TEXT_LEN);
#else
    mavlink_text_t packet;

    mav_array_memcpy(packet.text, text, sizeof(char)*255);
        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_TEXT_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_TEXT;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_TEXT_MIN_LEN, MAVLINK_MSG_ID_TEXT_LEN, MAVLINK_MSG_ID_TEXT_CRC);
}

/**
 * @brief Pack a text message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param text  MSG TEXT.
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_text_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   const char *text)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_TEXT_LEN];

    _mav_put_char_array(buf, 0, text, 255);
        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_TEXT_LEN);
#else
    mavlink_text_t packet;

    mav_array_memcpy(packet.text, text, sizeof(char)*255);
        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_TEXT_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_TEXT;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_TEXT_MIN_LEN, MAVLINK_MSG_ID_TEXT_LEN, MAVLINK_MSG_ID_TEXT_CRC);
}

/**
 * @brief Encode a text struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param text C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_text_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_text_t* text)
{
    return mavlink_msg_text_pack(system_id, component_id, msg, text->text);
}

/**
 * @brief Encode a text struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param text C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_text_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_text_t* text)
{
    return mavlink_msg_text_pack_chan(system_id, component_id, chan, msg, text->text);
}

/**
 * @brief Send a text message
 * @param chan MAVLink channel to send the message
 *
 * @param text  MSG TEXT.
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_text_send(mavlink_channel_t chan, const char *text)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_TEXT_LEN];

    _mav_put_char_array(buf, 0, text, 255);
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_TEXT, buf, MAVLINK_MSG_ID_TEXT_MIN_LEN, MAVLINK_MSG_ID_TEXT_LEN, MAVLINK_MSG_ID_TEXT_CRC);
#else
    mavlink_text_t packet;

    mav_array_memcpy(packet.text, text, sizeof(char)*255);
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_TEXT, (const char *)&packet, MAVLINK_MSG_ID_TEXT_MIN_LEN, MAVLINK_MSG_ID_TEXT_LEN, MAVLINK_MSG_ID_TEXT_CRC);
#endif
}

/**
 * @brief Send a text message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_text_send_struct(mavlink_channel_t chan, const mavlink_text_t* text)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_text_send(chan, text->text);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_TEXT, (const char *)text, MAVLINK_MSG_ID_TEXT_MIN_LEN, MAVLINK_MSG_ID_TEXT_LEN, MAVLINK_MSG_ID_TEXT_CRC);
#endif
}

#if MAVLINK_MSG_ID_TEXT_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_text_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  const char *text)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;

    _mav_put_char_array(buf, 0, text, 255);
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_TEXT, buf, MAVLINK_MSG_ID_TEXT_MIN_LEN, MAVLINK_MSG_ID_TEXT_LEN, MAVLINK_MSG_ID_TEXT_CRC);
#else
    mavlink_text_t *packet = (mavlink_text_t *)msgbuf;

    mav_array_memcpy(packet->text, text, sizeof(char)*255);
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_TEXT, (const char *)packet, MAVLINK_MSG_ID_TEXT_MIN_LEN, MAVLINK_MSG_ID_TEXT_LEN, MAVLINK_MSG_ID_TEXT_CRC);
#endif
}
#endif

#endif

// MESSAGE TEXT UNPACKING


/**
 * @brief Get field text from text message
 *
 * @return  MSG TEXT.
 */
static inline uint16_t mavlink_msg_text_get_text(const mavlink_message_t* msg, char *text)
{
    return _MAV_RETURN_char_array(msg, text, 255,  0);
}

/**
 * @brief Decode a text message into a struct
 *
 * @param msg The message to decode
 * @param text C-struct to decode the message contents into
 */
static inline void mavlink_msg_text_decode(const mavlink_message_t* msg, mavlink_text_t* text)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_text_get_text(msg, text->text);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_TEXT_LEN? msg->len : MAVLINK_MSG_ID_TEXT_LEN;
        memset(text, 0, MAVLINK_MSG_ID_TEXT_LEN);
    memcpy(text, _MAV_PAYLOAD(msg), len);
#endif
}
