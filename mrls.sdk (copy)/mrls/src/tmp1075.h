
#ifndef __TMP1075_H_
#define __TMP1075_H_

	#include "xil_types.h"
	#include "xiicps.h"

	#define TMP1075_TEMP_REG 0
	#define IIC_TEMPPAMP_TMP1075_SLAVE_ADDR		0x48
	#define IIC_READ_SIZE 2
	#define IIC_SCLK_RATE		100000
	#define TEST_BUFFER_SIZE 128
	#define IIC_TEMP_PAMP_DEVICE_ID XPAR_XIICPS_1_DEVICE_ID

	extern XIicPs Iic_Temp_PAMP;

	void TMP1075_setCONG_REG_RESOLUTION_12BIT(  );

#endif //  __TMP1075_H_

