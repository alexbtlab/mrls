#ifndef __INITMRLS_H_
#define __INITMRLS_H_

	#include "platform_config.h"
	#include "xparameters.h"
	#include "xuartps.h"
	#include "xscugic.h"
	#include "uartForDownPart.h"
	#include "compass.h"
	#include "xadc.h"
	#include "mrls_udp.h"
	#include "HMC769.h"
	#include "tmp100.h"
	#include "tmp1075.h"
	#include "mavLinkMRLS.h"


	#define MAX_PKT_LEN				8192*sizeof(uint32_t)
	#define RX_BUFFER_BASE4			(XPAR_PS7_DDR_0_S_AXI_BASEADDR + 0x00400000)
	#define UART_INT_IRQ_ID			XPAR_XUARTPS_1_INTR
	#define PWDN_RESET 				Xil_Out32(XPAR_AD9650_0_S00_AXI_BASEADDR + 8 , 0 << 1);
//	#define SWEEP_VAL(val) 			Xil_Out32(XPAR_HMC769_0_S00_AXI_BASEADDR + 24 , val);
	#define NUM_AMP_RECIVER 1
	#define ATTEN_VAL 4
	#define SYSMON_DEVICE_ID 		XPAR_SYSMON_0_DEVICE_ID
	#define RESET_TIMEOUT_COUNTER	10000

	#define EN_LO_AMP (1 << 3)
	#define DIS_LO_AMP (0 << 3)

	#define MAX_AMOUNT_AZIMUTH_FOR_SPEED_1_25_SEC 571
	#define MAX_AMOUNT_AZIMUTH_FOR_SPEED_2_5_SEC 284
	#define MAX_AMOUNT_AZIMUTH_FOR_SPEED_5_SEC 1

	#define AVERAG_VAL4       4
	#define AVERAG_VAL8       8
	#define AVERAG_VAL16      16
	#define SHIFT_DIVIDE_VAL1 1
	#define SHIFT_DIVIDE_VAL2 2
	#define SHIFT_DIVIDE_VAL3 3


//	#define MAX_PKT_LEN		8192*sizeof(uint32_t)
//	#define RX_BUFFER_BASE4			(XPAR_PS7_DDR_0_S_AXI_BASEADDR + 0x00400000)
//

#define NUM_REGISTERS_HMC769 16
typedef struct sHMC769_Reg *tHMC769_Reg;
typedef void (* tConfFunc)( tFrequencyBandsHMC frequencyBands, tHMC769_Reg reg ) ;





struct sHMC769_Reg {
	char *psNameRegHMC;
	uint8_t num;
	uint32_t valueFor120MHz;
	uint32_t valueCalculated;
};
struct sHMC769 {
	tFrequencyBandsHMC currentfrequencyBands;
	tHMC769_Reg listHMC769_Reg;
	tConfFunc pfConfigFunc;
};

	extern int32_t raw[2048];
	extern int32_t* praw;
	extern volatile size_t g_shiftFront;
	extern struct netif *echo_netif;
	extern bool mem_empty;
	extern bool  g_stateTriger;
	extern bool enable_transmit_RAWdata_to_PC;
//
	void MRLS2D_Init();
	void MRLS2D_app();
	void MRLS2D_TelemetryRead();
	void MRLS2D_Net();
	void MRLS2D_Process();
	void MRLS2D_ampEn(u8 num_amp_in_receiver);
//	void MRLS2D_delay(uint64_t delayVal);
	void MRLS2D_trigerState( bool state );
	void HMC769_configIC2( tFrequencyBandsHMC frequencyBands );
	void setRegHMC769( tFrequencyBandsHMC frequencyBands, tHMC769_Reg reg );
	void HMC769_CalculateParam(uint16_t Fstart, uint16_t Fstop, uint16_t Tramp);
	void AD9650_setAveragVal(uint8_t averagVal);
	float TMP1075_ReadTemPAMP(  );

#endif /* __DEFINITION_H_ */
