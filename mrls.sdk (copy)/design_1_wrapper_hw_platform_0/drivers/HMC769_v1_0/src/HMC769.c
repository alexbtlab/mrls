 #include "HMC769.h"

 volatile size_t g_shiftFront = 1000;
 bool  g_stateTriger = false;


 static void HMC769_startReceive();
 static void HMC769_waitReadySet();
 static void HMC769_waitReadyReset();
 static void HMC769_startSend();
 static void HMC769_setRegAdr(uint8_t adr);
 static void HMC769_setSendSata(uint32_t dataSend);

 void HMC769_init(){

     xil_printf("INFO: Start init AXI_HMC769--->\r\n");

     if (HMC769_read(HMC769_ID_REG) == 0x97370)
         xil_printf("INFO: init AXI_HMC769 completed \r\n");
     else{
         xil_printf("ERROR: init AXI_HMC769 is fault\r\n");
         if (HMC769_read(HMC769_ID_REG) != 0x97370)
        	 while(1);
         else
        	 xil_printf("INFO: repeat init AXI_HMC769 is completed\r\n");
     }
     HMC769_write(HMC769_REFDIV_REG, 0x4);

     if ( HMC769_read(HMC769_REFDIV_REG) == 0x4)
         xil_printf("INFO: compare value is completed\r\n");
     else
         xil_printf("ERROR: compare value is fault\r\n");
 }
 void HMC769_startTrig(bool on){

     volatile unsigned int* hmcbaseaddr = (unsigned int*)XPAR_HMC769_0_S00_AXI_BASEADDR;
     (*hmcbaseaddr) = on ? 0x4 : 0;
 }
 void HMC769_setAtten(uint8_t val){

 	volatile unsigned int* hmcbaseaddr = (unsigned int*)XPAR_HMC769_0_S00_AXI_BASEADDR;
 	volatile unsigned int* slv_reg4 = hmcbaseaddr + 4;

 	*slv_reg4 = val;
 }
 uint32_t HMC769_getData(){

     volatile unsigned int* hmcbaseaddr = (unsigned int*)XPAR_HMC769_0_S00_AXI_BASEADDR;
     volatile unsigned int* ip2mb_reg1 = hmcbaseaddr + 9;
     return *ip2mb_reg1;
 }
 void HMC769_write(uint8_t adrRegHMC, uint32_t dataSend){

     HMC769_waitReadyReset();
     HMC769_setRegAdr(adrRegHMC);
     HMC769_setSendSata(dataSend);
     HMC769_waitReadyReset();
     HMC769_startSend();
     HMC769_waitReadyReset();
     HMC769_waitReadySet();
     HMC769_waitReadyReset();
 }
 uint32_t HMC769_read(uint8_t adrRegHMC){

     HMC769_waitReadyReset();
     HMC769_setRegAdr(adrRegHMC);
     HMC769_waitReadyReset();
     HMC769_startReceive();
     HMC769_waitReadyReset();
     HMC769_waitReadySet();

     return HMC769_getData();
 }
 void HMC769_PWR_EN(uint8_t statePower){

     volatile unsigned int* hmcbaseaddr = (unsigned int*)XPAR_HMC769_0_S00_AXI_BASEADDR;
     volatile unsigned int* slv_reg5 = hmcbaseaddr + 5;
     *slv_reg5 = statePower;
 }
 void HMC769_setSweepVal2(uint32_t val){

 	volatile unsigned int* hmcbaseaddr = (unsigned int*)XPAR_HMC769_0_S00_AXI_BASEADDR; // Базовый адрес регистра IP ядра МС HMC769
 	volatile unsigned int* slv_reg7 = hmcbaseaddr + 7; // Адрес 1-го регистра для передачи данных в IP ядро из MB

 	*slv_reg7 = val; // Запись адреса регистра МС HMC769, в регистр IP ядра (slv_reg2 - адреса регистра МС для приема)
 }
 void HMC769_readBitMap(uint8_t adrRegHMC, void* rxReg){

    u32 dataReg;
    rxReg = &dataReg; // Указываем регистр dataReg принятых данных от МС, на необходимую структуру (void*)(созданную во время входа в функцию)
                      // т.к. на каждый регистр принятых данных от МС есть своя структура

    dataReg = HMC769_read(adrRegHMC); // Чтение данных по адресу u8 adrRegH из МС HMC769
    xil_printf("\r\nHMC769 ADR_REG:%x DATA_REG:%x\r\n", adrRegHMC, *(u32*)rxReg);

    switch (adrRegHMC) {
    case HMC769_ID_REG:
        xil_printf("chip_ID-%x\r\n", ((t_HMC769_ID_REG*)rxReg)->chip_ID);
        break;
    case HMC769_RST_Register_REG:
        xil_printf("EnFromSPI-%x\r\n", ((t_HMC769_RST_Register_REG*)rxReg)->EnFromSPI);
        xil_printf("EnKeepOns-%x\r\n", ((t_HMC769_RST_Register_REG*)rxReg)->EnKeepOns);
        xil_printf("EnPinSel-%x\r\n", ((t_HMC769_RST_Register_REG*)rxReg)->EnPinSel);
        xil_printf("EnSyncChpDis-%x\r\n", ((t_HMC769_RST_Register_REG*)rxReg)->EnSyncChpDis);
        break;
    case HMC769_REFDIV_REG:
        xil_printf("rdiv:%x\r\n", ((t_HMC769_REFDIV_REG*)rxReg)->rdiv);
        break;
    case HMC769_Frequency_Register_REG:
        xil_printf("intg-%x\r\n", ((t_HMC769_Frequency_Register_REG*)rxReg)->intg);
        break;
    case HMC769_Frequency_Register_Fractional_Part_REG:
        xil_printf("frac-%x\r\n", ((t_HMC769_Frequency_Register_Fractional_Part_REG*)rxReg)->frac);
        break;
    case HMC769_Seed_REG:
        xil_printf("SEED:%x\r\n", ((t_HMC769_Seed_REG*)rxReg)->SEED);
        break;
    case HMC769_SD_CFG_REG:
        xil_printf("autoseed:%x\r\n", ((t_HMC769_SD_CFG_REG*)rxReg)->autoseed);
        xil_printf("BIST_Enable:%x\r\n", ((t_HMC769_SD_CFG_REG*)rxReg)->BIST_Enable);
        xil_printf("Disable_Reset:%x\r\n", ((t_HMC769_SD_CFG_REG*)rxReg)->Disable_Reset);
        xil_printf("DSM_Clock_Source:%x\r\n", ((t_HMC769_SD_CFG_REG*)rxReg)->DSM_Clock_Source);
        xil_printf("External_Trigger_Enable:%x\r\n", ((t_HMC769_SD_CFG_REG*)rxReg)->External_Trigger_Enable);
        xil_printf("Force_DSM_Clock_n:%x\r\n", ((t_HMC769_SD_CFG_REG*)rxReg)->Force_DSM_Clock_n);
        xil_printf("Force_RDIV_bypass:%x\r\n", ((t_HMC769_SD_CFG_REG*)rxReg)->Force_RDIV_bypass);
        xil_printf("Invert_DSM_Clock:%x\r\n", ((t_HMC769_SD_CFG_REG*)rxReg)->Invert_DSM_Clock);
        xil_printf("Modulator_Type:%x\r\n", ((t_HMC769_SD_CFG_REG*)rxReg)->Modulator_Type);
        xil_printf("Number_of_Bist_Cycles:%x\r\n", ((t_HMC769_SD_CFG_REG*)rxReg)->Number_of_Bist_Cycles);
        xil_printf("Reserved_0:%x\r\n", ((t_HMC769_SD_CFG_REG*)rxReg)->Reserved_0);
        xil_printf("Reserved_1:%x\r\n", ((t_HMC769_SD_CFG_REG*)rxReg)->Reserved_1);
        xil_printf("Reserved_7:%x\r\n", ((t_HMC769_SD_CFG_REG*)rxReg)->Reserved_7);
        xil_printf("SD_Mode:%x\r\n", ((t_HMC769_SD_CFG_REG*)rxReg)->SD_Mode);
        xil_printf("Single_Step_Ramp_Mode:%x\r\n", ((t_HMC769_SD_CFG_REG*)rxReg)->Single_Step_Ramp_Mode);
        break;
    case HMC769_Lock_Detect_REG:
        xil_printf("Cycle_Slip_Prevention_Enable:%x\r\n", ((t_HMC769_Lock_Detect_REG*)rxReg)->Cycle_Slip_Prevention_Enable);
        xil_printf("LKDCounts:%x\r\n", ((t_HMC769_Lock_Detect_REG*)rxReg)->LKDCounts);
        xil_printf("Lock_Detect_Timer_Enable:%x\r\n", ((t_HMC769_Lock_Detect_REG*)rxReg)->Lock_Detect_Timer_Enable);
        xil_printf("LockDetect_Counters_Enable:%x\r\n", ((t_HMC769_Lock_Detect_REG*)rxReg)->LockDetect_Counters_Enable);
        xil_printf("Train_Lock_Detect_Timer:%x\r\n", ((t_HMC769_Lock_Detect_REG*)rxReg)->Train_Lock_Detect_Timer);
        break;
    case HMC769_Analog_EN_REG:
        xil_printf("EnBias:%x\r\n", ((t_HMC769_Analog_EN_REG*)rxReg)->EnBias);
        xil_printf("EnCP:%x\r\n", ((t_HMC769_Analog_EN_REG*)rxReg)->EnCP);
        xil_printf("EnMcnt:%x\r\n", ((t_HMC769_Analog_EN_REG*)rxReg)->EnMcnt);
        xil_printf("EnOpAmp:%x\r\n", ((t_HMC769_Analog_EN_REG*)rxReg)->EnOpAmp);
        xil_printf("EnPFD:%x\r\n", ((t_HMC769_Analog_EN_REG*)rxReg)->EnPFD);
        xil_printf("EnPS:%x\r\n", ((t_HMC769_Analog_EN_REG*)rxReg)->EnPS);
        xil_printf("EnVCO:%x\r\n", ((t_HMC769_Analog_EN_REG*)rxReg)->EnVCO);
        xil_printf("EnVCOBias:%x\r\n", ((t_HMC769_Analog_EN_REG*)rxReg)->EnVCOBias);
        xil_printf("EnXtal:%x\r\n", ((t_HMC769_Analog_EN_REG*)rxReg)->EnXtal);
        xil_printf("RFDiv2Sel:%x\r\n", ((t_HMC769_Analog_EN_REG*)rxReg)->RFDiv2Sel);
        xil_printf("VCOBWSel:%x\r\n", ((t_HMC769_Analog_EN_REG*)rxReg)->VCOBWSel);
        xil_printf("VCOOutBiasA:%x\r\n", ((t_HMC769_Analog_EN_REG*)rxReg)->VCOOutBiasA);
        xil_printf("VCOOutBiasB:%x\r\n", ((t_HMC769_Analog_EN_REG*)rxReg)->VCOOutBiasB);
        xil_printf("XtalDisSat:%x\r\n", ((t_HMC769_Analog_EN_REG*)rxReg)->XtalDisSat);
        xil_printf("XtalLowGain:%x\r\n", ((t_HMC769_Analog_EN_REG*)rxReg)->XtalLowGain);
        break;
    case HMC769_Charge_Pump_REG:
        xil_printf("CPHiK:%x\r\n", ((t_HMC769_Charge_Pump_REG*)rxReg)->CPHiK);
        xil_printf("CPIdn:%x\r\n", ((t_HMC769_Charge_Pump_REG*)rxReg)->CPIdn);
        xil_printf("CPIup:%x\r\n", ((t_HMC769_Charge_Pump_REG*)rxReg)->CPIup);
        xil_printf("CPOffset:%x\r\n", ((t_HMC769_Charge_Pump_REG*)rxReg)->CPOffset);
        xil_printf("CPSnkEn:%x\r\n", ((t_HMC769_Charge_Pump_REG*)rxReg)->CPSnkEn);
        xil_printf("CPSrcEn:%x\r\n", ((t_HMC769_Charge_Pump_REG*)rxReg)->CPSrcEn);
        break;
    case HMC769_Modulation_Step_REG:
        xil_printf("MODSTEP:%x\r\n", ((t_HMC769_Modulation_Step_REG*)rxReg)->MODSTEP);
        break;
    case HMC769_PD_REG:
        xil_printf("LKDProcTesttoCP:%x\r\n", ((t_HMC769_PD_REG*)rxReg)->LKDProcTesttoCP);
        xil_printf("McntClkGateSel:%x\r\n", ((t_HMC769_PD_REG*)rxReg)->McntClkGateSel);
        xil_printf("PFDDly:%x\r\n", ((t_HMC769_PD_REG*)rxReg)->PFDDly);
        xil_printf("PFDDnEN:%x\r\n", ((t_HMC769_PD_REG*)rxReg)->PFDDnEN);
        xil_printf("PFDForceDn:%x\r\n", ((t_HMC769_PD_REG*)rxReg)->PFDForceDn);
        xil_printf("PFDForceMid:%x\r\n", ((t_HMC769_PD_REG*)rxReg)->PFDForceMid);
        xil_printf("PFDForceUp:%x\r\n", ((t_HMC769_PD_REG*)rxReg)->PFDForceUp);
        xil_printf("PFDInv:%x\r\n", ((t_HMC769_PD_REG*)rxReg)->PFDInv);
        xil_printf("PFDShort:%x\r\n", ((t_HMC769_PD_REG*)rxReg)->PFDShort);
        xil_printf("PFDUpEn:%x\r\n", ((t_HMC769_PD_REG*)rxReg)->PFDUpEn);
        xil_printf("PSBiasSel:%x\r\n", ((t_HMC769_PD_REG*)rxReg)->PSBiasSel);
        xil_printf("VDIVExt:%x\r\n", ((t_HMC769_PD_REG*)rxReg)->VDIVExt);
        break;
    case HMC769_ALTINT_REG:
        xil_printf("ALTINT:%x\r\n", ((t_HMC769_ALTINT_REG*)rxReg)->ALTINT);
        break;
    case HMC769_ALTFRAC_REG:
        xil_printf("ALTFRAC:%x\r\n", ((t_HMC769_ALTFRAC_REG*)rxReg)->ALTFRAC);
        break;
    case HMC769_SPI_TRIG_REG:
        xil_printf("SPITRIG:%x\r\n", ((t_HMC769_SPI_TRIG_REG*)rxReg)->SPITRIG);
        break;
    case HMC769_GPO_REG:
        xil_printf("GPOAlways:%x\r\n", ((t_HMC769_GPO_REG*)rxReg)->GPOAlways);
        xil_printf("GPOOn:%x\r\n", ((t_HMC769_GPO_REG*)rxReg)->GPOOn);
        xil_printf("GPOPullDnDis:%x\r\n", ((t_HMC769_GPO_REG*)rxReg)->GPOPullDnDis);
        xil_printf("GPOPullUpDis:%x\r\n", ((t_HMC769_GPO_REG*)rxReg)->GPOPullUpDis);
        xil_printf("GPOPullUpDis:%x\r\n", ((t_HMC769_GPO_REG*)rxReg)->GPOPullUpDis);
        xil_printf("GPOTest:%x\r\n", ((t_HMC769_GPO_REG*)rxReg)->GPOTest);
        break;
    case HMC769_GPO2_REG:
        xil_printf("GPO:%x\r\n", ((t_HMC769_GPO2_REG*)rxReg)->GPO);
        xil_printf("Lock_Detect:%x\r\n", ((t_HMC769_GPO2_REG*)rxReg)->Lock_Detect);
        xil_printf("Ramp_Busy:%x\r\n", ((t_HMC769_GPO2_REG*)rxReg)->Ramp_Busy);
        break;
    case HMC769_BIST_REG:
        xil_printf("BIST_Busy:%x\r\n", ((t_HMC769_BIST_REG*)rxReg)->BIST_Busy);
        xil_printf("BIST_Signature:%x\r\n", ((t_HMC769_BIST_REG*)rxReg)->BIST_Signature);
        break;
    case HMC769_Lock_Detect_Timer_Status_REG:
        xil_printf("LkdSpeed:%x\r\n", ((t_HMC769_Lock_Detect_Timer_Status_REG*)rxReg)->LkdSpeed);
        xil_printf("LkdTraining:%x\r\n", ((t_HMC769_Lock_Detect_Timer_Status_REG*)rxReg)->LkdTraining);
        break;

    default:
        xil_printf("ERROR: no register with the given address u8 adrRegHMC\r\n");
        break;
    }
 }
 void HMC769_configIC(tFrequencyBandsHMC config){

 	switch(config){
 		case config_HMC_MAIN120 :	// 970 60 MHz
 		    HMC769_write(0x01, 0x000002);
 		    HMC769_write(0x02, 0x000001);
 		    HMC769_write(0x03, 0x00001D);
 		    HMC769_write(0x04, 1048576);
 		    HMC769_write(0x05, 0x000000);
 		    HMC769_write(0x07, 0x204865);
 		    HMC769_write(0x08, 0x036FFF);
 		    HMC769_write(0x09, 0x003264);
 			HMC769_write(0x0A, 81);
 			HMC769_write(0x0B, 0x01E071);
 			HMC769_write(0x0C, 0x00001D);
 			HMC769_write(0x0D, 7334176);
 			HMC769_write(0x0E, 0x000000);
 			HMC769_write(0x0F, 0x000001);
 			HMC769_write(0x06, 0x001FBF);
 			break;
 		case config_HMC_CUSTOM:	// 970 120 MHz
 		    HMC769_write(0x01, 0x000002);
 		    HMC769_write(0x02, 0x000001);
 		    HMC769_write(0x03, 0x00001D);
 		    HMC769_write(0x04, 1048576);
 		    HMC769_write(0x05, 0x000000);
 		    HMC769_write(0x07, 0x204865);
 		    HMC769_write(0x08, 0x036FFF);
 		    HMC769_write(0x09, 0x003264);
 			HMC769_write(0x0A, 81);
 			HMC769_write(0x0B, 0x01E071);
 			HMC769_write(0x0C, 0x00001D);
 			HMC769_write(0x0D, 7334176);
 			HMC769_write(0x0E, 0x000000);
 			HMC769_write(0x0F, 0x000001);
 			HMC769_write(0x06, 0x001FBF);
 			break;
 		default : break;
 	}
     xil_printf("INFO: HMC769 init completed \r\n");
 }
 void HMC769_setShiftFront(uint16_t val){

 	g_shiftFront = val;

 	int slv_reg7 = ( g_shiftFront  << 1 ) | g_stateTriger;
 	Xil_Out32(XPAR_HMC769_0_S00_AXI_BASEADDR + 7*4, slv_reg7);

 }
 void HMC769_viewAllDataPLL(){

    static t_HMC769_ID_REG* idReg;
    static t_HMC769_RST_Register_REG* rstReg;
    static t_HMC769_REFDIV_REG* refdivReg;
    static t_HMC769_Frequency_Register_REG* freqReg;
    static t_HMC769_Frequency_Register_Fractional_Part_REG* freqFracReg;
    static t_HMC769_Seed_REG* seedReg;
    static t_HMC769_SD_CFG_REG* sdCfgReg;
    static t_HMC769_Lock_Detect_REG* lockDetReg;
    static t_HMC769_Analog_EN_REG* AnEnReg;
    static t_HMC769_Charge_Pump_REG* ChargePumpReg;
    static t_HMC769_Modulation_Step_REG* modStepReg;
    static t_HMC769_PD_REG* pdReg;
    static t_HMC769_ALTINT_REG* altintReg;
    static t_HMC769_ALTFRAC_REG* altFracReg;
    static t_HMC769_SPI_TRIG_REG* spiTrigReg;
    static t_HMC769_GPO_REG* gpoReg;
    static t_HMC769_GPO2_REG* gpo2Reg;
    static t_HMC769_BIST_REG* bistReg;
    static t_HMC769_Lock_Detect_Timer_Status_REG* lockDetTimStatReg;

    HMC769_readBitMap(HMC769_ID_REG, idReg);
    HMC769_readBitMap(HMC769_RST_Register_REG, rstReg);
    HMC769_readBitMap(HMC769_REFDIV_REG, refdivReg);
    HMC769_readBitMap(HMC769_Frequency_Register_REG, freqReg);
    HMC769_readBitMap(HMC769_Frequency_Register_Fractional_Part_REG, freqFracReg);
    HMC769_readBitMap(HMC769_Seed_REG, seedReg);
    HMC769_readBitMap(HMC769_SD_CFG_REG, sdCfgReg);
    HMC769_readBitMap(HMC769_Lock_Detect_REG, lockDetReg);
    HMC769_readBitMap(HMC769_Analog_EN_REG, AnEnReg);
    HMC769_readBitMap(HMC769_Charge_Pump_REG, ChargePumpReg);
    HMC769_readBitMap(HMC769_Modulation_Step_REG, modStepReg);
    HMC769_readBitMap(HMC769_PD_REG, pdReg);
    HMC769_readBitMap(HMC769_ALTINT_REG, altintReg);
    HMC769_readBitMap(HMC769_ALTFRAC_REG, altFracReg);
    HMC769_readBitMap(HMC769_SPI_TRIG_REG, spiTrigReg);
    HMC769_readBitMap(HMC769_GPO_REG, gpoReg);
    HMC769_readBitMap(HMC769_GPO2_REG, gpo2Reg);
    HMC769_readBitMap(HMC769_BIST_REG, bistReg);
    HMC769_readBitMap(HMC769_Lock_Detect_Timer_Status_REG, lockDetTimStatReg);
 }
 static void HMC769_startReceive(){

     volatile unsigned int* hmcbaseaddr = (unsigned int*)XPAR_HMC769_0_S00_AXI_BASEADDR;
     (*hmcbaseaddr) = 0x2;
     for (uint32_t i = 0; i < 20000; i++) {    }
     (*hmcbaseaddr) = 0x0;
 }
 static void HMC769_waitReadyReset(){
     
     uint64_t timeOutReset = 0;
     volatile unsigned int* hmcbaseaddr = (unsigned int*)XPAR_HMC769_0_S00_AXI_BASEADDR;
     volatile unsigned int* ip2mb_reg0 = hmcbaseaddr + 8;
     
     while ((*ip2mb_reg0) != 0 ) {
     	if(timeOutReset == 1000000000){
     		xil_printf("ERROR: HMC769 config error HMC769_waitReadyReset!\r\n");
     		return;
     	} 
     	timeOutReset++;
     }
 }
 static void HMC769_waitReadySet(){

     uint64_t timeOutSet = 0;
     volatile unsigned int* hmcbaseaddr = (unsigned int*)XPAR_HMC769_0_S00_AXI_BASEADDR;
     volatile unsigned int* ip2mb_reg0 = hmcbaseaddr + 8;

     while ((*ip2mb_reg0) != 1 ) {
     	if(timeOutSet == 1000000000){
     		xil_printf("ERROR: HMC769 config error HMC769_waitReadySet!\r\n");
     		return;
     	}   
     	timeOutSet++;
     }
 }
 static void HMC769_startSend(){

     volatile unsigned int* hmcbaseaddr = (unsigned int*)XPAR_HMC769_0_S00_AXI_BASEADDR;
     (*hmcbaseaddr) = 0x1;
     for (uint32_t i = 0; i < 20000; i++) {    }
     (*hmcbaseaddr) = 0x0;
 }
 static void HMC769_setRegAdr(uint8_t adr){

     volatile unsigned int* hmcbaseaddr = (unsigned int*)XPAR_HMC769_0_S00_AXI_BASEADDR;
     volatile unsigned int* slv_reg1 = hmcbaseaddr + 1;
     volatile unsigned int* slv_reg2 = hmcbaseaddr + 2;

     *slv_reg1 = adr;
     *slv_reg2 = adr;
 }
 static void HMC769_setSendSata(uint32_t dataSend){

     volatile unsigned int* hmcbaseaddr = (unsigned int*)XPAR_HMC769_0_S00_AXI_BASEADDR;
     volatile unsigned int* slv_reg3 = hmcbaseaddr + 3;
     *slv_reg3 = dataSend;
 }
