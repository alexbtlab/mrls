`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03/30/2022 01:24:47 PM
// Design Name: 
// Module Name: tb_DNA
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_DNA(

    );
    
    wire [63:0] outID_0;
    wire  ready_0;
    reg  s00_axi_aclk_0;
    reg  s00_axi_aresetn_0;
    
  always begin
    #(10) s00_axi_aclk_0 = ~s00_axi_aclk_0;
  end
  
    initial begin
    
        s00_axi_aclk_0 = 0;
        s00_axi_aresetn_0 = 0;
        #100;
        s00_axi_aresetn_0 = 1;
    end
  
   
    design_2_wrapper design_2_wrapper
   (
   outID_0,
    ready_0,
    s00_axi_aclk_0,
    s00_axi_aresetn_0)
    ;

  
endmodule


