`timescale 1ns / 1ps

import axi_vip_pkg::*;
import design_2_axi_vip_0_0_pkg::*;

module tb_mrls(  );
    
//  wire [15:0]frameSize;
//  wire [31:0]fft_data_out;
//  wire fft_tlast;
//  wire fft_tvalid;
  
  reg m00_axis_aclk;
  reg s00_axis_aresetn;
//  reg [15:0] sweep_val_ext;
//  reg ext_load_sweep_val;
  reg clk_10MHz;
//  reg aclk_10MHz;
 reg [31:0]fifo_in_tdata;
 
//  reg reset;
//  reg [15:0] tguard_time;
//  reg pll_trig_reg;
//  reg START_trig;
//  reg [15:0] sweep_val;
//  reg [15:0] shift_front;
//  reg start_adc_count_r;
  reg azimut_0;
  
  design_2_axi_vip_0_0_mst_t      master_agent;

initial begin
    //Create an agent
    master_agent = new("master vip agent", design_2_wrapper.design_2_i.axi_vip_0.inst.IF);
  
    // set tag for agents for easy debug
    master_agent.set_agent_tag("Master VIP");
  
    // set print out verbosity level.
    master_agent.set_verbosity(400);
  
    //Start the agent
    master_agent.start_master();
  
    #50ns;
end
/*-------------------------------------------------------------------------------------------*/      
`define ADR_HMC769  32'h44A00000
`define ADR_AVERFFT 32'h44A10000
`define ADR_AD9650  32'h44A20000

xil_axi_prot_t  prot = 1;
bit [31:0]     sweepVal=9900,
               slvReg7 = ( 1000 << 1 | 1 );
bit [31:0]     amountOfAzimuthFromCpu = 2;
bit [31:0]     averagVal      = 4;
bit [31:0]     shiftDivideVal = 2;

xil_axi_resp_t     resp;

      initial begin
      
            master_agent.AXI4LITE_WRITE_BURST(`ADR_HMC769 + 24, prot, sweepVal, resp);                                      // sweepVal
            #100;
            master_agent.AXI4LITE_WRITE_BURST(`ADR_HMC769 + 28, prot, slvReg7, resp);                                       // sweepVal
            #100;
            master_agent.AXI4LITE_WRITE_BURST(`ADR_AD9650 + 12, prot, (averagVal << 16) | amountOfAzimuthFromCpu, resp);    // amountOfAzimuth
            #100;
            master_agent.AXI4LITE_WRITE_BURST(`ADR_AD9650 + 12, prot, (averagVal << 16) | amountOfAzimuthFromCpu, resp); 
            #100;
            master_agent.AXI4LITE_WRITE_BURST(`ADR_AVERFFT,     prot, averagVal, resp);   
            #100;
            master_agent.AXI4LITE_WRITE_BURST(`ADR_AVERFFT,     prot, averagVal, resp);   
            #100;
            master_agent.AXI4LITE_WRITE_BURST(`ADR_AVERFFT,     prot, averagVal, resp);                                     // averagVal
            #100;
            master_agent.AXI4LITE_WRITE_BURST(`ADR_AVERFFT,     prot, averagVal, resp);                                     // averagVal
            #100;
            master_agent.AXI4LITE_WRITE_BURST(`ADR_AVERFFT + 4, prot, shiftDivideVal, resp);                                // shiftDivideVal
      
            for (int i=0; i<1000; i++ ) begin
                azimut_0 = 1;
                #10000;
                azimut_0 = 0;
                #12000000;///#55000000;       // #12000000; for 4aver
            end
     end
    
    initial begin        
//        shift_front = 1000;
//        sweep_val = 9900;
//        reset = 0;
//        sweep_val_ext = 14000;
//        ext_load_sweep_val = 1;
        m00_axis_aclk = 0; 
        clk_10MHz = 0;
//        aclk_10MHz = 1;
        s00_axis_aresetn   = 0;
        #100;
        s00_axis_aresetn = 1;       
        #14000; 
                
    end

  
  
  always begin
        #(5) m00_axis_aclk = ~m00_axis_aclk;
    end
    always begin
        #(50) clk_10MHz = ~clk_10MHz;
    end
    always begin
        #2;
//        #(50) aclk_10MHz = ~aclk_10MHz;
    end
    

    
    reg [15:0] data_to_transmit = 0;
    
    always @ (posedge clk_10MHz) begin
//       data_to_transmit <= data_to_transmit + 4000;
       data_to_transmit <=  4000;
       fifo_in_tdata <= data_to_transmit;
    end

    wire frame_even;
    wire start_adc_count;
//    assign start_adc_count = start_adc_count_r;

    wire S00_AXI_araddr;
    wire [2:0] S00_AXI_arprot;
    wire S00_AXI_arready;
    wire S00_AXI_arvalid;
    wire [31:0] S00_AXI_awaddr;
    wire [2:0] S00_AXI_awprot;
    wire S00_AXI_awready;
    wire S00_AXI_awvalid;
    wire S00_AXI_bready;
    wire [1:0] S00_AXI_bresp;
    wire S00_AXI_bvalid;
    wire [31:0] S00_AXI_rdata;
    wire S00_AXI_rready;
    wire [1:0] S00_AXI_rresp;
    wire S00_AXI_rvalid;
    wire S00_AXI_wdata;
    wire S00_AXI_wready;
    wire [3:0] S00_AXI_wstrb;
    wire S00_AXI_wvalid;
    
    wire [3:0] AXI_AWCACHE;
    wire [3:0] AXI_ARCACHE;
       

      
    design_2_wrapper design_2_wrapper(
    
//    .S00_AXI_araddr     (S00_AXI_araddr),
//    .S00_AXI_arprot     (S00_AXI_arprot),
//    .S00_AXI_arready    (S00_AXI_arready),
//    .S00_AXI_arvalid    (S00_AXI_arvalid),
//    .S00_AXI_awaddr (S00_AXI_awaddr),
//    .S00_AXI_awprot(S00_AXI_awprot),
//    .S00_AXI_awready(S00_AXI_awready),
//    .S00_AXI_awvalid(S00_AXI_awvalid),
//    .S00_AXI_bready(S00_AXI_bready),
//    .S00_AXI_bresp(S00_AXI_bresp),
//    .S00_AXI_bvalid(S00_AXI_bvalid),
//    .S00_AXI_rdata(S00_AXI_rdata),
//    .S00_AXI_rready(S00_AXI_rready),
//    .S00_AXI_rresp(S00_AXI_rresp),
//    .S00_AXI_rvalid(S00_AXI_rvalid),
//    .S00_AXI_wdata(S00_AXI_wdata),
//    .S00_AXI_wready(S00_AXI_wready),
//    .S00_AXI_wstrb(S00_AXI_wstrb),
//    .S00_AXI_wvalid(S00_AXI_wvalid),
    
    
        .clk_10MHz(clk_10MHz),
//        .aclk_10MHz(aclk_10MHz),
//        .sweep_val_ext(sweep_val_ext),
//        .fft_data_out(fft_data_out),
//        .fft_tlast(fft_tlast),
//        .fft_tvalid(fft_tvalid),
        .fifo_in_tdata(fifo_in_tdata),
//        .ext_load_sweep_val(ext_load_sweep_val),
//        .frameSize(8192),
        .m00_axis_aclk(m00_axis_aclk),
        .s00_axis_aresetn(s00_axis_aresetn),
//        .allowed_clk(start_adc_count),
//         .mux_fft(frame_even),
         
         .azimut_0(azimut_0)
    
    );
   
   
    

  
 reg [15:0] frame_count;
 assign frame_even = frame_count[0];
 reg [15:0] trig_tsweep_counter;
 reg [15:0] trig_tguard_counter;
 
 
//always @(posedge clk_10MHz) begin

//    if( !reset | frame_count == 16)            frame_count <= 0;
//    if( trig_tguard_counter == tguard_time )   frame_count <= frame_count + 1;
        
////    if(START_trig) begin
//    if(START_trig) begin
//                if ((trig_tsweep_counter < sweep_val)&(trig_tguard_counter==0))
//                begin 
//                    trig_tsweep_counter <= trig_tsweep_counter + 1;
//                    pll_trig_reg <= 0;
//                end
//                else if ((trig_tsweep_counter == sweep_val)&(trig_tguard_counter==0))
//                begin
//                    trig_tsweep_counter <= 0;
//                    pll_trig_reg <= 1;
//                    trig_tguard_counter<=1;                        
//                end
//                else if ((trig_tsweep_counter==0)&(trig_tguard_counter>0)&(trig_tguard_counter < tguard_time))
//                begin
//                    trig_tsweep_counter <= 0;
//                    pll_trig_reg <= 0;
//                    trig_tguard_counter<=trig_tguard_counter+1;                        
//                end
//                else if ((trig_tsweep_counter==0)&(trig_tguard_counter == tguard_time))
//                begin
//                    trig_tsweep_counter <= 0;
//                    pll_trig_reg <= 1;
//                    trig_tguard_counter<=0;                        
//                end
                

//    end
//end



//always @(posedge clk_10MHz)  begin
       
////    if((trig_tsweep_counter < sweep_val) & (trig_tsweep_counter > shift_front))
////        start_adc_count_r <= 1;
////    else
////        start_adc_count_r <= 0; 
//      if( reset & START_trig) begin          
//            if( (trig_tsweep_counter == shift_front ) |
//                (trig_tsweep_counter == shift_front + 1 )   )
//                start_adc_count_r <= 1;
                  
//            if( trig_tsweep_counter == (sweep_val - 1) |
//                trig_tsweep_counter == (sweep_val - 2) ) 
//                start_adc_count_r <= 0;
//      end
//      else begin
//        start_adc_count_r <= 0;
//      end 
//end
  
endmodule
//    assign S00_AXI_rdata = 32'hFFFFFFFF;
    
//    task_axilm u_task_axilm(
//        // AXI4 Lite Interface
//        .ARESETN      ( s00_axis_aresetn       ),
//        .ACLK         ( m00_axis_aclk          ),
    
//        // Write Address Channel
//        .AXI_AWADDR   ( S00_AXI_awaddr  ),
//        .AXI_AWCACHE  (  AXI_AWCACHE  ),
//        .AXI_AWPROT   ( S00_AXI_awprot  ),
//        .AXI_AWVALID  ( S00_AXI_awvalid ),
//        .AXI_AWREADY  ( S00_AXI_awready ),
    
//        // Write Data Channel
//        .AXI_WDATA    ( S00_AXI_wdata   ),
//        .AXI_WSTRB    ( S00_AXI_wstrb   ),
//        .AXI_WVALID   ( S00_AXI_wvalid  ),
//        .AXI_WREADY   ( S00_AXI_wready  ),
    
//        // Write Response Channel
//        .AXI_BVALID   ( S00_AXI_bvalid  ),
//        .AXI_BREADY   ( S00_AXI_bready  ),
//        .AXI_BRESP    ( S00_AXI_bresp   ),
    
//        // Read Address Channel
//        .AXI_ARADDR   ( S00_AXI_araddr  ),
//        .AXI_ARCACHE  ( AXI_ARCACHE ), 
//        .AXI_ARPROT   ( S00_AXI_arprot  ),
//        .AXI_ARVALID  ( S00_AXI_arvalid ),
//        .AXI_ARREADY  ( S00_AXI_arready ),
    
//        // Read Data Channel
//        .AXI_RDATA    ( S00_AXI_rdata   ),
//        .AXI_RRESP    ( S00_AXI_rresp   ),
//        .AXI_RVALID   ( S00_AXI_rvalid  ),
//        .AXI_RREADY   ( S00_AXI_rready  )
//  );
// initial begin
//		wait (s00_axis_aresetn);
//		@(posedge m00_axis_aclk);
		
//		$display(" HMC769: FrameSize=8192");

//	    u_task_axilm.write(32'h0000_0000 , 9900);
////	    u_task_axilm.write(32'h0000_0000, 32'h0000_0000);
    
////    $display(" Start Clock: %d",count);
////        hold_image = 0;

////		@(posedge m00_axis_aclk);
////		@(posedge m00_axis_aclk);
////		forever begin
////		 if( S_AXIS_TREADY == 1'b1) begin
////		 	S_AXIS_TVALID <= 1'b1;
////		 end else begin
////		 	S_AXIS_TVALID <= 1'b0;
////		 end
////		 @(posedge CLK);
////		end
//	end
    
//    always begin
//           #(74500000) azimut_0 = ~azimut_0;
//        // #(500000000) azimut_0 = ~azimut_0;
//    end
    
//    initial begin
//        wait (s00_axis_aresetn);
//		  @(posedge clk_10MHz);
//		  $display(" Start Clock");
//          u_task_axilm.write(32'h1000_0000, 32'h8000_0000);        
//    end


