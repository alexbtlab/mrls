//Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
//Date        : Fri Jun  3 12:48:02 2022
//Host        : alexbtlab-System-Product-Name running 64-bit Ubuntu 20.04.3 LTS
//Command     : generate_target design_1_wrapper.bd
//Design      : design_1_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module design_1_wrapper
   (ADC_PDwN_0,
    ADC_SPI_0_cs,
    ADC_SPI_0_sck,
    ADC_SPI_0_sdio,
    ATTEN_0,
    DATA_CH1_in_0,
    DATA_CH2_in_0,
    DCO_OR_0_dcoa,
    DCO_OR_0_dcob,
    DCO_OR_0_ora,
    DCO_OR_0_orb,
    DDR_addr,
    DDR_ba,
    DDR_cas_n,
    DDR_ck_n,
    DDR_ck_p,
    DDR_cke,
    DDR_cs_n,
    DDR_dm,
    DDR_dq,
    DDR_dqs_n,
    DDR_dqs_p,
    DDR_odt,
    DDR_ras_n,
    DDR_reset_n,
    DDR_we_n,
    FIXED_IO_ddr_vrn,
    FIXED_IO_ddr_vrp,
    FIXED_IO_mio,
    FIXED_IO_ps_clk,
    FIXED_IO_ps_porb,
    FIXED_IO_ps_srstb,
    IIC_0_0_scl_io,
    IIC_0_0_sda_io,
    IIC_1_TEMP_PAMP_scl_io,
    IIC_1_TEMP_PAMP_sda_io,
    PAMP_ALM_FPGA,
    PAMP_EN_0,
    PLL_POW_EN_0,
    RX_AMP_0_amp0,
    RX_AMP_0_amp1,
    RX_AMP_0_amp2,
    SPI_PLL_0_cen,
    SPI_PLL_0_ld_sdo,
    SPI_PLL_0_mosi,
    SPI_PLL_0_sck,
    SPI_PLL_0_sen,
    SW_1_0_sw1_if1_ctrl,
    SW_1_0_sw1_if2_ctrl,
    SW_2_0_sw2_if1_ctrl,
    SW_2_0_sw2_if2_ctrl,
    SW_3_0_sw3_if1_ctrl,
    SW_3_0_sw3_if2_ctrl,
    SW_BASE_0_sw_out_if1_a0,
    SW_BASE_0_sw_out_if1_a1,
    SW_BASE_0_sw_out_if2_a0,
    SW_BASE_0_sw_out_if2_a1,
    SYNC_0,
    Vaux0_LDO_TEMP_v_n,
    Vaux0_LDO_TEMP_v_p,
    Vaux8_OPOW_DET_v_n,
    Vaux8_OPOW_DET_v_p,
    allow,
    allowed_read_out_0,
    async_azimut_0,
    low_amp_en_0,
    pll_trig_0,
    uart_rtl_0_rxd,
    uart_rtl_0_txd);
  output ADC_PDwN_0;
  output ADC_SPI_0_cs;
  output ADC_SPI_0_sck;
  inout ADC_SPI_0_sdio;
  output [5:0]ATTEN_0;
  input [15:0]DATA_CH1_in_0;
  input [15:0]DATA_CH2_in_0;
  input DCO_OR_0_dcoa;
  input DCO_OR_0_dcob;
  input DCO_OR_0_ora;
  input DCO_OR_0_orb;
  inout [14:0]DDR_addr;
  inout [2:0]DDR_ba;
  inout DDR_cas_n;
  inout DDR_ck_n;
  inout DDR_ck_p;
  inout DDR_cke;
  inout DDR_cs_n;
  inout [3:0]DDR_dm;
  inout [31:0]DDR_dq;
  inout [3:0]DDR_dqs_n;
  inout [3:0]DDR_dqs_p;
  inout DDR_odt;
  inout DDR_ras_n;
  inout DDR_reset_n;
  inout DDR_we_n;
  inout FIXED_IO_ddr_vrn;
  inout FIXED_IO_ddr_vrp;
  inout [53:0]FIXED_IO_mio;
  inout FIXED_IO_ps_clk;
  inout FIXED_IO_ps_porb;
  inout FIXED_IO_ps_srstb;
  inout IIC_0_0_scl_io;
  inout IIC_0_0_sda_io;
  inout IIC_1_TEMP_PAMP_scl_io;
  inout IIC_1_TEMP_PAMP_sda_io;
  input PAMP_ALM_FPGA;
  output PAMP_EN_0;
  output PLL_POW_EN_0;
  output RX_AMP_0_amp0;
  output RX_AMP_0_amp1;
  output RX_AMP_0_amp2;
  output SPI_PLL_0_cen;
  input SPI_PLL_0_ld_sdo;
  output SPI_PLL_0_mosi;
  output SPI_PLL_0_sck;
  output SPI_PLL_0_sen;
  output SW_1_0_sw1_if1_ctrl;
  output SW_1_0_sw1_if2_ctrl;
  output SW_2_0_sw2_if1_ctrl;
  output SW_2_0_sw2_if2_ctrl;
  output SW_3_0_sw3_if1_ctrl;
  output SW_3_0_sw3_if2_ctrl;
  output SW_BASE_0_sw_out_if1_a0;
  output SW_BASE_0_sw_out_if1_a1;
  output SW_BASE_0_sw_out_if2_a0;
  output SW_BASE_0_sw_out_if2_a1;
  output SYNC_0;
  input Vaux0_LDO_TEMP_v_n;
  input Vaux0_LDO_TEMP_v_p;
  input Vaux8_OPOW_DET_v_n;
  input Vaux8_OPOW_DET_v_p;
  output allow;
  output allowed_read_out_0;
  input async_azimut_0;
  output low_amp_en_0;
  output pll_trig_0;
  input uart_rtl_0_rxd;
  output uart_rtl_0_txd;

  wire ADC_PDwN_0;
  wire ADC_SPI_0_cs;
  wire ADC_SPI_0_sck;
  wire ADC_SPI_0_sdio;
  wire [5:0]ATTEN_0;
  wire [15:0]DATA_CH1_in_0;
  wire [15:0]DATA_CH2_in_0;
  wire DCO_OR_0_dcoa;
  wire DCO_OR_0_dcob;
  wire DCO_OR_0_ora;
  wire DCO_OR_0_orb;
  wire [14:0]DDR_addr;
  wire [2:0]DDR_ba;
  wire DDR_cas_n;
  wire DDR_ck_n;
  wire DDR_ck_p;
  wire DDR_cke;
  wire DDR_cs_n;
  wire [3:0]DDR_dm;
  wire [31:0]DDR_dq;
  wire [3:0]DDR_dqs_n;
  wire [3:0]DDR_dqs_p;
  wire DDR_odt;
  wire DDR_ras_n;
  wire DDR_reset_n;
  wire DDR_we_n;
  wire FIXED_IO_ddr_vrn;
  wire FIXED_IO_ddr_vrp;
  wire [53:0]FIXED_IO_mio;
  wire FIXED_IO_ps_clk;
  wire FIXED_IO_ps_porb;
  wire FIXED_IO_ps_srstb;
  wire IIC_0_0_scl_i;
  wire IIC_0_0_scl_io;
  wire IIC_0_0_scl_o;
  wire IIC_0_0_scl_t;
  wire IIC_0_0_sda_i;
  wire IIC_0_0_sda_io;
  wire IIC_0_0_sda_o;
  wire IIC_0_0_sda_t;
  wire IIC_1_TEMP_PAMP_scl_i;
  wire IIC_1_TEMP_PAMP_scl_io;
  wire IIC_1_TEMP_PAMP_scl_o;
  wire IIC_1_TEMP_PAMP_scl_t;
  wire IIC_1_TEMP_PAMP_sda_i;
  wire IIC_1_TEMP_PAMP_sda_io;
  wire IIC_1_TEMP_PAMP_sda_o;
  wire IIC_1_TEMP_PAMP_sda_t;
  wire PAMP_ALM_FPGA;
  wire PAMP_EN_0;
  wire PLL_POW_EN_0;
  wire RX_AMP_0_amp0;
  wire RX_AMP_0_amp1;
  wire RX_AMP_0_amp2;
  wire SPI_PLL_0_cen;
  wire SPI_PLL_0_ld_sdo;
  wire SPI_PLL_0_mosi;
  wire SPI_PLL_0_sck;
  wire SPI_PLL_0_sen;
  wire SW_1_0_sw1_if1_ctrl;
  wire SW_1_0_sw1_if2_ctrl;
  wire SW_2_0_sw2_if1_ctrl;
  wire SW_2_0_sw2_if2_ctrl;
  wire SW_3_0_sw3_if1_ctrl;
  wire SW_3_0_sw3_if2_ctrl;
  wire SW_BASE_0_sw_out_if1_a0;
  wire SW_BASE_0_sw_out_if1_a1;
  wire SW_BASE_0_sw_out_if2_a0;
  wire SW_BASE_0_sw_out_if2_a1;
  wire SYNC_0;
  wire Vaux0_LDO_TEMP_v_n;
  wire Vaux0_LDO_TEMP_v_p;
  wire Vaux8_OPOW_DET_v_n;
  wire Vaux8_OPOW_DET_v_p;
  wire allow;
  wire allowed_read_out_0;
  wire async_azimut_0;
  wire low_amp_en_0;
  wire pll_trig_0;
  wire uart_rtl_0_rxd;
  wire uart_rtl_0_txd;

  IOBUF IIC_0_0_scl_iobuf
       (.I(IIC_0_0_scl_o),
        .IO(IIC_0_0_scl_io),
        .O(IIC_0_0_scl_i),
        .T(IIC_0_0_scl_t));
  IOBUF IIC_0_0_sda_iobuf
       (.I(IIC_0_0_sda_o),
        .IO(IIC_0_0_sda_io),
        .O(IIC_0_0_sda_i),
        .T(IIC_0_0_sda_t));
  IOBUF IIC_1_TEMP_PAMP_scl_iobuf
       (.I(IIC_1_TEMP_PAMP_scl_o),
        .IO(IIC_1_TEMP_PAMP_scl_io),
        .O(IIC_1_TEMP_PAMP_scl_i),
        .T(IIC_1_TEMP_PAMP_scl_t));
  IOBUF IIC_1_TEMP_PAMP_sda_iobuf
       (.I(IIC_1_TEMP_PAMP_sda_o),
        .IO(IIC_1_TEMP_PAMP_sda_io),
        .O(IIC_1_TEMP_PAMP_sda_i),
        .T(IIC_1_TEMP_PAMP_sda_t));
  design_1 design_1_i
       (.ADC_PDwN_0(ADC_PDwN_0),
        .ADC_SPI_0_cs(ADC_SPI_0_cs),
        .ADC_SPI_0_sck(ADC_SPI_0_sck),
        .ADC_SPI_0_sdio(ADC_SPI_0_sdio),
        .ATTEN_0(ATTEN_0),
        .DATA_CH1_in_0(DATA_CH1_in_0),
        .DATA_CH2_in_0(DATA_CH2_in_0),
        .DCO_OR_0_dcoa(DCO_OR_0_dcoa),
        .DCO_OR_0_dcob(DCO_OR_0_dcob),
        .DCO_OR_0_ora(DCO_OR_0_ora),
        .DCO_OR_0_orb(DCO_OR_0_orb),
        .DDR_addr(DDR_addr),
        .DDR_ba(DDR_ba),
        .DDR_cas_n(DDR_cas_n),
        .DDR_ck_n(DDR_ck_n),
        .DDR_ck_p(DDR_ck_p),
        .DDR_cke(DDR_cke),
        .DDR_cs_n(DDR_cs_n),
        .DDR_dm(DDR_dm),
        .DDR_dq(DDR_dq),
        .DDR_dqs_n(DDR_dqs_n),
        .DDR_dqs_p(DDR_dqs_p),
        .DDR_odt(DDR_odt),
        .DDR_ras_n(DDR_ras_n),
        .DDR_reset_n(DDR_reset_n),
        .DDR_we_n(DDR_we_n),
        .FIXED_IO_ddr_vrn(FIXED_IO_ddr_vrn),
        .FIXED_IO_ddr_vrp(FIXED_IO_ddr_vrp),
        .FIXED_IO_mio(FIXED_IO_mio),
        .FIXED_IO_ps_clk(FIXED_IO_ps_clk),
        .FIXED_IO_ps_porb(FIXED_IO_ps_porb),
        .FIXED_IO_ps_srstb(FIXED_IO_ps_srstb),
        .IIC_0_0_scl_i(IIC_0_0_scl_i),
        .IIC_0_0_scl_o(IIC_0_0_scl_o),
        .IIC_0_0_scl_t(IIC_0_0_scl_t),
        .IIC_0_0_sda_i(IIC_0_0_sda_i),
        .IIC_0_0_sda_o(IIC_0_0_sda_o),
        .IIC_0_0_sda_t(IIC_0_0_sda_t),
        .IIC_1_TEMP_PAMP_scl_i(IIC_1_TEMP_PAMP_scl_i),
        .IIC_1_TEMP_PAMP_scl_o(IIC_1_TEMP_PAMP_scl_o),
        .IIC_1_TEMP_PAMP_scl_t(IIC_1_TEMP_PAMP_scl_t),
        .IIC_1_TEMP_PAMP_sda_i(IIC_1_TEMP_PAMP_sda_i),
        .IIC_1_TEMP_PAMP_sda_o(IIC_1_TEMP_PAMP_sda_o),
        .IIC_1_TEMP_PAMP_sda_t(IIC_1_TEMP_PAMP_sda_t),
        .PAMP_ALM_FPGA(PAMP_ALM_FPGA),
        .PAMP_EN_0(PAMP_EN_0),
        .PLL_POW_EN_0(PLL_POW_EN_0),
        .RX_AMP_0_amp0(RX_AMP_0_amp0),
        .RX_AMP_0_amp1(RX_AMP_0_amp1),
        .RX_AMP_0_amp2(RX_AMP_0_amp2),
        .SPI_PLL_0_cen(SPI_PLL_0_cen),
        .SPI_PLL_0_ld_sdo(SPI_PLL_0_ld_sdo),
        .SPI_PLL_0_mosi(SPI_PLL_0_mosi),
        .SPI_PLL_0_sck(SPI_PLL_0_sck),
        .SPI_PLL_0_sen(SPI_PLL_0_sen),
        .SW_1_0_sw1_if1_ctrl(SW_1_0_sw1_if1_ctrl),
        .SW_1_0_sw1_if2_ctrl(SW_1_0_sw1_if2_ctrl),
        .SW_2_0_sw2_if1_ctrl(SW_2_0_sw2_if1_ctrl),
        .SW_2_0_sw2_if2_ctrl(SW_2_0_sw2_if2_ctrl),
        .SW_3_0_sw3_if1_ctrl(SW_3_0_sw3_if1_ctrl),
        .SW_3_0_sw3_if2_ctrl(SW_3_0_sw3_if2_ctrl),
        .SW_BASE_0_sw_out_if1_a0(SW_BASE_0_sw_out_if1_a0),
        .SW_BASE_0_sw_out_if1_a1(SW_BASE_0_sw_out_if1_a1),
        .SW_BASE_0_sw_out_if2_a0(SW_BASE_0_sw_out_if2_a0),
        .SW_BASE_0_sw_out_if2_a1(SW_BASE_0_sw_out_if2_a1),
        .SYNC_0(SYNC_0),
        .Vaux0_LDO_TEMP_v_n(Vaux0_LDO_TEMP_v_n),
        .Vaux0_LDO_TEMP_v_p(Vaux0_LDO_TEMP_v_p),
        .Vaux8_OPOW_DET_v_n(Vaux8_OPOW_DET_v_n),
        .Vaux8_OPOW_DET_v_p(Vaux8_OPOW_DET_v_p),
        .allow(allow),
        .allowed_read_out_0(allowed_read_out_0),
        .async_azimut_0(async_azimut_0),
        .low_amp_en_0(low_amp_en_0),
        .pll_trig_0(pll_trig_0),
        .uart_rtl_0_rxd(uart_rtl_0_rxd),
        .uart_rtl_0_txd(uart_rtl_0_txd));
endmodule
