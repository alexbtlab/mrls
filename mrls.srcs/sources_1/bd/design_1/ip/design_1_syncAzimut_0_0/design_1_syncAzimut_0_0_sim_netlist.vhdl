-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
-- Date        : Mon Feb 28 18:13:07 2022
-- Host        : alexbtlab-System-Product-Name running 64-bit Ubuntu 20.04.3 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top design_1_syncAzimut_0_0 -prefix
--               design_1_syncAzimut_0_0_ design_1_syncAzimut_0_0_sim_netlist.vhdl
-- Design      : design_1_syncAzimut_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z020clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_syncAzimut_0_0_syncAzimut_v1_0 is
  port (
    sync_azimut : out STD_LOGIC;
    async_azimut : in STD_LOGIC;
    reset : in STD_LOGIC;
    sysclk_100 : in STD_LOGIC
  );
end design_1_syncAzimut_0_0_syncAzimut_v1_0;

architecture STRUCTURE of design_1_syncAzimut_0_0_syncAzimut_v1_0 is
  signal \cnt_notrecieved_azimut[7]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_notrecieved_azimut[7]_i_3_n_0\ : STD_LOGIC;
  signal cnt_notrecieved_azimut_reg : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \cnt_recieved_azimut[7]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_recieved_azimut[7]_i_3_n_0\ : STD_LOGIC;
  signal cnt_recieved_azimut_reg : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal p_0_in : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \p_0_in__0\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \^sync_azimut\ : STD_LOGIC;
  signal sync_azimut_r_i_1_n_0 : STD_LOGIC;
  signal sync_azimut_r_i_2_n_0 : STD_LOGIC;
  signal sync_azimut_r_i_3_n_0 : STD_LOGIC;
  signal sync_azimut_r_i_4_n_0 : STD_LOGIC;
  signal sync_azimut_r_i_5_n_0 : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \cnt_notrecieved_azimut[0]_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \cnt_notrecieved_azimut[1]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \cnt_notrecieved_azimut[2]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \cnt_notrecieved_azimut[3]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \cnt_notrecieved_azimut[4]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \cnt_notrecieved_azimut[6]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \cnt_notrecieved_azimut[7]_i_2\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \cnt_recieved_azimut[0]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \cnt_recieved_azimut[1]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \cnt_recieved_azimut[2]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \cnt_recieved_azimut[3]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \cnt_recieved_azimut[4]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \cnt_recieved_azimut[6]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \cnt_recieved_azimut[7]_i_2\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of sync_azimut_r_i_3 : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of sync_azimut_r_i_5 : label is "soft_lutpair6";
begin
  sync_azimut <= \^sync_azimut\;
\cnt_notrecieved_azimut[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt_notrecieved_azimut_reg(0),
      O => p_0_in(0)
    );
\cnt_notrecieved_azimut[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => cnt_notrecieved_azimut_reg(0),
      I1 => cnt_notrecieved_azimut_reg(1),
      O => p_0_in(1)
    );
\cnt_notrecieved_azimut[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => cnt_notrecieved_azimut_reg(1),
      I1 => cnt_notrecieved_azimut_reg(0),
      I2 => cnt_notrecieved_azimut_reg(2),
      O => p_0_in(2)
    );
\cnt_notrecieved_azimut[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => cnt_notrecieved_azimut_reg(2),
      I1 => cnt_notrecieved_azimut_reg(0),
      I2 => cnt_notrecieved_azimut_reg(1),
      I3 => cnt_notrecieved_azimut_reg(3),
      O => p_0_in(3)
    );
\cnt_notrecieved_azimut[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => cnt_notrecieved_azimut_reg(3),
      I1 => cnt_notrecieved_azimut_reg(1),
      I2 => cnt_notrecieved_azimut_reg(0),
      I3 => cnt_notrecieved_azimut_reg(2),
      I4 => cnt_notrecieved_azimut_reg(4),
      O => p_0_in(4)
    );
\cnt_notrecieved_azimut[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => cnt_notrecieved_azimut_reg(4),
      I1 => cnt_notrecieved_azimut_reg(2),
      I2 => cnt_notrecieved_azimut_reg(0),
      I3 => cnt_notrecieved_azimut_reg(1),
      I4 => cnt_notrecieved_azimut_reg(3),
      I5 => cnt_notrecieved_azimut_reg(5),
      O => p_0_in(5)
    );
\cnt_notrecieved_azimut[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \cnt_notrecieved_azimut[7]_i_3_n_0\,
      I1 => cnt_notrecieved_azimut_reg(6),
      O => p_0_in(6)
    );
\cnt_notrecieved_azimut[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => async_azimut,
      I1 => reset,
      O => \cnt_notrecieved_azimut[7]_i_1_n_0\
    );
\cnt_notrecieved_azimut[7]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D2"
    )
        port map (
      I0 => cnt_notrecieved_azimut_reg(6),
      I1 => \cnt_notrecieved_azimut[7]_i_3_n_0\,
      I2 => cnt_notrecieved_azimut_reg(7),
      O => p_0_in(7)
    );
\cnt_notrecieved_azimut[7]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFFFFFFFFFF"
    )
        port map (
      I0 => cnt_notrecieved_azimut_reg(4),
      I1 => cnt_notrecieved_azimut_reg(2),
      I2 => cnt_notrecieved_azimut_reg(0),
      I3 => cnt_notrecieved_azimut_reg(1),
      I4 => cnt_notrecieved_azimut_reg(3),
      I5 => cnt_notrecieved_azimut_reg(5),
      O => \cnt_notrecieved_azimut[7]_i_3_n_0\
    );
\cnt_notrecieved_azimut_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => sysclk_100,
      CE => '1',
      D => p_0_in(0),
      Q => cnt_notrecieved_azimut_reg(0),
      R => \cnt_notrecieved_azimut[7]_i_1_n_0\
    );
\cnt_notrecieved_azimut_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => sysclk_100,
      CE => '1',
      D => p_0_in(1),
      Q => cnt_notrecieved_azimut_reg(1),
      R => \cnt_notrecieved_azimut[7]_i_1_n_0\
    );
\cnt_notrecieved_azimut_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => sysclk_100,
      CE => '1',
      D => p_0_in(2),
      Q => cnt_notrecieved_azimut_reg(2),
      R => \cnt_notrecieved_azimut[7]_i_1_n_0\
    );
\cnt_notrecieved_azimut_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => sysclk_100,
      CE => '1',
      D => p_0_in(3),
      Q => cnt_notrecieved_azimut_reg(3),
      R => \cnt_notrecieved_azimut[7]_i_1_n_0\
    );
\cnt_notrecieved_azimut_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => sysclk_100,
      CE => '1',
      D => p_0_in(4),
      Q => cnt_notrecieved_azimut_reg(4),
      R => \cnt_notrecieved_azimut[7]_i_1_n_0\
    );
\cnt_notrecieved_azimut_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => sysclk_100,
      CE => '1',
      D => p_0_in(5),
      Q => cnt_notrecieved_azimut_reg(5),
      R => \cnt_notrecieved_azimut[7]_i_1_n_0\
    );
\cnt_notrecieved_azimut_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => sysclk_100,
      CE => '1',
      D => p_0_in(6),
      Q => cnt_notrecieved_azimut_reg(6),
      R => \cnt_notrecieved_azimut[7]_i_1_n_0\
    );
\cnt_notrecieved_azimut_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => sysclk_100,
      CE => '1',
      D => p_0_in(7),
      Q => cnt_notrecieved_azimut_reg(7),
      R => \cnt_notrecieved_azimut[7]_i_1_n_0\
    );
\cnt_recieved_azimut[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt_recieved_azimut_reg(0),
      O => \p_0_in__0\(0)
    );
\cnt_recieved_azimut[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => cnt_recieved_azimut_reg(0),
      I1 => cnt_recieved_azimut_reg(1),
      O => \p_0_in__0\(1)
    );
\cnt_recieved_azimut[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => cnt_recieved_azimut_reg(1),
      I1 => cnt_recieved_azimut_reg(0),
      I2 => cnt_recieved_azimut_reg(2),
      O => \p_0_in__0\(2)
    );
\cnt_recieved_azimut[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => cnt_recieved_azimut_reg(2),
      I1 => cnt_recieved_azimut_reg(0),
      I2 => cnt_recieved_azimut_reg(1),
      I3 => cnt_recieved_azimut_reg(3),
      O => \p_0_in__0\(3)
    );
\cnt_recieved_azimut[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => cnt_recieved_azimut_reg(3),
      I1 => cnt_recieved_azimut_reg(1),
      I2 => cnt_recieved_azimut_reg(0),
      I3 => cnt_recieved_azimut_reg(2),
      I4 => cnt_recieved_azimut_reg(4),
      O => \p_0_in__0\(4)
    );
\cnt_recieved_azimut[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => cnt_recieved_azimut_reg(4),
      I1 => cnt_recieved_azimut_reg(2),
      I2 => cnt_recieved_azimut_reg(0),
      I3 => cnt_recieved_azimut_reg(1),
      I4 => cnt_recieved_azimut_reg(3),
      I5 => cnt_recieved_azimut_reg(5),
      O => \p_0_in__0\(5)
    );
\cnt_recieved_azimut[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \cnt_recieved_azimut[7]_i_3_n_0\,
      I1 => cnt_recieved_azimut_reg(6),
      O => \p_0_in__0\(6)
    );
\cnt_recieved_azimut[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => reset,
      I1 => async_azimut,
      O => \cnt_recieved_azimut[7]_i_1_n_0\
    );
\cnt_recieved_azimut[7]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D2"
    )
        port map (
      I0 => cnt_recieved_azimut_reg(6),
      I1 => \cnt_recieved_azimut[7]_i_3_n_0\,
      I2 => cnt_recieved_azimut_reg(7),
      O => \p_0_in__0\(7)
    );
\cnt_recieved_azimut[7]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFFFFFFFFFF"
    )
        port map (
      I0 => cnt_recieved_azimut_reg(4),
      I1 => cnt_recieved_azimut_reg(2),
      I2 => cnt_recieved_azimut_reg(0),
      I3 => cnt_recieved_azimut_reg(1),
      I4 => cnt_recieved_azimut_reg(3),
      I5 => cnt_recieved_azimut_reg(5),
      O => \cnt_recieved_azimut[7]_i_3_n_0\
    );
\cnt_recieved_azimut_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => sysclk_100,
      CE => '1',
      D => \p_0_in__0\(0),
      Q => cnt_recieved_azimut_reg(0),
      R => \cnt_recieved_azimut[7]_i_1_n_0\
    );
\cnt_recieved_azimut_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => sysclk_100,
      CE => '1',
      D => \p_0_in__0\(1),
      Q => cnt_recieved_azimut_reg(1),
      R => \cnt_recieved_azimut[7]_i_1_n_0\
    );
\cnt_recieved_azimut_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => sysclk_100,
      CE => '1',
      D => \p_0_in__0\(2),
      Q => cnt_recieved_azimut_reg(2),
      R => \cnt_recieved_azimut[7]_i_1_n_0\
    );
\cnt_recieved_azimut_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => sysclk_100,
      CE => '1',
      D => \p_0_in__0\(3),
      Q => cnt_recieved_azimut_reg(3),
      R => \cnt_recieved_azimut[7]_i_1_n_0\
    );
\cnt_recieved_azimut_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => sysclk_100,
      CE => '1',
      D => \p_0_in__0\(4),
      Q => cnt_recieved_azimut_reg(4),
      R => \cnt_recieved_azimut[7]_i_1_n_0\
    );
\cnt_recieved_azimut_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => sysclk_100,
      CE => '1',
      D => \p_0_in__0\(5),
      Q => cnt_recieved_azimut_reg(5),
      R => \cnt_recieved_azimut[7]_i_1_n_0\
    );
\cnt_recieved_azimut_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => sysclk_100,
      CE => '1',
      D => \p_0_in__0\(6),
      Q => cnt_recieved_azimut_reg(6),
      R => \cnt_recieved_azimut[7]_i_1_n_0\
    );
\cnt_recieved_azimut_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => sysclk_100,
      CE => '1',
      D => \p_0_in__0\(7),
      Q => cnt_recieved_azimut_reg(7),
      R => \cnt_recieved_azimut[7]_i_1_n_0\
    );
sync_azimut_r_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8FFF888888888888"
    )
        port map (
      I0 => sync_azimut_r_i_2_n_0,
      I1 => sync_azimut_r_i_3_n_0,
      I2 => sync_azimut_r_i_4_n_0,
      I3 => sync_azimut_r_i_5_n_0,
      I4 => reset,
      I5 => \^sync_azimut\,
      O => sync_azimut_r_i_1_n_0
    );
sync_azimut_r_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => cnt_recieved_azimut_reg(5),
      I1 => cnt_recieved_azimut_reg(6),
      I2 => cnt_recieved_azimut_reg(3),
      I3 => cnt_recieved_azimut_reg(4),
      I4 => cnt_recieved_azimut_reg(7),
      I5 => async_azimut,
      O => sync_azimut_r_i_2_n_0
    );
sync_azimut_r_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0400"
    )
        port map (
      I0 => cnt_recieved_azimut_reg(0),
      I1 => reset,
      I2 => cnt_recieved_azimut_reg(1),
      I3 => cnt_recieved_azimut_reg(2),
      O => sync_azimut_r_i_3_n_0
    );
sync_azimut_r_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000002000000000"
    )
        port map (
      I0 => cnt_notrecieved_azimut_reg(5),
      I1 => cnt_notrecieved_azimut_reg(4),
      I2 => cnt_notrecieved_azimut_reg(2),
      I3 => cnt_notrecieved_azimut_reg(3),
      I4 => cnt_notrecieved_azimut_reg(7),
      I5 => cnt_notrecieved_azimut_reg(6),
      O => sync_azimut_r_i_4_n_0
    );
sync_azimut_r_i_5: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => cnt_notrecieved_azimut_reg(1),
      I1 => cnt_notrecieved_azimut_reg(0),
      I2 => async_azimut,
      O => sync_azimut_r_i_5_n_0
    );
sync_azimut_r_reg: unisim.vcomponents.FDRE
     port map (
      C => sysclk_100,
      CE => '1',
      D => sync_azimut_r_i_1_n_0,
      Q => \^sync_azimut\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_syncAzimut_0_0 is
  port (
    sysclk_100 : in STD_LOGIC;
    reset : in STD_LOGIC;
    async_azimut : in STD_LOGIC;
    sync_azimut : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_1_syncAzimut_0_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_1_syncAzimut_0_0 : entity is "design_1_syncAzimut_0_0,syncAzimut_v1_0,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of design_1_syncAzimut_0_0 : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of design_1_syncAzimut_0_0 : entity is "syncAzimut_v1_0,Vivado 2019.1";
end design_1_syncAzimut_0_0;

architecture STRUCTURE of design_1_syncAzimut_0_0 is
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of reset : signal is "xilinx.com:signal:reset:1.0 reset RST";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of reset : signal is "XIL_INTERFACENAME reset, POLARITY ACTIVE_LOW, INSERT_VIP 0";
begin
inst: entity work.design_1_syncAzimut_0_0_syncAzimut_v1_0
     port map (
      async_azimut => async_azimut,
      reset => reset,
      sync_azimut => sync_azimut,
      sysclk_100 => sysclk_100
    );
end STRUCTURE;
