-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
-- Date        : Fri Jun  3 12:19:47 2022
-- Host        : alexbtlab-System-Product-Name running 64-bit Ubuntu 20.04.3 LTS
-- Command     : write_vhdl -force -mode funcsim
--               /home/alexbtlab/source/mrls/mrls.srcs/sources_1/bd/design_1/ip/design_1_AD9650_0_0/design_1_AD9650_0_0_sim_netlist.vhdl
-- Design      : design_1_AD9650_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z020clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_AD9650_0_0_AD9650_v3_0_S00_AXI is
  port (
    s00_axi_wready : out STD_LOGIC;
    s00_axi_aresetn_0 : out STD_LOGIC;
    s00_axi_awready : out STD_LOGIC;
    s00_axi_arready : out STD_LOGIC;
    s00_axi_bvalid : out STD_LOGIC;
    s00_axi_rvalid : out STD_LOGIC;
    \cnt_reg[2]\ : out STD_LOGIC;
    \cnt_reg[2]_0\ : out STD_LOGIC;
    \slv_reg0_reg[11]_0\ : out STD_LOGIC;
    \slv_reg3_reg[31]_0\ : out STD_LOGIC_VECTOR ( 31 downto 0 );
    \slv_reg1_reg[0]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \slv_reg0_reg[12]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \slv_reg2_reg[1]_0\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC;
    DATA_TX_serial_r_reg : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 1 downto 0 );
    DATA_RX_r : in STD_LOGIC_VECTOR ( 7 downto 0 );
    data4 : in STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_wvalid : in STD_LOGIC;
    s00_axi_bready : in STD_LOGIC;
    s00_axi_arvalid : in STD_LOGIC;
    s00_axi_rready : in STD_LOGIC;
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_AD9650_0_0_AD9650_v3_0_S00_AXI : entity is "AD9650_v3_0_S00_AXI";
end design_1_AD9650_0_0_AD9650_v3_0_S00_AXI;

architecture STRUCTURE of design_1_AD9650_0_0_AD9650_v3_0_S00_AXI is
  signal DATA_TX : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal DATA_TX_serial_r_i_12_n_0 : STD_LOGIC;
  signal DATA_TX_serial_r_i_13_n_0 : STD_LOGIC;
  signal DATA_TX_serial_r_i_14_n_0 : STD_LOGIC;
  signal DATA_TX_serial_r_i_15_n_0 : STD_LOGIC;
  signal aw_en_i_1_n_0 : STD_LOGIC;
  signal aw_en_reg_n_0 : STD_LOGIC;
  signal axi_araddr : STD_LOGIC_VECTOR ( 4 downto 2 );
  signal \axi_araddr[2]_i_1_n_0\ : STD_LOGIC;
  signal \axi_araddr[3]_i_1_n_0\ : STD_LOGIC;
  signal \axi_araddr[4]_i_1_n_0\ : STD_LOGIC;
  signal axi_arready0 : STD_LOGIC;
  signal axi_awaddr : STD_LOGIC_VECTOR ( 4 downto 2 );
  signal \axi_awaddr[2]_i_1_n_0\ : STD_LOGIC;
  signal \axi_awaddr[3]_i_1_n_0\ : STD_LOGIC;
  signal \axi_awaddr[4]_i_1_n_0\ : STD_LOGIC;
  signal axi_awready0 : STD_LOGIC;
  signal axi_bvalid_i_1_n_0 : STD_LOGIC;
  signal \axi_rdata[0]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[14]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[16]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[17]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[18]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[20]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[21]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[22]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[24]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[25]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[26]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[28]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[29]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[30]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_2_n_0\ : STD_LOGIC;
  signal axi_rvalid_i_1_n_0 : STD_LOGIC;
  signal axi_wready0 : STD_LOGIC;
  signal reg_data_out : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \^s00_axi_aresetn_0\ : STD_LOGIC;
  signal \^s00_axi_arready\ : STD_LOGIC;
  signal \^s00_axi_awready\ : STD_LOGIC;
  signal \^s00_axi_bvalid\ : STD_LOGIC;
  signal \^s00_axi_rvalid\ : STD_LOGIC;
  signal \^s00_axi_wready\ : STD_LOGIC;
  signal \slv_reg0[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg0[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg0[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg0[7]_i_1_n_0\ : STD_LOGIC;
  signal \^slv_reg0_reg[12]_0\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \slv_reg0_reg_n_0_[0]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[10]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[11]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[1]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[21]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[22]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[23]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[24]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[25]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[26]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[27]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[28]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[29]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[2]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[30]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[31]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[3]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[4]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[5]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[6]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[7]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[8]\ : STD_LOGIC;
  signal \slv_reg0_reg_n_0_[9]\ : STD_LOGIC;
  signal slv_reg1 : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal \slv_reg1[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg1[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg1[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg1[7]_i_1_n_0\ : STD_LOGIC;
  signal \^slv_reg1_reg[0]_0\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \slv_reg2[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg2[1]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg2[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg2[31]_i_1_n_0\ : STD_LOGIC;
  signal \^slv_reg2_reg[1]_0\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \slv_reg2_reg_n_0_[10]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[11]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[12]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[13]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[14]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[15]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[16]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[17]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[18]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[19]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[20]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[21]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[22]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[23]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[24]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[25]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[26]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[27]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[28]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[29]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[2]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[30]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[31]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[3]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[4]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[5]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[6]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[7]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[8]\ : STD_LOGIC;
  signal \slv_reg2_reg_n_0_[9]\ : STD_LOGIC;
  signal \slv_reg3[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg3[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg3[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg3[7]_i_1_n_0\ : STD_LOGIC;
  signal \^slv_reg3_reg[31]_0\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg_rden__0\ : STD_LOGIC;
  signal \slv_reg_wren__0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \axi_araddr[3]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of axi_arready_i_1 : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of axi_awready_i_2 : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \axi_rdata[10]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \axi_rdata[11]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \axi_rdata[12]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \axi_rdata[13]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \axi_rdata[14]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \axi_rdata[15]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \axi_rdata[16]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \axi_rdata[17]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \axi_rdata[18]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \axi_rdata[19]_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \axi_rdata[20]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \axi_rdata[21]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \axi_rdata[22]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \axi_rdata[23]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \axi_rdata[24]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \axi_rdata[25]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \axi_rdata[26]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \axi_rdata[27]_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \axi_rdata[28]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \axi_rdata[29]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \axi_rdata[30]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \axi_rdata[31]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \axi_rdata[8]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \axi_rdata[9]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of axi_wready_i_1 : label is "soft_lutpair0";
begin
  s00_axi_aresetn_0 <= \^s00_axi_aresetn_0\;
  s00_axi_arready <= \^s00_axi_arready\;
  s00_axi_awready <= \^s00_axi_awready\;
  s00_axi_bvalid <= \^s00_axi_bvalid\;
  s00_axi_rvalid <= \^s00_axi_rvalid\;
  s00_axi_wready <= \^s00_axi_wready\;
  \slv_reg0_reg[12]_0\(0) <= \^slv_reg0_reg[12]_0\(0);
  \slv_reg1_reg[0]_0\(0) <= \^slv_reg1_reg[0]_0\(0);
  \slv_reg2_reg[1]_0\(1 downto 0) <= \^slv_reg2_reg[1]_0\(1 downto 0);
  \slv_reg3_reg[31]_0\(31 downto 0) <= \^slv_reg3_reg[31]_0\(31 downto 0);
DATA_TX_serial_r_i_12: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FACF0ACFFAC00AC0"
    )
        port map (
      I0 => DATA_TX(7),
      I1 => DATA_TX(6),
      I2 => Q(1),
      I3 => Q(0),
      I4 => DATA_TX(5),
      I5 => DATA_TX(4),
      O => DATA_TX_serial_r_i_12_n_0
    );
DATA_TX_serial_r_i_13: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FACF0ACFFAC00AC0"
    )
        port map (
      I0 => DATA_TX(3),
      I1 => DATA_TX(2),
      I2 => Q(1),
      I3 => Q(0),
      I4 => DATA_TX(1),
      I5 => DATA_TX(0),
      O => DATA_TX_serial_r_i_13_n_0
    );
DATA_TX_serial_r_i_14: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FACF0ACFFAC00AC0"
    )
        port map (
      I0 => \slv_reg0_reg_n_0_[3]\,
      I1 => \slv_reg0_reg_n_0_[2]\,
      I2 => Q(1),
      I3 => Q(0),
      I4 => \slv_reg0_reg_n_0_[1]\,
      I5 => \slv_reg0_reg_n_0_[0]\,
      O => DATA_TX_serial_r_i_14_n_0
    );
DATA_TX_serial_r_i_15: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FACF0ACFFAC00AC0"
    )
        port map (
      I0 => \slv_reg0_reg_n_0_[7]\,
      I1 => \slv_reg0_reg_n_0_[6]\,
      I2 => Q(1),
      I3 => Q(0),
      I4 => \slv_reg0_reg_n_0_[5]\,
      I5 => \slv_reg0_reg_n_0_[4]\,
      O => DATA_TX_serial_r_i_15_n_0
    );
DATA_TX_serial_r_i_16: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FACF0ACFFAC00AC0"
    )
        port map (
      I0 => \slv_reg0_reg_n_0_[11]\,
      I1 => \slv_reg0_reg_n_0_[10]\,
      I2 => Q(1),
      I3 => Q(0),
      I4 => \slv_reg0_reg_n_0_[9]\,
      I5 => \slv_reg0_reg_n_0_[8]\,
      O => \slv_reg0_reg[11]_0\
    );
DATA_TX_serial_r_reg_i_4: unisim.vcomponents.MUXF7
     port map (
      I0 => DATA_TX_serial_r_i_12_n_0,
      I1 => DATA_TX_serial_r_i_13_n_0,
      O => \cnt_reg[2]\,
      S => DATA_TX_serial_r_reg
    );
DATA_TX_serial_r_reg_i_7: unisim.vcomponents.MUXF7
     port map (
      I0 => DATA_TX_serial_r_i_14_n_0,
      I1 => DATA_TX_serial_r_i_15_n_0,
      O => \cnt_reg[2]_0\,
      S => DATA_TX_serial_r_reg
    );
aw_en_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F7FFC4CCC4CCC4CC"
    )
        port map (
      I0 => s00_axi_awvalid,
      I1 => aw_en_reg_n_0,
      I2 => \^s00_axi_awready\,
      I3 => s00_axi_wvalid,
      I4 => s00_axi_bready,
      I5 => \^s00_axi_bvalid\,
      O => aw_en_i_1_n_0
    );
aw_en_reg: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => aw_en_i_1_n_0,
      Q => aw_en_reg_n_0,
      S => \^s00_axi_aresetn_0\
    );
\axi_araddr[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => s00_axi_araddr(0),
      I1 => s00_axi_arvalid,
      I2 => \^s00_axi_arready\,
      I3 => axi_araddr(2),
      O => \axi_araddr[2]_i_1_n_0\
    );
\axi_araddr[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => s00_axi_araddr(1),
      I1 => s00_axi_arvalid,
      I2 => \^s00_axi_arready\,
      I3 => axi_araddr(3),
      O => \axi_araddr[3]_i_1_n_0\
    );
\axi_araddr[4]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => s00_axi_araddr(2),
      I1 => s00_axi_arvalid,
      I2 => \^s00_axi_arready\,
      I3 => axi_araddr(4),
      O => \axi_araddr[4]_i_1_n_0\
    );
\axi_araddr_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \axi_araddr[2]_i_1_n_0\,
      Q => axi_araddr(2),
      R => \^s00_axi_aresetn_0\
    );
\axi_araddr_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \axi_araddr[3]_i_1_n_0\,
      Q => axi_araddr(3),
      R => \^s00_axi_aresetn_0\
    );
\axi_araddr_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \axi_araddr[4]_i_1_n_0\,
      Q => axi_araddr(4),
      R => \^s00_axi_aresetn_0\
    );
axi_arready_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => s00_axi_arvalid,
      I1 => \^s00_axi_arready\,
      O => axi_arready0
    );
axi_arready_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_arready0,
      Q => \^s00_axi_arready\,
      R => \^s00_axi_aresetn_0\
    );
\axi_awaddr[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FBFFFFFF08000000"
    )
        port map (
      I0 => s00_axi_awaddr(0),
      I1 => s00_axi_wvalid,
      I2 => \^s00_axi_awready\,
      I3 => aw_en_reg_n_0,
      I4 => s00_axi_awvalid,
      I5 => axi_awaddr(2),
      O => \axi_awaddr[2]_i_1_n_0\
    );
\axi_awaddr[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FBFFFFFF08000000"
    )
        port map (
      I0 => s00_axi_awaddr(1),
      I1 => s00_axi_wvalid,
      I2 => \^s00_axi_awready\,
      I3 => aw_en_reg_n_0,
      I4 => s00_axi_awvalid,
      I5 => axi_awaddr(3),
      O => \axi_awaddr[3]_i_1_n_0\
    );
\axi_awaddr[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FBFFFFFF08000000"
    )
        port map (
      I0 => s00_axi_awaddr(2),
      I1 => s00_axi_wvalid,
      I2 => \^s00_axi_awready\,
      I3 => aw_en_reg_n_0,
      I4 => s00_axi_awvalid,
      I5 => axi_awaddr(4),
      O => \axi_awaddr[4]_i_1_n_0\
    );
\axi_awaddr_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \axi_awaddr[2]_i_1_n_0\,
      Q => axi_awaddr(2),
      R => \^s00_axi_aresetn_0\
    );
\axi_awaddr_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \axi_awaddr[3]_i_1_n_0\,
      Q => axi_awaddr(3),
      R => \^s00_axi_aresetn_0\
    );
\axi_awaddr_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \axi_awaddr[4]_i_1_n_0\,
      Q => axi_awaddr(4),
      R => \^s00_axi_aresetn_0\
    );
axi_awready_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => s00_axi_aresetn,
      O => \^s00_axi_aresetn_0\
    );
axi_awready_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2000"
    )
        port map (
      I0 => s00_axi_wvalid,
      I1 => \^s00_axi_awready\,
      I2 => aw_en_reg_n_0,
      I3 => s00_axi_awvalid,
      O => axi_awready0
    );
axi_awready_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_awready0,
      Q => \^s00_axi_awready\,
      R => \^s00_axi_aresetn_0\
    );
axi_bvalid_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000FFFF80008000"
    )
        port map (
      I0 => s00_axi_awvalid,
      I1 => s00_axi_wvalid,
      I2 => \^s00_axi_awready\,
      I3 => \^s00_axi_wready\,
      I4 => s00_axi_bready,
      I5 => \^s00_axi_bvalid\,
      O => axi_bvalid_i_1_n_0
    );
axi_bvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_bvalid_i_1_n_0,
      Q => \^s00_axi_bvalid\,
      R => \^s00_axi_aresetn_0\
    );
\axi_rdata[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E2FFFF00E20000"
    )
        port map (
      I0 => data4(0),
      I1 => axi_araddr(2),
      I2 => DATA_RX_r(0),
      I3 => axi_araddr(3),
      I4 => axi_araddr(4),
      I5 => \axi_rdata[0]_i_2_n_0\,
      O => reg_data_out(0)
    );
\axi_rdata[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg3_reg[31]_0\(0),
      I1 => \^slv_reg2_reg[1]_0\(0),
      I2 => axi_araddr(3),
      I3 => \^slv_reg1_reg[0]_0\(0),
      I4 => axi_araddr(2),
      I5 => \slv_reg0_reg_n_0_[0]\,
      O => \axi_rdata[0]_i_2_n_0\
    );
\axi_rdata[10]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \axi_rdata[10]_i_2_n_0\,
      I1 => axi_araddr(4),
      O => reg_data_out(10)
    );
\axi_rdata[10]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg3_reg[31]_0\(10),
      I1 => \slv_reg2_reg_n_0_[10]\,
      I2 => axi_araddr(3),
      I3 => slv_reg1(10),
      I4 => axi_araddr(2),
      I5 => \slv_reg0_reg_n_0_[10]\,
      O => \axi_rdata[10]_i_2_n_0\
    );
\axi_rdata[11]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \axi_rdata[11]_i_2_n_0\,
      I1 => axi_araddr(4),
      O => reg_data_out(11)
    );
\axi_rdata[11]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg3_reg[31]_0\(11),
      I1 => \slv_reg2_reg_n_0_[11]\,
      I2 => axi_araddr(3),
      I3 => slv_reg1(11),
      I4 => axi_araddr(2),
      I5 => \slv_reg0_reg_n_0_[11]\,
      O => \axi_rdata[11]_i_2_n_0\
    );
\axi_rdata[12]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \axi_rdata[12]_i_2_n_0\,
      I1 => axi_araddr(4),
      O => reg_data_out(12)
    );
\axi_rdata[12]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg3_reg[31]_0\(12),
      I1 => \slv_reg2_reg_n_0_[12]\,
      I2 => axi_araddr(3),
      I3 => slv_reg1(12),
      I4 => axi_araddr(2),
      I5 => \^slv_reg0_reg[12]_0\(0),
      O => \axi_rdata[12]_i_2_n_0\
    );
\axi_rdata[13]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \axi_rdata[13]_i_2_n_0\,
      I1 => axi_araddr(4),
      O => reg_data_out(13)
    );
\axi_rdata[13]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg3_reg[31]_0\(13),
      I1 => \slv_reg2_reg_n_0_[13]\,
      I2 => axi_araddr(3),
      I3 => slv_reg1(13),
      I4 => axi_araddr(2),
      I5 => DATA_TX(0),
      O => \axi_rdata[13]_i_2_n_0\
    );
\axi_rdata[14]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \axi_rdata[14]_i_2_n_0\,
      I1 => axi_araddr(4),
      O => reg_data_out(14)
    );
\axi_rdata[14]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg3_reg[31]_0\(14),
      I1 => \slv_reg2_reg_n_0_[14]\,
      I2 => axi_araddr(3),
      I3 => slv_reg1(14),
      I4 => axi_araddr(2),
      I5 => DATA_TX(1),
      O => \axi_rdata[14]_i_2_n_0\
    );
\axi_rdata[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \axi_rdata[15]_i_2_n_0\,
      I1 => axi_araddr(4),
      O => reg_data_out(15)
    );
\axi_rdata[15]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg3_reg[31]_0\(15),
      I1 => \slv_reg2_reg_n_0_[15]\,
      I2 => axi_araddr(3),
      I3 => slv_reg1(15),
      I4 => axi_araddr(2),
      I5 => DATA_TX(2),
      O => \axi_rdata[15]_i_2_n_0\
    );
\axi_rdata[16]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \axi_rdata[16]_i_2_n_0\,
      I1 => axi_araddr(4),
      O => reg_data_out(16)
    );
\axi_rdata[16]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg3_reg[31]_0\(16),
      I1 => \slv_reg2_reg_n_0_[16]\,
      I2 => axi_araddr(3),
      I3 => slv_reg1(16),
      I4 => axi_araddr(2),
      I5 => DATA_TX(3),
      O => \axi_rdata[16]_i_2_n_0\
    );
\axi_rdata[17]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \axi_rdata[17]_i_2_n_0\,
      I1 => axi_araddr(4),
      O => reg_data_out(17)
    );
\axi_rdata[17]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg3_reg[31]_0\(17),
      I1 => \slv_reg2_reg_n_0_[17]\,
      I2 => axi_araddr(3),
      I3 => slv_reg1(17),
      I4 => axi_araddr(2),
      I5 => DATA_TX(4),
      O => \axi_rdata[17]_i_2_n_0\
    );
\axi_rdata[18]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \axi_rdata[18]_i_2_n_0\,
      I1 => axi_araddr(4),
      O => reg_data_out(18)
    );
\axi_rdata[18]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg3_reg[31]_0\(18),
      I1 => \slv_reg2_reg_n_0_[18]\,
      I2 => axi_araddr(3),
      I3 => slv_reg1(18),
      I4 => axi_araddr(2),
      I5 => DATA_TX(5),
      O => \axi_rdata[18]_i_2_n_0\
    );
\axi_rdata[19]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \axi_rdata[19]_i_2_n_0\,
      I1 => axi_araddr(4),
      O => reg_data_out(19)
    );
\axi_rdata[19]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg3_reg[31]_0\(19),
      I1 => \slv_reg2_reg_n_0_[19]\,
      I2 => axi_araddr(3),
      I3 => slv_reg1(19),
      I4 => axi_araddr(2),
      I5 => DATA_TX(6),
      O => \axi_rdata[19]_i_2_n_0\
    );
\axi_rdata[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"40FF4000"
    )
        port map (
      I0 => axi_araddr(3),
      I1 => DATA_RX_r(1),
      I2 => axi_araddr(2),
      I3 => axi_araddr(4),
      I4 => \axi_rdata[1]_i_2_n_0\,
      O => reg_data_out(1)
    );
\axi_rdata[1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg3_reg[31]_0\(1),
      I1 => \^slv_reg2_reg[1]_0\(1),
      I2 => axi_araddr(3),
      I3 => slv_reg1(1),
      I4 => axi_araddr(2),
      I5 => \slv_reg0_reg_n_0_[1]\,
      O => \axi_rdata[1]_i_2_n_0\
    );
\axi_rdata[20]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \axi_rdata[20]_i_2_n_0\,
      I1 => axi_araddr(4),
      O => reg_data_out(20)
    );
\axi_rdata[20]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg3_reg[31]_0\(20),
      I1 => \slv_reg2_reg_n_0_[20]\,
      I2 => axi_araddr(3),
      I3 => slv_reg1(20),
      I4 => axi_araddr(2),
      I5 => DATA_TX(7),
      O => \axi_rdata[20]_i_2_n_0\
    );
\axi_rdata[21]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \axi_rdata[21]_i_2_n_0\,
      I1 => axi_araddr(4),
      O => reg_data_out(21)
    );
\axi_rdata[21]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg3_reg[31]_0\(21),
      I1 => \slv_reg2_reg_n_0_[21]\,
      I2 => axi_araddr(3),
      I3 => slv_reg1(21),
      I4 => axi_araddr(2),
      I5 => \slv_reg0_reg_n_0_[21]\,
      O => \axi_rdata[21]_i_2_n_0\
    );
\axi_rdata[22]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \axi_rdata[22]_i_2_n_0\,
      I1 => axi_araddr(4),
      O => reg_data_out(22)
    );
\axi_rdata[22]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg3_reg[31]_0\(22),
      I1 => \slv_reg2_reg_n_0_[22]\,
      I2 => axi_araddr(3),
      I3 => slv_reg1(22),
      I4 => axi_araddr(2),
      I5 => \slv_reg0_reg_n_0_[22]\,
      O => \axi_rdata[22]_i_2_n_0\
    );
\axi_rdata[23]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \axi_rdata[23]_i_2_n_0\,
      I1 => axi_araddr(4),
      O => reg_data_out(23)
    );
\axi_rdata[23]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg3_reg[31]_0\(23),
      I1 => \slv_reg2_reg_n_0_[23]\,
      I2 => axi_araddr(3),
      I3 => slv_reg1(23),
      I4 => axi_araddr(2),
      I5 => \slv_reg0_reg_n_0_[23]\,
      O => \axi_rdata[23]_i_2_n_0\
    );
\axi_rdata[24]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \axi_rdata[24]_i_2_n_0\,
      I1 => axi_araddr(4),
      O => reg_data_out(24)
    );
\axi_rdata[24]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg3_reg[31]_0\(24),
      I1 => \slv_reg2_reg_n_0_[24]\,
      I2 => axi_araddr(3),
      I3 => slv_reg1(24),
      I4 => axi_araddr(2),
      I5 => \slv_reg0_reg_n_0_[24]\,
      O => \axi_rdata[24]_i_2_n_0\
    );
\axi_rdata[25]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \axi_rdata[25]_i_2_n_0\,
      I1 => axi_araddr(4),
      O => reg_data_out(25)
    );
\axi_rdata[25]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg3_reg[31]_0\(25),
      I1 => \slv_reg2_reg_n_0_[25]\,
      I2 => axi_araddr(3),
      I3 => slv_reg1(25),
      I4 => axi_araddr(2),
      I5 => \slv_reg0_reg_n_0_[25]\,
      O => \axi_rdata[25]_i_2_n_0\
    );
\axi_rdata[26]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \axi_rdata[26]_i_2_n_0\,
      I1 => axi_araddr(4),
      O => reg_data_out(26)
    );
\axi_rdata[26]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg3_reg[31]_0\(26),
      I1 => \slv_reg2_reg_n_0_[26]\,
      I2 => axi_araddr(3),
      I3 => slv_reg1(26),
      I4 => axi_araddr(2),
      I5 => \slv_reg0_reg_n_0_[26]\,
      O => \axi_rdata[26]_i_2_n_0\
    );
\axi_rdata[27]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \axi_rdata[27]_i_2_n_0\,
      I1 => axi_araddr(4),
      O => reg_data_out(27)
    );
\axi_rdata[27]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg3_reg[31]_0\(27),
      I1 => \slv_reg2_reg_n_0_[27]\,
      I2 => axi_araddr(3),
      I3 => slv_reg1(27),
      I4 => axi_araddr(2),
      I5 => \slv_reg0_reg_n_0_[27]\,
      O => \axi_rdata[27]_i_2_n_0\
    );
\axi_rdata[28]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \axi_rdata[28]_i_2_n_0\,
      I1 => axi_araddr(4),
      O => reg_data_out(28)
    );
\axi_rdata[28]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg3_reg[31]_0\(28),
      I1 => \slv_reg2_reg_n_0_[28]\,
      I2 => axi_araddr(3),
      I3 => slv_reg1(28),
      I4 => axi_araddr(2),
      I5 => \slv_reg0_reg_n_0_[28]\,
      O => \axi_rdata[28]_i_2_n_0\
    );
\axi_rdata[29]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \axi_rdata[29]_i_2_n_0\,
      I1 => axi_araddr(4),
      O => reg_data_out(29)
    );
\axi_rdata[29]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg3_reg[31]_0\(29),
      I1 => \slv_reg2_reg_n_0_[29]\,
      I2 => axi_araddr(3),
      I3 => slv_reg1(29),
      I4 => axi_araddr(2),
      I5 => \slv_reg0_reg_n_0_[29]\,
      O => \axi_rdata[29]_i_2_n_0\
    );
\axi_rdata[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"40FF4000"
    )
        port map (
      I0 => axi_araddr(3),
      I1 => DATA_RX_r(2),
      I2 => axi_araddr(2),
      I3 => axi_araddr(4),
      I4 => \axi_rdata[2]_i_2_n_0\,
      O => reg_data_out(2)
    );
\axi_rdata[2]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg3_reg[31]_0\(2),
      I1 => \slv_reg2_reg_n_0_[2]\,
      I2 => axi_araddr(3),
      I3 => slv_reg1(2),
      I4 => axi_araddr(2),
      I5 => \slv_reg0_reg_n_0_[2]\,
      O => \axi_rdata[2]_i_2_n_0\
    );
\axi_rdata[30]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \axi_rdata[30]_i_2_n_0\,
      I1 => axi_araddr(4),
      O => reg_data_out(30)
    );
\axi_rdata[30]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg3_reg[31]_0\(30),
      I1 => \slv_reg2_reg_n_0_[30]\,
      I2 => axi_araddr(3),
      I3 => slv_reg1(30),
      I4 => axi_araddr(2),
      I5 => \slv_reg0_reg_n_0_[30]\,
      O => \axi_rdata[30]_i_2_n_0\
    );
\axi_rdata[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \axi_rdata[31]_i_2_n_0\,
      I1 => axi_araddr(4),
      O => reg_data_out(31)
    );
\axi_rdata[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg3_reg[31]_0\(31),
      I1 => \slv_reg2_reg_n_0_[31]\,
      I2 => axi_araddr(3),
      I3 => slv_reg1(31),
      I4 => axi_araddr(2),
      I5 => \slv_reg0_reg_n_0_[31]\,
      O => \axi_rdata[31]_i_2_n_0\
    );
\axi_rdata[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"40FF4000"
    )
        port map (
      I0 => axi_araddr(3),
      I1 => DATA_RX_r(3),
      I2 => axi_araddr(2),
      I3 => axi_araddr(4),
      I4 => \axi_rdata[3]_i_2_n_0\,
      O => reg_data_out(3)
    );
\axi_rdata[3]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg3_reg[31]_0\(3),
      I1 => \slv_reg2_reg_n_0_[3]\,
      I2 => axi_araddr(3),
      I3 => slv_reg1(3),
      I4 => axi_araddr(2),
      I5 => \slv_reg0_reg_n_0_[3]\,
      O => \axi_rdata[3]_i_2_n_0\
    );
\axi_rdata[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"40FF4000"
    )
        port map (
      I0 => axi_araddr(3),
      I1 => DATA_RX_r(4),
      I2 => axi_araddr(2),
      I3 => axi_araddr(4),
      I4 => \axi_rdata[4]_i_2_n_0\,
      O => reg_data_out(4)
    );
\axi_rdata[4]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg3_reg[31]_0\(4),
      I1 => \slv_reg2_reg_n_0_[4]\,
      I2 => axi_araddr(3),
      I3 => slv_reg1(4),
      I4 => axi_araddr(2),
      I5 => \slv_reg0_reg_n_0_[4]\,
      O => \axi_rdata[4]_i_2_n_0\
    );
\axi_rdata[5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"40FF4000"
    )
        port map (
      I0 => axi_araddr(3),
      I1 => DATA_RX_r(5),
      I2 => axi_araddr(2),
      I3 => axi_araddr(4),
      I4 => \axi_rdata[5]_i_2_n_0\,
      O => reg_data_out(5)
    );
\axi_rdata[5]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg3_reg[31]_0\(5),
      I1 => \slv_reg2_reg_n_0_[5]\,
      I2 => axi_araddr(3),
      I3 => slv_reg1(5),
      I4 => axi_araddr(2),
      I5 => \slv_reg0_reg_n_0_[5]\,
      O => \axi_rdata[5]_i_2_n_0\
    );
\axi_rdata[6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"40FF4000"
    )
        port map (
      I0 => axi_araddr(3),
      I1 => DATA_RX_r(6),
      I2 => axi_araddr(2),
      I3 => axi_araddr(4),
      I4 => \axi_rdata[6]_i_2_n_0\,
      O => reg_data_out(6)
    );
\axi_rdata[6]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg3_reg[31]_0\(6),
      I1 => \slv_reg2_reg_n_0_[6]\,
      I2 => axi_araddr(3),
      I3 => slv_reg1(6),
      I4 => axi_araddr(2),
      I5 => \slv_reg0_reg_n_0_[6]\,
      O => \axi_rdata[6]_i_2_n_0\
    );
\axi_rdata[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"40FF4000"
    )
        port map (
      I0 => axi_araddr(3),
      I1 => DATA_RX_r(7),
      I2 => axi_araddr(2),
      I3 => axi_araddr(4),
      I4 => \axi_rdata[7]_i_2_n_0\,
      O => reg_data_out(7)
    );
\axi_rdata[7]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg3_reg[31]_0\(7),
      I1 => \slv_reg2_reg_n_0_[7]\,
      I2 => axi_araddr(3),
      I3 => slv_reg1(7),
      I4 => axi_araddr(2),
      I5 => \slv_reg0_reg_n_0_[7]\,
      O => \axi_rdata[7]_i_2_n_0\
    );
\axi_rdata[8]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \axi_rdata[8]_i_2_n_0\,
      I1 => axi_araddr(4),
      O => reg_data_out(8)
    );
\axi_rdata[8]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg3_reg[31]_0\(8),
      I1 => \slv_reg2_reg_n_0_[8]\,
      I2 => axi_araddr(3),
      I3 => slv_reg1(8),
      I4 => axi_araddr(2),
      I5 => \slv_reg0_reg_n_0_[8]\,
      O => \axi_rdata[8]_i_2_n_0\
    );
\axi_rdata[9]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \axi_rdata[9]_i_2_n_0\,
      I1 => axi_araddr(4),
      O => reg_data_out(9)
    );
\axi_rdata[9]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^slv_reg3_reg[31]_0\(9),
      I1 => \slv_reg2_reg_n_0_[9]\,
      I2 => axi_araddr(3),
      I3 => slv_reg1(9),
      I4 => axi_araddr(2),
      I5 => \slv_reg0_reg_n_0_[9]\,
      O => \axi_rdata[9]_i_2_n_0\
    );
\axi_rdata_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(0),
      Q => s00_axi_rdata(0),
      R => \^s00_axi_aresetn_0\
    );
\axi_rdata_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(10),
      Q => s00_axi_rdata(10),
      R => \^s00_axi_aresetn_0\
    );
\axi_rdata_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(11),
      Q => s00_axi_rdata(11),
      R => \^s00_axi_aresetn_0\
    );
\axi_rdata_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(12),
      Q => s00_axi_rdata(12),
      R => \^s00_axi_aresetn_0\
    );
\axi_rdata_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(13),
      Q => s00_axi_rdata(13),
      R => \^s00_axi_aresetn_0\
    );
\axi_rdata_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(14),
      Q => s00_axi_rdata(14),
      R => \^s00_axi_aresetn_0\
    );
\axi_rdata_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(15),
      Q => s00_axi_rdata(15),
      R => \^s00_axi_aresetn_0\
    );
\axi_rdata_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(16),
      Q => s00_axi_rdata(16),
      R => \^s00_axi_aresetn_0\
    );
\axi_rdata_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(17),
      Q => s00_axi_rdata(17),
      R => \^s00_axi_aresetn_0\
    );
\axi_rdata_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(18),
      Q => s00_axi_rdata(18),
      R => \^s00_axi_aresetn_0\
    );
\axi_rdata_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(19),
      Q => s00_axi_rdata(19),
      R => \^s00_axi_aresetn_0\
    );
\axi_rdata_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(1),
      Q => s00_axi_rdata(1),
      R => \^s00_axi_aresetn_0\
    );
\axi_rdata_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(20),
      Q => s00_axi_rdata(20),
      R => \^s00_axi_aresetn_0\
    );
\axi_rdata_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(21),
      Q => s00_axi_rdata(21),
      R => \^s00_axi_aresetn_0\
    );
\axi_rdata_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(22),
      Q => s00_axi_rdata(22),
      R => \^s00_axi_aresetn_0\
    );
\axi_rdata_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(23),
      Q => s00_axi_rdata(23),
      R => \^s00_axi_aresetn_0\
    );
\axi_rdata_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(24),
      Q => s00_axi_rdata(24),
      R => \^s00_axi_aresetn_0\
    );
\axi_rdata_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(25),
      Q => s00_axi_rdata(25),
      R => \^s00_axi_aresetn_0\
    );
\axi_rdata_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(26),
      Q => s00_axi_rdata(26),
      R => \^s00_axi_aresetn_0\
    );
\axi_rdata_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(27),
      Q => s00_axi_rdata(27),
      R => \^s00_axi_aresetn_0\
    );
\axi_rdata_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(28),
      Q => s00_axi_rdata(28),
      R => \^s00_axi_aresetn_0\
    );
\axi_rdata_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(29),
      Q => s00_axi_rdata(29),
      R => \^s00_axi_aresetn_0\
    );
\axi_rdata_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(2),
      Q => s00_axi_rdata(2),
      R => \^s00_axi_aresetn_0\
    );
\axi_rdata_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(30),
      Q => s00_axi_rdata(30),
      R => \^s00_axi_aresetn_0\
    );
\axi_rdata_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(31),
      Q => s00_axi_rdata(31),
      R => \^s00_axi_aresetn_0\
    );
\axi_rdata_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(3),
      Q => s00_axi_rdata(3),
      R => \^s00_axi_aresetn_0\
    );
\axi_rdata_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(4),
      Q => s00_axi_rdata(4),
      R => \^s00_axi_aresetn_0\
    );
\axi_rdata_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(5),
      Q => s00_axi_rdata(5),
      R => \^s00_axi_aresetn_0\
    );
\axi_rdata_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(6),
      Q => s00_axi_rdata(6),
      R => \^s00_axi_aresetn_0\
    );
\axi_rdata_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(7),
      Q => s00_axi_rdata(7),
      R => \^s00_axi_aresetn_0\
    );
\axi_rdata_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(8),
      Q => s00_axi_rdata(8),
      R => \^s00_axi_aresetn_0\
    );
\axi_rdata_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(9),
      Q => s00_axi_rdata(9),
      R => \^s00_axi_aresetn_0\
    );
axi_rvalid_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"08F8"
    )
        port map (
      I0 => \^s00_axi_arready\,
      I1 => s00_axi_arvalid,
      I2 => \^s00_axi_rvalid\,
      I3 => s00_axi_rready,
      O => axi_rvalid_i_1_n_0
    );
axi_rvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_rvalid_i_1_n_0,
      Q => \^s00_axi_rvalid\,
      R => \^s00_axi_aresetn_0\
    );
axi_wready_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0800"
    )
        port map (
      I0 => s00_axi_awvalid,
      I1 => s00_axi_wvalid,
      I2 => \^s00_axi_wready\,
      I3 => aw_en_reg_n_0,
      O => axi_wready0
    );
axi_wready_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_wready0,
      Q => \^s00_axi_wready\,
      R => \^s00_axi_aresetn_0\
    );
\slv_reg0[15]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00020000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => axi_awaddr(3),
      I2 => axi_awaddr(2),
      I3 => axi_awaddr(4),
      I4 => s00_axi_wstrb(1),
      O => \slv_reg0[15]_i_1_n_0\
    );
\slv_reg0[23]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00020000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => axi_awaddr(3),
      I2 => axi_awaddr(2),
      I3 => axi_awaddr(4),
      I4 => s00_axi_wstrb(2),
      O => \slv_reg0[23]_i_1_n_0\
    );
\slv_reg0[31]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00020000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => axi_awaddr(3),
      I2 => axi_awaddr(2),
      I3 => axi_awaddr(4),
      I4 => s00_axi_wstrb(3),
      O => \slv_reg0[31]_i_1_n_0\
    );
\slv_reg0[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00020000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => axi_awaddr(3),
      I2 => axi_awaddr(2),
      I3 => axi_awaddr(4),
      I4 => s00_axi_wstrb(0),
      O => \slv_reg0[7]_i_1_n_0\
    );
\slv_reg0_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => \slv_reg0_reg_n_0_[0]\,
      R => \^s00_axi_aresetn_0\
    );
\slv_reg0_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => \slv_reg0_reg_n_0_[10]\,
      R => \^s00_axi_aresetn_0\
    );
\slv_reg0_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => \slv_reg0_reg_n_0_[11]\,
      R => \^s00_axi_aresetn_0\
    );
\slv_reg0_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => \^slv_reg0_reg[12]_0\(0),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg0_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => DATA_TX(0),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg0_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => DATA_TX(1),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg0_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => DATA_TX(2),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg0_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => DATA_TX(3),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg0_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => DATA_TX(4),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg0_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => DATA_TX(5),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg0_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => DATA_TX(6),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg0_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => \slv_reg0_reg_n_0_[1]\,
      R => \^s00_axi_aresetn_0\
    );
\slv_reg0_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => DATA_TX(7),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg0_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => \slv_reg0_reg_n_0_[21]\,
      R => \^s00_axi_aresetn_0\
    );
\slv_reg0_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => \slv_reg0_reg_n_0_[22]\,
      R => \^s00_axi_aresetn_0\
    );
\slv_reg0_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => \slv_reg0_reg_n_0_[23]\,
      R => \^s00_axi_aresetn_0\
    );
\slv_reg0_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => \slv_reg0_reg_n_0_[24]\,
      R => \^s00_axi_aresetn_0\
    );
\slv_reg0_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => \slv_reg0_reg_n_0_[25]\,
      R => \^s00_axi_aresetn_0\
    );
\slv_reg0_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => \slv_reg0_reg_n_0_[26]\,
      R => \^s00_axi_aresetn_0\
    );
\slv_reg0_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => \slv_reg0_reg_n_0_[27]\,
      R => \^s00_axi_aresetn_0\
    );
\slv_reg0_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => \slv_reg0_reg_n_0_[28]\,
      R => \^s00_axi_aresetn_0\
    );
\slv_reg0_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => \slv_reg0_reg_n_0_[29]\,
      R => \^s00_axi_aresetn_0\
    );
\slv_reg0_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => \slv_reg0_reg_n_0_[2]\,
      R => \^s00_axi_aresetn_0\
    );
\slv_reg0_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => \slv_reg0_reg_n_0_[30]\,
      R => \^s00_axi_aresetn_0\
    );
\slv_reg0_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => \slv_reg0_reg_n_0_[31]\,
      R => \^s00_axi_aresetn_0\
    );
\slv_reg0_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => \slv_reg0_reg_n_0_[3]\,
      R => \^s00_axi_aresetn_0\
    );
\slv_reg0_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => \slv_reg0_reg_n_0_[4]\,
      R => \^s00_axi_aresetn_0\
    );
\slv_reg0_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => \slv_reg0_reg_n_0_[5]\,
      R => \^s00_axi_aresetn_0\
    );
\slv_reg0_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => \slv_reg0_reg_n_0_[6]\,
      R => \^s00_axi_aresetn_0\
    );
\slv_reg0_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => \slv_reg0_reg_n_0_[7]\,
      R => \^s00_axi_aresetn_0\
    );
\slv_reg0_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => \slv_reg0_reg_n_0_[8]\,
      R => \^s00_axi_aresetn_0\
    );
\slv_reg0_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg0[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => \slv_reg0_reg_n_0_[9]\,
      R => \^s00_axi_aresetn_0\
    );
\slv_reg1[15]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => axi_awaddr(4),
      I2 => axi_awaddr(3),
      I3 => s00_axi_wstrb(1),
      I4 => axi_awaddr(2),
      O => \slv_reg1[15]_i_1_n_0\
    );
\slv_reg1[23]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => axi_awaddr(4),
      I2 => axi_awaddr(3),
      I3 => s00_axi_wstrb(2),
      I4 => axi_awaddr(2),
      O => \slv_reg1[23]_i_1_n_0\
    );
\slv_reg1[31]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => axi_awaddr(4),
      I2 => axi_awaddr(3),
      I3 => s00_axi_wstrb(3),
      I4 => axi_awaddr(2),
      O => \slv_reg1[31]_i_1_n_0\
    );
\slv_reg1[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => axi_awaddr(4),
      I2 => axi_awaddr(3),
      I3 => s00_axi_wstrb(0),
      I4 => axi_awaddr(2),
      O => \slv_reg1[7]_i_1_n_0\
    );
\slv_reg1_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => \^slv_reg1_reg[0]_0\(0),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg1_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg1(10),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg1_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg1(11),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg1_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg1(12),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg1_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg1(13),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg1_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg1(14),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg1_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg1(15),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg1_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg1(16),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg1_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg1(17),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg1_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg1(18),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg1_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg1(19),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg1_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => slv_reg1(1),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg1_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg1(20),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg1_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg1(21),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg1_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg1(22),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg1_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg1(23),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg1_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg1(24),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg1_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg1(25),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg1_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg1(26),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg1_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg1(27),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg1_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg1(28),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg1_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg1(29),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg1_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => slv_reg1(2),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg1_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg1(30),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg1_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg1(31),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg1_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg1(3),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg1_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg1(4),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg1_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg1(5),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg1_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg1(6),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg1_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg1(7),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg1_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg1(8),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg1_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg1[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg1(9),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg2[15]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => axi_awaddr(4),
      I2 => axi_awaddr(2),
      I3 => s00_axi_wstrb(1),
      I4 => axi_awaddr(3),
      O => \slv_reg2[15]_i_1_n_0\
    );
\slv_reg2[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => axi_awaddr(4),
      I2 => axi_awaddr(2),
      I3 => s00_axi_wstrb(0),
      I4 => axi_awaddr(3),
      O => \slv_reg2[1]_i_1_n_0\
    );
\slv_reg2[1]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => \^s00_axi_wready\,
      I1 => \^s00_axi_awready\,
      I2 => s00_axi_awvalid,
      I3 => s00_axi_wvalid,
      O => \slv_reg_wren__0\
    );
\slv_reg2[23]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => axi_awaddr(4),
      I2 => axi_awaddr(2),
      I3 => s00_axi_wstrb(2),
      I4 => axi_awaddr(3),
      O => \slv_reg2[23]_i_1_n_0\
    );
\slv_reg2[31]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => axi_awaddr(4),
      I2 => axi_awaddr(2),
      I3 => s00_axi_wstrb(3),
      I4 => axi_awaddr(3),
      O => \slv_reg2[31]_i_1_n_0\
    );
\slv_reg2_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[1]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => \^slv_reg2_reg[1]_0\(0),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg2_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => \slv_reg2_reg_n_0_[10]\,
      R => \^s00_axi_aresetn_0\
    );
\slv_reg2_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => \slv_reg2_reg_n_0_[11]\,
      R => \^s00_axi_aresetn_0\
    );
\slv_reg2_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => \slv_reg2_reg_n_0_[12]\,
      R => \^s00_axi_aresetn_0\
    );
\slv_reg2_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => \slv_reg2_reg_n_0_[13]\,
      R => \^s00_axi_aresetn_0\
    );
\slv_reg2_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => \slv_reg2_reg_n_0_[14]\,
      R => \^s00_axi_aresetn_0\
    );
\slv_reg2_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => \slv_reg2_reg_n_0_[15]\,
      R => \^s00_axi_aresetn_0\
    );
\slv_reg2_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => \slv_reg2_reg_n_0_[16]\,
      R => \^s00_axi_aresetn_0\
    );
\slv_reg2_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => \slv_reg2_reg_n_0_[17]\,
      R => \^s00_axi_aresetn_0\
    );
\slv_reg2_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => \slv_reg2_reg_n_0_[18]\,
      R => \^s00_axi_aresetn_0\
    );
\slv_reg2_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => \slv_reg2_reg_n_0_[19]\,
      R => \^s00_axi_aresetn_0\
    );
\slv_reg2_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[1]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => \^slv_reg2_reg[1]_0\(1),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg2_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => \slv_reg2_reg_n_0_[20]\,
      R => \^s00_axi_aresetn_0\
    );
\slv_reg2_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => \slv_reg2_reg_n_0_[21]\,
      R => \^s00_axi_aresetn_0\
    );
\slv_reg2_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => \slv_reg2_reg_n_0_[22]\,
      R => \^s00_axi_aresetn_0\
    );
\slv_reg2_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => \slv_reg2_reg_n_0_[23]\,
      R => \^s00_axi_aresetn_0\
    );
\slv_reg2_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => \slv_reg2_reg_n_0_[24]\,
      R => \^s00_axi_aresetn_0\
    );
\slv_reg2_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => \slv_reg2_reg_n_0_[25]\,
      R => \^s00_axi_aresetn_0\
    );
\slv_reg2_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => \slv_reg2_reg_n_0_[26]\,
      R => \^s00_axi_aresetn_0\
    );
\slv_reg2_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => \slv_reg2_reg_n_0_[27]\,
      R => \^s00_axi_aresetn_0\
    );
\slv_reg2_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => \slv_reg2_reg_n_0_[28]\,
      R => \^s00_axi_aresetn_0\
    );
\slv_reg2_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => \slv_reg2_reg_n_0_[29]\,
      R => \^s00_axi_aresetn_0\
    );
\slv_reg2_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[1]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => \slv_reg2_reg_n_0_[2]\,
      R => \^s00_axi_aresetn_0\
    );
\slv_reg2_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => \slv_reg2_reg_n_0_[30]\,
      R => \^s00_axi_aresetn_0\
    );
\slv_reg2_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => \slv_reg2_reg_n_0_[31]\,
      R => \^s00_axi_aresetn_0\
    );
\slv_reg2_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[1]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => \slv_reg2_reg_n_0_[3]\,
      R => \^s00_axi_aresetn_0\
    );
\slv_reg2_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[1]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => \slv_reg2_reg_n_0_[4]\,
      R => \^s00_axi_aresetn_0\
    );
\slv_reg2_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[1]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => \slv_reg2_reg_n_0_[5]\,
      R => \^s00_axi_aresetn_0\
    );
\slv_reg2_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[1]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => \slv_reg2_reg_n_0_[6]\,
      R => \^s00_axi_aresetn_0\
    );
\slv_reg2_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[1]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => \slv_reg2_reg_n_0_[7]\,
      R => \^s00_axi_aresetn_0\
    );
\slv_reg2_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => \slv_reg2_reg_n_0_[8]\,
      R => \^s00_axi_aresetn_0\
    );
\slv_reg2_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => \slv_reg2_reg_n_0_[9]\,
      R => \^s00_axi_aresetn_0\
    );
\slv_reg3[15]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"20000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => axi_awaddr(4),
      I2 => axi_awaddr(3),
      I3 => axi_awaddr(2),
      I4 => s00_axi_wstrb(1),
      O => \slv_reg3[15]_i_1_n_0\
    );
\slv_reg3[23]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"20000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => axi_awaddr(4),
      I2 => axi_awaddr(3),
      I3 => axi_awaddr(2),
      I4 => s00_axi_wstrb(2),
      O => \slv_reg3[23]_i_1_n_0\
    );
\slv_reg3[31]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"20000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => axi_awaddr(4),
      I2 => axi_awaddr(3),
      I3 => axi_awaddr(2),
      I4 => s00_axi_wstrb(3),
      O => \slv_reg3[31]_i_1_n_0\
    );
\slv_reg3[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"20000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => axi_awaddr(4),
      I2 => axi_awaddr(3),
      I3 => axi_awaddr(2),
      I4 => s00_axi_wstrb(0),
      O => \slv_reg3[7]_i_1_n_0\
    );
\slv_reg3_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => \^slv_reg3_reg[31]_0\(0),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg3_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => \^slv_reg3_reg[31]_0\(10),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg3_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => \^slv_reg3_reg[31]_0\(11),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg3_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => \^slv_reg3_reg[31]_0\(12),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg3_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => \^slv_reg3_reg[31]_0\(13),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg3_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => \^slv_reg3_reg[31]_0\(14),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg3_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => \^slv_reg3_reg[31]_0\(15),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg3_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => \^slv_reg3_reg[31]_0\(16),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg3_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => \^slv_reg3_reg[31]_0\(17),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg3_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => \^slv_reg3_reg[31]_0\(18),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg3_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => \^slv_reg3_reg[31]_0\(19),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg3_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => \^slv_reg3_reg[31]_0\(1),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg3_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => \^slv_reg3_reg[31]_0\(20),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg3_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => \^slv_reg3_reg[31]_0\(21),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg3_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => \^slv_reg3_reg[31]_0\(22),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg3_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => \^slv_reg3_reg[31]_0\(23),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg3_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => \^slv_reg3_reg[31]_0\(24),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg3_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => \^slv_reg3_reg[31]_0\(25),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg3_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => \^slv_reg3_reg[31]_0\(26),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg3_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => \^slv_reg3_reg[31]_0\(27),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg3_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => \^slv_reg3_reg[31]_0\(28),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg3_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => \^slv_reg3_reg[31]_0\(29),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg3_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => \^slv_reg3_reg[31]_0\(2),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg3_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => \^slv_reg3_reg[31]_0\(30),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg3_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => \^slv_reg3_reg[31]_0\(31),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg3_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => \^slv_reg3_reg[31]_0\(3),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg3_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => \^slv_reg3_reg[31]_0\(4),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg3_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => \^slv_reg3_reg[31]_0\(5),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg3_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => \^slv_reg3_reg[31]_0\(6),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg3_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => \^slv_reg3_reg[31]_0\(7),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg3_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => \^slv_reg3_reg[31]_0\(8),
      R => \^s00_axi_aresetn_0\
    );
\slv_reg3_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => \^slv_reg3_reg[31]_0\(9),
      R => \^s00_axi_aresetn_0\
    );
slv_reg_rden: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => s00_axi_arvalid,
      I1 => \^s00_axi_rvalid\,
      I2 => \^s00_axi_arready\,
      O => \slv_reg_rden__0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_AD9650_0_0_spi_AD9650 is
  port (
    adc_spi_cs : out STD_LOGIC;
    data4 : out STD_LOGIC_VECTOR ( 0 to 0 );
    Q : out STD_LOGIC_VECTOR ( 1 downto 0 );
    adc_spi_sck : out STD_LOGIC;
    \cnt_reg[2]_0\ : out STD_LOGIC;
    DATA_RX_r : out STD_LOGIC_VECTOR ( 7 downto 0 );
    adc_spi_sdio : inout STD_LOGIC;
    start_sync_reg_0 : in STD_LOGIC_VECTOR ( 0 to 0 );
    clk_10MHz : in STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    \cnt_re_reg[0]_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    DATA_TX_serial_r_reg_0 : in STD_LOGIC;
    DATA_TX_serial_r_i_3_0 : in STD_LOGIC_VECTOR ( 0 to 0 );
    DATA_TX_serial_r_i_3_1 : in STD_LOGIC;
    DATA_TX_serial_r_reg_1 : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_AD9650_0_0_spi_AD9650 : entity is "spi_AD9650";
end design_1_AD9650_0_0_spi_AD9650;

architecture STRUCTURE of design_1_AD9650_0_0_spi_AD9650 is
  signal \^data_rx_r\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \DATA_RX_r1_inferred__0/i__carry__0_n_0\ : STD_LOGIC;
  signal \DATA_RX_r1_inferred__0/i__carry__0_n_1\ : STD_LOGIC;
  signal \DATA_RX_r1_inferred__0/i__carry__0_n_2\ : STD_LOGIC;
  signal \DATA_RX_r1_inferred__0/i__carry__0_n_3\ : STD_LOGIC;
  signal \DATA_RX_r1_inferred__0/i__carry__0_n_4\ : STD_LOGIC;
  signal \DATA_RX_r1_inferred__0/i__carry__0_n_5\ : STD_LOGIC;
  signal \DATA_RX_r1_inferred__0/i__carry__0_n_6\ : STD_LOGIC;
  signal \DATA_RX_r1_inferred__0/i__carry__0_n_7\ : STD_LOGIC;
  signal \DATA_RX_r1_inferred__0/i__carry__1_n_0\ : STD_LOGIC;
  signal \DATA_RX_r1_inferred__0/i__carry__1_n_1\ : STD_LOGIC;
  signal \DATA_RX_r1_inferred__0/i__carry__1_n_2\ : STD_LOGIC;
  signal \DATA_RX_r1_inferred__0/i__carry__1_n_3\ : STD_LOGIC;
  signal \DATA_RX_r1_inferred__0/i__carry__1_n_4\ : STD_LOGIC;
  signal \DATA_RX_r1_inferred__0/i__carry__1_n_5\ : STD_LOGIC;
  signal \DATA_RX_r1_inferred__0/i__carry__1_n_6\ : STD_LOGIC;
  signal \DATA_RX_r1_inferred__0/i__carry__1_n_7\ : STD_LOGIC;
  signal \DATA_RX_r1_inferred__0/i__carry__2_n_0\ : STD_LOGIC;
  signal \DATA_RX_r1_inferred__0/i__carry__2_n_1\ : STD_LOGIC;
  signal \DATA_RX_r1_inferred__0/i__carry__2_n_2\ : STD_LOGIC;
  signal \DATA_RX_r1_inferred__0/i__carry__2_n_3\ : STD_LOGIC;
  signal \DATA_RX_r1_inferred__0/i__carry__2_n_4\ : STD_LOGIC;
  signal \DATA_RX_r1_inferred__0/i__carry__2_n_5\ : STD_LOGIC;
  signal \DATA_RX_r1_inferred__0/i__carry__2_n_6\ : STD_LOGIC;
  signal \DATA_RX_r1_inferred__0/i__carry__2_n_7\ : STD_LOGIC;
  signal \DATA_RX_r1_inferred__0/i__carry_n_0\ : STD_LOGIC;
  signal \DATA_RX_r1_inferred__0/i__carry_n_1\ : STD_LOGIC;
  signal \DATA_RX_r1_inferred__0/i__carry_n_2\ : STD_LOGIC;
  signal \DATA_RX_r1_inferred__0/i__carry_n_3\ : STD_LOGIC;
  signal \DATA_RX_r1_inferred__0/i__carry_n_4\ : STD_LOGIC;
  signal \DATA_RX_r1_inferred__0/i__carry_n_5\ : STD_LOGIC;
  signal \DATA_RX_r1_inferred__0/i__carry_n_6\ : STD_LOGIC;
  signal \DATA_RX_r[0]_i_1_n_0\ : STD_LOGIC;
  signal \DATA_RX_r[1]_i_1_n_0\ : STD_LOGIC;
  signal \DATA_RX_r[2]_i_1_n_0\ : STD_LOGIC;
  signal \DATA_RX_r[3]_i_1_n_0\ : STD_LOGIC;
  signal \DATA_RX_r[3]_i_2_n_0\ : STD_LOGIC;
  signal \DATA_RX_r[4]_i_1_n_0\ : STD_LOGIC;
  signal \DATA_RX_r[4]_i_2_n_0\ : STD_LOGIC;
  signal \DATA_RX_r[5]_i_1_n_0\ : STD_LOGIC;
  signal \DATA_RX_r[5]_i_2_n_0\ : STD_LOGIC;
  signal \DATA_RX_r[6]_i_1_n_0\ : STD_LOGIC;
  signal \DATA_RX_r[6]_i_2_n_0\ : STD_LOGIC;
  signal \DATA_RX_r[7]_i_1_n_0\ : STD_LOGIC;
  signal \DATA_RX_r[7]_i_2_n_0\ : STD_LOGIC;
  signal \DATA_RX_r[7]_i_3_n_0\ : STD_LOGIC;
  signal \DATA_RX_r[7]_i_4_n_0\ : STD_LOGIC;
  signal \DATA_RX_r[7]_i_5_n_0\ : STD_LOGIC;
  signal \DATA_RX_r[7]_i_6_n_0\ : STD_LOGIC;
  signal \DATA_RX_r[7]_i_7_n_0\ : STD_LOGIC;
  signal \DATA_RX_r[7]_i_9_n_0\ : STD_LOGIC;
  signal \DATA_RX_r_reg[7]_i_8_n_3\ : STD_LOGIC;
  signal DATA_RX_serial : STD_LOGIC;
  signal DATA_TX_serial : STD_LOGIC;
  signal DATA_TX_serial_r_i_10_n_0 : STD_LOGIC;
  signal DATA_TX_serial_r_i_1_n_0 : STD_LOGIC;
  signal DATA_TX_serial_r_i_2_n_0 : STD_LOGIC;
  signal DATA_TX_serial_r_i_3_n_0 : STD_LOGIC;
  signal DATA_TX_serial_r_i_5_n_0 : STD_LOGIC;
  signal DATA_TX_serial_r_i_6_n_0 : STD_LOGIC;
  signal DATA_TX_serial_r_i_8_n_0 : STD_LOGIC;
  signal DATA_TX_serial_r_i_9_n_0 : STD_LOGIC;
  signal \^q\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \^adc_spi_cs\ : STD_LOGIC;
  signal \cnt[2]_i_1_n_0\ : STD_LOGIC;
  signal \cnt[3]_i_1_n_0\ : STD_LOGIC;
  signal \cnt[4]_i_1_n_0\ : STD_LOGIC;
  signal \cnt[5]_i_1_n_0\ : STD_LOGIC;
  signal \cnt[7]_i_1_n_0\ : STD_LOGIC;
  signal \cnt[7]_i_3_n_0\ : STD_LOGIC;
  signal \cnt[7]_i_4_n_0\ : STD_LOGIC;
  signal cnt_re : STD_LOGIC;
  signal \cnt_re[0]_i_3_n_0\ : STD_LOGIC;
  signal \cnt_re[0]_i_4_n_0\ : STD_LOGIC;
  signal \cnt_re[0]_i_5_n_0\ : STD_LOGIC;
  signal \cnt_re[0]_i_6_n_0\ : STD_LOGIC;
  signal cnt_re_reg : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \cnt_re_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \cnt_re_reg[0]_i_2_n_1\ : STD_LOGIC;
  signal \cnt_re_reg[0]_i_2_n_2\ : STD_LOGIC;
  signal \cnt_re_reg[0]_i_2_n_3\ : STD_LOGIC;
  signal \cnt_re_reg[0]_i_2_n_4\ : STD_LOGIC;
  signal \cnt_re_reg[0]_i_2_n_5\ : STD_LOGIC;
  signal \cnt_re_reg[0]_i_2_n_6\ : STD_LOGIC;
  signal \cnt_re_reg[0]_i_2_n_7\ : STD_LOGIC;
  signal \cnt_re_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_re_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_re_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_re_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \cnt_re_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \cnt_re_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \cnt_re_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \cnt_re_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_re_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_re_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_re_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_re_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \cnt_re_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \cnt_re_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \cnt_re_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \cnt_re_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_re_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_re_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_re_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_re_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \cnt_re_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \cnt_re_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \cnt_re_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal cnt_reg : STD_LOGIC_VECTOR ( 7 downto 2 );
  signal cs_r : STD_LOGIC;
  signal cs_r_i_1_n_0 : STD_LOGIC;
  signal cs_r_i_3_n_0 : STD_LOGIC;
  signal \^data4\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal data_rx_ready_i_1_n_0 : STD_LOGIC;
  signal data_rx_ready_i_2_n_0 : STD_LOGIC;
  signal data_rx_ready_i_3_n_0 : STD_LOGIC;
  signal data_rx_ready_i_4_n_0 : STD_LOGIC;
  signal enable_cnt_i_1_n_0 : STD_LOGIC;
  signal enable_cnt_re : STD_LOGIC;
  signal enable_sck : STD_LOGIC;
  signal enable_sck_i_1_n_0 : STD_LOGIC;
  signal enable_sck_i_2_n_0 : STD_LOGIC;
  signal \i__carry__0_i_1_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_2_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_3_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_4_n_0\ : STD_LOGIC;
  signal \i__carry__1_i_1_n_0\ : STD_LOGIC;
  signal \i__carry__1_i_2_n_0\ : STD_LOGIC;
  signal \i__carry__1_i_3_n_0\ : STD_LOGIC;
  signal \i__carry__1_i_4_n_0\ : STD_LOGIC;
  signal \i__carry__2_i_1_n_0\ : STD_LOGIC;
  signal \i__carry__2_i_2_n_0\ : STD_LOGIC;
  signal \i__carry__2_i_3_n_0\ : STD_LOGIC;
  signal \i__carry__2_i_4_n_0\ : STD_LOGIC;
  signal \i__carry_i_1_n_0\ : STD_LOGIC;
  signal \i__carry_i_2_n_0\ : STD_LOGIC;
  signal \i__carry_i_3_n_0\ : STD_LOGIC;
  signal \i__carry_i_4_n_0\ : STD_LOGIC;
  signal p_0_in : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal rx_wire : STD_LOGIC;
  signal start_prev : STD_LOGIC;
  signal start_prev_fe_i_1_n_0 : STD_LOGIC;
  signal start_prev_fe_i_2_n_0 : STD_LOGIC;
  signal start_prev_fe_i_3_n_0 : STD_LOGIC;
  signal start_prev_fe_i_4_n_0 : STD_LOGIC;
  signal start_prev_fe_i_5_n_0 : STD_LOGIC;
  signal start_sync : STD_LOGIC;
  signal tristate : STD_LOGIC;
  signal tristate_i_1_n_0 : STD_LOGIC;
  signal tristate_i_2_n_0 : STD_LOGIC;
  signal tristate_i_3_n_0 : STD_LOGIC;
  signal tristate_i_4_n_0 : STD_LOGIC;
  signal tx_wire : STD_LOGIC;
  signal \NLW_DATA_RX_r1_inferred__0/i__carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_DATA_RX_r_reg[7]_i_8_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_DATA_RX_r_reg[7]_i_8_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_cnt_re_reg[12]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \DATA_RX_r[3]_i_2\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \DATA_RX_r[4]_i_2\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \DATA_RX_r[5]_i_2\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \DATA_RX_r[6]_i_2\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \DATA_RX_r[7]_i_2\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \DATA_RX_r[7]_i_3\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \DATA_RX_r[7]_i_9\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of DATA_TX_serial_r_i_8 : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of DATA_TX_serial_r_i_9 : label is "soft_lutpair15";
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of IBUF_inst : label is "PRIMITIVE";
  attribute CAPACITANCE : string;
  attribute CAPACITANCE of IBUF_inst : label is "DONT_CARE";
  attribute IBUF_DELAY_VALUE : string;
  attribute IBUF_DELAY_VALUE of IBUF_inst : label is "0";
  attribute IFD_DELAY_VALUE : string;
  attribute IFD_DELAY_VALUE of IBUF_inst : label is "AUTO";
  attribute BOX_TYPE of OBUFT_inst : label is "PRIMITIVE";
  attribute CAPACITANCE of OBUFT_inst : label is "DONT_CARE";
  attribute SOFT_HLUTNM of \cnt[0]_i_1\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \cnt[1]_i_1\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \cnt[2]_i_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \cnt[3]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \cnt[4]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \cnt[7]_i_2\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \cnt[7]_i_3\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \cnt_re[0]_i_6\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of cs_r_i_1 : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of data_rx_ready_i_2 : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of enable_cnt_i_1 : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of enable_sck_i_2 : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of tristate_i_2 : label is "soft_lutpair18";
begin
  DATA_RX_r(7 downto 0) <= \^data_rx_r\(7 downto 0);
  Q(1 downto 0) <= \^q\(1 downto 0);
  adc_spi_cs <= \^adc_spi_cs\;
  data4(0) <= \^data4\(0);
\DATA_RX_r1_inferred__0/i__carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \DATA_RX_r1_inferred__0/i__carry_n_0\,
      CO(2) => \DATA_RX_r1_inferred__0/i__carry_n_1\,
      CO(1) => \DATA_RX_r1_inferred__0/i__carry_n_2\,
      CO(0) => \DATA_RX_r1_inferred__0/i__carry_n_3\,
      CYINIT => '0',
      DI(3) => \i__carry_i_1_n_0\,
      DI(2) => \i__carry_i_2_n_0\,
      DI(1) => \i__carry_i_3_n_0\,
      DI(0) => '0',
      O(3) => \DATA_RX_r1_inferred__0/i__carry_n_4\,
      O(2) => \DATA_RX_r1_inferred__0/i__carry_n_5\,
      O(1) => \DATA_RX_r1_inferred__0/i__carry_n_6\,
      O(0) => \NLW_DATA_RX_r1_inferred__0/i__carry_O_UNCONNECTED\(0),
      S(3 downto 1) => cnt_re_reg(3 downto 1),
      S(0) => \i__carry_i_4_n_0\
    );
\DATA_RX_r1_inferred__0/i__carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \DATA_RX_r1_inferred__0/i__carry_n_0\,
      CO(3) => \DATA_RX_r1_inferred__0/i__carry__0_n_0\,
      CO(2) => \DATA_RX_r1_inferred__0/i__carry__0_n_1\,
      CO(1) => \DATA_RX_r1_inferred__0/i__carry__0_n_2\,
      CO(0) => \DATA_RX_r1_inferred__0/i__carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \i__carry__0_i_1_n_0\,
      O(3) => \DATA_RX_r1_inferred__0/i__carry__0_n_4\,
      O(2) => \DATA_RX_r1_inferred__0/i__carry__0_n_5\,
      O(1) => \DATA_RX_r1_inferred__0/i__carry__0_n_6\,
      O(0) => \DATA_RX_r1_inferred__0/i__carry__0_n_7\,
      S(3) => \i__carry__0_i_2_n_0\,
      S(2) => \i__carry__0_i_3_n_0\,
      S(1) => \i__carry__0_i_4_n_0\,
      S(0) => cnt_re_reg(4)
    );
\DATA_RX_r1_inferred__0/i__carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \DATA_RX_r1_inferred__0/i__carry__0_n_0\,
      CO(3) => \DATA_RX_r1_inferred__0/i__carry__1_n_0\,
      CO(2) => \DATA_RX_r1_inferred__0/i__carry__1_n_1\,
      CO(1) => \DATA_RX_r1_inferred__0/i__carry__1_n_2\,
      CO(0) => \DATA_RX_r1_inferred__0/i__carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \DATA_RX_r1_inferred__0/i__carry__1_n_4\,
      O(2) => \DATA_RX_r1_inferred__0/i__carry__1_n_5\,
      O(1) => \DATA_RX_r1_inferred__0/i__carry__1_n_6\,
      O(0) => \DATA_RX_r1_inferred__0/i__carry__1_n_7\,
      S(3) => \i__carry__1_i_1_n_0\,
      S(2) => \i__carry__1_i_2_n_0\,
      S(1) => \i__carry__1_i_3_n_0\,
      S(0) => \i__carry__1_i_4_n_0\
    );
\DATA_RX_r1_inferred__0/i__carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \DATA_RX_r1_inferred__0/i__carry__1_n_0\,
      CO(3) => \DATA_RX_r1_inferred__0/i__carry__2_n_0\,
      CO(2) => \DATA_RX_r1_inferred__0/i__carry__2_n_1\,
      CO(1) => \DATA_RX_r1_inferred__0/i__carry__2_n_2\,
      CO(0) => \DATA_RX_r1_inferred__0/i__carry__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \DATA_RX_r1_inferred__0/i__carry__2_n_4\,
      O(2) => \DATA_RX_r1_inferred__0/i__carry__2_n_5\,
      O(1) => \DATA_RX_r1_inferred__0/i__carry__2_n_6\,
      O(0) => \DATA_RX_r1_inferred__0/i__carry__2_n_7\,
      S(3) => \i__carry__2_i_1_n_0\,
      S(2) => \i__carry__2_i_2_n_0\,
      S(1) => \i__carry__2_i_3_n_0\,
      S(0) => \i__carry__2_i_4_n_0\
    );
\DATA_RX_r[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => DATA_RX_serial,
      I1 => \DATA_RX_r[4]_i_2_n_0\,
      I2 => \DATA_RX_r[3]_i_2_n_0\,
      I3 => \^data_rx_r\(0),
      O => \DATA_RX_r[0]_i_1_n_0\
    );
\DATA_RX_r[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => DATA_RX_serial,
      I1 => \DATA_RX_r[5]_i_2_n_0\,
      I2 => \DATA_RX_r[3]_i_2_n_0\,
      I3 => \^data_rx_r\(1),
      O => \DATA_RX_r[1]_i_1_n_0\
    );
\DATA_RX_r[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => DATA_RX_serial,
      I1 => \DATA_RX_r[6]_i_2_n_0\,
      I2 => \DATA_RX_r[3]_i_2_n_0\,
      I3 => \^data_rx_r\(2),
      O => \DATA_RX_r[2]_i_1_n_0\
    );
\DATA_RX_r[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => DATA_RX_serial,
      I1 => \DATA_RX_r[7]_i_3_n_0\,
      I2 => \DATA_RX_r[3]_i_2_n_0\,
      I3 => \^data_rx_r\(3),
      O => \DATA_RX_r[3]_i_1_n_0\
    );
\DATA_RX_r[3]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \DATA_RX_r1_inferred__0/i__carry_n_5\,
      I1 => \DATA_RX_r[7]_i_4_n_0\,
      I2 => \DATA_RX_r[7]_i_5_n_0\,
      I3 => \DATA_RX_r[7]_i_6_n_0\,
      O => \DATA_RX_r[3]_i_2_n_0\
    );
\DATA_RX_r[4]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF80"
    )
        port map (
      I0 => DATA_RX_serial,
      I1 => \DATA_RX_r[7]_i_2_n_0\,
      I2 => \DATA_RX_r[4]_i_2_n_0\,
      I3 => \^data_rx_r\(4),
      O => \DATA_RX_r[4]_i_1_n_0\
    );
\DATA_RX_r[4]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0008"
    )
        port map (
      I0 => \cnt_re_reg[0]_0\(0),
      I1 => cnt_re_reg(0),
      I2 => \DATA_RX_r[7]_i_7_n_0\,
      I3 => \DATA_RX_r1_inferred__0/i__carry_n_6\,
      O => \DATA_RX_r[4]_i_2_n_0\
    );
\DATA_RX_r[5]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF80"
    )
        port map (
      I0 => DATA_RX_serial,
      I1 => \DATA_RX_r[7]_i_2_n_0\,
      I2 => \DATA_RX_r[5]_i_2_n_0\,
      I3 => \^data_rx_r\(5),
      O => \DATA_RX_r[5]_i_1_n_0\
    );
\DATA_RX_r[5]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0004"
    )
        port map (
      I0 => cnt_re_reg(0),
      I1 => \cnt_re_reg[0]_0\(0),
      I2 => \DATA_RX_r[7]_i_7_n_0\,
      I3 => \DATA_RX_r1_inferred__0/i__carry_n_6\,
      O => \DATA_RX_r[5]_i_2_n_0\
    );
\DATA_RX_r[6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF80"
    )
        port map (
      I0 => DATA_RX_serial,
      I1 => \DATA_RX_r[7]_i_2_n_0\,
      I2 => \DATA_RX_r[6]_i_2_n_0\,
      I3 => \^data_rx_r\(6),
      O => \DATA_RX_r[6]_i_1_n_0\
    );
\DATA_RX_r[6]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2000"
    )
        port map (
      I0 => \DATA_RX_r1_inferred__0/i__carry_n_6\,
      I1 => \DATA_RX_r[7]_i_7_n_0\,
      I2 => \cnt_re_reg[0]_0\(0),
      I3 => cnt_re_reg(0),
      O => \DATA_RX_r[6]_i_2_n_0\
    );
\DATA_RX_r[7]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF80"
    )
        port map (
      I0 => DATA_RX_serial,
      I1 => \DATA_RX_r[7]_i_2_n_0\,
      I2 => \DATA_RX_r[7]_i_3_n_0\,
      I3 => \^data_rx_r\(7),
      O => \DATA_RX_r[7]_i_1_n_0\
    );
\DATA_RX_r[7]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0002"
    )
        port map (
      I0 => \DATA_RX_r1_inferred__0/i__carry_n_5\,
      I1 => \DATA_RX_r[7]_i_4_n_0\,
      I2 => \DATA_RX_r[7]_i_5_n_0\,
      I3 => \DATA_RX_r[7]_i_6_n_0\,
      O => \DATA_RX_r[7]_i_2_n_0\
    );
\DATA_RX_r[7]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0040"
    )
        port map (
      I0 => cnt_re_reg(0),
      I1 => \cnt_re_reg[0]_0\(0),
      I2 => \DATA_RX_r1_inferred__0/i__carry_n_6\,
      I3 => \DATA_RX_r[7]_i_7_n_0\,
      O => \DATA_RX_r[7]_i_3_n_0\
    );
\DATA_RX_r[7]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \DATA_RX_r1_inferred__0/i__carry__1_n_7\,
      I1 => \DATA_RX_r1_inferred__0/i__carry__1_n_5\,
      I2 => \DATA_RX_r1_inferred__0/i__carry__0_n_7\,
      I3 => \DATA_RX_r1_inferred__0/i__carry__2_n_6\,
      O => \DATA_RX_r[7]_i_4_n_0\
    );
\DATA_RX_r[7]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \DATA_RX_r1_inferred__0/i__carry__0_n_5\,
      I1 => \DATA_RX_r1_inferred__0/i__carry__0_n_4\,
      I2 => \DATA_RX_r1_inferred__0/i__carry__1_n_6\,
      I3 => \DATA_RX_r1_inferred__0/i__carry__2_n_7\,
      O => \DATA_RX_r[7]_i_5_n_0\
    );
\DATA_RX_r[7]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFEFFFFFFFF"
    )
        port map (
      I0 => \DATA_RX_r1_inferred__0/i__carry_n_4\,
      I1 => \DATA_RX_r1_inferred__0/i__carry__0_n_6\,
      I2 => \DATA_RX_r1_inferred__0/i__carry__2_n_4\,
      I3 => \DATA_RX_r1_inferred__0/i__carry__2_n_5\,
      I4 => \DATA_RX_r1_inferred__0/i__carry__1_n_4\,
      I5 => \DATA_RX_r_reg[7]_i_8_n_3\,
      O => \DATA_RX_r[7]_i_6_n_0\
    );
\DATA_RX_r[7]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFEFF"
    )
        port map (
      I0 => \cnt_re[0]_i_4_n_0\,
      I1 => \DATA_RX_r[7]_i_9_n_0\,
      I2 => cnt_re_reg(6),
      I3 => cnt_re_reg(4),
      I4 => cnt_re_reg(15),
      I5 => cnt_re_reg(5),
      O => \DATA_RX_r[7]_i_7_n_0\
    );
\DATA_RX_r[7]_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8077"
    )
        port map (
      I0 => cnt_re_reg(1),
      I1 => cnt_re_reg(2),
      I2 => cnt_re_reg(4),
      I3 => cnt_re_reg(3),
      O => \DATA_RX_r[7]_i_9_n_0\
    );
\DATA_RX_r_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \DATA_RX_r[0]_i_1_n_0\,
      Q => \^data_rx_r\(0),
      R => '0'
    );
\DATA_RX_r_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \DATA_RX_r[1]_i_1_n_0\,
      Q => \^data_rx_r\(1),
      R => '0'
    );
\DATA_RX_r_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \DATA_RX_r[2]_i_1_n_0\,
      Q => \^data_rx_r\(2),
      R => '0'
    );
\DATA_RX_r_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \DATA_RX_r[3]_i_1_n_0\,
      Q => \^data_rx_r\(3),
      R => '0'
    );
\DATA_RX_r_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \DATA_RX_r[4]_i_1_n_0\,
      Q => \^data_rx_r\(4),
      R => '0'
    );
\DATA_RX_r_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \DATA_RX_r[5]_i_1_n_0\,
      Q => \^data_rx_r\(5),
      R => '0'
    );
\DATA_RX_r_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \DATA_RX_r[6]_i_1_n_0\,
      Q => \^data_rx_r\(6),
      R => '0'
    );
\DATA_RX_r_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \DATA_RX_r[7]_i_1_n_0\,
      Q => \^data_rx_r\(7),
      R => '0'
    );
\DATA_RX_r_reg[7]_i_8\: unisim.vcomponents.CARRY4
     port map (
      CI => \DATA_RX_r1_inferred__0/i__carry__2_n_0\,
      CO(3 downto 1) => \NLW_DATA_RX_r_reg[7]_i_8_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \DATA_RX_r_reg[7]_i_8_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_DATA_RX_r_reg[7]_i_8_O_UNCONNECTED\(3 downto 0),
      S(3 downto 0) => B"0001"
    );
DATA_RX_serial_r_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => rx_wire,
      Q => DATA_RX_serial,
      R => '0'
    );
DATA_TX_serial_r_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0F0EEFFF0F0EE00"
    )
        port map (
      I0 => DATA_TX_serial_r_i_2_n_0,
      I1 => DATA_TX_serial_r_i_3_n_0,
      I2 => DATA_TX_serial_r_reg_1,
      I3 => DATA_TX_serial_r_i_5_n_0,
      I4 => DATA_TX_serial_r_i_6_n_0,
      I5 => DATA_TX_serial,
      O => DATA_TX_serial_r_i_1_n_0
    );
DATA_TX_serial_r_i_10: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFEF0F0F0FF"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      I2 => enable_sck_i_2_n_0,
      I3 => cnt_reg(2),
      I4 => cnt_reg(3),
      I5 => cnt_reg(4),
      O => DATA_TX_serial_r_i_10_n_0
    );
DATA_TX_serial_r_i_11: unisim.vcomponents.LUT3
    generic map(
      INIT => X"56"
    )
        port map (
      I0 => cnt_reg(2),
      I1 => \^q\(0),
      I2 => \^q\(1),
      O => \cnt_reg[2]_0\
    );
DATA_TX_serial_r_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0F0F0F0404000F0"
    )
        port map (
      I0 => \cnt[7]_i_3_n_0\,
      I1 => cnt_reg(4),
      I2 => \cnt_re_reg[0]_0\(0),
      I3 => \^q\(1),
      I4 => cs_r_i_3_n_0,
      I5 => enable_sck_i_2_n_0,
      O => DATA_TX_serial_r_i_2_n_0
    );
DATA_TX_serial_r_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000ABFEA802"
    )
        port map (
      I0 => DATA_TX_serial_r_reg_0,
      I1 => cnt_reg(2),
      I2 => DATA_TX_serial_r_i_8_n_0,
      I3 => cnt_reg(3),
      I4 => DATA_TX_serial_r_i_9_n_0,
      I5 => DATA_TX_serial_r_i_10_n_0,
      O => DATA_TX_serial_r_i_3_n_0
    );
DATA_TX_serial_r_i_5: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1111111111111114"
    )
        port map (
      I0 => enable_sck_i_2_n_0,
      I1 => cnt_reg(4),
      I2 => cnt_reg(3),
      I3 => cnt_reg(2),
      I4 => \^q\(1),
      I5 => \^q\(0),
      O => DATA_TX_serial_r_i_5_n_0
    );
DATA_TX_serial_r_i_6: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0010100010001000"
    )
        port map (
      I0 => \cnt_re_reg[0]_0\(0),
      I1 => enable_sck_i_2_n_0,
      I2 => cnt_reg(4),
      I3 => cnt_reg(3),
      I4 => cnt_reg(2),
      I5 => DATA_TX_serial_r_i_8_n_0,
      O => DATA_TX_serial_r_i_6_n_0
    );
DATA_TX_serial_r_i_8: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      O => DATA_TX_serial_r_i_8_n_0
    );
DATA_TX_serial_r_i_9: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EEEB2228"
    )
        port map (
      I0 => DATA_TX_serial_r_i_3_0(0),
      I1 => cnt_reg(2),
      I2 => \^q\(0),
      I3 => \^q\(1),
      I4 => DATA_TX_serial_r_i_3_1,
      O => DATA_TX_serial_r_i_9_n_0
    );
DATA_TX_serial_r_reg: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => DATA_TX_serial_r_i_1_n_0,
      Q => DATA_TX_serial,
      R => '0'
    );
IBUF_inst: unisim.vcomponents.IBUF
    generic map(
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => adc_spi_sdio,
      O => rx_wire
    );
OBUFT_inst: unisim.vcomponents.OBUFT
    generic map(
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => tx_wire,
      O => adc_spi_sdio,
      T => tristate
    );
adc_spi_sck_INST_0: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => enable_sck,
      I1 => clk_10MHz,
      O => adc_spi_sck
    );
\cnt[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(0),
      O => p_0_in(0)
    );
\cnt[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      O => p_0_in(1)
    );
\cnt[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => cnt_reg(2),
      I1 => \^q\(0),
      I2 => \^q\(1),
      O => \cnt[2]_i_1_n_0\
    );
\cnt[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6AAA"
    )
        port map (
      I0 => cnt_reg(3),
      I1 => cnt_reg(2),
      I2 => \^q\(1),
      I3 => \^q\(0),
      O => \cnt[3]_i_1_n_0\
    );
\cnt[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6AAAAAAA"
    )
        port map (
      I0 => cnt_reg(4),
      I1 => cnt_reg(3),
      I2 => \^q\(0),
      I3 => \^q\(1),
      I4 => cnt_reg(2),
      O => \cnt[4]_i_1_n_0\
    );
\cnt[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AAAAAAAAAAAAAAA"
    )
        port map (
      I0 => cnt_reg(5),
      I1 => cnt_reg(4),
      I2 => cnt_reg(2),
      I3 => \^q\(1),
      I4 => \^q\(0),
      I5 => cnt_reg(3),
      O => \cnt[5]_i_1_n_0\
    );
\cnt[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => cnt_reg(6),
      I1 => \cnt[7]_i_4_n_0\,
      O => p_0_in(6)
    );
\cnt[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00080000FFFFFFFF"
    )
        port map (
      I0 => cnt_reg(6),
      I1 => cnt_reg(4),
      I2 => cnt_reg(7),
      I3 => cnt_reg(5),
      I4 => \cnt[7]_i_3_n_0\,
      I5 => start_prev,
      O => \cnt[7]_i_1_n_0\
    );
\cnt[7]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => cnt_reg(7),
      I1 => \cnt[7]_i_4_n_0\,
      I2 => cnt_reg(6),
      O => p_0_in(7)
    );
\cnt[7]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      I2 => cnt_reg(2),
      I3 => cnt_reg(3),
      O => \cnt[7]_i_3_n_0\
    );
\cnt[7]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => cnt_reg(5),
      I1 => cnt_reg(4),
      I2 => cnt_reg(2),
      I3 => \^q\(1),
      I4 => \^q\(0),
      I5 => cnt_reg(3),
      O => \cnt[7]_i_4_n_0\
    );
\cnt_re[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"08AA"
    )
        port map (
      I0 => \cnt_re_reg[0]_0\(0),
      I1 => \cnt_re[0]_i_3_n_0\,
      I2 => \cnt_re[0]_i_4_n_0\,
      I3 => enable_cnt_re,
      O => cnt_re
    );
\cnt_re[0]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000001000"
    )
        port map (
      I0 => \cnt_re[0]_i_6_n_0\,
      I1 => cnt_re_reg(3),
      I2 => cnt_re_reg(6),
      I3 => cnt_re_reg(4),
      I4 => cnt_re_reg(15),
      I5 => cnt_re_reg(5),
      O => \cnt_re[0]_i_3_n_0\
    );
\cnt_re[0]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFD"
    )
        port map (
      I0 => data_rx_ready_i_4_n_0,
      I1 => cnt_re_reg(9),
      I2 => cnt_re_reg(12),
      I3 => cnt_re_reg(10),
      I4 => cnt_re_reg(11),
      O => \cnt_re[0]_i_4_n_0\
    );
\cnt_re[0]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt_re_reg(0),
      O => \cnt_re[0]_i_5_n_0\
    );
\cnt_re[0]_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => cnt_re_reg(2),
      I1 => cnt_re_reg(0),
      I2 => cnt_re_reg(1),
      O => \cnt_re[0]_i_6_n_0\
    );
\cnt_re_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_10MHz,
      CE => \cnt_re_reg[0]_0\(0),
      D => \cnt_re_reg[0]_i_2_n_7\,
      Q => cnt_re_reg(0),
      R => cnt_re
    );
\cnt_re_reg[0]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \cnt_re_reg[0]_i_2_n_0\,
      CO(2) => \cnt_re_reg[0]_i_2_n_1\,
      CO(1) => \cnt_re_reg[0]_i_2_n_2\,
      CO(0) => \cnt_re_reg[0]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \cnt_re_reg[0]_i_2_n_4\,
      O(2) => \cnt_re_reg[0]_i_2_n_5\,
      O(1) => \cnt_re_reg[0]_i_2_n_6\,
      O(0) => \cnt_re_reg[0]_i_2_n_7\,
      S(3 downto 1) => cnt_re_reg(3 downto 1),
      S(0) => \cnt_re[0]_i_5_n_0\
    );
\cnt_re_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_10MHz,
      CE => \cnt_re_reg[0]_0\(0),
      D => \cnt_re_reg[8]_i_1_n_5\,
      Q => cnt_re_reg(10),
      R => cnt_re
    );
\cnt_re_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_10MHz,
      CE => \cnt_re_reg[0]_0\(0),
      D => \cnt_re_reg[8]_i_1_n_4\,
      Q => cnt_re_reg(11),
      R => cnt_re
    );
\cnt_re_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_10MHz,
      CE => \cnt_re_reg[0]_0\(0),
      D => \cnt_re_reg[12]_i_1_n_7\,
      Q => cnt_re_reg(12),
      R => cnt_re
    );
\cnt_re_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_re_reg[8]_i_1_n_0\,
      CO(3) => \NLW_cnt_re_reg[12]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \cnt_re_reg[12]_i_1_n_1\,
      CO(1) => \cnt_re_reg[12]_i_1_n_2\,
      CO(0) => \cnt_re_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt_re_reg[12]_i_1_n_4\,
      O(2) => \cnt_re_reg[12]_i_1_n_5\,
      O(1) => \cnt_re_reg[12]_i_1_n_6\,
      O(0) => \cnt_re_reg[12]_i_1_n_7\,
      S(3 downto 0) => cnt_re_reg(15 downto 12)
    );
\cnt_re_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_10MHz,
      CE => \cnt_re_reg[0]_0\(0),
      D => \cnt_re_reg[12]_i_1_n_6\,
      Q => cnt_re_reg(13),
      R => cnt_re
    );
\cnt_re_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_10MHz,
      CE => \cnt_re_reg[0]_0\(0),
      D => \cnt_re_reg[12]_i_1_n_5\,
      Q => cnt_re_reg(14),
      R => cnt_re
    );
\cnt_re_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_10MHz,
      CE => \cnt_re_reg[0]_0\(0),
      D => \cnt_re_reg[12]_i_1_n_4\,
      Q => cnt_re_reg(15),
      R => cnt_re
    );
\cnt_re_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_10MHz,
      CE => \cnt_re_reg[0]_0\(0),
      D => \cnt_re_reg[0]_i_2_n_6\,
      Q => cnt_re_reg(1),
      R => cnt_re
    );
\cnt_re_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_10MHz,
      CE => \cnt_re_reg[0]_0\(0),
      D => \cnt_re_reg[0]_i_2_n_5\,
      Q => cnt_re_reg(2),
      R => cnt_re
    );
\cnt_re_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_10MHz,
      CE => \cnt_re_reg[0]_0\(0),
      D => \cnt_re_reg[0]_i_2_n_4\,
      Q => cnt_re_reg(3),
      R => cnt_re
    );
\cnt_re_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_10MHz,
      CE => \cnt_re_reg[0]_0\(0),
      D => \cnt_re_reg[4]_i_1_n_7\,
      Q => cnt_re_reg(4),
      R => cnt_re
    );
\cnt_re_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_re_reg[0]_i_2_n_0\,
      CO(3) => \cnt_re_reg[4]_i_1_n_0\,
      CO(2) => \cnt_re_reg[4]_i_1_n_1\,
      CO(1) => \cnt_re_reg[4]_i_1_n_2\,
      CO(0) => \cnt_re_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt_re_reg[4]_i_1_n_4\,
      O(2) => \cnt_re_reg[4]_i_1_n_5\,
      O(1) => \cnt_re_reg[4]_i_1_n_6\,
      O(0) => \cnt_re_reg[4]_i_1_n_7\,
      S(3 downto 0) => cnt_re_reg(7 downto 4)
    );
\cnt_re_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_10MHz,
      CE => \cnt_re_reg[0]_0\(0),
      D => \cnt_re_reg[4]_i_1_n_6\,
      Q => cnt_re_reg(5),
      R => cnt_re
    );
\cnt_re_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_10MHz,
      CE => \cnt_re_reg[0]_0\(0),
      D => \cnt_re_reg[4]_i_1_n_5\,
      Q => cnt_re_reg(6),
      R => cnt_re
    );
\cnt_re_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_10MHz,
      CE => \cnt_re_reg[0]_0\(0),
      D => \cnt_re_reg[4]_i_1_n_4\,
      Q => cnt_re_reg(7),
      R => cnt_re
    );
\cnt_re_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_10MHz,
      CE => \cnt_re_reg[0]_0\(0),
      D => \cnt_re_reg[8]_i_1_n_7\,
      Q => cnt_re_reg(8),
      R => cnt_re
    );
\cnt_re_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_re_reg[4]_i_1_n_0\,
      CO(3) => \cnt_re_reg[8]_i_1_n_0\,
      CO(2) => \cnt_re_reg[8]_i_1_n_1\,
      CO(1) => \cnt_re_reg[8]_i_1_n_2\,
      CO(0) => \cnt_re_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt_re_reg[8]_i_1_n_4\,
      O(2) => \cnt_re_reg[8]_i_1_n_5\,
      O(1) => \cnt_re_reg[8]_i_1_n_6\,
      O(0) => \cnt_re_reg[8]_i_1_n_7\,
      S(3 downto 0) => cnt_re_reg(11 downto 8)
    );
\cnt_re_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_10MHz,
      CE => \cnt_re_reg[0]_0\(0),
      D => \cnt_re_reg[8]_i_1_n_6\,
      Q => cnt_re_reg(9),
      R => cnt_re
    );
\cnt_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => p_0_in(0),
      Q => \^q\(0),
      R => \cnt[7]_i_1_n_0\
    );
\cnt_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => p_0_in(1),
      Q => \^q\(1),
      R => \cnt[7]_i_1_n_0\
    );
\cnt_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt[2]_i_1_n_0\,
      Q => cnt_reg(2),
      R => \cnt[7]_i_1_n_0\
    );
\cnt_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt[3]_i_1_n_0\,
      Q => cnt_reg(3),
      R => \cnt[7]_i_1_n_0\
    );
\cnt_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt[4]_i_1_n_0\,
      Q => cnt_reg(4),
      R => \cnt[7]_i_1_n_0\
    );
\cnt_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt[5]_i_1_n_0\,
      Q => cnt_reg(5),
      R => \cnt[7]_i_1_n_0\
    );
\cnt_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => p_0_in(6),
      Q => cnt_reg(6),
      R => \cnt[7]_i_1_n_0\
    );
\cnt_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => p_0_in(7),
      Q => cnt_reg(7),
      R => \cnt[7]_i_1_n_0\
    );
cs_r_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"E0EE"
    )
        port map (
      I0 => \^adc_spi_cs\,
      I1 => cs_r,
      I2 => start_prev,
      I3 => start_sync,
      O => cs_r_i_1_n_0
    );
cs_r_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000040"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      I2 => cnt_reg(5),
      I3 => cs_r_i_3_n_0,
      I4 => cnt_reg(6),
      I5 => cnt_reg(7),
      O => cs_r
    );
cs_r_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => cnt_reg(4),
      I1 => cnt_reg(3),
      I2 => cnt_reg(2),
      O => cs_r_i_3_n_0
    );
cs_r_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => cs_r_i_1_n_0,
      Q => \^adc_spi_cs\,
      R => '0'
    );
data_rx_ready_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"4F444444"
    )
        port map (
      I0 => data_rx_ready_i_2_n_0,
      I1 => \^data4\(0),
      I2 => data_rx_ready_i_3_n_0,
      I3 => data_rx_ready_i_4_n_0,
      I4 => tristate_i_3_n_0,
      O => data_rx_ready_i_1_n_0
    );
data_rx_ready_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4F"
    )
        port map (
      I0 => enable_cnt_re,
      I1 => start_sync,
      I2 => \cnt_re_reg[0]_0\(0),
      O => data_rx_ready_i_2_n_0
    );
data_rx_ready_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF07FFFFFF"
    )
        port map (
      I0 => cnt_re_reg(4),
      I1 => cnt_re_reg(3),
      I2 => cnt_re_reg(5),
      I3 => cnt_re_reg(1),
      I4 => cnt_re_reg(2),
      I5 => cnt_re_reg(0),
      O => data_rx_ready_i_3_n_0
    );
data_rx_ready_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => cnt_re_reg(8),
      I1 => cnt_re_reg(7),
      I2 => cnt_re_reg(14),
      I3 => cnt_re_reg(13),
      O => data_rx_ready_i_4_n_0
    );
data_rx_ready_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => data_rx_ready_i_1_n_0,
      Q => \^data4\(0),
      R => '0'
    );
enable_cnt_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"75"
    )
        port map (
      I0 => \cnt[7]_i_1_n_0\,
      I1 => start_prev,
      I2 => start_sync,
      O => enable_cnt_i_1_n_0
    );
enable_cnt_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => enable_cnt_i_1_n_0,
      Q => start_prev,
      R => '0'
    );
enable_sck_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1551155115515154"
    )
        port map (
      I0 => enable_sck_i_2_n_0,
      I1 => cnt_reg(4),
      I2 => cnt_reg(3),
      I3 => cnt_reg(2),
      I4 => \^q\(1),
      I5 => \^q\(0),
      O => enable_sck_i_1_n_0
    );
enable_sck_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => cnt_reg(5),
      I1 => cnt_reg(7),
      I2 => cnt_reg(6),
      O => enable_sck_i_2_n_0
    );
enable_sck_reg: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => enable_sck_i_1_n_0,
      Q => enable_sck,
      R => '0'
    );
\i__carry__0_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt_re_reg(4),
      O => \i__carry__0_i_1_n_0\
    );
\i__carry__0_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt_re_reg(7),
      O => \i__carry__0_i_2_n_0\
    );
\i__carry__0_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt_re_reg(6),
      O => \i__carry__0_i_3_n_0\
    );
\i__carry__0_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt_re_reg(5),
      O => \i__carry__0_i_4_n_0\
    );
\i__carry__1_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt_re_reg(11),
      O => \i__carry__1_i_1_n_0\
    );
\i__carry__1_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt_re_reg(10),
      O => \i__carry__1_i_2_n_0\
    );
\i__carry__1_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt_re_reg(9),
      O => \i__carry__1_i_3_n_0\
    );
\i__carry__1_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt_re_reg(8),
      O => \i__carry__1_i_4_n_0\
    );
\i__carry__2_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt_re_reg(15),
      O => \i__carry__2_i_1_n_0\
    );
\i__carry__2_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt_re_reg(14),
      O => \i__carry__2_i_2_n_0\
    );
\i__carry__2_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt_re_reg(13),
      O => \i__carry__2_i_3_n_0\
    );
\i__carry__2_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt_re_reg(12),
      O => \i__carry__2_i_4_n_0\
    );
\i__carry_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt_re_reg(3),
      O => \i__carry_i_1_n_0\
    );
\i__carry_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt_re_reg(2),
      O => \i__carry_i_2_n_0\
    );
\i__carry_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt_re_reg(1),
      O => \i__carry_i_3_n_0\
    );
\i__carry_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt_re_reg(0),
      O => \i__carry_i_4_n_0\
    );
start_prev_fe_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A800A8A8A8A8A8A8"
    )
        port map (
      I0 => \cnt_re_reg[0]_0\(0),
      I1 => start_sync,
      I2 => enable_cnt_re,
      I3 => start_prev_fe_i_2_n_0,
      I4 => start_prev_fe_i_3_n_0,
      I5 => start_prev_fe_i_4_n_0,
      O => start_prev_fe_i_1_n_0
    );
start_prev_fe_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F8FF"
    )
        port map (
      I0 => cnt_re_reg(4),
      I1 => cnt_re_reg(3),
      I2 => cnt_re_reg(5),
      I3 => data_rx_ready_i_4_n_0,
      O => start_prev_fe_i_2_n_0
    );
start_prev_fe_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000200"
    )
        port map (
      I0 => cnt_re_reg(4),
      I1 => cnt_re_reg(15),
      I2 => cnt_re_reg(5),
      I3 => \cnt_re_reg[0]_0\(0),
      I4 => cnt_re_reg(10),
      I5 => cnt_re_reg(11),
      O => start_prev_fe_i_3_n_0
    );
start_prev_fe_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00A2"
    )
        port map (
      I0 => start_prev_fe_i_5_n_0,
      I1 => cnt_re_reg(12),
      I2 => cnt_re_reg(13),
      I3 => cnt_re_reg(14),
      O => start_prev_fe_i_4_n_0
    );
start_prev_fe_i_5: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000100"
    )
        port map (
      I0 => cnt_re_reg(1),
      I1 => cnt_re_reg(0),
      I2 => cnt_re_reg(2),
      I3 => cnt_re_reg(6),
      I4 => cnt_re_reg(7),
      I5 => cnt_re_reg(9),
      O => start_prev_fe_i_5_n_0
    );
start_prev_fe_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => start_prev_fe_i_1_n_0,
      Q => enable_cnt_re,
      R => '0'
    );
start_sync_reg: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => start_sync_reg_0(0),
      Q => start_sync,
      R => '0'
    );
tristate_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AABAAAAAAAAAAAAA"
    )
        port map (
      I0 => tristate_i_2_n_0,
      I1 => start_prev_fe_i_2_n_0,
      I2 => cnt_re_reg(0),
      I3 => cnt_re_reg(2),
      I4 => cnt_re_reg(1),
      I5 => tristate_i_3_n_0,
      O => tristate_i_1_n_0
    );
tristate_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8808"
    )
        port map (
      I0 => tristate,
      I1 => \cnt_re_reg[0]_0\(0),
      I2 => start_sync,
      I3 => enable_cnt_re,
      O => tristate_i_2_n_0
    );
tristate_i_3: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02000202"
    )
        port map (
      I0 => start_prev_fe_i_3_n_0,
      I1 => tristate_i_4_n_0,
      I2 => cnt_re_reg(14),
      I3 => cnt_re_reg(13),
      I4 => cnt_re_reg(12),
      O => tristate_i_3_n_0
    );
tristate_i_4: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF44F4"
    )
        port map (
      I0 => cnt_re_reg(7),
      I1 => cnt_re_reg(6),
      I2 => cnt_re_reg(9),
      I3 => cnt_re_reg(10),
      I4 => cnt_re_reg(8),
      O => tristate_i_4_n_0
    );
tristate_reg: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => tristate_i_1_n_0,
      Q => tristate,
      R => '0'
    );
tx_reg_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => DATA_TX_serial,
      Q => tx_wire,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_AD9650_0_0_AD9650_v3_0 is
  port (
    m00_dma_axis_tvalid : out STD_LOGIC;
    m00_dma_axis_tlast : out STD_LOGIC;
    m00_fft_axis_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m01_fft_axis_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_dma_axis_tdata : out STD_LOGIC_VECTOR ( 63 downto 0 );
    s00_axi_awready : out STD_LOGIC;
    ADC_PDwN : out STD_LOGIC;
    s00_axi_wready : out STD_LOGIC;
    s00_axi_arready : out STD_LOGIC;
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    adc_spi_sck : out STD_LOGIC;
    allowed_read_out : out STD_LOGIC;
    s00_axi_rvalid : out STD_LOGIC;
    s00_axi_bvalid : out STD_LOGIC;
    adc_spi_cs : out STD_LOGIC;
    m00_fft_axis_tvalid : out STD_LOGIC;
    m00_fft_axis_tlast : out STD_LOGIC;
    m01_fft_axis_tvalid : out STD_LOGIC;
    m01_fft_axis_tlast : out STD_LOGIC;
    adc_spi_sdio : inout STD_LOGIC;
    azimut_0 : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC;
    s01_fft_axis_tvalid : in STD_LOGIC;
    s00_fft_axis_tvalid : in STD_LOGIC;
    s01_fft_axis_tlast : in STD_LOGIC;
    s00_fft_axis_tlast : in STD_LOGIC;
    DATA_INB : in STD_LOGIC_VECTOR ( 15 downto 0 );
    DATA_INA : in STD_LOGIC_VECTOR ( 15 downto 0 );
    s01_fft_axis_tdata : in STD_LOGIC_VECTOR ( 63 downto 0 );
    s00_fft_axis_tdata : in STD_LOGIC_VECTOR ( 63 downto 0 );
    allowed_clk : in STD_LOGIC;
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_wvalid : in STD_LOGIC;
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    clk_10MHz : in STD_LOGIC;
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_arvalid : in STD_LOGIC;
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_bready : in STD_LOGIC;
    s00_axi_rready : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of design_1_AD9650_0_0_AD9650_v3_0 : entity is "AD9650_v3_0";
end design_1_AD9650_0_0_AD9650_v3_0;

architecture STRUCTURE of design_1_AD9650_0_0_AD9650_v3_0 is
  signal AD9650_v3_0_S00_AXI_inst_n_1 : STD_LOGIC;
  signal AD9650_v3_0_S00_AXI_inst_n_42 : STD_LOGIC;
  signal AD9650_v3_0_S00_AXI_inst_n_44 : STD_LOGIC;
  signal AD9650_v3_0_S00_AXI_inst_n_6 : STD_LOGIC;
  signal AD9650_v3_0_S00_AXI_inst_n_7 : STD_LOGIC;
  signal AD9650_v3_0_S00_AXI_inst_n_8 : STD_LOGIC;
  signal DATA_RX_r : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal allowed_read0 : STD_LOGIC;
  signal \allowed_read0_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \allowed_read0_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \allowed_read0_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \allowed_read0_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \allowed_read0_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \allowed_read0_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \allowed_read0_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \allowed_read0_carry__0_i_8_n_0\ : STD_LOGIC;
  signal \allowed_read0_carry__0_n_1\ : STD_LOGIC;
  signal \allowed_read0_carry__0_n_2\ : STD_LOGIC;
  signal \allowed_read0_carry__0_n_3\ : STD_LOGIC;
  signal allowed_read0_carry_i_1_n_0 : STD_LOGIC;
  signal allowed_read0_carry_i_2_n_0 : STD_LOGIC;
  signal allowed_read0_carry_i_3_n_0 : STD_LOGIC;
  signal allowed_read0_carry_i_4_n_0 : STD_LOGIC;
  signal allowed_read0_carry_i_5_n_0 : STD_LOGIC;
  signal allowed_read0_carry_i_6_n_0 : STD_LOGIC;
  signal allowed_read0_carry_i_7_n_0 : STD_LOGIC;
  signal allowed_read0_carry_i_8_n_0 : STD_LOGIC;
  signal allowed_read0_carry_n_0 : STD_LOGIC;
  signal allowed_read0_carry_n_1 : STD_LOGIC;
  signal allowed_read0_carry_n_2 : STD_LOGIC;
  signal allowed_read0_carry_n_3 : STD_LOGIC;
  signal amountOfAzimuthFromCpu_r : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal amountOfAzimuthFromCpu_r_0 : STD_LOGIC;
  signal averagVal_r : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal cnt_DCO : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \cnt_DCO[0]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_DCO[13]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_DCO[15]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_DCO[15]_i_3_n_0\ : STD_LOGIC;
  signal \cnt_DCO[15]_i_4_n_0\ : STD_LOGIC;
  signal \cnt_DCO[15]_i_5_n_0\ : STD_LOGIC;
  signal \cnt_DCO[15]_i_6_n_0\ : STD_LOGIC;
  signal \cnt_DCO_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_DCO_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_DCO_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_DCO_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_DCO_reg[15]_i_2_n_2\ : STD_LOGIC;
  signal \cnt_DCO_reg[15]_i_2_n_3\ : STD_LOGIC;
  signal \cnt_DCO_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_DCO_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_DCO_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_DCO_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_DCO_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_DCO_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_DCO_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_DCO_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_in_DCO[0]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_in_DCO[0]_i_3_n_0\ : STD_LOGIC;
  signal cnt_in_DCO_reg : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \cnt_in_DCO_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \cnt_in_DCO_reg[0]_i_2_n_1\ : STD_LOGIC;
  signal \cnt_in_DCO_reg[0]_i_2_n_2\ : STD_LOGIC;
  signal \cnt_in_DCO_reg[0]_i_2_n_3\ : STD_LOGIC;
  signal \cnt_in_DCO_reg[0]_i_2_n_4\ : STD_LOGIC;
  signal \cnt_in_DCO_reg[0]_i_2_n_5\ : STD_LOGIC;
  signal \cnt_in_DCO_reg[0]_i_2_n_6\ : STD_LOGIC;
  signal \cnt_in_DCO_reg[0]_i_2_n_7\ : STD_LOGIC;
  signal \cnt_in_DCO_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_in_DCO_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_in_DCO_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_in_DCO_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \cnt_in_DCO_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \cnt_in_DCO_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \cnt_in_DCO_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \cnt_in_DCO_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_in_DCO_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_in_DCO_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_in_DCO_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_in_DCO_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \cnt_in_DCO_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \cnt_in_DCO_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \cnt_in_DCO_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \cnt_in_DCO_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_in_DCO_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_in_DCO_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_in_DCO_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_in_DCO_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \cnt_in_DCO_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \cnt_in_DCO_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \cnt_in_DCO_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal cnt_low_frame : STD_LOGIC;
  signal cnt_low_frame20_in : STD_LOGIC;
  signal \cnt_low_frame2_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \cnt_low_frame2_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \cnt_low_frame2_carry__0_n_3\ : STD_LOGIC;
  signal cnt_low_frame2_carry_i_1_n_0 : STD_LOGIC;
  signal cnt_low_frame2_carry_i_2_n_0 : STD_LOGIC;
  signal cnt_low_frame2_carry_i_3_n_0 : STD_LOGIC;
  signal cnt_low_frame2_carry_i_4_n_0 : STD_LOGIC;
  signal cnt_low_frame2_carry_n_0 : STD_LOGIC;
  signal cnt_low_frame2_carry_n_1 : STD_LOGIC;
  signal cnt_low_frame2_carry_n_2 : STD_LOGIC;
  signal cnt_low_frame2_carry_n_3 : STD_LOGIC;
  signal \cnt_low_frame8[0]_i_2_n_0\ : STD_LOGIC;
  signal \cnt_low_frame8[0]_i_4_n_0\ : STD_LOGIC;
  signal cnt_low_frame8_reg : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \cnt_low_frame8_reg[0]_i_3_n_0\ : STD_LOGIC;
  signal \cnt_low_frame8_reg[0]_i_3_n_1\ : STD_LOGIC;
  signal \cnt_low_frame8_reg[0]_i_3_n_2\ : STD_LOGIC;
  signal \cnt_low_frame8_reg[0]_i_3_n_3\ : STD_LOGIC;
  signal \cnt_low_frame8_reg[0]_i_3_n_4\ : STD_LOGIC;
  signal \cnt_low_frame8_reg[0]_i_3_n_5\ : STD_LOGIC;
  signal \cnt_low_frame8_reg[0]_i_3_n_6\ : STD_LOGIC;
  signal \cnt_low_frame8_reg[0]_i_3_n_7\ : STD_LOGIC;
  signal \cnt_low_frame8_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_low_frame8_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_low_frame8_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_low_frame8_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \cnt_low_frame8_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \cnt_low_frame8_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \cnt_low_frame8_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \cnt_low_frame8_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_low_frame8_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_low_frame8_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_low_frame8_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_low_frame8_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \cnt_low_frame8_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \cnt_low_frame8_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \cnt_low_frame8_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \cnt_low_frame8_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_low_frame8_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_low_frame8_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_low_frame8_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_low_frame8_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \cnt_low_frame8_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \cnt_low_frame8_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \cnt_low_frame8_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal \cnt_low_frame[0]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_low_frame[1]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_low_frame[2]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_low_frame[3]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_low_frame[3]_i_2_n_0\ : STD_LOGIC;
  signal \cnt_low_frame[4]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_low_frame[4]_i_2_n_0\ : STD_LOGIC;
  signal \cnt_low_frame[5]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_low_frame[5]_i_2_n_0\ : STD_LOGIC;
  signal \cnt_low_frame[6]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_low_frame[7]_i_2_n_0\ : STD_LOGIC;
  signal \cnt_low_frame[7]_i_3_n_0\ : STD_LOGIC;
  signal \cnt_low_frame[7]_i_4_n_0\ : STD_LOGIC;
  signal \cnt_low_frame[7]_i_5_n_0\ : STD_LOGIC;
  signal \cnt_low_frame[7]_i_6_n_0\ : STD_LOGIC;
  signal \cnt_low_frame[7]_i_7_n_0\ : STD_LOGIC;
  signal \cnt_low_frame[7]_i_8_n_0\ : STD_LOGIC;
  signal \cnt_low_frame_reg_n_0_[0]\ : STD_LOGIC;
  signal \cnt_low_frame_reg_n_0_[1]\ : STD_LOGIC;
  signal \cnt_low_frame_reg_n_0_[2]\ : STD_LOGIC;
  signal \cnt_low_frame_reg_n_0_[3]\ : STD_LOGIC;
  signal \cnt_low_frame_reg_n_0_[4]\ : STD_LOGIC;
  signal \cnt_low_frame_reg_n_0_[5]\ : STD_LOGIC;
  signal \cnt_low_frame_reg_n_0_[6]\ : STD_LOGIC;
  signal \cnt_low_frame_reg_n_0_[7]\ : STD_LOGIC;
  signal \cnt_not_allowed_clk[0]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_not_allowed_clk[0]_i_3_n_0\ : STD_LOGIC;
  signal cnt_not_allowed_clk_reg : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \cnt_not_allowed_clk_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \cnt_not_allowed_clk_reg[0]_i_2_n_1\ : STD_LOGIC;
  signal \cnt_not_allowed_clk_reg[0]_i_2_n_2\ : STD_LOGIC;
  signal \cnt_not_allowed_clk_reg[0]_i_2_n_3\ : STD_LOGIC;
  signal \cnt_not_allowed_clk_reg[0]_i_2_n_4\ : STD_LOGIC;
  signal \cnt_not_allowed_clk_reg[0]_i_2_n_5\ : STD_LOGIC;
  signal \cnt_not_allowed_clk_reg[0]_i_2_n_6\ : STD_LOGIC;
  signal \cnt_not_allowed_clk_reg[0]_i_2_n_7\ : STD_LOGIC;
  signal \cnt_not_allowed_clk_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_not_allowed_clk_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_not_allowed_clk_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_not_allowed_clk_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \cnt_not_allowed_clk_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \cnt_not_allowed_clk_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \cnt_not_allowed_clk_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \cnt_not_allowed_clk_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_not_allowed_clk_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_not_allowed_clk_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_not_allowed_clk_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_not_allowed_clk_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \cnt_not_allowed_clk_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \cnt_not_allowed_clk_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \cnt_not_allowed_clk_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \cnt_not_allowed_clk_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_not_allowed_clk_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_not_allowed_clk_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_not_allowed_clk_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_not_allowed_clk_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \cnt_not_allowed_clk_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \cnt_not_allowed_clk_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \cnt_not_allowed_clk_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal cnt_reg : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal data4 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal enable_cnt_low8_frame_i_1_n_0 : STD_LOGIC;
  signal enable_module : STD_LOGIC;
  signal m00_fft_axis_tlast_r_i_1_n_0 : STD_LOGIC;
  signal m00_fft_axis_tvalid_r_i_1_n_0 : STD_LOGIC;
  signal m00_fft_axis_tvalid_r_i_3_n_0 : STD_LOGIC;
  signal m00_fft_axis_tvalid_r_i_4_n_0 : STD_LOGIC;
  signal m00_fft_axis_tvalid_r_i_5_n_0 : STD_LOGIC;
  signal m00_fft_axis_tvalid_r_i_6_n_0 : STD_LOGIC;
  signal m00_fft_axis_tvalid_r_i_7_n_0 : STD_LOGIC;
  signal m01_fft_axis_tlast_r0 : STD_LOGIC;
  signal m01_fft_axis_tlast_r_i_1_n_0 : STD_LOGIC;
  signal m01_fft_axis_tvalid_r0 : STD_LOGIC;
  signal m01_fft_axis_tvalid_r_i_1_n_0 : STD_LOGIC;
  signal p_2_in : STD_LOGIC_VECTOR ( 15 downto 1 );
  signal slv_reg1 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal slv_reg3 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal spi_AD9650_inst_n_5 : STD_LOGIC;
  signal NLW_allowed_read0_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_allowed_read0_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_cnt_DCO_reg[15]_i_2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_cnt_DCO_reg[15]_i_2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_cnt_in_DCO_reg[12]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal NLW_cnt_low_frame2_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_cnt_low_frame2_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_cnt_low_frame2_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_cnt_low_frame8_reg[12]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_cnt_not_allowed_clk_reg[12]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of allowed_read_out_INST_0 : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \cnt_DCO[0]_i_1\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \cnt_DCO[15]_i_3\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \cnt_low_frame[0]_i_1\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \cnt_low_frame[1]_i_1\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \cnt_low_frame[3]_i_2\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \cnt_low_frame[4]_i_1\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \cnt_low_frame[4]_i_2\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \cnt_low_frame[5]_i_2\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[0]_INST_0\ : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[10]_INST_0\ : label is "soft_lutpair54";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[11]_INST_0\ : label is "soft_lutpair55";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[12]_INST_0\ : label is "soft_lutpair56";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[13]_INST_0\ : label is "soft_lutpair57";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[14]_INST_0\ : label is "soft_lutpair58";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[15]_INST_0\ : label is "soft_lutpair59";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[16]_INST_0\ : label is "soft_lutpair60";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[17]_INST_0\ : label is "soft_lutpair61";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[18]_INST_0\ : label is "soft_lutpair62";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[19]_INST_0\ : label is "soft_lutpair63";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[1]_INST_0\ : label is "soft_lutpair45";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[20]_INST_0\ : label is "soft_lutpair64";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[21]_INST_0\ : label is "soft_lutpair65";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[22]_INST_0\ : label is "soft_lutpair66";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[23]_INST_0\ : label is "soft_lutpair67";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[24]_INST_0\ : label is "soft_lutpair68";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[25]_INST_0\ : label is "soft_lutpair69";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[26]_INST_0\ : label is "soft_lutpair70";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[27]_INST_0\ : label is "soft_lutpair71";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[28]_INST_0\ : label is "soft_lutpair72";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[29]_INST_0\ : label is "soft_lutpair73";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[2]_INST_0\ : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[30]_INST_0\ : label is "soft_lutpair74";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[31]_INST_0\ : label is "soft_lutpair75";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[32]_INST_0\ : label is "soft_lutpair76";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[33]_INST_0\ : label is "soft_lutpair77";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[34]_INST_0\ : label is "soft_lutpair78";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[35]_INST_0\ : label is "soft_lutpair79";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[36]_INST_0\ : label is "soft_lutpair80";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[37]_INST_0\ : label is "soft_lutpair81";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[38]_INST_0\ : label is "soft_lutpair82";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[39]_INST_0\ : label is "soft_lutpair83";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[3]_INST_0\ : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[40]_INST_0\ : label is "soft_lutpair84";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[41]_INST_0\ : label is "soft_lutpair94";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[44]_INST_0\ : label is "soft_lutpair37";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[45]_INST_0\ : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[46]_INST_0\ : label is "soft_lutpair38";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[47]_INST_0\ : label is "soft_lutpair96";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[48]_INST_0\ : label is "soft_lutpair95";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[49]_INST_0\ : label is "soft_lutpair39";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[4]_INST_0\ : label is "soft_lutpair48";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[50]_INST_0\ : label is "soft_lutpair41";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[51]_INST_0\ : label is "soft_lutpair40";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[52]_INST_0\ : label is "soft_lutpair43";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[53]_INST_0\ : label is "soft_lutpair42";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[54]_INST_0\ : label is "soft_lutpair93";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[55]_INST_0\ : label is "soft_lutpair92";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[56]_INST_0\ : label is "soft_lutpair91";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[57]_INST_0\ : label is "soft_lutpair90";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[58]_INST_0\ : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[59]_INST_0\ : label is "soft_lutpair89";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[5]_INST_0\ : label is "soft_lutpair49";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[60]_INST_0\ : label is "soft_lutpair88";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[61]_INST_0\ : label is "soft_lutpair87";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[62]_INST_0\ : label is "soft_lutpair86";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[63]_INST_0\ : label is "soft_lutpair85";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[6]_INST_0\ : label is "soft_lutpair50";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[7]_INST_0\ : label is "soft_lutpair51";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[8]_INST_0\ : label is "soft_lutpair52";
  attribute SOFT_HLUTNM of \m00_dma_axis_tdata[9]_INST_0\ : label is "soft_lutpair53";
  attribute SOFT_HLUTNM of m00_dma_axis_tlast_INST_0 : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of m00_dma_axis_tvalid_INST_0 : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \m00_fft_axis_tdata[0]_INST_0\ : label is "soft_lutpair64";
  attribute SOFT_HLUTNM of \m00_fft_axis_tdata[10]_INST_0\ : label is "soft_lutpair54";
  attribute SOFT_HLUTNM of \m00_fft_axis_tdata[11]_INST_0\ : label is "soft_lutpair53";
  attribute SOFT_HLUTNM of \m00_fft_axis_tdata[12]_INST_0\ : label is "soft_lutpair52";
  attribute SOFT_HLUTNM of \m00_fft_axis_tdata[13]_INST_0\ : label is "soft_lutpair51";
  attribute SOFT_HLUTNM of \m00_fft_axis_tdata[14]_INST_0\ : label is "soft_lutpair50";
  attribute SOFT_HLUTNM of \m00_fft_axis_tdata[15]_INST_0\ : label is "soft_lutpair49";
  attribute SOFT_HLUTNM of \m00_fft_axis_tdata[16]_INST_0\ : label is "soft_lutpair48";
  attribute SOFT_HLUTNM of \m00_fft_axis_tdata[17]_INST_0\ : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of \m00_fft_axis_tdata[18]_INST_0\ : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of \m00_fft_axis_tdata[19]_INST_0\ : label is "soft_lutpair45";
  attribute SOFT_HLUTNM of \m00_fft_axis_tdata[1]_INST_0\ : label is "soft_lutpair63";
  attribute SOFT_HLUTNM of \m00_fft_axis_tdata[20]_INST_0\ : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of \m00_fft_axis_tdata[21]_INST_0\ : label is "soft_lutpair43";
  attribute SOFT_HLUTNM of \m00_fft_axis_tdata[22]_INST_0\ : label is "soft_lutpair42";
  attribute SOFT_HLUTNM of \m00_fft_axis_tdata[23]_INST_0\ : label is "soft_lutpair41";
  attribute SOFT_HLUTNM of \m00_fft_axis_tdata[24]_INST_0\ : label is "soft_lutpair40";
  attribute SOFT_HLUTNM of \m00_fft_axis_tdata[25]_INST_0\ : label is "soft_lutpair39";
  attribute SOFT_HLUTNM of \m00_fft_axis_tdata[26]_INST_0\ : label is "soft_lutpair38";
  attribute SOFT_HLUTNM of \m00_fft_axis_tdata[27]_INST_0\ : label is "soft_lutpair37";
  attribute SOFT_HLUTNM of \m00_fft_axis_tdata[28]_INST_0\ : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of \m00_fft_axis_tdata[29]_INST_0\ : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of \m00_fft_axis_tdata[2]_INST_0\ : label is "soft_lutpair62";
  attribute SOFT_HLUTNM of \m00_fft_axis_tdata[30]_INST_0\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \m00_fft_axis_tdata[31]_INST_0\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \m00_fft_axis_tdata[3]_INST_0\ : label is "soft_lutpair61";
  attribute SOFT_HLUTNM of \m00_fft_axis_tdata[4]_INST_0\ : label is "soft_lutpair60";
  attribute SOFT_HLUTNM of \m00_fft_axis_tdata[5]_INST_0\ : label is "soft_lutpair59";
  attribute SOFT_HLUTNM of \m00_fft_axis_tdata[6]_INST_0\ : label is "soft_lutpair58";
  attribute SOFT_HLUTNM of \m00_fft_axis_tdata[7]_INST_0\ : label is "soft_lutpair57";
  attribute SOFT_HLUTNM of \m00_fft_axis_tdata[8]_INST_0\ : label is "soft_lutpair56";
  attribute SOFT_HLUTNM of \m00_fft_axis_tdata[9]_INST_0\ : label is "soft_lutpair55";
  attribute SOFT_HLUTNM of m00_fft_axis_tlast_r_i_1 : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of m00_fft_axis_tlast_r_i_2 : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of m00_fft_axis_tvalid_r_i_1 : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of m00_fft_axis_tvalid_r_i_2 : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of m00_fft_axis_tvalid_r_i_4 : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[0]_INST_0\ : label is "soft_lutpair96";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[10]_INST_0\ : label is "soft_lutpair86";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[11]_INST_0\ : label is "soft_lutpair85";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[12]_INST_0\ : label is "soft_lutpair84";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[13]_INST_0\ : label is "soft_lutpair83";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[14]_INST_0\ : label is "soft_lutpair82";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[15]_INST_0\ : label is "soft_lutpair81";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[16]_INST_0\ : label is "soft_lutpair80";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[17]_INST_0\ : label is "soft_lutpair79";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[18]_INST_0\ : label is "soft_lutpair78";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[19]_INST_0\ : label is "soft_lutpair77";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[1]_INST_0\ : label is "soft_lutpair95";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[20]_INST_0\ : label is "soft_lutpair76";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[21]_INST_0\ : label is "soft_lutpair75";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[22]_INST_0\ : label is "soft_lutpair74";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[23]_INST_0\ : label is "soft_lutpair73";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[24]_INST_0\ : label is "soft_lutpair72";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[25]_INST_0\ : label is "soft_lutpair71";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[26]_INST_0\ : label is "soft_lutpair70";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[27]_INST_0\ : label is "soft_lutpair69";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[28]_INST_0\ : label is "soft_lutpair68";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[29]_INST_0\ : label is "soft_lutpair67";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[2]_INST_0\ : label is "soft_lutpair94";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[30]_INST_0\ : label is "soft_lutpair66";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[31]_INST_0\ : label is "soft_lutpair65";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[3]_INST_0\ : label is "soft_lutpair93";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[4]_INST_0\ : label is "soft_lutpair92";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[5]_INST_0\ : label is "soft_lutpair91";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[6]_INST_0\ : label is "soft_lutpair90";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[7]_INST_0\ : label is "soft_lutpair89";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[8]_INST_0\ : label is "soft_lutpair88";
  attribute SOFT_HLUTNM of \m01_fft_axis_tdata[9]_INST_0\ : label is "soft_lutpair87";
  attribute SOFT_HLUTNM of m01_fft_axis_tlast_r_i_1 : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of m01_fft_axis_tvalid_r_i_1 : label is "soft_lutpair32";
begin
AD9650_v3_0_S00_AXI_inst: entity work.design_1_AD9650_0_0_AD9650_v3_0_S00_AXI
     port map (
      DATA_RX_r(7 downto 0) => DATA_RX_r(7 downto 0),
      DATA_TX_serial_r_reg => spi_AD9650_inst_n_5,
      Q(1 downto 0) => cnt_reg(1 downto 0),
      \cnt_reg[2]\ => AD9650_v3_0_S00_AXI_inst_n_6,
      \cnt_reg[2]_0\ => AD9650_v3_0_S00_AXI_inst_n_7,
      data4(0) => data4(0),
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_araddr(2 downto 0) => s00_axi_araddr(2 downto 0),
      s00_axi_aresetn => s00_axi_aresetn,
      s00_axi_aresetn_0 => AD9650_v3_0_S00_AXI_inst_n_1,
      s00_axi_arready => s00_axi_arready,
      s00_axi_arvalid => s00_axi_arvalid,
      s00_axi_awaddr(2 downto 0) => s00_axi_awaddr(2 downto 0),
      s00_axi_awready => s00_axi_awready,
      s00_axi_awvalid => s00_axi_awvalid,
      s00_axi_bready => s00_axi_bready,
      s00_axi_bvalid => s00_axi_bvalid,
      s00_axi_rdata(31 downto 0) => s00_axi_rdata(31 downto 0),
      s00_axi_rready => s00_axi_rready,
      s00_axi_rvalid => s00_axi_rvalid,
      s00_axi_wdata(31 downto 0) => s00_axi_wdata(31 downto 0),
      s00_axi_wready => s00_axi_wready,
      s00_axi_wstrb(3 downto 0) => s00_axi_wstrb(3 downto 0),
      s00_axi_wvalid => s00_axi_wvalid,
      \slv_reg0_reg[11]_0\ => AD9650_v3_0_S00_AXI_inst_n_8,
      \slv_reg0_reg[12]_0\(0) => AD9650_v3_0_S00_AXI_inst_n_42,
      \slv_reg1_reg[0]_0\(0) => slv_reg1(0),
      \slv_reg2_reg[1]_0\(1) => ADC_PDwN,
      \slv_reg2_reg[1]_0\(0) => AD9650_v3_0_S00_AXI_inst_n_44,
      \slv_reg3_reg[31]_0\(31 downto 0) => slv_reg3(31 downto 0)
    );
allowed_read0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => allowed_read0_carry_n_0,
      CO(2) => allowed_read0_carry_n_1,
      CO(1) => allowed_read0_carry_n_2,
      CO(0) => allowed_read0_carry_n_3,
      CYINIT => '0',
      DI(3) => allowed_read0_carry_i_1_n_0,
      DI(2) => allowed_read0_carry_i_2_n_0,
      DI(1) => allowed_read0_carry_i_3_n_0,
      DI(0) => allowed_read0_carry_i_4_n_0,
      O(3 downto 0) => NLW_allowed_read0_carry_O_UNCONNECTED(3 downto 0),
      S(3) => allowed_read0_carry_i_5_n_0,
      S(2) => allowed_read0_carry_i_6_n_0,
      S(1) => allowed_read0_carry_i_7_n_0,
      S(0) => allowed_read0_carry_i_8_n_0
    );
\allowed_read0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => allowed_read0_carry_n_0,
      CO(3) => allowed_read0,
      CO(2) => \allowed_read0_carry__0_n_1\,
      CO(1) => \allowed_read0_carry__0_n_2\,
      CO(0) => \allowed_read0_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => \allowed_read0_carry__0_i_1_n_0\,
      DI(2) => \allowed_read0_carry__0_i_2_n_0\,
      DI(1) => \allowed_read0_carry__0_i_3_n_0\,
      DI(0) => \allowed_read0_carry__0_i_4_n_0\,
      O(3 downto 0) => \NLW_allowed_read0_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => \allowed_read0_carry__0_i_5_n_0\,
      S(2) => \allowed_read0_carry__0_i_6_n_0\,
      S(1) => \allowed_read0_carry__0_i_7_n_0\,
      S(0) => \allowed_read0_carry__0_i_8_n_0\
    );
\allowed_read0_carry__0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => amountOfAzimuthFromCpu_r(15),
      I1 => cnt_low_frame8_reg(15),
      I2 => amountOfAzimuthFromCpu_r(14),
      I3 => cnt_low_frame8_reg(14),
      O => \allowed_read0_carry__0_i_1_n_0\
    );
\allowed_read0_carry__0_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => amountOfAzimuthFromCpu_r(13),
      I1 => cnt_low_frame8_reg(13),
      I2 => amountOfAzimuthFromCpu_r(12),
      I3 => cnt_low_frame8_reg(12),
      O => \allowed_read0_carry__0_i_2_n_0\
    );
\allowed_read0_carry__0_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => amountOfAzimuthFromCpu_r(11),
      I1 => cnt_low_frame8_reg(11),
      I2 => amountOfAzimuthFromCpu_r(10),
      I3 => cnt_low_frame8_reg(10),
      O => \allowed_read0_carry__0_i_3_n_0\
    );
\allowed_read0_carry__0_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => amountOfAzimuthFromCpu_r(9),
      I1 => cnt_low_frame8_reg(9),
      I2 => amountOfAzimuthFromCpu_r(8),
      I3 => cnt_low_frame8_reg(8),
      O => \allowed_read0_carry__0_i_4_n_0\
    );
\allowed_read0_carry__0_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => cnt_low_frame8_reg(15),
      I1 => amountOfAzimuthFromCpu_r(15),
      I2 => cnt_low_frame8_reg(14),
      I3 => amountOfAzimuthFromCpu_r(14),
      O => \allowed_read0_carry__0_i_5_n_0\
    );
\allowed_read0_carry__0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => cnt_low_frame8_reg(13),
      I1 => amountOfAzimuthFromCpu_r(13),
      I2 => cnt_low_frame8_reg(12),
      I3 => amountOfAzimuthFromCpu_r(12),
      O => \allowed_read0_carry__0_i_6_n_0\
    );
\allowed_read0_carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => cnt_low_frame8_reg(11),
      I1 => amountOfAzimuthFromCpu_r(11),
      I2 => cnt_low_frame8_reg(10),
      I3 => amountOfAzimuthFromCpu_r(10),
      O => \allowed_read0_carry__0_i_7_n_0\
    );
\allowed_read0_carry__0_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => cnt_low_frame8_reg(9),
      I1 => amountOfAzimuthFromCpu_r(9),
      I2 => cnt_low_frame8_reg(8),
      I3 => amountOfAzimuthFromCpu_r(8),
      O => \allowed_read0_carry__0_i_8_n_0\
    );
allowed_read0_carry_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => amountOfAzimuthFromCpu_r(7),
      I1 => cnt_low_frame8_reg(7),
      I2 => amountOfAzimuthFromCpu_r(6),
      I3 => cnt_low_frame8_reg(6),
      O => allowed_read0_carry_i_1_n_0
    );
allowed_read0_carry_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => amountOfAzimuthFromCpu_r(5),
      I1 => cnt_low_frame8_reg(5),
      I2 => amountOfAzimuthFromCpu_r(4),
      I3 => cnt_low_frame8_reg(4),
      O => allowed_read0_carry_i_2_n_0
    );
allowed_read0_carry_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => amountOfAzimuthFromCpu_r(3),
      I1 => cnt_low_frame8_reg(3),
      I2 => amountOfAzimuthFromCpu_r(2),
      I3 => cnt_low_frame8_reg(2),
      O => allowed_read0_carry_i_3_n_0
    );
allowed_read0_carry_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"22B2"
    )
        port map (
      I0 => amountOfAzimuthFromCpu_r(1),
      I1 => cnt_low_frame8_reg(1),
      I2 => amountOfAzimuthFromCpu_r(0),
      I3 => cnt_low_frame8_reg(0),
      O => allowed_read0_carry_i_4_n_0
    );
allowed_read0_carry_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => cnt_low_frame8_reg(7),
      I1 => amountOfAzimuthFromCpu_r(7),
      I2 => cnt_low_frame8_reg(6),
      I3 => amountOfAzimuthFromCpu_r(6),
      O => allowed_read0_carry_i_5_n_0
    );
allowed_read0_carry_i_6: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => cnt_low_frame8_reg(5),
      I1 => amountOfAzimuthFromCpu_r(5),
      I2 => cnt_low_frame8_reg(4),
      I3 => amountOfAzimuthFromCpu_r(4),
      O => allowed_read0_carry_i_6_n_0
    );
allowed_read0_carry_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => cnt_low_frame8_reg(3),
      I1 => amountOfAzimuthFromCpu_r(3),
      I2 => cnt_low_frame8_reg(2),
      I3 => amountOfAzimuthFromCpu_r(2),
      O => allowed_read0_carry_i_7_n_0
    );
allowed_read0_carry_i_8: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => cnt_low_frame8_reg(1),
      I1 => amountOfAzimuthFromCpu_r(1),
      I2 => amountOfAzimuthFromCpu_r(0),
      I3 => cnt_low_frame8_reg(0),
      O => allowed_read0_carry_i_8_n_0
    );
allowed_read_out_INST_0: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => allowed_read0,
      I1 => enable_module,
      I2 => allowed_clk,
      O => allowed_read_out
    );
\amountOfAzimuthFromCpu_r_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => azimut_0,
      D => slv_reg3(0),
      Q => amountOfAzimuthFromCpu_r(0),
      R => AD9650_v3_0_S00_AXI_inst_n_1
    );
\amountOfAzimuthFromCpu_r_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => azimut_0,
      D => slv_reg3(10),
      Q => amountOfAzimuthFromCpu_r(10),
      R => AD9650_v3_0_S00_AXI_inst_n_1
    );
\amountOfAzimuthFromCpu_r_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => azimut_0,
      D => slv_reg3(11),
      Q => amountOfAzimuthFromCpu_r(11),
      R => AD9650_v3_0_S00_AXI_inst_n_1
    );
\amountOfAzimuthFromCpu_r_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => azimut_0,
      D => slv_reg3(12),
      Q => amountOfAzimuthFromCpu_r(12),
      R => AD9650_v3_0_S00_AXI_inst_n_1
    );
\amountOfAzimuthFromCpu_r_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => azimut_0,
      D => slv_reg3(13),
      Q => amountOfAzimuthFromCpu_r(13),
      R => AD9650_v3_0_S00_AXI_inst_n_1
    );
\amountOfAzimuthFromCpu_r_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => azimut_0,
      D => slv_reg3(14),
      Q => amountOfAzimuthFromCpu_r(14),
      R => AD9650_v3_0_S00_AXI_inst_n_1
    );
\amountOfAzimuthFromCpu_r_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => azimut_0,
      D => slv_reg3(15),
      Q => amountOfAzimuthFromCpu_r(15),
      R => AD9650_v3_0_S00_AXI_inst_n_1
    );
\amountOfAzimuthFromCpu_r_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => azimut_0,
      D => slv_reg3(1),
      Q => amountOfAzimuthFromCpu_r(1),
      R => AD9650_v3_0_S00_AXI_inst_n_1
    );
\amountOfAzimuthFromCpu_r_reg[2]\: unisim.vcomponents.FDSE
     port map (
      C => clk_10MHz,
      CE => azimut_0,
      D => slv_reg3(2),
      Q => amountOfAzimuthFromCpu_r(2),
      S => AD9650_v3_0_S00_AXI_inst_n_1
    );
\amountOfAzimuthFromCpu_r_reg[3]\: unisim.vcomponents.FDSE
     port map (
      C => clk_10MHz,
      CE => azimut_0,
      D => slv_reg3(3),
      Q => amountOfAzimuthFromCpu_r(3),
      S => AD9650_v3_0_S00_AXI_inst_n_1
    );
\amountOfAzimuthFromCpu_r_reg[4]\: unisim.vcomponents.FDSE
     port map (
      C => clk_10MHz,
      CE => azimut_0,
      D => slv_reg3(4),
      Q => amountOfAzimuthFromCpu_r(4),
      S => AD9650_v3_0_S00_AXI_inst_n_1
    );
\amountOfAzimuthFromCpu_r_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => azimut_0,
      D => slv_reg3(5),
      Q => amountOfAzimuthFromCpu_r(5),
      R => AD9650_v3_0_S00_AXI_inst_n_1
    );
\amountOfAzimuthFromCpu_r_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => azimut_0,
      D => slv_reg3(6),
      Q => amountOfAzimuthFromCpu_r(6),
      R => AD9650_v3_0_S00_AXI_inst_n_1
    );
\amountOfAzimuthFromCpu_r_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => azimut_0,
      D => slv_reg3(7),
      Q => amountOfAzimuthFromCpu_r(7),
      R => AD9650_v3_0_S00_AXI_inst_n_1
    );
\amountOfAzimuthFromCpu_r_reg[8]\: unisim.vcomponents.FDSE
     port map (
      C => clk_10MHz,
      CE => azimut_0,
      D => slv_reg3(8),
      Q => amountOfAzimuthFromCpu_r(8),
      S => AD9650_v3_0_S00_AXI_inst_n_1
    );
\amountOfAzimuthFromCpu_r_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => azimut_0,
      D => slv_reg3(9),
      Q => amountOfAzimuthFromCpu_r(9),
      R => AD9650_v3_0_S00_AXI_inst_n_1
    );
\averagVal_r_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => azimut_0,
      D => slv_reg3(16),
      Q => averagVal_r(0),
      R => AD9650_v3_0_S00_AXI_inst_n_1
    );
\averagVal_r_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => azimut_0,
      D => slv_reg3(26),
      Q => averagVal_r(10),
      R => AD9650_v3_0_S00_AXI_inst_n_1
    );
\averagVal_r_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => azimut_0,
      D => slv_reg3(27),
      Q => averagVal_r(11),
      R => AD9650_v3_0_S00_AXI_inst_n_1
    );
\averagVal_r_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => azimut_0,
      D => slv_reg3(28),
      Q => averagVal_r(12),
      R => AD9650_v3_0_S00_AXI_inst_n_1
    );
\averagVal_r_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => azimut_0,
      D => slv_reg3(29),
      Q => averagVal_r(13),
      R => AD9650_v3_0_S00_AXI_inst_n_1
    );
\averagVal_r_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => azimut_0,
      D => slv_reg3(30),
      Q => averagVal_r(14),
      R => AD9650_v3_0_S00_AXI_inst_n_1
    );
\averagVal_r_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => azimut_0,
      D => slv_reg3(31),
      Q => averagVal_r(15),
      R => AD9650_v3_0_S00_AXI_inst_n_1
    );
\averagVal_r_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => azimut_0,
      D => slv_reg3(17),
      Q => averagVal_r(1),
      R => AD9650_v3_0_S00_AXI_inst_n_1
    );
\averagVal_r_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => azimut_0,
      D => slv_reg3(18),
      Q => averagVal_r(2),
      R => AD9650_v3_0_S00_AXI_inst_n_1
    );
\averagVal_r_reg[3]\: unisim.vcomponents.FDSE
     port map (
      C => clk_10MHz,
      CE => azimut_0,
      D => slv_reg3(19),
      Q => averagVal_r(3),
      S => AD9650_v3_0_S00_AXI_inst_n_1
    );
\averagVal_r_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => azimut_0,
      D => slv_reg3(20),
      Q => averagVal_r(4),
      R => AD9650_v3_0_S00_AXI_inst_n_1
    );
\averagVal_r_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => azimut_0,
      D => slv_reg3(21),
      Q => averagVal_r(5),
      R => AD9650_v3_0_S00_AXI_inst_n_1
    );
\averagVal_r_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => azimut_0,
      D => slv_reg3(22),
      Q => averagVal_r(6),
      R => AD9650_v3_0_S00_AXI_inst_n_1
    );
\averagVal_r_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => azimut_0,
      D => slv_reg3(23),
      Q => averagVal_r(7),
      R => AD9650_v3_0_S00_AXI_inst_n_1
    );
\averagVal_r_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => azimut_0,
      D => slv_reg3(24),
      Q => averagVal_r(8),
      R => AD9650_v3_0_S00_AXI_inst_n_1
    );
\averagVal_r_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => azimut_0,
      D => slv_reg3(25),
      Q => averagVal_r(9),
      R => AD9650_v3_0_S00_AXI_inst_n_1
    );
\cnt_DCO[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00808080"
    )
        port map (
      I0 => allowed_clk,
      I1 => enable_module,
      I2 => allowed_read0,
      I3 => \cnt_DCO[15]_i_3_n_0\,
      I4 => cnt_DCO(0),
      O => \cnt_DCO[0]_i_1_n_0\
    );
\cnt_DCO[13]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"80800080"
    )
        port map (
      I0 => allowed_clk,
      I1 => enable_module,
      I2 => allowed_read0,
      I3 => \cnt_DCO[15]_i_3_n_0\,
      I4 => p_2_in(13),
      O => \cnt_DCO[13]_i_1_n_0\
    );
\cnt_DCO[15]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => allowed_clk,
      I1 => enable_module,
      I2 => allowed_read0,
      I3 => \cnt_DCO[15]_i_3_n_0\,
      O => \cnt_DCO[15]_i_1_n_0\
    );
\cnt_DCO[15]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFEFF"
    )
        port map (
      I0 => \cnt_DCO[15]_i_4_n_0\,
      I1 => cnt_DCO(3),
      I2 => cnt_DCO(2),
      I3 => cnt_DCO(0),
      I4 => \cnt_DCO[15]_i_5_n_0\,
      O => \cnt_DCO[15]_i_3_n_0\
    );
\cnt_DCO[15]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => cnt_DCO(7),
      I1 => cnt_DCO(9),
      I2 => cnt_DCO(5),
      I3 => cnt_DCO(10),
      I4 => \cnt_DCO[15]_i_6_n_0\,
      O => \cnt_DCO[15]_i_4_n_0\
    );
\cnt_DCO[15]_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"EF"
    )
        port map (
      I0 => cnt_DCO(14),
      I1 => cnt_DCO(15),
      I2 => cnt_DCO(13),
      O => \cnt_DCO[15]_i_5_n_0\
    );
\cnt_DCO[15]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => cnt_DCO(1),
      I1 => cnt_DCO(8),
      I2 => cnt_DCO(4),
      I3 => cnt_DCO(11),
      I4 => cnt_DCO(6),
      I5 => cnt_DCO(12),
      O => \cnt_DCO[15]_i_6_n_0\
    );
\cnt_DCO_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_DCO[0]_i_1_n_0\,
      Q => cnt_DCO(0),
      R => '0'
    );
\cnt_DCO_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => p_2_in(10),
      Q => cnt_DCO(10),
      R => \cnt_DCO[15]_i_1_n_0\
    );
\cnt_DCO_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => p_2_in(11),
      Q => cnt_DCO(11),
      R => \cnt_DCO[15]_i_1_n_0\
    );
\cnt_DCO_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => p_2_in(12),
      Q => cnt_DCO(12),
      R => \cnt_DCO[15]_i_1_n_0\
    );
\cnt_DCO_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_DCO_reg[8]_i_1_n_0\,
      CO(3) => \cnt_DCO_reg[12]_i_1_n_0\,
      CO(2) => \cnt_DCO_reg[12]_i_1_n_1\,
      CO(1) => \cnt_DCO_reg[12]_i_1_n_2\,
      CO(0) => \cnt_DCO_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => p_2_in(12 downto 9),
      S(3 downto 0) => cnt_DCO(12 downto 9)
    );
\cnt_DCO_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_DCO[13]_i_1_n_0\,
      Q => cnt_DCO(13),
      R => '0'
    );
\cnt_DCO_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => p_2_in(14),
      Q => cnt_DCO(14),
      R => \cnt_DCO[15]_i_1_n_0\
    );
\cnt_DCO_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => p_2_in(15),
      Q => cnt_DCO(15),
      R => \cnt_DCO[15]_i_1_n_0\
    );
\cnt_DCO_reg[15]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_DCO_reg[12]_i_1_n_0\,
      CO(3 downto 2) => \NLW_cnt_DCO_reg[15]_i_2_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \cnt_DCO_reg[15]_i_2_n_2\,
      CO(0) => \cnt_DCO_reg[15]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \NLW_cnt_DCO_reg[15]_i_2_O_UNCONNECTED\(3),
      O(2 downto 0) => p_2_in(15 downto 13),
      S(3) => '0',
      S(2 downto 0) => cnt_DCO(15 downto 13)
    );
\cnt_DCO_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => p_2_in(1),
      Q => cnt_DCO(1),
      R => \cnt_DCO[15]_i_1_n_0\
    );
\cnt_DCO_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => p_2_in(2),
      Q => cnt_DCO(2),
      R => \cnt_DCO[15]_i_1_n_0\
    );
\cnt_DCO_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => p_2_in(3),
      Q => cnt_DCO(3),
      R => \cnt_DCO[15]_i_1_n_0\
    );
\cnt_DCO_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => p_2_in(4),
      Q => cnt_DCO(4),
      R => \cnt_DCO[15]_i_1_n_0\
    );
\cnt_DCO_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \cnt_DCO_reg[4]_i_1_n_0\,
      CO(2) => \cnt_DCO_reg[4]_i_1_n_1\,
      CO(1) => \cnt_DCO_reg[4]_i_1_n_2\,
      CO(0) => \cnt_DCO_reg[4]_i_1_n_3\,
      CYINIT => cnt_DCO(0),
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => p_2_in(4 downto 1),
      S(3 downto 0) => cnt_DCO(4 downto 1)
    );
\cnt_DCO_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => p_2_in(5),
      Q => cnt_DCO(5),
      R => \cnt_DCO[15]_i_1_n_0\
    );
\cnt_DCO_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => p_2_in(6),
      Q => cnt_DCO(6),
      R => \cnt_DCO[15]_i_1_n_0\
    );
\cnt_DCO_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => p_2_in(7),
      Q => cnt_DCO(7),
      R => \cnt_DCO[15]_i_1_n_0\
    );
\cnt_DCO_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => p_2_in(8),
      Q => cnt_DCO(8),
      R => \cnt_DCO[15]_i_1_n_0\
    );
\cnt_DCO_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_DCO_reg[4]_i_1_n_0\,
      CO(3) => \cnt_DCO_reg[8]_i_1_n_0\,
      CO(2) => \cnt_DCO_reg[8]_i_1_n_1\,
      CO(1) => \cnt_DCO_reg[8]_i_1_n_2\,
      CO(0) => \cnt_DCO_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => p_2_in(8 downto 5),
      S(3 downto 0) => cnt_DCO(8 downto 5)
    );
\cnt_DCO_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => p_2_in(9),
      Q => cnt_DCO(9),
      R => \cnt_DCO[15]_i_1_n_0\
    );
\cnt_in_DCO[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2A"
    )
        port map (
      I0 => s00_axi_aresetn,
      I1 => allowed_clk,
      I2 => clk_10MHz,
      O => \cnt_in_DCO[0]_i_1_n_0\
    );
\cnt_in_DCO[0]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt_in_DCO_reg(0),
      O => \cnt_in_DCO[0]_i_3_n_0\
    );
\cnt_in_DCO_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => s00_axi_aresetn,
      D => \cnt_in_DCO_reg[0]_i_2_n_7\,
      Q => cnt_in_DCO_reg(0),
      R => \cnt_in_DCO[0]_i_1_n_0\
    );
\cnt_in_DCO_reg[0]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \cnt_in_DCO_reg[0]_i_2_n_0\,
      CO(2) => \cnt_in_DCO_reg[0]_i_2_n_1\,
      CO(1) => \cnt_in_DCO_reg[0]_i_2_n_2\,
      CO(0) => \cnt_in_DCO_reg[0]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \cnt_in_DCO_reg[0]_i_2_n_4\,
      O(2) => \cnt_in_DCO_reg[0]_i_2_n_5\,
      O(1) => \cnt_in_DCO_reg[0]_i_2_n_6\,
      O(0) => \cnt_in_DCO_reg[0]_i_2_n_7\,
      S(3 downto 1) => cnt_in_DCO_reg(3 downto 1),
      S(0) => \cnt_in_DCO[0]_i_3_n_0\
    );
\cnt_in_DCO_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => s00_axi_aresetn,
      D => \cnt_in_DCO_reg[8]_i_1_n_5\,
      Q => cnt_in_DCO_reg(10),
      R => \cnt_in_DCO[0]_i_1_n_0\
    );
\cnt_in_DCO_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => s00_axi_aresetn,
      D => \cnt_in_DCO_reg[8]_i_1_n_4\,
      Q => cnt_in_DCO_reg(11),
      R => \cnt_in_DCO[0]_i_1_n_0\
    );
\cnt_in_DCO_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => s00_axi_aresetn,
      D => \cnt_in_DCO_reg[12]_i_1_n_7\,
      Q => cnt_in_DCO_reg(12),
      R => \cnt_in_DCO[0]_i_1_n_0\
    );
\cnt_in_DCO_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_in_DCO_reg[8]_i_1_n_0\,
      CO(3) => \NLW_cnt_in_DCO_reg[12]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \cnt_in_DCO_reg[12]_i_1_n_1\,
      CO(1) => \cnt_in_DCO_reg[12]_i_1_n_2\,
      CO(0) => \cnt_in_DCO_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt_in_DCO_reg[12]_i_1_n_4\,
      O(2) => \cnt_in_DCO_reg[12]_i_1_n_5\,
      O(1) => \cnt_in_DCO_reg[12]_i_1_n_6\,
      O(0) => \cnt_in_DCO_reg[12]_i_1_n_7\,
      S(3 downto 0) => cnt_in_DCO_reg(15 downto 12)
    );
\cnt_in_DCO_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => s00_axi_aresetn,
      D => \cnt_in_DCO_reg[12]_i_1_n_6\,
      Q => cnt_in_DCO_reg(13),
      R => \cnt_in_DCO[0]_i_1_n_0\
    );
\cnt_in_DCO_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => s00_axi_aresetn,
      D => \cnt_in_DCO_reg[12]_i_1_n_5\,
      Q => cnt_in_DCO_reg(14),
      R => \cnt_in_DCO[0]_i_1_n_0\
    );
\cnt_in_DCO_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => s00_axi_aresetn,
      D => \cnt_in_DCO_reg[12]_i_1_n_4\,
      Q => cnt_in_DCO_reg(15),
      R => \cnt_in_DCO[0]_i_1_n_0\
    );
\cnt_in_DCO_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => s00_axi_aresetn,
      D => \cnt_in_DCO_reg[0]_i_2_n_6\,
      Q => cnt_in_DCO_reg(1),
      R => \cnt_in_DCO[0]_i_1_n_0\
    );
\cnt_in_DCO_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => s00_axi_aresetn,
      D => \cnt_in_DCO_reg[0]_i_2_n_5\,
      Q => cnt_in_DCO_reg(2),
      R => \cnt_in_DCO[0]_i_1_n_0\
    );
\cnt_in_DCO_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => s00_axi_aresetn,
      D => \cnt_in_DCO_reg[0]_i_2_n_4\,
      Q => cnt_in_DCO_reg(3),
      R => \cnt_in_DCO[0]_i_1_n_0\
    );
\cnt_in_DCO_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => s00_axi_aresetn,
      D => \cnt_in_DCO_reg[4]_i_1_n_7\,
      Q => cnt_in_DCO_reg(4),
      R => \cnt_in_DCO[0]_i_1_n_0\
    );
\cnt_in_DCO_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_in_DCO_reg[0]_i_2_n_0\,
      CO(3) => \cnt_in_DCO_reg[4]_i_1_n_0\,
      CO(2) => \cnt_in_DCO_reg[4]_i_1_n_1\,
      CO(1) => \cnt_in_DCO_reg[4]_i_1_n_2\,
      CO(0) => \cnt_in_DCO_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt_in_DCO_reg[4]_i_1_n_4\,
      O(2) => \cnt_in_DCO_reg[4]_i_1_n_5\,
      O(1) => \cnt_in_DCO_reg[4]_i_1_n_6\,
      O(0) => \cnt_in_DCO_reg[4]_i_1_n_7\,
      S(3 downto 0) => cnt_in_DCO_reg(7 downto 4)
    );
\cnt_in_DCO_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => s00_axi_aresetn,
      D => \cnt_in_DCO_reg[4]_i_1_n_6\,
      Q => cnt_in_DCO_reg(5),
      R => \cnt_in_DCO[0]_i_1_n_0\
    );
\cnt_in_DCO_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => s00_axi_aresetn,
      D => \cnt_in_DCO_reg[4]_i_1_n_5\,
      Q => cnt_in_DCO_reg(6),
      R => \cnt_in_DCO[0]_i_1_n_0\
    );
\cnt_in_DCO_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => s00_axi_aresetn,
      D => \cnt_in_DCO_reg[4]_i_1_n_4\,
      Q => cnt_in_DCO_reg(7),
      R => \cnt_in_DCO[0]_i_1_n_0\
    );
\cnt_in_DCO_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => s00_axi_aresetn,
      D => \cnt_in_DCO_reg[8]_i_1_n_7\,
      Q => cnt_in_DCO_reg(8),
      R => \cnt_in_DCO[0]_i_1_n_0\
    );
\cnt_in_DCO_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_in_DCO_reg[4]_i_1_n_0\,
      CO(3) => \cnt_in_DCO_reg[8]_i_1_n_0\,
      CO(2) => \cnt_in_DCO_reg[8]_i_1_n_1\,
      CO(1) => \cnt_in_DCO_reg[8]_i_1_n_2\,
      CO(0) => \cnt_in_DCO_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt_in_DCO_reg[8]_i_1_n_4\,
      O(2) => \cnt_in_DCO_reg[8]_i_1_n_5\,
      O(1) => \cnt_in_DCO_reg[8]_i_1_n_6\,
      O(0) => \cnt_in_DCO_reg[8]_i_1_n_7\,
      S(3 downto 0) => cnt_in_DCO_reg(11 downto 8)
    );
\cnt_in_DCO_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => s00_axi_aresetn,
      D => \cnt_in_DCO_reg[8]_i_1_n_6\,
      Q => cnt_in_DCO_reg(9),
      R => \cnt_in_DCO[0]_i_1_n_0\
    );
cnt_low_frame2_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => cnt_low_frame2_carry_n_0,
      CO(2) => cnt_low_frame2_carry_n_1,
      CO(1) => cnt_low_frame2_carry_n_2,
      CO(0) => cnt_low_frame2_carry_n_3,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => NLW_cnt_low_frame2_carry_O_UNCONNECTED(3 downto 0),
      S(3) => cnt_low_frame2_carry_i_1_n_0,
      S(2) => cnt_low_frame2_carry_i_2_n_0,
      S(1) => cnt_low_frame2_carry_i_3_n_0,
      S(0) => cnt_low_frame2_carry_i_4_n_0
    );
\cnt_low_frame2_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => cnt_low_frame2_carry_n_0,
      CO(3 downto 2) => \NLW_cnt_low_frame2_carry__0_CO_UNCONNECTED\(3 downto 2),
      CO(1) => cnt_low_frame20_in,
      CO(0) => \cnt_low_frame2_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_cnt_low_frame2_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3 downto 2) => B"00",
      S(1) => \cnt_low_frame2_carry__0_i_1_n_0\,
      S(0) => \cnt_low_frame2_carry__0_i_2_n_0\
    );
\cnt_low_frame2_carry__0_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => averagVal_r(15),
      O => \cnt_low_frame2_carry__0_i_1_n_0\
    );
\cnt_low_frame2_carry__0_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => averagVal_r(13),
      I1 => averagVal_r(12),
      I2 => averagVal_r(14),
      O => \cnt_low_frame2_carry__0_i_2_n_0\
    );
cnt_low_frame2_carry_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => averagVal_r(10),
      I1 => averagVal_r(9),
      I2 => averagVal_r(11),
      O => cnt_low_frame2_carry_i_1_n_0
    );
cnt_low_frame2_carry_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00009009"
    )
        port map (
      I0 => averagVal_r(6),
      I1 => \cnt_low_frame_reg_n_0_[6]\,
      I2 => \cnt_low_frame_reg_n_0_[7]\,
      I3 => averagVal_r(7),
      I4 => averagVal_r(8),
      O => cnt_low_frame2_carry_i_2_n_0
    );
cnt_low_frame2_carry_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[4]\,
      I1 => averagVal_r(4),
      I2 => \cnt_low_frame_reg_n_0_[3]\,
      I3 => averagVal_r(3),
      I4 => averagVal_r(5),
      I5 => \cnt_low_frame_reg_n_0_[5]\,
      O => cnt_low_frame2_carry_i_3_n_0
    );
cnt_low_frame2_carry_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[2]\,
      I1 => averagVal_r(2),
      I2 => \cnt_low_frame_reg_n_0_[0]\,
      I3 => averagVal_r(0),
      I4 => averagVal_r(1),
      I5 => \cnt_low_frame_reg_n_0_[1]\,
      O => cnt_low_frame2_carry_i_4_n_0
    );
\cnt_low_frame8[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => azimut_0,
      I1 => s00_axi_aresetn,
      O => amountOfAzimuthFromCpu_r_0
    );
\cnt_low_frame8[0]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \cnt_low_frame[7]_i_4_n_0\,
      I1 => cnt_low_frame20_in,
      O => \cnt_low_frame8[0]_i_2_n_0\
    );
\cnt_low_frame8[0]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt_low_frame8_reg(0),
      O => \cnt_low_frame8[0]_i_4_n_0\
    );
\cnt_low_frame8_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => \cnt_low_frame8[0]_i_2_n_0\,
      D => \cnt_low_frame8_reg[0]_i_3_n_7\,
      Q => cnt_low_frame8_reg(0),
      R => amountOfAzimuthFromCpu_r_0
    );
\cnt_low_frame8_reg[0]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \cnt_low_frame8_reg[0]_i_3_n_0\,
      CO(2) => \cnt_low_frame8_reg[0]_i_3_n_1\,
      CO(1) => \cnt_low_frame8_reg[0]_i_3_n_2\,
      CO(0) => \cnt_low_frame8_reg[0]_i_3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \cnt_low_frame8_reg[0]_i_3_n_4\,
      O(2) => \cnt_low_frame8_reg[0]_i_3_n_5\,
      O(1) => \cnt_low_frame8_reg[0]_i_3_n_6\,
      O(0) => \cnt_low_frame8_reg[0]_i_3_n_7\,
      S(3 downto 1) => cnt_low_frame8_reg(3 downto 1),
      S(0) => \cnt_low_frame8[0]_i_4_n_0\
    );
\cnt_low_frame8_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => \cnt_low_frame8[0]_i_2_n_0\,
      D => \cnt_low_frame8_reg[8]_i_1_n_5\,
      Q => cnt_low_frame8_reg(10),
      R => amountOfAzimuthFromCpu_r_0
    );
\cnt_low_frame8_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => \cnt_low_frame8[0]_i_2_n_0\,
      D => \cnt_low_frame8_reg[8]_i_1_n_4\,
      Q => cnt_low_frame8_reg(11),
      R => amountOfAzimuthFromCpu_r_0
    );
\cnt_low_frame8_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => \cnt_low_frame8[0]_i_2_n_0\,
      D => \cnt_low_frame8_reg[12]_i_1_n_7\,
      Q => cnt_low_frame8_reg(12),
      R => amountOfAzimuthFromCpu_r_0
    );
\cnt_low_frame8_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_low_frame8_reg[8]_i_1_n_0\,
      CO(3) => \NLW_cnt_low_frame8_reg[12]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \cnt_low_frame8_reg[12]_i_1_n_1\,
      CO(1) => \cnt_low_frame8_reg[12]_i_1_n_2\,
      CO(0) => \cnt_low_frame8_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt_low_frame8_reg[12]_i_1_n_4\,
      O(2) => \cnt_low_frame8_reg[12]_i_1_n_5\,
      O(1) => \cnt_low_frame8_reg[12]_i_1_n_6\,
      O(0) => \cnt_low_frame8_reg[12]_i_1_n_7\,
      S(3 downto 0) => cnt_low_frame8_reg(15 downto 12)
    );
\cnt_low_frame8_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => \cnt_low_frame8[0]_i_2_n_0\,
      D => \cnt_low_frame8_reg[12]_i_1_n_6\,
      Q => cnt_low_frame8_reg(13),
      R => amountOfAzimuthFromCpu_r_0
    );
\cnt_low_frame8_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => \cnt_low_frame8[0]_i_2_n_0\,
      D => \cnt_low_frame8_reg[12]_i_1_n_5\,
      Q => cnt_low_frame8_reg(14),
      R => amountOfAzimuthFromCpu_r_0
    );
\cnt_low_frame8_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => \cnt_low_frame8[0]_i_2_n_0\,
      D => \cnt_low_frame8_reg[12]_i_1_n_4\,
      Q => cnt_low_frame8_reg(15),
      R => amountOfAzimuthFromCpu_r_0
    );
\cnt_low_frame8_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => \cnt_low_frame8[0]_i_2_n_0\,
      D => \cnt_low_frame8_reg[0]_i_3_n_6\,
      Q => cnt_low_frame8_reg(1),
      R => amountOfAzimuthFromCpu_r_0
    );
\cnt_low_frame8_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => \cnt_low_frame8[0]_i_2_n_0\,
      D => \cnt_low_frame8_reg[0]_i_3_n_5\,
      Q => cnt_low_frame8_reg(2),
      R => amountOfAzimuthFromCpu_r_0
    );
\cnt_low_frame8_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => \cnt_low_frame8[0]_i_2_n_0\,
      D => \cnt_low_frame8_reg[0]_i_3_n_4\,
      Q => cnt_low_frame8_reg(3),
      R => amountOfAzimuthFromCpu_r_0
    );
\cnt_low_frame8_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => \cnt_low_frame8[0]_i_2_n_0\,
      D => \cnt_low_frame8_reg[4]_i_1_n_7\,
      Q => cnt_low_frame8_reg(4),
      R => amountOfAzimuthFromCpu_r_0
    );
\cnt_low_frame8_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_low_frame8_reg[0]_i_3_n_0\,
      CO(3) => \cnt_low_frame8_reg[4]_i_1_n_0\,
      CO(2) => \cnt_low_frame8_reg[4]_i_1_n_1\,
      CO(1) => \cnt_low_frame8_reg[4]_i_1_n_2\,
      CO(0) => \cnt_low_frame8_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt_low_frame8_reg[4]_i_1_n_4\,
      O(2) => \cnt_low_frame8_reg[4]_i_1_n_5\,
      O(1) => \cnt_low_frame8_reg[4]_i_1_n_6\,
      O(0) => \cnt_low_frame8_reg[4]_i_1_n_7\,
      S(3 downto 0) => cnt_low_frame8_reg(7 downto 4)
    );
\cnt_low_frame8_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => \cnt_low_frame8[0]_i_2_n_0\,
      D => \cnt_low_frame8_reg[4]_i_1_n_6\,
      Q => cnt_low_frame8_reg(5),
      R => amountOfAzimuthFromCpu_r_0
    );
\cnt_low_frame8_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => \cnt_low_frame8[0]_i_2_n_0\,
      D => \cnt_low_frame8_reg[4]_i_1_n_5\,
      Q => cnt_low_frame8_reg(6),
      R => amountOfAzimuthFromCpu_r_0
    );
\cnt_low_frame8_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => \cnt_low_frame8[0]_i_2_n_0\,
      D => \cnt_low_frame8_reg[4]_i_1_n_4\,
      Q => cnt_low_frame8_reg(7),
      R => amountOfAzimuthFromCpu_r_0
    );
\cnt_low_frame8_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => \cnt_low_frame8[0]_i_2_n_0\,
      D => \cnt_low_frame8_reg[8]_i_1_n_7\,
      Q => cnt_low_frame8_reg(8),
      R => amountOfAzimuthFromCpu_r_0
    );
\cnt_low_frame8_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_low_frame8_reg[4]_i_1_n_0\,
      CO(3) => \cnt_low_frame8_reg[8]_i_1_n_0\,
      CO(2) => \cnt_low_frame8_reg[8]_i_1_n_1\,
      CO(1) => \cnt_low_frame8_reg[8]_i_1_n_2\,
      CO(0) => \cnt_low_frame8_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt_low_frame8_reg[8]_i_1_n_4\,
      O(2) => \cnt_low_frame8_reg[8]_i_1_n_5\,
      O(1) => \cnt_low_frame8_reg[8]_i_1_n_6\,
      O(0) => \cnt_low_frame8_reg[8]_i_1_n_7\,
      S(3 downto 0) => cnt_low_frame8_reg(11 downto 8)
    );
\cnt_low_frame8_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => \cnt_low_frame8[0]_i_2_n_0\,
      D => \cnt_low_frame8_reg[8]_i_1_n_6\,
      Q => cnt_low_frame8_reg(9),
      R => amountOfAzimuthFromCpu_r_0
    );
\cnt_low_frame[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0DFF"
    )
        port map (
      I0 => cnt_low_frame20_in,
      I1 => azimut_0,
      I2 => \cnt_low_frame_reg_n_0_[0]\,
      I3 => s00_axi_aresetn,
      O => \cnt_low_frame[0]_i_1_n_0\
    );
\cnt_low_frame[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0DD0FFFF"
    )
        port map (
      I0 => cnt_low_frame20_in,
      I1 => azimut_0,
      I2 => \cnt_low_frame_reg_n_0_[0]\,
      I3 => \cnt_low_frame_reg_n_0_[1]\,
      I4 => s00_axi_aresetn,
      O => \cnt_low_frame[1]_i_1_n_0\
    );
\cnt_low_frame[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6A6A006AFFFFFFFF"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[2]\,
      I1 => \cnt_low_frame_reg_n_0_[1]\,
      I2 => \cnt_low_frame_reg_n_0_[0]\,
      I3 => cnt_low_frame20_in,
      I4 => azimut_0,
      I5 => s00_axi_aresetn,
      O => \cnt_low_frame[2]_i_1_n_0\
    );
\cnt_low_frame[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00006AAAFFFFFFFF"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[3]\,
      I1 => \cnt_low_frame_reg_n_0_[2]\,
      I2 => \cnt_low_frame_reg_n_0_[0]\,
      I3 => \cnt_low_frame_reg_n_0_[1]\,
      I4 => \cnt_low_frame[3]_i_2_n_0\,
      I5 => s00_axi_aresetn,
      O => \cnt_low_frame[3]_i_1_n_0\
    );
\cnt_low_frame[3]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => cnt_low_frame20_in,
      I1 => azimut_0,
      O => \cnt_low_frame[3]_i_2_n_0\
    );
\cnt_low_frame[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"9909FFFF"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[4]\,
      I1 => \cnt_low_frame[4]_i_2_n_0\,
      I2 => cnt_low_frame20_in,
      I3 => azimut_0,
      I4 => s00_axi_aresetn,
      O => \cnt_low_frame[4]_i_1_n_0\
    );
\cnt_low_frame[4]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[2]\,
      I1 => \cnt_low_frame_reg_n_0_[0]\,
      I2 => \cnt_low_frame_reg_n_0_[1]\,
      I3 => \cnt_low_frame_reg_n_0_[3]\,
      O => \cnt_low_frame[4]_i_2_n_0\
    );
\cnt_low_frame[5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"9909FFFF"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[5]\,
      I1 => \cnt_low_frame[5]_i_2_n_0\,
      I2 => cnt_low_frame20_in,
      I3 => azimut_0,
      I4 => s00_axi_aresetn,
      O => \cnt_low_frame[5]_i_1_n_0\
    );
\cnt_low_frame[5]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFFFFFF"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[3]\,
      I1 => \cnt_low_frame_reg_n_0_[1]\,
      I2 => \cnt_low_frame_reg_n_0_[0]\,
      I3 => \cnt_low_frame_reg_n_0_[2]\,
      I4 => \cnt_low_frame_reg_n_0_[4]\,
      O => \cnt_low_frame[5]_i_2_n_0\
    );
\cnt_low_frame[6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6606FFFF"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[6]\,
      I1 => \cnt_low_frame[7]_i_5_n_0\,
      I2 => cnt_low_frame20_in,
      I3 => azimut_0,
      I4 => s00_axi_aresetn,
      O => \cnt_low_frame[6]_i_1_n_0\
    );
\cnt_low_frame[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => azimut_0,
      I1 => s00_axi_aresetn,
      O => cnt_low_frame
    );
\cnt_low_frame[7]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \cnt_low_frame[7]_i_4_n_0\,
      I1 => s00_axi_aresetn,
      O => \cnt_low_frame[7]_i_2_n_0\
    );
\cnt_low_frame[7]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0DD0D0D0FFFFFFFF"
    )
        port map (
      I0 => cnt_low_frame20_in,
      I1 => azimut_0,
      I2 => \cnt_low_frame_reg_n_0_[7]\,
      I3 => \cnt_low_frame_reg_n_0_[6]\,
      I4 => \cnt_low_frame[7]_i_5_n_0\,
      I5 => s00_axi_aresetn,
      O => \cnt_low_frame[7]_i_3_n_0\
    );
\cnt_low_frame[7]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000002"
    )
        port map (
      I0 => \cnt_low_frame[7]_i_6_n_0\,
      I1 => \cnt_low_frame[7]_i_7_n_0\,
      I2 => \cnt_low_frame[7]_i_8_n_0\,
      I3 => cnt_not_allowed_clk_reg(13),
      I4 => cnt_not_allowed_clk_reg(7),
      I5 => cnt_not_allowed_clk_reg(2),
      O => \cnt_low_frame[7]_i_4_n_0\
    );
\cnt_low_frame[7]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[5]\,
      I1 => \cnt_low_frame_reg_n_0_[4]\,
      I2 => \cnt_low_frame_reg_n_0_[2]\,
      I3 => \cnt_low_frame_reg_n_0_[0]\,
      I4 => \cnt_low_frame_reg_n_0_[1]\,
      I5 => \cnt_low_frame_reg_n_0_[3]\,
      O => \cnt_low_frame[7]_i_5_n_0\
    );
\cnt_low_frame[7]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000400"
    )
        port map (
      I0 => cnt_not_allowed_clk_reg(0),
      I1 => cnt_not_allowed_clk_reg(5),
      I2 => cnt_not_allowed_clk_reg(10),
      I3 => cnt_not_allowed_clk_reg(4),
      I4 => cnt_not_allowed_clk_reg(6),
      I5 => cnt_not_allowed_clk_reg(11),
      O => \cnt_low_frame[7]_i_6_n_0\
    );
\cnt_low_frame[7]_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => cnt_not_allowed_clk_reg(1),
      I1 => cnt_not_allowed_clk_reg(14),
      I2 => cnt_not_allowed_clk_reg(3),
      I3 => cnt_not_allowed_clk_reg(15),
      O => \cnt_low_frame[7]_i_7_n_0\
    );
\cnt_low_frame[7]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => enable_module,
      I1 => cnt_not_allowed_clk_reg(12),
      I2 => cnt_not_allowed_clk_reg(9),
      I3 => cnt_not_allowed_clk_reg(8),
      O => \cnt_low_frame[7]_i_8_n_0\
    );
\cnt_low_frame_reg[0]\: unisim.vcomponents.FDSE
     port map (
      C => clk_10MHz,
      CE => \cnt_low_frame[7]_i_2_n_0\,
      D => \cnt_low_frame[0]_i_1_n_0\,
      Q => \cnt_low_frame_reg_n_0_[0]\,
      S => cnt_low_frame
    );
\cnt_low_frame_reg[1]\: unisim.vcomponents.FDSE
     port map (
      C => clk_10MHz,
      CE => \cnt_low_frame[7]_i_2_n_0\,
      D => \cnt_low_frame[1]_i_1_n_0\,
      Q => \cnt_low_frame_reg_n_0_[1]\,
      S => cnt_low_frame
    );
\cnt_low_frame_reg[2]\: unisim.vcomponents.FDSE
     port map (
      C => clk_10MHz,
      CE => \cnt_low_frame[7]_i_2_n_0\,
      D => \cnt_low_frame[2]_i_1_n_0\,
      Q => \cnt_low_frame_reg_n_0_[2]\,
      S => cnt_low_frame
    );
\cnt_low_frame_reg[3]\: unisim.vcomponents.FDSE
     port map (
      C => clk_10MHz,
      CE => \cnt_low_frame[7]_i_2_n_0\,
      D => \cnt_low_frame[3]_i_1_n_0\,
      Q => \cnt_low_frame_reg_n_0_[3]\,
      S => cnt_low_frame
    );
\cnt_low_frame_reg[4]\: unisim.vcomponents.FDSE
     port map (
      C => clk_10MHz,
      CE => \cnt_low_frame[7]_i_2_n_0\,
      D => \cnt_low_frame[4]_i_1_n_0\,
      Q => \cnt_low_frame_reg_n_0_[4]\,
      S => cnt_low_frame
    );
\cnt_low_frame_reg[5]\: unisim.vcomponents.FDSE
     port map (
      C => clk_10MHz,
      CE => \cnt_low_frame[7]_i_2_n_0\,
      D => \cnt_low_frame[5]_i_1_n_0\,
      Q => \cnt_low_frame_reg_n_0_[5]\,
      S => cnt_low_frame
    );
\cnt_low_frame_reg[6]\: unisim.vcomponents.FDSE
     port map (
      C => clk_10MHz,
      CE => \cnt_low_frame[7]_i_2_n_0\,
      D => \cnt_low_frame[6]_i_1_n_0\,
      Q => \cnt_low_frame_reg_n_0_[6]\,
      S => cnt_low_frame
    );
\cnt_low_frame_reg[7]\: unisim.vcomponents.FDSE
     port map (
      C => clk_10MHz,
      CE => \cnt_low_frame[7]_i_2_n_0\,
      D => \cnt_low_frame[7]_i_3_n_0\,
      Q => \cnt_low_frame_reg_n_0_[7]\,
      S => cnt_low_frame
    );
\cnt_not_allowed_clk[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => allowed_clk,
      I1 => s00_axi_aresetn,
      I2 => azimut_0,
      O => \cnt_not_allowed_clk[0]_i_1_n_0\
    );
\cnt_not_allowed_clk[0]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt_not_allowed_clk_reg(0),
      O => \cnt_not_allowed_clk[0]_i_3_n_0\
    );
\cnt_not_allowed_clk_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_not_allowed_clk_reg[0]_i_2_n_7\,
      Q => cnt_not_allowed_clk_reg(0),
      R => \cnt_not_allowed_clk[0]_i_1_n_0\
    );
\cnt_not_allowed_clk_reg[0]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \cnt_not_allowed_clk_reg[0]_i_2_n_0\,
      CO(2) => \cnt_not_allowed_clk_reg[0]_i_2_n_1\,
      CO(1) => \cnt_not_allowed_clk_reg[0]_i_2_n_2\,
      CO(0) => \cnt_not_allowed_clk_reg[0]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \cnt_not_allowed_clk_reg[0]_i_2_n_4\,
      O(2) => \cnt_not_allowed_clk_reg[0]_i_2_n_5\,
      O(1) => \cnt_not_allowed_clk_reg[0]_i_2_n_6\,
      O(0) => \cnt_not_allowed_clk_reg[0]_i_2_n_7\,
      S(3 downto 1) => cnt_not_allowed_clk_reg(3 downto 1),
      S(0) => \cnt_not_allowed_clk[0]_i_3_n_0\
    );
\cnt_not_allowed_clk_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_not_allowed_clk_reg[8]_i_1_n_5\,
      Q => cnt_not_allowed_clk_reg(10),
      R => \cnt_not_allowed_clk[0]_i_1_n_0\
    );
\cnt_not_allowed_clk_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_not_allowed_clk_reg[8]_i_1_n_4\,
      Q => cnt_not_allowed_clk_reg(11),
      R => \cnt_not_allowed_clk[0]_i_1_n_0\
    );
\cnt_not_allowed_clk_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_not_allowed_clk_reg[12]_i_1_n_7\,
      Q => cnt_not_allowed_clk_reg(12),
      R => \cnt_not_allowed_clk[0]_i_1_n_0\
    );
\cnt_not_allowed_clk_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_not_allowed_clk_reg[8]_i_1_n_0\,
      CO(3) => \NLW_cnt_not_allowed_clk_reg[12]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \cnt_not_allowed_clk_reg[12]_i_1_n_1\,
      CO(1) => \cnt_not_allowed_clk_reg[12]_i_1_n_2\,
      CO(0) => \cnt_not_allowed_clk_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt_not_allowed_clk_reg[12]_i_1_n_4\,
      O(2) => \cnt_not_allowed_clk_reg[12]_i_1_n_5\,
      O(1) => \cnt_not_allowed_clk_reg[12]_i_1_n_6\,
      O(0) => \cnt_not_allowed_clk_reg[12]_i_1_n_7\,
      S(3 downto 0) => cnt_not_allowed_clk_reg(15 downto 12)
    );
\cnt_not_allowed_clk_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_not_allowed_clk_reg[12]_i_1_n_6\,
      Q => cnt_not_allowed_clk_reg(13),
      R => \cnt_not_allowed_clk[0]_i_1_n_0\
    );
\cnt_not_allowed_clk_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_not_allowed_clk_reg[12]_i_1_n_5\,
      Q => cnt_not_allowed_clk_reg(14),
      R => \cnt_not_allowed_clk[0]_i_1_n_0\
    );
\cnt_not_allowed_clk_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_not_allowed_clk_reg[12]_i_1_n_4\,
      Q => cnt_not_allowed_clk_reg(15),
      R => \cnt_not_allowed_clk[0]_i_1_n_0\
    );
\cnt_not_allowed_clk_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_not_allowed_clk_reg[0]_i_2_n_6\,
      Q => cnt_not_allowed_clk_reg(1),
      R => \cnt_not_allowed_clk[0]_i_1_n_0\
    );
\cnt_not_allowed_clk_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_not_allowed_clk_reg[0]_i_2_n_5\,
      Q => cnt_not_allowed_clk_reg(2),
      R => \cnt_not_allowed_clk[0]_i_1_n_0\
    );
\cnt_not_allowed_clk_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_not_allowed_clk_reg[0]_i_2_n_4\,
      Q => cnt_not_allowed_clk_reg(3),
      R => \cnt_not_allowed_clk[0]_i_1_n_0\
    );
\cnt_not_allowed_clk_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_not_allowed_clk_reg[4]_i_1_n_7\,
      Q => cnt_not_allowed_clk_reg(4),
      R => \cnt_not_allowed_clk[0]_i_1_n_0\
    );
\cnt_not_allowed_clk_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_not_allowed_clk_reg[0]_i_2_n_0\,
      CO(3) => \cnt_not_allowed_clk_reg[4]_i_1_n_0\,
      CO(2) => \cnt_not_allowed_clk_reg[4]_i_1_n_1\,
      CO(1) => \cnt_not_allowed_clk_reg[4]_i_1_n_2\,
      CO(0) => \cnt_not_allowed_clk_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt_not_allowed_clk_reg[4]_i_1_n_4\,
      O(2) => \cnt_not_allowed_clk_reg[4]_i_1_n_5\,
      O(1) => \cnt_not_allowed_clk_reg[4]_i_1_n_6\,
      O(0) => \cnt_not_allowed_clk_reg[4]_i_1_n_7\,
      S(3 downto 0) => cnt_not_allowed_clk_reg(7 downto 4)
    );
\cnt_not_allowed_clk_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_not_allowed_clk_reg[4]_i_1_n_6\,
      Q => cnt_not_allowed_clk_reg(5),
      R => \cnt_not_allowed_clk[0]_i_1_n_0\
    );
\cnt_not_allowed_clk_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_not_allowed_clk_reg[4]_i_1_n_5\,
      Q => cnt_not_allowed_clk_reg(6),
      R => \cnt_not_allowed_clk[0]_i_1_n_0\
    );
\cnt_not_allowed_clk_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_not_allowed_clk_reg[4]_i_1_n_4\,
      Q => cnt_not_allowed_clk_reg(7),
      R => \cnt_not_allowed_clk[0]_i_1_n_0\
    );
\cnt_not_allowed_clk_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_not_allowed_clk_reg[8]_i_1_n_7\,
      Q => cnt_not_allowed_clk_reg(8),
      R => \cnt_not_allowed_clk[0]_i_1_n_0\
    );
\cnt_not_allowed_clk_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_not_allowed_clk_reg[4]_i_1_n_0\,
      CO(3) => \cnt_not_allowed_clk_reg[8]_i_1_n_0\,
      CO(2) => \cnt_not_allowed_clk_reg[8]_i_1_n_1\,
      CO(1) => \cnt_not_allowed_clk_reg[8]_i_1_n_2\,
      CO(0) => \cnt_not_allowed_clk_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt_not_allowed_clk_reg[8]_i_1_n_4\,
      O(2) => \cnt_not_allowed_clk_reg[8]_i_1_n_5\,
      O(1) => \cnt_not_allowed_clk_reg[8]_i_1_n_6\,
      O(0) => \cnt_not_allowed_clk_reg[8]_i_1_n_7\,
      S(3 downto 0) => cnt_not_allowed_clk_reg(11 downto 8)
    );
\cnt_not_allowed_clk_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_not_allowed_clk_reg[8]_i_1_n_6\,
      Q => cnt_not_allowed_clk_reg(9),
      R => \cnt_not_allowed_clk[0]_i_1_n_0\
    );
enable_cnt_low8_frame_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"C8"
    )
        port map (
      I0 => enable_module,
      I1 => s00_axi_aresetn,
      I2 => azimut_0,
      O => enable_cnt_low8_frame_i_1_n_0
    );
enable_cnt_low8_frame_reg: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => enable_cnt_low8_frame_i_1_n_0,
      Q => enable_module,
      R => '0'
    );
\m00_dma_axis_tdata[0]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(0),
      I3 => s00_fft_axis_tdata(0),
      O => m00_dma_axis_tdata(0)
    );
\m00_dma_axis_tdata[10]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(10),
      I3 => s00_fft_axis_tdata(10),
      O => m00_dma_axis_tdata(10)
    );
\m00_dma_axis_tdata[11]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(11),
      I3 => s00_fft_axis_tdata(11),
      O => m00_dma_axis_tdata(11)
    );
\m00_dma_axis_tdata[12]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(12),
      I3 => s00_fft_axis_tdata(12),
      O => m00_dma_axis_tdata(12)
    );
\m00_dma_axis_tdata[13]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(13),
      I3 => s00_fft_axis_tdata(13),
      O => m00_dma_axis_tdata(13)
    );
\m00_dma_axis_tdata[14]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(14),
      I3 => s00_fft_axis_tdata(14),
      O => m00_dma_axis_tdata(14)
    );
\m00_dma_axis_tdata[15]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(15),
      I3 => s00_fft_axis_tdata(15),
      O => m00_dma_axis_tdata(15)
    );
\m00_dma_axis_tdata[16]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(16),
      I3 => s00_fft_axis_tdata(16),
      O => m00_dma_axis_tdata(16)
    );
\m00_dma_axis_tdata[17]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(17),
      I3 => s00_fft_axis_tdata(17),
      O => m00_dma_axis_tdata(17)
    );
\m00_dma_axis_tdata[18]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(18),
      I3 => s00_fft_axis_tdata(18),
      O => m00_dma_axis_tdata(18)
    );
\m00_dma_axis_tdata[19]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(19),
      I3 => s00_fft_axis_tdata(19),
      O => m00_dma_axis_tdata(19)
    );
\m00_dma_axis_tdata[1]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(1),
      I3 => s00_fft_axis_tdata(1),
      O => m00_dma_axis_tdata(1)
    );
\m00_dma_axis_tdata[20]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(20),
      I3 => s00_fft_axis_tdata(20),
      O => m00_dma_axis_tdata(20)
    );
\m00_dma_axis_tdata[21]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(21),
      I3 => s00_fft_axis_tdata(21),
      O => m00_dma_axis_tdata(21)
    );
\m00_dma_axis_tdata[22]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(22),
      I3 => s00_fft_axis_tdata(22),
      O => m00_dma_axis_tdata(22)
    );
\m00_dma_axis_tdata[23]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(23),
      I3 => s00_fft_axis_tdata(23),
      O => m00_dma_axis_tdata(23)
    );
\m00_dma_axis_tdata[24]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(24),
      I3 => s00_fft_axis_tdata(24),
      O => m00_dma_axis_tdata(24)
    );
\m00_dma_axis_tdata[25]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(25),
      I3 => s00_fft_axis_tdata(25),
      O => m00_dma_axis_tdata(25)
    );
\m00_dma_axis_tdata[26]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(26),
      I3 => s00_fft_axis_tdata(26),
      O => m00_dma_axis_tdata(26)
    );
\m00_dma_axis_tdata[27]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(27),
      I3 => s00_fft_axis_tdata(27),
      O => m00_dma_axis_tdata(27)
    );
\m00_dma_axis_tdata[28]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(28),
      I3 => s00_fft_axis_tdata(28),
      O => m00_dma_axis_tdata(28)
    );
\m00_dma_axis_tdata[29]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(29),
      I3 => s00_fft_axis_tdata(29),
      O => m00_dma_axis_tdata(29)
    );
\m00_dma_axis_tdata[2]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(2),
      I3 => s00_fft_axis_tdata(2),
      O => m00_dma_axis_tdata(2)
    );
\m00_dma_axis_tdata[30]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(30),
      I3 => s00_fft_axis_tdata(30),
      O => m00_dma_axis_tdata(30)
    );
\m00_dma_axis_tdata[31]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(31),
      I3 => s00_fft_axis_tdata(31),
      O => m00_dma_axis_tdata(31)
    );
\m00_dma_axis_tdata[32]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(32),
      I3 => s00_fft_axis_tdata(32),
      O => m00_dma_axis_tdata(32)
    );
\m00_dma_axis_tdata[33]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(33),
      I3 => s00_fft_axis_tdata(33),
      O => m00_dma_axis_tdata(33)
    );
\m00_dma_axis_tdata[34]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(34),
      I3 => s00_fft_axis_tdata(34),
      O => m00_dma_axis_tdata(34)
    );
\m00_dma_axis_tdata[35]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(35),
      I3 => s00_fft_axis_tdata(35),
      O => m00_dma_axis_tdata(35)
    );
\m00_dma_axis_tdata[36]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(36),
      I3 => s00_fft_axis_tdata(36),
      O => m00_dma_axis_tdata(36)
    );
\m00_dma_axis_tdata[37]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(37),
      I3 => s00_fft_axis_tdata(37),
      O => m00_dma_axis_tdata(37)
    );
\m00_dma_axis_tdata[38]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(38),
      I3 => s00_fft_axis_tdata(38),
      O => m00_dma_axis_tdata(38)
    );
\m00_dma_axis_tdata[39]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(39),
      I3 => s00_fft_axis_tdata(39),
      O => m00_dma_axis_tdata(39)
    );
\m00_dma_axis_tdata[3]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(3),
      I3 => s00_fft_axis_tdata(3),
      O => m00_dma_axis_tdata(3)
    );
\m00_dma_axis_tdata[40]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(40),
      I3 => s00_fft_axis_tdata(40),
      O => m00_dma_axis_tdata(40)
    );
\m00_dma_axis_tdata[41]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(41),
      I3 => s00_fft_axis_tdata(41),
      O => m00_dma_axis_tdata(41)
    );
\m00_dma_axis_tdata[42]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(42),
      I3 => s00_fft_axis_tdata(42),
      O => m00_dma_axis_tdata(42)
    );
\m00_dma_axis_tdata[43]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(43),
      I3 => s00_fft_axis_tdata(43),
      O => m00_dma_axis_tdata(43)
    );
\m00_dma_axis_tdata[44]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(44),
      I3 => s00_fft_axis_tdata(44),
      O => m00_dma_axis_tdata(44)
    );
\m00_dma_axis_tdata[45]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(45),
      I3 => s00_fft_axis_tdata(45),
      O => m00_dma_axis_tdata(45)
    );
\m00_dma_axis_tdata[46]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(46),
      I3 => s00_fft_axis_tdata(46),
      O => m00_dma_axis_tdata(46)
    );
\m00_dma_axis_tdata[47]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(47),
      I3 => s00_fft_axis_tdata(47),
      O => m00_dma_axis_tdata(47)
    );
\m00_dma_axis_tdata[48]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(48),
      I3 => s00_fft_axis_tdata(48),
      O => m00_dma_axis_tdata(48)
    );
\m00_dma_axis_tdata[49]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(49),
      I3 => s00_fft_axis_tdata(49),
      O => m00_dma_axis_tdata(49)
    );
\m00_dma_axis_tdata[4]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(4),
      I3 => s00_fft_axis_tdata(4),
      O => m00_dma_axis_tdata(4)
    );
\m00_dma_axis_tdata[50]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(50),
      I3 => s00_fft_axis_tdata(50),
      O => m00_dma_axis_tdata(50)
    );
\m00_dma_axis_tdata[51]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(51),
      I3 => s00_fft_axis_tdata(51),
      O => m00_dma_axis_tdata(51)
    );
\m00_dma_axis_tdata[52]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(52),
      I3 => s00_fft_axis_tdata(52),
      O => m00_dma_axis_tdata(52)
    );
\m00_dma_axis_tdata[53]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(53),
      I3 => s00_fft_axis_tdata(53),
      O => m00_dma_axis_tdata(53)
    );
\m00_dma_axis_tdata[54]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(54),
      I3 => s00_fft_axis_tdata(54),
      O => m00_dma_axis_tdata(54)
    );
\m00_dma_axis_tdata[55]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(55),
      I3 => s00_fft_axis_tdata(55),
      O => m00_dma_axis_tdata(55)
    );
\m00_dma_axis_tdata[56]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(56),
      I3 => s00_fft_axis_tdata(56),
      O => m00_dma_axis_tdata(56)
    );
\m00_dma_axis_tdata[57]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(57),
      I3 => s00_fft_axis_tdata(57),
      O => m00_dma_axis_tdata(57)
    );
\m00_dma_axis_tdata[58]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(58),
      I3 => s00_fft_axis_tdata(58),
      O => m00_dma_axis_tdata(58)
    );
\m00_dma_axis_tdata[59]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(59),
      I3 => s00_fft_axis_tdata(59),
      O => m00_dma_axis_tdata(59)
    );
\m00_dma_axis_tdata[5]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(5),
      I3 => s00_fft_axis_tdata(5),
      O => m00_dma_axis_tdata(5)
    );
\m00_dma_axis_tdata[60]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(60),
      I3 => s00_fft_axis_tdata(60),
      O => m00_dma_axis_tdata(60)
    );
\m00_dma_axis_tdata[61]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(61),
      I3 => s00_fft_axis_tdata(61),
      O => m00_dma_axis_tdata(61)
    );
\m00_dma_axis_tdata[62]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(62),
      I3 => s00_fft_axis_tdata(62),
      O => m00_dma_axis_tdata(62)
    );
\m00_dma_axis_tdata[63]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(63),
      I3 => s00_fft_axis_tdata(63),
      O => m00_dma_axis_tdata(63)
    );
\m00_dma_axis_tdata[6]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(6),
      I3 => s00_fft_axis_tdata(6),
      O => m00_dma_axis_tdata(6)
    );
\m00_dma_axis_tdata[7]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(7),
      I3 => s00_fft_axis_tdata(7),
      O => m00_dma_axis_tdata(7)
    );
\m00_dma_axis_tdata[8]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(8),
      I3 => s00_fft_axis_tdata(8),
      O => m00_dma_axis_tdata(8)
    );
\m00_dma_axis_tdata[9]_INST_0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tdata(9),
      I3 => s00_fft_axis_tdata(9),
      O => m00_dma_axis_tdata(9)
    );
m00_dma_axis_tlast_INST_0: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tlast,
      I3 => s00_fft_axis_tlast,
      O => m00_dma_axis_tlast
    );
m00_dma_axis_tvalid_INST_0: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F960"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => s01_fft_axis_tvalid,
      I3 => s00_fft_axis_tvalid,
      O => m00_dma_axis_tvalid
    );
\m00_fft_axis_tdata[0]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"60"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INA(0),
      O => m00_fft_axis_tdata(0)
    );
\m00_fft_axis_tdata[10]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"60"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INA(10),
      O => m00_fft_axis_tdata(10)
    );
\m00_fft_axis_tdata[11]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"60"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INA(11),
      O => m00_fft_axis_tdata(11)
    );
\m00_fft_axis_tdata[12]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"60"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INA(12),
      O => m00_fft_axis_tdata(12)
    );
\m00_fft_axis_tdata[13]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"60"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INA(13),
      O => m00_fft_axis_tdata(13)
    );
\m00_fft_axis_tdata[14]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"60"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INA(14),
      O => m00_fft_axis_tdata(14)
    );
\m00_fft_axis_tdata[15]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"60"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INA(15),
      O => m00_fft_axis_tdata(15)
    );
\m00_fft_axis_tdata[16]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"60"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INB(0),
      O => m00_fft_axis_tdata(16)
    );
\m00_fft_axis_tdata[17]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"60"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INB(1),
      O => m00_fft_axis_tdata(17)
    );
\m00_fft_axis_tdata[18]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"60"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INB(2),
      O => m00_fft_axis_tdata(18)
    );
\m00_fft_axis_tdata[19]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"60"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INB(3),
      O => m00_fft_axis_tdata(19)
    );
\m00_fft_axis_tdata[1]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"60"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INA(1),
      O => m00_fft_axis_tdata(1)
    );
\m00_fft_axis_tdata[20]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"60"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INB(4),
      O => m00_fft_axis_tdata(20)
    );
\m00_fft_axis_tdata[21]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"60"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INB(5),
      O => m00_fft_axis_tdata(21)
    );
\m00_fft_axis_tdata[22]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"60"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INB(6),
      O => m00_fft_axis_tdata(22)
    );
\m00_fft_axis_tdata[23]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"60"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INB(7),
      O => m00_fft_axis_tdata(23)
    );
\m00_fft_axis_tdata[24]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"60"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INB(8),
      O => m00_fft_axis_tdata(24)
    );
\m00_fft_axis_tdata[25]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"60"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INB(9),
      O => m00_fft_axis_tdata(25)
    );
\m00_fft_axis_tdata[26]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"60"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INB(10),
      O => m00_fft_axis_tdata(26)
    );
\m00_fft_axis_tdata[27]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"60"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INB(11),
      O => m00_fft_axis_tdata(27)
    );
\m00_fft_axis_tdata[28]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"60"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INB(12),
      O => m00_fft_axis_tdata(28)
    );
\m00_fft_axis_tdata[29]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"60"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INB(13),
      O => m00_fft_axis_tdata(29)
    );
\m00_fft_axis_tdata[2]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"60"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INA(2),
      O => m00_fft_axis_tdata(2)
    );
\m00_fft_axis_tdata[30]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"60"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INB(14),
      O => m00_fft_axis_tdata(30)
    );
\m00_fft_axis_tdata[31]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"60"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INB(15),
      O => m00_fft_axis_tdata(31)
    );
\m00_fft_axis_tdata[3]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"60"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INA(3),
      O => m00_fft_axis_tdata(3)
    );
\m00_fft_axis_tdata[4]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"60"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INA(4),
      O => m00_fft_axis_tdata(4)
    );
\m00_fft_axis_tdata[5]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"60"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INA(5),
      O => m00_fft_axis_tdata(5)
    );
\m00_fft_axis_tdata[6]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"60"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INA(6),
      O => m00_fft_axis_tdata(6)
    );
\m00_fft_axis_tdata[7]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"60"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INA(7),
      O => m00_fft_axis_tdata(7)
    );
\m00_fft_axis_tdata[8]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"60"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INA(8),
      O => m00_fft_axis_tdata(8)
    );
\m00_fft_axis_tdata[9]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"60"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INA(9),
      O => m00_fft_axis_tdata(9)
    );
m00_fft_axis_tlast_r_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6000"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => m01_fft_axis_tlast_r0,
      I3 => s00_axi_aresetn,
      O => m00_fft_axis_tlast_r_i_1_n_0
    );
m00_fft_axis_tlast_r_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00020000"
    )
        port map (
      I0 => m00_fft_axis_tvalid_r_i_4_n_0,
      I1 => m00_fft_axis_tvalid_r_i_3_n_0,
      I2 => cnt_DCO(14),
      I3 => cnt_DCO(15),
      I4 => cnt_DCO(13),
      O => m01_fft_axis_tlast_r0
    );
m00_fft_axis_tlast_r_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => m00_fft_axis_tlast_r_i_1_n_0,
      Q => m00_fft_axis_tlast,
      R => '0'
    );
m00_fft_axis_tvalid_r_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6000"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => m01_fft_axis_tvalid_r0,
      I3 => s00_axi_aresetn,
      O => m00_fft_axis_tvalid_r_i_1_n_0
    );
m00_fft_axis_tvalid_r_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"01000001"
    )
        port map (
      I0 => cnt_DCO(14),
      I1 => cnt_DCO(15),
      I2 => m00_fft_axis_tvalid_r_i_3_n_0,
      I3 => cnt_DCO(13),
      I4 => m00_fft_axis_tvalid_r_i_4_n_0,
      O => m01_fft_axis_tvalid_r0
    );
m00_fft_axis_tvalid_r_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => m00_fft_axis_tvalid_r_i_5_n_0,
      I1 => cnt_in_DCO_reg(14),
      I2 => cnt_in_DCO_reg(2),
      I3 => cnt_in_DCO_reg(9),
      I4 => cnt_in_DCO_reg(4),
      I5 => m00_fft_axis_tvalid_r_i_6_n_0,
      O => m00_fft_axis_tvalid_r_i_3_n_0
    );
m00_fft_axis_tvalid_r_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => \cnt_DCO[15]_i_4_n_0\,
      I1 => cnt_DCO(2),
      I2 => cnt_DCO(3),
      I3 => cnt_DCO(0),
      O => m00_fft_axis_tvalid_r_i_4_n_0
    );
m00_fft_axis_tvalid_r_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => cnt_in_DCO_reg(13),
      I1 => cnt_in_DCO_reg(3),
      I2 => cnt_in_DCO_reg(7),
      I3 => cnt_in_DCO_reg(1),
      O => m00_fft_axis_tvalid_r_i_5_n_0
    );
m00_fft_axis_tvalid_r_i_6: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFEFF"
    )
        port map (
      I0 => cnt_in_DCO_reg(5),
      I1 => cnt_in_DCO_reg(11),
      I2 => cnt_in_DCO_reg(15),
      I3 => cnt_in_DCO_reg(0),
      I4 => m00_fft_axis_tvalid_r_i_7_n_0,
      O => m00_fft_axis_tvalid_r_i_6_n_0
    );
m00_fft_axis_tvalid_r_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => cnt_in_DCO_reg(12),
      I1 => cnt_in_DCO_reg(10),
      I2 => cnt_in_DCO_reg(8),
      I3 => cnt_in_DCO_reg(6),
      O => m00_fft_axis_tvalid_r_i_7_n_0
    );
m00_fft_axis_tvalid_r_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => m00_fft_axis_tvalid_r_i_1_n_0,
      Q => m00_fft_axis_tvalid,
      R => '0'
    );
\m01_fft_axis_tdata[0]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"90"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INA(0),
      O => m01_fft_axis_tdata(0)
    );
\m01_fft_axis_tdata[10]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"90"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INA(10),
      O => m01_fft_axis_tdata(10)
    );
\m01_fft_axis_tdata[11]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"90"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INA(11),
      O => m01_fft_axis_tdata(11)
    );
\m01_fft_axis_tdata[12]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"90"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INA(12),
      O => m01_fft_axis_tdata(12)
    );
\m01_fft_axis_tdata[13]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"90"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INA(13),
      O => m01_fft_axis_tdata(13)
    );
\m01_fft_axis_tdata[14]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"90"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INA(14),
      O => m01_fft_axis_tdata(14)
    );
\m01_fft_axis_tdata[15]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"90"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INA(15),
      O => m01_fft_axis_tdata(15)
    );
\m01_fft_axis_tdata[16]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"90"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INB(0),
      O => m01_fft_axis_tdata(16)
    );
\m01_fft_axis_tdata[17]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"90"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INB(1),
      O => m01_fft_axis_tdata(17)
    );
\m01_fft_axis_tdata[18]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"90"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INB(2),
      O => m01_fft_axis_tdata(18)
    );
\m01_fft_axis_tdata[19]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"90"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INB(3),
      O => m01_fft_axis_tdata(19)
    );
\m01_fft_axis_tdata[1]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"90"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INA(1),
      O => m01_fft_axis_tdata(1)
    );
\m01_fft_axis_tdata[20]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"90"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INB(4),
      O => m01_fft_axis_tdata(20)
    );
\m01_fft_axis_tdata[21]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"90"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INB(5),
      O => m01_fft_axis_tdata(21)
    );
\m01_fft_axis_tdata[22]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"90"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INB(6),
      O => m01_fft_axis_tdata(22)
    );
\m01_fft_axis_tdata[23]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"90"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INB(7),
      O => m01_fft_axis_tdata(23)
    );
\m01_fft_axis_tdata[24]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"90"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INB(8),
      O => m01_fft_axis_tdata(24)
    );
\m01_fft_axis_tdata[25]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"90"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INB(9),
      O => m01_fft_axis_tdata(25)
    );
\m01_fft_axis_tdata[26]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"90"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INB(10),
      O => m01_fft_axis_tdata(26)
    );
\m01_fft_axis_tdata[27]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"90"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INB(11),
      O => m01_fft_axis_tdata(27)
    );
\m01_fft_axis_tdata[28]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"90"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INB(12),
      O => m01_fft_axis_tdata(28)
    );
\m01_fft_axis_tdata[29]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"90"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INB(13),
      O => m01_fft_axis_tdata(29)
    );
\m01_fft_axis_tdata[2]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"90"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INA(2),
      O => m01_fft_axis_tdata(2)
    );
\m01_fft_axis_tdata[30]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"90"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INB(14),
      O => m01_fft_axis_tdata(30)
    );
\m01_fft_axis_tdata[31]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"90"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INB(15),
      O => m01_fft_axis_tdata(31)
    );
\m01_fft_axis_tdata[3]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"90"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INA(3),
      O => m01_fft_axis_tdata(3)
    );
\m01_fft_axis_tdata[4]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"90"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INA(4),
      O => m01_fft_axis_tdata(4)
    );
\m01_fft_axis_tdata[5]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"90"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INA(5),
      O => m01_fft_axis_tdata(5)
    );
\m01_fft_axis_tdata[6]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"90"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INA(6),
      O => m01_fft_axis_tdata(6)
    );
\m01_fft_axis_tdata[7]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"90"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INA(7),
      O => m01_fft_axis_tdata(7)
    );
\m01_fft_axis_tdata[8]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"90"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INA(8),
      O => m01_fft_axis_tdata(8)
    );
\m01_fft_axis_tdata[9]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"90"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => DATA_INA(9),
      O => m01_fft_axis_tdata(9)
    );
m01_fft_axis_tlast_r_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9000"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => m01_fft_axis_tlast_r0,
      I3 => s00_axi_aresetn,
      O => m01_fft_axis_tlast_r_i_1_n_0
    );
m01_fft_axis_tlast_r_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => m01_fft_axis_tlast_r_i_1_n_0,
      Q => m01_fft_axis_tlast,
      R => '0'
    );
m01_fft_axis_tvalid_r_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9000"
    )
        port map (
      I0 => \cnt_low_frame_reg_n_0_[0]\,
      I1 => cnt_low_frame8_reg(0),
      I2 => m01_fft_axis_tvalid_r0,
      I3 => s00_axi_aresetn,
      O => m01_fft_axis_tvalid_r_i_1_n_0
    );
m01_fft_axis_tvalid_r_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => m01_fft_axis_tvalid_r_i_1_n_0,
      Q => m01_fft_axis_tvalid,
      R => '0'
    );
spi_AD9650_inst: entity work.design_1_AD9650_0_0_spi_AD9650
     port map (
      DATA_RX_r(7 downto 0) => DATA_RX_r(7 downto 0),
      DATA_TX_serial_r_i_3_0(0) => AD9650_v3_0_S00_AXI_inst_n_42,
      DATA_TX_serial_r_i_3_1 => AD9650_v3_0_S00_AXI_inst_n_8,
      DATA_TX_serial_r_reg_0 => AD9650_v3_0_S00_AXI_inst_n_7,
      DATA_TX_serial_r_reg_1 => AD9650_v3_0_S00_AXI_inst_n_6,
      Q(1 downto 0) => cnt_reg(1 downto 0),
      adc_spi_cs => adc_spi_cs,
      adc_spi_sck => adc_spi_sck,
      adc_spi_sdio => adc_spi_sdio,
      clk_10MHz => clk_10MHz,
      \cnt_re_reg[0]_0\(0) => AD9650_v3_0_S00_AXI_inst_n_44,
      \cnt_reg[2]_0\ => spi_AD9650_inst_n_5,
      data4(0) => data4(0),
      s00_axi_aclk => s00_axi_aclk,
      start_sync_reg_0(0) => slv_reg1(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_AD9650_0_0 is
  port (
    clk_10MHz : in STD_LOGIC;
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 5 downto 0 );
    s00_axi_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_awready : out STD_LOGIC;
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_wvalid : in STD_LOGIC;
    s00_axi_wready : out STD_LOGIC;
    s00_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_bvalid : out STD_LOGIC;
    s00_axi_bready : in STD_LOGIC;
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 5 downto 0 );
    s00_axi_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_arvalid : in STD_LOGIC;
    s00_axi_arready : out STD_LOGIC;
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_rvalid : out STD_LOGIC;
    s00_axi_rready : in STD_LOGIC;
    m00_fft_axis_tvalid : out STD_LOGIC;
    m00_fft_axis_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_fft_axis_tstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m00_fft_axis_tlast : out STD_LOGIC;
    m00_fft_axis_tready : in STD_LOGIC;
    m01_fft_axis_tvalid : out STD_LOGIC;
    m01_fft_axis_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m01_fft_axis_tstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m01_fft_axis_tlast : out STD_LOGIC;
    m01_fft_axis_tready : in STD_LOGIC;
    s00_fft_axis_tvalid : in STD_LOGIC;
    s00_fft_axis_tdata : in STD_LOGIC_VECTOR ( 63 downto 0 );
    s00_fft_axis_tstrb : in STD_LOGIC_VECTOR ( 7 downto 0 );
    s00_fft_axis_tlast : in STD_LOGIC;
    s00_fft_axis_tready : out STD_LOGIC;
    s01_fft_axis_tvalid : in STD_LOGIC;
    s01_fft_axis_tdata : in STD_LOGIC_VECTOR ( 63 downto 0 );
    s01_fft_axis_tstrb : in STD_LOGIC_VECTOR ( 7 downto 0 );
    s01_fft_axis_tlast : in STD_LOGIC;
    s01_fft_axis_tready : out STD_LOGIC;
    m00_dma_axis_tvalid : out STD_LOGIC;
    m00_dma_axis_tdata : out STD_LOGIC_VECTOR ( 63 downto 0 );
    m00_dma_axis_tstrb : out STD_LOGIC_VECTOR ( 7 downto 0 );
    m00_dma_axis_tlast : out STD_LOGIC;
    m00_dma_axis_tready : in STD_LOGIC;
    adc_spi_sck : out STD_LOGIC;
    adc_spi_cs : out STD_LOGIC;
    adc_spi_sdio : inout STD_LOGIC;
    ADC_PDwN : out STD_LOGIC;
    SYNC : out STD_LOGIC;
    DATA_INA : in STD_LOGIC_VECTOR ( 15 downto 0 );
    DATA_INB : in STD_LOGIC_VECTOR ( 15 downto 0 );
    allowed_clk : in STD_LOGIC;
    azimut_0 : in STD_LOGIC;
    allowed_read_out : out STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of design_1_AD9650_0_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of design_1_AD9650_0_0 : entity is "design_1_AD9650_0_0,AD9650_v3_0,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of design_1_AD9650_0_0 : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of design_1_AD9650_0_0 : entity is "AD9650_v3_0,Vivado 2019.1";
end design_1_AD9650_0_0;

architecture STRUCTURE of design_1_AD9650_0_0 is
  signal \<const0>\ : STD_LOGIC;
  signal \<const1>\ : STD_LOGIC;
  signal n_0_263 : STD_LOGIC;
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of adc_spi_cs : signal is "bt.local:interface:adc_spi:1.0 ADC_SPI cs";
  attribute X_INTERFACE_INFO of adc_spi_sck : signal is "bt.local:interface:adc_spi:1.0 ADC_SPI sck";
  attribute X_INTERFACE_INFO of adc_spi_sdio : signal is "bt.local:interface:adc_spi:1.0 ADC_SPI sdio";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of adc_spi_sdio : signal is "XIL_INTERFACENAME ADC_SPI, SV_INTERFACE true";
  attribute X_INTERFACE_INFO of allowed_clk : signal is "xilinx.com:signal:clock:1.0 allowed_clk CLK";
  attribute X_INTERFACE_PARAMETER of allowed_clk : signal is "XIL_INTERFACENAME allowed_clk, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_dma_axis_tlast : signal is "xilinx.com:interface:axis:1.0 m00_dma_axis TLAST";
  attribute X_INTERFACE_INFO of m00_dma_axis_tready : signal is "xilinx.com:interface:axis:1.0 m00_dma_axis TREADY";
  attribute X_INTERFACE_PARAMETER of m00_dma_axis_tready : signal is "XIL_INTERFACENAME m00_dma_axis, TDATA_NUM_BYTES 8, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_dma_axis_tvalid : signal is "xilinx.com:interface:axis:1.0 m00_dma_axis TVALID";
  attribute X_INTERFACE_INFO of m00_fft_axis_tlast : signal is "xilinx.com:interface:axis:1.0 m00_fft_axis TLAST";
  attribute X_INTERFACE_INFO of m00_fft_axis_tready : signal is "xilinx.com:interface:axis:1.0 m00_fft_axis TREADY";
  attribute X_INTERFACE_PARAMETER of m00_fft_axis_tready : signal is "XIL_INTERFACENAME m00_fft_axis, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_fft_axis_tvalid : signal is "xilinx.com:interface:axis:1.0 m00_fft_axis TVALID";
  attribute X_INTERFACE_INFO of m01_fft_axis_tlast : signal is "xilinx.com:interface:axis:1.0 m01_fft_axis TLAST";
  attribute X_INTERFACE_INFO of m01_fft_axis_tready : signal is "xilinx.com:interface:axis:1.0 m01_fft_axis TREADY";
  attribute X_INTERFACE_PARAMETER of m01_fft_axis_tready : signal is "XIL_INTERFACENAME m01_fft_axis, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m01_fft_axis_tvalid : signal is "xilinx.com:interface:axis:1.0 m01_fft_axis TVALID";
  attribute X_INTERFACE_INFO of s00_axi_aclk : signal is "xilinx.com:signal:clock:1.0 S00_AXI_CLK CLK";
  attribute X_INTERFACE_PARAMETER of s00_axi_aclk : signal is "XIL_INTERFACENAME S00_AXI_CLK, ASSOCIATED_BUSIF S00_AXI:m00_fft_axis:m01_fft_axis:s00_fft_axis:s01_fft_axis:m00_dma_axis, ASSOCIATED_RESET s00_axi_aresetn, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of s00_axi_aresetn : signal is "xilinx.com:signal:reset:1.0 S00_AXI_RST RST";
  attribute X_INTERFACE_PARAMETER of s00_axi_aresetn : signal is "XIL_INTERFACENAME S00_AXI_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of s00_axi_arready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARREADY";
  attribute X_INTERFACE_INFO of s00_axi_arvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARVALID";
  attribute X_INTERFACE_INFO of s00_axi_awready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWREADY";
  attribute X_INTERFACE_INFO of s00_axi_awvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWVALID";
  attribute X_INTERFACE_INFO of s00_axi_bready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI BREADY";
  attribute X_INTERFACE_INFO of s00_axi_bvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI BVALID";
  attribute X_INTERFACE_INFO of s00_axi_rready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RREADY";
  attribute X_INTERFACE_PARAMETER of s00_axi_rready : signal is "XIL_INTERFACENAME S00_AXI, WIZ_DATA_WIDTH 32, WIZ_NUM_REG 4, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 100000000, ID_WIDTH 0, ADDR_WIDTH 6, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 1, NUM_WRITE_OUTSTANDING 1, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of s00_axi_rvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RVALID";
  attribute X_INTERFACE_INFO of s00_axi_wready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WREADY";
  attribute X_INTERFACE_INFO of s00_axi_wvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WVALID";
  attribute X_INTERFACE_INFO of s00_fft_axis_tlast : signal is "xilinx.com:interface:axis:1.0 s00_fft_axis TLAST";
  attribute X_INTERFACE_INFO of s00_fft_axis_tready : signal is "xilinx.com:interface:axis:1.0 s00_fft_axis TREADY";
  attribute X_INTERFACE_PARAMETER of s00_fft_axis_tready : signal is "XIL_INTERFACENAME s00_fft_axis, TDATA_NUM_BYTES 8, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {TDATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type automatic dependency {} format long minimum {} maximum {}} value 524286} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} array_type {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value chan} size {attribs {resolve_type generated dependency chan_size format long minimum {} maximum {}} value 1} stride {attribs {resolve_type generated dependency chan_stride format long minimum {} maximum {}} value 64} datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type automatic dependency {} format long minimum {} maximum {}} value 524286} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} struct {field_xn_re {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value xn_re} enabled {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value true} datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type automatic dependency {} format long minimum {} maximum {}} value 524254} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} array_type {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} size {attribs {resolve_type generated dependency frame_size format long minimum {} maximum {}} value 8192} stride {attribs {resolve_type generated dependency frame_stride format long minimum {} maximum {}} value 64} datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type generated dependency xn_width format long minimum {} maximum {}} value 30} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} real {fixed {fractwidth {attribs {resolve_type generated dependency xn_fractwidth format long minimum {} maximum {}} value 15} signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value true}}}}}}} field_xn_im {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value xn_im} enabled {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value true} datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type automatic dependency {} format long minimum {} maximum {}} value 524254} bitoffset {attribs {resolve_type generated dependency xn_im_offset format long minimum {} maximum {}} value 32} array_type {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} size {attribs {resolve_type generated dependency frame_size format long minimum {} maximum {}} value 8192} stride {attribs {resolve_type generated dependency frame_stride format long minimum {} maximum {}} value 64} datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type generated dependency xn_width format long minimum {} maximum {}} value 30} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} real {fixed {fractwidth {attribs {resolve_type generated dependency xn_fractwidth format long minimum {} maximum {}} value 15} signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value true}}}}}}}}}}}} TDATA_WIDTH 64 TUSER {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type automatic dependency {} format long minimum {} maximum {}} value 0} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} struct {field_xk_index {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value xk_index} enabled {attribs {resolve_type generated dependency xk_index_enabled format bool minimum {} maximum {}} value false} datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type generated dependency xk_index_width format long minimum {} maximum {}} value 0} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}} field_blk_exp {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value blk_exp} enabled {attribs {resolve_type generated dependency blk_exp_enabled format bool minimum {} maximum {}} value false} datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type automatic dependency {} format long minimum {} maximum {}} value 5} bitoffset {attribs {resolve_type generated dependency blk_exp_offset format long minimum {} maximum {}} value 0} array_type {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} size {attribs {resolve_type generated dependency chan_size format long minimum {} maximum {}} value 1} stride {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 8} datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 5} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}}}} field_ovflo {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value ovflo} enabled {attribs {resolve_type generated dependency ovflo_enabled format bool minimum {} maximum {}} value false} datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type automatic dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type generated dependency ovflo_offset format long minimum {} maximum {}} value 0} array_type {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} size {attribs {resolve_type generated dependency chan_size format long minimum {} maximum {}} value 1} stride {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}}}}}}} TUSER_WIDTH 0}, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of s00_fft_axis_tvalid : signal is "xilinx.com:interface:axis:1.0 s00_fft_axis TVALID";
  attribute X_INTERFACE_INFO of s01_fft_axis_tlast : signal is "xilinx.com:interface:axis:1.0 s01_fft_axis TLAST";
  attribute X_INTERFACE_INFO of s01_fft_axis_tready : signal is "xilinx.com:interface:axis:1.0 s01_fft_axis TREADY";
  attribute X_INTERFACE_PARAMETER of s01_fft_axis_tready : signal is "XIL_INTERFACENAME s01_fft_axis, TDATA_NUM_BYTES 8, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {TDATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type automatic dependency {} format long minimum {} maximum {}} value 524286} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} array_type {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value chan} size {attribs {resolve_type generated dependency chan_size format long minimum {} maximum {}} value 1} stride {attribs {resolve_type generated dependency chan_stride format long minimum {} maximum {}} value 64} datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type automatic dependency {} format long minimum {} maximum {}} value 524286} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} struct {field_xn_re {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value xn_re} enabled {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value true} datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type automatic dependency {} format long minimum {} maximum {}} value 524254} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} array_type {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} size {attribs {resolve_type generated dependency frame_size format long minimum {} maximum {}} value 8192} stride {attribs {resolve_type generated dependency frame_stride format long minimum {} maximum {}} value 64} datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type generated dependency xn_width format long minimum {} maximum {}} value 30} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} real {fixed {fractwidth {attribs {resolve_type generated dependency xn_fractwidth format long minimum {} maximum {}} value 15} signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value true}}}}}}} field_xn_im {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value xn_im} enabled {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value true} datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type automatic dependency {} format long minimum {} maximum {}} value 524254} bitoffset {attribs {resolve_type generated dependency xn_im_offset format long minimum {} maximum {}} value 32} array_type {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} size {attribs {resolve_type generated dependency frame_size format long minimum {} maximum {}} value 8192} stride {attribs {resolve_type generated dependency frame_stride format long minimum {} maximum {}} value 64} datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type generated dependency xn_width format long minimum {} maximum {}} value 30} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} real {fixed {fractwidth {attribs {resolve_type generated dependency xn_fractwidth format long minimum {} maximum {}} value 15} signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value true}}}}}}}}}}}} TDATA_WIDTH 64 TUSER {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type automatic dependency {} format long minimum {} maximum {}} value 0} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} struct {field_xk_index {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value xk_index} enabled {attribs {resolve_type generated dependency xk_index_enabled format bool minimum {} maximum {}} value false} datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type generated dependency xk_index_width format long minimum {} maximum {}} value 0} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}} field_blk_exp {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value blk_exp} enabled {attribs {resolve_type generated dependency blk_exp_enabled format bool minimum {} maximum {}} value false} datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type automatic dependency {} format long minimum {} maximum {}} value 5} bitoffset {attribs {resolve_type generated dependency blk_exp_offset format long minimum {} maximum {}} value 0} array_type {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} size {attribs {resolve_type generated dependency chan_size format long minimum {} maximum {}} value 1} stride {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 8} datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 5} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}}}} field_ovflo {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value ovflo} enabled {attribs {resolve_type generated dependency ovflo_enabled format bool minimum {} maximum {}} value false} datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type automatic dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type generated dependency ovflo_offset format long minimum {} maximum {}} value 0} array_type {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} size {attribs {resolve_type generated dependency chan_size format long minimum {} maximum {}} value 1} stride {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value {}} bitwidth {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 1} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}}}}}}} TUSER_WIDTH 0}, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of s01_fft_axis_tvalid : signal is "xilinx.com:interface:axis:1.0 s01_fft_axis TVALID";
  attribute X_INTERFACE_INFO of m00_dma_axis_tdata : signal is "xilinx.com:interface:axis:1.0 m00_dma_axis TDATA";
  attribute X_INTERFACE_INFO of m00_dma_axis_tstrb : signal is "xilinx.com:interface:axis:1.0 m00_dma_axis TSTRB";
  attribute X_INTERFACE_INFO of m00_fft_axis_tdata : signal is "xilinx.com:interface:axis:1.0 m00_fft_axis TDATA";
  attribute X_INTERFACE_INFO of m00_fft_axis_tstrb : signal is "xilinx.com:interface:axis:1.0 m00_fft_axis TSTRB";
  attribute X_INTERFACE_INFO of m01_fft_axis_tdata : signal is "xilinx.com:interface:axis:1.0 m01_fft_axis TDATA";
  attribute X_INTERFACE_INFO of m01_fft_axis_tstrb : signal is "xilinx.com:interface:axis:1.0 m01_fft_axis TSTRB";
  attribute X_INTERFACE_INFO of s00_axi_araddr : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARADDR";
  attribute X_INTERFACE_INFO of s00_axi_arprot : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARPROT";
  attribute X_INTERFACE_INFO of s00_axi_awaddr : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWADDR";
  attribute X_INTERFACE_INFO of s00_axi_awprot : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWPROT";
  attribute X_INTERFACE_INFO of s00_axi_bresp : signal is "xilinx.com:interface:aximm:1.0 S00_AXI BRESP";
  attribute X_INTERFACE_INFO of s00_axi_rdata : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RDATA";
  attribute X_INTERFACE_INFO of s00_axi_rresp : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RRESP";
  attribute X_INTERFACE_INFO of s00_axi_wdata : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WDATA";
  attribute X_INTERFACE_INFO of s00_axi_wstrb : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WSTRB";
  attribute X_INTERFACE_INFO of s00_fft_axis_tdata : signal is "xilinx.com:interface:axis:1.0 s00_fft_axis TDATA";
  attribute X_INTERFACE_INFO of s00_fft_axis_tstrb : signal is "xilinx.com:interface:axis:1.0 s00_fft_axis TSTRB";
  attribute X_INTERFACE_INFO of s01_fft_axis_tdata : signal is "xilinx.com:interface:axis:1.0 s01_fft_axis TDATA";
  attribute X_INTERFACE_INFO of s01_fft_axis_tstrb : signal is "xilinx.com:interface:axis:1.0 s01_fft_axis TSTRB";
begin
  SYNC <= \<const1>\;
  m00_dma_axis_tstrb(7) <= \<const1>\;
  m00_dma_axis_tstrb(6) <= \<const1>\;
  m00_dma_axis_tstrb(5) <= \<const1>\;
  m00_dma_axis_tstrb(4) <= \<const1>\;
  m00_dma_axis_tstrb(3) <= \<const1>\;
  m00_dma_axis_tstrb(2) <= \<const1>\;
  m00_dma_axis_tstrb(1) <= \<const1>\;
  m00_dma_axis_tstrb(0) <= \<const1>\;
  m00_fft_axis_tstrb(3) <= \<const1>\;
  m00_fft_axis_tstrb(2) <= \<const1>\;
  m00_fft_axis_tstrb(1) <= \<const1>\;
  m00_fft_axis_tstrb(0) <= \<const1>\;
  m01_fft_axis_tstrb(3) <= \<const1>\;
  m01_fft_axis_tstrb(2) <= \<const1>\;
  m01_fft_axis_tstrb(1) <= \<const1>\;
  m01_fft_axis_tstrb(0) <= \<const1>\;
  s00_axi_bresp(1) <= \<const0>\;
  s00_axi_bresp(0) <= \<const0>\;
  s00_axi_rresp(1) <= \<const0>\;
  s00_axi_rresp(0) <= \<const0>\;
  s00_fft_axis_tready <= \<const1>\;
  s01_fft_axis_tready <= \<const1>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
VCC: unisim.vcomponents.VCC
     port map (
      P => \<const1>\
    );
i_263: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => s00_axi_aresetn,
      O => n_0_263
    );
inst: entity work.design_1_AD9650_0_0_AD9650_v3_0
     port map (
      ADC_PDwN => ADC_PDwN,
      DATA_INA(15 downto 0) => DATA_INA(15 downto 0),
      DATA_INB(15 downto 0) => DATA_INB(15 downto 0),
      adc_spi_cs => adc_spi_cs,
      adc_spi_sck => adc_spi_sck,
      adc_spi_sdio => adc_spi_sdio,
      allowed_clk => allowed_clk,
      allowed_read_out => allowed_read_out,
      azimut_0 => azimut_0,
      clk_10MHz => clk_10MHz,
      m00_dma_axis_tdata(63 downto 0) => m00_dma_axis_tdata(63 downto 0),
      m00_dma_axis_tlast => m00_dma_axis_tlast,
      m00_dma_axis_tvalid => m00_dma_axis_tvalid,
      m00_fft_axis_tdata(31 downto 0) => m00_fft_axis_tdata(31 downto 0),
      m00_fft_axis_tlast => m00_fft_axis_tlast,
      m00_fft_axis_tvalid => m00_fft_axis_tvalid,
      m01_fft_axis_tdata(31 downto 0) => m01_fft_axis_tdata(31 downto 0),
      m01_fft_axis_tlast => m01_fft_axis_tlast,
      m01_fft_axis_tvalid => m01_fft_axis_tvalid,
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_araddr(2 downto 0) => s00_axi_araddr(4 downto 2),
      s00_axi_aresetn => s00_axi_aresetn,
      s00_axi_arready => s00_axi_arready,
      s00_axi_arvalid => s00_axi_arvalid,
      s00_axi_awaddr(2 downto 0) => s00_axi_awaddr(4 downto 2),
      s00_axi_awready => s00_axi_awready,
      s00_axi_awvalid => s00_axi_awvalid,
      s00_axi_bready => s00_axi_bready,
      s00_axi_bvalid => s00_axi_bvalid,
      s00_axi_rdata(31 downto 0) => s00_axi_rdata(31 downto 0),
      s00_axi_rready => s00_axi_rready,
      s00_axi_rvalid => s00_axi_rvalid,
      s00_axi_wdata(31 downto 0) => s00_axi_wdata(31 downto 0),
      s00_axi_wready => s00_axi_wready,
      s00_axi_wstrb(3 downto 0) => s00_axi_wstrb(3 downto 0),
      s00_axi_wvalid => s00_axi_wvalid,
      s00_fft_axis_tdata(63 downto 0) => s00_fft_axis_tdata(63 downto 0),
      s00_fft_axis_tlast => s00_fft_axis_tlast,
      s00_fft_axis_tvalid => s00_fft_axis_tvalid,
      s01_fft_axis_tdata(63 downto 0) => s01_fft_axis_tdata(63 downto 0),
      s01_fft_axis_tlast => s01_fft_axis_tlast,
      s01_fft_axis_tvalid => s01_fft_axis_tvalid
    );
end STRUCTURE;
