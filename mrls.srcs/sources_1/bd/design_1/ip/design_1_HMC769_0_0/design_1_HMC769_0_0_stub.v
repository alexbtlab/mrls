// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
// Date        : Wed May 11 16:10:35 2022
// Host        : alexbtlab-System-Product-Name running 64-bit Ubuntu 20.04.3 LTS
// Command     : write_verilog -force -mode synth_stub
//               /home/alexbtlab/source/mrls/mrls.srcs/sources_1/bd/design_1/ip/design_1_HMC769_0_0/design_1_HMC769_0_0_stub.v
// Design      : design_1_HMC769_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z020clg400-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "HMC769_v5_0,Vivado 2019.1" *)
module design_1_HMC769_0_0(clk_10MHz, clk_SPI_PLL, clkDCO_10MHz, 
  s00_axi_awaddr, s00_axi_awprot, s00_axi_awvalid, s00_axi_awready, s00_axi_wdata, 
  s00_axi_wstrb, s00_axi_wvalid, s00_axi_wready, s00_axi_bresp, s00_axi_bvalid, 
  s00_axi_bready, s00_axi_araddr, s00_axi_arprot, s00_axi_arvalid, s00_axi_arready, 
  s00_axi_rdata, s00_axi_rresp, s00_axi_rvalid, s00_axi_rready, azimut_0, spi_pll_sen, 
  spi_pll_sck, spi_pll_mosi, spi_pll_cen, spi_pll_ld_sdo, pll_trig, ATTEN, PLL_POW_EN, 
  LOW_AMP_EN, PAMP_EN, start_adc_count, PAMP_ALM_FPGA, s00_axi_aclk, s00_axi_aresetn)
/* synthesis syn_black_box black_box_pad_pin="clk_10MHz,clk_SPI_PLL,clkDCO_10MHz,s00_axi_awaddr[5:0],s00_axi_awprot[2:0],s00_axi_awvalid,s00_axi_awready,s00_axi_wdata[31:0],s00_axi_wstrb[3:0],s00_axi_wvalid,s00_axi_wready,s00_axi_bresp[1:0],s00_axi_bvalid,s00_axi_bready,s00_axi_araddr[5:0],s00_axi_arprot[2:0],s00_axi_arvalid,s00_axi_arready,s00_axi_rdata[31:0],s00_axi_rresp[1:0],s00_axi_rvalid,s00_axi_rready,azimut_0,spi_pll_sen,spi_pll_sck,spi_pll_mosi,spi_pll_cen,spi_pll_ld_sdo,pll_trig,ATTEN[5:0],PLL_POW_EN,LOW_AMP_EN,PAMP_EN,start_adc_count,PAMP_ALM_FPGA,s00_axi_aclk,s00_axi_aresetn" */;
  input clk_10MHz;
  input clk_SPI_PLL;
  input clkDCO_10MHz;
  input [5:0]s00_axi_awaddr;
  input [2:0]s00_axi_awprot;
  input s00_axi_awvalid;
  output s00_axi_awready;
  input [31:0]s00_axi_wdata;
  input [3:0]s00_axi_wstrb;
  input s00_axi_wvalid;
  output s00_axi_wready;
  output [1:0]s00_axi_bresp;
  output s00_axi_bvalid;
  input s00_axi_bready;
  input [5:0]s00_axi_araddr;
  input [2:0]s00_axi_arprot;
  input s00_axi_arvalid;
  output s00_axi_arready;
  output [31:0]s00_axi_rdata;
  output [1:0]s00_axi_rresp;
  output s00_axi_rvalid;
  input s00_axi_rready;
  input azimut_0;
  output spi_pll_sen;
  output spi_pll_sck;
  output spi_pll_mosi;
  output spi_pll_cen;
  input spi_pll_ld_sdo;
  output pll_trig;
  output [5:0]ATTEN;
  output PLL_POW_EN;
  output LOW_AMP_EN;
  output PAMP_EN;
  output start_adc_count;
  input PAMP_ALM_FPGA;
  input s00_axi_aclk;
  input s00_axi_aresetn;
endmodule
