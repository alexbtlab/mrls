# aclk {FREQ_HZ 100000000 CLK_DOMAIN design_2_m00_axis_aclk PHASE 0.000}
# Clock Domain: design_2_m00_axis_aclk
create_clock -name aclk -period 10.000 [get_ports aclk]
# Generated clocks
