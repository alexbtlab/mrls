//Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
//Date        : Fri Jun  3 11:50:32 2022
//Host        : alexbtlab-System-Product-Name running 64-bit Ubuntu 20.04.3 LTS
//Command     : generate_target design_2_wrapper.bd
//Design      : design_2_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module design_2_wrapper
   (azimut_0,
    clk_10MHz,
    fifo_in_tdata,
    m00_axis_aclk,
    s00_axis_aresetn);
  input azimut_0;
  input clk_10MHz;
  input [31:0]fifo_in_tdata;
  input m00_axis_aclk;
  input s00_axis_aresetn;

  wire azimut_0;
  wire clk_10MHz;
  wire [31:0]fifo_in_tdata;
  wire m00_axis_aclk;
  wire s00_axis_aresetn;

  design_2 design_2_i
       (.azimut_0(azimut_0),
        .clk_10MHz(clk_10MHz),
        .fifo_in_tdata(fifo_in_tdata),
        .m00_axis_aclk(m00_axis_aclk),
        .s00_axis_aresetn(s00_axis_aresetn));
endmodule
