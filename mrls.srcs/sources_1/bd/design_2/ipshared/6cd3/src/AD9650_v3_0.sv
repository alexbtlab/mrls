`timescale 1 ns / 1 ps
`define AVERAG_VAL_DEFAULT 8
`define AMOUNT_OF_AZIMUTH_DEFAULT 284

interface adc_spi;
    logic sck, cs, sdio;      // Indicates if slave is ready to accept data
    modport master  (output sck, output cs, inout sdio);
endinterface

	module AD9650_v3_0
	#
	(
		// Parameters of Axi Slave Bus Interface S00_AXI
		
		parameter integer C_S00_AXI_FFT_DATA_WIDTH	= 64,
		parameter integer C_S00_AXI_DATA_WIDTH	= 32,
		parameter integer C_S00_AXI_ADDR_WIDTH	= 6
	)
	(	         
	    input wire  s00_axi_aclk,
		input wire  s00_axi_aresetn,  
		// Ports of Axi Slave Bus Interface S00_AXI	
		input wire clk_10MHz,
		input wire [C_S00_AXI_ADDR_WIDTH-1 : 0] s00_axi_awaddr,
		input wire [2 : 0] s00_axi_awprot,
		input wire  s00_axi_awvalid,
		output wire  s00_axi_awready,
		input wire [C_S00_AXI_DATA_WIDTH-1 : 0] s00_axi_wdata,
		input wire [(C_S00_AXI_DATA_WIDTH/8)-1 : 0] s00_axi_wstrb,
		input wire  s00_axi_wvalid,
		output wire  s00_axi_wready,
		output wire [1 : 0] s00_axi_bresp,
		output wire  s00_axi_bvalid,
		input wire  s00_axi_bready,
		input wire [C_S00_AXI_ADDR_WIDTH-1 : 0] s00_axi_araddr,
		input wire [2 : 0] s00_axi_arprot,
		input wire  s00_axi_arvalid,
		output wire  s00_axi_arready,
		output wire [C_S00_AXI_DATA_WIDTH-1 : 0] s00_axi_rdata,
		output wire [1 : 0] s00_axi_rresp,
		output wire  s00_axi_rvalid,
		input wire  s00_axi_rready,
		/*-----------------------------------------------------------------------*/
		output wire  m00_fft_axis_tvalid,
		output wire [C_S00_AXI_DATA_WIDTH-1 : 0] m00_fft_axis_tdata,
		output wire [(C_S00_AXI_DATA_WIDTH/8)-1 : 0] m00_fft_axis_tstrb,
		output wire  m00_fft_axis_tlast,
		input wire  m00_fft_axis_tready,
				
		output wire  m01_fft_axis_tvalid,
		output wire [C_S00_AXI_DATA_WIDTH-1 : 0] m01_fft_axis_tdata,
		output wire [(C_S00_AXI_DATA_WIDTH/8)-1 : 0] m01_fft_axis_tstrb,
		output wire  m01_fft_axis_tlast,
		input wire  m01_fft_axis_tready,
		/*-----------------------------------------------------------------------*/
		input wire  s00_fft_axis_tvalid,
		input wire [C_S00_AXI_FFT_DATA_WIDTH-1 : 0] s00_fft_axis_tdata,
		input wire [(C_S00_AXI_FFT_DATA_WIDTH/8)-1 : 0] s00_fft_axis_tstrb,
		input wire  s00_fft_axis_tlast,
		output wire  s00_fft_axis_tready,
		
		input wire  s01_fft_axis_tvalid,
		input wire [C_S00_AXI_FFT_DATA_WIDTH-1 : 0] s01_fft_axis_tdata,
		input wire [(C_S00_AXI_FFT_DATA_WIDTH/8)-1 : 0] s01_fft_axis_tstrb,
		input wire  s01_fft_axis_tlast,
		output wire  s01_fft_axis_tready,
		/*-----------------------------------------------------------------------*/
	    output wire  m00_dma_axis_tvalid,
		output wire [C_S00_AXI_FFT_DATA_WIDTH-1 : 0] m00_dma_axis_tdata,
		output wire [(C_S00_AXI_FFT_DATA_WIDTH/8)-1 : 0] m00_dma_axis_tstrb,
		output wire  m00_dma_axis_tlast,
		input wire  m00_dma_axis_tready,
		               
//		adc_spi.master ADC_SPI,
        
        output wire ADC_PDwN,  // Power-Down Input in External Pin Mode.
        output wire SYNC,      // Digital Synchronization Pin
                
        input wire [15:0] DATA_INA,
        input wire [15:0] DATA_INB, 
        input wire  allowed_clk,
        input wire [15:0] azimut8,
        input wire  azimut_0
    );
    
    assign m00_dma_axis_tstrb = 8'hFF;
    wire START;
    wire [12:0] ADR;
    wire [7:0] DATA_TX;
    wire [7:0] DATA_RX;
    wire [7:0] DATA_RX2;
    wire RW;
    wire data_rx_ready;
    wire clk_100MHz;
    assign s00_fft_axis_tready = 1;
    assign s01_fft_axis_tready = 1;
    wire [15:0] FrameSize2;
    localparam  FrameSize = 8192;
    assign m00_fft_axis_tstrb = 4'hF;  
    assign m01_fft_axis_tstrb = 4'hF;  
    wire [32-1:0]	ip2mb_reg0;
    wire [32-1:0]	ip2mb_reg1;
    wire [32-1:0]	ip2mb_reg2;
    wire [32-1:0]	ip2mb_reg3;
    wire [32-1:0]	ip2mb_reg4;
    wire [32-1:0]	ip2mb_reg5;
    wire [32-1:0]	ip2mb_reg6;
    wire [32-1:0]	ip2mb_reg7;     
    wire [7:0] reg_address;
    wire [7:0] write_data;
    reg [31:0] slv_reg3;
    reg [31:0] slv_reg4;
    reg [31:0] slv_reg5;
    reg [31:0] slv_reg6;
    reg [31:0] slv_reg7;
    wire SYNC_signal;
    wire [31:0] max_azimut_read_inner;

    assign clk_100MHz = s00_axi_aclk;
    assign  ip2mb_reg0[0]=  data_rx_ready;
    assign  ip2mb_reg1 = DATA_RX;  
  
    reg [15:0] cnt_in_DCO;
    reg [15:0] cnt_DCO;
    
    reg m00_fft_axis_tvalid_r;
    reg m01_fft_axis_tvalid_r;
    assign m00_fft_axis_tvalid = m00_fft_axis_tvalid_r;
    assign m01_fft_axis_tvalid = m01_fft_axis_tvalid_r;
    
    reg m00_fft_axis_tlast_r;
    reg m01_fft_axis_tlast_r;
    assign m00_fft_axis_tlast = m00_fft_axis_tlast_r;
    assign m01_fft_axis_tlast = m01_fft_axis_tlast_r;
  
    wire mux_fft_in;
    wire mux_fft_out;
    
    assign m00_fft_axis_tdata = (mux_fft_in)  ?  {DATA_INB, DATA_INA} : 0;
    assign m01_fft_axis_tdata = (~mux_fft_in) ?  {DATA_INB, DATA_INA} : 0;
    
    wire [31:0] cntDCO_setVal;
    wire [15:0] AVERAG_VAL;
    wire [15:0] amountOfAzimuthFromCpu;
     
    spi_AD9650 spi_AD9650_inst(
        . START(START),
        . RW(RW),
        . ADR(ADR),
        . DATA_TX(DATA_TX),
        . DATA_RX(DATA_RX),
        . DATA_RX2(DATA_RX2),
        . CLK_10(clk_10MHz),
        . CLK_100(clk_100MHz),
//        . SDIO(ADC_SPI.sdio), 
//        . SCK(ADC_SPI.sck),
//        . CS(ADC_SPI.cs),
        . data_ready(data_rx_ready)
   ); 
        AD9650_v3_0_S00_AXI # ( 
		.C_S_AXI_DATA_WIDTH(C_S00_AXI_DATA_WIDTH),
		.C_S_AXI_ADDR_WIDTH(C_S00_AXI_ADDR_WIDTH)
	) AD9650_v3_0_S00_AXI_inst (
		.S_AXI_ACLK(s00_axi_aclk),
		.S_AXI_ARESETN(s00_axi_aresetn),
		.S_AXI_AWADDR(s00_axi_awaddr),
		.S_AXI_AWPROT(s00_axi_awprot),
		.S_AXI_AWVALID(s00_axi_awvalid),
		.S_AXI_AWREADY(s00_axi_awready),
		.S_AXI_WDATA(s00_axi_wdata),
		.S_AXI_WSTRB(s00_axi_wstrb),
		.S_AXI_WVALID(s00_axi_wvalid),
		.S_AXI_WREADY(s00_axi_wready),
		.S_AXI_BRESP(s00_axi_bresp),
		.S_AXI_BVALID(s00_axi_bvalid),
		.S_AXI_BREADY(s00_axi_bready),
		.S_AXI_ARADDR(s00_axi_araddr),
		.S_AXI_ARPROT(s00_axi_arprot),
		.S_AXI_ARVALID(s00_axi_arvalid),
		.S_AXI_ARREADY(s00_axi_arready),
		.S_AXI_RDATA(s00_axi_rdata),
		.S_AXI_RRESP(s00_axi_rresp),
		.S_AXI_RVALID(s00_axi_rvalid),
		.S_AXI_RREADY(s00_axi_rready),
		
	    .slv_reg0({ DATA_TX, ADR}),       // 0x0
		.slv_reg1({ START }),             // 0x4
		.slv_reg2({ ADC_PDwN, RW }),      // 0x8
		.slv_reg3({ AVERAG_VAL, amountOfAzimuthFromCpu  }),          // 12

		.ip2mb_reg0(ip2mb_reg0),         //16
		.ip2mb_reg1(ip2mb_reg1),         //20
		.ip2mb_reg2(ip2mb_reg2),         //24
		.ip2mb_reg3(ip2mb_reg3)          //28
	);
   
assign m00_dma_axis_tvalid = ( mux_fft_out ) ? s00_fft_axis_tvalid : s01_fft_axis_tvalid;
assign m00_dma_axis_tlast  = ( mux_fft_out ) ? s00_fft_axis_tlast  : s01_fft_axis_tlast;
assign m00_dma_axis_tdata  = ( mux_fft_out ) ? s00_fft_axis_tdata  : s01_fft_axis_tdata;

reg [15:0] cnt_allowed_clk;
reg [15:0] cnt_not_allowed_clk;
reg [3:0] cnt_high_frame;
reg [7:0] cnt_low_frame;
reg [15:0] cnt_low_frame8;
 
assign mux_fft_out = (azimut8[0] == 0)        ?  cnt_high_frame[0] : cnt_high_frame[0] ;
assign mux_fft_in =  (cnt_low_frame8[0] == 0) ?  cnt_low_frame[0]  : ~cnt_low_frame[0];

reg azimut_0_prev;
reg enable_module;
reg enable_cnt_low8_frame;
reg [15:0] amountOfAzimuthFromCpu_r;
reg [15:0] averagVal_r;


    always @ (posedge clk_10MHz) begin
        
        if ( s00_axi_aresetn) begin
        
                    if (  allowed_clk )                  cnt_allowed_clk     <= cnt_allowed_clk + 1;
                    else                                 cnt_allowed_clk     <= 0;
                    
                    if ( !allowed_clk )                  cnt_not_allowed_clk <= cnt_not_allowed_clk + 1;
                    else                                 cnt_not_allowed_clk <= 0;
                    
                    if ( cnt_allowed_clk     == 2000 )     cnt_high_frame <= cnt_high_frame + 1;
                    
                    if ( cnt_not_allowed_clk == 50 & enable_cnt_low8_frame )       begin
                        cnt_low_frame  <= cnt_low_frame + 1;
                         if ( cnt_low_frame == averagVal_r & !azimut_0)  begin
                                cnt_low_frame <= 0;
                                cnt_low_frame8 <= cnt_low_frame8 + 1;
                        end
                    end
                                        
                    if ( azimut_0 ) begin
                        averagVal_r                 <= AVERAG_VAL;
                        amountOfAzimuthFromCpu_r    <= amountOfAzimuthFromCpu;
                        enable_cnt_low8_frame       <= 1; 
                        enable_module               <= 1; 
                        cnt_low_frame               <= 8'hFF;
                        cnt_low_frame8              <= 0;
                        cnt_high_frame              <= 0;
                        cnt_allowed_clk             <= 0;
                        cnt_not_allowed_clk         <= 0;
                    end

                  
            
        end
        else begin
            averagVal_r                 <= `AVERAG_VAL_DEFAULT;
            amountOfAzimuthFromCpu_r    <= `AMOUNT_OF_AZIMUTH_DEFAULT;
            enable_cnt_low8_frame       <= 0;
            enable_module               <= 0;
            cnt_high_frame              <= 0;
            cnt_low_frame               <= 8'hFF;
            cnt_allowed_clk             <= 0;
            cnt_not_allowed_clk         <= 0;
            cnt_low_frame8              <= 0;
        end     
    end
    
   reg SYNC_reg;
   assign SYNC = SYNC_reg;
   
   reg [15:0] cnt_10MHz;
   
   always @ (posedge clk_10MHz) begin
        if ( s00_axi_aresetn ) begin
           if ( cnt_10MHz == 50000)   cnt_10MHz <= 0;
           else                       cnt_10MHz <= cnt_10MHz + 1;
           
           if ( cnt_10MHz == 1000)   SYNC_reg <= 1;
           if ( cnt_10MHz == 1100)   SYNC_reg <= 0;
        end 
        else begin
            cnt_10MHz <= 0;
        end
   
        
   end 

  always @ (posedge s00_axi_aclk) begin
    if ( s00_axi_aresetn ) begin
  
                    if( clk_10MHz & allowed_clk  )              cnt_in_DCO <= cnt_in_DCO + 1;
                    else                                        cnt_in_DCO <= 0;
            
                                if(mux_fft_in) begin
                                        m01_fft_axis_tlast_r  <= 0;
                                        m01_fft_axis_tvalid_r <= 0; 
                                        if(cnt_in_DCO == 1 & cnt_DCO > 0 & cnt_DCO <= FrameSize)        m00_fft_axis_tvalid_r <= 1;
                                        else                                                            m00_fft_axis_tvalid_r <= 0;
                                        if(cnt_in_DCO == 1 & cnt_DCO == FrameSize)                      m00_fft_axis_tlast_r  <= 1;
                                        else                                                            m00_fft_axis_tlast_r  <= 0;    
                                end
                                else begin
                                        m00_fft_axis_tlast_r  <= 0;
                                        m00_fft_axis_tvalid_r <= 0; 
                                        if(cnt_in_DCO == 1 & cnt_DCO > 0 & cnt_DCO <= FrameSize)        m01_fft_axis_tvalid_r <= 1;
                                        else                                                            m01_fft_axis_tvalid_r <= 0;
                                        if(cnt_in_DCO == 1 & cnt_DCO == FrameSize)                      m01_fft_axis_tlast_r  <= 1;
                                        else                                                            m01_fft_axis_tlast_r  <= 0;    
                                end              
    end
    else begin
        m00_fft_axis_tvalid_r <= 0;
        m00_fft_axis_tlast_r  <= 0;
        m01_fft_axis_tvalid_r <= 0;
        m01_fft_axis_tlast_r  <= 0;    
    end                
  end
  
  wire   allowed_read;

  assign allowed_read = allowed_clk   & 
                        enable_module &
                        ( cnt_low_frame8 < amountOfAzimuthFromCpu_r  ) ;
   
  always @ (posedge clk_10MHz) begin
     if( allowed_read ) begin  
        if(cnt_DCO != FrameSize + 1)      cnt_DCO <= cnt_DCO + 1;
        else                              cnt_DCO <= FrameSize + 1;
     end
     else begin
        cnt_DCO <= 0;
     end
  end
  
endmodule
