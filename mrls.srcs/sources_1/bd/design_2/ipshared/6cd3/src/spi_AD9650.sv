`define WRITE_STATE         0
`define READ_STATE          1
`define W0                  0
`define W1                  0
`define SIZE_TRANSACTION    24
`define ADDRESS_SECTION     cnt >= 4  & cnt <= 16
`define DATA_WRITE_SECTION  cnt >= 21 & cnt <= 28
`define DATA_READ_SECTION   cnt_re >= 22 & cnt_re <= 29

`define ALLOW_SCK_RANGE     (cnt > 0 & cnt <= 16) | (cnt > 20 & cnt <= 28)

`define POS_STATE           cnt == 1
`define POS_W0              cnt == 2
`define POS_W1              cnt == 3
`define POS_CS_SET          cnt == 34
`define POS_MAX_CNT         cnt == 80
`define POS_MAX_CNT_RE      cnt_re == 80
`define POS_READY_RX        cnt_re == 30
`define POS_SWICH_IO        cnt_re == 19

`define ENABLE_COUNT        enable_cnt & cnt != 80   
`define ENABLE_COUNT_RE     enable_cnt_re & cnt_re != 80 

`define STATE_WAIT_START_SIGNAL_FROM_APP       start_sync == 1 & start_prev == 0
`define STATE_WAIT_START_SIGNAL_FROM_APP_FE    start_sync == 1 & start_prev_fe == 0
 /* ------- - ------- - ------- - ------- - ------- - ------- - ------- - ------- - ------- - ------- - ------- - ------- -  

    *** ***  WRITE TRANSACTION  RW = 0  SDIO => "out" all time
    CS    --__________________________________________________________________________________________________----
    SCLK    _|`|__|`|__|`|__|`|____________|`|________  ...  _____|`|__|`|__|`|__|`|__|`|__|`|__|`|__|`|___________
             RW---W0---w1---ADR[12]  ...  ADR[0]                   D[7]             ...              D[0]
    SDIO    --x---x----x----x--      ...    x                    --x--              ...            --x--
            \________________________________/                   \______________________________________/
                    SDIO => out                                           SDIO => out
            
    *** *** READ TRANSACTION    RW = 1  SDIO => first sck 16 clock "out" after "in" for read state 
    CS    --__________________________________________________________________________________________________----
    SCLK    _|`|__|`|__|`|__|`|____________|`|________  ...  _____|`|__|`|__|`|__|`|__|`|__|`|__|`|__|`|___________
             RW---W0---w1---ADR[12]  ...  ADR[0]                   D[7]             ...              D[0]
    SDIO    --x---x----x----x--      ...    x                    --x--              ...            --x--
           \________________________________/                   \______________________________________/
                    SDIO => out                                           SDIO => in                                         
 ------- - ------- - ------- - ------- - ------- - ------- - ------- - ------- - ------- - ------- - ------- - ------- -  */  

module spi_AD9650(
    input START,
    input RW,
    input [12:0] ADR,
    input [7:0] DATA_TX,
    output [7:0] DATA_RX,
    output [7:0] DATA_RX2,
    input CLK_10,
    input CLK_100, 
    output data_ready,
    inout wire SDIO,
    output CS,
    output SCK
);

   wire rx_wire;
   reg tx_reg;
   reg DATA_TX_serial_r;
   reg  DATA_RX_serial_r;
   wire DATA_RX_serial;         assign DATA_RX_serial = DATA_RX_serial_r;
   wire DATA_TX_serial;         assign DATA_TX_serial = DATA_TX_serial_r;
   
   /*----  BI-DIRECTIONAL INTERFACE  ------------------*/
   always @(posedge CLK_100) begin
        tx_reg <= DATA_TX_serial;
        DATA_RX_serial_r <= rx_wire;
   end
   
   reg tristate;
   wire tx_wire;
   assign tx_wire = tx_reg;
   
   OBUFT #(
      .DRIVE(12),               // Specify the output drive strength
      .IOSTANDARD("DEFAULT"),   // Specify the output I/O standard
      .SLEW("SLOW")             // Specify the output slew rate
   ) OBUFT_inst (
      .O(SDIO),                 // Buffer output (connect directly to top-level port)
      .I(tx_wire),              // Buffer input
      .T(tristate)            // 3-state enable input
   );
   IBUF #(
      .IBUF_LOW_PWR("TRUE"),    // Low power (TRUE) vs. performance (FALSE) setting for referenced I/O standards
      .IOSTANDARD("DEFAULT")    // Specify the input I/O standard
   ) IBUF_inst (
      .O(rx_wire),              // Buffer output
      .I(SDIO)                  // Buffer input (connect directly to top-level port)
   );  
   /*-------------------------------------------------------------------------*/ 
    reg enable_sck;                 reg start_sync;
    reg [7:0] cnt = 0;              reg [7:0] DATA_RX_r;
    reg start_prev = 0;             assign DATA_RX = DATA_RX_r; 
    reg enable_cnt = 0;             assign SCK =  CLK_10 &  enable_sck;
    reg cs_r = 1;                   assign CS = cs_r;
    
    reg [7:0] DATA_RX_r2;
    assign DATA_RX2 = DATA_RX_r2;
    
    always @ (negedge CLK_10) begin
        start_sync <= START;    /*-- ��� ������ ����������� ������� ����� ������  ������� cnt � cnt_re*/ 
    end
       
    always @ (negedge CLK_10) begin
        /*--- ��������� ���� ������\�������� SPI ---*/ 
        if( `POS_STATE )            if(RW)      DATA_TX_serial_r <= `READ_STATE;  
                                    else        DATA_TX_serial_r <= `WRITE_STATE;  
        if( `POS_W0 )                           DATA_TX_serial_r <= `W0;           
        if( `POS_W1 )                           DATA_TX_serial_r <= `W1;           
        if( `ADDRESS_SECTION )                  DATA_TX_serial_r <= ADR    [16 - cnt];
        if(RW == 0) if( `DATA_WRITE_SECTION )   DATA_TX_serial_r <= DATA_TX[28 - cnt];
        /*--- ������� ���������� ������ ������������� �������� ����� ������ ������� START---*/
        if(  `ENABLE_COUNT     )                cnt <= cnt + 1;
        else                                    cnt <= 0;
        /*--- ���� ������� ��������� � ����������� ������������ � �������� ALLOW_SCK_RANGE -> ��������� ����������� ������������� ��� SPI ---*/
        if(  `ALLOW_SCK_RANGE  )                enable_sck <= 1;
        else                                    enable_sck <= 0;   
        /*--- ���� ������� ��������� � �����������: POS_CS_SET -> ����� ����������. ��������� Slave ---*/
        if(  `POS_CS_SET       )                cs_r <= 1;
        /*--- ���� ������� ��������� � �����������: POS_MAX_CNT -> ������������� ���������� � ���������: "�������� ������� START" ---*/
        if(  `POS_MAX_CNT      ) begin
            enable_cnt <= 0;
            start_prev <= 0;
        end          
        /*--- ���� ��������� � ��������� "�������� ������� START" ---*/
        if(`STATE_WAIT_START_SIGNAL_FROM_APP) begin                       
            start_prev <= 1;
            enable_cnt <= 1;
            cs_r <= 0;
        end   
    end
    
    /*-------------------------------------------------------------------------*/    
                reg [15:0] cnt_re = 0;
                reg enable_cnt_re = 0;
                reg start_prev_fe = 0;
                reg data_rx_ready = 0;
                assign data_ready = data_rx_ready;
                
                always @ (posedge CLK_10) begin
                    if(RW == 1) begin 
                        if(`STATE_WAIT_START_SIGNAL_FROM_APP_FE) begin  
                            tristate <= 0;                       
                            start_prev_fe <= 1;
                            enable_cnt_re <= 1;
                            data_rx_ready <= 0; 
                        end     
                            /*--- ���� ������� ��������� � �����������: POS_SWICH_IO -> ���� ���������� ����� �� ���� � ��������� ������ ---*/
                            if(`POS_SWICH_IO)         tristate <= 1;
                            /*--- ���� ������� ��������� � �����������: DATA_READ_SECTION -> �������� ������ � ��������������� ���� � ������ ���� � ������ ����� --*/
                            if(`DATA_READ_SECTION)     DATA_RX_r[29 - cnt_re] <= DATA_RX_serial;
                            /*--- ���� ������� ��������� � �����������: POS_READY_RX -> ���� ������������� ������ "DATA_READY" � ����� �������� �������� ������  */
                            if(`POS_READY_RX)         data_rx_ready <= 1; 
                            /*--- ������� ���������� ������ ������������� �������� ����� ������ ������� START */
                            if(`ENABLE_COUNT_RE)      cnt_re <= cnt_re + 1;
                            else                      cnt_re <= 0;
                            /*--- ���� ������� ��������� � �����������: POS_MAX_CNT -> ������������� ���������� � ���������: "�������� ������� START"  */
                            if(`POS_MAX_CNT_RE) begin
                                enable_cnt_re <= 0;
                                start_prev_fe <= 0;
                            end
                    end  
                    else begin  /*-- ��� ������ ������ �� SPI, �� ������ ������������� ������ �� ������ --*/
                        data_rx_ready <= 0; 
                        tristate <= 0;
                        start_prev_fe <= 0; 
                        enable_cnt_re <= 0;
                    end                     
                end
   
endmodule
