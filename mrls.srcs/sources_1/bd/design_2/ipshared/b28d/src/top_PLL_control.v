
module top_PLL_control(

    input wire clk_100MHz,
    input wire clk_10MHz,
    input wire clk_SPI_PLL,
    input wire clkDCO_10MHz,
    input wire reset,
//    output wire frame_even,
    output wire pll_sen,
    output wire pll_sck,
    output wire pll_mosi,
    input wire pll_ld_sdo,    
    output wire pll_cen,
    output wire pll_trig,
    output wire [5:0] ATTEN, 
    output wire PLL_POW_EN,
    output wire LOW_AMP_EN,
    output wire PAMP_EN,
    output wire start_adc_count,

    input wire [31:0]	slv_reg0,
    input wire [31:0]	slv_reg1,
    input wire [31:0]	slv_reg2,
    input wire [31:0]	slv_reg3,
    input wire [31:0]	slv_reg4,
    input wire [31:0]	slv_reg5,
    input wire [31:0]	slv_reg6,
    input wire [31:0]	slv_reg7,
    
    output wire [31:0]	ip2mb_reg0,
    output wire [31:0]	ip2mb_reg1,
    output reg  [31:0]	ip2mb_reg2,
    output reg  [31:0]	ip2mb_reg3,
    output reg  [31:0]	ip2mb_reg4,
    output reg  [31:0]	ip2mb_reg5,
    output reg  [31:0]	ip2mb_reg6,
    output reg  [31:0]	ip2mb_reg7,    
    input wire azimut_0,
    output wire [31:0] azimut,
    input wire PAMP_ALM_FPGA
);
        wire pll_sck_read;
        wire pll_mosi_read;
        wire pll_sen_read;
        wire pll_data_ready;
        wire pll_data_ready_RX;
        wire pll_data_ready_TX;
        assign pll_data_ready = pll_data_ready_RX | pll_data_ready_TX;  
        wire pll_sck_write;
        wire pll_mosi_write;
        wire pll_sen_write;
        reg [5:0] atten_reg = 6'h3F;
        reg pll_cen_reg;
        reg START_send;        
        reg START_receive; 
        reg START_trig; 
        wire [31:0] DATA_receive;
        wire [31:0] DATA_send;
        wire [7:0] reg_address_receive;
        wire [4:0] reg_address_send;
        wire miso;
        reg start_adc_count_r;
        reg PLL_POW_EN_reg;
        reg LOW_AMP_EN_reg;
        reg PAMP_EN_reg;
        reg pll_trig_reg2;
             
        assign start_adc_count = start_adc_count_r;
        assign PLL_POW_EN = PLL_POW_EN_reg;
        assign LOW_AMP_EN = LOW_AMP_EN_reg;
        assign ATTEN = atten_reg; 
        assign PAMP_EN = PAMP_EN_reg;
        assign pll_sen = pll_sen_write | pll_sen_read;
        assign pll_sck = pll_sck_write | pll_sck_read;
        assign pll_mosi = pll_mosi_write | pll_mosi_read;   
        assign pll_cen = pll_cen_reg;
        assign pll_trig = pll_trig_reg2;  
        reg [15:0] trig_tguard_counter=0;   
        reg [15:0] trig_tsweep_counter=0;

 spi_hmc_mode_RX spi_hmc_mode_RX_inst(
    .start(START_receive),
    .adr(reg_address_receive),
    .data(DATA_receive),
    .clk(clk_SPI_PLL),
    .mosi(pll_mosi_read),
    .sck(pll_sck_read),
    .miso(pll_ld_sdo),
    .cs(pll_sen_read),
    .ready(pll_data_ready_RX)
);     

reg  netSTART_send;    
//reg  [15:0] sweep_val; 
localparam sweep_val = 9900;
reg  [15:0] sweep_val_tmp; 
wire azimut_0_to_cpu;
assign  azimut_0_to_cpu = azimut_0;
assign  ip2mb_reg0[0] = pll_data_ready;  
assign  ip2mb_reg0[1] = azimut_0_to_cpu;
assign  ip2mb_reg0[2] = PAMP_ALM_FPGA;
assign  ip2mb_reg1 = DATA_receive;      
assign  reg_address_send = slv_reg1;   
assign  reg_address_receive = slv_reg2[7:0];
assign  DATA_send = slv_reg3;  
        
spi_hmc_mode_TX spi_hmc_mode_TX_inst(
    .start(START_send),
    .adr(reg_address_send),
    .data(DATA_send),
    .clk(clk_SPI_PLL),
    .mosi(pll_mosi_write),
    .sck(pll_sck_write),
    .miso(miso),
    .cs(pll_sen_write),
    .ready(pll_data_ready_TX)
);
      
always @(posedge clk_100MHz) begin 
     ip2mb_reg2 <= 32'hACDC0002;
     ip2mb_reg3 <= 32'hACDC0003;
     ip2mb_reg4 <= 32'hACDC0004;
     ip2mb_reg5 <= 32'hACDC0005;
     ip2mb_reg6 <= 32'hACDC0006;
     ip2mb_reg7 <= 32'hACDC0007;    
end   

localparam tguard_time = 100;      
//reg [15:0] frame_1ms_count;
//assign frame_even = frame_1ms_count[0];
reg [15:0] shift_front_from_cpu;
reg enable_triger;
reg enable_triger_CMD;

always @(posedge clk_100MHz) begin    
      if ( reset ) begin      
                                        PLL_POW_EN_reg          <= slv_reg5[0];
                                        PAMP_EN_reg             <= slv_reg5[1];
                                        pll_cen_reg             <= slv_reg5[2];
                                        LOW_AMP_EN_reg          <= slv_reg5[3];
                                        atten_reg               <= slv_reg4[5:0];
//                                        sweep_val_tmp           <= slv_reg6;
//                                        sweep_val               <= sweep_val_tmp;
                                        shift_front_from_cpu    <= slv_reg7[31:1]; 
                                                         
                                        enable_triger_CMD       <= slv_reg7[0];    
            if(slv_reg0[0] == 1'b1)     START_send              <= 1;
            else                        START_send              <= 0;
            if(slv_reg0[1] == 1'b1)     START_receive           <= 1;
            else                        START_receive           <= 0; 
            START_trig <= 1;               
      end
      else begin
//            sweep_val <= 9900;
            enable_triger_CMD <= 0;
      end  
end
             
reg [15:0] tsweep_counter;
reg [15:0] tguard_counter;
reg azimut_0_prev = 0;
reg reset_cnt_trig;  

always @(posedge clkDCO_10MHz)  begin

      if( reset & START_trig) begin          
            if( tsweep_counter == shift_front_from_cpu  )
                start_adc_count_r <= 1;
                  
            if( tsweep_counter == (sweep_val - 1) | ( tsweep_counter == 0) ) 
                start_adc_count_r <= 0;
      end
      else begin
        start_adc_count_r <= 0;
      end 
end

always @(posedge clk_10MHz) begin

                if( azimut_0) begin
                    enable_triger <= enable_triger_CMD;                        // Здесь принятая команда активируется
                end

                if(reset & enable_triger) begin
                
                                        if(!azimut_0) begin     
                                                    if ((tsweep_counter < sweep_val)&(tguard_counter==0))              begin 
                                                        tsweep_counter <= tsweep_counter + 1;
                                                        pll_trig_reg2 <= 0;
                                                    end
                                                    else if ((tsweep_counter == sweep_val)&(tguard_counter==0))               begin
                                                        tsweep_counter <= 0;
                                                        pll_trig_reg2 <= 1;
                                                        tguard_counter<=1;                        
                                                    end
                                                    else if ((tsweep_counter==0)&(tguard_counter>0)&(tguard_counter < tguard_time))            begin
                                                        tsweep_counter <= 0;
                                                        pll_trig_reg2 <= 0;
                                                        tguard_counter<=tguard_counter+1;                        
                                                    end
                                                    else if ((tsweep_counter==0)&(tguard_counter == tguard_time))             begin
                                                        tsweep_counter <= 0;
                                                        pll_trig_reg2 <= 1;
                                                        tguard_counter<=0;                        
                                                    end
                                        end
                                        else begin 
                                            pll_trig_reg2 <= 0;
                                            tsweep_counter <= shift_front_from_cpu - 1;
//                                            tsweep_counter <= sweep_val;
                                            tguard_counter <= 0;
                                        end        
                end
                else begin
                    pll_trig_reg2 <= 0;
                    tsweep_counter <=0;      
                    tguard_counter <= 0;
                end
end
endmodule