-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
-- Date        : Thu May 12 10:37:25 2022
-- Host        : alexbtlab-System-Product-Name running 64-bit Ubuntu 20.04.3 LTS
-- Command     : write_vhdl -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_AD9650_0_0_stub.vhdl
-- Design      : design_1_AD9650_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7z020clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  Port ( 
    clk_10MHz : in STD_LOGIC;
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 5 downto 0 );
    s00_axi_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_awready : out STD_LOGIC;
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_wvalid : in STD_LOGIC;
    s00_axi_wready : out STD_LOGIC;
    s00_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_bvalid : out STD_LOGIC;
    s00_axi_bready : in STD_LOGIC;
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 5 downto 0 );
    s00_axi_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_arvalid : in STD_LOGIC;
    s00_axi_arready : out STD_LOGIC;
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_rvalid : out STD_LOGIC;
    s00_axi_rready : in STD_LOGIC;
    m00_fft_axis_tvalid : out STD_LOGIC;
    m00_fft_axis_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_fft_axis_tstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m00_fft_axis_tlast : out STD_LOGIC;
    m00_fft_axis_tready : in STD_LOGIC;
    m01_fft_axis_tvalid : out STD_LOGIC;
    m01_fft_axis_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m01_fft_axis_tstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m01_fft_axis_tlast : out STD_LOGIC;
    m01_fft_axis_tready : in STD_LOGIC;
    s00_fft_axis_tvalid : in STD_LOGIC;
    s00_fft_axis_tdata : in STD_LOGIC_VECTOR ( 63 downto 0 );
    s00_fft_axis_tstrb : in STD_LOGIC_VECTOR ( 7 downto 0 );
    s00_fft_axis_tlast : in STD_LOGIC;
    s00_fft_axis_tready : out STD_LOGIC;
    s01_fft_axis_tvalid : in STD_LOGIC;
    s01_fft_axis_tdata : in STD_LOGIC_VECTOR ( 63 downto 0 );
    s01_fft_axis_tstrb : in STD_LOGIC_VECTOR ( 7 downto 0 );
    s01_fft_axis_tlast : in STD_LOGIC;
    s01_fft_axis_tready : out STD_LOGIC;
    m00_dma_axis_tvalid : out STD_LOGIC;
    m00_dma_axis_tdata : out STD_LOGIC_VECTOR ( 63 downto 0 );
    m00_dma_axis_tstrb : out STD_LOGIC_VECTOR ( 7 downto 0 );
    m00_dma_axis_tlast : out STD_LOGIC;
    m00_dma_axis_tready : in STD_LOGIC;
    adc_spi_sck : out STD_LOGIC;
    adc_spi_cs : out STD_LOGIC;
    adc_spi_sdio : inout STD_LOGIC;
    ADC_PDwN : out STD_LOGIC;
    SYNC : out STD_LOGIC;
    DATA_INA : in STD_LOGIC_VECTOR ( 15 downto 0 );
    DATA_INB : in STD_LOGIC_VECTOR ( 15 downto 0 );
    allowed_clk : in STD_LOGIC;
    azimut8 : in STD_LOGIC_VECTOR ( 15 downto 0 );
    azimut_0 : in STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC
  );

end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture stub of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "clk_10MHz,s00_axi_awaddr[5:0],s00_axi_awprot[2:0],s00_axi_awvalid,s00_axi_awready,s00_axi_wdata[31:0],s00_axi_wstrb[3:0],s00_axi_wvalid,s00_axi_wready,s00_axi_bresp[1:0],s00_axi_bvalid,s00_axi_bready,s00_axi_araddr[5:0],s00_axi_arprot[2:0],s00_axi_arvalid,s00_axi_arready,s00_axi_rdata[31:0],s00_axi_rresp[1:0],s00_axi_rvalid,s00_axi_rready,m00_fft_axis_tvalid,m00_fft_axis_tdata[31:0],m00_fft_axis_tstrb[3:0],m00_fft_axis_tlast,m00_fft_axis_tready,m01_fft_axis_tvalid,m01_fft_axis_tdata[31:0],m01_fft_axis_tstrb[3:0],m01_fft_axis_tlast,m01_fft_axis_tready,s00_fft_axis_tvalid,s00_fft_axis_tdata[63:0],s00_fft_axis_tstrb[7:0],s00_fft_axis_tlast,s00_fft_axis_tready,s01_fft_axis_tvalid,s01_fft_axis_tdata[63:0],s01_fft_axis_tstrb[7:0],s01_fft_axis_tlast,s01_fft_axis_tready,m00_dma_axis_tvalid,m00_dma_axis_tdata[63:0],m00_dma_axis_tstrb[7:0],m00_dma_axis_tlast,m00_dma_axis_tready,adc_spi_sck,adc_spi_cs,adc_spi_sdio,ADC_PDwN,SYNC,DATA_INA[15:0],DATA_INB[15:0],allowed_clk,azimut8[15:0],azimut_0,s00_axi_aclk,s00_axi_aresetn";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "AD9650_v3_0,Vivado 2019.1";
begin
end;
