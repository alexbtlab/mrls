-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
-- Date        : Fri Apr  1 16:54:31 2022
-- Host        : alexbtlab-System-Product-Name running 64-bit Ubuntu 20.04.3 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_HMC769_0_0_sim_netlist.vhdl
-- Design      : design_1_HMC769_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z020clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_HMC769_v5_0_S00_AXI is
  port (
    s00_axi_wready : out STD_LOGIC;
    SR : out STD_LOGIC_VECTOR ( 0 to 0 );
    s00_axi_awready : out STD_LOGIC;
    s00_axi_arready : out STD_LOGIC;
    s00_axi_bvalid : out STD_LOGIC;
    s00_axi_rvalid : out STD_LOGIC;
    \cnt_reg[3]\ : out STD_LOGIC;
    enable_triger_CMD : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 15 downto 0 );
    \cnt_reg[2]\ : out STD_LOGIC;
    reg_address_receive_ila : out STD_LOGIC_VECTOR ( 7 downto 0 );
    DATA_send_ila : out STD_LOGIC_VECTOR ( 31 downto 0 );
    \slv_reg1_reg[0]_0\ : out STD_LOGIC;
    reg_address_send_ila : out STD_LOGIC_VECTOR ( 4 downto 0 );
    \axi_araddr_reg[5]_0\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \slv_reg6_reg[15]_0\ : out STD_LOGIC_VECTOR ( 15 downto 0 );
    \slv_reg5_reg[2]_0\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \slv_reg4_reg[5]_0\ : out STD_LOGIC_VECTOR ( 5 downto 0 );
    \axi_araddr_reg[4]_0\ : out STD_LOGIC;
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    slv_reg0 : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_aclk : in STD_LOGIC;
    \out\ : in STD_LOGIC_VECTOR ( 4 downto 0 );
    mosi_r_reg : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC;
    mosi_r_reg_0 : in STD_LOGIC_VECTOR ( 2 downto 0 );
    \axi_rdata_reg[0]_0\ : in STD_LOGIC;
    DATA_receive_ila : in STD_LOGIC_VECTOR ( 22 downto 0 );
    azimut_0 : in STD_LOGIC;
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_wvalid : in STD_LOGIC;
    s00_axi_bready : in STD_LOGIC;
    s00_axi_arvalid : in STD_LOGIC;
    s00_axi_rready : in STD_LOGIC;
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_HMC769_v5_0_S00_AXI;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_HMC769_v5_0_S00_AXI is
  signal \^data_send_ila\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \^q\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \^sr\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal aw_en_i_1_n_0 : STD_LOGIC;
  signal aw_en_reg_n_0 : STD_LOGIC;
  signal \^axi_araddr_reg[5]_0\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal axi_arready0 : STD_LOGIC;
  signal axi_awready0 : STD_LOGIC;
  signal axi_bvalid_i_1_n_0 : STD_LOGIC;
  signal \axi_rdata[0]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[0]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[10]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[11]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[12]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[13]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[14]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[14]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[15]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[16]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[16]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[17]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[17]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[18]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[18]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[19]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[1]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[20]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[20]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[21]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[21]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[21]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[22]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[22]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[23]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[24]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[24]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[25]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[25]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[26]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[26]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[27]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[28]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[28]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[29]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[29]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[29]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[2]_i_4_n_0\ : STD_LOGIC;
  signal \axi_rdata[30]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[30]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[31]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[3]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[4]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[5]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[6]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[7]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[8]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata[9]_i_3_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[18]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[19]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[20]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[22]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[23]_i_2_n_0\ : STD_LOGIC;
  signal \axi_rdata_reg[2]_i_2_n_0\ : STD_LOGIC;
  signal axi_rvalid_i_1_n_0 : STD_LOGIC;
  signal axi_wready0 : STD_LOGIC;
  signal mosi_r_i_15_n_0 : STD_LOGIC;
  signal mosi_r_i_16_n_0 : STD_LOGIC;
  signal mosi_r_i_17_n_0 : STD_LOGIC;
  signal mosi_r_i_18_n_0 : STD_LOGIC;
  signal mosi_r_i_19_n_0 : STD_LOGIC;
  signal mosi_r_i_20_n_0 : STD_LOGIC;
  signal mosi_r_i_7_n_0 : STD_LOGIC;
  signal mosi_r_reg_i_10_n_0 : STD_LOGIC;
  signal mosi_r_reg_i_11_n_0 : STD_LOGIC;
  signal mosi_r_reg_i_12_n_0 : STD_LOGIC;
  signal p_0_in : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal p_1_in : STD_LOGIC_VECTOR ( 31 downto 4 );
  signal \^reg_address_receive_ila\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \^reg_address_send_ila\ : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal reg_data_out : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \^s00_axi_arready\ : STD_LOGIC;
  signal \^s00_axi_awready\ : STD_LOGIC;
  signal \^s00_axi_bvalid\ : STD_LOGIC;
  signal \^s00_axi_rvalid\ : STD_LOGIC;
  signal \^s00_axi_wready\ : STD_LOGIC;
  signal sel0 : STD_LOGIC_VECTOR ( 2 downto 1 );
  signal \^slv_reg0\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \slv_reg0[0]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg0[1]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg0[1]_i_2_n_0\ : STD_LOGIC;
  signal slv_reg1 : STD_LOGIC_VECTOR ( 31 downto 5 );
  signal slv_reg2 : STD_LOGIC_VECTOR ( 31 downto 8 );
  signal \slv_reg2[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg2[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg2[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg2[7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg3[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg3[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg3[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg3[7]_i_1_n_0\ : STD_LOGIC;
  signal slv_reg4 : STD_LOGIC_VECTOR ( 31 downto 6 );
  signal \slv_reg4[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg4[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg4[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg4[7]_i_1_n_0\ : STD_LOGIC;
  signal \^slv_reg4_reg[5]_0\ : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal slv_reg5 : STD_LOGIC_VECTOR ( 31 downto 3 );
  signal \slv_reg5[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg5[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg5[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg5[7]_i_1_n_0\ : STD_LOGIC;
  signal \^slv_reg5_reg[2]_0\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal slv_reg6 : STD_LOGIC_VECTOR ( 31 downto 16 );
  signal \slv_reg6[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg6[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg6[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg6[7]_i_1_n_0\ : STD_LOGIC;
  signal \^slv_reg6_reg[15]_0\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal slv_reg7 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \slv_reg7[15]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg7[23]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg7[31]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg7[7]_i_1_n_0\ : STD_LOGIC;
  signal \slv_reg_rden__0\ : STD_LOGIC;
  signal \slv_reg_wren__0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \axi_rdata[0]_i_5\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \axi_rdata[21]_i_4\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \axi_rdata[26]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \axi_rdata[29]_i_4\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of axi_wready_i_1 : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \slv_reg0[0]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \slv_reg0[1]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \slv_reg3[31]_i_2\ : label is "soft_lutpair2";
begin
  DATA_send_ila(31 downto 0) <= \^data_send_ila\(31 downto 0);
  Q(15 downto 0) <= \^q\(15 downto 0);
  SR(0) <= \^sr\(0);
  \axi_araddr_reg[5]_0\(1 downto 0) <= \^axi_araddr_reg[5]_0\(1 downto 0);
  reg_address_receive_ila(7 downto 0) <= \^reg_address_receive_ila\(7 downto 0);
  reg_address_send_ila(4 downto 0) <= \^reg_address_send_ila\(4 downto 0);
  s00_axi_arready <= \^s00_axi_arready\;
  s00_axi_awready <= \^s00_axi_awready\;
  s00_axi_bvalid <= \^s00_axi_bvalid\;
  s00_axi_rvalid <= \^s00_axi_rvalid\;
  s00_axi_wready <= \^s00_axi_wready\;
  slv_reg0(1 downto 0) <= \^slv_reg0\(1 downto 0);
  \slv_reg4_reg[5]_0\(5 downto 0) <= \^slv_reg4_reg[5]_0\(5 downto 0);
  \slv_reg5_reg[2]_0\(2 downto 0) <= \^slv_reg5_reg[2]_0\(2 downto 0);
  \slv_reg6_reg[15]_0\(15 downto 0) <= \^slv_reg6_reg[15]_0\(15 downto 0);
aw_en_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F7FFC4CCC4CCC4CC"
    )
        port map (
      I0 => s00_axi_awvalid,
      I1 => aw_en_reg_n_0,
      I2 => \^s00_axi_awready\,
      I3 => s00_axi_wvalid,
      I4 => s00_axi_bready,
      I5 => \^s00_axi_bvalid\,
      O => aw_en_i_1_n_0
    );
aw_en_reg: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => aw_en_i_1_n_0,
      Q => aw_en_reg_n_0,
      S => \^sr\(0)
    );
\axi_araddr_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => axi_arready0,
      D => s00_axi_araddr(0),
      Q => \^axi_araddr_reg[5]_0\(0),
      R => \^sr\(0)
    );
\axi_araddr_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => axi_arready0,
      D => s00_axi_araddr(1),
      Q => sel0(1),
      R => \^sr\(0)
    );
\axi_araddr_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => axi_arready0,
      D => s00_axi_araddr(2),
      Q => sel0(2),
      R => \^sr\(0)
    );
\axi_araddr_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => axi_arready0,
      D => s00_axi_araddr(3),
      Q => \^axi_araddr_reg[5]_0\(1),
      R => \^sr\(0)
    );
axi_arready_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => s00_axi_arvalid,
      I1 => \^s00_axi_arready\,
      O => axi_arready0
    );
axi_arready_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_arready0,
      Q => \^s00_axi_arready\,
      R => \^sr\(0)
    );
\axi_awaddr_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => axi_awready0,
      D => s00_axi_awaddr(0),
      Q => p_0_in(0),
      R => \^sr\(0)
    );
\axi_awaddr_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => axi_awready0,
      D => s00_axi_awaddr(1),
      Q => p_0_in(1),
      R => \^sr\(0)
    );
\axi_awaddr_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => axi_awready0,
      D => s00_axi_awaddr(2),
      Q => p_0_in(2),
      R => \^sr\(0)
    );
\axi_awaddr_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => axi_awready0,
      D => s00_axi_awaddr(3),
      Q => p_0_in(3),
      R => \^sr\(0)
    );
axi_awready_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => s00_axi_aresetn,
      O => \^sr\(0)
    );
axi_awready_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2000"
    )
        port map (
      I0 => s00_axi_wvalid,
      I1 => \^s00_axi_awready\,
      I2 => aw_en_reg_n_0,
      I3 => s00_axi_awvalid,
      O => axi_awready0
    );
axi_awready_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_awready0,
      Q => \^s00_axi_awready\,
      R => \^sr\(0)
    );
axi_bvalid_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000FFFF80008000"
    )
        port map (
      I0 => s00_axi_awvalid,
      I1 => s00_axi_wvalid,
      I2 => \^s00_axi_awready\,
      I3 => \^s00_axi_wready\,
      I4 => s00_axi_bready,
      I5 => \^s00_axi_bvalid\,
      O => axi_bvalid_i_1_n_0
    );
axi_bvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_bvalid_i_1_n_0,
      Q => \^s00_axi_bvalid\,
      R => \^sr\(0)
    );
\axi_rdata[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF4540"
    )
        port map (
      I0 => \^axi_araddr_reg[5]_0\(1),
      I1 => \axi_rdata[0]_i_2_n_0\,
      I2 => sel0(2),
      I3 => \axi_rdata[0]_i_3_n_0\,
      I4 => \axi_rdata_reg[0]_0\,
      O => reg_data_out(0)
    );
\axi_rdata[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(0),
      I1 => \^slv_reg6_reg[15]_0\(0),
      I2 => sel0(1),
      I3 => \^slv_reg5_reg[2]_0\(0),
      I4 => \^axi_araddr_reg[5]_0\(0),
      I5 => \^slv_reg4_reg[5]_0\(0),
      O => \axi_rdata[0]_i_2_n_0\
    );
\axi_rdata[0]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFA0CFCF"
    )
        port map (
      I0 => \^data_send_ila\(0),
      I1 => \^reg_address_receive_ila\(0),
      I2 => sel0(1),
      I3 => \^reg_address_send_ila\(0),
      I4 => \^axi_araddr_reg[5]_0\(0),
      O => \axi_rdata[0]_i_3_n_0\
    );
\axi_rdata[0]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => sel0(2),
      I1 => sel0(1),
      O => \axi_araddr_reg[4]_0\
    );
\axi_rdata[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4F444F4F4F444444"
    )
        port map (
      I0 => \axi_rdata[21]_i_4_n_0\,
      I1 => DATA_receive_ila(9),
      I2 => \^axi_araddr_reg[5]_0\(1),
      I3 => \axi_rdata[10]_i_2_n_0\,
      I4 => sel0(2),
      I5 => \axi_rdata[10]_i_3_n_0\,
      O => reg_data_out(10)
    );
\axi_rdata[10]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q\(9),
      I1 => \^slv_reg6_reg[15]_0\(10),
      I2 => sel0(1),
      I3 => slv_reg5(10),
      I4 => \^axi_araddr_reg[5]_0\(0),
      I5 => slv_reg4(10),
      O => \axi_rdata[10]_i_2_n_0\
    );
\axi_rdata[10]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFA0CFCF"
    )
        port map (
      I0 => \^data_send_ila\(10),
      I1 => slv_reg2(10),
      I2 => sel0(1),
      I3 => slv_reg1(10),
      I4 => \^axi_araddr_reg[5]_0\(0),
      O => \axi_rdata[10]_i_3_n_0\
    );
\axi_rdata[11]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4540FFFF45404540"
    )
        port map (
      I0 => \^axi_araddr_reg[5]_0\(1),
      I1 => \axi_rdata[11]_i_2_n_0\,
      I2 => sel0(2),
      I3 => \axi_rdata[11]_i_3_n_0\,
      I4 => \axi_rdata[21]_i_4_n_0\,
      I5 => DATA_receive_ila(10),
      O => reg_data_out(11)
    );
\axi_rdata[11]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q\(10),
      I1 => \^slv_reg6_reg[15]_0\(11),
      I2 => sel0(1),
      I3 => slv_reg5(11),
      I4 => \^axi_araddr_reg[5]_0\(0),
      I5 => slv_reg4(11),
      O => \axi_rdata[11]_i_2_n_0\
    );
\axi_rdata[11]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFA0CFCF"
    )
        port map (
      I0 => \^data_send_ila\(11),
      I1 => slv_reg2(11),
      I2 => sel0(1),
      I3 => slv_reg1(11),
      I4 => \^axi_araddr_reg[5]_0\(0),
      O => \axi_rdata[11]_i_3_n_0\
    );
\axi_rdata[12]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4540FFFF45404540"
    )
        port map (
      I0 => \^axi_araddr_reg[5]_0\(1),
      I1 => \axi_rdata[12]_i_2_n_0\,
      I2 => sel0(2),
      I3 => \axi_rdata[12]_i_3_n_0\,
      I4 => \axi_rdata[21]_i_4_n_0\,
      I5 => DATA_receive_ila(11),
      O => reg_data_out(12)
    );
\axi_rdata[12]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q\(11),
      I1 => \^slv_reg6_reg[15]_0\(12),
      I2 => sel0(1),
      I3 => slv_reg5(12),
      I4 => \^axi_araddr_reg[5]_0\(0),
      I5 => slv_reg4(12),
      O => \axi_rdata[12]_i_2_n_0\
    );
\axi_rdata[12]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFA0CFCF"
    )
        port map (
      I0 => \^data_send_ila\(12),
      I1 => slv_reg2(12),
      I2 => sel0(1),
      I3 => slv_reg1(12),
      I4 => \^axi_araddr_reg[5]_0\(0),
      O => \axi_rdata[12]_i_3_n_0\
    );
\axi_rdata[13]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4F444F4F4F444444"
    )
        port map (
      I0 => \axi_rdata[21]_i_4_n_0\,
      I1 => DATA_receive_ila(12),
      I2 => \^axi_araddr_reg[5]_0\(1),
      I3 => \axi_rdata[13]_i_2_n_0\,
      I4 => sel0(2),
      I5 => \axi_rdata[13]_i_3_n_0\,
      O => reg_data_out(13)
    );
\axi_rdata[13]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q\(12),
      I1 => \^slv_reg6_reg[15]_0\(13),
      I2 => sel0(1),
      I3 => slv_reg5(13),
      I4 => \^axi_araddr_reg[5]_0\(0),
      I5 => slv_reg4(13),
      O => \axi_rdata[13]_i_2_n_0\
    );
\axi_rdata[13]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFA0CFCF"
    )
        port map (
      I0 => \^data_send_ila\(13),
      I1 => slv_reg2(13),
      I2 => sel0(1),
      I3 => slv_reg1(13),
      I4 => \^axi_araddr_reg[5]_0\(0),
      O => \axi_rdata[13]_i_3_n_0\
    );
\axi_rdata[14]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4540FFFF45404540"
    )
        port map (
      I0 => \^axi_araddr_reg[5]_0\(1),
      I1 => \axi_rdata[14]_i_2_n_0\,
      I2 => sel0(2),
      I3 => \axi_rdata[14]_i_3_n_0\,
      I4 => \axi_rdata[21]_i_4_n_0\,
      I5 => DATA_receive_ila(13),
      O => reg_data_out(14)
    );
\axi_rdata[14]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q\(13),
      I1 => \^slv_reg6_reg[15]_0\(14),
      I2 => sel0(1),
      I3 => slv_reg5(14),
      I4 => \^axi_araddr_reg[5]_0\(0),
      I5 => slv_reg4(14),
      O => \axi_rdata[14]_i_2_n_0\
    );
\axi_rdata[14]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFA0CFCF"
    )
        port map (
      I0 => \^data_send_ila\(14),
      I1 => slv_reg2(14),
      I2 => sel0(1),
      I3 => slv_reg1(14),
      I4 => \^axi_araddr_reg[5]_0\(0),
      O => \axi_rdata[14]_i_3_n_0\
    );
\axi_rdata[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4540FFFF45404540"
    )
        port map (
      I0 => \^axi_araddr_reg[5]_0\(1),
      I1 => \axi_rdata[15]_i_2_n_0\,
      I2 => sel0(2),
      I3 => \axi_rdata[15]_i_3_n_0\,
      I4 => \axi_rdata[21]_i_4_n_0\,
      I5 => DATA_receive_ila(14),
      O => reg_data_out(15)
    );
\axi_rdata[15]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q\(14),
      I1 => \^slv_reg6_reg[15]_0\(15),
      I2 => sel0(1),
      I3 => slv_reg5(15),
      I4 => \^axi_araddr_reg[5]_0\(0),
      I5 => slv_reg4(15),
      O => \axi_rdata[15]_i_2_n_0\
    );
\axi_rdata[15]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFA0CFCF"
    )
        port map (
      I0 => \^data_send_ila\(15),
      I1 => slv_reg2(15),
      I2 => sel0(1),
      I3 => slv_reg1(15),
      I4 => \^axi_araddr_reg[5]_0\(0),
      O => \axi_rdata[15]_i_3_n_0\
    );
\axi_rdata[16]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4F444F4F4F444444"
    )
        port map (
      I0 => \axi_rdata[21]_i_4_n_0\,
      I1 => DATA_receive_ila(15),
      I2 => \^axi_araddr_reg[5]_0\(1),
      I3 => \axi_rdata[16]_i_2_n_0\,
      I4 => sel0(2),
      I5 => \axi_rdata[16]_i_3_n_0\,
      O => reg_data_out(16)
    );
\axi_rdata[16]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q\(15),
      I1 => slv_reg6(16),
      I2 => sel0(1),
      I3 => slv_reg5(16),
      I4 => \^axi_araddr_reg[5]_0\(0),
      I5 => slv_reg4(16),
      O => \axi_rdata[16]_i_2_n_0\
    );
\axi_rdata[16]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => \^data_send_ila\(16),
      I1 => slv_reg2(16),
      I2 => sel0(1),
      I3 => \^axi_araddr_reg[5]_0\(0),
      I4 => slv_reg1(16),
      O => \axi_rdata[16]_i_3_n_0\
    );
\axi_rdata[17]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4F444F4F4F444444"
    )
        port map (
      I0 => \axi_rdata[21]_i_4_n_0\,
      I1 => DATA_receive_ila(16),
      I2 => \^axi_araddr_reg[5]_0\(1),
      I3 => \axi_rdata[17]_i_2_n_0\,
      I4 => sel0(2),
      I5 => \axi_rdata[17]_i_3_n_0\,
      O => reg_data_out(17)
    );
\axi_rdata[17]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(17),
      I1 => slv_reg6(17),
      I2 => sel0(1),
      I3 => slv_reg5(17),
      I4 => \^axi_araddr_reg[5]_0\(0),
      I5 => slv_reg4(17),
      O => \axi_rdata[17]_i_2_n_0\
    );
\axi_rdata[17]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => \^data_send_ila\(17),
      I1 => slv_reg2(17),
      I2 => sel0(1),
      I3 => \^axi_araddr_reg[5]_0\(0),
      I4 => slv_reg1(17),
      O => \axi_rdata[17]_i_3_n_0\
    );
\axi_rdata[18]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFEAFFFFFFEA0000"
    )
        port map (
      I0 => sel0(2),
      I1 => \^axi_araddr_reg[5]_0\(0),
      I2 => DATA_receive_ila(17),
      I3 => sel0(1),
      I4 => \^axi_araddr_reg[5]_0\(1),
      I5 => \axi_rdata_reg[18]_i_2_n_0\,
      O => reg_data_out(18)
    );
\axi_rdata[18]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFA0CFCF"
    )
        port map (
      I0 => \^data_send_ila\(18),
      I1 => slv_reg2(18),
      I2 => sel0(1),
      I3 => slv_reg1(18),
      I4 => \^axi_araddr_reg[5]_0\(0),
      O => \axi_rdata[18]_i_3_n_0\
    );
\axi_rdata[18]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(18),
      I1 => slv_reg6(18),
      I2 => sel0(1),
      I3 => slv_reg5(18),
      I4 => \^axi_araddr_reg[5]_0\(0),
      I5 => slv_reg4(18),
      O => \axi_rdata[18]_i_4_n_0\
    );
\axi_rdata[19]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFEAFFFFFFEA0000"
    )
        port map (
      I0 => sel0(2),
      I1 => \^axi_araddr_reg[5]_0\(0),
      I2 => DATA_receive_ila(18),
      I3 => sel0(1),
      I4 => \^axi_araddr_reg[5]_0\(1),
      I5 => \axi_rdata_reg[19]_i_2_n_0\,
      O => reg_data_out(19)
    );
\axi_rdata[19]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFA0CFCF"
    )
        port map (
      I0 => \^data_send_ila\(19),
      I1 => slv_reg2(19),
      I2 => sel0(1),
      I3 => slv_reg1(19),
      I4 => \^axi_araddr_reg[5]_0\(0),
      O => \axi_rdata[19]_i_3_n_0\
    );
\axi_rdata[19]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(19),
      I1 => slv_reg6(19),
      I2 => sel0(1),
      I3 => slv_reg5(19),
      I4 => \^axi_araddr_reg[5]_0\(0),
      I5 => slv_reg4(19),
      O => \axi_rdata[19]_i_4_n_0\
    );
\axi_rdata[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8BBB888"
    )
        port map (
      I0 => \axi_rdata[1]_i_2_n_0\,
      I1 => \^axi_araddr_reg[5]_0\(1),
      I2 => \axi_rdata[1]_i_3_n_0\,
      I3 => sel0(2),
      I4 => \axi_rdata[1]_i_4_n_0\,
      O => reg_data_out(1)
    );
\axi_rdata[1]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BABBBAAA"
    )
        port map (
      I0 => sel0(1),
      I1 => sel0(2),
      I2 => DATA_receive_ila(0),
      I3 => \^axi_araddr_reg[5]_0\(0),
      I4 => azimut_0,
      O => \axi_rdata[1]_i_2_n_0\
    );
\axi_rdata[1]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^slv_reg6_reg[15]_0\(1),
      I2 => sel0(1),
      I3 => \^slv_reg5_reg[2]_0\(1),
      I4 => \^axi_araddr_reg[5]_0\(0),
      I5 => \^slv_reg4_reg[5]_0\(1),
      O => \axi_rdata[1]_i_3_n_0\
    );
\axi_rdata[1]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFA0C0C0"
    )
        port map (
      I0 => \^data_send_ila\(1),
      I1 => \^reg_address_receive_ila\(1),
      I2 => sel0(1),
      I3 => \^reg_address_send_ila\(1),
      I4 => \^axi_araddr_reg[5]_0\(0),
      O => \axi_rdata[1]_i_4_n_0\
    );
\axi_rdata[20]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFEAFFFFFFEA0000"
    )
        port map (
      I0 => sel0(2),
      I1 => \^axi_araddr_reg[5]_0\(0),
      I2 => DATA_receive_ila(19),
      I3 => sel0(1),
      I4 => \^axi_araddr_reg[5]_0\(1),
      I5 => \axi_rdata_reg[20]_i_2_n_0\,
      O => reg_data_out(20)
    );
\axi_rdata[20]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFA0CFCF"
    )
        port map (
      I0 => \^data_send_ila\(20),
      I1 => slv_reg2(20),
      I2 => sel0(1),
      I3 => slv_reg1(20),
      I4 => \^axi_araddr_reg[5]_0\(0),
      O => \axi_rdata[20]_i_3_n_0\
    );
\axi_rdata[20]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(20),
      I1 => slv_reg6(20),
      I2 => sel0(1),
      I3 => slv_reg5(20),
      I4 => \^axi_araddr_reg[5]_0\(0),
      I5 => slv_reg4(20),
      O => \axi_rdata[20]_i_4_n_0\
    );
\axi_rdata[21]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4540FFFF45404540"
    )
        port map (
      I0 => \^axi_araddr_reg[5]_0\(1),
      I1 => \axi_rdata[21]_i_2_n_0\,
      I2 => sel0(2),
      I3 => \axi_rdata[21]_i_3_n_0\,
      I4 => \axi_rdata[21]_i_4_n_0\,
      I5 => DATA_receive_ila(20),
      O => reg_data_out(21)
    );
\axi_rdata[21]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(21),
      I1 => slv_reg6(21),
      I2 => sel0(1),
      I3 => slv_reg5(21),
      I4 => \^axi_araddr_reg[5]_0\(0),
      I5 => slv_reg4(21),
      O => \axi_rdata[21]_i_2_n_0\
    );
\axi_rdata[21]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => \^data_send_ila\(21),
      I1 => slv_reg2(21),
      I2 => sel0(1),
      I3 => \^axi_araddr_reg[5]_0\(0),
      I4 => slv_reg1(21),
      O => \axi_rdata[21]_i_3_n_0\
    );
\axi_rdata[21]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFDF"
    )
        port map (
      I0 => \^axi_araddr_reg[5]_0\(0),
      I1 => sel0(1),
      I2 => \^axi_araddr_reg[5]_0\(1),
      I3 => sel0(2),
      O => \axi_rdata[21]_i_4_n_0\
    );
\axi_rdata[22]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFF8FFFFFFF80000"
    )
        port map (
      I0 => DATA_receive_ila(21),
      I1 => \^axi_araddr_reg[5]_0\(0),
      I2 => sel0(1),
      I3 => sel0(2),
      I4 => \^axi_araddr_reg[5]_0\(1),
      I5 => \axi_rdata_reg[22]_i_2_n_0\,
      O => reg_data_out(22)
    );
\axi_rdata[22]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFA0CFCF"
    )
        port map (
      I0 => \^data_send_ila\(22),
      I1 => slv_reg2(22),
      I2 => sel0(1),
      I3 => slv_reg1(22),
      I4 => \^axi_araddr_reg[5]_0\(0),
      O => \axi_rdata[22]_i_3_n_0\
    );
\axi_rdata[22]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(22),
      I1 => slv_reg6(22),
      I2 => sel0(1),
      I3 => slv_reg5(22),
      I4 => \^axi_araddr_reg[5]_0\(0),
      I5 => slv_reg4(22),
      O => \axi_rdata[22]_i_4_n_0\
    );
\axi_rdata[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFF8FFFFFFF80000"
    )
        port map (
      I0 => DATA_receive_ila(22),
      I1 => \^axi_araddr_reg[5]_0\(0),
      I2 => sel0(1),
      I3 => sel0(2),
      I4 => \^axi_araddr_reg[5]_0\(1),
      I5 => \axi_rdata_reg[23]_i_2_n_0\,
      O => reg_data_out(23)
    );
\axi_rdata[23]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFA0CFCF"
    )
        port map (
      I0 => \^data_send_ila\(23),
      I1 => slv_reg2(23),
      I2 => sel0(1),
      I3 => slv_reg1(23),
      I4 => \^axi_araddr_reg[5]_0\(0),
      O => \axi_rdata[23]_i_3_n_0\
    );
\axi_rdata[23]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(23),
      I1 => slv_reg6(23),
      I2 => sel0(1),
      I3 => slv_reg5(23),
      I4 => \^axi_araddr_reg[5]_0\(0),
      I5 => slv_reg4(23),
      O => \axi_rdata[23]_i_4_n_0\
    );
\axi_rdata[24]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00E2"
    )
        port map (
      I0 => \axi_rdata[24]_i_2_n_0\,
      I1 => sel0(2),
      I2 => \axi_rdata[24]_i_3_n_0\,
      I3 => \^axi_araddr_reg[5]_0\(1),
      O => reg_data_out(24)
    );
\axi_rdata[24]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => \^data_send_ila\(24),
      I1 => slv_reg2(24),
      I2 => sel0(1),
      I3 => \^axi_araddr_reg[5]_0\(0),
      I4 => slv_reg1(24),
      O => \axi_rdata[24]_i_2_n_0\
    );
\axi_rdata[24]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(24),
      I1 => slv_reg6(24),
      I2 => sel0(1),
      I3 => slv_reg5(24),
      I4 => \^axi_araddr_reg[5]_0\(0),
      I5 => slv_reg4(24),
      O => \axi_rdata[24]_i_3_n_0\
    );
\axi_rdata[25]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00E2"
    )
        port map (
      I0 => \axi_rdata[25]_i_2_n_0\,
      I1 => sel0(2),
      I2 => \axi_rdata[25]_i_3_n_0\,
      I3 => \^axi_araddr_reg[5]_0\(1),
      O => reg_data_out(25)
    );
\axi_rdata[25]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => \^data_send_ila\(25),
      I1 => slv_reg2(25),
      I2 => sel0(1),
      I3 => \^axi_araddr_reg[5]_0\(0),
      I4 => slv_reg1(25),
      O => \axi_rdata[25]_i_2_n_0\
    );
\axi_rdata[25]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(25),
      I1 => slv_reg6(25),
      I2 => sel0(1),
      I3 => slv_reg5(25),
      I4 => \^axi_araddr_reg[5]_0\(0),
      I5 => slv_reg4(25),
      O => \axi_rdata[25]_i_3_n_0\
    );
\axi_rdata[26]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FCBBFC88"
    )
        port map (
      I0 => sel0(1),
      I1 => \^axi_araddr_reg[5]_0\(1),
      I2 => \axi_rdata[26]_i_2_n_0\,
      I3 => sel0(2),
      I4 => \axi_rdata[26]_i_3_n_0\,
      O => reg_data_out(26)
    );
\axi_rdata[26]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(26),
      I1 => slv_reg6(26),
      I2 => sel0(1),
      I3 => slv_reg5(26),
      I4 => \^axi_araddr_reg[5]_0\(0),
      I5 => slv_reg4(26),
      O => \axi_rdata[26]_i_2_n_0\
    );
\axi_rdata[26]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFA0CFCF"
    )
        port map (
      I0 => \^data_send_ila\(26),
      I1 => slv_reg2(26),
      I2 => sel0(1),
      I3 => slv_reg1(26),
      I4 => \^axi_araddr_reg[5]_0\(0),
      O => \axi_rdata[26]_i_3_n_0\
    );
\axi_rdata[27]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FCBBFC88"
    )
        port map (
      I0 => sel0(1),
      I1 => \^axi_araddr_reg[5]_0\(1),
      I2 => \axi_rdata[27]_i_2_n_0\,
      I3 => sel0(2),
      I4 => \axi_rdata[27]_i_3_n_0\,
      O => reg_data_out(27)
    );
\axi_rdata[27]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(27),
      I1 => slv_reg6(27),
      I2 => sel0(1),
      I3 => slv_reg5(27),
      I4 => \^axi_araddr_reg[5]_0\(0),
      I5 => slv_reg4(27),
      O => \axi_rdata[27]_i_2_n_0\
    );
\axi_rdata[27]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFA0CFCF"
    )
        port map (
      I0 => \^data_send_ila\(27),
      I1 => slv_reg2(27),
      I2 => sel0(1),
      I3 => slv_reg1(27),
      I4 => \^axi_araddr_reg[5]_0\(0),
      O => \axi_rdata[27]_i_3_n_0\
    );
\axi_rdata[28]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00E2"
    )
        port map (
      I0 => \axi_rdata[28]_i_2_n_0\,
      I1 => sel0(2),
      I2 => \axi_rdata[28]_i_3_n_0\,
      I3 => \^axi_araddr_reg[5]_0\(1),
      O => reg_data_out(28)
    );
\axi_rdata[28]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => \^data_send_ila\(28),
      I1 => slv_reg2(28),
      I2 => sel0(1),
      I3 => \^axi_araddr_reg[5]_0\(0),
      I4 => slv_reg1(28),
      O => \axi_rdata[28]_i_2_n_0\
    );
\axi_rdata[28]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(28),
      I1 => slv_reg6(28),
      I2 => sel0(1),
      I3 => slv_reg5(28),
      I4 => \^axi_araddr_reg[5]_0\(0),
      I5 => slv_reg4(28),
      O => \axi_rdata[28]_i_3_n_0\
    );
\axi_rdata[29]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFBBFFBBFFB8FCB8"
    )
        port map (
      I0 => sel0(1),
      I1 => \^axi_araddr_reg[5]_0\(1),
      I2 => \axi_rdata[29]_i_2_n_0\,
      I3 => sel0(2),
      I4 => \axi_rdata[29]_i_3_n_0\,
      I5 => \axi_rdata[29]_i_4_n_0\,
      O => reg_data_out(29)
    );
\axi_rdata[29]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0000B800"
    )
        port map (
      I0 => \^data_send_ila\(29),
      I1 => \^axi_araddr_reg[5]_0\(0),
      I2 => slv_reg2(29),
      I3 => sel0(1),
      I4 => sel0(2),
      O => \axi_rdata[29]_i_2_n_0\
    );
\axi_rdata[29]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(29),
      I1 => slv_reg6(29),
      I2 => sel0(1),
      I3 => slv_reg5(29),
      I4 => \^axi_araddr_reg[5]_0\(0),
      I5 => slv_reg4(29),
      O => \axi_rdata[29]_i_3_n_0\
    );
\axi_rdata[29]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1011"
    )
        port map (
      I0 => sel0(1),
      I1 => sel0(2),
      I2 => slv_reg1(29),
      I3 => \^axi_araddr_reg[5]_0\(0),
      O => \axi_rdata[29]_i_4_n_0\
    );
\axi_rdata[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BAAAFFFFBAAA0000"
    )
        port map (
      I0 => sel0(2),
      I1 => sel0(1),
      I2 => \^axi_araddr_reg[5]_0\(0),
      I3 => DATA_receive_ila(1),
      I4 => \^axi_araddr_reg[5]_0\(1),
      I5 => \axi_rdata_reg[2]_i_2_n_0\,
      O => reg_data_out(2)
    );
\axi_rdata[2]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFA0CFCF"
    )
        port map (
      I0 => \^data_send_ila\(2),
      I1 => \^reg_address_receive_ila\(2),
      I2 => sel0(1),
      I3 => \^reg_address_send_ila\(2),
      I4 => \^axi_araddr_reg[5]_0\(0),
      O => \axi_rdata[2]_i_3_n_0\
    );
\axi_rdata[2]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^slv_reg6_reg[15]_0\(2),
      I2 => sel0(1),
      I3 => \^slv_reg5_reg[2]_0\(2),
      I4 => \^axi_araddr_reg[5]_0\(0),
      I5 => \^slv_reg4_reg[5]_0\(2),
      O => \axi_rdata[2]_i_4_n_0\
    );
\axi_rdata[30]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00E2"
    )
        port map (
      I0 => \axi_rdata[30]_i_2_n_0\,
      I1 => sel0(2),
      I2 => \axi_rdata[30]_i_3_n_0\,
      I3 => \^axi_araddr_reg[5]_0\(1),
      O => reg_data_out(30)
    );
\axi_rdata[30]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => \^data_send_ila\(30),
      I1 => slv_reg2(30),
      I2 => sel0(1),
      I3 => \^axi_araddr_reg[5]_0\(0),
      I4 => slv_reg1(30),
      O => \axi_rdata[30]_i_2_n_0\
    );
\axi_rdata[30]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(30),
      I1 => slv_reg6(30),
      I2 => sel0(1),
      I3 => slv_reg5(30),
      I4 => \^axi_araddr_reg[5]_0\(0),
      I5 => slv_reg4(30),
      O => \axi_rdata[30]_i_3_n_0\
    );
\axi_rdata[31]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FCBBFC88"
    )
        port map (
      I0 => sel0(1),
      I1 => \^axi_araddr_reg[5]_0\(1),
      I2 => \axi_rdata[31]_i_2_n_0\,
      I3 => sel0(2),
      I4 => \axi_rdata[31]_i_3_n_0\,
      O => reg_data_out(31)
    );
\axi_rdata[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => slv_reg7(31),
      I1 => slv_reg6(31),
      I2 => sel0(1),
      I3 => slv_reg5(31),
      I4 => \^axi_araddr_reg[5]_0\(0),
      I5 => slv_reg4(31),
      O => \axi_rdata[31]_i_2_n_0\
    );
\axi_rdata[31]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFA0CFCF"
    )
        port map (
      I0 => \^data_send_ila\(31),
      I1 => slv_reg2(31),
      I2 => sel0(1),
      I3 => slv_reg1(31),
      I4 => \^axi_araddr_reg[5]_0\(0),
      O => \axi_rdata[31]_i_3_n_0\
    );
\axi_rdata[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4F444F4F4F444444"
    )
        port map (
      I0 => \axi_rdata[21]_i_4_n_0\,
      I1 => DATA_receive_ila(2),
      I2 => \^axi_araddr_reg[5]_0\(1),
      I3 => \axi_rdata[3]_i_2_n_0\,
      I4 => sel0(2),
      I5 => \axi_rdata[3]_i_3_n_0\,
      O => reg_data_out(3)
    );
\axi_rdata[3]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^slv_reg6_reg[15]_0\(3),
      I2 => sel0(1),
      I3 => slv_reg5(3),
      I4 => \^axi_araddr_reg[5]_0\(0),
      I5 => \^slv_reg4_reg[5]_0\(3),
      O => \axi_rdata[3]_i_2_n_0\
    );
\axi_rdata[3]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFA0CFCF"
    )
        port map (
      I0 => \^data_send_ila\(3),
      I1 => \^reg_address_receive_ila\(3),
      I2 => sel0(1),
      I3 => \^reg_address_send_ila\(3),
      I4 => \^axi_araddr_reg[5]_0\(0),
      O => \axi_rdata[3]_i_3_n_0\
    );
\axi_rdata[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4F444F4F4F444444"
    )
        port map (
      I0 => \axi_rdata[21]_i_4_n_0\,
      I1 => DATA_receive_ila(3),
      I2 => \^axi_araddr_reg[5]_0\(1),
      I3 => \axi_rdata[4]_i_2_n_0\,
      I4 => sel0(2),
      I5 => \axi_rdata[4]_i_3_n_0\,
      O => reg_data_out(4)
    );
\axi_rdata[4]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q\(3),
      I1 => \^slv_reg6_reg[15]_0\(4),
      I2 => sel0(1),
      I3 => slv_reg5(4),
      I4 => \^axi_araddr_reg[5]_0\(0),
      I5 => \^slv_reg4_reg[5]_0\(4),
      O => \axi_rdata[4]_i_2_n_0\
    );
\axi_rdata[4]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFA0C0C0"
    )
        port map (
      I0 => \^data_send_ila\(4),
      I1 => \^reg_address_receive_ila\(4),
      I2 => sel0(1),
      I3 => \^reg_address_send_ila\(4),
      I4 => \^axi_araddr_reg[5]_0\(0),
      O => \axi_rdata[4]_i_3_n_0\
    );
\axi_rdata[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4540FFFF45404540"
    )
        port map (
      I0 => \^axi_araddr_reg[5]_0\(1),
      I1 => \axi_rdata[5]_i_2_n_0\,
      I2 => sel0(2),
      I3 => \axi_rdata[5]_i_3_n_0\,
      I4 => \axi_rdata[21]_i_4_n_0\,
      I5 => DATA_receive_ila(4),
      O => reg_data_out(5)
    );
\axi_rdata[5]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q\(4),
      I1 => \^slv_reg6_reg[15]_0\(5),
      I2 => sel0(1),
      I3 => slv_reg5(5),
      I4 => \^axi_araddr_reg[5]_0\(0),
      I5 => \^slv_reg4_reg[5]_0\(5),
      O => \axi_rdata[5]_i_2_n_0\
    );
\axi_rdata[5]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFA0CFCF"
    )
        port map (
      I0 => \^data_send_ila\(5),
      I1 => \^reg_address_receive_ila\(5),
      I2 => sel0(1),
      I3 => slv_reg1(5),
      I4 => \^axi_araddr_reg[5]_0\(0),
      O => \axi_rdata[5]_i_3_n_0\
    );
\axi_rdata[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4F444F4F4F444444"
    )
        port map (
      I0 => \axi_rdata[21]_i_4_n_0\,
      I1 => DATA_receive_ila(5),
      I2 => \^axi_araddr_reg[5]_0\(1),
      I3 => \axi_rdata[6]_i_2_n_0\,
      I4 => sel0(2),
      I5 => \axi_rdata[6]_i_3_n_0\,
      O => reg_data_out(6)
    );
\axi_rdata[6]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q\(5),
      I1 => \^slv_reg6_reg[15]_0\(6),
      I2 => sel0(1),
      I3 => slv_reg5(6),
      I4 => \^axi_araddr_reg[5]_0\(0),
      I5 => slv_reg4(6),
      O => \axi_rdata[6]_i_2_n_0\
    );
\axi_rdata[6]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFA0CFCF"
    )
        port map (
      I0 => \^data_send_ila\(6),
      I1 => \^reg_address_receive_ila\(6),
      I2 => sel0(1),
      I3 => slv_reg1(6),
      I4 => \^axi_araddr_reg[5]_0\(0),
      O => \axi_rdata[6]_i_3_n_0\
    );
\axi_rdata[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4F444F4F4F444444"
    )
        port map (
      I0 => \axi_rdata[21]_i_4_n_0\,
      I1 => DATA_receive_ila(6),
      I2 => \^axi_araddr_reg[5]_0\(1),
      I3 => \axi_rdata[7]_i_2_n_0\,
      I4 => sel0(2),
      I5 => \axi_rdata[7]_i_3_n_0\,
      O => reg_data_out(7)
    );
\axi_rdata[7]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q\(6),
      I1 => \^slv_reg6_reg[15]_0\(7),
      I2 => sel0(1),
      I3 => slv_reg5(7),
      I4 => \^axi_araddr_reg[5]_0\(0),
      I5 => slv_reg4(7),
      O => \axi_rdata[7]_i_2_n_0\
    );
\axi_rdata[7]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFA0CFCF"
    )
        port map (
      I0 => \^data_send_ila\(7),
      I1 => \^reg_address_receive_ila\(7),
      I2 => sel0(1),
      I3 => slv_reg1(7),
      I4 => \^axi_araddr_reg[5]_0\(0),
      O => \axi_rdata[7]_i_3_n_0\
    );
\axi_rdata[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4F444F4F4F444444"
    )
        port map (
      I0 => \axi_rdata[21]_i_4_n_0\,
      I1 => DATA_receive_ila(7),
      I2 => \^axi_araddr_reg[5]_0\(1),
      I3 => \axi_rdata[8]_i_2_n_0\,
      I4 => sel0(2),
      I5 => \axi_rdata[8]_i_3_n_0\,
      O => reg_data_out(8)
    );
\axi_rdata[8]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q\(7),
      I1 => \^slv_reg6_reg[15]_0\(8),
      I2 => sel0(1),
      I3 => slv_reg5(8),
      I4 => \^axi_araddr_reg[5]_0\(0),
      I5 => slv_reg4(8),
      O => \axi_rdata[8]_i_2_n_0\
    );
\axi_rdata[8]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFC0A0C0"
    )
        port map (
      I0 => \^data_send_ila\(8),
      I1 => slv_reg2(8),
      I2 => sel0(1),
      I3 => \^axi_araddr_reg[5]_0\(0),
      I4 => slv_reg1(8),
      O => \axi_rdata[8]_i_3_n_0\
    );
\axi_rdata[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4F444F4F4F444444"
    )
        port map (
      I0 => \axi_rdata[21]_i_4_n_0\,
      I1 => DATA_receive_ila(8),
      I2 => \^axi_araddr_reg[5]_0\(1),
      I3 => \axi_rdata[9]_i_2_n_0\,
      I4 => sel0(2),
      I5 => \axi_rdata[9]_i_3_n_0\,
      O => reg_data_out(9)
    );
\axi_rdata[9]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^q\(8),
      I1 => \^slv_reg6_reg[15]_0\(9),
      I2 => sel0(1),
      I3 => slv_reg5(9),
      I4 => \^axi_araddr_reg[5]_0\(0),
      I5 => slv_reg4(9),
      O => \axi_rdata[9]_i_2_n_0\
    );
\axi_rdata[9]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AFA0CFCF"
    )
        port map (
      I0 => \^data_send_ila\(9),
      I1 => slv_reg2(9),
      I2 => sel0(1),
      I3 => slv_reg1(9),
      I4 => \^axi_araddr_reg[5]_0\(0),
      O => \axi_rdata[9]_i_3_n_0\
    );
\axi_rdata_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(0),
      Q => s00_axi_rdata(0),
      R => \^sr\(0)
    );
\axi_rdata_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(10),
      Q => s00_axi_rdata(10),
      R => \^sr\(0)
    );
\axi_rdata_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(11),
      Q => s00_axi_rdata(11),
      R => \^sr\(0)
    );
\axi_rdata_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(12),
      Q => s00_axi_rdata(12),
      R => \^sr\(0)
    );
\axi_rdata_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(13),
      Q => s00_axi_rdata(13),
      R => \^sr\(0)
    );
\axi_rdata_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(14),
      Q => s00_axi_rdata(14),
      R => \^sr\(0)
    );
\axi_rdata_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(15),
      Q => s00_axi_rdata(15),
      R => \^sr\(0)
    );
\axi_rdata_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(16),
      Q => s00_axi_rdata(16),
      R => \^sr\(0)
    );
\axi_rdata_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(17),
      Q => s00_axi_rdata(17),
      R => \^sr\(0)
    );
\axi_rdata_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(18),
      Q => s00_axi_rdata(18),
      R => \^sr\(0)
    );
\axi_rdata_reg[18]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[18]_i_3_n_0\,
      I1 => \axi_rdata[18]_i_4_n_0\,
      O => \axi_rdata_reg[18]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(19),
      Q => s00_axi_rdata(19),
      R => \^sr\(0)
    );
\axi_rdata_reg[19]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[19]_i_3_n_0\,
      I1 => \axi_rdata[19]_i_4_n_0\,
      O => \axi_rdata_reg[19]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(1),
      Q => s00_axi_rdata(1),
      R => \^sr\(0)
    );
\axi_rdata_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(20),
      Q => s00_axi_rdata(20),
      R => \^sr\(0)
    );
\axi_rdata_reg[20]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[20]_i_3_n_0\,
      I1 => \axi_rdata[20]_i_4_n_0\,
      O => \axi_rdata_reg[20]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(21),
      Q => s00_axi_rdata(21),
      R => \^sr\(0)
    );
\axi_rdata_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(22),
      Q => s00_axi_rdata(22),
      R => \^sr\(0)
    );
\axi_rdata_reg[22]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[22]_i_3_n_0\,
      I1 => \axi_rdata[22]_i_4_n_0\,
      O => \axi_rdata_reg[22]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(23),
      Q => s00_axi_rdata(23),
      R => \^sr\(0)
    );
\axi_rdata_reg[23]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[23]_i_3_n_0\,
      I1 => \axi_rdata[23]_i_4_n_0\,
      O => \axi_rdata_reg[23]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(24),
      Q => s00_axi_rdata(24),
      R => \^sr\(0)
    );
\axi_rdata_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(25),
      Q => s00_axi_rdata(25),
      R => \^sr\(0)
    );
\axi_rdata_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(26),
      Q => s00_axi_rdata(26),
      R => \^sr\(0)
    );
\axi_rdata_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(27),
      Q => s00_axi_rdata(27),
      R => \^sr\(0)
    );
\axi_rdata_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(28),
      Q => s00_axi_rdata(28),
      R => \^sr\(0)
    );
\axi_rdata_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(29),
      Q => s00_axi_rdata(29),
      R => \^sr\(0)
    );
\axi_rdata_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(2),
      Q => s00_axi_rdata(2),
      R => \^sr\(0)
    );
\axi_rdata_reg[2]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \axi_rdata[2]_i_3_n_0\,
      I1 => \axi_rdata[2]_i_4_n_0\,
      O => \axi_rdata_reg[2]_i_2_n_0\,
      S => sel0(2)
    );
\axi_rdata_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(30),
      Q => s00_axi_rdata(30),
      R => \^sr\(0)
    );
\axi_rdata_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(31),
      Q => s00_axi_rdata(31),
      R => \^sr\(0)
    );
\axi_rdata_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(3),
      Q => s00_axi_rdata(3),
      R => \^sr\(0)
    );
\axi_rdata_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(4),
      Q => s00_axi_rdata(4),
      R => \^sr\(0)
    );
\axi_rdata_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(5),
      Q => s00_axi_rdata(5),
      R => \^sr\(0)
    );
\axi_rdata_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(6),
      Q => s00_axi_rdata(6),
      R => \^sr\(0)
    );
\axi_rdata_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(7),
      Q => s00_axi_rdata(7),
      R => \^sr\(0)
    );
\axi_rdata_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(8),
      Q => s00_axi_rdata(8),
      R => \^sr\(0)
    );
\axi_rdata_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg_rden__0\,
      D => reg_data_out(9),
      Q => s00_axi_rdata(9),
      R => \^sr\(0)
    );
axi_rvalid_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"08F8"
    )
        port map (
      I0 => \^s00_axi_arready\,
      I1 => s00_axi_arvalid,
      I2 => \^s00_axi_rvalid\,
      I3 => s00_axi_rready,
      O => axi_rvalid_i_1_n_0
    );
axi_rvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_rvalid_i_1_n_0,
      Q => \^s00_axi_rvalid\,
      R => \^sr\(0)
    );
axi_wready_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0800"
    )
        port map (
      I0 => s00_axi_awvalid,
      I1 => s00_axi_wvalid,
      I2 => \^s00_axi_wready\,
      I3 => aw_en_reg_n_0,
      O => axi_wready0
    );
axi_wready_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => axi_wready0,
      Q => \^s00_axi_wready\,
      R => \^sr\(0)
    );
enable_triger_CMD_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => slv_reg7(0),
      I1 => s00_axi_aresetn,
      O => enable_triger_CMD
    );
mosi_r_i_15: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^data_send_ila\(4),
      I1 => \^data_send_ila\(5),
      I2 => \out\(1),
      I3 => \^data_send_ila\(6),
      I4 => \out\(0),
      I5 => \^data_send_ila\(7),
      O => mosi_r_i_15_n_0
    );
mosi_r_i_16: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^data_send_ila\(0),
      I1 => \^data_send_ila\(1),
      I2 => \out\(1),
      I3 => \^data_send_ila\(2),
      I4 => \out\(0),
      I5 => \^data_send_ila\(3),
      O => mosi_r_i_16_n_0
    );
mosi_r_i_17: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^data_send_ila\(12),
      I1 => \^data_send_ila\(13),
      I2 => \out\(1),
      I3 => \^data_send_ila\(14),
      I4 => \out\(0),
      I5 => \^data_send_ila\(15),
      O => mosi_r_i_17_n_0
    );
mosi_r_i_18: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^data_send_ila\(8),
      I1 => \^data_send_ila\(9),
      I2 => \out\(1),
      I3 => \^data_send_ila\(10),
      I4 => \out\(0),
      I5 => \^data_send_ila\(11),
      O => mosi_r_i_18_n_0
    );
mosi_r_i_19: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^data_send_ila\(20),
      I1 => \^data_send_ila\(21),
      I2 => \out\(1),
      I3 => \^data_send_ila\(22),
      I4 => \out\(0),
      I5 => \^data_send_ila\(23),
      O => mosi_r_i_19_n_0
    );
mosi_r_i_20: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^data_send_ila\(16),
      I1 => \^data_send_ila\(17),
      I2 => \out\(1),
      I3 => \^data_send_ila\(18),
      I4 => \out\(0),
      I5 => \^data_send_ila\(19),
      O => mosi_r_i_20_n_0
    );
\mosi_r_i_2__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"88BB8B8B"
    )
        port map (
      I0 => mosi_r_i_7_n_0,
      I1 => mosi_r_reg_0(2),
      I2 => \^reg_address_receive_ila\(5),
      I3 => \^reg_address_receive_ila\(4),
      I4 => mosi_r_reg_0(0),
      O => \cnt_reg[2]\
    );
\mosi_r_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000474700FF"
    )
        port map (
      I0 => mosi_r_reg_i_10_n_0,
      I1 => \out\(3),
      I2 => mosi_r_reg_i_11_n_0,
      I3 => mosi_r_reg_i_12_n_0,
      I4 => \out\(4),
      I5 => mosi_r_reg,
      O => \cnt_reg[3]\
    );
mosi_r_i_7: unisim.vcomponents.LUT6
    generic map(
      INIT => X"505F3030505F3F3F"
    )
        port map (
      I0 => \^reg_address_receive_ila\(0),
      I1 => \^reg_address_receive_ila\(1),
      I2 => mosi_r_reg_0(1),
      I3 => \^reg_address_receive_ila\(2),
      I4 => mosi_r_reg_0(0),
      I5 => \^reg_address_receive_ila\(3),
      O => mosi_r_i_7_n_0
    );
\mosi_r_i_7__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => \^reg_address_send_ila\(0),
      I1 => \^reg_address_send_ila\(1),
      I2 => \out\(1),
      I3 => \^reg_address_send_ila\(2),
      I4 => \out\(0),
      I5 => \^reg_address_send_ila\(3),
      O => \slv_reg1_reg[0]_0\
    );
mosi_r_reg_i_10: unisim.vcomponents.MUXF7
     port map (
      I0 => mosi_r_i_15_n_0,
      I1 => mosi_r_i_16_n_0,
      O => mosi_r_reg_i_10_n_0,
      S => \out\(2)
    );
mosi_r_reg_i_11: unisim.vcomponents.MUXF7
     port map (
      I0 => mosi_r_i_17_n_0,
      I1 => mosi_r_i_18_n_0,
      O => mosi_r_reg_i_11_n_0,
      S => \out\(2)
    );
mosi_r_reg_i_12: unisim.vcomponents.MUXF7
     port map (
      I0 => mosi_r_i_19_n_0,
      I1 => mosi_r_i_20_n_0,
      O => mosi_r_reg_i_12_n_0,
      S => \out\(2)
    );
\slv_reg0[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s00_axi_wdata(0),
      I1 => \slv_reg0[1]_i_2_n_0\,
      I2 => \^slv_reg0\(0),
      O => \slv_reg0[0]_i_1_n_0\
    );
\slv_reg0[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s00_axi_wdata(1),
      I1 => \slv_reg0[1]_i_2_n_0\,
      I2 => \^slv_reg0\(1),
      O => \slv_reg0[1]_i_1_n_0\
    );
\slv_reg0[1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000200000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => p_0_in(0),
      I2 => p_0_in(3),
      I3 => p_0_in(1),
      I4 => p_0_in(2),
      I5 => s00_axi_wstrb(0),
      O => \slv_reg0[1]_i_2_n_0\
    );
\slv_reg0_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \slv_reg0[0]_i_1_n_0\,
      Q => \^slv_reg0\(0),
      R => \^sr\(0)
    );
\slv_reg0_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => \slv_reg0[1]_i_1_n_0\,
      Q => \^slv_reg0\(1),
      R => \^sr\(0)
    );
\slv_reg1[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => s00_axi_wstrb(1),
      I2 => p_0_in(3),
      I3 => p_0_in(1),
      I4 => p_0_in(2),
      I5 => p_0_in(0),
      O => p_1_in(15)
    );
\slv_reg1[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => s00_axi_wstrb(2),
      I2 => p_0_in(3),
      I3 => p_0_in(1),
      I4 => p_0_in(2),
      I5 => p_0_in(0),
      O => p_1_in(23)
    );
\slv_reg1[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => s00_axi_wstrb(3),
      I2 => p_0_in(3),
      I3 => p_0_in(1),
      I4 => p_0_in(2),
      I5 => p_0_in(0),
      O => p_1_in(31)
    );
\slv_reg1[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => s00_axi_wstrb(0),
      I2 => p_0_in(3),
      I3 => p_0_in(1),
      I4 => p_0_in(2),
      I5 => p_0_in(0),
      O => p_1_in(4)
    );
\slv_reg1_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(4),
      D => s00_axi_wdata(0),
      Q => \^reg_address_send_ila\(0),
      R => \^sr\(0)
    );
\slv_reg1_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(10),
      Q => slv_reg1(10),
      R => \^sr\(0)
    );
\slv_reg1_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(11),
      Q => slv_reg1(11),
      R => \^sr\(0)
    );
\slv_reg1_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(12),
      Q => slv_reg1(12),
      R => \^sr\(0)
    );
\slv_reg1_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(13),
      Q => slv_reg1(13),
      R => \^sr\(0)
    );
\slv_reg1_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(14),
      Q => slv_reg1(14),
      R => \^sr\(0)
    );
\slv_reg1_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(15),
      Q => slv_reg1(15),
      R => \^sr\(0)
    );
\slv_reg1_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(16),
      Q => slv_reg1(16),
      R => \^sr\(0)
    );
\slv_reg1_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(17),
      Q => slv_reg1(17),
      R => \^sr\(0)
    );
\slv_reg1_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(18),
      Q => slv_reg1(18),
      R => \^sr\(0)
    );
\slv_reg1_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(19),
      Q => slv_reg1(19),
      R => \^sr\(0)
    );
\slv_reg1_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(4),
      D => s00_axi_wdata(1),
      Q => \^reg_address_send_ila\(1),
      R => \^sr\(0)
    );
\slv_reg1_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(20),
      Q => slv_reg1(20),
      R => \^sr\(0)
    );
\slv_reg1_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(21),
      Q => slv_reg1(21),
      R => \^sr\(0)
    );
\slv_reg1_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(22),
      Q => slv_reg1(22),
      R => \^sr\(0)
    );
\slv_reg1_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(23),
      D => s00_axi_wdata(23),
      Q => slv_reg1(23),
      R => \^sr\(0)
    );
\slv_reg1_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(24),
      Q => slv_reg1(24),
      R => \^sr\(0)
    );
\slv_reg1_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(25),
      Q => slv_reg1(25),
      R => \^sr\(0)
    );
\slv_reg1_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(26),
      Q => slv_reg1(26),
      R => \^sr\(0)
    );
\slv_reg1_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(27),
      Q => slv_reg1(27),
      R => \^sr\(0)
    );
\slv_reg1_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(28),
      Q => slv_reg1(28),
      R => \^sr\(0)
    );
\slv_reg1_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(29),
      Q => slv_reg1(29),
      R => \^sr\(0)
    );
\slv_reg1_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(4),
      D => s00_axi_wdata(2),
      Q => \^reg_address_send_ila\(2),
      R => \^sr\(0)
    );
\slv_reg1_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(30),
      Q => slv_reg1(30),
      R => \^sr\(0)
    );
\slv_reg1_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(31),
      D => s00_axi_wdata(31),
      Q => slv_reg1(31),
      R => \^sr\(0)
    );
\slv_reg1_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(4),
      D => s00_axi_wdata(3),
      Q => \^reg_address_send_ila\(3),
      R => \^sr\(0)
    );
\slv_reg1_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(4),
      D => s00_axi_wdata(4),
      Q => \^reg_address_send_ila\(4),
      R => \^sr\(0)
    );
\slv_reg1_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(4),
      D => s00_axi_wdata(5),
      Q => slv_reg1(5),
      R => \^sr\(0)
    );
\slv_reg1_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(4),
      D => s00_axi_wdata(6),
      Q => slv_reg1(6),
      R => \^sr\(0)
    );
\slv_reg1_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(4),
      D => s00_axi_wdata(7),
      Q => slv_reg1(7),
      R => \^sr\(0)
    );
\slv_reg1_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(8),
      Q => slv_reg1(8),
      R => \^sr\(0)
    );
\slv_reg1_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => p_1_in(15),
      D => s00_axi_wdata(9),
      Q => slv_reg1(9),
      R => \^sr\(0)
    );
\slv_reg2[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => s00_axi_wstrb(1),
      I2 => p_0_in(3),
      I3 => p_0_in(0),
      I4 => p_0_in(2),
      I5 => p_0_in(1),
      O => \slv_reg2[15]_i_1_n_0\
    );
\slv_reg2[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => s00_axi_wstrb(2),
      I2 => p_0_in(3),
      I3 => p_0_in(0),
      I4 => p_0_in(2),
      I5 => p_0_in(1),
      O => \slv_reg2[23]_i_1_n_0\
    );
\slv_reg2[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => s00_axi_wstrb(3),
      I2 => p_0_in(3),
      I3 => p_0_in(0),
      I4 => p_0_in(2),
      I5 => p_0_in(1),
      O => \slv_reg2[31]_i_1_n_0\
    );
\slv_reg2[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => s00_axi_wstrb(0),
      I2 => p_0_in(3),
      I3 => p_0_in(0),
      I4 => p_0_in(2),
      I5 => p_0_in(1),
      O => \slv_reg2[7]_i_1_n_0\
    );
\slv_reg2_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => \^reg_address_receive_ila\(0),
      R => \^sr\(0)
    );
\slv_reg2_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg2(10),
      R => \^sr\(0)
    );
\slv_reg2_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg2(11),
      R => \^sr\(0)
    );
\slv_reg2_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg2(12),
      R => \^sr\(0)
    );
\slv_reg2_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg2(13),
      R => \^sr\(0)
    );
\slv_reg2_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg2(14),
      R => \^sr\(0)
    );
\slv_reg2_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg2(15),
      R => \^sr\(0)
    );
\slv_reg2_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg2(16),
      R => \^sr\(0)
    );
\slv_reg2_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg2(17),
      R => \^sr\(0)
    );
\slv_reg2_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg2(18),
      R => \^sr\(0)
    );
\slv_reg2_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg2(19),
      R => \^sr\(0)
    );
\slv_reg2_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => \^reg_address_receive_ila\(1),
      R => \^sr\(0)
    );
\slv_reg2_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg2(20),
      R => \^sr\(0)
    );
\slv_reg2_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg2(21),
      R => \^sr\(0)
    );
\slv_reg2_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg2(22),
      R => \^sr\(0)
    );
\slv_reg2_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg2(23),
      R => \^sr\(0)
    );
\slv_reg2_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg2(24),
      R => \^sr\(0)
    );
\slv_reg2_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg2(25),
      R => \^sr\(0)
    );
\slv_reg2_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg2(26),
      R => \^sr\(0)
    );
\slv_reg2_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg2(27),
      R => \^sr\(0)
    );
\slv_reg2_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg2(28),
      R => \^sr\(0)
    );
\slv_reg2_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg2(29),
      R => \^sr\(0)
    );
\slv_reg2_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => \^reg_address_receive_ila\(2),
      R => \^sr\(0)
    );
\slv_reg2_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg2(30),
      R => \^sr\(0)
    );
\slv_reg2_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg2(31),
      R => \^sr\(0)
    );
\slv_reg2_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => \^reg_address_receive_ila\(3),
      R => \^sr\(0)
    );
\slv_reg2_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => \^reg_address_receive_ila\(4),
      R => \^sr\(0)
    );
\slv_reg2_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => \^reg_address_receive_ila\(5),
      R => \^sr\(0)
    );
\slv_reg2_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => \^reg_address_receive_ila\(6),
      R => \^sr\(0)
    );
\slv_reg2_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => \^reg_address_receive_ila\(7),
      R => \^sr\(0)
    );
\slv_reg2_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg2(8),
      R => \^sr\(0)
    );
\slv_reg2_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg2[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg2(9),
      R => \^sr\(0)
    );
\slv_reg3[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => s00_axi_wstrb(1),
      I2 => p_0_in(0),
      I3 => p_0_in(1),
      I4 => p_0_in(2),
      I5 => p_0_in(3),
      O => \slv_reg3[15]_i_1_n_0\
    );
\slv_reg3[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => s00_axi_wstrb(2),
      I2 => p_0_in(0),
      I3 => p_0_in(1),
      I4 => p_0_in(2),
      I5 => p_0_in(3),
      O => \slv_reg3[23]_i_1_n_0\
    );
\slv_reg3[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => s00_axi_wstrb(3),
      I2 => p_0_in(0),
      I3 => p_0_in(1),
      I4 => p_0_in(2),
      I5 => p_0_in(3),
      O => \slv_reg3[31]_i_1_n_0\
    );
\slv_reg3[31]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => \^s00_axi_wready\,
      I1 => \^s00_axi_awready\,
      I2 => s00_axi_awvalid,
      I3 => s00_axi_wvalid,
      O => \slv_reg_wren__0\
    );
\slv_reg3[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => s00_axi_wstrb(0),
      I2 => p_0_in(0),
      I3 => p_0_in(1),
      I4 => p_0_in(2),
      I5 => p_0_in(3),
      O => \slv_reg3[7]_i_1_n_0\
    );
\slv_reg3_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => \^data_send_ila\(0),
      R => \^sr\(0)
    );
\slv_reg3_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => \^data_send_ila\(10),
      R => \^sr\(0)
    );
\slv_reg3_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => \^data_send_ila\(11),
      R => \^sr\(0)
    );
\slv_reg3_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => \^data_send_ila\(12),
      R => \^sr\(0)
    );
\slv_reg3_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => \^data_send_ila\(13),
      R => \^sr\(0)
    );
\slv_reg3_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => \^data_send_ila\(14),
      R => \^sr\(0)
    );
\slv_reg3_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => \^data_send_ila\(15),
      R => \^sr\(0)
    );
\slv_reg3_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => \^data_send_ila\(16),
      R => \^sr\(0)
    );
\slv_reg3_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => \^data_send_ila\(17),
      R => \^sr\(0)
    );
\slv_reg3_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => \^data_send_ila\(18),
      R => \^sr\(0)
    );
\slv_reg3_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => \^data_send_ila\(19),
      R => \^sr\(0)
    );
\slv_reg3_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => \^data_send_ila\(1),
      R => \^sr\(0)
    );
\slv_reg3_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => \^data_send_ila\(20),
      R => \^sr\(0)
    );
\slv_reg3_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => \^data_send_ila\(21),
      R => \^sr\(0)
    );
\slv_reg3_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => \^data_send_ila\(22),
      R => \^sr\(0)
    );
\slv_reg3_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => \^data_send_ila\(23),
      R => \^sr\(0)
    );
\slv_reg3_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => \^data_send_ila\(24),
      R => \^sr\(0)
    );
\slv_reg3_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => \^data_send_ila\(25),
      R => \^sr\(0)
    );
\slv_reg3_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => \^data_send_ila\(26),
      R => \^sr\(0)
    );
\slv_reg3_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => \^data_send_ila\(27),
      R => \^sr\(0)
    );
\slv_reg3_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => \^data_send_ila\(28),
      R => \^sr\(0)
    );
\slv_reg3_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => \^data_send_ila\(29),
      R => \^sr\(0)
    );
\slv_reg3_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => \^data_send_ila\(2),
      R => \^sr\(0)
    );
\slv_reg3_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => \^data_send_ila\(30),
      R => \^sr\(0)
    );
\slv_reg3_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => \^data_send_ila\(31),
      R => \^sr\(0)
    );
\slv_reg3_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => \^data_send_ila\(3),
      R => \^sr\(0)
    );
\slv_reg3_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => \^data_send_ila\(4),
      R => \^sr\(0)
    );
\slv_reg3_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => \^data_send_ila\(5),
      R => \^sr\(0)
    );
\slv_reg3_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => \^data_send_ila\(6),
      R => \^sr\(0)
    );
\slv_reg3_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => \^data_send_ila\(7),
      R => \^sr\(0)
    );
\slv_reg3_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => \^data_send_ila\(8),
      R => \^sr\(0)
    );
\slv_reg3_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg3[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => \^data_send_ila\(9),
      R => \^sr\(0)
    );
\slv_reg4[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => s00_axi_wstrb(1),
      I2 => p_0_in(3),
      I3 => p_0_in(1),
      I4 => p_0_in(0),
      I5 => p_0_in(2),
      O => \slv_reg4[15]_i_1_n_0\
    );
\slv_reg4[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => s00_axi_wstrb(2),
      I2 => p_0_in(3),
      I3 => p_0_in(1),
      I4 => p_0_in(0),
      I5 => p_0_in(2),
      O => \slv_reg4[23]_i_1_n_0\
    );
\slv_reg4[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => s00_axi_wstrb(3),
      I2 => p_0_in(3),
      I3 => p_0_in(1),
      I4 => p_0_in(0),
      I5 => p_0_in(2),
      O => \slv_reg4[31]_i_1_n_0\
    );
\slv_reg4[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000800000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => s00_axi_wstrb(0),
      I2 => p_0_in(3),
      I3 => p_0_in(1),
      I4 => p_0_in(0),
      I5 => p_0_in(2),
      O => \slv_reg4[7]_i_1_n_0\
    );
\slv_reg4_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => \^slv_reg4_reg[5]_0\(0),
      R => \^sr\(0)
    );
\slv_reg4_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg4(10),
      R => \^sr\(0)
    );
\slv_reg4_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg4(11),
      R => \^sr\(0)
    );
\slv_reg4_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg4(12),
      R => \^sr\(0)
    );
\slv_reg4_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg4(13),
      R => \^sr\(0)
    );
\slv_reg4_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg4(14),
      R => \^sr\(0)
    );
\slv_reg4_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg4(15),
      R => \^sr\(0)
    );
\slv_reg4_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg4(16),
      R => \^sr\(0)
    );
\slv_reg4_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg4(17),
      R => \^sr\(0)
    );
\slv_reg4_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg4(18),
      R => \^sr\(0)
    );
\slv_reg4_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg4(19),
      R => \^sr\(0)
    );
\slv_reg4_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => \^slv_reg4_reg[5]_0\(1),
      R => \^sr\(0)
    );
\slv_reg4_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg4(20),
      R => \^sr\(0)
    );
\slv_reg4_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg4(21),
      R => \^sr\(0)
    );
\slv_reg4_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg4(22),
      R => \^sr\(0)
    );
\slv_reg4_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg4(23),
      R => \^sr\(0)
    );
\slv_reg4_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg4(24),
      R => \^sr\(0)
    );
\slv_reg4_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg4(25),
      R => \^sr\(0)
    );
\slv_reg4_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg4(26),
      R => \^sr\(0)
    );
\slv_reg4_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg4(27),
      R => \^sr\(0)
    );
\slv_reg4_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg4(28),
      R => \^sr\(0)
    );
\slv_reg4_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg4(29),
      R => \^sr\(0)
    );
\slv_reg4_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => \^slv_reg4_reg[5]_0\(2),
      R => \^sr\(0)
    );
\slv_reg4_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg4(30),
      R => \^sr\(0)
    );
\slv_reg4_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg4(31),
      R => \^sr\(0)
    );
\slv_reg4_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => \^slv_reg4_reg[5]_0\(3),
      R => \^sr\(0)
    );
\slv_reg4_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => \^slv_reg4_reg[5]_0\(4),
      R => \^sr\(0)
    );
\slv_reg4_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => \^slv_reg4_reg[5]_0\(5),
      R => \^sr\(0)
    );
\slv_reg4_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg4(6),
      R => \^sr\(0)
    );
\slv_reg4_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg4(7),
      R => \^sr\(0)
    );
\slv_reg4_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg4(8),
      R => \^sr\(0)
    );
\slv_reg4_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg4[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg4(9),
      R => \^sr\(0)
    );
\slv_reg5[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => s00_axi_wstrb(1),
      I2 => p_0_in(0),
      I3 => p_0_in(2),
      I4 => p_0_in(1),
      I5 => p_0_in(3),
      O => \slv_reg5[15]_i_1_n_0\
    );
\slv_reg5[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => s00_axi_wstrb(2),
      I2 => p_0_in(0),
      I3 => p_0_in(2),
      I4 => p_0_in(1),
      I5 => p_0_in(3),
      O => \slv_reg5[23]_i_1_n_0\
    );
\slv_reg5[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => s00_axi_wstrb(3),
      I2 => p_0_in(0),
      I3 => p_0_in(2),
      I4 => p_0_in(1),
      I5 => p_0_in(3),
      O => \slv_reg5[31]_i_1_n_0\
    );
\slv_reg5[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => s00_axi_wstrb(0),
      I2 => p_0_in(0),
      I3 => p_0_in(2),
      I4 => p_0_in(1),
      I5 => p_0_in(3),
      O => \slv_reg5[7]_i_1_n_0\
    );
\slv_reg5_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => \^slv_reg5_reg[2]_0\(0),
      R => \^sr\(0)
    );
\slv_reg5_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => slv_reg5(10),
      R => \^sr\(0)
    );
\slv_reg5_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => slv_reg5(11),
      R => \^sr\(0)
    );
\slv_reg5_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => slv_reg5(12),
      R => \^sr\(0)
    );
\slv_reg5_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => slv_reg5(13),
      R => \^sr\(0)
    );
\slv_reg5_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => slv_reg5(14),
      R => \^sr\(0)
    );
\slv_reg5_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => slv_reg5(15),
      R => \^sr\(0)
    );
\slv_reg5_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg5(16),
      R => \^sr\(0)
    );
\slv_reg5_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg5(17),
      R => \^sr\(0)
    );
\slv_reg5_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg5(18),
      R => \^sr\(0)
    );
\slv_reg5_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg5(19),
      R => \^sr\(0)
    );
\slv_reg5_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => \^slv_reg5_reg[2]_0\(1),
      R => \^sr\(0)
    );
\slv_reg5_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg5(20),
      R => \^sr\(0)
    );
\slv_reg5_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg5(21),
      R => \^sr\(0)
    );
\slv_reg5_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg5(22),
      R => \^sr\(0)
    );
\slv_reg5_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg5(23),
      R => \^sr\(0)
    );
\slv_reg5_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg5(24),
      R => \^sr\(0)
    );
\slv_reg5_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg5(25),
      R => \^sr\(0)
    );
\slv_reg5_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg5(26),
      R => \^sr\(0)
    );
\slv_reg5_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg5(27),
      R => \^sr\(0)
    );
\slv_reg5_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg5(28),
      R => \^sr\(0)
    );
\slv_reg5_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg5(29),
      R => \^sr\(0)
    );
\slv_reg5_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => \^slv_reg5_reg[2]_0\(2),
      R => \^sr\(0)
    );
\slv_reg5_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg5(30),
      R => \^sr\(0)
    );
\slv_reg5_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg5(31),
      R => \^sr\(0)
    );
\slv_reg5_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => slv_reg5(3),
      R => \^sr\(0)
    );
\slv_reg5_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => slv_reg5(4),
      R => \^sr\(0)
    );
\slv_reg5_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => slv_reg5(5),
      R => \^sr\(0)
    );
\slv_reg5_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => slv_reg5(6),
      R => \^sr\(0)
    );
\slv_reg5_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => slv_reg5(7),
      R => \^sr\(0)
    );
\slv_reg5_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => slv_reg5(8),
      R => \^sr\(0)
    );
\slv_reg5_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg5[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => slv_reg5(9),
      R => \^sr\(0)
    );
\slv_reg6[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => s00_axi_wstrb(1),
      I2 => p_0_in(2),
      I3 => p_0_in(1),
      I4 => p_0_in(0),
      I5 => p_0_in(3),
      O => \slv_reg6[15]_i_1_n_0\
    );
\slv_reg6[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => s00_axi_wstrb(2),
      I2 => p_0_in(2),
      I3 => p_0_in(1),
      I4 => p_0_in(0),
      I5 => p_0_in(3),
      O => \slv_reg6[23]_i_1_n_0\
    );
\slv_reg6[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => s00_axi_wstrb(3),
      I2 => p_0_in(2),
      I3 => p_0_in(1),
      I4 => p_0_in(0),
      I5 => p_0_in(3),
      O => \slv_reg6[31]_i_1_n_0\
    );
\slv_reg6[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000008000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => s00_axi_wstrb(0),
      I2 => p_0_in(2),
      I3 => p_0_in(1),
      I4 => p_0_in(0),
      I5 => p_0_in(3),
      O => \slv_reg6[7]_i_1_n_0\
    );
\slv_reg6_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => \^slv_reg6_reg[15]_0\(0),
      R => \^sr\(0)
    );
\slv_reg6_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => \^slv_reg6_reg[15]_0\(10),
      R => \^sr\(0)
    );
\slv_reg6_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => \^slv_reg6_reg[15]_0\(11),
      R => \^sr\(0)
    );
\slv_reg6_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => \^slv_reg6_reg[15]_0\(12),
      R => \^sr\(0)
    );
\slv_reg6_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => \^slv_reg6_reg[15]_0\(13),
      R => \^sr\(0)
    );
\slv_reg6_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => \^slv_reg6_reg[15]_0\(14),
      R => \^sr\(0)
    );
\slv_reg6_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => \^slv_reg6_reg[15]_0\(15),
      R => \^sr\(0)
    );
\slv_reg6_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => slv_reg6(16),
      R => \^sr\(0)
    );
\slv_reg6_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg6(17),
      R => \^sr\(0)
    );
\slv_reg6_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg6(18),
      R => \^sr\(0)
    );
\slv_reg6_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg6(19),
      R => \^sr\(0)
    );
\slv_reg6_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => \^slv_reg6_reg[15]_0\(1),
      R => \^sr\(0)
    );
\slv_reg6_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg6(20),
      R => \^sr\(0)
    );
\slv_reg6_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg6(21),
      R => \^sr\(0)
    );
\slv_reg6_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg6(22),
      R => \^sr\(0)
    );
\slv_reg6_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg6(23),
      R => \^sr\(0)
    );
\slv_reg6_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg6(24),
      R => \^sr\(0)
    );
\slv_reg6_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg6(25),
      R => \^sr\(0)
    );
\slv_reg6_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg6(26),
      R => \^sr\(0)
    );
\slv_reg6_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg6(27),
      R => \^sr\(0)
    );
\slv_reg6_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg6(28),
      R => \^sr\(0)
    );
\slv_reg6_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg6(29),
      R => \^sr\(0)
    );
\slv_reg6_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => \^slv_reg6_reg[15]_0\(2),
      R => \^sr\(0)
    );
\slv_reg6_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg6(30),
      R => \^sr\(0)
    );
\slv_reg6_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg6(31),
      R => \^sr\(0)
    );
\slv_reg6_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => \^slv_reg6_reg[15]_0\(3),
      R => \^sr\(0)
    );
\slv_reg6_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => \^slv_reg6_reg[15]_0\(4),
      R => \^sr\(0)
    );
\slv_reg6_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => \^slv_reg6_reg[15]_0\(5),
      R => \^sr\(0)
    );
\slv_reg6_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => \^slv_reg6_reg[15]_0\(6),
      R => \^sr\(0)
    );
\slv_reg6_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => \^slv_reg6_reg[15]_0\(7),
      R => \^sr\(0)
    );
\slv_reg6_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => \^slv_reg6_reg[15]_0\(8),
      R => \^sr\(0)
    );
\slv_reg6_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg6[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => \^slv_reg6_reg[15]_0\(9),
      R => \^sr\(0)
    );
\slv_reg7[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000080000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => p_0_in(2),
      I2 => s00_axi_wstrb(1),
      I3 => p_0_in(0),
      I4 => p_0_in(1),
      I5 => p_0_in(3),
      O => \slv_reg7[15]_i_1_n_0\
    );
\slv_reg7[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000080000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => p_0_in(2),
      I2 => s00_axi_wstrb(2),
      I3 => p_0_in(0),
      I4 => p_0_in(1),
      I5 => p_0_in(3),
      O => \slv_reg7[23]_i_1_n_0\
    );
\slv_reg7[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000080000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => p_0_in(2),
      I2 => s00_axi_wstrb(3),
      I3 => p_0_in(0),
      I4 => p_0_in(1),
      I5 => p_0_in(3),
      O => \slv_reg7[31]_i_1_n_0\
    );
\slv_reg7[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000080000000"
    )
        port map (
      I0 => \slv_reg_wren__0\,
      I1 => p_0_in(2),
      I2 => s00_axi_wstrb(0),
      I3 => p_0_in(0),
      I4 => p_0_in(1),
      I5 => p_0_in(3),
      O => \slv_reg7[7]_i_1_n_0\
    );
\slv_reg7_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[7]_i_1_n_0\,
      D => s00_axi_wdata(0),
      Q => slv_reg7(0),
      R => \^sr\(0)
    );
\slv_reg7_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[15]_i_1_n_0\,
      D => s00_axi_wdata(10),
      Q => \^q\(9),
      R => \^sr\(0)
    );
\slv_reg7_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[15]_i_1_n_0\,
      D => s00_axi_wdata(11),
      Q => \^q\(10),
      R => \^sr\(0)
    );
\slv_reg7_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[15]_i_1_n_0\,
      D => s00_axi_wdata(12),
      Q => \^q\(11),
      R => \^sr\(0)
    );
\slv_reg7_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[15]_i_1_n_0\,
      D => s00_axi_wdata(13),
      Q => \^q\(12),
      R => \^sr\(0)
    );
\slv_reg7_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[15]_i_1_n_0\,
      D => s00_axi_wdata(14),
      Q => \^q\(13),
      R => \^sr\(0)
    );
\slv_reg7_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[15]_i_1_n_0\,
      D => s00_axi_wdata(15),
      Q => \^q\(14),
      R => \^sr\(0)
    );
\slv_reg7_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[23]_i_1_n_0\,
      D => s00_axi_wdata(16),
      Q => \^q\(15),
      R => \^sr\(0)
    );
\slv_reg7_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[23]_i_1_n_0\,
      D => s00_axi_wdata(17),
      Q => slv_reg7(17),
      R => \^sr\(0)
    );
\slv_reg7_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[23]_i_1_n_0\,
      D => s00_axi_wdata(18),
      Q => slv_reg7(18),
      R => \^sr\(0)
    );
\slv_reg7_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[23]_i_1_n_0\,
      D => s00_axi_wdata(19),
      Q => slv_reg7(19),
      R => \^sr\(0)
    );
\slv_reg7_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[7]_i_1_n_0\,
      D => s00_axi_wdata(1),
      Q => \^q\(0),
      R => \^sr\(0)
    );
\slv_reg7_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[23]_i_1_n_0\,
      D => s00_axi_wdata(20),
      Q => slv_reg7(20),
      R => \^sr\(0)
    );
\slv_reg7_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[23]_i_1_n_0\,
      D => s00_axi_wdata(21),
      Q => slv_reg7(21),
      R => \^sr\(0)
    );
\slv_reg7_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[23]_i_1_n_0\,
      D => s00_axi_wdata(22),
      Q => slv_reg7(22),
      R => \^sr\(0)
    );
\slv_reg7_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[23]_i_1_n_0\,
      D => s00_axi_wdata(23),
      Q => slv_reg7(23),
      R => \^sr\(0)
    );
\slv_reg7_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[31]_i_1_n_0\,
      D => s00_axi_wdata(24),
      Q => slv_reg7(24),
      R => \^sr\(0)
    );
\slv_reg7_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[31]_i_1_n_0\,
      D => s00_axi_wdata(25),
      Q => slv_reg7(25),
      R => \^sr\(0)
    );
\slv_reg7_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[31]_i_1_n_0\,
      D => s00_axi_wdata(26),
      Q => slv_reg7(26),
      R => \^sr\(0)
    );
\slv_reg7_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[31]_i_1_n_0\,
      D => s00_axi_wdata(27),
      Q => slv_reg7(27),
      R => \^sr\(0)
    );
\slv_reg7_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[31]_i_1_n_0\,
      D => s00_axi_wdata(28),
      Q => slv_reg7(28),
      R => \^sr\(0)
    );
\slv_reg7_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[31]_i_1_n_0\,
      D => s00_axi_wdata(29),
      Q => slv_reg7(29),
      R => \^sr\(0)
    );
\slv_reg7_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[7]_i_1_n_0\,
      D => s00_axi_wdata(2),
      Q => \^q\(1),
      R => \^sr\(0)
    );
\slv_reg7_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[31]_i_1_n_0\,
      D => s00_axi_wdata(30),
      Q => slv_reg7(30),
      R => \^sr\(0)
    );
\slv_reg7_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[31]_i_1_n_0\,
      D => s00_axi_wdata(31),
      Q => slv_reg7(31),
      R => \^sr\(0)
    );
\slv_reg7_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[7]_i_1_n_0\,
      D => s00_axi_wdata(3),
      Q => \^q\(2),
      R => \^sr\(0)
    );
\slv_reg7_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[7]_i_1_n_0\,
      D => s00_axi_wdata(4),
      Q => \^q\(3),
      R => \^sr\(0)
    );
\slv_reg7_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[7]_i_1_n_0\,
      D => s00_axi_wdata(5),
      Q => \^q\(4),
      R => \^sr\(0)
    );
\slv_reg7_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[7]_i_1_n_0\,
      D => s00_axi_wdata(6),
      Q => \^q\(5),
      R => \^sr\(0)
    );
\slv_reg7_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[7]_i_1_n_0\,
      D => s00_axi_wdata(7),
      Q => \^q\(6),
      R => \^sr\(0)
    );
\slv_reg7_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[15]_i_1_n_0\,
      D => s00_axi_wdata(8),
      Q => \^q\(7),
      R => \^sr\(0)
    );
\slv_reg7_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => \slv_reg7[15]_i_1_n_0\,
      D => s00_axi_wdata(9),
      Q => \^q\(8),
      R => \^sr\(0)
    );
slv_reg_rden: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => s00_axi_arvalid,
      I1 => \^s00_axi_rvalid\,
      I2 => \^s00_axi_arready\,
      O => \slv_reg_rden__0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_spi_hmc_mode_RX is
  port (
    enable_sck : out STD_LOGIC;
    pll_mosi_read : out STD_LOGIC;
    pll_sen_read : out STD_LOGIC;
    pll_data_ready_RX_ila : out STD_LOGIC;
    \out\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \data_r_reg[0]_0\ : out STD_LOGIC;
    DATA_receive_ila : out STD_LOGIC_VECTOR ( 23 downto 0 );
    clk_10MHz : in STD_LOGIC;
    ready_r_reg_0 : in STD_LOGIC;
    \axi_rdata_reg[0]\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \axi_rdata_reg[0]_0\ : in STD_LOGIC;
    pll_data_ready_TX_ila : in STD_LOGIC;
    mosi_r_reg_0 : in STD_LOGIC;
    spi_pll_ld_sdo : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_spi_hmc_mode_RX;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_spi_hmc_mode_RX is
  signal \^data_receive_ila\ : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal \cnt[0]_i_1_n_0\ : STD_LOGIC;
  signal \cnt[0]_i_3_n_0\ : STD_LOGIC;
  signal \cnt[0]_i_4_n_0\ : STD_LOGIC;
  signal \cnt[0]_i_5_n_0\ : STD_LOGIC;
  signal \cnt[0]_i_6__0_n_0\ : STD_LOGIC;
  signal \cnt[0]_i_7_n_0\ : STD_LOGIC;
  signal cnt_reg : STD_LOGIC_VECTOR ( 15 downto 3 );
  signal \cnt_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \cnt_reg[0]_i_2_n_1\ : STD_LOGIC;
  signal \cnt_reg[0]_i_2_n_2\ : STD_LOGIC;
  signal \cnt_reg[0]_i_2_n_3\ : STD_LOGIC;
  signal \cnt_reg[0]_i_2_n_4\ : STD_LOGIC;
  signal \cnt_reg[0]_i_2_n_5\ : STD_LOGIC;
  signal \cnt_reg[0]_i_2_n_6\ : STD_LOGIC;
  signal \cnt_reg[0]_i_2_n_7\ : STD_LOGIC;
  signal \cnt_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \cnt_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \cnt_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \cnt_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \cnt_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \cnt_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \cnt_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \cnt_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \cnt_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \cnt_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \cnt_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \cnt_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal cs_r_i_1_n_0 : STD_LOGIC;
  signal cs_r_i_2_n_0 : STD_LOGIC;
  signal cs_r_i_3_n_0 : STD_LOGIC;
  signal cs_r_i_4_n_0 : STD_LOGIC;
  signal cs_r_i_5_n_0 : STD_LOGIC;
  signal data_r1 : STD_LOGIC_VECTOR ( 15 downto 1 );
  signal \data_r1_inferred__0/i__carry__0_n_0\ : STD_LOGIC;
  signal \data_r1_inferred__0/i__carry__0_n_1\ : STD_LOGIC;
  signal \data_r1_inferred__0/i__carry__0_n_2\ : STD_LOGIC;
  signal \data_r1_inferred__0/i__carry__0_n_3\ : STD_LOGIC;
  signal \data_r1_inferred__0/i__carry__1_n_0\ : STD_LOGIC;
  signal \data_r1_inferred__0/i__carry__1_n_1\ : STD_LOGIC;
  signal \data_r1_inferred__0/i__carry__1_n_2\ : STD_LOGIC;
  signal \data_r1_inferred__0/i__carry__1_n_3\ : STD_LOGIC;
  signal \data_r1_inferred__0/i__carry__2_n_0\ : STD_LOGIC;
  signal \data_r1_inferred__0/i__carry__2_n_2\ : STD_LOGIC;
  signal \data_r1_inferred__0/i__carry__2_n_3\ : STD_LOGIC;
  signal \data_r1_inferred__0/i__carry_n_0\ : STD_LOGIC;
  signal \data_r1_inferred__0/i__carry_n_1\ : STD_LOGIC;
  signal \data_r1_inferred__0/i__carry_n_2\ : STD_LOGIC;
  signal \data_r1_inferred__0/i__carry_n_3\ : STD_LOGIC;
  signal \data_r[0]_i_1_n_0\ : STD_LOGIC;
  signal \data_r[10]_i_1_n_0\ : STD_LOGIC;
  signal \data_r[11]_i_1_n_0\ : STD_LOGIC;
  signal \data_r[12]_i_1_n_0\ : STD_LOGIC;
  signal \data_r[13]_i_1_n_0\ : STD_LOGIC;
  signal \data_r[14]_i_1_n_0\ : STD_LOGIC;
  signal \data_r[14]_i_2_n_0\ : STD_LOGIC;
  signal \data_r[15]_i_1_n_0\ : STD_LOGIC;
  signal \data_r[16]_i_1_n_0\ : STD_LOGIC;
  signal \data_r[17]_i_1_n_0\ : STD_LOGIC;
  signal \data_r[18]_i_1_n_0\ : STD_LOGIC;
  signal \data_r[19]_i_1_n_0\ : STD_LOGIC;
  signal \data_r[1]_i_1_n_0\ : STD_LOGIC;
  signal \data_r[20]_i_1_n_0\ : STD_LOGIC;
  signal \data_r[21]_i_1_n_0\ : STD_LOGIC;
  signal \data_r[22]_i_1_n_0\ : STD_LOGIC;
  signal \data_r[22]_i_2_n_0\ : STD_LOGIC;
  signal \data_r[22]_i_3_n_0\ : STD_LOGIC;
  signal \data_r[23]_i_1_n_0\ : STD_LOGIC;
  signal \data_r[23]_i_2_n_0\ : STD_LOGIC;
  signal \data_r[23]_i_3_n_0\ : STD_LOGIC;
  signal \data_r[23]_i_4_n_0\ : STD_LOGIC;
  signal \data_r[23]_i_5_n_0\ : STD_LOGIC;
  signal \data_r[23]_i_6_n_0\ : STD_LOGIC;
  signal \data_r[2]_i_1_n_0\ : STD_LOGIC;
  signal \data_r[3]_i_1_n_0\ : STD_LOGIC;
  signal \data_r[4]_i_1_n_0\ : STD_LOGIC;
  signal \data_r[5]_i_1_n_0\ : STD_LOGIC;
  signal \data_r[6]_i_1_n_0\ : STD_LOGIC;
  signal \data_r[6]_i_2_n_0\ : STD_LOGIC;
  signal \data_r[7]_i_1_n_0\ : STD_LOGIC;
  signal \data_r[8]_i_1_n_0\ : STD_LOGIC;
  signal \data_r[9]_i_1_n_0\ : STD_LOGIC;
  signal enable_cnt : STD_LOGIC;
  signal enable_sck0 : STD_LOGIC;
  signal enable_sck_i_2_n_0 : STD_LOGIC;
  signal enable_sck_i_3_n_0 : STD_LOGIC;
  signal \i__carry__0_i_1_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_2__1_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_3__1_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_4_n_0\ : STD_LOGIC;
  signal \i__carry__1_i_1_n_0\ : STD_LOGIC;
  signal \i__carry__1_i_2_n_0\ : STD_LOGIC;
  signal \i__carry__1_i_3_n_0\ : STD_LOGIC;
  signal \i__carry__1_i_4_n_0\ : STD_LOGIC;
  signal \i__carry__2_i_1_n_0\ : STD_LOGIC;
  signal \i__carry__2_i_2_n_0\ : STD_LOGIC;
  signal \i__carry__2_i_3_n_0\ : STD_LOGIC;
  signal \i__carry_i_1__1_n_0\ : STD_LOGIC;
  signal \i__carry_i_2_n_0\ : STD_LOGIC;
  signal \i__carry_i_3__1_n_0\ : STD_LOGIC;
  signal \i__carry_i_4__1_n_0\ : STD_LOGIC;
  signal \i__carry_i_5__0_n_0\ : STD_LOGIC;
  signal mosi_r_i_1_n_0 : STD_LOGIC;
  signal mosi_r_i_3_n_0 : STD_LOGIC;
  signal mosi_r_i_4_n_0 : STD_LOGIC;
  signal mosi_r_i_5_n_0 : STD_LOGIC;
  signal mosi_r_i_6_n_0 : STD_LOGIC;
  signal mosi_r_i_8_n_0 : STD_LOGIC;
  signal \mosi_r_i_9__0_n_0\ : STD_LOGIC;
  signal \^out\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \^pll_data_ready_rx_ila\ : STD_LOGIC;
  signal \^pll_mosi_read\ : STD_LOGIC;
  signal \^pll_sen_read\ : STD_LOGIC;
  signal ready_r0 : STD_LOGIC;
  signal ready_r_i_1_n_0 : STD_LOGIC;
  signal ready_r_i_4_n_0 : STD_LOGIC;
  signal ready_r_i_5_n_0 : STD_LOGIC;
  signal ready_r_i_6_n_0 : STD_LOGIC;
  signal ready_r_i_7_n_0 : STD_LOGIC;
  signal ready_r_i_8_n_0 : STD_LOGIC;
  signal ready_r_i_9_n_0 : STD_LOGIC;
  signal start_prev : STD_LOGIC;
  signal start_prev_i_1_n_0 : STD_LOGIC;
  signal \NLW_cnt_reg[12]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_data_r1_inferred__0/i__carry__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 to 2 );
  signal \NLW_data_r1_inferred__0/i__carry__2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \cnt[0]_i_3\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \cnt[0]_i_4\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \cnt[0]_i_7\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of cs_r_i_1 : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of cs_r_i_4 : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of cs_r_i_5 : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of enable_sck_i_3 : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of mosi_r_i_5 : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of mosi_r_i_6 : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \mosi_r_i_9__0\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of ready_r_i_1 : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of start_prev_i_1 : label is "soft_lutpair4";
begin
  DATA_receive_ila(23 downto 0) <= \^data_receive_ila\(23 downto 0);
  \out\(2 downto 0) <= \^out\(2 downto 0);
  pll_data_ready_RX_ila <= \^pll_data_ready_rx_ila\;
  pll_mosi_read <= \^pll_mosi_read\;
  pll_sen_read <= \^pll_sen_read\;
\axi_rdata[0]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8C8C8C8CC0C0C000"
    )
        port map (
      I0 => \^data_receive_ila\(0),
      I1 => \axi_rdata_reg[0]\(1),
      I2 => \axi_rdata_reg[0]_0\,
      I3 => pll_data_ready_TX_ila,
      I4 => \^pll_data_ready_rx_ila\,
      I5 => \axi_rdata_reg[0]\(0),
      O => \data_r_reg[0]_0\
    );
\cnt[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"555555555555555D"
    )
        port map (
      I0 => enable_cnt,
      I1 => \cnt[0]_i_3_n_0\,
      I2 => cnt_reg(4),
      I3 => cnt_reg(13),
      I4 => \cnt[0]_i_4_n_0\,
      I5 => \cnt[0]_i_5_n_0\,
      O => \cnt[0]_i_1_n_0\
    );
\cnt[0]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0008"
    )
        port map (
      I0 => cnt_reg(9),
      I1 => cnt_reg(5),
      I2 => cnt_reg(14),
      I3 => cnt_reg(12),
      O => \cnt[0]_i_3_n_0\
    );
\cnt[0]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => cnt_reg(10),
      I1 => cnt_reg(11),
      O => \cnt[0]_i_4_n_0\
    );
\cnt[0]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEFFFFF"
    )
        port map (
      I0 => \cnt[0]_i_7_n_0\,
      I1 => \^out\(0),
      I2 => cnt_reg(6),
      I3 => cnt_reg(15),
      I4 => cnt_reg(3),
      O => \cnt[0]_i_5_n_0\
    );
\cnt[0]_i_6__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^out\(0),
      O => \cnt[0]_i_6__0_n_0\
    );
\cnt[0]_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFF7"
    )
        port map (
      I0 => cnt_reg(8),
      I1 => cnt_reg(7),
      I2 => \^out\(2),
      I3 => \^out\(1),
      O => \cnt[0]_i_7_n_0\
    );
\cnt_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_reg[0]_i_2_n_7\,
      Q => \^out\(0),
      R => \cnt[0]_i_1_n_0\
    );
\cnt_reg[0]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \cnt_reg[0]_i_2_n_0\,
      CO(2) => \cnt_reg[0]_i_2_n_1\,
      CO(1) => \cnt_reg[0]_i_2_n_2\,
      CO(0) => \cnt_reg[0]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \cnt_reg[0]_i_2_n_4\,
      O(2) => \cnt_reg[0]_i_2_n_5\,
      O(1) => \cnt_reg[0]_i_2_n_6\,
      O(0) => \cnt_reg[0]_i_2_n_7\,
      S(3) => cnt_reg(3),
      S(2 downto 1) => \^out\(2 downto 1),
      S(0) => \cnt[0]_i_6__0_n_0\
    );
\cnt_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_reg[8]_i_1_n_5\,
      Q => cnt_reg(10),
      R => \cnt[0]_i_1_n_0\
    );
\cnt_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_reg[8]_i_1_n_4\,
      Q => cnt_reg(11),
      R => \cnt[0]_i_1_n_0\
    );
\cnt_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_reg[12]_i_1_n_7\,
      Q => cnt_reg(12),
      R => \cnt[0]_i_1_n_0\
    );
\cnt_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_reg[8]_i_1_n_0\,
      CO(3) => \NLW_cnt_reg[12]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \cnt_reg[12]_i_1_n_1\,
      CO(1) => \cnt_reg[12]_i_1_n_2\,
      CO(0) => \cnt_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt_reg[12]_i_1_n_4\,
      O(2) => \cnt_reg[12]_i_1_n_5\,
      O(1) => \cnt_reg[12]_i_1_n_6\,
      O(0) => \cnt_reg[12]_i_1_n_7\,
      S(3 downto 0) => cnt_reg(15 downto 12)
    );
\cnt_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_reg[12]_i_1_n_6\,
      Q => cnt_reg(13),
      R => \cnt[0]_i_1_n_0\
    );
\cnt_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_reg[12]_i_1_n_5\,
      Q => cnt_reg(14),
      R => \cnt[0]_i_1_n_0\
    );
\cnt_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_reg[12]_i_1_n_4\,
      Q => cnt_reg(15),
      R => \cnt[0]_i_1_n_0\
    );
\cnt_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_reg[0]_i_2_n_6\,
      Q => \^out\(1),
      R => \cnt[0]_i_1_n_0\
    );
\cnt_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_reg[0]_i_2_n_5\,
      Q => \^out\(2),
      R => \cnt[0]_i_1_n_0\
    );
\cnt_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_reg[0]_i_2_n_4\,
      Q => cnt_reg(3),
      R => \cnt[0]_i_1_n_0\
    );
\cnt_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_reg[4]_i_1_n_7\,
      Q => cnt_reg(4),
      R => \cnt[0]_i_1_n_0\
    );
\cnt_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_reg[0]_i_2_n_0\,
      CO(3) => \cnt_reg[4]_i_1_n_0\,
      CO(2) => \cnt_reg[4]_i_1_n_1\,
      CO(1) => \cnt_reg[4]_i_1_n_2\,
      CO(0) => \cnt_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt_reg[4]_i_1_n_4\,
      O(2) => \cnt_reg[4]_i_1_n_5\,
      O(1) => \cnt_reg[4]_i_1_n_6\,
      O(0) => \cnt_reg[4]_i_1_n_7\,
      S(3 downto 0) => cnt_reg(7 downto 4)
    );
\cnt_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_reg[4]_i_1_n_6\,
      Q => cnt_reg(5),
      R => \cnt[0]_i_1_n_0\
    );
\cnt_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_reg[4]_i_1_n_5\,
      Q => cnt_reg(6),
      R => \cnt[0]_i_1_n_0\
    );
\cnt_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_reg[4]_i_1_n_4\,
      Q => cnt_reg(7),
      R => \cnt[0]_i_1_n_0\
    );
\cnt_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_reg[8]_i_1_n_7\,
      Q => cnt_reg(8),
      R => \cnt[0]_i_1_n_0\
    );
\cnt_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_reg[4]_i_1_n_0\,
      CO(3) => \cnt_reg[8]_i_1_n_0\,
      CO(2) => \cnt_reg[8]_i_1_n_1\,
      CO(1) => \cnt_reg[8]_i_1_n_2\,
      CO(0) => \cnt_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt_reg[8]_i_1_n_4\,
      O(2) => \cnt_reg[8]_i_1_n_5\,
      O(1) => \cnt_reg[8]_i_1_n_6\,
      O(0) => \cnt_reg[8]_i_1_n_7\,
      S(3 downto 0) => cnt_reg(11 downto 8)
    );
\cnt_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_reg[8]_i_1_n_6\,
      Q => cnt_reg(9),
      R => \cnt[0]_i_1_n_0\
    );
cs_r_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00AE"
    )
        port map (
      I0 => \^pll_sen_read\,
      I1 => ready_r_reg_0,
      I2 => enable_cnt,
      I3 => cs_r_i_2_n_0,
      O => cs_r_i_1_n_0
    );
cs_r_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000002000000"
    )
        port map (
      I0 => cs_r_i_3_n_0,
      I1 => cnt_reg(14),
      I2 => cnt_reg(12),
      I3 => cnt_reg(5),
      I4 => \^out\(0),
      I5 => cs_r_i_4_n_0,
      O => cs_r_i_2_n_0
    );
cs_r_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => enable_sck_i_3_n_0,
      I1 => cnt_reg(3),
      I2 => cnt_reg(15),
      I3 => cnt_reg(4),
      I4 => cnt_reg(13),
      I5 => cs_r_i_5_n_0,
      O => cs_r_i_3_n_0
    );
cs_r_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => cnt_reg(11),
      I1 => cnt_reg(10),
      I2 => cnt_reg(9),
      I3 => cnt_reg(8),
      O => cs_r_i_4_n_0
    );
cs_r_i_5: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => cnt_reg(7),
      I1 => cnt_reg(6),
      O => cs_r_i_5_n_0
    );
cs_r_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => cs_r_i_1_n_0,
      Q => \^pll_sen_read\,
      R => '0'
    );
\data_r1_inferred__0/i__carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \data_r1_inferred__0/i__carry_n_0\,
      CO(2) => \data_r1_inferred__0/i__carry_n_1\,
      CO(1) => \data_r1_inferred__0/i__carry_n_2\,
      CO(0) => \data_r1_inferred__0/i__carry_n_3\,
      CYINIT => \i__carry_i_1__1_n_0\,
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data_r1(4 downto 1),
      S(3) => \i__carry_i_2_n_0\,
      S(2) => \i__carry_i_3__1_n_0\,
      S(1) => \i__carry_i_4__1_n_0\,
      S(0) => \i__carry_i_5__0_n_0\
    );
\data_r1_inferred__0/i__carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \data_r1_inferred__0/i__carry_n_0\,
      CO(3) => \data_r1_inferred__0/i__carry__0_n_0\,
      CO(2) => \data_r1_inferred__0/i__carry__0_n_1\,
      CO(1) => \data_r1_inferred__0/i__carry__0_n_2\,
      CO(0) => \data_r1_inferred__0/i__carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \i__carry__0_i_1_n_0\,
      O(3 downto 0) => data_r1(8 downto 5),
      S(3) => \i__carry__0_i_2__1_n_0\,
      S(2) => \i__carry__0_i_3__1_n_0\,
      S(1) => \i__carry__0_i_4_n_0\,
      S(0) => cnt_reg(5)
    );
\data_r1_inferred__0/i__carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \data_r1_inferred__0/i__carry__0_n_0\,
      CO(3) => \data_r1_inferred__0/i__carry__1_n_0\,
      CO(2) => \data_r1_inferred__0/i__carry__1_n_1\,
      CO(1) => \data_r1_inferred__0/i__carry__1_n_2\,
      CO(0) => \data_r1_inferred__0/i__carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data_r1(12 downto 9),
      S(3) => \i__carry__1_i_1_n_0\,
      S(2) => \i__carry__1_i_2_n_0\,
      S(1) => \i__carry__1_i_3_n_0\,
      S(0) => \i__carry__1_i_4_n_0\
    );
\data_r1_inferred__0/i__carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \data_r1_inferred__0/i__carry__1_n_0\,
      CO(3) => \data_r1_inferred__0/i__carry__2_n_0\,
      CO(2) => \NLW_data_r1_inferred__0/i__carry__2_CO_UNCONNECTED\(2),
      CO(1) => \data_r1_inferred__0/i__carry__2_n_2\,
      CO(0) => \data_r1_inferred__0/i__carry__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \NLW_data_r1_inferred__0/i__carry__2_O_UNCONNECTED\(3),
      O(2 downto 0) => data_r1(15 downto 13),
      S(3) => '1',
      S(2) => \i__carry__2_i_1_n_0\,
      S(1) => \i__carry__2_i_2_n_0\,
      S(0) => \i__carry__2_i_3_n_0\
    );
\data_r[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000002"
    )
        port map (
      I0 => spi_pll_ld_sdo,
      I1 => \^out\(0),
      I2 => data_r1(1),
      I3 => data_r1(2),
      I4 => \data_r[6]_i_2_n_0\,
      I5 => \^data_receive_ila\(0),
      O => \data_r[0]_i_1_n_0\
    );
\data_r[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFEF00000020"
    )
        port map (
      I0 => spi_pll_ld_sdo,
      I1 => \^out\(0),
      I2 => data_r1(1),
      I3 => data_r1(2),
      I4 => \data_r[14]_i_2_n_0\,
      I5 => \^data_receive_ila\(10),
      O => \data_r[10]_i_1_n_0\
    );
\data_r[11]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFBF00000080"
    )
        port map (
      I0 => spi_pll_ld_sdo,
      I1 => \^out\(0),
      I2 => data_r1(1),
      I3 => data_r1(2),
      I4 => \data_r[14]_i_2_n_0\,
      I5 => \^data_receive_ila\(11),
      O => \data_r[11]_i_1_n_0\
    );
\data_r[12]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFEFF00000200"
    )
        port map (
      I0 => spi_pll_ld_sdo,
      I1 => \^out\(0),
      I2 => data_r1(1),
      I3 => data_r1(2),
      I4 => \data_r[14]_i_2_n_0\,
      I5 => \^data_receive_ila\(12),
      O => \data_r[12]_i_1_n_0\
    );
\data_r[13]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFEFFF00002000"
    )
        port map (
      I0 => spi_pll_ld_sdo,
      I1 => data_r1(1),
      I2 => \^out\(0),
      I3 => data_r1(2),
      I4 => \data_r[14]_i_2_n_0\,
      I5 => \^data_receive_ila\(13),
      O => \data_r[13]_i_1_n_0\
    );
\data_r[14]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFEFFF00002000"
    )
        port map (
      I0 => spi_pll_ld_sdo,
      I1 => \^out\(0),
      I2 => data_r1(1),
      I3 => data_r1(2),
      I4 => \data_r[14]_i_2_n_0\,
      I5 => \^data_receive_ila\(14),
      O => \data_r[14]_i_1_n_0\
    );
\data_r[14]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFB"
    )
        port map (
      I0 => \data_r[23]_i_6_n_0\,
      I1 => data_r1(3),
      I2 => data_r1(4),
      I3 => \data_r[23]_i_5_n_0\,
      I4 => \data_r[23]_i_4_n_0\,
      I5 => \data_r[22]_i_3_n_0\,
      O => \data_r[14]_i_2_n_0\
    );
\data_r[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFBF00000080"
    )
        port map (
      I0 => spi_pll_ld_sdo,
      I1 => \data_r[23]_i_3_n_0\,
      I2 => data_r1(3),
      I3 => data_r1(4),
      I4 => \data_r[23]_i_2_n_0\,
      I5 => \^data_receive_ila\(15),
      O => \data_r[15]_i_1_n_0\
    );
\data_r[16]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFB00000008"
    )
        port map (
      I0 => spi_pll_ld_sdo,
      I1 => \data_r[22]_i_2_n_0\,
      I2 => \^out\(0),
      I3 => data_r1(1),
      I4 => data_r1(2),
      I5 => \^data_receive_ila\(16),
      O => \data_r[16]_i_1_n_0\
    );
\data_r[17]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFBFF00000800"
    )
        port map (
      I0 => spi_pll_ld_sdo,
      I1 => \data_r[22]_i_2_n_0\,
      I2 => data_r1(1),
      I3 => \^out\(0),
      I4 => data_r1(2),
      I5 => \^data_receive_ila\(17),
      O => \data_r[17]_i_1_n_0\
    );
\data_r[18]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFBFF00000800"
    )
        port map (
      I0 => spi_pll_ld_sdo,
      I1 => \data_r[22]_i_2_n_0\,
      I2 => \^out\(0),
      I3 => data_r1(1),
      I4 => data_r1(2),
      I5 => \^data_receive_ila\(18),
      O => \data_r[18]_i_1_n_0\
    );
\data_r[19]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFBFFF00008000"
    )
        port map (
      I0 => spi_pll_ld_sdo,
      I1 => \data_r[22]_i_2_n_0\,
      I2 => \^out\(0),
      I3 => data_r1(1),
      I4 => data_r1(2),
      I5 => \^data_receive_ila\(19),
      O => \data_r[19]_i_1_n_0\
    );
\data_r[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFEF00000020"
    )
        port map (
      I0 => spi_pll_ld_sdo,
      I1 => data_r1(1),
      I2 => \^out\(0),
      I3 => data_r1(2),
      I4 => \data_r[6]_i_2_n_0\,
      I5 => \^data_receive_ila\(1),
      O => \data_r[1]_i_1_n_0\
    );
\data_r[20]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFBFFFF00080000"
    )
        port map (
      I0 => spi_pll_ld_sdo,
      I1 => \data_r[22]_i_2_n_0\,
      I2 => \^out\(0),
      I3 => data_r1(1),
      I4 => data_r1(2),
      I5 => \^data_receive_ila\(20),
      O => \data_r[20]_i_1_n_0\
    );
\data_r[21]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FBFFFFFF08000000"
    )
        port map (
      I0 => spi_pll_ld_sdo,
      I1 => \data_r[22]_i_2_n_0\,
      I2 => data_r1(1),
      I3 => \^out\(0),
      I4 => data_r1(2),
      I5 => \^data_receive_ila\(21),
      O => \data_r[21]_i_1_n_0\
    );
\data_r[22]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EFFFFFFF20000000"
    )
        port map (
      I0 => spi_pll_ld_sdo,
      I1 => \^out\(0),
      I2 => data_r1(1),
      I3 => data_r1(2),
      I4 => \data_r[22]_i_2_n_0\,
      I5 => \^data_receive_ila\(22),
      O => \data_r[22]_i_1_n_0\
    );
\data_r[22]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000004"
    )
        port map (
      I0 => data_r1(3),
      I1 => data_r1(4),
      I2 => \data_r[22]_i_3_n_0\,
      I3 => \data_r[23]_i_4_n_0\,
      I4 => \data_r[23]_i_5_n_0\,
      I5 => \data_r[23]_i_6_n_0\,
      O => \data_r[22]_i_2_n_0\
    );
\data_r[22]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => data_r1(5),
      I1 => data_r1(14),
      I2 => data_r1(12),
      I3 => data_r1(15),
      O => \data_r[22]_i_3_n_0\
    );
\data_r[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFEFFFFF00200000"
    )
        port map (
      I0 => spi_pll_ld_sdo,
      I1 => data_r1(3),
      I2 => data_r1(4),
      I3 => \data_r[23]_i_2_n_0\,
      I4 => \data_r[23]_i_3_n_0\,
      I5 => \^data_receive_ila\(23),
      O => \data_r[23]_i_1_n_0\
    );
\data_r[23]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => data_r1(15),
      I1 => data_r1(12),
      I2 => data_r1(14),
      I3 => data_r1(5),
      I4 => \data_r[23]_i_4_n_0\,
      I5 => \data_r[23]_i_5_n_0\,
      O => \data_r[23]_i_2_n_0\
    );
\data_r[23]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4000"
    )
        port map (
      I0 => \data_r[23]_i_6_n_0\,
      I1 => data_r1(2),
      I2 => \^out\(0),
      I3 => data_r1(1),
      O => \data_r[23]_i_3_n_0\
    );
\data_r[23]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => data_r1(9),
      I1 => data_r1(11),
      I2 => data_r1(6),
      I3 => data_r1(10),
      O => \data_r[23]_i_4_n_0\
    );
\data_r[23]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => \data_r1_inferred__0/i__carry__2_n_0\,
      I1 => data_r1(8),
      I2 => data_r1(7),
      I3 => data_r1(13),
      O => \data_r[23]_i_5_n_0\
    );
\data_r[23]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFEFF00FFFF"
    )
        port map (
      I0 => \^out\(0),
      I1 => \^out\(1),
      I2 => \^out\(2),
      I3 => enable_sck_i_2_n_0,
      I4 => mosi_r_i_8_n_0,
      I5 => cnt_reg(5),
      O => \data_r[23]_i_6_n_0\
    );
\data_r[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFEF00000020"
    )
        port map (
      I0 => spi_pll_ld_sdo,
      I1 => \^out\(0),
      I2 => data_r1(1),
      I3 => data_r1(2),
      I4 => \data_r[6]_i_2_n_0\,
      I5 => \^data_receive_ila\(2),
      O => \data_r[2]_i_1_n_0\
    );
\data_r[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFBF00000080"
    )
        port map (
      I0 => spi_pll_ld_sdo,
      I1 => \^out\(0),
      I2 => data_r1(1),
      I3 => data_r1(2),
      I4 => \data_r[6]_i_2_n_0\,
      I5 => \^data_receive_ila\(3),
      O => \data_r[3]_i_1_n_0\
    );
\data_r[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFEFF00000200"
    )
        port map (
      I0 => spi_pll_ld_sdo,
      I1 => \^out\(0),
      I2 => data_r1(1),
      I3 => data_r1(2),
      I4 => \data_r[6]_i_2_n_0\,
      I5 => \^data_receive_ila\(4),
      O => \data_r[4]_i_1_n_0\
    );
\data_r[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFEFFF00002000"
    )
        port map (
      I0 => spi_pll_ld_sdo,
      I1 => data_r1(1),
      I2 => \^out\(0),
      I3 => data_r1(2),
      I4 => \data_r[6]_i_2_n_0\,
      I5 => \^data_receive_ila\(5),
      O => \data_r[5]_i_1_n_0\
    );
\data_r[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFEFFF00002000"
    )
        port map (
      I0 => spi_pll_ld_sdo,
      I1 => \^out\(0),
      I2 => data_r1(1),
      I3 => data_r1(2),
      I4 => \data_r[6]_i_2_n_0\,
      I5 => \^data_receive_ila\(6),
      O => \data_r[6]_i_1_n_0\
    );
\data_r[6]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \data_r[23]_i_6_n_0\,
      I1 => data_r1(4),
      I2 => \data_r[23]_i_5_n_0\,
      I3 => \data_r[23]_i_4_n_0\,
      I4 => \data_r[22]_i_3_n_0\,
      I5 => data_r1(3),
      O => \data_r[6]_i_2_n_0\
    );
\data_r[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFB00000008"
    )
        port map (
      I0 => spi_pll_ld_sdo,
      I1 => \data_r[23]_i_3_n_0\,
      I2 => data_r1(4),
      I3 => \data_r[23]_i_2_n_0\,
      I4 => data_r1(3),
      I5 => \^data_receive_ila\(7),
      O => \data_r[7]_i_1_n_0\
    );
\data_r[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFE00000002"
    )
        port map (
      I0 => spi_pll_ld_sdo,
      I1 => \^out\(0),
      I2 => data_r1(1),
      I3 => data_r1(2),
      I4 => \data_r[14]_i_2_n_0\,
      I5 => \^data_receive_ila\(8),
      O => \data_r[8]_i_1_n_0\
    );
\data_r[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFEF00000020"
    )
        port map (
      I0 => spi_pll_ld_sdo,
      I1 => data_r1(1),
      I2 => \^out\(0),
      I3 => data_r1(2),
      I4 => \data_r[14]_i_2_n_0\,
      I5 => \^data_receive_ila\(9),
      O => \data_r[9]_i_1_n_0\
    );
\data_r_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \data_r[0]_i_1_n_0\,
      Q => \^data_receive_ila\(0),
      R => '0'
    );
\data_r_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \data_r[10]_i_1_n_0\,
      Q => \^data_receive_ila\(10),
      R => '0'
    );
\data_r_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \data_r[11]_i_1_n_0\,
      Q => \^data_receive_ila\(11),
      R => '0'
    );
\data_r_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \data_r[12]_i_1_n_0\,
      Q => \^data_receive_ila\(12),
      R => '0'
    );
\data_r_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \data_r[13]_i_1_n_0\,
      Q => \^data_receive_ila\(13),
      R => '0'
    );
\data_r_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \data_r[14]_i_1_n_0\,
      Q => \^data_receive_ila\(14),
      R => '0'
    );
\data_r_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \data_r[15]_i_1_n_0\,
      Q => \^data_receive_ila\(15),
      R => '0'
    );
\data_r_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \data_r[16]_i_1_n_0\,
      Q => \^data_receive_ila\(16),
      R => '0'
    );
\data_r_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \data_r[17]_i_1_n_0\,
      Q => \^data_receive_ila\(17),
      R => '0'
    );
\data_r_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \data_r[18]_i_1_n_0\,
      Q => \^data_receive_ila\(18),
      R => '0'
    );
\data_r_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \data_r[19]_i_1_n_0\,
      Q => \^data_receive_ila\(19),
      R => '0'
    );
\data_r_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \data_r[1]_i_1_n_0\,
      Q => \^data_receive_ila\(1),
      R => '0'
    );
\data_r_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \data_r[20]_i_1_n_0\,
      Q => \^data_receive_ila\(20),
      R => '0'
    );
\data_r_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \data_r[21]_i_1_n_0\,
      Q => \^data_receive_ila\(21),
      R => '0'
    );
\data_r_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \data_r[22]_i_1_n_0\,
      Q => \^data_receive_ila\(22),
      R => '0'
    );
\data_r_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \data_r[23]_i_1_n_0\,
      Q => \^data_receive_ila\(23),
      R => '0'
    );
\data_r_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \data_r[2]_i_1_n_0\,
      Q => \^data_receive_ila\(2),
      R => '0'
    );
\data_r_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \data_r[3]_i_1_n_0\,
      Q => \^data_receive_ila\(3),
      R => '0'
    );
\data_r_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \data_r[4]_i_1_n_0\,
      Q => \^data_receive_ila\(4),
      R => '0'
    );
\data_r_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \data_r[5]_i_1_n_0\,
      Q => \^data_receive_ila\(5),
      R => '0'
    );
\data_r_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \data_r[6]_i_1_n_0\,
      Q => \^data_receive_ila\(6),
      R => '0'
    );
\data_r_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \data_r[7]_i_1_n_0\,
      Q => \^data_receive_ila\(7),
      R => '0'
    );
\data_r_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \data_r[8]_i_1_n_0\,
      Q => \^data_receive_ila\(8),
      R => '0'
    );
\data_r_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \data_r[9]_i_1_n_0\,
      Q => \^data_receive_ila\(9),
      R => '0'
    );
enable_sck_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1111111111111110"
    )
        port map (
      I0 => cnt_reg(5),
      I1 => enable_sck_i_2_n_0,
      I2 => \^out\(0),
      I3 => enable_sck_i_3_n_0,
      I4 => cnt_reg(4),
      I5 => cnt_reg(3),
      O => enable_sck0
    );
enable_sck_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => cnt_reg(8),
      I1 => cnt_reg(9),
      I2 => \cnt[0]_i_4_n_0\,
      I3 => cnt_reg(7),
      I4 => cnt_reg(6),
      I5 => ready_r_i_8_n_0,
      O => enable_sck_i_2_n_0
    );
enable_sck_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \^out\(1),
      I1 => \^out\(2),
      O => enable_sck_i_3_n_0
    );
enable_sck_reg: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => enable_sck0,
      Q => enable_sck,
      R => '0'
    );
\i__carry__0_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt_reg(5),
      O => \i__carry__0_i_1_n_0\
    );
\i__carry__0_i_2__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt_reg(8),
      O => \i__carry__0_i_2__1_n_0\
    );
\i__carry__0_i_3__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt_reg(7),
      O => \i__carry__0_i_3__1_n_0\
    );
\i__carry__0_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt_reg(6),
      O => \i__carry__0_i_4_n_0\
    );
\i__carry__1_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt_reg(12),
      O => \i__carry__1_i_1_n_0\
    );
\i__carry__1_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt_reg(11),
      O => \i__carry__1_i_2_n_0\
    );
\i__carry__1_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt_reg(10),
      O => \i__carry__1_i_3_n_0\
    );
\i__carry__1_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt_reg(9),
      O => \i__carry__1_i_4_n_0\
    );
\i__carry__2_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt_reg(15),
      O => \i__carry__2_i_1_n_0\
    );
\i__carry__2_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt_reg(14),
      O => \i__carry__2_i_2_n_0\
    );
\i__carry__2_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt_reg(13),
      O => \i__carry__2_i_3_n_0\
    );
\i__carry_i_1__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^out\(0),
      O => \i__carry_i_1__1_n_0\
    );
\i__carry_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt_reg(4),
      O => \i__carry_i_2_n_0\
    );
\i__carry_i_3__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt_reg(3),
      O => \i__carry_i_3__1_n_0\
    );
\i__carry_i_4__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^out\(2),
      O => \i__carry_i_4__1_n_0\
    );
\i__carry_i_5__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^out\(1),
      O => \i__carry_i_5__0_n_0\
    );
mosi_r_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F5F5F0F3F5F5F0F0"
    )
        port map (
      I0 => mosi_r_reg_0,
      I1 => mosi_r_i_3_n_0,
      I2 => mosi_r_i_4_n_0,
      I3 => mosi_r_i_5_n_0,
      I4 => mosi_r_i_6_n_0,
      I5 => \^pll_mosi_read\,
      O => mosi_r_i_1_n_0
    );
mosi_r_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0001010144444444"
    )
        port map (
      I0 => enable_sck_i_2_n_0,
      I1 => mosi_r_i_8_n_0,
      I2 => \^out\(2),
      I3 => \^out\(0),
      I4 => \^out\(1),
      I5 => cnt_reg(5),
      O => mosi_r_i_3_n_0
    );
mosi_r_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000200000000"
    )
        port map (
      I0 => \mosi_r_i_9__0_n_0\,
      I1 => \^out\(2),
      I2 => cnt_reg(3),
      I3 => cnt_reg(4),
      I4 => \^out\(1),
      I5 => \^out\(0),
      O => mosi_r_i_4_n_0
    );
mosi_r_i_5: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F4"
    )
        port map (
      I0 => enable_cnt,
      I1 => ready_r_reg_0,
      I2 => cs_r_i_2_n_0,
      O => mosi_r_i_5_n_0
    );
mosi_r_i_6: unisim.vcomponents.LUT5
    generic map(
      INIT => X"000000E0"
    )
        port map (
      I0 => \^out\(2),
      I1 => \^out\(1),
      I2 => \mosi_r_i_9__0_n_0\,
      I3 => cnt_reg(4),
      I4 => cnt_reg(3),
      O => mosi_r_i_6_n_0
    );
mosi_r_i_8: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => cnt_reg(4),
      I1 => cnt_reg(3),
      O => mosi_r_i_8_n_0
    );
\mosi_r_i_9__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt_reg(5),
      I1 => enable_sck_i_2_n_0,
      O => \mosi_r_i_9__0_n_0\
    );
mosi_r_reg: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => mosi_r_i_1_n_0,
      Q => \^pll_mosi_read\,
      R => '0'
    );
ready_r_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0000EECE"
    )
        port map (
      I0 => \^pll_data_ready_rx_ila\,
      I1 => ready_r0,
      I2 => ready_r_reg_0,
      I3 => enable_cnt,
      I4 => start_prev,
      O => ready_r_i_1_n_0
    );
ready_r_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAA88808888"
    )
        port map (
      I0 => ready_r_i_4_n_0,
      I1 => cnt_reg(8),
      I2 => cnt_reg(7),
      I3 => cnt_reg(6),
      I4 => ready_r_i_5_n_0,
      I5 => cnt_reg(9),
      O => ready_r0
    );
ready_r_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0002000000000000"
    )
        port map (
      I0 => ready_r_i_6_n_0,
      I1 => ready_r_i_7_n_0,
      I2 => ready_r_i_8_n_0,
      I3 => enable_sck_i_3_n_0,
      I4 => cnt_reg(7),
      I5 => cnt_reg(8),
      O => start_prev
    );
ready_r_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000002"
    )
        port map (
      I0 => ready_r_i_9_n_0,
      I1 => \cnt[0]_i_4_n_0\,
      I2 => cnt_reg(14),
      I3 => cnt_reg(12),
      I4 => cnt_reg(15),
      I5 => cnt_reg(13),
      O => ready_r_i_4_n_0
    );
ready_r_i_5: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00001FFFFFFFFFFF"
    )
        port map (
      I0 => \^out\(0),
      I1 => \^out\(1),
      I2 => cnt_reg(3),
      I3 => \^out\(2),
      I4 => cnt_reg(4),
      I5 => cnt_reg(5),
      O => ready_r_i_5_n_0
    );
ready_r_i_6: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000800000"
    )
        port map (
      I0 => cnt_reg(5),
      I1 => cnt_reg(7),
      I2 => cnt_reg(6),
      I3 => cnt_reg(4),
      I4 => cnt_reg(3),
      I5 => \cnt[0]_i_4_n_0\,
      O => ready_r_i_6_n_0
    );
ready_r_i_7: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F4F4F4F4FFFFF4FF"
    )
        port map (
      I0 => \^out\(1),
      I1 => \^out\(0),
      I2 => \^out\(2),
      I3 => cnt_reg(9),
      I4 => cnt_reg(10),
      I5 => cnt_reg(11),
      O => ready_r_i_7_n_0
    );
ready_r_i_8: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => cnt_reg(14),
      I1 => cnt_reg(12),
      I2 => cnt_reg(15),
      I3 => cnt_reg(13),
      O => ready_r_i_8_n_0
    );
ready_r_i_9: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFFFFFFFFFF"
    )
        port map (
      I0 => mosi_r_i_8_n_0,
      I1 => cnt_reg(5),
      I2 => cnt_reg(9),
      I3 => cnt_reg(6),
      I4 => cnt_reg(7),
      I5 => cnt_reg(8),
      O => ready_r_i_9_n_0
    );
ready_r_reg: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => ready_r_i_1_n_0,
      Q => \^pll_data_ready_rx_ila\,
      R => '0'
    );
start_prev_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"0E"
    )
        port map (
      I0 => enable_cnt,
      I1 => ready_r_reg_0,
      I2 => start_prev,
      O => start_prev_i_1_n_0
    );
start_prev_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => start_prev_i_1_n_0,
      Q => enable_cnt,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_spi_hmc_mode_TX is
  port (
    pll_data_ready_TX_ila : out STD_LOGIC;
    \out\ : out STD_LOGIC_VECTOR ( 4 downto 0 );
    \cnt_reg[5]_0\ : out STD_LOGIC;
    spi_pll_sck : out STD_LOGIC;
    spi_pll_sen : out STD_LOGIC;
    spi_pll_mosi : out STD_LOGIC;
    clk_10MHz : in STD_LOGIC;
    ready_tx_r_reg_0 : in STD_LOGIC;
    reg_address_send_ila : in STD_LOGIC_VECTOR ( 0 to 0 );
    mosi_r_reg_0 : in STD_LOGIC;
    enable_sck : in STD_LOGIC;
    pll_sen_read : in STD_LOGIC;
    pll_mosi_read : in STD_LOGIC;
    mosi_r_reg_1 : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_spi_hmc_mode_TX;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_spi_hmc_mode_TX is
  signal \cnt[0]_i_1__0_n_0\ : STD_LOGIC;
  signal \cnt[0]_i_3__0_n_0\ : STD_LOGIC;
  signal \cnt[0]_i_4__0_n_0\ : STD_LOGIC;
  signal \cnt[0]_i_5__0_n_0\ : STD_LOGIC;
  signal \cnt[0]_i_6_n_0\ : STD_LOGIC;
  signal cnt_reg : STD_LOGIC_VECTOR ( 15 downto 5 );
  signal \cnt_reg[0]_i_2__0_n_0\ : STD_LOGIC;
  signal \cnt_reg[0]_i_2__0_n_1\ : STD_LOGIC;
  signal \cnt_reg[0]_i_2__0_n_2\ : STD_LOGIC;
  signal \cnt_reg[0]_i_2__0_n_3\ : STD_LOGIC;
  signal \cnt_reg[0]_i_2__0_n_4\ : STD_LOGIC;
  signal \cnt_reg[0]_i_2__0_n_5\ : STD_LOGIC;
  signal \cnt_reg[0]_i_2__0_n_6\ : STD_LOGIC;
  signal \cnt_reg[0]_i_2__0_n_7\ : STD_LOGIC;
  signal \cnt_reg[12]_i_1__0_n_1\ : STD_LOGIC;
  signal \cnt_reg[12]_i_1__0_n_2\ : STD_LOGIC;
  signal \cnt_reg[12]_i_1__0_n_3\ : STD_LOGIC;
  signal \cnt_reg[12]_i_1__0_n_4\ : STD_LOGIC;
  signal \cnt_reg[12]_i_1__0_n_5\ : STD_LOGIC;
  signal \cnt_reg[12]_i_1__0_n_6\ : STD_LOGIC;
  signal \cnt_reg[12]_i_1__0_n_7\ : STD_LOGIC;
  signal \cnt_reg[4]_i_1__0_n_0\ : STD_LOGIC;
  signal \cnt_reg[4]_i_1__0_n_1\ : STD_LOGIC;
  signal \cnt_reg[4]_i_1__0_n_2\ : STD_LOGIC;
  signal \cnt_reg[4]_i_1__0_n_3\ : STD_LOGIC;
  signal \cnt_reg[4]_i_1__0_n_4\ : STD_LOGIC;
  signal \cnt_reg[4]_i_1__0_n_5\ : STD_LOGIC;
  signal \cnt_reg[4]_i_1__0_n_6\ : STD_LOGIC;
  signal \cnt_reg[4]_i_1__0_n_7\ : STD_LOGIC;
  signal \^cnt_reg[5]_0\ : STD_LOGIC;
  signal \cnt_reg[8]_i_1__0_n_0\ : STD_LOGIC;
  signal \cnt_reg[8]_i_1__0_n_1\ : STD_LOGIC;
  signal \cnt_reg[8]_i_1__0_n_2\ : STD_LOGIC;
  signal \cnt_reg[8]_i_1__0_n_3\ : STD_LOGIC;
  signal \cnt_reg[8]_i_1__0_n_4\ : STD_LOGIC;
  signal \cnt_reg[8]_i_1__0_n_5\ : STD_LOGIC;
  signal \cnt_reg[8]_i_1__0_n_6\ : STD_LOGIC;
  signal \cnt_reg[8]_i_1__0_n_7\ : STD_LOGIC;
  signal \cs_r_i_1__0_n_0\ : STD_LOGIC;
  signal \cs_r_i_3__0_n_0\ : STD_LOGIC;
  signal \cs_r_i_4__0_n_0\ : STD_LOGIC;
  signal \cs_r_i_5__0_n_0\ : STD_LOGIC;
  signal cs_r_i_6_n_0 : STD_LOGIC;
  signal cs_r_i_7_n_0 : STD_LOGIC;
  signal enable_cnt : STD_LOGIC;
  signal enable_sck_0 : STD_LOGIC;
  signal \enable_sck_i_1__0_n_0\ : STD_LOGIC;
  signal \enable_sck_i_2__0_n_0\ : STD_LOGIC;
  signal \enable_sck_i_3__0_n_0\ : STD_LOGIC;
  signal enable_sck_i_4_n_0 : STD_LOGIC;
  signal enable_sck_i_5_n_0 : STD_LOGIC;
  signal mosi_r_i_13_n_0 : STD_LOGIC;
  signal mosi_r_i_14_n_0 : STD_LOGIC;
  signal \mosi_r_i_1__0_n_0\ : STD_LOGIC;
  signal mosi_r_i_2_n_0 : STD_LOGIC;
  signal \mosi_r_i_3__0_n_0\ : STD_LOGIC;
  signal \mosi_r_i_6__0_n_0\ : STD_LOGIC;
  signal \mosi_r_i_8__0_n_0\ : STD_LOGIC;
  signal mosi_r_i_9_n_0 : STD_LOGIC;
  signal \^out\ : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal \^pll_data_ready_tx_ila\ : STD_LOGIC;
  signal pll_mosi_write : STD_LOGIC;
  signal pll_sen_write : STD_LOGIC;
  signal ready_tx_r : STD_LOGIC;
  signal ready_tx_r0 : STD_LOGIC;
  signal ready_tx_r08_out : STD_LOGIC;
  signal ready_tx_r_i_1_n_0 : STD_LOGIC;
  signal ready_tx_r_i_4_n_0 : STD_LOGIC;
  signal ready_tx_r_i_5_n_0 : STD_LOGIC;
  signal ready_tx_r_i_6_n_0 : STD_LOGIC;
  signal ready_tx_r_i_7_n_0 : STD_LOGIC;
  signal \start_prev_i_1__0_n_0\ : STD_LOGIC;
  signal \NLW_cnt_reg[12]_i_1__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \cnt[0]_i_3__0\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \cnt[0]_i_4__0\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \cnt[0]_i_5__0\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \cs_r_i_5__0\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of cs_r_i_7 : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of enable_sck_i_5 : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of mosi_r_i_13 : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \mosi_r_i_8__0\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of ready_tx_r_i_1 : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \start_prev_i_1__0\ : label is "soft_lutpair10";
begin
  \cnt_reg[5]_0\ <= \^cnt_reg[5]_0\;
  \out\(4 downto 0) <= \^out\(4 downto 0);
  pll_data_ready_TX_ila <= \^pll_data_ready_tx_ila\;
\cnt[0]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"555555D5"
    )
        port map (
      I0 => enable_cnt,
      I1 => \cnt[0]_i_3__0_n_0\,
      I2 => \enable_sck_i_3__0_n_0\,
      I3 => \cnt[0]_i_4__0_n_0\,
      I4 => \cnt[0]_i_5__0_n_0\,
      O => \cnt[0]_i_1__0_n_0\
    );
\cnt[0]_i_3__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000020"
    )
        port map (
      I0 => \^out\(3),
      I1 => \^out\(4),
      I2 => cnt_reg(6),
      I3 => cnt_reg(15),
      I4 => cnt_reg(12),
      O => \cnt[0]_i_3__0_n_0\
    );
\cnt[0]_i_4__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => cnt_reg(11),
      I1 => cnt_reg(10),
      I2 => cnt_reg(14),
      I3 => cnt_reg(13),
      O => \cnt[0]_i_4__0_n_0\
    );
\cnt[0]_i_5__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => cnt_reg(9),
      I1 => cnt_reg(5),
      I2 => cnt_reg(7),
      I3 => cnt_reg(8),
      O => \cnt[0]_i_5__0_n_0\
    );
\cnt[0]_i_6\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^out\(0),
      O => \cnt[0]_i_6_n_0\
    );
\cnt_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_reg[0]_i_2__0_n_7\,
      Q => \^out\(0),
      R => \cnt[0]_i_1__0_n_0\
    );
\cnt_reg[0]_i_2__0\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \cnt_reg[0]_i_2__0_n_0\,
      CO(2) => \cnt_reg[0]_i_2__0_n_1\,
      CO(1) => \cnt_reg[0]_i_2__0_n_2\,
      CO(0) => \cnt_reg[0]_i_2__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \cnt_reg[0]_i_2__0_n_4\,
      O(2) => \cnt_reg[0]_i_2__0_n_5\,
      O(1) => \cnt_reg[0]_i_2__0_n_6\,
      O(0) => \cnt_reg[0]_i_2__0_n_7\,
      S(3 downto 1) => \^out\(3 downto 1),
      S(0) => \cnt[0]_i_6_n_0\
    );
\cnt_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_reg[8]_i_1__0_n_5\,
      Q => cnt_reg(10),
      R => \cnt[0]_i_1__0_n_0\
    );
\cnt_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_reg[8]_i_1__0_n_4\,
      Q => cnt_reg(11),
      R => \cnt[0]_i_1__0_n_0\
    );
\cnt_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_reg[12]_i_1__0_n_7\,
      Q => cnt_reg(12),
      R => \cnt[0]_i_1__0_n_0\
    );
\cnt_reg[12]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_reg[8]_i_1__0_n_0\,
      CO(3) => \NLW_cnt_reg[12]_i_1__0_CO_UNCONNECTED\(3),
      CO(2) => \cnt_reg[12]_i_1__0_n_1\,
      CO(1) => \cnt_reg[12]_i_1__0_n_2\,
      CO(0) => \cnt_reg[12]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt_reg[12]_i_1__0_n_4\,
      O(2) => \cnt_reg[12]_i_1__0_n_5\,
      O(1) => \cnt_reg[12]_i_1__0_n_6\,
      O(0) => \cnt_reg[12]_i_1__0_n_7\,
      S(3 downto 0) => cnt_reg(15 downto 12)
    );
\cnt_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_reg[12]_i_1__0_n_6\,
      Q => cnt_reg(13),
      R => \cnt[0]_i_1__0_n_0\
    );
\cnt_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_reg[12]_i_1__0_n_5\,
      Q => cnt_reg(14),
      R => \cnt[0]_i_1__0_n_0\
    );
\cnt_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_reg[12]_i_1__0_n_4\,
      Q => cnt_reg(15),
      R => \cnt[0]_i_1__0_n_0\
    );
\cnt_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_reg[0]_i_2__0_n_6\,
      Q => \^out\(1),
      R => \cnt[0]_i_1__0_n_0\
    );
\cnt_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_reg[0]_i_2__0_n_5\,
      Q => \^out\(2),
      R => \cnt[0]_i_1__0_n_0\
    );
\cnt_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_reg[0]_i_2__0_n_4\,
      Q => \^out\(3),
      R => \cnt[0]_i_1__0_n_0\
    );
\cnt_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_reg[4]_i_1__0_n_7\,
      Q => \^out\(4),
      R => \cnt[0]_i_1__0_n_0\
    );
\cnt_reg[4]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_reg[0]_i_2__0_n_0\,
      CO(3) => \cnt_reg[4]_i_1__0_n_0\,
      CO(2) => \cnt_reg[4]_i_1__0_n_1\,
      CO(1) => \cnt_reg[4]_i_1__0_n_2\,
      CO(0) => \cnt_reg[4]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt_reg[4]_i_1__0_n_4\,
      O(2) => \cnt_reg[4]_i_1__0_n_5\,
      O(1) => \cnt_reg[4]_i_1__0_n_6\,
      O(0) => \cnt_reg[4]_i_1__0_n_7\,
      S(3 downto 1) => cnt_reg(7 downto 5),
      S(0) => \^out\(4)
    );
\cnt_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_reg[4]_i_1__0_n_6\,
      Q => cnt_reg(5),
      R => \cnt[0]_i_1__0_n_0\
    );
\cnt_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_reg[4]_i_1__0_n_5\,
      Q => cnt_reg(6),
      R => \cnt[0]_i_1__0_n_0\
    );
\cnt_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_reg[4]_i_1__0_n_4\,
      Q => cnt_reg(7),
      R => \cnt[0]_i_1__0_n_0\
    );
\cnt_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_reg[8]_i_1__0_n_7\,
      Q => cnt_reg(8),
      R => \cnt[0]_i_1__0_n_0\
    );
\cnt_reg[8]_i_1__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_reg[4]_i_1__0_n_0\,
      CO(3) => \cnt_reg[8]_i_1__0_n_0\,
      CO(2) => \cnt_reg[8]_i_1__0_n_1\,
      CO(1) => \cnt_reg[8]_i_1__0_n_2\,
      CO(0) => \cnt_reg[8]_i_1__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt_reg[8]_i_1__0_n_4\,
      O(2) => \cnt_reg[8]_i_1__0_n_5\,
      O(1) => \cnt_reg[8]_i_1__0_n_6\,
      O(0) => \cnt_reg[8]_i_1__0_n_7\,
      S(3 downto 0) => cnt_reg(11 downto 8)
    );
\cnt_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_reg[8]_i_1__0_n_6\,
      Q => cnt_reg(9),
      R => \cnt[0]_i_1__0_n_0\
    );
\cs_r_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EEEEEEEEEEE0EEEE"
    )
        port map (
      I0 => pll_sen_write,
      I1 => ready_tx_r0,
      I2 => \cs_r_i_3__0_n_0\,
      I3 => \cs_r_i_4__0_n_0\,
      I4 => \cs_r_i_5__0_n_0\,
      I5 => cs_r_i_6_n_0,
      O => \cs_r_i_1__0_n_0\
    );
\cs_r_i_2__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => ready_tx_r_reg_0,
      I1 => enable_cnt,
      O => ready_tx_r0
    );
\cs_r_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFEFFFF"
    )
        port map (
      I0 => cnt_reg(13),
      I1 => cnt_reg(14),
      I2 => cs_r_i_7_n_0,
      I3 => \^out\(4),
      I4 => cnt_reg(5),
      I5 => cnt_reg(15),
      O => \cs_r_i_3__0_n_0\
    );
\cs_r_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFF2FF"
    )
        port map (
      I0 => cnt_reg(9),
      I1 => cnt_reg(10),
      I2 => cnt_reg(11),
      I3 => \^out\(0),
      I4 => \^out\(1),
      I5 => \^out\(2),
      O => \cs_r_i_4__0_n_0\
    );
\cs_r_i_5__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"8A"
    )
        port map (
      I0 => cnt_reg(5),
      I1 => \^out\(4),
      I2 => \^out\(3),
      O => \cs_r_i_5__0_n_0\
    );
cs_r_i_6: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFF2"
    )
        port map (
      I0 => cnt_reg(12),
      I1 => cnt_reg(13),
      I2 => cnt_reg(14),
      I3 => cnt_reg(7),
      I4 => cnt_reg(6),
      I5 => cnt_reg(8),
      O => cs_r_i_6_n_0
    );
cs_r_i_7: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => cnt_reg(10),
      I1 => cnt_reg(11),
      O => cs_r_i_7_n_0
    );
cs_r_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \cs_r_i_1__0_n_0\,
      Q => pll_sen_write,
      R => '0'
    );
\enable_sck_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000AAAAAA2A"
    )
        port map (
      I0 => \enable_sck_i_2__0_n_0\,
      I1 => \enable_sck_i_3__0_n_0\,
      I2 => enable_sck_i_4_n_0,
      I3 => \^out\(4),
      I4 => \^out\(3),
      I5 => cnt_reg(5),
      O => \enable_sck_i_1__0_n_0\
    );
\enable_sck_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000100"
    )
        port map (
      I0 => cnt_reg(9),
      I1 => cnt_reg(11),
      I2 => cnt_reg(10),
      I3 => enable_sck_i_4_n_0,
      I4 => enable_sck_i_5_n_0,
      I5 => cnt_reg(8),
      O => \enable_sck_i_2__0_n_0\
    );
\enable_sck_i_3__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \^out\(0),
      I1 => \^out\(2),
      I2 => \^out\(1),
      O => \enable_sck_i_3__0_n_0\
    );
enable_sck_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => cnt_reg(14),
      I1 => cnt_reg(13),
      I2 => cnt_reg(15),
      I3 => cnt_reg(12),
      O => enable_sck_i_4_n_0
    );
enable_sck_i_5: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => cnt_reg(7),
      I1 => cnt_reg(6),
      O => enable_sck_i_5_n_0
    );
enable_sck_reg: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \enable_sck_i_1__0_n_0\,
      Q => enable_sck_0,
      R => '0'
    );
mosi_ila_INST_0: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => pll_mosi_write,
      I1 => pll_mosi_read,
      O => spi_pll_mosi
    );
mosi_r_i_13: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \^out\(4),
      I1 => \^out\(3),
      O => mosi_r_i_13_n_0
    );
mosi_r_i_14: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFDFFFFFFFF"
    )
        port map (
      I0 => cnt_reg(5),
      I1 => \^out\(4),
      I2 => \^out\(3),
      I3 => \^out\(2),
      I4 => \^out\(1),
      I5 => \^out\(0),
      O => mosi_r_i_14_n_0
    );
\mosi_r_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF0C080C000C080C"
    )
        port map (
      I0 => mosi_r_i_2_n_0,
      I1 => \mosi_r_i_3__0_n_0\,
      I2 => mosi_r_reg_1,
      I3 => \^cnt_reg[5]_0\,
      I4 => \mosi_r_i_6__0_n_0\,
      I5 => pll_mosi_write,
      O => \mosi_r_i_1__0_n_0\
    );
mosi_r_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000FF800080"
    )
        port map (
      I0 => \^out\(1),
      I1 => \^out\(0),
      I2 => reg_address_send_ila(0),
      I3 => \^out\(2),
      I4 => mosi_r_reg_0,
      I5 => \mosi_r_i_8__0_n_0\,
      O => mosi_r_i_2_n_0
    );
\mosi_r_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFEFFFFFFFF"
    )
        port map (
      I0 => mosi_r_i_9_n_0,
      I1 => cnt_reg(9),
      I2 => cnt_reg(11),
      I3 => cnt_reg(10),
      I4 => enable_sck_i_5_n_0,
      I5 => enable_sck_i_4_n_0,
      O => \mosi_r_i_3__0_n_0\
    );
\mosi_r_i_5__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BBB9B9B9FFFFFFFF"
    )
        port map (
      I0 => cnt_reg(5),
      I1 => mosi_r_i_13_n_0,
      I2 => \^out\(2),
      I3 => \^out\(1),
      I4 => \^out\(0),
      I5 => \enable_sck_i_2__0_n_0\,
      O => \^cnt_reg[5]_0\
    );
\mosi_r_i_6__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000FEFF00FF"
    )
        port map (
      I0 => mosi_r_i_13_n_0,
      I1 => cnt_reg(5),
      I2 => \enable_sck_i_3__0_n_0\,
      I3 => \enable_sck_i_2__0_n_0\,
      I4 => mosi_r_i_14_n_0,
      I5 => ready_tx_r0,
      O => \mosi_r_i_6__0_n_0\
    );
\mosi_r_i_8__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FEFF"
    )
        port map (
      I0 => \^out\(4),
      I1 => \^out\(3),
      I2 => cnt_reg(5),
      I3 => \enable_sck_i_2__0_n_0\,
      O => \mosi_r_i_8__0_n_0\
    );
mosi_r_i_9: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFEFFFFFFFFFF"
    )
        port map (
      I0 => mosi_r_i_13_n_0,
      I1 => \^out\(2),
      I2 => cnt_reg(8),
      I3 => cnt_reg(5),
      I4 => \^out\(1),
      I5 => \^out\(0),
      O => mosi_r_i_9_n_0
    );
mosi_r_reg: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \mosi_r_i_1__0_n_0\,
      Q => pll_mosi_write,
      R => '0'
    );
ready_tx_r_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0000EECE"
    )
        port map (
      I0 => \^pll_data_ready_tx_ila\,
      I1 => ready_tx_r08_out,
      I2 => ready_tx_r_reg_0,
      I3 => enable_cnt,
      I4 => ready_tx_r,
      O => ready_tx_r_i_1_n_0
    );
ready_tx_r_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAA8AAAA88888888"
    )
        port map (
      I0 => ready_tx_r_i_4_n_0,
      I1 => cnt_reg(9),
      I2 => cnt_reg(7),
      I3 => cnt_reg(6),
      I4 => ready_tx_r_i_5_n_0,
      I5 => cnt_reg(8),
      O => ready_tx_r08_out
    );
ready_tx_r_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000040000000"
    )
        port map (
      I0 => ready_tx_r_i_6_n_0,
      I1 => ready_tx_r_i_7_n_0,
      I2 => cnt_reg(7),
      I3 => cnt_reg(8),
      I4 => \enable_sck_i_3__0_n_0\,
      I5 => \cs_r_i_3__0_n_0\,
      O => ready_tx_r
    );
ready_tx_r_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000000000F700"
    )
        port map (
      I0 => cnt_reg(6),
      I1 => mosi_r_i_13_n_0,
      I2 => \cnt[0]_i_5__0_n_0\,
      I3 => enable_sck_i_4_n_0,
      I4 => cnt_reg(10),
      I5 => cnt_reg(11),
      O => ready_tx_r_i_4_n_0
    );
ready_tx_r_i_5: unisim.vcomponents.LUT6
    generic map(
      INIT => X"555555557F7F7FFF"
    )
        port map (
      I0 => cnt_reg(5),
      I1 => \^out\(3),
      I2 => \^out\(2),
      I3 => \^out\(0),
      I4 => \^out\(1),
      I5 => \^out\(4),
      O => ready_tx_r_i_5_n_0
    );
ready_tx_r_i_6: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F2F2F2F2FFFFF2FF"
    )
        port map (
      I0 => cnt_reg(12),
      I1 => cnt_reg(13),
      I2 => cnt_reg(14),
      I3 => cnt_reg(9),
      I4 => cnt_reg(10),
      I5 => cnt_reg(11),
      O => ready_tx_r_i_6_n_0
    );
ready_tx_r_i_7: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000400040404040"
    )
        port map (
      I0 => \^out\(2),
      I1 => cnt_reg(7),
      I2 => cnt_reg(6),
      I3 => \^out\(3),
      I4 => \^out\(4),
      I5 => cnt_reg(5),
      O => ready_tx_r_i_7_n_0
    );
ready_tx_r_reg: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => ready_tx_r_i_1_n_0,
      Q => \^pll_data_ready_tx_ila\,
      R => '0'
    );
sck_ila_INST_0: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A8"
    )
        port map (
      I0 => clk_10MHz,
      I1 => enable_sck_0,
      I2 => enable_sck,
      O => spi_pll_sck
    );
sen_ila_INST_0: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => pll_sen_write,
      I1 => pll_sen_read,
      O => spi_pll_sen
    );
\start_prev_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"0E"
    )
        port map (
      I0 => enable_cnt,
      I1 => ready_tx_r_reg_0,
      I2 => ready_tx_r,
      O => \start_prev_i_1__0_n_0\
    );
start_prev_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk_10MHz,
      CE => '1',
      D => \start_prev_i_1__0_n_0\,
      Q => enable_cnt,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_top_PLL_control is
  port (
    spi_pll_cen : out STD_LOGIC;
    PLL_POW_EN : out STD_LOGIC;
    PAMP_EN : out STD_LOGIC;
    START_receive_reg_0 : out STD_LOGIC;
    START_send_reg_0 : out STD_LOGIC;
    \out\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    pll_data_ready_RX_ila : out STD_LOGIC;
    pll_data_ready_TX_ila : out STD_LOGIC;
    pll_trig : out STD_LOGIC;
    start_adc_count : out STD_LOGIC;
    \cnt_reg[4]\ : out STD_LOGIC_VECTOR ( 4 downto 0 );
    \cnt_reg[5]\ : out STD_LOGIC;
    spi_pll_sck : out STD_LOGIC;
    \data_r_reg[0]\ : out STD_LOGIC;
    DATA_receive_ila : out STD_LOGIC_VECTOR ( 23 downto 0 );
    spi_pll_sen : out STD_LOGIC;
    spi_pll_mosi : out STD_LOGIC;
    ATTEN : out STD_LOGIC_VECTOR ( 5 downto 0 );
    clk_10MHz : in STD_LOGIC;
    enable_triger_CMD : in STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC;
    pll_cen_reg_reg_0 : in STD_LOGIC_VECTOR ( 2 downto 0 );
    slv_reg0 : in STD_LOGIC_VECTOR ( 1 downto 0 );
    clkDCO_10MHz : in STD_LOGIC;
    azimut_0 : in STD_LOGIC;
    reg_address_send_ila : in STD_LOGIC_VECTOR ( 0 to 0 );
    mosi_r_reg : in STD_LOGIC;
    \axi_rdata_reg[0]\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \axi_rdata_reg[0]_0\ : in STD_LOGIC;
    mosi_r_reg_0 : in STD_LOGIC;
    mosi_r_reg_1 : in STD_LOGIC;
    spi_pll_ld_sdo : in STD_LOGIC;
    D : in STD_LOGIC_VECTOR ( 15 downto 0 );
    SS : in STD_LOGIC_VECTOR ( 0 to 0 );
    \atten_reg_reg[5]_0\ : in STD_LOGIC_VECTOR ( 5 downto 0 );
    \shift_front_from_cpu_reg[15]_0\ : in STD_LOGIC_VECTOR ( 15 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_top_PLL_control;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_top_PLL_control is
  signal \^start_receive_reg_0\ : STD_LOGIC;
  signal \^start_send_reg_0\ : STD_LOGIC;
  signal enable_sck : STD_LOGIC;
  signal enable_triger : STD_LOGIC;
  signal enable_triger_CMD_reg_n_0 : STD_LOGIC;
  signal enable_triger_i_1_n_0 : STD_LOGIC;
  signal \i__carry__0_i_1__0_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_1__1_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_1__1_n_2\ : STD_LOGIC;
  signal \i__carry__0_i_1__1_n_3\ : STD_LOGIC;
  signal \i__carry__0_i_2__0_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_2_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_3__0_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_3_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_4__0_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_4__1_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_5__0_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_5_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_6__0_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_6_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_7_n_0\ : STD_LOGIC;
  signal \i__carry__0_i_8_n_0\ : STD_LOGIC;
  signal \i__carry_i_10_n_0\ : STD_LOGIC;
  signal \i__carry_i_11_n_0\ : STD_LOGIC;
  signal \i__carry_i_12_n_0\ : STD_LOGIC;
  signal \i__carry_i_13_n_0\ : STD_LOGIC;
  signal \i__carry_i_14_n_0\ : STD_LOGIC;
  signal \i__carry_i_15_n_0\ : STD_LOGIC;
  signal \i__carry_i_16_n_0\ : STD_LOGIC;
  signal \i__carry_i_17_n_0\ : STD_LOGIC;
  signal \i__carry_i_18_n_0\ : STD_LOGIC;
  signal \i__carry_i_19_n_0\ : STD_LOGIC;
  signal \i__carry_i_1__0_n_0\ : STD_LOGIC;
  signal \i__carry_i_1_n_0\ : STD_LOGIC;
  signal \i__carry_i_2__0_n_0\ : STD_LOGIC;
  signal \i__carry_i_2__1_n_0\ : STD_LOGIC;
  signal \i__carry_i_3__0_n_0\ : STD_LOGIC;
  signal \i__carry_i_3_n_0\ : STD_LOGIC;
  signal \i__carry_i_4__0_n_0\ : STD_LOGIC;
  signal \i__carry_i_4_n_0\ : STD_LOGIC;
  signal \i__carry_i_5__1_n_0\ : STD_LOGIC;
  signal \i__carry_i_5__1_n_1\ : STD_LOGIC;
  signal \i__carry_i_5__1_n_2\ : STD_LOGIC;
  signal \i__carry_i_5__1_n_3\ : STD_LOGIC;
  signal \i__carry_i_5_n_0\ : STD_LOGIC;
  signal \i__carry_i_6__0_n_0\ : STD_LOGIC;
  signal \i__carry_i_6__0_n_1\ : STD_LOGIC;
  signal \i__carry_i_6__0_n_2\ : STD_LOGIC;
  signal \i__carry_i_6__0_n_3\ : STD_LOGIC;
  signal \i__carry_i_6_n_0\ : STD_LOGIC;
  signal \i__carry_i_7__0_n_0\ : STD_LOGIC;
  signal \i__carry_i_7__0_n_1\ : STD_LOGIC;
  signal \i__carry_i_7__0_n_2\ : STD_LOGIC;
  signal \i__carry_i_7__0_n_3\ : STD_LOGIC;
  signal \i__carry_i_7_n_0\ : STD_LOGIC;
  signal \i__carry_i_8__0_n_0\ : STD_LOGIC;
  signal \i__carry_i_8_n_0\ : STD_LOGIC;
  signal \i__carry_i_9_n_0\ : STD_LOGIC;
  signal p_2_in : STD_LOGIC_VECTOR ( 15 downto 1 );
  signal p_4_in : STD_LOGIC;
  signal \^pll_data_ready_tx_ila\ : STD_LOGIC;
  signal pll_mosi_read : STD_LOGIC;
  signal pll_sen_read : STD_LOGIC;
  signal \^pll_trig\ : STD_LOGIC;
  signal pll_trig_reg2_i_1_n_0 : STD_LOGIC;
  signal pll_trig_reg2_i_2_n_0 : STD_LOGIC;
  signal pll_trig_reg2_i_3_n_0 : STD_LOGIC;
  signal pll_trig_reg2_i_4_n_0 : STD_LOGIC;
  signal pll_trig_reg2_i_5_n_0 : STD_LOGIC;
  signal pll_trig_reg2_i_6_n_0 : STD_LOGIC;
  signal pll_trig_reg2_i_7_n_0 : STD_LOGIC;
  signal pll_trig_reg2_i_8_n_0 : STD_LOGIC;
  signal shift_front_from_cpu : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \^start_adc_count\ : STD_LOGIC;
  signal start_adc_count_r0 : STD_LOGIC;
  signal \start_adc_count_r0_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \start_adc_count_r0_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \start_adc_count_r0_carry__0_n_3\ : STD_LOGIC;
  signal start_adc_count_r0_carry_i_1_n_0 : STD_LOGIC;
  signal start_adc_count_r0_carry_i_2_n_0 : STD_LOGIC;
  signal start_adc_count_r0_carry_i_3_n_0 : STD_LOGIC;
  signal start_adc_count_r0_carry_i_4_n_0 : STD_LOGIC;
  signal start_adc_count_r0_carry_n_0 : STD_LOGIC;
  signal start_adc_count_r0_carry_n_1 : STD_LOGIC;
  signal start_adc_count_r0_carry_n_2 : STD_LOGIC;
  signal start_adc_count_r0_carry_n_3 : STD_LOGIC;
  signal start_adc_count_r10_in : STD_LOGIC;
  signal \start_adc_count_r1_inferred__0/i__carry__0_n_0\ : STD_LOGIC;
  signal \start_adc_count_r1_inferred__0/i__carry__0_n_1\ : STD_LOGIC;
  signal \start_adc_count_r1_inferred__0/i__carry__0_n_2\ : STD_LOGIC;
  signal \start_adc_count_r1_inferred__0/i__carry__0_n_3\ : STD_LOGIC;
  signal \start_adc_count_r1_inferred__0/i__carry__1_n_2\ : STD_LOGIC;
  signal \start_adc_count_r1_inferred__0/i__carry__1_n_3\ : STD_LOGIC;
  signal \start_adc_count_r1_inferred__0/i__carry_n_0\ : STD_LOGIC;
  signal \start_adc_count_r1_inferred__0/i__carry_n_1\ : STD_LOGIC;
  signal \start_adc_count_r1_inferred__0/i__carry_n_2\ : STD_LOGIC;
  signal \start_adc_count_r1_inferred__0/i__carry_n_3\ : STD_LOGIC;
  signal start_adc_count_r2 : STD_LOGIC_VECTOR ( 15 downto 1 );
  signal start_adc_count_r_i_1_n_0 : STD_LOGIC;
  signal start_adc_count_r_i_2_n_0 : STD_LOGIC;
  signal start_adc_count_r_i_3_n_0 : STD_LOGIC;
  signal start_adc_count_r_i_4_n_0 : STD_LOGIC;
  signal start_adc_count_r_i_5_n_0 : STD_LOGIC;
  signal sweep_val : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal sweep_val_tmp : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal tguard_counter : STD_LOGIC;
  signal \tguard_counter[0]_i_1_n_0\ : STD_LOGIC;
  signal \tguard_counter[15]_i_1_n_0\ : STD_LOGIC;
  signal \tguard_counter[15]_i_4_n_0\ : STD_LOGIC;
  signal \tguard_counter[15]_i_5_n_0\ : STD_LOGIC;
  signal \tguard_counter[15]_i_6_n_0\ : STD_LOGIC;
  signal \tguard_counter[15]_i_7_n_0\ : STD_LOGIC;
  signal \tguard_counter[15]_i_8_n_0\ : STD_LOGIC;
  signal \tguard_counter_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \tguard_counter_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \tguard_counter_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \tguard_counter_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \tguard_counter_reg[15]_i_3_n_2\ : STD_LOGIC;
  signal \tguard_counter_reg[15]_i_3_n_3\ : STD_LOGIC;
  signal \tguard_counter_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \tguard_counter_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \tguard_counter_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \tguard_counter_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \tguard_counter_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \tguard_counter_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \tguard_counter_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \tguard_counter_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \tguard_counter_reg_n_0_[0]\ : STD_LOGIC;
  signal \tguard_counter_reg_n_0_[10]\ : STD_LOGIC;
  signal \tguard_counter_reg_n_0_[11]\ : STD_LOGIC;
  signal \tguard_counter_reg_n_0_[12]\ : STD_LOGIC;
  signal \tguard_counter_reg_n_0_[13]\ : STD_LOGIC;
  signal \tguard_counter_reg_n_0_[14]\ : STD_LOGIC;
  signal \tguard_counter_reg_n_0_[15]\ : STD_LOGIC;
  signal \tguard_counter_reg_n_0_[1]\ : STD_LOGIC;
  signal \tguard_counter_reg_n_0_[2]\ : STD_LOGIC;
  signal \tguard_counter_reg_n_0_[3]\ : STD_LOGIC;
  signal \tguard_counter_reg_n_0_[4]\ : STD_LOGIC;
  signal \tguard_counter_reg_n_0_[5]\ : STD_LOGIC;
  signal \tguard_counter_reg_n_0_[6]\ : STD_LOGIC;
  signal \tguard_counter_reg_n_0_[7]\ : STD_LOGIC;
  signal \tguard_counter_reg_n_0_[8]\ : STD_LOGIC;
  signal \tguard_counter_reg_n_0_[9]\ : STD_LOGIC;
  signal tsweep_counter118_in : STD_LOGIC;
  signal \tsweep_counter1_inferred__0/i__carry__0_n_1\ : STD_LOGIC;
  signal \tsweep_counter1_inferred__0/i__carry__0_n_2\ : STD_LOGIC;
  signal \tsweep_counter1_inferred__0/i__carry__0_n_3\ : STD_LOGIC;
  signal \tsweep_counter1_inferred__0/i__carry_n_0\ : STD_LOGIC;
  signal \tsweep_counter1_inferred__0/i__carry_n_1\ : STD_LOGIC;
  signal \tsweep_counter1_inferred__0/i__carry_n_2\ : STD_LOGIC;
  signal \tsweep_counter1_inferred__0/i__carry_n_3\ : STD_LOGIC;
  signal tsweep_counter2 : STD_LOGIC;
  signal \tsweep_counter2_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \tsweep_counter2_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \tsweep_counter2_carry__0_n_3\ : STD_LOGIC;
  signal tsweep_counter2_carry_i_1_n_0 : STD_LOGIC;
  signal tsweep_counter2_carry_i_2_n_0 : STD_LOGIC;
  signal tsweep_counter2_carry_i_3_n_0 : STD_LOGIC;
  signal tsweep_counter2_carry_i_4_n_0 : STD_LOGIC;
  signal tsweep_counter2_carry_n_0 : STD_LOGIC;
  signal tsweep_counter2_carry_n_1 : STD_LOGIC;
  signal tsweep_counter2_carry_n_2 : STD_LOGIC;
  signal tsweep_counter2_carry_n_3 : STD_LOGIC;
  signal \tsweep_counter[0]_i_1_n_0\ : STD_LOGIC;
  signal \tsweep_counter[0]_i_2_n_0\ : STD_LOGIC;
  signal \tsweep_counter[0]_i_5_n_0\ : STD_LOGIC;
  signal \tsweep_counter[0]_i_6_n_0\ : STD_LOGIC;
  signal \tsweep_counter[0]_i_7_n_0\ : STD_LOGIC;
  signal \tsweep_counter[0]_i_8_n_0\ : STD_LOGIC;
  signal \tsweep_counter[12]_i_2_n_0\ : STD_LOGIC;
  signal \tsweep_counter[12]_i_3_n_0\ : STD_LOGIC;
  signal \tsweep_counter[12]_i_4_n_0\ : STD_LOGIC;
  signal \tsweep_counter[12]_i_5_n_0\ : STD_LOGIC;
  signal \tsweep_counter[4]_i_2_n_0\ : STD_LOGIC;
  signal \tsweep_counter[4]_i_3_n_0\ : STD_LOGIC;
  signal \tsweep_counter[4]_i_4_n_0\ : STD_LOGIC;
  signal \tsweep_counter[4]_i_5_n_0\ : STD_LOGIC;
  signal \tsweep_counter[8]_i_2_n_0\ : STD_LOGIC;
  signal \tsweep_counter[8]_i_3_n_0\ : STD_LOGIC;
  signal \tsweep_counter[8]_i_4_n_0\ : STD_LOGIC;
  signal \tsweep_counter[8]_i_5_n_0\ : STD_LOGIC;
  signal tsweep_counter_reg : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \tsweep_counter_reg[0]_i_3_n_0\ : STD_LOGIC;
  signal \tsweep_counter_reg[0]_i_3_n_1\ : STD_LOGIC;
  signal \tsweep_counter_reg[0]_i_3_n_2\ : STD_LOGIC;
  signal \tsweep_counter_reg[0]_i_3_n_3\ : STD_LOGIC;
  signal \tsweep_counter_reg[0]_i_3_n_4\ : STD_LOGIC;
  signal \tsweep_counter_reg[0]_i_3_n_5\ : STD_LOGIC;
  signal \tsweep_counter_reg[0]_i_3_n_6\ : STD_LOGIC;
  signal \tsweep_counter_reg[0]_i_3_n_7\ : STD_LOGIC;
  signal \tsweep_counter_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \tsweep_counter_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \tsweep_counter_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \tsweep_counter_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \tsweep_counter_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \tsweep_counter_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \tsweep_counter_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \tsweep_counter_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \tsweep_counter_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \tsweep_counter_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \tsweep_counter_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \tsweep_counter_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \tsweep_counter_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \tsweep_counter_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \tsweep_counter_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \tsweep_counter_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \tsweep_counter_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \tsweep_counter_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \tsweep_counter_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \tsweep_counter_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \tsweep_counter_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \tsweep_counter_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \tsweep_counter_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal \NLW_i__carry__0_i_1__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 to 2 );
  signal \NLW_i__carry__0_i_1__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal NLW_start_adc_count_r0_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_start_adc_count_r0_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_start_adc_count_r0_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_start_adc_count_r1_inferred__0/i__carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_start_adc_count_r1_inferred__0/i__carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_start_adc_count_r1_inferred__0/i__carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_start_adc_count_r1_inferred__0/i__carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tguard_counter_reg[15]_i_3_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_tguard_counter_reg[15]_i_3_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_tsweep_counter1_inferred__0/i__carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tsweep_counter1_inferred__0/i__carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_tsweep_counter2_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tsweep_counter2_carry__0_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_tsweep_counter2_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tsweep_counter_reg[12]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of enable_triger_i_1 : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of pll_trig_reg2_i_2 : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of pll_trig_reg2_i_4 : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of pll_trig_reg2_i_5 : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of pll_trig_reg2_i_6 : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of pll_trig_reg2_i_7 : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \tguard_counter[15]_i_5\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \tguard_counter[15]_i_6\ : label is "soft_lutpair17";
begin
  START_receive_reg_0 <= \^start_receive_reg_0\;
  START_send_reg_0 <= \^start_send_reg_0\;
  pll_data_ready_TX_ila <= \^pll_data_ready_tx_ila\;
  pll_trig <= \^pll_trig\;
  start_adc_count <= \^start_adc_count\;
PAMP_EN_reg_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => s00_axi_aresetn,
      D => pll_cen_reg_reg_0(1),
      Q => PAMP_EN,
      R => '0'
    );
PLL_POW_EN_reg_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => s00_axi_aresetn,
      D => pll_cen_reg_reg_0(0),
      Q => PLL_POW_EN,
      R => '0'
    );
START_receive_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => s00_axi_aresetn,
      D => slv_reg0(1),
      Q => \^start_receive_reg_0\,
      R => '0'
    );
START_send_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => s00_axi_aresetn,
      D => slv_reg0(0),
      Q => \^start_send_reg_0\,
      R => '0'
    );
\atten_reg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => s00_axi_aresetn,
      D => \atten_reg_reg[5]_0\(0),
      Q => ATTEN(0),
      R => '0'
    );
\atten_reg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => s00_axi_aresetn,
      D => \atten_reg_reg[5]_0\(1),
      Q => ATTEN(1),
      R => '0'
    );
\atten_reg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => s00_axi_aresetn,
      D => \atten_reg_reg[5]_0\(2),
      Q => ATTEN(2),
      R => '0'
    );
\atten_reg_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => s00_axi_aresetn,
      D => \atten_reg_reg[5]_0\(3),
      Q => ATTEN(3),
      R => '0'
    );
\atten_reg_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => s00_axi_aresetn,
      D => \atten_reg_reg[5]_0\(4),
      Q => ATTEN(4),
      R => '0'
    );
\atten_reg_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => s00_axi_aclk,
      CE => s00_axi_aresetn,
      D => \atten_reg_reg[5]_0\(5),
      Q => ATTEN(5),
      R => '0'
    );
enable_triger_CMD_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => enable_triger_CMD,
      Q => enable_triger_CMD_reg_n_0,
      R => '0'
    );
enable_triger_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => enable_triger_CMD_reg_n_0,
      I1 => azimut_0,
      I2 => enable_triger,
      O => enable_triger_i_1_n_0
    );
enable_triger_reg: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => enable_triger_i_1_n_0,
      Q => enable_triger,
      R => '0'
    );
\i__carry__0_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4D44"
    )
        port map (
      I0 => tsweep_counter_reg(15),
      I1 => sweep_val(15),
      I2 => tsweep_counter_reg(14),
      I3 => sweep_val(14),
      O => \i__carry__0_i_1__0_n_0\
    );
\i__carry__0_i_1__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \i__carry_i_5__1_n_0\,
      CO(3) => \i__carry__0_i_1__1_n_0\,
      CO(2) => \NLW_i__carry__0_i_1__1_CO_UNCONNECTED\(2),
      CO(1) => \i__carry__0_i_1__1_n_2\,
      CO(0) => \i__carry__0_i_1__1_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2 downto 0) => sweep_val(15 downto 13),
      O(3) => \NLW_i__carry__0_i_1__1_O_UNCONNECTED\(3),
      O(2 downto 0) => start_adc_count_r2(15 downto 13),
      S(3) => '1',
      S(2) => \i__carry__0_i_4__1_n_0\,
      S(1) => \i__carry__0_i_5__0_n_0\,
      S(0) => \i__carry__0_i_6__0_n_0\
    );
\i__carry__0_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"44D4"
    )
        port map (
      I0 => tsweep_counter_reg(13),
      I1 => sweep_val(13),
      I2 => sweep_val(12),
      I3 => tsweep_counter_reg(12),
      O => \i__carry__0_i_2_n_0\
    );
\i__carry__0_i_2__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"82"
    )
        port map (
      I0 => \i__carry__0_i_1__1_n_0\,
      I1 => start_adc_count_r2(15),
      I2 => tsweep_counter_reg(15),
      O => \i__carry__0_i_2__0_n_0\
    );
\i__carry__0_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"44D4"
    )
        port map (
      I0 => tsweep_counter_reg(11),
      I1 => sweep_val(11),
      I2 => sweep_val(10),
      I3 => tsweep_counter_reg(10),
      O => \i__carry__0_i_3_n_0\
    );
\i__carry__0_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => tsweep_counter_reg(12),
      I1 => start_adc_count_r2(12),
      I2 => tsweep_counter_reg(13),
      I3 => start_adc_count_r2(13),
      I4 => start_adc_count_r2(14),
      I5 => tsweep_counter_reg(14),
      O => \i__carry__0_i_3__0_n_0\
    );
\i__carry__0_i_4__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"44D4"
    )
        port map (
      I0 => tsweep_counter_reg(9),
      I1 => sweep_val(9),
      I2 => sweep_val(8),
      I3 => tsweep_counter_reg(8),
      O => \i__carry__0_i_4__0_n_0\
    );
\i__carry__0_i_4__1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => sweep_val(15),
      O => \i__carry__0_i_4__1_n_0\
    );
\i__carry__0_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => sweep_val(15),
      I1 => tsweep_counter_reg(15),
      I2 => sweep_val(14),
      I3 => tsweep_counter_reg(14),
      O => \i__carry__0_i_5_n_0\
    );
\i__carry__0_i_5__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => sweep_val(14),
      O => \i__carry__0_i_5__0_n_0\
    );
\i__carry__0_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => sweep_val(13),
      I1 => tsweep_counter_reg(13),
      I2 => sweep_val(12),
      I3 => tsweep_counter_reg(12),
      O => \i__carry__0_i_6_n_0\
    );
\i__carry__0_i_6__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => sweep_val(13),
      O => \i__carry__0_i_6__0_n_0\
    );
\i__carry__0_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => sweep_val(11),
      I1 => tsweep_counter_reg(11),
      I2 => sweep_val(10),
      I3 => tsweep_counter_reg(10),
      O => \i__carry__0_i_7_n_0\
    );
\i__carry__0_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => sweep_val(9),
      I1 => tsweep_counter_reg(9),
      I2 => sweep_val(8),
      I3 => tsweep_counter_reg(8),
      O => \i__carry__0_i_8_n_0\
    );
\i__carry_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"44D4"
    )
        port map (
      I0 => tsweep_counter_reg(7),
      I1 => sweep_val(7),
      I2 => sweep_val(6),
      I3 => tsweep_counter_reg(6),
      O => \i__carry_i_1_n_0\
    );
\i__carry_i_10\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => sweep_val(10),
      O => \i__carry_i_10_n_0\
    );
\i__carry_i_11\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => sweep_val(9),
      O => \i__carry_i_11_n_0\
    );
\i__carry_i_12\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => sweep_val(8),
      O => \i__carry_i_12_n_0\
    );
\i__carry_i_13\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => sweep_val(7),
      O => \i__carry_i_13_n_0\
    );
\i__carry_i_14\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => sweep_val(6),
      O => \i__carry_i_14_n_0\
    );
\i__carry_i_15\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => sweep_val(5),
      O => \i__carry_i_15_n_0\
    );
\i__carry_i_16\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => sweep_val(4),
      O => \i__carry_i_16_n_0\
    );
\i__carry_i_17\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => sweep_val(3),
      O => \i__carry_i_17_n_0\
    );
\i__carry_i_18\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => sweep_val(2),
      O => \i__carry_i_18_n_0\
    );
\i__carry_i_19\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => sweep_val(1),
      O => \i__carry_i_19_n_0\
    );
\i__carry_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => tsweep_counter_reg(10),
      I1 => start_adc_count_r2(10),
      I2 => tsweep_counter_reg(9),
      I3 => start_adc_count_r2(9),
      I4 => start_adc_count_r2(11),
      I5 => tsweep_counter_reg(11),
      O => \i__carry_i_1__0_n_0\
    );
\i__carry_i_2__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"44D4"
    )
        port map (
      I0 => tsweep_counter_reg(5),
      I1 => sweep_val(5),
      I2 => sweep_val(4),
      I3 => tsweep_counter_reg(4),
      O => \i__carry_i_2__0_n_0\
    );
\i__carry_i_2__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => start_adc_count_r2(8),
      I1 => tsweep_counter_reg(8),
      I2 => tsweep_counter_reg(6),
      I3 => start_adc_count_r2(6),
      I4 => tsweep_counter_reg(7),
      I5 => start_adc_count_r2(7),
      O => \i__carry_i_2__1_n_0\
    );
\i__carry_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"44D4"
    )
        port map (
      I0 => tsweep_counter_reg(3),
      I1 => sweep_val(3),
      I2 => sweep_val(2),
      I3 => tsweep_counter_reg(2),
      O => \i__carry_i_3_n_0\
    );
\i__carry_i_3__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => tsweep_counter_reg(5),
      I1 => start_adc_count_r2(5),
      I2 => tsweep_counter_reg(3),
      I3 => start_adc_count_r2(3),
      I4 => start_adc_count_r2(4),
      I5 => tsweep_counter_reg(4),
      O => \i__carry_i_3__0_n_0\
    );
\i__carry_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4D44"
    )
        port map (
      I0 => tsweep_counter_reg(1),
      I1 => sweep_val(1),
      I2 => tsweep_counter_reg(0),
      I3 => sweep_val(0),
      O => \i__carry_i_4_n_0\
    );
\i__carry_i_4__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6006000000006006"
    )
        port map (
      I0 => sweep_val(0),
      I1 => tsweep_counter_reg(0),
      I2 => tsweep_counter_reg(2),
      I3 => start_adc_count_r2(2),
      I4 => tsweep_counter_reg(1),
      I5 => start_adc_count_r2(1),
      O => \i__carry_i_4__0_n_0\
    );
\i__carry_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => sweep_val(7),
      I1 => tsweep_counter_reg(7),
      I2 => sweep_val(6),
      I3 => tsweep_counter_reg(6),
      O => \i__carry_i_5_n_0\
    );
\i__carry_i_5__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \i__carry_i_6__0_n_0\,
      CO(3) => \i__carry_i_5__1_n_0\,
      CO(2) => \i__carry_i_5__1_n_1\,
      CO(1) => \i__carry_i_5__1_n_2\,
      CO(0) => \i__carry_i_5__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => sweep_val(12 downto 9),
      O(3 downto 0) => start_adc_count_r2(12 downto 9),
      S(3) => \i__carry_i_8__0_n_0\,
      S(2) => \i__carry_i_9_n_0\,
      S(1) => \i__carry_i_10_n_0\,
      S(0) => \i__carry_i_11_n_0\
    );
\i__carry_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => sweep_val(5),
      I1 => tsweep_counter_reg(5),
      I2 => sweep_val(4),
      I3 => tsweep_counter_reg(4),
      O => \i__carry_i_6_n_0\
    );
\i__carry_i_6__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \i__carry_i_7__0_n_0\,
      CO(3) => \i__carry_i_6__0_n_0\,
      CO(2) => \i__carry_i_6__0_n_1\,
      CO(1) => \i__carry_i_6__0_n_2\,
      CO(0) => \i__carry_i_6__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => sweep_val(8 downto 5),
      O(3 downto 0) => start_adc_count_r2(8 downto 5),
      S(3) => \i__carry_i_12_n_0\,
      S(2) => \i__carry_i_13_n_0\,
      S(1) => \i__carry_i_14_n_0\,
      S(0) => \i__carry_i_15_n_0\
    );
\i__carry_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => sweep_val(3),
      I1 => tsweep_counter_reg(3),
      I2 => sweep_val(2),
      I3 => tsweep_counter_reg(2),
      O => \i__carry_i_7_n_0\
    );
\i__carry_i_7__0\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \i__carry_i_7__0_n_0\,
      CO(2) => \i__carry_i_7__0_n_1\,
      CO(1) => \i__carry_i_7__0_n_2\,
      CO(0) => \i__carry_i_7__0_n_3\,
      CYINIT => sweep_val(0),
      DI(3 downto 0) => sweep_val(4 downto 1),
      O(3 downto 0) => start_adc_count_r2(4 downto 1),
      S(3) => \i__carry_i_16_n_0\,
      S(2) => \i__carry_i_17_n_0\,
      S(1) => \i__carry_i_18_n_0\,
      S(0) => \i__carry_i_19_n_0\
    );
\i__carry_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => sweep_val(1),
      I1 => tsweep_counter_reg(1),
      I2 => sweep_val(0),
      I3 => tsweep_counter_reg(0),
      O => \i__carry_i_8_n_0\
    );
\i__carry_i_8__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => sweep_val(12),
      O => \i__carry_i_8__0_n_0\
    );
\i__carry_i_9\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => sweep_val(11),
      O => \i__carry_i_9_n_0\
    );
pll_cen_reg_reg: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => s00_axi_aresetn,
      D => pll_cen_reg_reg_0(2),
      Q => spi_pll_cen,
      R => '0'
    );
pll_trig_reg2_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000FCFE0000"
    )
        port map (
      I0 => \^pll_trig\,
      I1 => pll_trig_reg2_i_2_n_0,
      I2 => pll_trig_reg2_i_3_n_0,
      I3 => tguard_counter,
      I4 => pll_trig_reg2_i_4_n_0,
      I5 => pll_trig_reg2_i_5_n_0,
      O => pll_trig_reg2_i_1_n_0
    );
pll_trig_reg2_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tsweep_counter2,
      I1 => \tguard_counter[15]_i_4_n_0\,
      O => pll_trig_reg2_i_2_n_0
    );
pll_trig_reg2_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000020"
    )
        port map (
      I0 => start_adc_count_r_i_2_n_0,
      I1 => \tguard_counter_reg_n_0_[7]\,
      I2 => \tguard_counter_reg_n_0_[2]\,
      I3 => pll_trig_reg2_i_6_n_0,
      I4 => pll_trig_reg2_i_7_n_0,
      I5 => pll_trig_reg2_i_8_n_0,
      O => pll_trig_reg2_i_3_n_0
    );
pll_trig_reg2_i_4: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => azimut_0,
      I1 => enable_triger,
      I2 => s00_axi_aresetn,
      O => pll_trig_reg2_i_4_n_0
    );
pll_trig_reg2_i_5: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => tsweep_counter118_in,
      I1 => \tguard_counter[15]_i_4_n_0\,
      O => pll_trig_reg2_i_5_n_0
    );
pll_trig_reg2_i_6: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \tguard_counter_reg_n_0_[4]\,
      I1 => \tguard_counter_reg_n_0_[3]\,
      O => pll_trig_reg2_i_6_n_0
    );
pll_trig_reg2_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFF7"
    )
        port map (
      I0 => \tguard_counter_reg_n_0_[5]\,
      I1 => \tguard_counter_reg_n_0_[6]\,
      I2 => \tguard_counter_reg_n_0_[1]\,
      I3 => \tguard_counter_reg_n_0_[0]\,
      O => pll_trig_reg2_i_7_n_0
    );
pll_trig_reg2_i_8: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => \tguard_counter_reg_n_0_[15]\,
      I1 => \tguard_counter_reg_n_0_[8]\,
      I2 => \tguard_counter_reg_n_0_[12]\,
      I3 => \tguard_counter_reg_n_0_[11]\,
      I4 => \tguard_counter[15]_i_8_n_0\,
      O => pll_trig_reg2_i_8_n_0
    );
pll_trig_reg2_reg: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => pll_trig_reg2_i_1_n_0,
      Q => \^pll_trig\,
      R => '0'
    );
\shift_front_from_cpu_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => s00_axi_aresetn,
      D => \shift_front_from_cpu_reg[15]_0\(0),
      Q => shift_front_from_cpu(0),
      R => '0'
    );
\shift_front_from_cpu_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => s00_axi_aresetn,
      D => \shift_front_from_cpu_reg[15]_0\(10),
      Q => shift_front_from_cpu(10),
      R => '0'
    );
\shift_front_from_cpu_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => s00_axi_aresetn,
      D => \shift_front_from_cpu_reg[15]_0\(11),
      Q => shift_front_from_cpu(11),
      R => '0'
    );
\shift_front_from_cpu_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => s00_axi_aresetn,
      D => \shift_front_from_cpu_reg[15]_0\(12),
      Q => shift_front_from_cpu(12),
      R => '0'
    );
\shift_front_from_cpu_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => s00_axi_aresetn,
      D => \shift_front_from_cpu_reg[15]_0\(13),
      Q => shift_front_from_cpu(13),
      R => '0'
    );
\shift_front_from_cpu_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => s00_axi_aresetn,
      D => \shift_front_from_cpu_reg[15]_0\(14),
      Q => shift_front_from_cpu(14),
      R => '0'
    );
\shift_front_from_cpu_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => s00_axi_aresetn,
      D => \shift_front_from_cpu_reg[15]_0\(15),
      Q => shift_front_from_cpu(15),
      R => '0'
    );
\shift_front_from_cpu_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => s00_axi_aresetn,
      D => \shift_front_from_cpu_reg[15]_0\(1),
      Q => shift_front_from_cpu(1),
      R => '0'
    );
\shift_front_from_cpu_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => s00_axi_aresetn,
      D => \shift_front_from_cpu_reg[15]_0\(2),
      Q => shift_front_from_cpu(2),
      R => '0'
    );
\shift_front_from_cpu_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => s00_axi_aresetn,
      D => \shift_front_from_cpu_reg[15]_0\(3),
      Q => shift_front_from_cpu(3),
      R => '0'
    );
\shift_front_from_cpu_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => s00_axi_aresetn,
      D => \shift_front_from_cpu_reg[15]_0\(4),
      Q => shift_front_from_cpu(4),
      R => '0'
    );
\shift_front_from_cpu_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => s00_axi_aresetn,
      D => \shift_front_from_cpu_reg[15]_0\(5),
      Q => shift_front_from_cpu(5),
      R => '0'
    );
\shift_front_from_cpu_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => s00_axi_aresetn,
      D => \shift_front_from_cpu_reg[15]_0\(6),
      Q => shift_front_from_cpu(6),
      R => '0'
    );
\shift_front_from_cpu_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => s00_axi_aresetn,
      D => \shift_front_from_cpu_reg[15]_0\(7),
      Q => shift_front_from_cpu(7),
      R => '0'
    );
\shift_front_from_cpu_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => s00_axi_aresetn,
      D => \shift_front_from_cpu_reg[15]_0\(8),
      Q => shift_front_from_cpu(8),
      R => '0'
    );
\shift_front_from_cpu_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => s00_axi_aresetn,
      D => \shift_front_from_cpu_reg[15]_0\(9),
      Q => shift_front_from_cpu(9),
      R => '0'
    );
spi_hmc_mode_RX_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_spi_hmc_mode_RX
     port map (
      DATA_receive_ila(23 downto 0) => DATA_receive_ila(23 downto 0),
      \axi_rdata_reg[0]\(1 downto 0) => \axi_rdata_reg[0]\(1 downto 0),
      \axi_rdata_reg[0]_0\ => \axi_rdata_reg[0]_0\,
      clk_10MHz => clk_10MHz,
      \data_r_reg[0]_0\ => \data_r_reg[0]\,
      enable_sck => enable_sck,
      mosi_r_reg_0 => mosi_r_reg_0,
      \out\(2 downto 0) => \out\(2 downto 0),
      pll_data_ready_RX_ila => pll_data_ready_RX_ila,
      pll_data_ready_TX_ila => \^pll_data_ready_tx_ila\,
      pll_mosi_read => pll_mosi_read,
      pll_sen_read => pll_sen_read,
      ready_r_reg_0 => \^start_receive_reg_0\,
      spi_pll_ld_sdo => spi_pll_ld_sdo
    );
spi_hmc_mode_TX_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_spi_hmc_mode_TX
     port map (
      clk_10MHz => clk_10MHz,
      \cnt_reg[5]_0\ => \cnt_reg[5]\,
      enable_sck => enable_sck,
      mosi_r_reg_0 => mosi_r_reg,
      mosi_r_reg_1 => mosi_r_reg_1,
      \out\(4 downto 0) => \cnt_reg[4]\(4 downto 0),
      pll_data_ready_TX_ila => \^pll_data_ready_tx_ila\,
      pll_mosi_read => pll_mosi_read,
      pll_sen_read => pll_sen_read,
      ready_tx_r_reg_0 => \^start_send_reg_0\,
      reg_address_send_ila(0) => reg_address_send_ila(0),
      spi_pll_mosi => spi_pll_mosi,
      spi_pll_sck => spi_pll_sck,
      spi_pll_sen => spi_pll_sen
    );
start_adc_count_r0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => start_adc_count_r0_carry_n_0,
      CO(2) => start_adc_count_r0_carry_n_1,
      CO(1) => start_adc_count_r0_carry_n_2,
      CO(0) => start_adc_count_r0_carry_n_3,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => NLW_start_adc_count_r0_carry_O_UNCONNECTED(3 downto 0),
      S(3) => start_adc_count_r0_carry_i_1_n_0,
      S(2) => start_adc_count_r0_carry_i_2_n_0,
      S(1) => start_adc_count_r0_carry_i_3_n_0,
      S(0) => start_adc_count_r0_carry_i_4_n_0
    );
\start_adc_count_r0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => start_adc_count_r0_carry_n_0,
      CO(3 downto 2) => \NLW_start_adc_count_r0_carry__0_CO_UNCONNECTED\(3 downto 2),
      CO(1) => start_adc_count_r0,
      CO(0) => \start_adc_count_r0_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_start_adc_count_r0_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3 downto 2) => B"00",
      S(1) => \start_adc_count_r0_carry__0_i_1_n_0\,
      S(0) => \start_adc_count_r0_carry__0_i_2_n_0\
    );
\start_adc_count_r0_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => shift_front_from_cpu(15),
      I1 => tsweep_counter_reg(15),
      O => \start_adc_count_r0_carry__0_i_1_n_0\
    );
\start_adc_count_r0_carry__0_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => tsweep_counter_reg(14),
      I1 => shift_front_from_cpu(14),
      I2 => tsweep_counter_reg(12),
      I3 => shift_front_from_cpu(12),
      I4 => shift_front_from_cpu(13),
      I5 => tsweep_counter_reg(13),
      O => \start_adc_count_r0_carry__0_i_2_n_0\
    );
start_adc_count_r0_carry_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => tsweep_counter_reg(11),
      I1 => shift_front_from_cpu(11),
      I2 => tsweep_counter_reg(9),
      I3 => shift_front_from_cpu(9),
      I4 => shift_front_from_cpu(10),
      I5 => tsweep_counter_reg(10),
      O => start_adc_count_r0_carry_i_1_n_0
    );
start_adc_count_r0_carry_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => shift_front_from_cpu(8),
      I1 => tsweep_counter_reg(8),
      I2 => tsweep_counter_reg(6),
      I3 => shift_front_from_cpu(6),
      I4 => tsweep_counter_reg(7),
      I5 => shift_front_from_cpu(7),
      O => start_adc_count_r0_carry_i_2_n_0
    );
start_adc_count_r0_carry_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => tsweep_counter_reg(3),
      I1 => shift_front_from_cpu(3),
      I2 => tsweep_counter_reg(4),
      I3 => shift_front_from_cpu(4),
      I4 => shift_front_from_cpu(5),
      I5 => tsweep_counter_reg(5),
      O => start_adc_count_r0_carry_i_3_n_0
    );
start_adc_count_r0_carry_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => tsweep_counter_reg(1),
      I1 => shift_front_from_cpu(1),
      I2 => tsweep_counter_reg(0),
      I3 => shift_front_from_cpu(0),
      I4 => shift_front_from_cpu(2),
      I5 => tsweep_counter_reg(2),
      O => start_adc_count_r0_carry_i_4_n_0
    );
\start_adc_count_r1_inferred__0/i__carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \start_adc_count_r1_inferred__0/i__carry_n_0\,
      CO(2) => \start_adc_count_r1_inferred__0/i__carry_n_1\,
      CO(1) => \start_adc_count_r1_inferred__0/i__carry_n_2\,
      CO(0) => \start_adc_count_r1_inferred__0/i__carry_n_3\,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_start_adc_count_r1_inferred__0/i__carry_O_UNCONNECTED\(3 downto 0),
      S(3) => \i__carry_i_1__0_n_0\,
      S(2) => \i__carry_i_2__1_n_0\,
      S(1) => \i__carry_i_3__0_n_0\,
      S(0) => \i__carry_i_4__0_n_0\
    );
\start_adc_count_r1_inferred__0/i__carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \start_adc_count_r1_inferred__0/i__carry_n_0\,
      CO(3) => \start_adc_count_r1_inferred__0/i__carry__0_n_0\,
      CO(2) => \start_adc_count_r1_inferred__0/i__carry__0_n_1\,
      CO(1) => \start_adc_count_r1_inferred__0/i__carry__0_n_2\,
      CO(0) => \start_adc_count_r1_inferred__0/i__carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_start_adc_count_r1_inferred__0/i__carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => \i__carry__0_i_1__1_n_0\,
      S(2) => \i__carry__0_i_1__1_n_0\,
      S(1) => \i__carry__0_i_2__0_n_0\,
      S(0) => \i__carry__0_i_3__0_n_0\
    );
\start_adc_count_r1_inferred__0/i__carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \start_adc_count_r1_inferred__0/i__carry__0_n_0\,
      CO(3) => \NLW_start_adc_count_r1_inferred__0/i__carry__1_CO_UNCONNECTED\(3),
      CO(2) => start_adc_count_r10_in,
      CO(1) => \start_adc_count_r1_inferred__0/i__carry__1_n_2\,
      CO(0) => \start_adc_count_r1_inferred__0/i__carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_start_adc_count_r1_inferred__0/i__carry__1_O_UNCONNECTED\(3 downto 0),
      S(3) => '0',
      S(2) => \i__carry__0_i_1__1_n_0\,
      S(1) => \i__carry__0_i_1__1_n_0\,
      S(0) => \i__carry__0_i_1__1_n_0\
    );
start_adc_count_r_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"000E0000"
    )
        port map (
      I0 => \^start_adc_count\,
      I1 => start_adc_count_r0,
      I2 => start_adc_count_r10_in,
      I3 => start_adc_count_r_i_2_n_0,
      I4 => s00_axi_aresetn,
      O => start_adc_count_r_i_1_n_0
    );
start_adc_count_r_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => start_adc_count_r_i_3_n_0,
      I1 => tsweep_counter_reg(3),
      I2 => tsweep_counter_reg(0),
      I3 => tsweep_counter_reg(2),
      I4 => tsweep_counter_reg(1),
      I5 => start_adc_count_r_i_4_n_0,
      O => start_adc_count_r_i_2_n_0
    );
start_adc_count_r_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => tsweep_counter_reg(6),
      I1 => tsweep_counter_reg(5),
      I2 => tsweep_counter_reg(7),
      I3 => tsweep_counter_reg(4),
      O => start_adc_count_r_i_3_n_0
    );
start_adc_count_r_i_4: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => tsweep_counter_reg(8),
      I1 => tsweep_counter_reg(9),
      I2 => tsweep_counter_reg(10),
      I3 => tsweep_counter_reg(11),
      I4 => start_adc_count_r_i_5_n_0,
      O => start_adc_count_r_i_4_n_0
    );
start_adc_count_r_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => tsweep_counter_reg(14),
      I1 => tsweep_counter_reg(13),
      I2 => tsweep_counter_reg(15),
      I3 => tsweep_counter_reg(12),
      O => start_adc_count_r_i_5_n_0
    );
start_adc_count_r_reg: unisim.vcomponents.FDRE
     port map (
      C => clkDCO_10MHz,
      CE => '1',
      D => start_adc_count_r_i_1_n_0,
      Q => \^start_adc_count\,
      R => '0'
    );
\sweep_val_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sweep_val_tmp(0),
      Q => sweep_val(0),
      R => SS(0)
    );
\sweep_val_reg[10]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sweep_val_tmp(10),
      Q => sweep_val(10),
      S => SS(0)
    );
\sweep_val_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sweep_val_tmp(11),
      Q => sweep_val(11),
      R => SS(0)
    );
\sweep_val_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sweep_val_tmp(12),
      Q => sweep_val(12),
      R => SS(0)
    );
\sweep_val_reg[13]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sweep_val_tmp(13),
      Q => sweep_val(13),
      S => SS(0)
    );
\sweep_val_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sweep_val_tmp(14),
      Q => sweep_val(14),
      R => SS(0)
    );
\sweep_val_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sweep_val_tmp(15),
      Q => sweep_val(15),
      R => SS(0)
    );
\sweep_val_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sweep_val_tmp(1),
      Q => sweep_val(1),
      R => SS(0)
    );
\sweep_val_reg[2]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sweep_val_tmp(2),
      Q => sweep_val(2),
      S => SS(0)
    );
\sweep_val_reg[3]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sweep_val_tmp(3),
      Q => sweep_val(3),
      S => SS(0)
    );
\sweep_val_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sweep_val_tmp(4),
      Q => sweep_val(4),
      R => SS(0)
    );
\sweep_val_reg[5]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sweep_val_tmp(5),
      Q => sweep_val(5),
      S => SS(0)
    );
\sweep_val_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sweep_val_tmp(6),
      Q => sweep_val(6),
      R => SS(0)
    );
\sweep_val_reg[7]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sweep_val_tmp(7),
      Q => sweep_val(7),
      S => SS(0)
    );
\sweep_val_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sweep_val_tmp(8),
      Q => sweep_val(8),
      R => SS(0)
    );
\sweep_val_reg[9]\: unisim.vcomponents.FDSE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sweep_val_tmp(9),
      Q => sweep_val(9),
      S => SS(0)
    );
\sweep_val_tmp_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => s00_axi_aresetn,
      D => D(0),
      Q => sweep_val_tmp(0),
      R => '0'
    );
\sweep_val_tmp_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => s00_axi_aresetn,
      D => D(10),
      Q => sweep_val_tmp(10),
      R => '0'
    );
\sweep_val_tmp_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => s00_axi_aresetn,
      D => D(11),
      Q => sweep_val_tmp(11),
      R => '0'
    );
\sweep_val_tmp_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => s00_axi_aresetn,
      D => D(12),
      Q => sweep_val_tmp(12),
      R => '0'
    );
\sweep_val_tmp_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => s00_axi_aresetn,
      D => D(13),
      Q => sweep_val_tmp(13),
      R => '0'
    );
\sweep_val_tmp_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => s00_axi_aresetn,
      D => D(14),
      Q => sweep_val_tmp(14),
      R => '0'
    );
\sweep_val_tmp_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => s00_axi_aresetn,
      D => D(15),
      Q => sweep_val_tmp(15),
      R => '0'
    );
\sweep_val_tmp_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => s00_axi_aresetn,
      D => D(1),
      Q => sweep_val_tmp(1),
      R => '0'
    );
\sweep_val_tmp_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => s00_axi_aresetn,
      D => D(2),
      Q => sweep_val_tmp(2),
      R => '0'
    );
\sweep_val_tmp_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => s00_axi_aresetn,
      D => D(3),
      Q => sweep_val_tmp(3),
      R => '0'
    );
\sweep_val_tmp_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => s00_axi_aresetn,
      D => D(4),
      Q => sweep_val_tmp(4),
      R => '0'
    );
\sweep_val_tmp_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => s00_axi_aresetn,
      D => D(5),
      Q => sweep_val_tmp(5),
      R => '0'
    );
\sweep_val_tmp_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => s00_axi_aresetn,
      D => D(6),
      Q => sweep_val_tmp(6),
      R => '0'
    );
\sweep_val_tmp_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => s00_axi_aresetn,
      D => D(7),
      Q => sweep_val_tmp(7),
      R => '0'
    );
\sweep_val_tmp_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => s00_axi_aresetn,
      D => D(8),
      Q => sweep_val_tmp(8),
      R => '0'
    );
\sweep_val_tmp_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s00_axi_aclk,
      CE => s00_axi_aresetn,
      D => D(9),
      Q => sweep_val_tmp(9),
      R => '0'
    );
\tguard_counter[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"12FF000012120000"
    )
        port map (
      I0 => \tguard_counter_reg_n_0_[0]\,
      I1 => pll_trig_reg2_i_3_n_0,
      I2 => tguard_counter,
      I3 => tsweep_counter118_in,
      I4 => pll_trig_reg2_i_4_n_0,
      I5 => pll_trig_reg2_i_2_n_0,
      O => \tguard_counter[0]_i_1_n_0\
    );
\tguard_counter[15]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0F4FFFF"
    )
        port map (
      I0 => \tguard_counter[15]_i_4_n_0\,
      I1 => tsweep_counter2,
      I2 => pll_trig_reg2_i_3_n_0,
      I3 => tsweep_counter118_in,
      I4 => pll_trig_reg2_i_4_n_0,
      O => \tguard_counter[15]_i_1_n_0\
    );
\tguard_counter[15]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000000000AAAAAA8"
    )
        port map (
      I0 => start_adc_count_r_i_2_n_0,
      I1 => \tguard_counter[15]_i_5_n_0\,
      I2 => \tguard_counter_reg_n_0_[5]\,
      I3 => \tguard_counter_reg_n_0_[6]\,
      I4 => \tguard_counter[15]_i_6_n_0\,
      I5 => \tguard_counter[15]_i_7_n_0\,
      O => tguard_counter
    );
\tguard_counter[15]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \tguard_counter[15]_i_7_n_0\,
      I1 => \tguard_counter_reg_n_0_[1]\,
      I2 => \tguard_counter_reg_n_0_[0]\,
      I3 => \tguard_counter_reg_n_0_[5]\,
      I4 => \tguard_counter_reg_n_0_[6]\,
      I5 => \tguard_counter[15]_i_6_n_0\,
      O => \tguard_counter[15]_i_4_n_0\
    );
\tguard_counter[15]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \tguard_counter_reg_n_0_[0]\,
      I1 => \tguard_counter_reg_n_0_[1]\,
      O => \tguard_counter[15]_i_5_n_0\
    );
\tguard_counter[15]_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => \tguard_counter_reg_n_0_[2]\,
      I1 => \tguard_counter_reg_n_0_[3]\,
      I2 => \tguard_counter_reg_n_0_[4]\,
      O => \tguard_counter[15]_i_6_n_0\
    );
\tguard_counter[15]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \tguard_counter_reg_n_0_[7]\,
      I1 => \tguard_counter[15]_i_8_n_0\,
      I2 => \tguard_counter_reg_n_0_[11]\,
      I3 => \tguard_counter_reg_n_0_[12]\,
      I4 => \tguard_counter_reg_n_0_[8]\,
      I5 => \tguard_counter_reg_n_0_[15]\,
      O => \tguard_counter[15]_i_7_n_0\
    );
\tguard_counter[15]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \tguard_counter_reg_n_0_[10]\,
      I1 => \tguard_counter_reg_n_0_[13]\,
      I2 => \tguard_counter_reg_n_0_[9]\,
      I3 => \tguard_counter_reg_n_0_[14]\,
      O => \tguard_counter[15]_i_8_n_0\
    );
\tguard_counter_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \tguard_counter[0]_i_1_n_0\,
      Q => \tguard_counter_reg_n_0_[0]\,
      R => '0'
    );
\tguard_counter_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => tguard_counter,
      D => p_2_in(10),
      Q => \tguard_counter_reg_n_0_[10]\,
      R => \tguard_counter[15]_i_1_n_0\
    );
\tguard_counter_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => tguard_counter,
      D => p_2_in(11),
      Q => \tguard_counter_reg_n_0_[11]\,
      R => \tguard_counter[15]_i_1_n_0\
    );
\tguard_counter_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => tguard_counter,
      D => p_2_in(12),
      Q => \tguard_counter_reg_n_0_[12]\,
      R => \tguard_counter[15]_i_1_n_0\
    );
\tguard_counter_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \tguard_counter_reg[8]_i_1_n_0\,
      CO(3) => \tguard_counter_reg[12]_i_1_n_0\,
      CO(2) => \tguard_counter_reg[12]_i_1_n_1\,
      CO(1) => \tguard_counter_reg[12]_i_1_n_2\,
      CO(0) => \tguard_counter_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => p_2_in(12 downto 9),
      S(3) => \tguard_counter_reg_n_0_[12]\,
      S(2) => \tguard_counter_reg_n_0_[11]\,
      S(1) => \tguard_counter_reg_n_0_[10]\,
      S(0) => \tguard_counter_reg_n_0_[9]\
    );
\tguard_counter_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => tguard_counter,
      D => p_2_in(13),
      Q => \tguard_counter_reg_n_0_[13]\,
      R => \tguard_counter[15]_i_1_n_0\
    );
\tguard_counter_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => tguard_counter,
      D => p_2_in(14),
      Q => \tguard_counter_reg_n_0_[14]\,
      R => \tguard_counter[15]_i_1_n_0\
    );
\tguard_counter_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => tguard_counter,
      D => p_2_in(15),
      Q => \tguard_counter_reg_n_0_[15]\,
      R => \tguard_counter[15]_i_1_n_0\
    );
\tguard_counter_reg[15]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => \tguard_counter_reg[12]_i_1_n_0\,
      CO(3 downto 2) => \NLW_tguard_counter_reg[15]_i_3_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \tguard_counter_reg[15]_i_3_n_2\,
      CO(0) => \tguard_counter_reg[15]_i_3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \NLW_tguard_counter_reg[15]_i_3_O_UNCONNECTED\(3),
      O(2 downto 0) => p_2_in(15 downto 13),
      S(3) => '0',
      S(2) => \tguard_counter_reg_n_0_[15]\,
      S(1) => \tguard_counter_reg_n_0_[14]\,
      S(0) => \tguard_counter_reg_n_0_[13]\
    );
\tguard_counter_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => tguard_counter,
      D => p_2_in(1),
      Q => \tguard_counter_reg_n_0_[1]\,
      R => \tguard_counter[15]_i_1_n_0\
    );
\tguard_counter_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => tguard_counter,
      D => p_2_in(2),
      Q => \tguard_counter_reg_n_0_[2]\,
      R => \tguard_counter[15]_i_1_n_0\
    );
\tguard_counter_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => tguard_counter,
      D => p_2_in(3),
      Q => \tguard_counter_reg_n_0_[3]\,
      R => \tguard_counter[15]_i_1_n_0\
    );
\tguard_counter_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => tguard_counter,
      D => p_2_in(4),
      Q => \tguard_counter_reg_n_0_[4]\,
      R => \tguard_counter[15]_i_1_n_0\
    );
\tguard_counter_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \tguard_counter_reg[4]_i_1_n_0\,
      CO(2) => \tguard_counter_reg[4]_i_1_n_1\,
      CO(1) => \tguard_counter_reg[4]_i_1_n_2\,
      CO(0) => \tguard_counter_reg[4]_i_1_n_3\,
      CYINIT => \tguard_counter_reg_n_0_[0]\,
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => p_2_in(4 downto 1),
      S(3) => \tguard_counter_reg_n_0_[4]\,
      S(2) => \tguard_counter_reg_n_0_[3]\,
      S(1) => \tguard_counter_reg_n_0_[2]\,
      S(0) => \tguard_counter_reg_n_0_[1]\
    );
\tguard_counter_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => tguard_counter,
      D => p_2_in(5),
      Q => \tguard_counter_reg_n_0_[5]\,
      R => \tguard_counter[15]_i_1_n_0\
    );
\tguard_counter_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => tguard_counter,
      D => p_2_in(6),
      Q => \tguard_counter_reg_n_0_[6]\,
      R => \tguard_counter[15]_i_1_n_0\
    );
\tguard_counter_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => tguard_counter,
      D => p_2_in(7),
      Q => \tguard_counter_reg_n_0_[7]\,
      R => \tguard_counter[15]_i_1_n_0\
    );
\tguard_counter_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => tguard_counter,
      D => p_2_in(8),
      Q => \tguard_counter_reg_n_0_[8]\,
      R => \tguard_counter[15]_i_1_n_0\
    );
\tguard_counter_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \tguard_counter_reg[4]_i_1_n_0\,
      CO(3) => \tguard_counter_reg[8]_i_1_n_0\,
      CO(2) => \tguard_counter_reg[8]_i_1_n_1\,
      CO(1) => \tguard_counter_reg[8]_i_1_n_2\,
      CO(0) => \tguard_counter_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => p_2_in(8 downto 5),
      S(3) => \tguard_counter_reg_n_0_[8]\,
      S(2) => \tguard_counter_reg_n_0_[7]\,
      S(1) => \tguard_counter_reg_n_0_[6]\,
      S(0) => \tguard_counter_reg_n_0_[5]\
    );
\tguard_counter_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => tguard_counter,
      D => p_2_in(9),
      Q => \tguard_counter_reg_n_0_[9]\,
      R => \tguard_counter[15]_i_1_n_0\
    );
\tsweep_counter1_inferred__0/i__carry\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \tsweep_counter1_inferred__0/i__carry_n_0\,
      CO(2) => \tsweep_counter1_inferred__0/i__carry_n_1\,
      CO(1) => \tsweep_counter1_inferred__0/i__carry_n_2\,
      CO(0) => \tsweep_counter1_inferred__0/i__carry_n_3\,
      CYINIT => '0',
      DI(3) => \i__carry_i_1_n_0\,
      DI(2) => \i__carry_i_2__0_n_0\,
      DI(1) => \i__carry_i_3_n_0\,
      DI(0) => \i__carry_i_4_n_0\,
      O(3 downto 0) => \NLW_tsweep_counter1_inferred__0/i__carry_O_UNCONNECTED\(3 downto 0),
      S(3) => \i__carry_i_5_n_0\,
      S(2) => \i__carry_i_6_n_0\,
      S(1) => \i__carry_i_7_n_0\,
      S(0) => \i__carry_i_8_n_0\
    );
\tsweep_counter1_inferred__0/i__carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => \tsweep_counter1_inferred__0/i__carry_n_0\,
      CO(3) => tsweep_counter118_in,
      CO(2) => \tsweep_counter1_inferred__0/i__carry__0_n_1\,
      CO(1) => \tsweep_counter1_inferred__0/i__carry__0_n_2\,
      CO(0) => \tsweep_counter1_inferred__0/i__carry__0_n_3\,
      CYINIT => '0',
      DI(3) => \i__carry__0_i_1__0_n_0\,
      DI(2) => \i__carry__0_i_2_n_0\,
      DI(1) => \i__carry__0_i_3_n_0\,
      DI(0) => \i__carry__0_i_4__0_n_0\,
      O(3 downto 0) => \NLW_tsweep_counter1_inferred__0/i__carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => \i__carry__0_i_5_n_0\,
      S(2) => \i__carry__0_i_6_n_0\,
      S(1) => \i__carry__0_i_7_n_0\,
      S(0) => \i__carry__0_i_8_n_0\
    );
tsweep_counter2_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => tsweep_counter2_carry_n_0,
      CO(2) => tsweep_counter2_carry_n_1,
      CO(1) => tsweep_counter2_carry_n_2,
      CO(0) => tsweep_counter2_carry_n_3,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => NLW_tsweep_counter2_carry_O_UNCONNECTED(3 downto 0),
      S(3) => tsweep_counter2_carry_i_1_n_0,
      S(2) => tsweep_counter2_carry_i_2_n_0,
      S(1) => tsweep_counter2_carry_i_3_n_0,
      S(0) => tsweep_counter2_carry_i_4_n_0
    );
\tsweep_counter2_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => tsweep_counter2_carry_n_0,
      CO(3 downto 2) => \NLW_tsweep_counter2_carry__0_CO_UNCONNECTED\(3 downto 2),
      CO(1) => tsweep_counter2,
      CO(0) => \tsweep_counter2_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_tsweep_counter2_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3 downto 2) => B"00",
      S(1) => \tsweep_counter2_carry__0_i_1_n_0\,
      S(0) => \tsweep_counter2_carry__0_i_2_n_0\
    );
\tsweep_counter2_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => sweep_val(15),
      I1 => tsweep_counter_reg(15),
      O => \tsweep_counter2_carry__0_i_1_n_0\
    );
\tsweep_counter2_carry__0_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => tsweep_counter_reg(12),
      I1 => sweep_val(12),
      I2 => tsweep_counter_reg(13),
      I3 => sweep_val(13),
      I4 => sweep_val(14),
      I5 => tsweep_counter_reg(14),
      O => \tsweep_counter2_carry__0_i_2_n_0\
    );
tsweep_counter2_carry_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => tsweep_counter_reg(10),
      I1 => sweep_val(10),
      I2 => tsweep_counter_reg(11),
      I3 => sweep_val(11),
      I4 => sweep_val(9),
      I5 => tsweep_counter_reg(9),
      O => tsweep_counter2_carry_i_1_n_0
    );
tsweep_counter2_carry_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => tsweep_counter_reg(6),
      I1 => sweep_val(6),
      I2 => tsweep_counter_reg(7),
      I3 => sweep_val(7),
      I4 => sweep_val(8),
      I5 => tsweep_counter_reg(8),
      O => tsweep_counter2_carry_i_2_n_0
    );
tsweep_counter2_carry_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => tsweep_counter_reg(4),
      I1 => sweep_val(4),
      I2 => tsweep_counter_reg(5),
      I3 => sweep_val(5),
      I4 => sweep_val(3),
      I5 => tsweep_counter_reg(3),
      O => tsweep_counter2_carry_i_3_n_0
    );
tsweep_counter2_carry_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => tsweep_counter_reg(0),
      I1 => sweep_val(0),
      I2 => tsweep_counter_reg(1),
      I3 => sweep_val(1),
      I4 => sweep_val(2),
      I5 => tsweep_counter_reg(2),
      O => tsweep_counter2_carry_i_4_n_0
    );
\tsweep_counter[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7F7F7F7F7F7F7F77"
    )
        port map (
      I0 => enable_triger,
      I1 => s00_axi_aresetn,
      I2 => \tsweep_counter[0]_i_2_n_0\,
      I3 => pll_trig_reg2_i_2_n_0,
      I4 => pll_trig_reg2_i_3_n_0,
      I5 => tguard_counter,
      O => \tsweep_counter[0]_i_1_n_0\
    );
\tsweep_counter[0]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"BA"
    )
        port map (
      I0 => azimut_0,
      I1 => \tguard_counter[15]_i_4_n_0\,
      I2 => tsweep_counter118_in,
      O => \tsweep_counter[0]_i_2_n_0\
    );
\tsweep_counter[0]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => azimut_0,
      O => p_4_in
    );
\tsweep_counter[0]_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => sweep_val(3),
      I1 => azimut_0,
      I2 => tsweep_counter_reg(3),
      O => \tsweep_counter[0]_i_5_n_0\
    );
\tsweep_counter[0]_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => sweep_val(2),
      I1 => azimut_0,
      I2 => tsweep_counter_reg(2),
      O => \tsweep_counter[0]_i_6_n_0\
    );
\tsweep_counter[0]_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => sweep_val(1),
      I1 => azimut_0,
      I2 => tsweep_counter_reg(1),
      O => \tsweep_counter[0]_i_7_n_0\
    );
\tsweep_counter[0]_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"C5"
    )
        port map (
      I0 => tsweep_counter_reg(0),
      I1 => sweep_val(0),
      I2 => azimut_0,
      O => \tsweep_counter[0]_i_8_n_0\
    );
\tsweep_counter[12]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => sweep_val(15),
      I1 => azimut_0,
      I2 => tsweep_counter_reg(15),
      O => \tsweep_counter[12]_i_2_n_0\
    );
\tsweep_counter[12]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => sweep_val(14),
      I1 => azimut_0,
      I2 => tsweep_counter_reg(14),
      O => \tsweep_counter[12]_i_3_n_0\
    );
\tsweep_counter[12]_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => sweep_val(13),
      I1 => azimut_0,
      I2 => tsweep_counter_reg(13),
      O => \tsweep_counter[12]_i_4_n_0\
    );
\tsweep_counter[12]_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => sweep_val(12),
      I1 => azimut_0,
      I2 => tsweep_counter_reg(12),
      O => \tsweep_counter[12]_i_5_n_0\
    );
\tsweep_counter[4]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => sweep_val(7),
      I1 => azimut_0,
      I2 => tsweep_counter_reg(7),
      O => \tsweep_counter[4]_i_2_n_0\
    );
\tsweep_counter[4]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => sweep_val(6),
      I1 => azimut_0,
      I2 => tsweep_counter_reg(6),
      O => \tsweep_counter[4]_i_3_n_0\
    );
\tsweep_counter[4]_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => sweep_val(5),
      I1 => azimut_0,
      I2 => tsweep_counter_reg(5),
      O => \tsweep_counter[4]_i_4_n_0\
    );
\tsweep_counter[4]_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => sweep_val(4),
      I1 => azimut_0,
      I2 => tsweep_counter_reg(4),
      O => \tsweep_counter[4]_i_5_n_0\
    );
\tsweep_counter[8]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => sweep_val(11),
      I1 => azimut_0,
      I2 => tsweep_counter_reg(11),
      O => \tsweep_counter[8]_i_2_n_0\
    );
\tsweep_counter[8]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => sweep_val(10),
      I1 => azimut_0,
      I2 => tsweep_counter_reg(10),
      O => \tsweep_counter[8]_i_3_n_0\
    );
\tsweep_counter[8]_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => sweep_val(9),
      I1 => azimut_0,
      I2 => tsweep_counter_reg(9),
      O => \tsweep_counter[8]_i_4_n_0\
    );
\tsweep_counter[8]_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => sweep_val(8),
      I1 => azimut_0,
      I2 => tsweep_counter_reg(8),
      O => \tsweep_counter[8]_i_5_n_0\
    );
\tsweep_counter_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => \tsweep_counter[0]_i_2_n_0\,
      D => \tsweep_counter_reg[0]_i_3_n_7\,
      Q => tsweep_counter_reg(0),
      R => \tsweep_counter[0]_i_1_n_0\
    );
\tsweep_counter_reg[0]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \tsweep_counter_reg[0]_i_3_n_0\,
      CO(2) => \tsweep_counter_reg[0]_i_3_n_1\,
      CO(1) => \tsweep_counter_reg[0]_i_3_n_2\,
      CO(0) => \tsweep_counter_reg[0]_i_3_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => p_4_in,
      O(3) => \tsweep_counter_reg[0]_i_3_n_4\,
      O(2) => \tsweep_counter_reg[0]_i_3_n_5\,
      O(1) => \tsweep_counter_reg[0]_i_3_n_6\,
      O(0) => \tsweep_counter_reg[0]_i_3_n_7\,
      S(3) => \tsweep_counter[0]_i_5_n_0\,
      S(2) => \tsweep_counter[0]_i_6_n_0\,
      S(1) => \tsweep_counter[0]_i_7_n_0\,
      S(0) => \tsweep_counter[0]_i_8_n_0\
    );
\tsweep_counter_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => \tsweep_counter[0]_i_2_n_0\,
      D => \tsweep_counter_reg[8]_i_1_n_5\,
      Q => tsweep_counter_reg(10),
      R => \tsweep_counter[0]_i_1_n_0\
    );
\tsweep_counter_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => \tsweep_counter[0]_i_2_n_0\,
      D => \tsweep_counter_reg[8]_i_1_n_4\,
      Q => tsweep_counter_reg(11),
      R => \tsweep_counter[0]_i_1_n_0\
    );
\tsweep_counter_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => \tsweep_counter[0]_i_2_n_0\,
      D => \tsweep_counter_reg[12]_i_1_n_7\,
      Q => tsweep_counter_reg(12),
      R => \tsweep_counter[0]_i_1_n_0\
    );
\tsweep_counter_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \tsweep_counter_reg[8]_i_1_n_0\,
      CO(3) => \NLW_tsweep_counter_reg[12]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \tsweep_counter_reg[12]_i_1_n_1\,
      CO(1) => \tsweep_counter_reg[12]_i_1_n_2\,
      CO(0) => \tsweep_counter_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \tsweep_counter_reg[12]_i_1_n_4\,
      O(2) => \tsweep_counter_reg[12]_i_1_n_5\,
      O(1) => \tsweep_counter_reg[12]_i_1_n_6\,
      O(0) => \tsweep_counter_reg[12]_i_1_n_7\,
      S(3) => \tsweep_counter[12]_i_2_n_0\,
      S(2) => \tsweep_counter[12]_i_3_n_0\,
      S(1) => \tsweep_counter[12]_i_4_n_0\,
      S(0) => \tsweep_counter[12]_i_5_n_0\
    );
\tsweep_counter_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => \tsweep_counter[0]_i_2_n_0\,
      D => \tsweep_counter_reg[12]_i_1_n_6\,
      Q => tsweep_counter_reg(13),
      R => \tsweep_counter[0]_i_1_n_0\
    );
\tsweep_counter_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => \tsweep_counter[0]_i_2_n_0\,
      D => \tsweep_counter_reg[12]_i_1_n_5\,
      Q => tsweep_counter_reg(14),
      R => \tsweep_counter[0]_i_1_n_0\
    );
\tsweep_counter_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => \tsweep_counter[0]_i_2_n_0\,
      D => \tsweep_counter_reg[12]_i_1_n_4\,
      Q => tsweep_counter_reg(15),
      R => \tsweep_counter[0]_i_1_n_0\
    );
\tsweep_counter_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => \tsweep_counter[0]_i_2_n_0\,
      D => \tsweep_counter_reg[0]_i_3_n_6\,
      Q => tsweep_counter_reg(1),
      R => \tsweep_counter[0]_i_1_n_0\
    );
\tsweep_counter_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => \tsweep_counter[0]_i_2_n_0\,
      D => \tsweep_counter_reg[0]_i_3_n_5\,
      Q => tsweep_counter_reg(2),
      R => \tsweep_counter[0]_i_1_n_0\
    );
\tsweep_counter_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => \tsweep_counter[0]_i_2_n_0\,
      D => \tsweep_counter_reg[0]_i_3_n_4\,
      Q => tsweep_counter_reg(3),
      R => \tsweep_counter[0]_i_1_n_0\
    );
\tsweep_counter_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => \tsweep_counter[0]_i_2_n_0\,
      D => \tsweep_counter_reg[4]_i_1_n_7\,
      Q => tsweep_counter_reg(4),
      R => \tsweep_counter[0]_i_1_n_0\
    );
\tsweep_counter_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \tsweep_counter_reg[0]_i_3_n_0\,
      CO(3) => \tsweep_counter_reg[4]_i_1_n_0\,
      CO(2) => \tsweep_counter_reg[4]_i_1_n_1\,
      CO(1) => \tsweep_counter_reg[4]_i_1_n_2\,
      CO(0) => \tsweep_counter_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \tsweep_counter_reg[4]_i_1_n_4\,
      O(2) => \tsweep_counter_reg[4]_i_1_n_5\,
      O(1) => \tsweep_counter_reg[4]_i_1_n_6\,
      O(0) => \tsweep_counter_reg[4]_i_1_n_7\,
      S(3) => \tsweep_counter[4]_i_2_n_0\,
      S(2) => \tsweep_counter[4]_i_3_n_0\,
      S(1) => \tsweep_counter[4]_i_4_n_0\,
      S(0) => \tsweep_counter[4]_i_5_n_0\
    );
\tsweep_counter_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => \tsweep_counter[0]_i_2_n_0\,
      D => \tsweep_counter_reg[4]_i_1_n_6\,
      Q => tsweep_counter_reg(5),
      R => \tsweep_counter[0]_i_1_n_0\
    );
\tsweep_counter_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => \tsweep_counter[0]_i_2_n_0\,
      D => \tsweep_counter_reg[4]_i_1_n_5\,
      Q => tsweep_counter_reg(6),
      R => \tsweep_counter[0]_i_1_n_0\
    );
\tsweep_counter_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => \tsweep_counter[0]_i_2_n_0\,
      D => \tsweep_counter_reg[4]_i_1_n_4\,
      Q => tsweep_counter_reg(7),
      R => \tsweep_counter[0]_i_1_n_0\
    );
\tsweep_counter_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => \tsweep_counter[0]_i_2_n_0\,
      D => \tsweep_counter_reg[8]_i_1_n_7\,
      Q => tsweep_counter_reg(8),
      R => \tsweep_counter[0]_i_1_n_0\
    );
\tsweep_counter_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \tsweep_counter_reg[4]_i_1_n_0\,
      CO(3) => \tsweep_counter_reg[8]_i_1_n_0\,
      CO(2) => \tsweep_counter_reg[8]_i_1_n_1\,
      CO(1) => \tsweep_counter_reg[8]_i_1_n_2\,
      CO(0) => \tsweep_counter_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \tsweep_counter_reg[8]_i_1_n_4\,
      O(2) => \tsweep_counter_reg[8]_i_1_n_5\,
      O(1) => \tsweep_counter_reg[8]_i_1_n_6\,
      O(0) => \tsweep_counter_reg[8]_i_1_n_7\,
      S(3) => \tsweep_counter[8]_i_2_n_0\,
      S(2) => \tsweep_counter[8]_i_3_n_0\,
      S(1) => \tsweep_counter[8]_i_4_n_0\,
      S(0) => \tsweep_counter[8]_i_5_n_0\
    );
\tsweep_counter_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => \tsweep_counter[0]_i_2_n_0\,
      D => \tsweep_counter_reg[8]_i_1_n_6\,
      Q => tsweep_counter_reg(9),
      R => \tsweep_counter[0]_i_1_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_HMC769_v5_0 is
  port (
    DATA_receive_ila : out STD_LOGIC_VECTOR ( 23 downto 0 );
    spi_pll_cen : out STD_LOGIC;
    ATTEN : out STD_LOGIC_VECTOR ( 5 downto 0 );
    PLL_POW_EN : out STD_LOGIC;
    PAMP_EN : out STD_LOGIC;
    START_receive_ila : out STD_LOGIC;
    START_send_ila : out STD_LOGIC;
    s00_axi_wready : out STD_LOGIC;
    s00_axi_awready : out STD_LOGIC;
    s00_axi_arready : out STD_LOGIC;
    reg_address_send_ila : out STD_LOGIC_VECTOR ( 4 downto 0 );
    reg_address_receive_ila : out STD_LOGIC_VECTOR ( 7 downto 0 );
    DATA_send_ila : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    pll_trig : out STD_LOGIC;
    start_adc_count : out STD_LOGIC;
    spi_pll_sck : out STD_LOGIC;
    pll_data_ready_TX_ila : out STD_LOGIC;
    pll_data_ready_RX_ila : out STD_LOGIC;
    s00_axi_rvalid : out STD_LOGIC;
    spi_pll_sen : out STD_LOGIC;
    spi_pll_mosi : out STD_LOGIC;
    s00_axi_bvalid : out STD_LOGIC;
    azimut_0 : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC;
    spi_pll_ld_sdo : in STD_LOGIC;
    clk_10MHz : in STD_LOGIC;
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_wvalid : in STD_LOGIC;
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_arvalid : in STD_LOGIC;
    s00_axi_bready : in STD_LOGIC;
    s00_axi_rready : in STD_LOGIC;
    clkDCO_10MHz : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_HMC769_v5_0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_HMC769_v5_0 is
  signal \^data_receive_ila\ : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal HMC769_v5_0_S00_AXI_inst_n_1 : STD_LOGIC;
  signal HMC769_v5_0_S00_AXI_inst_n_24 : STD_LOGIC;
  signal HMC769_v5_0_S00_AXI_inst_n_6 : STD_LOGIC;
  signal HMC769_v5_0_S00_AXI_inst_n_65 : STD_LOGIC;
  signal HMC769_v5_0_S00_AXI_inst_n_98 : STD_LOGIC;
  signal enable_triger_CMD : STD_LOGIC;
  signal \^reg_address_send_ila\ : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal sel0 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal slv_reg0 : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal slv_reg4 : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal slv_reg5 : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal slv_reg6 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal slv_reg7 : STD_LOGIC_VECTOR ( 16 downto 1 );
  signal \spi_hmc_mode_RX_inst/cnt_reg\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \spi_hmc_mode_TX_inst/cnt_reg\ : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal top_PLL_control_i_n_17 : STD_LOGIC;
  signal top_PLL_control_i_n_19 : STD_LOGIC;
begin
  DATA_receive_ila(23 downto 0) <= \^data_receive_ila\(23 downto 0);
  reg_address_send_ila(4 downto 0) <= \^reg_address_send_ila\(4 downto 0);
HMC769_v5_0_S00_AXI_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_HMC769_v5_0_S00_AXI
     port map (
      DATA_receive_ila(22 downto 0) => \^data_receive_ila\(23 downto 1),
      DATA_send_ila(31 downto 0) => DATA_send_ila(31 downto 0),
      Q(15 downto 0) => slv_reg7(16 downto 1),
      SR(0) => HMC769_v5_0_S00_AXI_inst_n_1,
      \axi_araddr_reg[4]_0\ => HMC769_v5_0_S00_AXI_inst_n_98,
      \axi_araddr_reg[5]_0\(1) => sel0(3),
      \axi_araddr_reg[5]_0\(0) => sel0(0),
      \axi_rdata_reg[0]_0\ => top_PLL_control_i_n_19,
      azimut_0 => azimut_0,
      \cnt_reg[2]\ => HMC769_v5_0_S00_AXI_inst_n_24,
      \cnt_reg[3]\ => HMC769_v5_0_S00_AXI_inst_n_6,
      enable_triger_CMD => enable_triger_CMD,
      mosi_r_reg => top_PLL_control_i_n_17,
      mosi_r_reg_0(2 downto 0) => \spi_hmc_mode_RX_inst/cnt_reg\(2 downto 0),
      \out\(4 downto 0) => \spi_hmc_mode_TX_inst/cnt_reg\(4 downto 0),
      reg_address_receive_ila(7 downto 0) => reg_address_receive_ila(7 downto 0),
      reg_address_send_ila(4 downto 0) => \^reg_address_send_ila\(4 downto 0),
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_araddr(3 downto 0) => s00_axi_araddr(3 downto 0),
      s00_axi_aresetn => s00_axi_aresetn,
      s00_axi_arready => s00_axi_arready,
      s00_axi_arvalid => s00_axi_arvalid,
      s00_axi_awaddr(3 downto 0) => s00_axi_awaddr(3 downto 0),
      s00_axi_awready => s00_axi_awready,
      s00_axi_awvalid => s00_axi_awvalid,
      s00_axi_bready => s00_axi_bready,
      s00_axi_bvalid => s00_axi_bvalid,
      s00_axi_rdata(31 downto 0) => s00_axi_rdata(31 downto 0),
      s00_axi_rready => s00_axi_rready,
      s00_axi_rvalid => s00_axi_rvalid,
      s00_axi_wdata(31 downto 0) => s00_axi_wdata(31 downto 0),
      s00_axi_wready => s00_axi_wready,
      s00_axi_wstrb(3 downto 0) => s00_axi_wstrb(3 downto 0),
      s00_axi_wvalid => s00_axi_wvalid,
      slv_reg0(1 downto 0) => slv_reg0(1 downto 0),
      \slv_reg1_reg[0]_0\ => HMC769_v5_0_S00_AXI_inst_n_65,
      \slv_reg4_reg[5]_0\(5 downto 0) => slv_reg4(5 downto 0),
      \slv_reg5_reg[2]_0\(2 downto 0) => slv_reg5(2 downto 0),
      \slv_reg6_reg[15]_0\(15 downto 0) => slv_reg6(15 downto 0)
    );
top_PLL_control_i: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_top_PLL_control
     port map (
      ATTEN(5 downto 0) => ATTEN(5 downto 0),
      D(15 downto 0) => slv_reg6(15 downto 0),
      DATA_receive_ila(23 downto 0) => \^data_receive_ila\(23 downto 0),
      PAMP_EN => PAMP_EN,
      PLL_POW_EN => PLL_POW_EN,
      SS(0) => HMC769_v5_0_S00_AXI_inst_n_1,
      START_receive_reg_0 => START_receive_ila,
      START_send_reg_0 => START_send_ila,
      \atten_reg_reg[5]_0\(5 downto 0) => slv_reg4(5 downto 0),
      \axi_rdata_reg[0]\(1) => sel0(3),
      \axi_rdata_reg[0]\(0) => sel0(0),
      \axi_rdata_reg[0]_0\ => HMC769_v5_0_S00_AXI_inst_n_98,
      azimut_0 => azimut_0,
      clkDCO_10MHz => clkDCO_10MHz,
      clk_10MHz => clk_10MHz,
      \cnt_reg[4]\(4 downto 0) => \spi_hmc_mode_TX_inst/cnt_reg\(4 downto 0),
      \cnt_reg[5]\ => top_PLL_control_i_n_17,
      \data_r_reg[0]\ => top_PLL_control_i_n_19,
      enable_triger_CMD => enable_triger_CMD,
      mosi_r_reg => HMC769_v5_0_S00_AXI_inst_n_65,
      mosi_r_reg_0 => HMC769_v5_0_S00_AXI_inst_n_24,
      mosi_r_reg_1 => HMC769_v5_0_S00_AXI_inst_n_6,
      \out\(2 downto 0) => \spi_hmc_mode_RX_inst/cnt_reg\(2 downto 0),
      pll_cen_reg_reg_0(2 downto 0) => slv_reg5(2 downto 0),
      pll_data_ready_RX_ila => pll_data_ready_RX_ila,
      pll_data_ready_TX_ila => pll_data_ready_TX_ila,
      pll_trig => pll_trig,
      reg_address_send_ila(0) => \^reg_address_send_ila\(4),
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_aresetn => s00_axi_aresetn,
      \shift_front_from_cpu_reg[15]_0\(15 downto 0) => slv_reg7(16 downto 1),
      slv_reg0(1 downto 0) => slv_reg0(1 downto 0),
      spi_pll_cen => spi_pll_cen,
      spi_pll_ld_sdo => spi_pll_ld_sdo,
      spi_pll_mosi => spi_pll_mosi,
      spi_pll_sck => spi_pll_sck,
      spi_pll_sen => spi_pll_sen,
      start_adc_count => start_adc_count
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    clk_10MHz : in STD_LOGIC;
    clkDCO_10MHz : in STD_LOGIC;
    s00_axi_awaddr : in STD_LOGIC_VECTOR ( 5 downto 0 );
    s00_axi_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_awvalid : in STD_LOGIC;
    s00_axi_awready : out STD_LOGIC;
    s00_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s00_axi_wvalid : in STD_LOGIC;
    s00_axi_wready : out STD_LOGIC;
    s00_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_bvalid : out STD_LOGIC;
    s00_axi_bready : in STD_LOGIC;
    s00_axi_araddr : in STD_LOGIC_VECTOR ( 5 downto 0 );
    s00_axi_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s00_axi_arvalid : in STD_LOGIC;
    s00_axi_arready : out STD_LOGIC;
    s00_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s00_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s00_axi_rvalid : out STD_LOGIC;
    s00_axi_rready : in STD_LOGIC;
    azimut_0 : in STD_LOGIC;
    spi_pll_sen : out STD_LOGIC;
    spi_pll_sck : out STD_LOGIC;
    spi_pll_mosi : out STD_LOGIC;
    spi_pll_cen : out STD_LOGIC;
    spi_pll_ld_sdo : in STD_LOGIC;
    pll_trig : out STD_LOGIC;
    ATTEN : out STD_LOGIC_VECTOR ( 5 downto 0 );
    PLL_POW_EN : out STD_LOGIC;
    PAMP_EN : out STD_LOGIC;
    start_adc_count : out STD_LOGIC;
    sen_ila : out STD_LOGIC;
    sck_ila : out STD_LOGIC;
    mosi_ila : out STD_LOGIC;
    cen_ila : out STD_LOGIC;
    ld_sdo_ila : out STD_LOGIC;
    pll_data_ready_RX_ila : out STD_LOGIC;
    pll_data_ready_TX_ila : out STD_LOGIC;
    START_receive_ila : out STD_LOGIC;
    START_send_ila : out STD_LOGIC;
    DATA_send_ila : out STD_LOGIC_VECTOR ( 31 downto 0 );
    DATA_receive_ila : out STD_LOGIC_VECTOR ( 31 downto 0 );
    reg_address_send_ila : out STD_LOGIC_VECTOR ( 4 downto 0 );
    reg_address_receive_ila : out STD_LOGIC_VECTOR ( 7 downto 0 );
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_1_HMC769_0_0,HMC769_v5_0,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "HMC769_v5_0,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \<const0>\ : STD_LOGIC;
  signal \^data_receive_ila\ : STD_LOGIC_VECTOR ( 23 downto 0 );
  signal \^spi_pll_cen\ : STD_LOGIC;
  signal \^spi_pll_ld_sdo\ : STD_LOGIC;
  signal \^spi_pll_mosi\ : STD_LOGIC;
  signal \^spi_pll_sck\ : STD_LOGIC;
  signal \^spi_pll_sen\ : STD_LOGIC;
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of s00_axi_aclk : signal is "xilinx.com:signal:clock:1.0 S00_AXI_CLK CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of s00_axi_aclk : signal is "XIL_INTERFACENAME S00_AXI_CLK, ASSOCIATED_BUSIF S00_AXI, ASSOCIATED_RESET s00_axi_aresetn, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of s00_axi_aresetn : signal is "xilinx.com:signal:reset:1.0 S00_AXI_RST RST";
  attribute X_INTERFACE_PARAMETER of s00_axi_aresetn : signal is "XIL_INTERFACENAME S00_AXI_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of s00_axi_arready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARREADY";
  attribute X_INTERFACE_INFO of s00_axi_arvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARVALID";
  attribute X_INTERFACE_INFO of s00_axi_awready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWREADY";
  attribute X_INTERFACE_INFO of s00_axi_awvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWVALID";
  attribute X_INTERFACE_INFO of s00_axi_bready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI BREADY";
  attribute X_INTERFACE_INFO of s00_axi_bvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI BVALID";
  attribute X_INTERFACE_INFO of s00_axi_rready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RREADY";
  attribute X_INTERFACE_PARAMETER of s00_axi_rready : signal is "XIL_INTERFACENAME S00_AXI, WIZ_DATA_WIDTH 32, WIZ_NUM_REG 4, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 100000000, ID_WIDTH 0, ADDR_WIDTH 6, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 2, NUM_WRITE_OUTSTANDING 2, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of s00_axi_rvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RVALID";
  attribute X_INTERFACE_INFO of s00_axi_wready : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WREADY";
  attribute X_INTERFACE_INFO of s00_axi_wvalid : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WVALID";
  attribute X_INTERFACE_INFO of spi_pll_cen : signal is "user.org:interface:spi_pll:1.0 SPI_PLL cen";
  attribute X_INTERFACE_INFO of spi_pll_ld_sdo : signal is "user.org:interface:spi_pll:1.0 SPI_PLL ld_sdo";
  attribute X_INTERFACE_PARAMETER of spi_pll_ld_sdo : signal is "XIL_INTERFACENAME SPI_PLL, SV_INTERFACE true";
  attribute X_INTERFACE_INFO of spi_pll_mosi : signal is "user.org:interface:spi_pll:1.0 SPI_PLL mosi";
  attribute X_INTERFACE_INFO of spi_pll_sck : signal is "user.org:interface:spi_pll:1.0 SPI_PLL sck";
  attribute X_INTERFACE_INFO of spi_pll_sen : signal is "user.org:interface:spi_pll:1.0 SPI_PLL sen";
  attribute X_INTERFACE_INFO of s00_axi_araddr : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARADDR";
  attribute X_INTERFACE_INFO of s00_axi_arprot : signal is "xilinx.com:interface:aximm:1.0 S00_AXI ARPROT";
  attribute X_INTERFACE_INFO of s00_axi_awaddr : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWADDR";
  attribute X_INTERFACE_INFO of s00_axi_awprot : signal is "xilinx.com:interface:aximm:1.0 S00_AXI AWPROT";
  attribute X_INTERFACE_INFO of s00_axi_bresp : signal is "xilinx.com:interface:aximm:1.0 S00_AXI BRESP";
  attribute X_INTERFACE_INFO of s00_axi_rdata : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RDATA";
  attribute X_INTERFACE_INFO of s00_axi_rresp : signal is "xilinx.com:interface:aximm:1.0 S00_AXI RRESP";
  attribute X_INTERFACE_INFO of s00_axi_wdata : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WDATA";
  attribute X_INTERFACE_INFO of s00_axi_wstrb : signal is "xilinx.com:interface:aximm:1.0 S00_AXI WSTRB";
begin
  DATA_receive_ila(31) <= \<const0>\;
  DATA_receive_ila(30) <= \<const0>\;
  DATA_receive_ila(29) <= \<const0>\;
  DATA_receive_ila(28) <= \<const0>\;
  DATA_receive_ila(27) <= \<const0>\;
  DATA_receive_ila(26) <= \<const0>\;
  DATA_receive_ila(25) <= \<const0>\;
  DATA_receive_ila(24) <= \<const0>\;
  DATA_receive_ila(23 downto 0) <= \^data_receive_ila\(23 downto 0);
  \^spi_pll_ld_sdo\ <= spi_pll_ld_sdo;
  cen_ila <= \^spi_pll_cen\;
  ld_sdo_ila <= \^spi_pll_ld_sdo\;
  mosi_ila <= \^spi_pll_mosi\;
  s00_axi_bresp(1) <= \<const0>\;
  s00_axi_bresp(0) <= \<const0>\;
  s00_axi_rresp(1) <= \<const0>\;
  s00_axi_rresp(0) <= \<const0>\;
  sck_ila <= \^spi_pll_sck\;
  sen_ila <= \^spi_pll_sen\;
  spi_pll_cen <= \^spi_pll_cen\;
  spi_pll_mosi <= \^spi_pll_mosi\;
  spi_pll_sck <= \^spi_pll_sck\;
  spi_pll_sen <= \^spi_pll_sen\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_HMC769_v5_0
     port map (
      ATTEN(5 downto 0) => ATTEN(5 downto 0),
      DATA_receive_ila(23 downto 0) => \^data_receive_ila\(23 downto 0),
      DATA_send_ila(31 downto 0) => DATA_send_ila(31 downto 0),
      PAMP_EN => PAMP_EN,
      PLL_POW_EN => PLL_POW_EN,
      START_receive_ila => START_receive_ila,
      START_send_ila => START_send_ila,
      azimut_0 => azimut_0,
      clkDCO_10MHz => clkDCO_10MHz,
      clk_10MHz => clk_10MHz,
      pll_data_ready_RX_ila => pll_data_ready_RX_ila,
      pll_data_ready_TX_ila => pll_data_ready_TX_ila,
      pll_trig => pll_trig,
      reg_address_receive_ila(7 downto 0) => reg_address_receive_ila(7 downto 0),
      reg_address_send_ila(4 downto 0) => reg_address_send_ila(4 downto 0),
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_araddr(3 downto 0) => s00_axi_araddr(5 downto 2),
      s00_axi_aresetn => s00_axi_aresetn,
      s00_axi_arready => s00_axi_arready,
      s00_axi_arvalid => s00_axi_arvalid,
      s00_axi_awaddr(3 downto 0) => s00_axi_awaddr(5 downto 2),
      s00_axi_awready => s00_axi_awready,
      s00_axi_awvalid => s00_axi_awvalid,
      s00_axi_bready => s00_axi_bready,
      s00_axi_bvalid => s00_axi_bvalid,
      s00_axi_rdata(31 downto 0) => s00_axi_rdata(31 downto 0),
      s00_axi_rready => s00_axi_rready,
      s00_axi_rvalid => s00_axi_rvalid,
      s00_axi_wdata(31 downto 0) => s00_axi_wdata(31 downto 0),
      s00_axi_wready => s00_axi_wready,
      s00_axi_wstrb(3 downto 0) => s00_axi_wstrb(3 downto 0),
      s00_axi_wvalid => s00_axi_wvalid,
      spi_pll_cen => \^spi_pll_cen\,
      spi_pll_ld_sdo => \^spi_pll_ld_sdo\,
      spi_pll_mosi => \^spi_pll_mosi\,
      spi_pll_sck => \^spi_pll_sck\,
      spi_pll_sen => \^spi_pll_sen\,
      start_adc_count => start_adc_count
    );
end STRUCTURE;
