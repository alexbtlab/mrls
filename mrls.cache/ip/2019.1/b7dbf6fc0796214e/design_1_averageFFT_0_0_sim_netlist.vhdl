-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
-- Date        : Fri Apr 29 15:58:03 2022
-- Host        : alexbtlab-System-Product-Name running 64-bit Ubuntu 20.04.3 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_averageFFT_0_0_sim_netlist.vhdl
-- Design      : design_1_averageFFT_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z020clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_averageFFT_v4_0 is
  port (
    azimut8 : out STD_LOGIC_VECTOR ( 15 downto 0 );
    m00_axis_tdata : out STD_LOGIC_VECTOR ( 16 downto 0 );
    m00_axis_tlast : out STD_LOGIC;
    m00_axis_tvalid : out STD_LOGIC;
    m00_axis_aresetn : in STD_LOGIC;
    s00_axis_tvalid : in STD_LOGIC;
    clk_10MHz : in STD_LOGIC;
    m00_axis_aclk : in STD_LOGIC;
    s00_axis_tlast : in STD_LOGIC;
    azimuth_0 : in STD_LOGIC;
    allowed_clk : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_averageFFT_v4_0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_averageFFT_v4_0 is
  signal \^azimut8\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \cnt_high_allowed_clk[0]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_high_allowed_clk[0]_i_3_n_0\ : STD_LOGIC;
  signal cnt_high_allowed_clk_reg : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \cnt_high_allowed_clk_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \cnt_high_allowed_clk_reg[0]_i_2_n_1\ : STD_LOGIC;
  signal \cnt_high_allowed_clk_reg[0]_i_2_n_2\ : STD_LOGIC;
  signal \cnt_high_allowed_clk_reg[0]_i_2_n_3\ : STD_LOGIC;
  signal \cnt_high_allowed_clk_reg[0]_i_2_n_4\ : STD_LOGIC;
  signal \cnt_high_allowed_clk_reg[0]_i_2_n_5\ : STD_LOGIC;
  signal \cnt_high_allowed_clk_reg[0]_i_2_n_6\ : STD_LOGIC;
  signal \cnt_high_allowed_clk_reg[0]_i_2_n_7\ : STD_LOGIC;
  signal \cnt_high_allowed_clk_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_high_allowed_clk_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_high_allowed_clk_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_high_allowed_clk_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \cnt_high_allowed_clk_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \cnt_high_allowed_clk_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \cnt_high_allowed_clk_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \cnt_high_allowed_clk_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_high_allowed_clk_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_high_allowed_clk_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_high_allowed_clk_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_high_allowed_clk_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \cnt_high_allowed_clk_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \cnt_high_allowed_clk_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \cnt_high_allowed_clk_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \cnt_high_allowed_clk_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_high_allowed_clk_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \cnt_high_allowed_clk_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \cnt_high_allowed_clk_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \cnt_high_allowed_clk_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \cnt_high_allowed_clk_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \cnt_high_allowed_clk_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \cnt_high_allowed_clk_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal fft_azimut8_r : STD_LOGIC;
  signal \fft_azimut8_r[15]_i_1_n_0\ : STD_LOGIC;
  signal \fft_azimut8_r[15]_i_4_n_0\ : STD_LOGIC;
  signal \fft_azimut8_r[15]_i_5_n_0\ : STD_LOGIC;
  signal \fft_azimut8_r[15]_i_6_n_0\ : STD_LOGIC;
  signal \fft_azimut8_r[15]_i_7_n_0\ : STD_LOGIC;
  signal \fft_azimut8_r[15]_i_8_n_0\ : STD_LOGIC;
  signal \fft_azimut8_r[3]_i_2_n_0\ : STD_LOGIC;
  signal \fft_azimut8_r_reg[11]_i_1_n_0\ : STD_LOGIC;
  signal \fft_azimut8_r_reg[11]_i_1_n_1\ : STD_LOGIC;
  signal \fft_azimut8_r_reg[11]_i_1_n_2\ : STD_LOGIC;
  signal \fft_azimut8_r_reg[11]_i_1_n_3\ : STD_LOGIC;
  signal \fft_azimut8_r_reg[11]_i_1_n_4\ : STD_LOGIC;
  signal \fft_azimut8_r_reg[11]_i_1_n_5\ : STD_LOGIC;
  signal \fft_azimut8_r_reg[11]_i_1_n_6\ : STD_LOGIC;
  signal \fft_azimut8_r_reg[11]_i_1_n_7\ : STD_LOGIC;
  signal \fft_azimut8_r_reg[15]_i_3_n_1\ : STD_LOGIC;
  signal \fft_azimut8_r_reg[15]_i_3_n_2\ : STD_LOGIC;
  signal \fft_azimut8_r_reg[15]_i_3_n_3\ : STD_LOGIC;
  signal \fft_azimut8_r_reg[15]_i_3_n_4\ : STD_LOGIC;
  signal \fft_azimut8_r_reg[15]_i_3_n_5\ : STD_LOGIC;
  signal \fft_azimut8_r_reg[15]_i_3_n_6\ : STD_LOGIC;
  signal \fft_azimut8_r_reg[15]_i_3_n_7\ : STD_LOGIC;
  signal \fft_azimut8_r_reg[3]_i_1_n_0\ : STD_LOGIC;
  signal \fft_azimut8_r_reg[3]_i_1_n_1\ : STD_LOGIC;
  signal \fft_azimut8_r_reg[3]_i_1_n_2\ : STD_LOGIC;
  signal \fft_azimut8_r_reg[3]_i_1_n_3\ : STD_LOGIC;
  signal \fft_azimut8_r_reg[3]_i_1_n_4\ : STD_LOGIC;
  signal \fft_azimut8_r_reg[3]_i_1_n_5\ : STD_LOGIC;
  signal \fft_azimut8_r_reg[3]_i_1_n_6\ : STD_LOGIC;
  signal \fft_azimut8_r_reg[3]_i_1_n_7\ : STD_LOGIC;
  signal \fft_azimut8_r_reg[7]_i_1_n_0\ : STD_LOGIC;
  signal \fft_azimut8_r_reg[7]_i_1_n_1\ : STD_LOGIC;
  signal \fft_azimut8_r_reg[7]_i_1_n_2\ : STD_LOGIC;
  signal \fft_azimut8_r_reg[7]_i_1_n_3\ : STD_LOGIC;
  signal \fft_azimut8_r_reg[7]_i_1_n_4\ : STD_LOGIC;
  signal \fft_azimut8_r_reg[7]_i_1_n_5\ : STD_LOGIC;
  signal \fft_azimut8_r_reg[7]_i_1_n_6\ : STD_LOGIC;
  signal \fft_azimut8_r_reg[7]_i_1_n_7\ : STD_LOGIC;
  signal fft_azimut_r : STD_LOGIC;
  signal \fft_azimut_r[0]_i_1_n_0\ : STD_LOGIC;
  signal \fft_azimut_r[1]_i_1_n_0\ : STD_LOGIC;
  signal \fft_azimut_r[2]_i_1_n_0\ : STD_LOGIC;
  signal \fft_azimut_r[3]_i_2_n_0\ : STD_LOGIC;
  signal \fft_azimut_r_reg_n_0_[0]\ : STD_LOGIC;
  signal \fft_azimut_r_reg_n_0_[1]\ : STD_LOGIC;
  signal \fft_azimut_r_reg_n_0_[2]\ : STD_LOGIC;
  signal \fft_azimut_r_reg_n_0_[3]\ : STD_LOGIC;
  signal m00_axis_tdata_r : STD_LOGIC_VECTOR ( 31 downto 11 );
  signal \m00_axis_tdata_r[16]_i_1_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[31]_i_1_n_0\ : STD_LOGIC;
  signal \m00_axis_tdata_r[31]_i_2_n_0\ : STD_LOGIC;
  signal \NLW_cnt_high_allowed_clk_reg[12]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_fft_azimut8_r_reg[15]_i_3_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \fft_azimut8_r[15]_i_8\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \fft_azimut_r[0]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \fft_azimut_r[2]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \fft_azimut_r[3]_i_2\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \m00_axis_tdata_r[16]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \m00_axis_tdata_r[17]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \m00_axis_tdata_r[18]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \m00_axis_tdata_r[19]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \m00_axis_tdata_r[20]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \m00_axis_tdata_r[21]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \m00_axis_tdata_r[22]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \m00_axis_tdata_r[23]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \m00_axis_tdata_r[24]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \m00_axis_tdata_r[25]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \m00_axis_tdata_r[26]_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \m00_axis_tdata_r[27]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \m00_axis_tdata_r[28]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \m00_axis_tdata_r[29]_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \m00_axis_tdata_r[30]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \m00_axis_tdata_r[31]_i_3\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of m00_axis_tlast_INST_0 : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of m00_axis_tvalid_INST_0 : label is "soft_lutpair0";
begin
  azimut8(15 downto 0) <= \^azimut8\(15 downto 0);
\cnt_high_allowed_clk[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"DF"
    )
        port map (
      I0 => m00_axis_aresetn,
      I1 => azimuth_0,
      I2 => allowed_clk,
      O => \cnt_high_allowed_clk[0]_i_1_n_0\
    );
\cnt_high_allowed_clk[0]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt_high_allowed_clk_reg(0),
      O => \cnt_high_allowed_clk[0]_i_3_n_0\
    );
\cnt_high_allowed_clk_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_high_allowed_clk_reg[0]_i_2_n_7\,
      Q => cnt_high_allowed_clk_reg(0),
      R => \cnt_high_allowed_clk[0]_i_1_n_0\
    );
\cnt_high_allowed_clk_reg[0]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \cnt_high_allowed_clk_reg[0]_i_2_n_0\,
      CO(2) => \cnt_high_allowed_clk_reg[0]_i_2_n_1\,
      CO(1) => \cnt_high_allowed_clk_reg[0]_i_2_n_2\,
      CO(0) => \cnt_high_allowed_clk_reg[0]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \cnt_high_allowed_clk_reg[0]_i_2_n_4\,
      O(2) => \cnt_high_allowed_clk_reg[0]_i_2_n_5\,
      O(1) => \cnt_high_allowed_clk_reg[0]_i_2_n_6\,
      O(0) => \cnt_high_allowed_clk_reg[0]_i_2_n_7\,
      S(3 downto 1) => cnt_high_allowed_clk_reg(3 downto 1),
      S(0) => \cnt_high_allowed_clk[0]_i_3_n_0\
    );
\cnt_high_allowed_clk_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_high_allowed_clk_reg[8]_i_1_n_5\,
      Q => cnt_high_allowed_clk_reg(10),
      R => \cnt_high_allowed_clk[0]_i_1_n_0\
    );
\cnt_high_allowed_clk_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_high_allowed_clk_reg[8]_i_1_n_4\,
      Q => cnt_high_allowed_clk_reg(11),
      R => \cnt_high_allowed_clk[0]_i_1_n_0\
    );
\cnt_high_allowed_clk_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_high_allowed_clk_reg[12]_i_1_n_7\,
      Q => cnt_high_allowed_clk_reg(12),
      R => \cnt_high_allowed_clk[0]_i_1_n_0\
    );
\cnt_high_allowed_clk_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_high_allowed_clk_reg[8]_i_1_n_0\,
      CO(3) => \NLW_cnt_high_allowed_clk_reg[12]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \cnt_high_allowed_clk_reg[12]_i_1_n_1\,
      CO(1) => \cnt_high_allowed_clk_reg[12]_i_1_n_2\,
      CO(0) => \cnt_high_allowed_clk_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt_high_allowed_clk_reg[12]_i_1_n_4\,
      O(2) => \cnt_high_allowed_clk_reg[12]_i_1_n_5\,
      O(1) => \cnt_high_allowed_clk_reg[12]_i_1_n_6\,
      O(0) => \cnt_high_allowed_clk_reg[12]_i_1_n_7\,
      S(3 downto 0) => cnt_high_allowed_clk_reg(15 downto 12)
    );
\cnt_high_allowed_clk_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_high_allowed_clk_reg[12]_i_1_n_6\,
      Q => cnt_high_allowed_clk_reg(13),
      R => \cnt_high_allowed_clk[0]_i_1_n_0\
    );
\cnt_high_allowed_clk_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_high_allowed_clk_reg[12]_i_1_n_5\,
      Q => cnt_high_allowed_clk_reg(14),
      R => \cnt_high_allowed_clk[0]_i_1_n_0\
    );
\cnt_high_allowed_clk_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_high_allowed_clk_reg[12]_i_1_n_4\,
      Q => cnt_high_allowed_clk_reg(15),
      R => \cnt_high_allowed_clk[0]_i_1_n_0\
    );
\cnt_high_allowed_clk_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_high_allowed_clk_reg[0]_i_2_n_6\,
      Q => cnt_high_allowed_clk_reg(1),
      R => \cnt_high_allowed_clk[0]_i_1_n_0\
    );
\cnt_high_allowed_clk_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_high_allowed_clk_reg[0]_i_2_n_5\,
      Q => cnt_high_allowed_clk_reg(2),
      R => \cnt_high_allowed_clk[0]_i_1_n_0\
    );
\cnt_high_allowed_clk_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_high_allowed_clk_reg[0]_i_2_n_4\,
      Q => cnt_high_allowed_clk_reg(3),
      R => \cnt_high_allowed_clk[0]_i_1_n_0\
    );
\cnt_high_allowed_clk_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_high_allowed_clk_reg[4]_i_1_n_7\,
      Q => cnt_high_allowed_clk_reg(4),
      R => \cnt_high_allowed_clk[0]_i_1_n_0\
    );
\cnt_high_allowed_clk_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_high_allowed_clk_reg[0]_i_2_n_0\,
      CO(3) => \cnt_high_allowed_clk_reg[4]_i_1_n_0\,
      CO(2) => \cnt_high_allowed_clk_reg[4]_i_1_n_1\,
      CO(1) => \cnt_high_allowed_clk_reg[4]_i_1_n_2\,
      CO(0) => \cnt_high_allowed_clk_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt_high_allowed_clk_reg[4]_i_1_n_4\,
      O(2) => \cnt_high_allowed_clk_reg[4]_i_1_n_5\,
      O(1) => \cnt_high_allowed_clk_reg[4]_i_1_n_6\,
      O(0) => \cnt_high_allowed_clk_reg[4]_i_1_n_7\,
      S(3 downto 0) => cnt_high_allowed_clk_reg(7 downto 4)
    );
\cnt_high_allowed_clk_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_high_allowed_clk_reg[4]_i_1_n_6\,
      Q => cnt_high_allowed_clk_reg(5),
      R => \cnt_high_allowed_clk[0]_i_1_n_0\
    );
\cnt_high_allowed_clk_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_high_allowed_clk_reg[4]_i_1_n_5\,
      Q => cnt_high_allowed_clk_reg(6),
      R => \cnt_high_allowed_clk[0]_i_1_n_0\
    );
\cnt_high_allowed_clk_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_high_allowed_clk_reg[4]_i_1_n_4\,
      Q => cnt_high_allowed_clk_reg(7),
      R => \cnt_high_allowed_clk[0]_i_1_n_0\
    );
\cnt_high_allowed_clk_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_high_allowed_clk_reg[8]_i_1_n_7\,
      Q => cnt_high_allowed_clk_reg(8),
      R => \cnt_high_allowed_clk[0]_i_1_n_0\
    );
\cnt_high_allowed_clk_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_high_allowed_clk_reg[4]_i_1_n_0\,
      CO(3) => \cnt_high_allowed_clk_reg[8]_i_1_n_0\,
      CO(2) => \cnt_high_allowed_clk_reg[8]_i_1_n_1\,
      CO(1) => \cnt_high_allowed_clk_reg[8]_i_1_n_2\,
      CO(0) => \cnt_high_allowed_clk_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt_high_allowed_clk_reg[8]_i_1_n_4\,
      O(2) => \cnt_high_allowed_clk_reg[8]_i_1_n_5\,
      O(1) => \cnt_high_allowed_clk_reg[8]_i_1_n_6\,
      O(0) => \cnt_high_allowed_clk_reg[8]_i_1_n_7\,
      S(3 downto 0) => cnt_high_allowed_clk_reg(11 downto 8)
    );
\cnt_high_allowed_clk_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => \cnt_high_allowed_clk_reg[8]_i_1_n_6\,
      Q => cnt_high_allowed_clk_reg(9),
      R => \cnt_high_allowed_clk[0]_i_1_n_0\
    );
\fft_azimut8_r[15]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => azimuth_0,
      I1 => m00_axis_aresetn,
      O => \fft_azimut8_r[15]_i_1_n_0\
    );
\fft_azimut8_r[15]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000010"
    )
        port map (
      I0 => \fft_azimut8_r[15]_i_4_n_0\,
      I1 => \fft_azimut8_r[15]_i_5_n_0\,
      I2 => \fft_azimut8_r[15]_i_6_n_0\,
      I3 => \fft_azimut8_r[15]_i_7_n_0\,
      I4 => \fft_azimut8_r[15]_i_8_n_0\,
      O => fft_azimut8_r
    );
\fft_azimut8_r[15]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => cnt_high_allowed_clk_reg(8),
      I1 => cnt_high_allowed_clk_reg(0),
      I2 => cnt_high_allowed_clk_reg(12),
      I3 => cnt_high_allowed_clk_reg(2),
      O => \fft_azimut8_r[15]_i_4_n_0\
    );
\fft_azimut8_r[15]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFDF"
    )
        port map (
      I0 => cnt_high_allowed_clk_reg(6),
      I1 => cnt_high_allowed_clk_reg(4),
      I2 => cnt_high_allowed_clk_reg(3),
      I3 => cnt_high_allowed_clk_reg(10),
      O => \fft_azimut8_r[15]_i_5_n_0\
    );
\fft_azimut8_r[15]_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1000"
    )
        port map (
      I0 => cnt_high_allowed_clk_reg(15),
      I1 => cnt_high_allowed_clk_reg(1),
      I2 => cnt_high_allowed_clk_reg(7),
      I3 => cnt_high_allowed_clk_reg(5),
      O => \fft_azimut8_r[15]_i_6_n_0\
    );
\fft_azimut8_r[15]_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => cnt_high_allowed_clk_reg(9),
      I1 => cnt_high_allowed_clk_reg(13),
      I2 => cnt_high_allowed_clk_reg(14),
      I3 => cnt_high_allowed_clk_reg(11),
      O => \fft_azimut8_r[15]_i_7_n_0\
    );
\fft_azimut8_r[15]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => \fft_azimut_r_reg_n_0_[3]\,
      I1 => \fft_azimut_r_reg_n_0_[0]\,
      I2 => \fft_azimut_r_reg_n_0_[1]\,
      I3 => \fft_azimut_r_reg_n_0_[2]\,
      O => \fft_azimut8_r[15]_i_8_n_0\
    );
\fft_azimut8_r[3]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^azimut8\(0),
      O => \fft_azimut8_r[3]_i_2_n_0\
    );
\fft_azimut8_r_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => fft_azimut8_r,
      D => \fft_azimut8_r_reg[3]_i_1_n_7\,
      Q => \^azimut8\(0),
      R => \fft_azimut8_r[15]_i_1_n_0\
    );
\fft_azimut8_r_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => fft_azimut8_r,
      D => \fft_azimut8_r_reg[11]_i_1_n_5\,
      Q => \^azimut8\(10),
      R => \fft_azimut8_r[15]_i_1_n_0\
    );
\fft_azimut8_r_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => fft_azimut8_r,
      D => \fft_azimut8_r_reg[11]_i_1_n_4\,
      Q => \^azimut8\(11),
      R => \fft_azimut8_r[15]_i_1_n_0\
    );
\fft_azimut8_r_reg[11]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \fft_azimut8_r_reg[7]_i_1_n_0\,
      CO(3) => \fft_azimut8_r_reg[11]_i_1_n_0\,
      CO(2) => \fft_azimut8_r_reg[11]_i_1_n_1\,
      CO(1) => \fft_azimut8_r_reg[11]_i_1_n_2\,
      CO(0) => \fft_azimut8_r_reg[11]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \fft_azimut8_r_reg[11]_i_1_n_4\,
      O(2) => \fft_azimut8_r_reg[11]_i_1_n_5\,
      O(1) => \fft_azimut8_r_reg[11]_i_1_n_6\,
      O(0) => \fft_azimut8_r_reg[11]_i_1_n_7\,
      S(3 downto 0) => \^azimut8\(11 downto 8)
    );
\fft_azimut8_r_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => fft_azimut8_r,
      D => \fft_azimut8_r_reg[15]_i_3_n_7\,
      Q => \^azimut8\(12),
      R => \fft_azimut8_r[15]_i_1_n_0\
    );
\fft_azimut8_r_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => fft_azimut8_r,
      D => \fft_azimut8_r_reg[15]_i_3_n_6\,
      Q => \^azimut8\(13),
      R => \fft_azimut8_r[15]_i_1_n_0\
    );
\fft_azimut8_r_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => fft_azimut8_r,
      D => \fft_azimut8_r_reg[15]_i_3_n_5\,
      Q => \^azimut8\(14),
      R => \fft_azimut8_r[15]_i_1_n_0\
    );
\fft_azimut8_r_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => fft_azimut8_r,
      D => \fft_azimut8_r_reg[15]_i_3_n_4\,
      Q => \^azimut8\(15),
      R => \fft_azimut8_r[15]_i_1_n_0\
    );
\fft_azimut8_r_reg[15]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => \fft_azimut8_r_reg[11]_i_1_n_0\,
      CO(3) => \NLW_fft_azimut8_r_reg[15]_i_3_CO_UNCONNECTED\(3),
      CO(2) => \fft_azimut8_r_reg[15]_i_3_n_1\,
      CO(1) => \fft_azimut8_r_reg[15]_i_3_n_2\,
      CO(0) => \fft_azimut8_r_reg[15]_i_3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \fft_azimut8_r_reg[15]_i_3_n_4\,
      O(2) => \fft_azimut8_r_reg[15]_i_3_n_5\,
      O(1) => \fft_azimut8_r_reg[15]_i_3_n_6\,
      O(0) => \fft_azimut8_r_reg[15]_i_3_n_7\,
      S(3 downto 0) => \^azimut8\(15 downto 12)
    );
\fft_azimut8_r_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => fft_azimut8_r,
      D => \fft_azimut8_r_reg[3]_i_1_n_6\,
      Q => \^azimut8\(1),
      R => \fft_azimut8_r[15]_i_1_n_0\
    );
\fft_azimut8_r_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => fft_azimut8_r,
      D => \fft_azimut8_r_reg[3]_i_1_n_5\,
      Q => \^azimut8\(2),
      R => \fft_azimut8_r[15]_i_1_n_0\
    );
\fft_azimut8_r_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => fft_azimut8_r,
      D => \fft_azimut8_r_reg[3]_i_1_n_4\,
      Q => \^azimut8\(3),
      R => \fft_azimut8_r[15]_i_1_n_0\
    );
\fft_azimut8_r_reg[3]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \fft_azimut8_r_reg[3]_i_1_n_0\,
      CO(2) => \fft_azimut8_r_reg[3]_i_1_n_1\,
      CO(1) => \fft_azimut8_r_reg[3]_i_1_n_2\,
      CO(0) => \fft_azimut8_r_reg[3]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \fft_azimut8_r_reg[3]_i_1_n_4\,
      O(2) => \fft_azimut8_r_reg[3]_i_1_n_5\,
      O(1) => \fft_azimut8_r_reg[3]_i_1_n_6\,
      O(0) => \fft_azimut8_r_reg[3]_i_1_n_7\,
      S(3 downto 1) => \^azimut8\(3 downto 1),
      S(0) => \fft_azimut8_r[3]_i_2_n_0\
    );
\fft_azimut8_r_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => fft_azimut8_r,
      D => \fft_azimut8_r_reg[7]_i_1_n_7\,
      Q => \^azimut8\(4),
      R => \fft_azimut8_r[15]_i_1_n_0\
    );
\fft_azimut8_r_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => fft_azimut8_r,
      D => \fft_azimut8_r_reg[7]_i_1_n_6\,
      Q => \^azimut8\(5),
      R => \fft_azimut8_r[15]_i_1_n_0\
    );
\fft_azimut8_r_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => fft_azimut8_r,
      D => \fft_azimut8_r_reg[7]_i_1_n_5\,
      Q => \^azimut8\(6),
      R => \fft_azimut8_r[15]_i_1_n_0\
    );
\fft_azimut8_r_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => fft_azimut8_r,
      D => \fft_azimut8_r_reg[7]_i_1_n_4\,
      Q => \^azimut8\(7),
      R => \fft_azimut8_r[15]_i_1_n_0\
    );
\fft_azimut8_r_reg[7]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \fft_azimut8_r_reg[3]_i_1_n_0\,
      CO(3) => \fft_azimut8_r_reg[7]_i_1_n_0\,
      CO(2) => \fft_azimut8_r_reg[7]_i_1_n_1\,
      CO(1) => \fft_azimut8_r_reg[7]_i_1_n_2\,
      CO(0) => \fft_azimut8_r_reg[7]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \fft_azimut8_r_reg[7]_i_1_n_4\,
      O(2) => \fft_azimut8_r_reg[7]_i_1_n_5\,
      O(1) => \fft_azimut8_r_reg[7]_i_1_n_6\,
      O(0) => \fft_azimut8_r_reg[7]_i_1_n_7\,
      S(3 downto 0) => \^azimut8\(7 downto 4)
    );
\fft_azimut8_r_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => fft_azimut8_r,
      D => \fft_azimut8_r_reg[11]_i_1_n_7\,
      Q => \^azimut8\(8),
      R => \fft_azimut8_r[15]_i_1_n_0\
    );
\fft_azimut8_r_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => fft_azimut8_r,
      D => \fft_azimut8_r_reg[11]_i_1_n_6\,
      Q => \^azimut8\(9),
      R => \fft_azimut8_r[15]_i_1_n_0\
    );
\fft_azimut_r[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00EF"
    )
        port map (
      I0 => \fft_azimut_r_reg_n_0_[2]\,
      I1 => \fft_azimut_r_reg_n_0_[1]\,
      I2 => \fft_azimut_r_reg_n_0_[3]\,
      I3 => \fft_azimut_r_reg_n_0_[0]\,
      O => \fft_azimut_r[0]_i_1_n_0\
    );
\fft_azimut_r[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \fft_azimut_r_reg_n_0_[1]\,
      I1 => \fft_azimut_r_reg_n_0_[0]\,
      O => \fft_azimut_r[1]_i_1_n_0\
    );
\fft_azimut_r[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => \fft_azimut_r_reg_n_0_[2]\,
      I1 => \fft_azimut_r_reg_n_0_[0]\,
      I2 => \fft_azimut_r_reg_n_0_[1]\,
      O => \fft_azimut_r[2]_i_1_n_0\
    );
\fft_azimut_r[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0004"
    )
        port map (
      I0 => \fft_azimut8_r[15]_i_7_n_0\,
      I1 => \fft_azimut8_r[15]_i_6_n_0\,
      I2 => \fft_azimut8_r[15]_i_5_n_0\,
      I3 => \fft_azimut8_r[15]_i_4_n_0\,
      O => fft_azimut_r
    );
\fft_azimut_r[3]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7E80"
    )
        port map (
      I0 => \fft_azimut_r_reg_n_0_[2]\,
      I1 => \fft_azimut_r_reg_n_0_[0]\,
      I2 => \fft_azimut_r_reg_n_0_[1]\,
      I3 => \fft_azimut_r_reg_n_0_[3]\,
      O => \fft_azimut_r[3]_i_2_n_0\
    );
\fft_azimut_r_reg[0]\: unisim.vcomponents.FDSE
     port map (
      C => clk_10MHz,
      CE => fft_azimut_r,
      D => \fft_azimut_r[0]_i_1_n_0\,
      Q => \fft_azimut_r_reg_n_0_[0]\,
      S => \fft_azimut8_r[15]_i_1_n_0\
    );
\fft_azimut_r_reg[1]\: unisim.vcomponents.FDSE
     port map (
      C => clk_10MHz,
      CE => fft_azimut_r,
      D => \fft_azimut_r[1]_i_1_n_0\,
      Q => \fft_azimut_r_reg_n_0_[1]\,
      S => \fft_azimut8_r[15]_i_1_n_0\
    );
\fft_azimut_r_reg[2]\: unisim.vcomponents.FDSE
     port map (
      C => clk_10MHz,
      CE => fft_azimut_r,
      D => \fft_azimut_r[2]_i_1_n_0\,
      Q => \fft_azimut_r_reg_n_0_[2]\,
      S => \fft_azimut8_r[15]_i_1_n_0\
    );
\fft_azimut_r_reg[3]\: unisim.vcomponents.FDSE
     port map (
      C => clk_10MHz,
      CE => fft_azimut_r,
      D => \fft_azimut_r[3]_i_2_n_0\,
      Q => \fft_azimut_r_reg_n_0_[3]\,
      S => \fft_azimut8_r[15]_i_1_n_0\
    );
\m00_axis_tdata_r[11]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => s00_axis_tvalid,
      I1 => m00_axis_aresetn,
      O => m00_axis_tdata_r(11)
    );
\m00_axis_tdata_r[16]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => m00_axis_aresetn,
      I1 => s00_axis_tvalid,
      I2 => \^azimut8\(0),
      O => \m00_axis_tdata_r[16]_i_1_n_0\
    );
\m00_axis_tdata_r[17]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => \^azimut8\(1),
      I1 => m00_axis_aresetn,
      I2 => s00_axis_tvalid,
      O => m00_axis_tdata_r(17)
    );
\m00_axis_tdata_r[18]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => \^azimut8\(2),
      I1 => m00_axis_aresetn,
      I2 => s00_axis_tvalid,
      O => m00_axis_tdata_r(18)
    );
\m00_axis_tdata_r[19]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => \^azimut8\(3),
      I1 => m00_axis_aresetn,
      I2 => s00_axis_tvalid,
      O => m00_axis_tdata_r(19)
    );
\m00_axis_tdata_r[20]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => \^azimut8\(4),
      I1 => m00_axis_aresetn,
      I2 => s00_axis_tvalid,
      O => m00_axis_tdata_r(20)
    );
\m00_axis_tdata_r[21]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => \^azimut8\(5),
      I1 => m00_axis_aresetn,
      I2 => s00_axis_tvalid,
      O => m00_axis_tdata_r(21)
    );
\m00_axis_tdata_r[22]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => \^azimut8\(6),
      I1 => m00_axis_aresetn,
      I2 => s00_axis_tvalid,
      O => m00_axis_tdata_r(22)
    );
\m00_axis_tdata_r[23]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => \^azimut8\(7),
      I1 => m00_axis_aresetn,
      I2 => s00_axis_tvalid,
      O => m00_axis_tdata_r(23)
    );
\m00_axis_tdata_r[24]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => \^azimut8\(8),
      I1 => m00_axis_aresetn,
      I2 => s00_axis_tvalid,
      O => m00_axis_tdata_r(24)
    );
\m00_axis_tdata_r[25]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => \^azimut8\(9),
      I1 => m00_axis_aresetn,
      I2 => s00_axis_tvalid,
      O => m00_axis_tdata_r(25)
    );
\m00_axis_tdata_r[26]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => \^azimut8\(10),
      I1 => m00_axis_aresetn,
      I2 => s00_axis_tvalid,
      O => m00_axis_tdata_r(26)
    );
\m00_axis_tdata_r[27]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => \^azimut8\(11),
      I1 => m00_axis_aresetn,
      I2 => s00_axis_tvalid,
      O => m00_axis_tdata_r(27)
    );
\m00_axis_tdata_r[28]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => \^azimut8\(12),
      I1 => m00_axis_aresetn,
      I2 => s00_axis_tvalid,
      O => m00_axis_tdata_r(28)
    );
\m00_axis_tdata_r[29]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => \^azimut8\(13),
      I1 => m00_axis_aresetn,
      I2 => s00_axis_tvalid,
      O => m00_axis_tdata_r(29)
    );
\m00_axis_tdata_r[30]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => \^azimut8\(14),
      I1 => m00_axis_aresetn,
      I2 => s00_axis_tvalid,
      O => m00_axis_tdata_r(30)
    );
\m00_axis_tdata_r[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FEFF000000000000"
    )
        port map (
      I0 => \fft_azimut_r_reg_n_0_[2]\,
      I1 => \fft_azimut_r_reg_n_0_[1]\,
      I2 => \fft_azimut_r_reg_n_0_[0]\,
      I3 => \fft_azimut_r_reg_n_0_[3]\,
      I4 => m00_axis_aresetn,
      I5 => s00_axis_tvalid,
      O => \m00_axis_tdata_r[31]_i_1_n_0\
    );
\m00_axis_tdata_r[31]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => s00_axis_tvalid,
      I1 => m00_axis_aresetn,
      O => \m00_axis_tdata_r[31]_i_2_n_0\
    );
\m00_axis_tdata_r[31]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => \^azimut8\(15),
      I1 => m00_axis_aresetn,
      I2 => s00_axis_tvalid,
      O => m00_axis_tdata_r(31)
    );
\m00_axis_tdata_r_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \m00_axis_tdata_r[31]_i_2_n_0\,
      D => m00_axis_tdata_r(11),
      Q => m00_axis_tdata(0),
      R => \m00_axis_tdata_r[31]_i_1_n_0\
    );
\m00_axis_tdata_r_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \m00_axis_tdata_r[31]_i_2_n_0\,
      D => \m00_axis_tdata_r[16]_i_1_n_0\,
      Q => m00_axis_tdata(1),
      R => \m00_axis_tdata_r[31]_i_1_n_0\
    );
\m00_axis_tdata_r_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \m00_axis_tdata_r[31]_i_2_n_0\,
      D => m00_axis_tdata_r(17),
      Q => m00_axis_tdata(2),
      R => \m00_axis_tdata_r[31]_i_1_n_0\
    );
\m00_axis_tdata_r_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \m00_axis_tdata_r[31]_i_2_n_0\,
      D => m00_axis_tdata_r(18),
      Q => m00_axis_tdata(3),
      R => \m00_axis_tdata_r[31]_i_1_n_0\
    );
\m00_axis_tdata_r_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \m00_axis_tdata_r[31]_i_2_n_0\,
      D => m00_axis_tdata_r(19),
      Q => m00_axis_tdata(4),
      R => \m00_axis_tdata_r[31]_i_1_n_0\
    );
\m00_axis_tdata_r_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \m00_axis_tdata_r[31]_i_2_n_0\,
      D => m00_axis_tdata_r(20),
      Q => m00_axis_tdata(5),
      R => \m00_axis_tdata_r[31]_i_1_n_0\
    );
\m00_axis_tdata_r_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \m00_axis_tdata_r[31]_i_2_n_0\,
      D => m00_axis_tdata_r(21),
      Q => m00_axis_tdata(6),
      R => \m00_axis_tdata_r[31]_i_1_n_0\
    );
\m00_axis_tdata_r_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \m00_axis_tdata_r[31]_i_2_n_0\,
      D => m00_axis_tdata_r(22),
      Q => m00_axis_tdata(7),
      R => \m00_axis_tdata_r[31]_i_1_n_0\
    );
\m00_axis_tdata_r_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \m00_axis_tdata_r[31]_i_2_n_0\,
      D => m00_axis_tdata_r(23),
      Q => m00_axis_tdata(8),
      R => \m00_axis_tdata_r[31]_i_1_n_0\
    );
\m00_axis_tdata_r_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \m00_axis_tdata_r[31]_i_2_n_0\,
      D => m00_axis_tdata_r(24),
      Q => m00_axis_tdata(9),
      R => \m00_axis_tdata_r[31]_i_1_n_0\
    );
\m00_axis_tdata_r_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \m00_axis_tdata_r[31]_i_2_n_0\,
      D => m00_axis_tdata_r(25),
      Q => m00_axis_tdata(10),
      R => \m00_axis_tdata_r[31]_i_1_n_0\
    );
\m00_axis_tdata_r_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \m00_axis_tdata_r[31]_i_2_n_0\,
      D => m00_axis_tdata_r(26),
      Q => m00_axis_tdata(11),
      R => \m00_axis_tdata_r[31]_i_1_n_0\
    );
\m00_axis_tdata_r_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \m00_axis_tdata_r[31]_i_2_n_0\,
      D => m00_axis_tdata_r(27),
      Q => m00_axis_tdata(12),
      R => \m00_axis_tdata_r[31]_i_1_n_0\
    );
\m00_axis_tdata_r_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \m00_axis_tdata_r[31]_i_2_n_0\,
      D => m00_axis_tdata_r(28),
      Q => m00_axis_tdata(13),
      R => \m00_axis_tdata_r[31]_i_1_n_0\
    );
\m00_axis_tdata_r_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \m00_axis_tdata_r[31]_i_2_n_0\,
      D => m00_axis_tdata_r(29),
      Q => m00_axis_tdata(14),
      R => \m00_axis_tdata_r[31]_i_1_n_0\
    );
\m00_axis_tdata_r_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \m00_axis_tdata_r[31]_i_2_n_0\,
      D => m00_axis_tdata_r(30),
      Q => m00_axis_tdata(15),
      R => \m00_axis_tdata_r[31]_i_1_n_0\
    );
\m00_axis_tdata_r_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \m00_axis_tdata_r[31]_i_2_n_0\,
      D => m00_axis_tdata_r(31),
      Q => m00_axis_tdata(16),
      R => \m00_axis_tdata_r[31]_i_1_n_0\
    );
m00_axis_tlast_INST_0: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00020000"
    )
        port map (
      I0 => s00_axis_tlast,
      I1 => \fft_azimut_r_reg_n_0_[2]\,
      I2 => \fft_azimut_r_reg_n_0_[1]\,
      I3 => \fft_azimut_r_reg_n_0_[0]\,
      I4 => \fft_azimut_r_reg_n_0_[3]\,
      O => m00_axis_tlast
    );
m00_axis_tvalid_INST_0: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00020000"
    )
        port map (
      I0 => s00_axis_tvalid,
      I1 => \fft_azimut_r_reg_n_0_[2]\,
      I2 => \fft_azimut_r_reg_n_0_[1]\,
      I3 => \fft_azimut_r_reg_n_0_[0]\,
      I4 => \fft_azimut_r_reg_n_0_[3]\,
      O => m00_axis_tvalid
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    s00_axis_tready : out STD_LOGIC;
    s00_axis_tdata : in STD_LOGIC_VECTOR ( 63 downto 0 );
    s00_axis_tstrb : in STD_LOGIC_VECTOR ( 7 downto 0 );
    s00_axis_tlast : in STD_LOGIC;
    s00_axis_tvalid : in STD_LOGIC;
    m00_axis_tvalid : out STD_LOGIC;
    m00_axis_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_axis_tstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m00_axis_tlast : out STD_LOGIC;
    m00_axis_tready : in STD_LOGIC;
    allowed_clk : in STD_LOGIC;
    azimuth_0 : in STD_LOGIC;
    m00_axis_aclk : in STD_LOGIC;
    clk_10MHz : in STD_LOGIC;
    m00_axis_aresetn : in STD_LOGIC;
    azimut8 : out STD_LOGIC_VECTOR ( 15 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_1_averageFFT_0_0,averageFFT_v4_0,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "averageFFT_v4_0,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \<const0>\ : STD_LOGIC;
  signal \<const1>\ : STD_LOGIC;
  signal \^m00_axis_tdata\ : STD_LOGIC_VECTOR ( 31 downto 11 );
  signal \^m00_axis_tready\ : STD_LOGIC;
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of allowed_clk : signal is "xilinx.com:signal:clock:1.0 allowed_clk CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of allowed_clk : signal is "XIL_INTERFACENAME allowed_clk, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_aclk : signal is "xilinx.com:signal:clock:1.0 m00_axis_aclk CLK";
  attribute X_INTERFACE_PARAMETER of m00_axis_aclk : signal is "XIL_INTERFACENAME m00_axis_aclk, ASSOCIATED_BUSIF m00_axis:s00_axis, ASSOCIATED_RESET m00_axis_aresetn, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_aresetn : signal is "xilinx.com:signal:reset:1.0 m00_axis_aresetn RST";
  attribute X_INTERFACE_PARAMETER of m00_axis_aresetn : signal is "XIL_INTERFACENAME m00_axis_aresetn, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_tlast : signal is "xilinx.com:interface:axis:1.0 m00_axis TLAST";
  attribute X_INTERFACE_INFO of m00_axis_tready : signal is "xilinx.com:interface:axis:1.0 m00_axis TREADY";
  attribute X_INTERFACE_PARAMETER of m00_axis_tready : signal is "XIL_INTERFACENAME m00_axis, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_tvalid : signal is "xilinx.com:interface:axis:1.0 m00_axis TVALID";
  attribute X_INTERFACE_INFO of s00_axis_tlast : signal is "xilinx.com:interface:axis:1.0 s00_axis TLAST";
  attribute X_INTERFACE_INFO of s00_axis_tready : signal is "xilinx.com:interface:axis:1.0 s00_axis TREADY";
  attribute X_INTERFACE_INFO of s00_axis_tvalid : signal is "xilinx.com:interface:axis:1.0 s00_axis TVALID";
  attribute X_INTERFACE_PARAMETER of s00_axis_tvalid : signal is "XIL_INTERFACENAME s00_axis, TDATA_NUM_BYTES 8, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_tdata : signal is "xilinx.com:interface:axis:1.0 m00_axis TDATA";
  attribute X_INTERFACE_INFO of m00_axis_tstrb : signal is "xilinx.com:interface:axis:1.0 m00_axis TSTRB";
  attribute X_INTERFACE_INFO of s00_axis_tdata : signal is "xilinx.com:interface:axis:1.0 s00_axis TDATA";
  attribute X_INTERFACE_INFO of s00_axis_tstrb : signal is "xilinx.com:interface:axis:1.0 s00_axis TSTRB";
begin
  \^m00_axis_tready\ <= m00_axis_tready;
  m00_axis_tdata(31 downto 16) <= \^m00_axis_tdata\(31 downto 16);
  m00_axis_tdata(15) <= \<const0>\;
  m00_axis_tdata(14) <= \<const0>\;
  m00_axis_tdata(13) <= \<const0>\;
  m00_axis_tdata(12) <= \<const0>\;
  m00_axis_tdata(11) <= \^m00_axis_tdata\(11);
  m00_axis_tdata(10) <= \<const0>\;
  m00_axis_tdata(9) <= \<const0>\;
  m00_axis_tdata(8) <= \<const0>\;
  m00_axis_tdata(7) <= \<const0>\;
  m00_axis_tdata(6) <= \<const0>\;
  m00_axis_tdata(5) <= \<const0>\;
  m00_axis_tdata(4) <= \<const0>\;
  m00_axis_tdata(3) <= \<const0>\;
  m00_axis_tdata(2) <= \<const0>\;
  m00_axis_tdata(1) <= \<const0>\;
  m00_axis_tdata(0) <= \<const0>\;
  m00_axis_tstrb(3) <= \<const1>\;
  m00_axis_tstrb(2) <= \<const1>\;
  m00_axis_tstrb(1) <= \<const1>\;
  m00_axis_tstrb(0) <= \<const1>\;
  s00_axis_tready <= \^m00_axis_tready\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
VCC: unisim.vcomponents.VCC
     port map (
      P => \<const1>\
    );
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_averageFFT_v4_0
     port map (
      allowed_clk => allowed_clk,
      azimut8(15 downto 0) => azimut8(15 downto 0),
      azimuth_0 => azimuth_0,
      clk_10MHz => clk_10MHz,
      m00_axis_aclk => m00_axis_aclk,
      m00_axis_aresetn => m00_axis_aresetn,
      m00_axis_tdata(16 downto 1) => \^m00_axis_tdata\(31 downto 16),
      m00_axis_tdata(0) => \^m00_axis_tdata\(11),
      m00_axis_tlast => m00_axis_tlast,
      m00_axis_tvalid => m00_axis_tvalid,
      s00_axis_tlast => s00_axis_tlast,
      s00_axis_tvalid => s00_axis_tvalid
    );
end STRUCTURE;
