// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
// Date        : Mon Feb 28 18:13:28 2022
// Host        : alexbtlab-System-Product-Name running 64-bit Ubuntu 20.04.3 LTS
// Command     : write_verilog -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_syncADCData_0_0_stub.v
// Design      : design_1_syncADCData_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z020clg400-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "syncADCData_v1_0,Vivado 2019.1" *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix(dco_or_dcoa, dco_or_dcob, dco_or_ora, 
  dco_or_orb, reset, DATA_CH1_out, DATA_CH2_out, clk_10MHz, clk_100MHz, DATA_CH1_in, DATA_CH2_in)
/* synthesis syn_black_box black_box_pad_pin="dco_or_dcoa,dco_or_dcob,dco_or_ora,dco_or_orb,reset,DATA_CH1_out[15:0],DATA_CH2_out[15:0],clk_10MHz,clk_100MHz,DATA_CH1_in[15:0],DATA_CH2_in[15:0]" */;
  input dco_or_dcoa;
  input dco_or_dcob;
  input dco_or_ora;
  input dco_or_orb;
  input reset;
  output [15:0]DATA_CH1_out;
  output [15:0]DATA_CH2_out;
  input clk_10MHz;
  input clk_100MHz;
  input [15:0]DATA_CH1_in;
  input [15:0]DATA_CH2_in;
endmodule
