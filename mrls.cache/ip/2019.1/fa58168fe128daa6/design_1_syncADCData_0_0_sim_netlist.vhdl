-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
-- Date        : Tue May 31 11:30:27 2022
-- Host        : alexbtlab-System-Product-Name running 64-bit Ubuntu 20.04.3 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_syncADCData_0_0_sim_netlist.vhdl
-- Design      : design_1_syncADCData_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z020clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    dco_or_dcoa : in STD_LOGIC;
    dco_or_dcob : in STD_LOGIC;
    dco_or_ora : in STD_LOGIC;
    dco_or_orb : in STD_LOGIC;
    reset : in STD_LOGIC;
    DCOA : out STD_LOGIC;
    DATA_CH1_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    DATA_CH2_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    clk_10MHz : in STD_LOGIC;
    clk_100MHz : in STD_LOGIC;
    DATA_CH1_in : in STD_LOGIC_VECTOR ( 15 downto 0 );
    DATA_CH2_in : in STD_LOGIC_VECTOR ( 15 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_1_syncADCData_0_0,syncADCData_v1_0,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "syncADCData_v1_0,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \^data_ch1_in\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \^data_ch2_in\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \^dco_or_dcoa\ : STD_LOGIC;
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of dco_or_dcoa : signal is "user.org:interface:dco_or:1.0 DCO_OR dcoa";
  attribute X_INTERFACE_INFO of dco_or_dcob : signal is "user.org:interface:dco_or:1.0 DCO_OR dcob";
  attribute X_INTERFACE_INFO of dco_or_ora : signal is "user.org:interface:dco_or:1.0 DCO_OR ora";
  attribute X_INTERFACE_INFO of dco_or_orb : signal is "user.org:interface:dco_or:1.0 DCO_OR orb";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of dco_or_orb : signal is "XIL_INTERFACENAME DCO_OR, SV_INTERFACE true";
  attribute X_INTERFACE_INFO of reset : signal is "xilinx.com:signal:reset:1.0 reset RST";
  attribute X_INTERFACE_PARAMETER of reset : signal is "XIL_INTERFACENAME reset, POLARITY ACTIVE_LOW, INSERT_VIP 0";
begin
  DATA_CH1_out(15 downto 0) <= \^data_ch1_in\(15 downto 0);
  DATA_CH2_out(15 downto 0) <= \^data_ch2_in\(15 downto 0);
  DCOA <= \^dco_or_dcoa\;
  \^data_ch1_in\(15 downto 0) <= DATA_CH1_in(15 downto 0);
  \^data_ch2_in\(15 downto 0) <= DATA_CH2_in(15 downto 0);
  \^dco_or_dcoa\ <= dco_or_dcoa;
end STRUCTURE;
