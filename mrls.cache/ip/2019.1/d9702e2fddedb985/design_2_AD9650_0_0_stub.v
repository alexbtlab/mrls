// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
// Date        : Thu May  5 15:38:42 2022
// Host        : alexbtlab-System-Product-Name running 64-bit Ubuntu 20.04.3 LTS
// Command     : write_verilog -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_2_AD9650_0_0_stub.v
// Design      : design_2_AD9650_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z020clg400-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "AD9650_v3_0,Vivado 2019.1" *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix(clk_10MHz, s00_axi_awaddr, s00_axi_awprot, 
  s00_axi_awvalid, s00_axi_awready, s00_axi_wdata, s00_axi_wstrb, s00_axi_wvalid, 
  s00_axi_wready, s00_axi_bresp, s00_axi_bvalid, s00_axi_bready, s00_axi_araddr, 
  s00_axi_arprot, s00_axi_arvalid, s00_axi_arready, s00_axi_rdata, s00_axi_rresp, 
  s00_axi_rvalid, s00_axi_rready, m00_fft_axis_tvalid, m00_fft_axis_tdata, 
  m00_fft_axis_tstrb, m00_fft_axis_tlast, m00_fft_axis_tready, m01_fft_axis_tvalid, 
  m01_fft_axis_tdata, m01_fft_axis_tstrb, m01_fft_axis_tlast, m01_fft_axis_tready, 
  s00_fft_axis_tvalid, s00_fft_axis_tdata, s00_fft_axis_tstrb, s00_fft_axis_tlast, 
  s00_fft_axis_tready, s01_fft_axis_tvalid, s01_fft_axis_tdata, s01_fft_axis_tstrb, 
  s01_fft_axis_tlast, s01_fft_axis_tready, m00_dma_axis_tvalid, m00_dma_axis_tdata, 
  m00_dma_axis_tstrb, m00_dma_axis_tlast, m00_dma_axis_tready, ADC_PDwN, SYNC, DATA_INA, 
  DATA_INB, allowed_clk, azimut8, azimut_0, s00_axi_aclk, s00_axi_aresetn)
/* synthesis syn_black_box black_box_pad_pin="clk_10MHz,s00_axi_awaddr[5:0],s00_axi_awprot[2:0],s00_axi_awvalid,s00_axi_awready,s00_axi_wdata[31:0],s00_axi_wstrb[3:0],s00_axi_wvalid,s00_axi_wready,s00_axi_bresp[1:0],s00_axi_bvalid,s00_axi_bready,s00_axi_araddr[5:0],s00_axi_arprot[2:0],s00_axi_arvalid,s00_axi_arready,s00_axi_rdata[31:0],s00_axi_rresp[1:0],s00_axi_rvalid,s00_axi_rready,m00_fft_axis_tvalid,m00_fft_axis_tdata[31:0],m00_fft_axis_tstrb[3:0],m00_fft_axis_tlast,m00_fft_axis_tready,m01_fft_axis_tvalid,m01_fft_axis_tdata[31:0],m01_fft_axis_tstrb[3:0],m01_fft_axis_tlast,m01_fft_axis_tready,s00_fft_axis_tvalid,s00_fft_axis_tdata[63:0],s00_fft_axis_tstrb[7:0],s00_fft_axis_tlast,s00_fft_axis_tready,s01_fft_axis_tvalid,s01_fft_axis_tdata[63:0],s01_fft_axis_tstrb[7:0],s01_fft_axis_tlast,s01_fft_axis_tready,m00_dma_axis_tvalid,m00_dma_axis_tdata[63:0],m00_dma_axis_tstrb[7:0],m00_dma_axis_tlast,m00_dma_axis_tready,ADC_PDwN,SYNC,DATA_INA[15:0],DATA_INB[15:0],allowed_clk,azimut8[15:0],azimut_0,s00_axi_aclk,s00_axi_aresetn" */;
  input clk_10MHz;
  input [5:0]s00_axi_awaddr;
  input [2:0]s00_axi_awprot;
  input s00_axi_awvalid;
  output s00_axi_awready;
  input [31:0]s00_axi_wdata;
  input [3:0]s00_axi_wstrb;
  input s00_axi_wvalid;
  output s00_axi_wready;
  output [1:0]s00_axi_bresp;
  output s00_axi_bvalid;
  input s00_axi_bready;
  input [5:0]s00_axi_araddr;
  input [2:0]s00_axi_arprot;
  input s00_axi_arvalid;
  output s00_axi_arready;
  output [31:0]s00_axi_rdata;
  output [1:0]s00_axi_rresp;
  output s00_axi_rvalid;
  input s00_axi_rready;
  output m00_fft_axis_tvalid;
  output [31:0]m00_fft_axis_tdata;
  output [3:0]m00_fft_axis_tstrb;
  output m00_fft_axis_tlast;
  input m00_fft_axis_tready;
  output m01_fft_axis_tvalid;
  output [31:0]m01_fft_axis_tdata;
  output [3:0]m01_fft_axis_tstrb;
  output m01_fft_axis_tlast;
  input m01_fft_axis_tready;
  input s00_fft_axis_tvalid;
  input [63:0]s00_fft_axis_tdata;
  input [7:0]s00_fft_axis_tstrb;
  input s00_fft_axis_tlast;
  output s00_fft_axis_tready;
  input s01_fft_axis_tvalid;
  input [63:0]s01_fft_axis_tdata;
  input [7:0]s01_fft_axis_tstrb;
  input s01_fft_axis_tlast;
  output s01_fft_axis_tready;
  output m00_dma_axis_tvalid;
  output [63:0]m00_dma_axis_tdata;
  output [7:0]m00_dma_axis_tstrb;
  output m00_dma_axis_tlast;
  input m00_dma_axis_tready;
  output ADC_PDwN;
  output SYNC;
  input [15:0]DATA_INA;
  input [15:0]DATA_INB;
  input allowed_clk;
  input [15:0]azimut8;
  input azimut_0;
  input s00_axi_aclk;
  input s00_axi_aresetn;
endmodule
