// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
// Date        : Wed Mar 30 15:48:37 2022
// Host        : alexbtlab-System-Product-Name running 64-bit Ubuntu 20.04.3 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_2_ID_FPGA_0_0_sim_netlist.v
// Design      : design_2_ID_FPGA_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ID_FPGA_v1_0
   (s00_axi_aclk,
    s00_axi_aresetn);
  input s00_axi_aclk;
  input s00_axi_aresetn;

  wire READ;
  wire \cnt_clk_reg[0]_C_n_0 ;
  wire \cnt_clk_reg[0]_LDC_i_1_n_0 ;
  wire \cnt_clk_reg[0]_LDC_i_2_n_0 ;
  wire \cnt_clk_reg[0]_LDC_n_0 ;
  wire \cnt_clk_reg[0]_P_n_0 ;
  wire \cnt_clk_reg[10]_C_n_0 ;
  wire \cnt_clk_reg[10]_LDC_i_1_n_0 ;
  wire \cnt_clk_reg[10]_LDC_i_2_n_0 ;
  wire \cnt_clk_reg[10]_LDC_n_0 ;
  wire \cnt_clk_reg[10]_P_n_0 ;
  wire \cnt_clk_reg[11]_C_n_0 ;
  wire \cnt_clk_reg[11]_LDC_i_1_n_0 ;
  wire \cnt_clk_reg[11]_LDC_i_2_n_0 ;
  wire \cnt_clk_reg[11]_LDC_i_3_n_0 ;
  wire \cnt_clk_reg[11]_LDC_i_3_n_1 ;
  wire \cnt_clk_reg[11]_LDC_i_3_n_2 ;
  wire \cnt_clk_reg[11]_LDC_i_3_n_3 ;
  wire \cnt_clk_reg[11]_LDC_i_4_n_0 ;
  wire \cnt_clk_reg[11]_LDC_i_5_n_0 ;
  wire \cnt_clk_reg[11]_LDC_i_6_n_0 ;
  wire \cnt_clk_reg[11]_LDC_i_7_n_0 ;
  wire \cnt_clk_reg[11]_LDC_n_0 ;
  wire \cnt_clk_reg[11]_P_n_0 ;
  wire \cnt_clk_reg[12]_C_n_0 ;
  wire \cnt_clk_reg[12]_LDC_i_1_n_0 ;
  wire \cnt_clk_reg[12]_LDC_i_2_n_0 ;
  wire \cnt_clk_reg[12]_LDC_n_0 ;
  wire \cnt_clk_reg[12]_P_n_0 ;
  wire \cnt_clk_reg[13]_C_n_0 ;
  wire \cnt_clk_reg[13]_LDC_i_1_n_0 ;
  wire \cnt_clk_reg[13]_LDC_i_2_n_0 ;
  wire \cnt_clk_reg[13]_LDC_n_0 ;
  wire \cnt_clk_reg[13]_P_n_0 ;
  wire \cnt_clk_reg[14]_C_n_0 ;
  wire \cnt_clk_reg[14]_LDC_i_1_n_0 ;
  wire \cnt_clk_reg[14]_LDC_i_2_n_0 ;
  wire \cnt_clk_reg[14]_LDC_n_0 ;
  wire \cnt_clk_reg[14]_P_n_0 ;
  wire \cnt_clk_reg[15]_C_n_0 ;
  wire \cnt_clk_reg[15]_LDC_i_1_n_0 ;
  wire \cnt_clk_reg[15]_LDC_i_2_n_0 ;
  wire \cnt_clk_reg[15]_LDC_i_3_n_0 ;
  wire \cnt_clk_reg[15]_LDC_i_3_n_1 ;
  wire \cnt_clk_reg[15]_LDC_i_3_n_2 ;
  wire \cnt_clk_reg[15]_LDC_i_3_n_3 ;
  wire \cnt_clk_reg[15]_LDC_i_4_n_0 ;
  wire \cnt_clk_reg[15]_LDC_i_5_n_0 ;
  wire \cnt_clk_reg[15]_LDC_i_6_n_0 ;
  wire \cnt_clk_reg[15]_LDC_i_7_n_0 ;
  wire \cnt_clk_reg[15]_LDC_n_0 ;
  wire \cnt_clk_reg[15]_P_n_0 ;
  wire \cnt_clk_reg[16]_C_n_0 ;
  wire \cnt_clk_reg[16]_LDC_i_1_n_0 ;
  wire \cnt_clk_reg[16]_LDC_i_2_n_0 ;
  wire \cnt_clk_reg[16]_LDC_n_0 ;
  wire \cnt_clk_reg[16]_P_n_0 ;
  wire \cnt_clk_reg[17]_C_n_0 ;
  wire \cnt_clk_reg[17]_LDC_i_1_n_0 ;
  wire \cnt_clk_reg[17]_LDC_i_2_n_0 ;
  wire \cnt_clk_reg[17]_LDC_n_0 ;
  wire \cnt_clk_reg[17]_P_n_0 ;
  wire \cnt_clk_reg[18]_C_n_0 ;
  wire \cnt_clk_reg[18]_LDC_i_1_n_0 ;
  wire \cnt_clk_reg[18]_LDC_i_2_n_0 ;
  wire \cnt_clk_reg[18]_LDC_n_0 ;
  wire \cnt_clk_reg[18]_P_n_0 ;
  wire \cnt_clk_reg[19]_C_n_0 ;
  wire \cnt_clk_reg[19]_LDC_i_1_n_0 ;
  wire \cnt_clk_reg[19]_LDC_i_2_n_0 ;
  wire \cnt_clk_reg[19]_LDC_i_3_n_0 ;
  wire \cnt_clk_reg[19]_LDC_i_3_n_1 ;
  wire \cnt_clk_reg[19]_LDC_i_3_n_2 ;
  wire \cnt_clk_reg[19]_LDC_i_3_n_3 ;
  wire \cnt_clk_reg[19]_LDC_i_4_n_0 ;
  wire \cnt_clk_reg[19]_LDC_i_5_n_0 ;
  wire \cnt_clk_reg[19]_LDC_i_6_n_0 ;
  wire \cnt_clk_reg[19]_LDC_i_7_n_0 ;
  wire \cnt_clk_reg[19]_LDC_n_0 ;
  wire \cnt_clk_reg[19]_P_n_0 ;
  wire \cnt_clk_reg[1]_C_n_0 ;
  wire \cnt_clk_reg[1]_LDC_i_1_n_0 ;
  wire \cnt_clk_reg[1]_LDC_i_2_n_0 ;
  wire \cnt_clk_reg[1]_LDC_n_0 ;
  wire \cnt_clk_reg[1]_P_n_0 ;
  wire \cnt_clk_reg[20]_C_n_0 ;
  wire \cnt_clk_reg[20]_LDC_i_1_n_0 ;
  wire \cnt_clk_reg[20]_LDC_i_2_n_0 ;
  wire \cnt_clk_reg[20]_LDC_n_0 ;
  wire \cnt_clk_reg[20]_P_n_0 ;
  wire \cnt_clk_reg[21]_C_n_0 ;
  wire \cnt_clk_reg[21]_LDC_i_1_n_0 ;
  wire \cnt_clk_reg[21]_LDC_i_2_n_0 ;
  wire \cnt_clk_reg[21]_LDC_n_0 ;
  wire \cnt_clk_reg[21]_P_n_0 ;
  wire \cnt_clk_reg[22]_C_n_0 ;
  wire \cnt_clk_reg[22]_LDC_i_1_n_0 ;
  wire \cnt_clk_reg[22]_LDC_i_2_n_0 ;
  wire \cnt_clk_reg[22]_LDC_n_0 ;
  wire \cnt_clk_reg[22]_P_n_0 ;
  wire \cnt_clk_reg[23]_C_n_0 ;
  wire \cnt_clk_reg[23]_LDC_i_1_n_0 ;
  wire \cnt_clk_reg[23]_LDC_i_2_n_0 ;
  wire \cnt_clk_reg[23]_LDC_i_3_n_0 ;
  wire \cnt_clk_reg[23]_LDC_i_3_n_1 ;
  wire \cnt_clk_reg[23]_LDC_i_3_n_2 ;
  wire \cnt_clk_reg[23]_LDC_i_3_n_3 ;
  wire \cnt_clk_reg[23]_LDC_i_4_n_0 ;
  wire \cnt_clk_reg[23]_LDC_i_5_n_0 ;
  wire \cnt_clk_reg[23]_LDC_i_6_n_0 ;
  wire \cnt_clk_reg[23]_LDC_i_7_n_0 ;
  wire \cnt_clk_reg[23]_LDC_n_0 ;
  wire \cnt_clk_reg[23]_P_n_0 ;
  wire \cnt_clk_reg[24]_C_n_0 ;
  wire \cnt_clk_reg[24]_LDC_i_1_n_0 ;
  wire \cnt_clk_reg[24]_LDC_i_2_n_0 ;
  wire \cnt_clk_reg[24]_LDC_n_0 ;
  wire \cnt_clk_reg[24]_P_n_0 ;
  wire \cnt_clk_reg[25]_C_n_0 ;
  wire \cnt_clk_reg[25]_LDC_i_1_n_0 ;
  wire \cnt_clk_reg[25]_LDC_i_2_n_0 ;
  wire \cnt_clk_reg[25]_LDC_n_0 ;
  wire \cnt_clk_reg[25]_P_n_0 ;
  wire \cnt_clk_reg[26]_C_n_0 ;
  wire \cnt_clk_reg[26]_LDC_i_1_n_0 ;
  wire \cnt_clk_reg[26]_LDC_i_2_n_0 ;
  wire \cnt_clk_reg[26]_LDC_n_0 ;
  wire \cnt_clk_reg[26]_P_n_0 ;
  wire \cnt_clk_reg[27]_C_n_0 ;
  wire \cnt_clk_reg[27]_LDC_i_1_n_0 ;
  wire \cnt_clk_reg[27]_LDC_i_2_n_0 ;
  wire \cnt_clk_reg[27]_LDC_i_3_n_0 ;
  wire \cnt_clk_reg[27]_LDC_i_3_n_1 ;
  wire \cnt_clk_reg[27]_LDC_i_3_n_2 ;
  wire \cnt_clk_reg[27]_LDC_i_3_n_3 ;
  wire \cnt_clk_reg[27]_LDC_i_4_n_0 ;
  wire \cnt_clk_reg[27]_LDC_i_5_n_0 ;
  wire \cnt_clk_reg[27]_LDC_i_6_n_0 ;
  wire \cnt_clk_reg[27]_LDC_i_7_n_0 ;
  wire \cnt_clk_reg[27]_LDC_n_0 ;
  wire \cnt_clk_reg[27]_P_n_0 ;
  wire \cnt_clk_reg[28]_C_n_0 ;
  wire \cnt_clk_reg[28]_LDC_i_1_n_0 ;
  wire \cnt_clk_reg[28]_LDC_i_2_n_0 ;
  wire \cnt_clk_reg[28]_LDC_n_0 ;
  wire \cnt_clk_reg[28]_P_n_0 ;
  wire \cnt_clk_reg[29]_C_n_0 ;
  wire \cnt_clk_reg[29]_LDC_i_1_n_0 ;
  wire \cnt_clk_reg[29]_LDC_i_2_n_0 ;
  wire \cnt_clk_reg[29]_LDC_n_0 ;
  wire \cnt_clk_reg[29]_P_n_0 ;
  wire \cnt_clk_reg[2]_C_n_0 ;
  wire \cnt_clk_reg[2]_LDC_i_1_n_0 ;
  wire \cnt_clk_reg[2]_LDC_i_2_n_0 ;
  wire \cnt_clk_reg[2]_LDC_n_0 ;
  wire \cnt_clk_reg[2]_P_n_0 ;
  wire \cnt_clk_reg[30]_C_n_0 ;
  wire \cnt_clk_reg[30]_LDC_i_1_n_0 ;
  wire \cnt_clk_reg[30]_LDC_i_2_n_0 ;
  wire \cnt_clk_reg[30]_LDC_n_0 ;
  wire \cnt_clk_reg[30]_P_n_0 ;
  wire \cnt_clk_reg[31]_C_n_0 ;
  wire \cnt_clk_reg[31]_LDC_i_10_n_0 ;
  wire \cnt_clk_reg[31]_LDC_i_11_n_0 ;
  wire \cnt_clk_reg[31]_LDC_i_12_n_0 ;
  wire \cnt_clk_reg[31]_LDC_i_13_n_0 ;
  wire \cnt_clk_reg[31]_LDC_i_14_n_0 ;
  wire \cnt_clk_reg[31]_LDC_i_15_n_0 ;
  wire \cnt_clk_reg[31]_LDC_i_16_n_0 ;
  wire \cnt_clk_reg[31]_LDC_i_1_n_0 ;
  wire \cnt_clk_reg[31]_LDC_i_2_n_0 ;
  wire \cnt_clk_reg[31]_LDC_i_3_n_1 ;
  wire \cnt_clk_reg[31]_LDC_i_3_n_2 ;
  wire \cnt_clk_reg[31]_LDC_i_3_n_3 ;
  wire \cnt_clk_reg[31]_LDC_i_4_n_0 ;
  wire \cnt_clk_reg[31]_LDC_i_5_n_0 ;
  wire \cnt_clk_reg[31]_LDC_i_6_n_0 ;
  wire \cnt_clk_reg[31]_LDC_i_7_n_0 ;
  wire \cnt_clk_reg[31]_LDC_i_8_n_0 ;
  wire \cnt_clk_reg[31]_LDC_i_9_n_0 ;
  wire \cnt_clk_reg[31]_LDC_n_0 ;
  wire \cnt_clk_reg[31]_P_n_0 ;
  wire \cnt_clk_reg[3]_C_n_0 ;
  wire \cnt_clk_reg[3]_LDC_i_1_n_0 ;
  wire \cnt_clk_reg[3]_LDC_i_2_n_0 ;
  wire \cnt_clk_reg[3]_LDC_i_3_n_0 ;
  wire \cnt_clk_reg[3]_LDC_i_3_n_1 ;
  wire \cnt_clk_reg[3]_LDC_i_3_n_2 ;
  wire \cnt_clk_reg[3]_LDC_i_3_n_3 ;
  wire \cnt_clk_reg[3]_LDC_i_4_n_0 ;
  wire \cnt_clk_reg[3]_LDC_i_5_n_0 ;
  wire \cnt_clk_reg[3]_LDC_i_6_n_0 ;
  wire \cnt_clk_reg[3]_LDC_i_7_n_0 ;
  wire \cnt_clk_reg[3]_LDC_i_8_n_0 ;
  wire \cnt_clk_reg[3]_LDC_n_0 ;
  wire \cnt_clk_reg[3]_P_n_0 ;
  wire \cnt_clk_reg[4]_C_n_0 ;
  wire \cnt_clk_reg[4]_LDC_i_1_n_0 ;
  wire \cnt_clk_reg[4]_LDC_i_2_n_0 ;
  wire \cnt_clk_reg[4]_LDC_n_0 ;
  wire \cnt_clk_reg[4]_P_n_0 ;
  wire \cnt_clk_reg[5]_C_n_0 ;
  wire \cnt_clk_reg[5]_LDC_i_1_n_0 ;
  wire \cnt_clk_reg[5]_LDC_i_2_n_0 ;
  wire \cnt_clk_reg[5]_LDC_n_0 ;
  wire \cnt_clk_reg[5]_P_n_0 ;
  wire \cnt_clk_reg[6]_C_n_0 ;
  wire \cnt_clk_reg[6]_LDC_i_1_n_0 ;
  wire \cnt_clk_reg[6]_LDC_i_2_n_0 ;
  wire \cnt_clk_reg[6]_LDC_n_0 ;
  wire \cnt_clk_reg[6]_P_n_0 ;
  wire \cnt_clk_reg[7]_C_n_0 ;
  wire \cnt_clk_reg[7]_LDC_i_1_n_0 ;
  wire \cnt_clk_reg[7]_LDC_i_2_n_0 ;
  wire \cnt_clk_reg[7]_LDC_i_3_n_0 ;
  wire \cnt_clk_reg[7]_LDC_i_3_n_1 ;
  wire \cnt_clk_reg[7]_LDC_i_3_n_2 ;
  wire \cnt_clk_reg[7]_LDC_i_3_n_3 ;
  wire \cnt_clk_reg[7]_LDC_i_4_n_0 ;
  wire \cnt_clk_reg[7]_LDC_i_5_n_0 ;
  wire \cnt_clk_reg[7]_LDC_i_6_n_0 ;
  wire \cnt_clk_reg[7]_LDC_i_7_n_0 ;
  wire \cnt_clk_reg[7]_LDC_n_0 ;
  wire \cnt_clk_reg[7]_P_n_0 ;
  wire \cnt_clk_reg[8]_C_n_0 ;
  wire \cnt_clk_reg[8]_LDC_i_1_n_0 ;
  wire \cnt_clk_reg[8]_LDC_i_2_n_0 ;
  wire \cnt_clk_reg[8]_LDC_n_0 ;
  wire \cnt_clk_reg[8]_P_n_0 ;
  wire \cnt_clk_reg[9]_C_n_0 ;
  wire \cnt_clk_reg[9]_LDC_i_1_n_0 ;
  wire \cnt_clk_reg[9]_LDC_i_2_n_0 ;
  wire \cnt_clk_reg[9]_LDC_n_0 ;
  wire \cnt_clk_reg[9]_P_n_0 ;
  wire [31:0]data0;
  wire read_r_reg_i_10_n_0;
  wire read_r_reg_i_11_n_0;
  wire read_r_reg_i_12_n_0;
  wire read_r_reg_i_13_n_0;
  wire read_r_reg_i_14_n_0;
  wire read_r_reg_i_15_n_0;
  wire read_r_reg_i_16_n_0;
  wire read_r_reg_i_17_n_0;
  wire read_r_reg_i_1_n_0;
  wire read_r_reg_i_3_n_0;
  wire read_r_reg_i_5_n_0;
  wire read_r_reg_i_6_n_0;
  wire read_r_reg_i_7_n_0;
  wire read_r_reg_i_8_n_0;
  wire s00_axi_aclk;
  wire s00_axi_aresetn;
  wire [31:0]sel0;
  wire [1:0]stateDNA_reg;
  wire \stateDNA_reg_reg[0]_LDC_i_1_n_0 ;
  wire \stateDNA_reg_reg[0]_LDC_i_2_n_0 ;
  wire \stateDNA_reg_reg[0]_LDC_n_0 ;
  wire \stateDNA_reg_reg[0]_P_n_0 ;
  wire \stateDNA_reg_reg[1]_LDC_i_1_n_0 ;
  wire \stateDNA_reg_reg[1]_LDC_i_2_n_0 ;
  wire \stateDNA_reg_reg[1]_LDC_i_3_n_0 ;
  wire \stateDNA_reg_reg[1]_LDC_n_0 ;
  wire \stateDNA_reg_reg[1]_P_n_0 ;
  wire NLW_DNA_PORT_inst_DOUT_UNCONNECTED;
  wire [3:3]\NLW_cnt_clk_reg[31]_LDC_i_3_CO_UNCONNECTED ;

  (* BOX_TYPE = "PRIMITIVE" *) 
  DNA_PORT #(
    .SIM_DNA_VALUE(57'h123456789012345)) 
    DNA_PORT_inst
       (.CLK(s00_axi_aclk),
        .DIN(1'b0),
        .DOUT(NLW_DNA_PORT_inst_DOUT_UNCONNECTED),
        .READ(READ),
        .SHIFT(1'b0));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk[0]_C_i_1 
       (.I0(\cnt_clk_reg[0]_P_n_0 ),
        .I1(\cnt_clk_reg[0]_LDC_n_0 ),
        .I2(\cnt_clk_reg[0]_C_n_0 ),
        .O(sel0[0]));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk[10]_C_i_1 
       (.I0(\cnt_clk_reg[10]_P_n_0 ),
        .I1(\cnt_clk_reg[10]_LDC_n_0 ),
        .I2(\cnt_clk_reg[10]_C_n_0 ),
        .O(sel0[10]));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk[11]_C_i_1 
       (.I0(\cnt_clk_reg[11]_P_n_0 ),
        .I1(\cnt_clk_reg[11]_LDC_n_0 ),
        .I2(\cnt_clk_reg[11]_C_n_0 ),
        .O(sel0[11]));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk[12]_C_i_1 
       (.I0(\cnt_clk_reg[12]_P_n_0 ),
        .I1(\cnt_clk_reg[12]_LDC_n_0 ),
        .I2(\cnt_clk_reg[12]_C_n_0 ),
        .O(sel0[12]));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk[13]_C_i_1 
       (.I0(\cnt_clk_reg[13]_P_n_0 ),
        .I1(\cnt_clk_reg[13]_LDC_n_0 ),
        .I2(\cnt_clk_reg[13]_C_n_0 ),
        .O(sel0[13]));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk[14]_C_i_1 
       (.I0(\cnt_clk_reg[14]_P_n_0 ),
        .I1(\cnt_clk_reg[14]_LDC_n_0 ),
        .I2(\cnt_clk_reg[14]_C_n_0 ),
        .O(sel0[14]));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk[15]_C_i_1 
       (.I0(\cnt_clk_reg[15]_P_n_0 ),
        .I1(\cnt_clk_reg[15]_LDC_n_0 ),
        .I2(\cnt_clk_reg[15]_C_n_0 ),
        .O(sel0[15]));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk[16]_C_i_1 
       (.I0(\cnt_clk_reg[16]_P_n_0 ),
        .I1(\cnt_clk_reg[16]_LDC_n_0 ),
        .I2(\cnt_clk_reg[16]_C_n_0 ),
        .O(sel0[16]));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk[17]_C_i_1 
       (.I0(\cnt_clk_reg[17]_P_n_0 ),
        .I1(\cnt_clk_reg[17]_LDC_n_0 ),
        .I2(\cnt_clk_reg[17]_C_n_0 ),
        .O(sel0[17]));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk[18]_C_i_1 
       (.I0(\cnt_clk_reg[18]_P_n_0 ),
        .I1(\cnt_clk_reg[18]_LDC_n_0 ),
        .I2(\cnt_clk_reg[18]_C_n_0 ),
        .O(sel0[18]));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk[19]_C_i_1 
       (.I0(\cnt_clk_reg[19]_P_n_0 ),
        .I1(\cnt_clk_reg[19]_LDC_n_0 ),
        .I2(\cnt_clk_reg[19]_C_n_0 ),
        .O(sel0[19]));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk[1]_C_i_1 
       (.I0(\cnt_clk_reg[1]_P_n_0 ),
        .I1(\cnt_clk_reg[1]_LDC_n_0 ),
        .I2(\cnt_clk_reg[1]_C_n_0 ),
        .O(sel0[1]));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk[20]_C_i_1 
       (.I0(\cnt_clk_reg[20]_P_n_0 ),
        .I1(\cnt_clk_reg[20]_LDC_n_0 ),
        .I2(\cnt_clk_reg[20]_C_n_0 ),
        .O(sel0[20]));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk[21]_C_i_1 
       (.I0(\cnt_clk_reg[21]_P_n_0 ),
        .I1(\cnt_clk_reg[21]_LDC_n_0 ),
        .I2(\cnt_clk_reg[21]_C_n_0 ),
        .O(sel0[21]));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk[22]_C_i_1 
       (.I0(\cnt_clk_reg[22]_P_n_0 ),
        .I1(\cnt_clk_reg[22]_LDC_n_0 ),
        .I2(\cnt_clk_reg[22]_C_n_0 ),
        .O(sel0[22]));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk[23]_C_i_1 
       (.I0(\cnt_clk_reg[23]_P_n_0 ),
        .I1(\cnt_clk_reg[23]_LDC_n_0 ),
        .I2(\cnt_clk_reg[23]_C_n_0 ),
        .O(sel0[23]));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk[24]_C_i_1 
       (.I0(\cnt_clk_reg[24]_P_n_0 ),
        .I1(\cnt_clk_reg[24]_LDC_n_0 ),
        .I2(\cnt_clk_reg[24]_C_n_0 ),
        .O(sel0[24]));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk[25]_C_i_1 
       (.I0(\cnt_clk_reg[25]_P_n_0 ),
        .I1(\cnt_clk_reg[25]_LDC_n_0 ),
        .I2(\cnt_clk_reg[25]_C_n_0 ),
        .O(sel0[25]));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk[26]_C_i_1 
       (.I0(\cnt_clk_reg[26]_P_n_0 ),
        .I1(\cnt_clk_reg[26]_LDC_n_0 ),
        .I2(\cnt_clk_reg[26]_C_n_0 ),
        .O(sel0[26]));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk[27]_C_i_1 
       (.I0(\cnt_clk_reg[27]_P_n_0 ),
        .I1(\cnt_clk_reg[27]_LDC_n_0 ),
        .I2(\cnt_clk_reg[27]_C_n_0 ),
        .O(sel0[27]));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk[28]_C_i_1 
       (.I0(\cnt_clk_reg[28]_P_n_0 ),
        .I1(\cnt_clk_reg[28]_LDC_n_0 ),
        .I2(\cnt_clk_reg[28]_C_n_0 ),
        .O(sel0[28]));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk[29]_C_i_1 
       (.I0(\cnt_clk_reg[29]_P_n_0 ),
        .I1(\cnt_clk_reg[29]_LDC_n_0 ),
        .I2(\cnt_clk_reg[29]_C_n_0 ),
        .O(sel0[29]));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk[2]_C_i_1 
       (.I0(\cnt_clk_reg[2]_P_n_0 ),
        .I1(\cnt_clk_reg[2]_LDC_n_0 ),
        .I2(\cnt_clk_reg[2]_C_n_0 ),
        .O(sel0[2]));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk[30]_C_i_1 
       (.I0(\cnt_clk_reg[30]_P_n_0 ),
        .I1(\cnt_clk_reg[30]_LDC_n_0 ),
        .I2(\cnt_clk_reg[30]_C_n_0 ),
        .O(sel0[30]));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk[31]_C_i_1 
       (.I0(\cnt_clk_reg[31]_P_n_0 ),
        .I1(\cnt_clk_reg[31]_LDC_n_0 ),
        .I2(\cnt_clk_reg[31]_C_n_0 ),
        .O(sel0[31]));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk[3]_C_i_1 
       (.I0(\cnt_clk_reg[3]_P_n_0 ),
        .I1(\cnt_clk_reg[3]_LDC_n_0 ),
        .I2(\cnt_clk_reg[3]_C_n_0 ),
        .O(sel0[3]));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk[4]_C_i_1 
       (.I0(\cnt_clk_reg[4]_P_n_0 ),
        .I1(\cnt_clk_reg[4]_LDC_n_0 ),
        .I2(\cnt_clk_reg[4]_C_n_0 ),
        .O(sel0[4]));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk[5]_C_i_1 
       (.I0(\cnt_clk_reg[5]_P_n_0 ),
        .I1(\cnt_clk_reg[5]_LDC_n_0 ),
        .I2(\cnt_clk_reg[5]_C_n_0 ),
        .O(sel0[5]));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk[6]_C_i_1 
       (.I0(\cnt_clk_reg[6]_P_n_0 ),
        .I1(\cnt_clk_reg[6]_LDC_n_0 ),
        .I2(\cnt_clk_reg[6]_C_n_0 ),
        .O(sel0[6]));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk[7]_C_i_1 
       (.I0(\cnt_clk_reg[7]_P_n_0 ),
        .I1(\cnt_clk_reg[7]_LDC_n_0 ),
        .I2(\cnt_clk_reg[7]_C_n_0 ),
        .O(sel0[7]));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk[8]_C_i_1 
       (.I0(\cnt_clk_reg[8]_P_n_0 ),
        .I1(\cnt_clk_reg[8]_LDC_n_0 ),
        .I2(\cnt_clk_reg[8]_C_n_0 ),
        .O(sel0[8]));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk[9]_C_i_1 
       (.I0(\cnt_clk_reg[9]_P_n_0 ),
        .I1(\cnt_clk_reg[9]_LDC_n_0 ),
        .I2(\cnt_clk_reg[9]_C_n_0 ),
        .O(sel0[9]));
  FDCE \cnt_clk_reg[0]_C 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(\cnt_clk_reg[0]_LDC_i_2_n_0 ),
        .D(sel0[0]),
        .Q(\cnt_clk_reg[0]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \cnt_clk_reg[0]_LDC 
       (.CLR(\cnt_clk_reg[0]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\cnt_clk_reg[0]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\cnt_clk_reg[0]_LDC_n_0 ));
  LUT6 #(
    .INIT(64'h8888888888888088)) 
    \cnt_clk_reg[0]_LDC_i_1 
       (.I0(data0[0]),
        .I1(s00_axi_aresetn),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[0]_LDC_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h2222222222222A22)) 
    \cnt_clk_reg[0]_LDC_i_2 
       (.I0(s00_axi_aresetn),
        .I1(data0[0]),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[0]_LDC_i_2_n_0 ));
  FDPE \cnt_clk_reg[0]_P 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sel0[0]),
        .PRE(\cnt_clk_reg[0]_LDC_i_1_n_0 ),
        .Q(\cnt_clk_reg[0]_P_n_0 ));
  FDCE \cnt_clk_reg[10]_C 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(\cnt_clk_reg[10]_LDC_i_2_n_0 ),
        .D(sel0[10]),
        .Q(\cnt_clk_reg[10]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \cnt_clk_reg[10]_LDC 
       (.CLR(\cnt_clk_reg[10]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\cnt_clk_reg[10]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\cnt_clk_reg[10]_LDC_n_0 ));
  LUT6 #(
    .INIT(64'h8888888888888088)) 
    \cnt_clk_reg[10]_LDC_i_1 
       (.I0(data0[10]),
        .I1(s00_axi_aresetn),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[10]_LDC_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h2222222222222A22)) 
    \cnt_clk_reg[10]_LDC_i_2 
       (.I0(s00_axi_aresetn),
        .I1(data0[10]),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[10]_LDC_i_2_n_0 ));
  FDPE \cnt_clk_reg[10]_P 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sel0[10]),
        .PRE(\cnt_clk_reg[10]_LDC_i_1_n_0 ),
        .Q(\cnt_clk_reg[10]_P_n_0 ));
  FDCE \cnt_clk_reg[11]_C 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(\cnt_clk_reg[11]_LDC_i_2_n_0 ),
        .D(sel0[11]),
        .Q(\cnt_clk_reg[11]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \cnt_clk_reg[11]_LDC 
       (.CLR(\cnt_clk_reg[11]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\cnt_clk_reg[11]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\cnt_clk_reg[11]_LDC_n_0 ));
  LUT6 #(
    .INIT(64'h8888888888888088)) 
    \cnt_clk_reg[11]_LDC_i_1 
       (.I0(data0[11]),
        .I1(s00_axi_aresetn),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[11]_LDC_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h2222222222222A22)) 
    \cnt_clk_reg[11]_LDC_i_2 
       (.I0(s00_axi_aresetn),
        .I1(data0[11]),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[11]_LDC_i_2_n_0 ));
  CARRY4 \cnt_clk_reg[11]_LDC_i_3 
       (.CI(\cnt_clk_reg[7]_LDC_i_3_n_0 ),
        .CO({\cnt_clk_reg[11]_LDC_i_3_n_0 ,\cnt_clk_reg[11]_LDC_i_3_n_1 ,\cnt_clk_reg[11]_LDC_i_3_n_2 ,\cnt_clk_reg[11]_LDC_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[11:8]),
        .S({\cnt_clk_reg[11]_LDC_i_4_n_0 ,\cnt_clk_reg[11]_LDC_i_5_n_0 ,\cnt_clk_reg[11]_LDC_i_6_n_0 ,\cnt_clk_reg[11]_LDC_i_7_n_0 }));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk_reg[11]_LDC_i_4 
       (.I0(\cnt_clk_reg[11]_P_n_0 ),
        .I1(\cnt_clk_reg[11]_LDC_n_0 ),
        .I2(\cnt_clk_reg[11]_C_n_0 ),
        .O(\cnt_clk_reg[11]_LDC_i_4_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk_reg[11]_LDC_i_5 
       (.I0(\cnt_clk_reg[10]_P_n_0 ),
        .I1(\cnt_clk_reg[10]_LDC_n_0 ),
        .I2(\cnt_clk_reg[10]_C_n_0 ),
        .O(\cnt_clk_reg[11]_LDC_i_5_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk_reg[11]_LDC_i_6 
       (.I0(\cnt_clk_reg[9]_P_n_0 ),
        .I1(\cnt_clk_reg[9]_LDC_n_0 ),
        .I2(\cnt_clk_reg[9]_C_n_0 ),
        .O(\cnt_clk_reg[11]_LDC_i_6_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk_reg[11]_LDC_i_7 
       (.I0(\cnt_clk_reg[8]_P_n_0 ),
        .I1(\cnt_clk_reg[8]_LDC_n_0 ),
        .I2(\cnt_clk_reg[8]_C_n_0 ),
        .O(\cnt_clk_reg[11]_LDC_i_7_n_0 ));
  FDPE \cnt_clk_reg[11]_P 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sel0[11]),
        .PRE(\cnt_clk_reg[11]_LDC_i_1_n_0 ),
        .Q(\cnt_clk_reg[11]_P_n_0 ));
  FDCE \cnt_clk_reg[12]_C 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(\cnt_clk_reg[12]_LDC_i_2_n_0 ),
        .D(sel0[12]),
        .Q(\cnt_clk_reg[12]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \cnt_clk_reg[12]_LDC 
       (.CLR(\cnt_clk_reg[12]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\cnt_clk_reg[12]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\cnt_clk_reg[12]_LDC_n_0 ));
  LUT6 #(
    .INIT(64'h8888888888888088)) 
    \cnt_clk_reg[12]_LDC_i_1 
       (.I0(data0[12]),
        .I1(s00_axi_aresetn),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[12]_LDC_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h2222222222222A22)) 
    \cnt_clk_reg[12]_LDC_i_2 
       (.I0(s00_axi_aresetn),
        .I1(data0[12]),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[12]_LDC_i_2_n_0 ));
  FDPE \cnt_clk_reg[12]_P 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sel0[12]),
        .PRE(\cnt_clk_reg[12]_LDC_i_1_n_0 ),
        .Q(\cnt_clk_reg[12]_P_n_0 ));
  FDCE \cnt_clk_reg[13]_C 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(\cnt_clk_reg[13]_LDC_i_2_n_0 ),
        .D(sel0[13]),
        .Q(\cnt_clk_reg[13]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \cnt_clk_reg[13]_LDC 
       (.CLR(\cnt_clk_reg[13]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\cnt_clk_reg[13]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\cnt_clk_reg[13]_LDC_n_0 ));
  LUT6 #(
    .INIT(64'h8888888888888088)) 
    \cnt_clk_reg[13]_LDC_i_1 
       (.I0(data0[13]),
        .I1(s00_axi_aresetn),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[13]_LDC_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h2222222222222A22)) 
    \cnt_clk_reg[13]_LDC_i_2 
       (.I0(s00_axi_aresetn),
        .I1(data0[13]),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[13]_LDC_i_2_n_0 ));
  FDPE \cnt_clk_reg[13]_P 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sel0[13]),
        .PRE(\cnt_clk_reg[13]_LDC_i_1_n_0 ),
        .Q(\cnt_clk_reg[13]_P_n_0 ));
  FDCE \cnt_clk_reg[14]_C 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(\cnt_clk_reg[14]_LDC_i_2_n_0 ),
        .D(sel0[14]),
        .Q(\cnt_clk_reg[14]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \cnt_clk_reg[14]_LDC 
       (.CLR(\cnt_clk_reg[14]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\cnt_clk_reg[14]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\cnt_clk_reg[14]_LDC_n_0 ));
  LUT6 #(
    .INIT(64'h8888888888888088)) 
    \cnt_clk_reg[14]_LDC_i_1 
       (.I0(data0[14]),
        .I1(s00_axi_aresetn),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[14]_LDC_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h2222222222222A22)) 
    \cnt_clk_reg[14]_LDC_i_2 
       (.I0(s00_axi_aresetn),
        .I1(data0[14]),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[14]_LDC_i_2_n_0 ));
  FDPE \cnt_clk_reg[14]_P 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sel0[14]),
        .PRE(\cnt_clk_reg[14]_LDC_i_1_n_0 ),
        .Q(\cnt_clk_reg[14]_P_n_0 ));
  FDCE \cnt_clk_reg[15]_C 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(\cnt_clk_reg[15]_LDC_i_2_n_0 ),
        .D(sel0[15]),
        .Q(\cnt_clk_reg[15]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \cnt_clk_reg[15]_LDC 
       (.CLR(\cnt_clk_reg[15]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\cnt_clk_reg[15]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\cnt_clk_reg[15]_LDC_n_0 ));
  LUT6 #(
    .INIT(64'h8888888888888088)) 
    \cnt_clk_reg[15]_LDC_i_1 
       (.I0(data0[15]),
        .I1(s00_axi_aresetn),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[15]_LDC_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h2222222222222A22)) 
    \cnt_clk_reg[15]_LDC_i_2 
       (.I0(s00_axi_aresetn),
        .I1(data0[15]),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[15]_LDC_i_2_n_0 ));
  CARRY4 \cnt_clk_reg[15]_LDC_i_3 
       (.CI(\cnt_clk_reg[11]_LDC_i_3_n_0 ),
        .CO({\cnt_clk_reg[15]_LDC_i_3_n_0 ,\cnt_clk_reg[15]_LDC_i_3_n_1 ,\cnt_clk_reg[15]_LDC_i_3_n_2 ,\cnt_clk_reg[15]_LDC_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[15:12]),
        .S({\cnt_clk_reg[15]_LDC_i_4_n_0 ,\cnt_clk_reg[15]_LDC_i_5_n_0 ,\cnt_clk_reg[15]_LDC_i_6_n_0 ,\cnt_clk_reg[15]_LDC_i_7_n_0 }));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk_reg[15]_LDC_i_4 
       (.I0(\cnt_clk_reg[15]_P_n_0 ),
        .I1(\cnt_clk_reg[15]_LDC_n_0 ),
        .I2(\cnt_clk_reg[15]_C_n_0 ),
        .O(\cnt_clk_reg[15]_LDC_i_4_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk_reg[15]_LDC_i_5 
       (.I0(\cnt_clk_reg[14]_P_n_0 ),
        .I1(\cnt_clk_reg[14]_LDC_n_0 ),
        .I2(\cnt_clk_reg[14]_C_n_0 ),
        .O(\cnt_clk_reg[15]_LDC_i_5_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk_reg[15]_LDC_i_6 
       (.I0(\cnt_clk_reg[13]_P_n_0 ),
        .I1(\cnt_clk_reg[13]_LDC_n_0 ),
        .I2(\cnt_clk_reg[13]_C_n_0 ),
        .O(\cnt_clk_reg[15]_LDC_i_6_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk_reg[15]_LDC_i_7 
       (.I0(\cnt_clk_reg[12]_P_n_0 ),
        .I1(\cnt_clk_reg[12]_LDC_n_0 ),
        .I2(\cnt_clk_reg[12]_C_n_0 ),
        .O(\cnt_clk_reg[15]_LDC_i_7_n_0 ));
  FDPE \cnt_clk_reg[15]_P 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sel0[15]),
        .PRE(\cnt_clk_reg[15]_LDC_i_1_n_0 ),
        .Q(\cnt_clk_reg[15]_P_n_0 ));
  FDCE \cnt_clk_reg[16]_C 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(\cnt_clk_reg[16]_LDC_i_2_n_0 ),
        .D(sel0[16]),
        .Q(\cnt_clk_reg[16]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \cnt_clk_reg[16]_LDC 
       (.CLR(\cnt_clk_reg[16]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\cnt_clk_reg[16]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\cnt_clk_reg[16]_LDC_n_0 ));
  LUT6 #(
    .INIT(64'h8888888888888088)) 
    \cnt_clk_reg[16]_LDC_i_1 
       (.I0(data0[16]),
        .I1(s00_axi_aresetn),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[16]_LDC_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h2222222222222A22)) 
    \cnt_clk_reg[16]_LDC_i_2 
       (.I0(s00_axi_aresetn),
        .I1(data0[16]),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[16]_LDC_i_2_n_0 ));
  FDPE \cnt_clk_reg[16]_P 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sel0[16]),
        .PRE(\cnt_clk_reg[16]_LDC_i_1_n_0 ),
        .Q(\cnt_clk_reg[16]_P_n_0 ));
  FDCE \cnt_clk_reg[17]_C 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(\cnt_clk_reg[17]_LDC_i_2_n_0 ),
        .D(sel0[17]),
        .Q(\cnt_clk_reg[17]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \cnt_clk_reg[17]_LDC 
       (.CLR(\cnt_clk_reg[17]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\cnt_clk_reg[17]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\cnt_clk_reg[17]_LDC_n_0 ));
  LUT6 #(
    .INIT(64'h8888888888888088)) 
    \cnt_clk_reg[17]_LDC_i_1 
       (.I0(data0[17]),
        .I1(s00_axi_aresetn),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[17]_LDC_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h2222222222222A22)) 
    \cnt_clk_reg[17]_LDC_i_2 
       (.I0(s00_axi_aresetn),
        .I1(data0[17]),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[17]_LDC_i_2_n_0 ));
  FDPE \cnt_clk_reg[17]_P 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sel0[17]),
        .PRE(\cnt_clk_reg[17]_LDC_i_1_n_0 ),
        .Q(\cnt_clk_reg[17]_P_n_0 ));
  FDCE \cnt_clk_reg[18]_C 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(\cnt_clk_reg[18]_LDC_i_2_n_0 ),
        .D(sel0[18]),
        .Q(\cnt_clk_reg[18]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \cnt_clk_reg[18]_LDC 
       (.CLR(\cnt_clk_reg[18]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\cnt_clk_reg[18]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\cnt_clk_reg[18]_LDC_n_0 ));
  LUT6 #(
    .INIT(64'h8888888888888088)) 
    \cnt_clk_reg[18]_LDC_i_1 
       (.I0(data0[18]),
        .I1(s00_axi_aresetn),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[18]_LDC_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h2222222222222A22)) 
    \cnt_clk_reg[18]_LDC_i_2 
       (.I0(s00_axi_aresetn),
        .I1(data0[18]),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[18]_LDC_i_2_n_0 ));
  FDPE \cnt_clk_reg[18]_P 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sel0[18]),
        .PRE(\cnt_clk_reg[18]_LDC_i_1_n_0 ),
        .Q(\cnt_clk_reg[18]_P_n_0 ));
  FDCE \cnt_clk_reg[19]_C 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(\cnt_clk_reg[19]_LDC_i_2_n_0 ),
        .D(sel0[19]),
        .Q(\cnt_clk_reg[19]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \cnt_clk_reg[19]_LDC 
       (.CLR(\cnt_clk_reg[19]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\cnt_clk_reg[19]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\cnt_clk_reg[19]_LDC_n_0 ));
  LUT6 #(
    .INIT(64'h8888888888888088)) 
    \cnt_clk_reg[19]_LDC_i_1 
       (.I0(data0[19]),
        .I1(s00_axi_aresetn),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[19]_LDC_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h2222222222222A22)) 
    \cnt_clk_reg[19]_LDC_i_2 
       (.I0(s00_axi_aresetn),
        .I1(data0[19]),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[19]_LDC_i_2_n_0 ));
  CARRY4 \cnt_clk_reg[19]_LDC_i_3 
       (.CI(\cnt_clk_reg[15]_LDC_i_3_n_0 ),
        .CO({\cnt_clk_reg[19]_LDC_i_3_n_0 ,\cnt_clk_reg[19]_LDC_i_3_n_1 ,\cnt_clk_reg[19]_LDC_i_3_n_2 ,\cnt_clk_reg[19]_LDC_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[19:16]),
        .S({\cnt_clk_reg[19]_LDC_i_4_n_0 ,\cnt_clk_reg[19]_LDC_i_5_n_0 ,\cnt_clk_reg[19]_LDC_i_6_n_0 ,\cnt_clk_reg[19]_LDC_i_7_n_0 }));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk_reg[19]_LDC_i_4 
       (.I0(\cnt_clk_reg[19]_P_n_0 ),
        .I1(\cnt_clk_reg[19]_LDC_n_0 ),
        .I2(\cnt_clk_reg[19]_C_n_0 ),
        .O(\cnt_clk_reg[19]_LDC_i_4_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk_reg[19]_LDC_i_5 
       (.I0(\cnt_clk_reg[18]_P_n_0 ),
        .I1(\cnt_clk_reg[18]_LDC_n_0 ),
        .I2(\cnt_clk_reg[18]_C_n_0 ),
        .O(\cnt_clk_reg[19]_LDC_i_5_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk_reg[19]_LDC_i_6 
       (.I0(\cnt_clk_reg[17]_P_n_0 ),
        .I1(\cnt_clk_reg[17]_LDC_n_0 ),
        .I2(\cnt_clk_reg[17]_C_n_0 ),
        .O(\cnt_clk_reg[19]_LDC_i_6_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk_reg[19]_LDC_i_7 
       (.I0(\cnt_clk_reg[16]_P_n_0 ),
        .I1(\cnt_clk_reg[16]_LDC_n_0 ),
        .I2(\cnt_clk_reg[16]_C_n_0 ),
        .O(\cnt_clk_reg[19]_LDC_i_7_n_0 ));
  FDPE \cnt_clk_reg[19]_P 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sel0[19]),
        .PRE(\cnt_clk_reg[19]_LDC_i_1_n_0 ),
        .Q(\cnt_clk_reg[19]_P_n_0 ));
  FDCE \cnt_clk_reg[1]_C 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(\cnt_clk_reg[1]_LDC_i_2_n_0 ),
        .D(sel0[1]),
        .Q(\cnt_clk_reg[1]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \cnt_clk_reg[1]_LDC 
       (.CLR(\cnt_clk_reg[1]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\cnt_clk_reg[1]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\cnt_clk_reg[1]_LDC_n_0 ));
  LUT6 #(
    .INIT(64'h8888888888888088)) 
    \cnt_clk_reg[1]_LDC_i_1 
       (.I0(data0[1]),
        .I1(s00_axi_aresetn),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[1]_LDC_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h2222222222222A22)) 
    \cnt_clk_reg[1]_LDC_i_2 
       (.I0(s00_axi_aresetn),
        .I1(data0[1]),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[1]_LDC_i_2_n_0 ));
  FDPE \cnt_clk_reg[1]_P 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sel0[1]),
        .PRE(\cnt_clk_reg[1]_LDC_i_1_n_0 ),
        .Q(\cnt_clk_reg[1]_P_n_0 ));
  FDCE \cnt_clk_reg[20]_C 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(\cnt_clk_reg[20]_LDC_i_2_n_0 ),
        .D(sel0[20]),
        .Q(\cnt_clk_reg[20]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \cnt_clk_reg[20]_LDC 
       (.CLR(\cnt_clk_reg[20]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\cnt_clk_reg[20]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\cnt_clk_reg[20]_LDC_n_0 ));
  LUT6 #(
    .INIT(64'h8888888888888088)) 
    \cnt_clk_reg[20]_LDC_i_1 
       (.I0(data0[20]),
        .I1(s00_axi_aresetn),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[20]_LDC_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h2222222222222A22)) 
    \cnt_clk_reg[20]_LDC_i_2 
       (.I0(s00_axi_aresetn),
        .I1(data0[20]),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[20]_LDC_i_2_n_0 ));
  FDPE \cnt_clk_reg[20]_P 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sel0[20]),
        .PRE(\cnt_clk_reg[20]_LDC_i_1_n_0 ),
        .Q(\cnt_clk_reg[20]_P_n_0 ));
  FDCE \cnt_clk_reg[21]_C 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(\cnt_clk_reg[21]_LDC_i_2_n_0 ),
        .D(sel0[21]),
        .Q(\cnt_clk_reg[21]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \cnt_clk_reg[21]_LDC 
       (.CLR(\cnt_clk_reg[21]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\cnt_clk_reg[21]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\cnt_clk_reg[21]_LDC_n_0 ));
  LUT6 #(
    .INIT(64'h8888888888888088)) 
    \cnt_clk_reg[21]_LDC_i_1 
       (.I0(data0[21]),
        .I1(s00_axi_aresetn),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[21]_LDC_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h2222222222222A22)) 
    \cnt_clk_reg[21]_LDC_i_2 
       (.I0(s00_axi_aresetn),
        .I1(data0[21]),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[21]_LDC_i_2_n_0 ));
  FDPE \cnt_clk_reg[21]_P 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sel0[21]),
        .PRE(\cnt_clk_reg[21]_LDC_i_1_n_0 ),
        .Q(\cnt_clk_reg[21]_P_n_0 ));
  FDCE \cnt_clk_reg[22]_C 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(\cnt_clk_reg[22]_LDC_i_2_n_0 ),
        .D(sel0[22]),
        .Q(\cnt_clk_reg[22]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \cnt_clk_reg[22]_LDC 
       (.CLR(\cnt_clk_reg[22]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\cnt_clk_reg[22]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\cnt_clk_reg[22]_LDC_n_0 ));
  LUT6 #(
    .INIT(64'h8888888888888088)) 
    \cnt_clk_reg[22]_LDC_i_1 
       (.I0(data0[22]),
        .I1(s00_axi_aresetn),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[22]_LDC_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h2222222222222A22)) 
    \cnt_clk_reg[22]_LDC_i_2 
       (.I0(s00_axi_aresetn),
        .I1(data0[22]),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[22]_LDC_i_2_n_0 ));
  FDPE \cnt_clk_reg[22]_P 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sel0[22]),
        .PRE(\cnt_clk_reg[22]_LDC_i_1_n_0 ),
        .Q(\cnt_clk_reg[22]_P_n_0 ));
  FDCE \cnt_clk_reg[23]_C 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(\cnt_clk_reg[23]_LDC_i_2_n_0 ),
        .D(sel0[23]),
        .Q(\cnt_clk_reg[23]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \cnt_clk_reg[23]_LDC 
       (.CLR(\cnt_clk_reg[23]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\cnt_clk_reg[23]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\cnt_clk_reg[23]_LDC_n_0 ));
  LUT6 #(
    .INIT(64'h8888888888888088)) 
    \cnt_clk_reg[23]_LDC_i_1 
       (.I0(data0[23]),
        .I1(s00_axi_aresetn),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[23]_LDC_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h2222222222222A22)) 
    \cnt_clk_reg[23]_LDC_i_2 
       (.I0(s00_axi_aresetn),
        .I1(data0[23]),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[23]_LDC_i_2_n_0 ));
  CARRY4 \cnt_clk_reg[23]_LDC_i_3 
       (.CI(\cnt_clk_reg[19]_LDC_i_3_n_0 ),
        .CO({\cnt_clk_reg[23]_LDC_i_3_n_0 ,\cnt_clk_reg[23]_LDC_i_3_n_1 ,\cnt_clk_reg[23]_LDC_i_3_n_2 ,\cnt_clk_reg[23]_LDC_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[23:20]),
        .S({\cnt_clk_reg[23]_LDC_i_4_n_0 ,\cnt_clk_reg[23]_LDC_i_5_n_0 ,\cnt_clk_reg[23]_LDC_i_6_n_0 ,\cnt_clk_reg[23]_LDC_i_7_n_0 }));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk_reg[23]_LDC_i_4 
       (.I0(\cnt_clk_reg[23]_P_n_0 ),
        .I1(\cnt_clk_reg[23]_LDC_n_0 ),
        .I2(\cnt_clk_reg[23]_C_n_0 ),
        .O(\cnt_clk_reg[23]_LDC_i_4_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk_reg[23]_LDC_i_5 
       (.I0(\cnt_clk_reg[22]_P_n_0 ),
        .I1(\cnt_clk_reg[22]_LDC_n_0 ),
        .I2(\cnt_clk_reg[22]_C_n_0 ),
        .O(\cnt_clk_reg[23]_LDC_i_5_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk_reg[23]_LDC_i_6 
       (.I0(\cnt_clk_reg[21]_P_n_0 ),
        .I1(\cnt_clk_reg[21]_LDC_n_0 ),
        .I2(\cnt_clk_reg[21]_C_n_0 ),
        .O(\cnt_clk_reg[23]_LDC_i_6_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk_reg[23]_LDC_i_7 
       (.I0(\cnt_clk_reg[20]_P_n_0 ),
        .I1(\cnt_clk_reg[20]_LDC_n_0 ),
        .I2(\cnt_clk_reg[20]_C_n_0 ),
        .O(\cnt_clk_reg[23]_LDC_i_7_n_0 ));
  FDPE \cnt_clk_reg[23]_P 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sel0[23]),
        .PRE(\cnt_clk_reg[23]_LDC_i_1_n_0 ),
        .Q(\cnt_clk_reg[23]_P_n_0 ));
  FDCE \cnt_clk_reg[24]_C 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(\cnt_clk_reg[24]_LDC_i_2_n_0 ),
        .D(sel0[24]),
        .Q(\cnt_clk_reg[24]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \cnt_clk_reg[24]_LDC 
       (.CLR(\cnt_clk_reg[24]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\cnt_clk_reg[24]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\cnt_clk_reg[24]_LDC_n_0 ));
  LUT6 #(
    .INIT(64'h8888888888888088)) 
    \cnt_clk_reg[24]_LDC_i_1 
       (.I0(data0[24]),
        .I1(s00_axi_aresetn),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[24]_LDC_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h2222222222222A22)) 
    \cnt_clk_reg[24]_LDC_i_2 
       (.I0(s00_axi_aresetn),
        .I1(data0[24]),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[24]_LDC_i_2_n_0 ));
  FDPE \cnt_clk_reg[24]_P 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sel0[24]),
        .PRE(\cnt_clk_reg[24]_LDC_i_1_n_0 ),
        .Q(\cnt_clk_reg[24]_P_n_0 ));
  FDCE \cnt_clk_reg[25]_C 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(\cnt_clk_reg[25]_LDC_i_2_n_0 ),
        .D(sel0[25]),
        .Q(\cnt_clk_reg[25]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \cnt_clk_reg[25]_LDC 
       (.CLR(\cnt_clk_reg[25]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\cnt_clk_reg[25]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\cnt_clk_reg[25]_LDC_n_0 ));
  LUT6 #(
    .INIT(64'h8888888888888088)) 
    \cnt_clk_reg[25]_LDC_i_1 
       (.I0(data0[25]),
        .I1(s00_axi_aresetn),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[25]_LDC_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h2222222222222A22)) 
    \cnt_clk_reg[25]_LDC_i_2 
       (.I0(s00_axi_aresetn),
        .I1(data0[25]),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[25]_LDC_i_2_n_0 ));
  FDPE \cnt_clk_reg[25]_P 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sel0[25]),
        .PRE(\cnt_clk_reg[25]_LDC_i_1_n_0 ),
        .Q(\cnt_clk_reg[25]_P_n_0 ));
  FDCE \cnt_clk_reg[26]_C 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(\cnt_clk_reg[26]_LDC_i_2_n_0 ),
        .D(sel0[26]),
        .Q(\cnt_clk_reg[26]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \cnt_clk_reg[26]_LDC 
       (.CLR(\cnt_clk_reg[26]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\cnt_clk_reg[26]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\cnt_clk_reg[26]_LDC_n_0 ));
  LUT6 #(
    .INIT(64'h8888888888888088)) 
    \cnt_clk_reg[26]_LDC_i_1 
       (.I0(data0[26]),
        .I1(s00_axi_aresetn),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[26]_LDC_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h2222222222222A22)) 
    \cnt_clk_reg[26]_LDC_i_2 
       (.I0(s00_axi_aresetn),
        .I1(data0[26]),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[26]_LDC_i_2_n_0 ));
  FDPE \cnt_clk_reg[26]_P 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sel0[26]),
        .PRE(\cnt_clk_reg[26]_LDC_i_1_n_0 ),
        .Q(\cnt_clk_reg[26]_P_n_0 ));
  FDCE \cnt_clk_reg[27]_C 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(\cnt_clk_reg[27]_LDC_i_2_n_0 ),
        .D(sel0[27]),
        .Q(\cnt_clk_reg[27]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \cnt_clk_reg[27]_LDC 
       (.CLR(\cnt_clk_reg[27]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\cnt_clk_reg[27]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\cnt_clk_reg[27]_LDC_n_0 ));
  LUT6 #(
    .INIT(64'h8888888888888088)) 
    \cnt_clk_reg[27]_LDC_i_1 
       (.I0(data0[27]),
        .I1(s00_axi_aresetn),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[27]_LDC_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h2222222222222A22)) 
    \cnt_clk_reg[27]_LDC_i_2 
       (.I0(s00_axi_aresetn),
        .I1(data0[27]),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[27]_LDC_i_2_n_0 ));
  CARRY4 \cnt_clk_reg[27]_LDC_i_3 
       (.CI(\cnt_clk_reg[23]_LDC_i_3_n_0 ),
        .CO({\cnt_clk_reg[27]_LDC_i_3_n_0 ,\cnt_clk_reg[27]_LDC_i_3_n_1 ,\cnt_clk_reg[27]_LDC_i_3_n_2 ,\cnt_clk_reg[27]_LDC_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[27:24]),
        .S({\cnt_clk_reg[27]_LDC_i_4_n_0 ,\cnt_clk_reg[27]_LDC_i_5_n_0 ,\cnt_clk_reg[27]_LDC_i_6_n_0 ,\cnt_clk_reg[27]_LDC_i_7_n_0 }));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk_reg[27]_LDC_i_4 
       (.I0(\cnt_clk_reg[27]_P_n_0 ),
        .I1(\cnt_clk_reg[27]_LDC_n_0 ),
        .I2(\cnt_clk_reg[27]_C_n_0 ),
        .O(\cnt_clk_reg[27]_LDC_i_4_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk_reg[27]_LDC_i_5 
       (.I0(\cnt_clk_reg[26]_P_n_0 ),
        .I1(\cnt_clk_reg[26]_LDC_n_0 ),
        .I2(\cnt_clk_reg[26]_C_n_0 ),
        .O(\cnt_clk_reg[27]_LDC_i_5_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk_reg[27]_LDC_i_6 
       (.I0(\cnt_clk_reg[25]_P_n_0 ),
        .I1(\cnt_clk_reg[25]_LDC_n_0 ),
        .I2(\cnt_clk_reg[25]_C_n_0 ),
        .O(\cnt_clk_reg[27]_LDC_i_6_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk_reg[27]_LDC_i_7 
       (.I0(\cnt_clk_reg[24]_P_n_0 ),
        .I1(\cnt_clk_reg[24]_LDC_n_0 ),
        .I2(\cnt_clk_reg[24]_C_n_0 ),
        .O(\cnt_clk_reg[27]_LDC_i_7_n_0 ));
  FDPE \cnt_clk_reg[27]_P 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sel0[27]),
        .PRE(\cnt_clk_reg[27]_LDC_i_1_n_0 ),
        .Q(\cnt_clk_reg[27]_P_n_0 ));
  FDCE \cnt_clk_reg[28]_C 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(\cnt_clk_reg[28]_LDC_i_2_n_0 ),
        .D(sel0[28]),
        .Q(\cnt_clk_reg[28]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \cnt_clk_reg[28]_LDC 
       (.CLR(\cnt_clk_reg[28]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\cnt_clk_reg[28]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\cnt_clk_reg[28]_LDC_n_0 ));
  LUT6 #(
    .INIT(64'h8888888888888088)) 
    \cnt_clk_reg[28]_LDC_i_1 
       (.I0(data0[28]),
        .I1(s00_axi_aresetn),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[28]_LDC_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h2222222222222A22)) 
    \cnt_clk_reg[28]_LDC_i_2 
       (.I0(s00_axi_aresetn),
        .I1(data0[28]),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[28]_LDC_i_2_n_0 ));
  FDPE \cnt_clk_reg[28]_P 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sel0[28]),
        .PRE(\cnt_clk_reg[28]_LDC_i_1_n_0 ),
        .Q(\cnt_clk_reg[28]_P_n_0 ));
  FDCE \cnt_clk_reg[29]_C 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(\cnt_clk_reg[29]_LDC_i_2_n_0 ),
        .D(sel0[29]),
        .Q(\cnt_clk_reg[29]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \cnt_clk_reg[29]_LDC 
       (.CLR(\cnt_clk_reg[29]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\cnt_clk_reg[29]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\cnt_clk_reg[29]_LDC_n_0 ));
  LUT6 #(
    .INIT(64'h8888888888888088)) 
    \cnt_clk_reg[29]_LDC_i_1 
       (.I0(data0[29]),
        .I1(s00_axi_aresetn),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[29]_LDC_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h2222222222222A22)) 
    \cnt_clk_reg[29]_LDC_i_2 
       (.I0(s00_axi_aresetn),
        .I1(data0[29]),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[29]_LDC_i_2_n_0 ));
  FDPE \cnt_clk_reg[29]_P 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sel0[29]),
        .PRE(\cnt_clk_reg[29]_LDC_i_1_n_0 ),
        .Q(\cnt_clk_reg[29]_P_n_0 ));
  FDCE \cnt_clk_reg[2]_C 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(\cnt_clk_reg[2]_LDC_i_2_n_0 ),
        .D(sel0[2]),
        .Q(\cnt_clk_reg[2]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \cnt_clk_reg[2]_LDC 
       (.CLR(\cnt_clk_reg[2]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\cnt_clk_reg[2]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\cnt_clk_reg[2]_LDC_n_0 ));
  LUT6 #(
    .INIT(64'h8888888888888088)) 
    \cnt_clk_reg[2]_LDC_i_1 
       (.I0(data0[2]),
        .I1(s00_axi_aresetn),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[2]_LDC_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h2222222222222A22)) 
    \cnt_clk_reg[2]_LDC_i_2 
       (.I0(s00_axi_aresetn),
        .I1(data0[2]),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[2]_LDC_i_2_n_0 ));
  FDPE \cnt_clk_reg[2]_P 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sel0[2]),
        .PRE(\cnt_clk_reg[2]_LDC_i_1_n_0 ),
        .Q(\cnt_clk_reg[2]_P_n_0 ));
  FDCE \cnt_clk_reg[30]_C 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(\cnt_clk_reg[30]_LDC_i_2_n_0 ),
        .D(sel0[30]),
        .Q(\cnt_clk_reg[30]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \cnt_clk_reg[30]_LDC 
       (.CLR(\cnt_clk_reg[30]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\cnt_clk_reg[30]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\cnt_clk_reg[30]_LDC_n_0 ));
  LUT6 #(
    .INIT(64'h8888888888888088)) 
    \cnt_clk_reg[30]_LDC_i_1 
       (.I0(data0[30]),
        .I1(s00_axi_aresetn),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[30]_LDC_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h2222222222222A22)) 
    \cnt_clk_reg[30]_LDC_i_2 
       (.I0(s00_axi_aresetn),
        .I1(data0[30]),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[30]_LDC_i_2_n_0 ));
  FDPE \cnt_clk_reg[30]_P 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sel0[30]),
        .PRE(\cnt_clk_reg[30]_LDC_i_1_n_0 ),
        .Q(\cnt_clk_reg[30]_P_n_0 ));
  FDCE \cnt_clk_reg[31]_C 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(\cnt_clk_reg[31]_LDC_i_2_n_0 ),
        .D(sel0[31]),
        .Q(\cnt_clk_reg[31]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \cnt_clk_reg[31]_LDC 
       (.CLR(\cnt_clk_reg[31]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\cnt_clk_reg[31]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\cnt_clk_reg[31]_LDC_n_0 ));
  LUT6 #(
    .INIT(64'h8888888888888088)) 
    \cnt_clk_reg[31]_LDC_i_1 
       (.I0(data0[31]),
        .I1(s00_axi_aresetn),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[31]_LDC_i_1_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk_reg[31]_LDC_i_10 
       (.I0(\cnt_clk_reg[28]_P_n_0 ),
        .I1(\cnt_clk_reg[28]_LDC_n_0 ),
        .I2(\cnt_clk_reg[28]_C_n_0 ),
        .O(\cnt_clk_reg[31]_LDC_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFEFEA)) 
    \cnt_clk_reg[31]_LDC_i_11 
       (.I0(sel0[23]),
        .I1(\cnt_clk_reg[22]_P_n_0 ),
        .I2(\cnt_clk_reg[22]_LDC_n_0 ),
        .I3(\cnt_clk_reg[22]_C_n_0 ),
        .I4(sel0[25]),
        .I5(sel0[24]),
        .O(\cnt_clk_reg[31]_LDC_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFEFEA)) 
    \cnt_clk_reg[31]_LDC_i_12 
       (.I0(sel0[27]),
        .I1(\cnt_clk_reg[26]_P_n_0 ),
        .I2(\cnt_clk_reg[26]_LDC_n_0 ),
        .I3(\cnt_clk_reg[26]_C_n_0 ),
        .I4(sel0[29]),
        .I5(sel0[28]),
        .O(\cnt_clk_reg[31]_LDC_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFEFEA)) 
    \cnt_clk_reg[31]_LDC_i_13 
       (.I0(sel0[15]),
        .I1(\cnt_clk_reg[14]_P_n_0 ),
        .I2(\cnt_clk_reg[14]_LDC_n_0 ),
        .I3(\cnt_clk_reg[14]_C_n_0 ),
        .I4(sel0[17]),
        .I5(sel0[16]),
        .O(\cnt_clk_reg[31]_LDC_i_13_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFEFEA)) 
    \cnt_clk_reg[31]_LDC_i_14 
       (.I0(sel0[19]),
        .I1(\cnt_clk_reg[18]_P_n_0 ),
        .I2(\cnt_clk_reg[18]_LDC_n_0 ),
        .I3(\cnt_clk_reg[18]_C_n_0 ),
        .I4(sel0[21]),
        .I5(sel0[20]),
        .O(\cnt_clk_reg[31]_LDC_i_14_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFEFEA)) 
    \cnt_clk_reg[31]_LDC_i_15 
       (.I0(sel0[11]),
        .I1(\cnt_clk_reg[10]_P_n_0 ),
        .I2(\cnt_clk_reg[10]_LDC_n_0 ),
        .I3(\cnt_clk_reg[10]_C_n_0 ),
        .I4(sel0[13]),
        .I5(sel0[12]),
        .O(\cnt_clk_reg[31]_LDC_i_15_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFD)) 
    \cnt_clk_reg[31]_LDC_i_16 
       (.I0(sel0[6]),
        .I1(sel0[30]),
        .I2(sel0[31]),
        .I3(sel0[9]),
        .I4(sel0[8]),
        .O(\cnt_clk_reg[31]_LDC_i_16_n_0 ));
  LUT6 #(
    .INIT(64'h2222222222222A22)) 
    \cnt_clk_reg[31]_LDC_i_2 
       (.I0(s00_axi_aresetn),
        .I1(data0[31]),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[31]_LDC_i_2_n_0 ));
  CARRY4 \cnt_clk_reg[31]_LDC_i_3 
       (.CI(\cnt_clk_reg[27]_LDC_i_3_n_0 ),
        .CO({\NLW_cnt_clk_reg[31]_LDC_i_3_CO_UNCONNECTED [3],\cnt_clk_reg[31]_LDC_i_3_n_1 ,\cnt_clk_reg[31]_LDC_i_3_n_2 ,\cnt_clk_reg[31]_LDC_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[31:28]),
        .S({\cnt_clk_reg[31]_LDC_i_7_n_0 ,\cnt_clk_reg[31]_LDC_i_8_n_0 ,\cnt_clk_reg[31]_LDC_i_9_n_0 ,\cnt_clk_reg[31]_LDC_i_10_n_0 }));
  LUT6 #(
    .INIT(64'h1D1D1DFFFFFF1DFF)) 
    \cnt_clk_reg[31]_LDC_i_4 
       (.I0(\cnt_clk_reg[5]_C_n_0 ),
        .I1(\cnt_clk_reg[5]_LDC_n_0 ),
        .I2(\cnt_clk_reg[5]_P_n_0 ),
        .I3(\cnt_clk_reg[7]_C_n_0 ),
        .I4(\cnt_clk_reg[7]_LDC_n_0 ),
        .I5(\cnt_clk_reg[7]_P_n_0 ),
        .O(\cnt_clk_reg[31]_LDC_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h757FFFFFFFFFFFFF)) 
    \cnt_clk_reg[31]_LDC_i_5 
       (.I0(sel0[2]),
        .I1(\cnt_clk_reg[1]_P_n_0 ),
        .I2(\cnt_clk_reg[1]_LDC_n_0 ),
        .I3(\cnt_clk_reg[1]_C_n_0 ),
        .I4(sel0[4]),
        .I5(sel0[3]),
        .O(\cnt_clk_reg[31]_LDC_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \cnt_clk_reg[31]_LDC_i_6 
       (.I0(\cnt_clk_reg[31]_LDC_i_11_n_0 ),
        .I1(\cnt_clk_reg[31]_LDC_i_12_n_0 ),
        .I2(\cnt_clk_reg[31]_LDC_i_13_n_0 ),
        .I3(\cnt_clk_reg[31]_LDC_i_14_n_0 ),
        .I4(\cnt_clk_reg[31]_LDC_i_15_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_16_n_0 ),
        .O(\cnt_clk_reg[31]_LDC_i_6_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk_reg[31]_LDC_i_7 
       (.I0(\cnt_clk_reg[31]_P_n_0 ),
        .I1(\cnt_clk_reg[31]_LDC_n_0 ),
        .I2(\cnt_clk_reg[31]_C_n_0 ),
        .O(\cnt_clk_reg[31]_LDC_i_7_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk_reg[31]_LDC_i_8 
       (.I0(\cnt_clk_reg[30]_P_n_0 ),
        .I1(\cnt_clk_reg[30]_LDC_n_0 ),
        .I2(\cnt_clk_reg[30]_C_n_0 ),
        .O(\cnt_clk_reg[31]_LDC_i_8_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk_reg[31]_LDC_i_9 
       (.I0(\cnt_clk_reg[29]_P_n_0 ),
        .I1(\cnt_clk_reg[29]_LDC_n_0 ),
        .I2(\cnt_clk_reg[29]_C_n_0 ),
        .O(\cnt_clk_reg[31]_LDC_i_9_n_0 ));
  FDPE \cnt_clk_reg[31]_P 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sel0[31]),
        .PRE(\cnt_clk_reg[31]_LDC_i_1_n_0 ),
        .Q(\cnt_clk_reg[31]_P_n_0 ));
  FDCE \cnt_clk_reg[3]_C 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(\cnt_clk_reg[3]_LDC_i_2_n_0 ),
        .D(sel0[3]),
        .Q(\cnt_clk_reg[3]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \cnt_clk_reg[3]_LDC 
       (.CLR(\cnt_clk_reg[3]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\cnt_clk_reg[3]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\cnt_clk_reg[3]_LDC_n_0 ));
  LUT6 #(
    .INIT(64'h8888888888888088)) 
    \cnt_clk_reg[3]_LDC_i_1 
       (.I0(data0[3]),
        .I1(s00_axi_aresetn),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[3]_LDC_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h2222222222222A22)) 
    \cnt_clk_reg[3]_LDC_i_2 
       (.I0(s00_axi_aresetn),
        .I1(data0[3]),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[3]_LDC_i_2_n_0 ));
  CARRY4 \cnt_clk_reg[3]_LDC_i_3 
       (.CI(1'b0),
        .CO({\cnt_clk_reg[3]_LDC_i_3_n_0 ,\cnt_clk_reg[3]_LDC_i_3_n_1 ,\cnt_clk_reg[3]_LDC_i_3_n_2 ,\cnt_clk_reg[3]_LDC_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,\cnt_clk_reg[3]_LDC_i_4_n_0 }),
        .O(data0[3:0]),
        .S({\cnt_clk_reg[3]_LDC_i_5_n_0 ,\cnt_clk_reg[3]_LDC_i_6_n_0 ,\cnt_clk_reg[3]_LDC_i_7_n_0 ,\cnt_clk_reg[3]_LDC_i_8_n_0 }));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk_reg[3]_LDC_i_4 
       (.I0(\cnt_clk_reg[0]_P_n_0 ),
        .I1(\cnt_clk_reg[0]_LDC_n_0 ),
        .I2(\cnt_clk_reg[0]_C_n_0 ),
        .O(\cnt_clk_reg[3]_LDC_i_4_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk_reg[3]_LDC_i_5 
       (.I0(\cnt_clk_reg[3]_P_n_0 ),
        .I1(\cnt_clk_reg[3]_LDC_n_0 ),
        .I2(\cnt_clk_reg[3]_C_n_0 ),
        .O(\cnt_clk_reg[3]_LDC_i_5_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk_reg[3]_LDC_i_6 
       (.I0(\cnt_clk_reg[2]_P_n_0 ),
        .I1(\cnt_clk_reg[2]_LDC_n_0 ),
        .I2(\cnt_clk_reg[2]_C_n_0 ),
        .O(\cnt_clk_reg[3]_LDC_i_6_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk_reg[3]_LDC_i_7 
       (.I0(\cnt_clk_reg[1]_P_n_0 ),
        .I1(\cnt_clk_reg[1]_LDC_n_0 ),
        .I2(\cnt_clk_reg[1]_C_n_0 ),
        .O(\cnt_clk_reg[3]_LDC_i_7_n_0 ));
  LUT3 #(
    .INIT(8'h1D)) 
    \cnt_clk_reg[3]_LDC_i_8 
       (.I0(\cnt_clk_reg[0]_C_n_0 ),
        .I1(\cnt_clk_reg[0]_LDC_n_0 ),
        .I2(\cnt_clk_reg[0]_P_n_0 ),
        .O(\cnt_clk_reg[3]_LDC_i_8_n_0 ));
  FDPE \cnt_clk_reg[3]_P 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sel0[3]),
        .PRE(\cnt_clk_reg[3]_LDC_i_1_n_0 ),
        .Q(\cnt_clk_reg[3]_P_n_0 ));
  FDCE \cnt_clk_reg[4]_C 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(\cnt_clk_reg[4]_LDC_i_2_n_0 ),
        .D(sel0[4]),
        .Q(\cnt_clk_reg[4]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \cnt_clk_reg[4]_LDC 
       (.CLR(\cnt_clk_reg[4]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\cnt_clk_reg[4]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\cnt_clk_reg[4]_LDC_n_0 ));
  LUT6 #(
    .INIT(64'h8888888888888088)) 
    \cnt_clk_reg[4]_LDC_i_1 
       (.I0(data0[4]),
        .I1(s00_axi_aresetn),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[4]_LDC_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h2222222222222A22)) 
    \cnt_clk_reg[4]_LDC_i_2 
       (.I0(s00_axi_aresetn),
        .I1(data0[4]),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[4]_LDC_i_2_n_0 ));
  FDPE \cnt_clk_reg[4]_P 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sel0[4]),
        .PRE(\cnt_clk_reg[4]_LDC_i_1_n_0 ),
        .Q(\cnt_clk_reg[4]_P_n_0 ));
  FDCE \cnt_clk_reg[5]_C 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(\cnt_clk_reg[5]_LDC_i_2_n_0 ),
        .D(sel0[5]),
        .Q(\cnt_clk_reg[5]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \cnt_clk_reg[5]_LDC 
       (.CLR(\cnt_clk_reg[5]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\cnt_clk_reg[5]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\cnt_clk_reg[5]_LDC_n_0 ));
  LUT6 #(
    .INIT(64'h8888888888888088)) 
    \cnt_clk_reg[5]_LDC_i_1 
       (.I0(data0[5]),
        .I1(s00_axi_aresetn),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[5]_LDC_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h2222222222222A22)) 
    \cnt_clk_reg[5]_LDC_i_2 
       (.I0(s00_axi_aresetn),
        .I1(data0[5]),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[5]_LDC_i_2_n_0 ));
  FDPE \cnt_clk_reg[5]_P 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sel0[5]),
        .PRE(\cnt_clk_reg[5]_LDC_i_1_n_0 ),
        .Q(\cnt_clk_reg[5]_P_n_0 ));
  FDCE \cnt_clk_reg[6]_C 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(\cnt_clk_reg[6]_LDC_i_2_n_0 ),
        .D(sel0[6]),
        .Q(\cnt_clk_reg[6]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \cnt_clk_reg[6]_LDC 
       (.CLR(\cnt_clk_reg[6]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\cnt_clk_reg[6]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\cnt_clk_reg[6]_LDC_n_0 ));
  LUT6 #(
    .INIT(64'h8888888888888088)) 
    \cnt_clk_reg[6]_LDC_i_1 
       (.I0(data0[6]),
        .I1(s00_axi_aresetn),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[6]_LDC_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h2222222222222A22)) 
    \cnt_clk_reg[6]_LDC_i_2 
       (.I0(s00_axi_aresetn),
        .I1(data0[6]),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[6]_LDC_i_2_n_0 ));
  FDPE \cnt_clk_reg[6]_P 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sel0[6]),
        .PRE(\cnt_clk_reg[6]_LDC_i_1_n_0 ),
        .Q(\cnt_clk_reg[6]_P_n_0 ));
  FDCE \cnt_clk_reg[7]_C 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(\cnt_clk_reg[7]_LDC_i_2_n_0 ),
        .D(sel0[7]),
        .Q(\cnt_clk_reg[7]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \cnt_clk_reg[7]_LDC 
       (.CLR(\cnt_clk_reg[7]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\cnt_clk_reg[7]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\cnt_clk_reg[7]_LDC_n_0 ));
  LUT6 #(
    .INIT(64'h8888888888888088)) 
    \cnt_clk_reg[7]_LDC_i_1 
       (.I0(data0[7]),
        .I1(s00_axi_aresetn),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[7]_LDC_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h2222222222222A22)) 
    \cnt_clk_reg[7]_LDC_i_2 
       (.I0(s00_axi_aresetn),
        .I1(data0[7]),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[7]_LDC_i_2_n_0 ));
  CARRY4 \cnt_clk_reg[7]_LDC_i_3 
       (.CI(\cnt_clk_reg[3]_LDC_i_3_n_0 ),
        .CO({\cnt_clk_reg[7]_LDC_i_3_n_0 ,\cnt_clk_reg[7]_LDC_i_3_n_1 ,\cnt_clk_reg[7]_LDC_i_3_n_2 ,\cnt_clk_reg[7]_LDC_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[7:4]),
        .S({\cnt_clk_reg[7]_LDC_i_4_n_0 ,\cnt_clk_reg[7]_LDC_i_5_n_0 ,\cnt_clk_reg[7]_LDC_i_6_n_0 ,\cnt_clk_reg[7]_LDC_i_7_n_0 }));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk_reg[7]_LDC_i_4 
       (.I0(\cnt_clk_reg[7]_P_n_0 ),
        .I1(\cnt_clk_reg[7]_LDC_n_0 ),
        .I2(\cnt_clk_reg[7]_C_n_0 ),
        .O(\cnt_clk_reg[7]_LDC_i_4_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk_reg[7]_LDC_i_5 
       (.I0(\cnt_clk_reg[6]_P_n_0 ),
        .I1(\cnt_clk_reg[6]_LDC_n_0 ),
        .I2(\cnt_clk_reg[6]_C_n_0 ),
        .O(\cnt_clk_reg[7]_LDC_i_5_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk_reg[7]_LDC_i_6 
       (.I0(\cnt_clk_reg[5]_P_n_0 ),
        .I1(\cnt_clk_reg[5]_LDC_n_0 ),
        .I2(\cnt_clk_reg[5]_C_n_0 ),
        .O(\cnt_clk_reg[7]_LDC_i_6_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_clk_reg[7]_LDC_i_7 
       (.I0(\cnt_clk_reg[4]_P_n_0 ),
        .I1(\cnt_clk_reg[4]_LDC_n_0 ),
        .I2(\cnt_clk_reg[4]_C_n_0 ),
        .O(\cnt_clk_reg[7]_LDC_i_7_n_0 ));
  FDPE \cnt_clk_reg[7]_P 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sel0[7]),
        .PRE(\cnt_clk_reg[7]_LDC_i_1_n_0 ),
        .Q(\cnt_clk_reg[7]_P_n_0 ));
  FDCE \cnt_clk_reg[8]_C 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(\cnt_clk_reg[8]_LDC_i_2_n_0 ),
        .D(sel0[8]),
        .Q(\cnt_clk_reg[8]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \cnt_clk_reg[8]_LDC 
       (.CLR(\cnt_clk_reg[8]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\cnt_clk_reg[8]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\cnt_clk_reg[8]_LDC_n_0 ));
  LUT6 #(
    .INIT(64'h8888888888888088)) 
    \cnt_clk_reg[8]_LDC_i_1 
       (.I0(data0[8]),
        .I1(s00_axi_aresetn),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[8]_LDC_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h2222222222222A22)) 
    \cnt_clk_reg[8]_LDC_i_2 
       (.I0(s00_axi_aresetn),
        .I1(data0[8]),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[8]_LDC_i_2_n_0 ));
  FDPE \cnt_clk_reg[8]_P 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sel0[8]),
        .PRE(\cnt_clk_reg[8]_LDC_i_1_n_0 ),
        .Q(\cnt_clk_reg[8]_P_n_0 ));
  FDCE \cnt_clk_reg[9]_C 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .CLR(\cnt_clk_reg[9]_LDC_i_2_n_0 ),
        .D(sel0[9]),
        .Q(\cnt_clk_reg[9]_C_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \cnt_clk_reg[9]_LDC 
       (.CLR(\cnt_clk_reg[9]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\cnt_clk_reg[9]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\cnt_clk_reg[9]_LDC_n_0 ));
  LUT6 #(
    .INIT(64'h8888888888888088)) 
    \cnt_clk_reg[9]_LDC_i_1 
       (.I0(data0[9]),
        .I1(s00_axi_aresetn),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[9]_LDC_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h2222222222222A22)) 
    \cnt_clk_reg[9]_LDC_i_2 
       (.I0(s00_axi_aresetn),
        .I1(data0[9]),
        .I2(\cnt_clk_reg[31]_LDC_i_4_n_0 ),
        .I3(sel0[0]),
        .I4(\cnt_clk_reg[31]_LDC_i_5_n_0 ),
        .I5(\cnt_clk_reg[31]_LDC_i_6_n_0 ),
        .O(\cnt_clk_reg[9]_LDC_i_2_n_0 ));
  FDPE \cnt_clk_reg[9]_P 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sel0[9]),
        .PRE(\cnt_clk_reg[9]_LDC_i_1_n_0 ),
        .Q(\cnt_clk_reg[9]_P_n_0 ));
  (* OPT_MODIFIED = "MLO" *) 
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0),
    .IS_G_INVERTED(1'b1)) 
    read_r_reg
       (.CLR(1'b0),
        .D(read_r_reg_i_1_n_0),
        .G(read_r_reg_i_3_n_0),
        .GE(1'b1),
        .Q(READ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT2 #(
    .INIT(4'h7)) 
    read_r_reg_i_1
       (.I0(\stateDNA_reg_reg[1]_P_n_0 ),
        .I1(\stateDNA_reg_reg[1]_LDC_n_0 ),
        .O(read_r_reg_i_1_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFE2E2E2FFE2)) 
    read_r_reg_i_10
       (.I0(\cnt_clk_reg[5]_C_n_0 ),
        .I1(\cnt_clk_reg[5]_LDC_n_0 ),
        .I2(\cnt_clk_reg[5]_P_n_0 ),
        .I3(\cnt_clk_reg[7]_C_n_0 ),
        .I4(\cnt_clk_reg[7]_LDC_n_0 ),
        .I5(\cnt_clk_reg[7]_P_n_0 ),
        .O(read_r_reg_i_10_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFE2E2E2FFE2)) 
    read_r_reg_i_11
       (.I0(\cnt_clk_reg[8]_C_n_0 ),
        .I1(\cnt_clk_reg[8]_LDC_n_0 ),
        .I2(\cnt_clk_reg[8]_P_n_0 ),
        .I3(\cnt_clk_reg[9]_C_n_0 ),
        .I4(\cnt_clk_reg[9]_LDC_n_0 ),
        .I5(\cnt_clk_reg[9]_P_n_0 ),
        .O(read_r_reg_i_11_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFE2E2E2FFE2)) 
    read_r_reg_i_12
       (.I0(\cnt_clk_reg[12]_C_n_0 ),
        .I1(\cnt_clk_reg[12]_LDC_n_0 ),
        .I2(\cnt_clk_reg[12]_P_n_0 ),
        .I3(\cnt_clk_reg[13]_C_n_0 ),
        .I4(\cnt_clk_reg[13]_LDC_n_0 ),
        .I5(\cnt_clk_reg[13]_P_n_0 ),
        .O(read_r_reg_i_12_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFE2E2E2FFE2)) 
    read_r_reg_i_13
       (.I0(\cnt_clk_reg[10]_C_n_0 ),
        .I1(\cnt_clk_reg[10]_LDC_n_0 ),
        .I2(\cnt_clk_reg[10]_P_n_0 ),
        .I3(\cnt_clk_reg[11]_C_n_0 ),
        .I4(\cnt_clk_reg[11]_LDC_n_0 ),
        .I5(\cnt_clk_reg[11]_P_n_0 ),
        .O(read_r_reg_i_13_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFE2E2E2FFE2)) 
    read_r_reg_i_14
       (.I0(\cnt_clk_reg[20]_C_n_0 ),
        .I1(\cnt_clk_reg[20]_LDC_n_0 ),
        .I2(\cnt_clk_reg[20]_P_n_0 ),
        .I3(\cnt_clk_reg[21]_C_n_0 ),
        .I4(\cnt_clk_reg[21]_LDC_n_0 ),
        .I5(\cnt_clk_reg[21]_P_n_0 ),
        .O(read_r_reg_i_14_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFE2E2E2FFE2)) 
    read_r_reg_i_15
       (.I0(\cnt_clk_reg[18]_C_n_0 ),
        .I1(\cnt_clk_reg[18]_LDC_n_0 ),
        .I2(\cnt_clk_reg[18]_P_n_0 ),
        .I3(\cnt_clk_reg[19]_C_n_0 ),
        .I4(\cnt_clk_reg[19]_LDC_n_0 ),
        .I5(\cnt_clk_reg[19]_P_n_0 ),
        .O(read_r_reg_i_15_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFE2E2E2FFE2)) 
    read_r_reg_i_16
       (.I0(\cnt_clk_reg[28]_C_n_0 ),
        .I1(\cnt_clk_reg[28]_LDC_n_0 ),
        .I2(\cnt_clk_reg[28]_P_n_0 ),
        .I3(\cnt_clk_reg[29]_C_n_0 ),
        .I4(\cnt_clk_reg[29]_LDC_n_0 ),
        .I5(\cnt_clk_reg[29]_P_n_0 ),
        .O(read_r_reg_i_16_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFE2E2E2FFE2)) 
    read_r_reg_i_17
       (.I0(\cnt_clk_reg[26]_C_n_0 ),
        .I1(\cnt_clk_reg[26]_LDC_n_0 ),
        .I2(\cnt_clk_reg[26]_P_n_0 ),
        .I3(\cnt_clk_reg[27]_C_n_0 ),
        .I4(\cnt_clk_reg[27]_LDC_n_0 ),
        .I5(\cnt_clk_reg[27]_P_n_0 ),
        .O(read_r_reg_i_17_n_0));
  LUT6 #(
    .INIT(64'h00000001FFFFFFFF)) 
    read_r_reg_i_3
       (.I0(stateDNA_reg[1]),
        .I1(read_r_reg_i_5_n_0),
        .I2(read_r_reg_i_6_n_0),
        .I3(read_r_reg_i_7_n_0),
        .I4(read_r_reg_i_8_n_0),
        .I5(stateDNA_reg[0]),
        .O(read_r_reg_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT2 #(
    .INIT(4'h8)) 
    read_r_reg_i_4
       (.I0(\stateDNA_reg_reg[1]_LDC_n_0 ),
        .I1(\stateDNA_reg_reg[1]_P_n_0 ),
        .O(stateDNA_reg[1]));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    read_r_reg_i_5
       (.I0(sel0[3]),
        .I1(sel0[4]),
        .I2(sel0[1]),
        .I3(sel0[2]),
        .I4(sel0[0]),
        .I5(read_r_reg_i_10_n_0),
        .O(read_r_reg_i_5_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFEFF)) 
    read_r_reg_i_6
       (.I0(read_r_reg_i_11_n_0),
        .I1(sel0[31]),
        .I2(sel0[30]),
        .I3(sel0[6]),
        .I4(read_r_reg_i_12_n_0),
        .I5(read_r_reg_i_13_n_0),
        .O(read_r_reg_i_6_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    read_r_reg_i_7
       (.I0(sel0[16]),
        .I1(sel0[17]),
        .I2(sel0[14]),
        .I3(sel0[15]),
        .I4(read_r_reg_i_14_n_0),
        .I5(read_r_reg_i_15_n_0),
        .O(read_r_reg_i_7_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    read_r_reg_i_8
       (.I0(sel0[24]),
        .I1(sel0[25]),
        .I2(sel0[22]),
        .I3(sel0[23]),
        .I4(read_r_reg_i_16_n_0),
        .I5(read_r_reg_i_17_n_0),
        .O(read_r_reg_i_8_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    read_r_reg_i_9
       (.I0(\stateDNA_reg_reg[0]_LDC_n_0 ),
        .I1(\stateDNA_reg_reg[0]_P_n_0 ),
        .O(stateDNA_reg[0]));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \stateDNA_reg_reg[0]_LDC 
       (.CLR(\stateDNA_reg_reg[0]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\stateDNA_reg_reg[0]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\stateDNA_reg_reg[0]_LDC_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \stateDNA_reg_reg[0]_LDC_i_1 
       (.I0(read_r_reg_i_3_n_0),
        .I1(s00_axi_aresetn),
        .O(\stateDNA_reg_reg[0]_LDC_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \stateDNA_reg_reg[0]_LDC_i_2 
       (.I0(s00_axi_aresetn),
        .I1(read_r_reg_i_3_n_0),
        .O(\stateDNA_reg_reg[0]_LDC_i_2_n_0 ));
  FDPE \stateDNA_reg_reg[0]_P 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(1'b0),
        .PRE(\stateDNA_reg_reg[0]_LDC_i_1_n_0 ),
        .Q(\stateDNA_reg_reg[0]_P_n_0 ));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \stateDNA_reg_reg[1]_LDC 
       (.CLR(\stateDNA_reg_reg[1]_LDC_i_2_n_0 ),
        .D(1'b1),
        .G(\stateDNA_reg_reg[1]_LDC_i_1_n_0 ),
        .GE(1'b1),
        .Q(\stateDNA_reg_reg[1]_LDC_n_0 ));
  LUT6 #(
    .INIT(64'h2A0000002A808080)) 
    \stateDNA_reg_reg[1]_LDC_i_1 
       (.I0(s00_axi_aresetn),
        .I1(\stateDNA_reg_reg[0]_P_n_0 ),
        .I2(\stateDNA_reg_reg[0]_LDC_n_0 ),
        .I3(\stateDNA_reg_reg[1]_LDC_n_0 ),
        .I4(\stateDNA_reg_reg[1]_P_n_0 ),
        .I5(\stateDNA_reg_reg[1]_LDC_i_3_n_0 ),
        .O(\stateDNA_reg_reg[1]_LDC_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hA8880AAA0AAA0AAA)) 
    \stateDNA_reg_reg[1]_LDC_i_2 
       (.I0(s00_axi_aresetn),
        .I1(\stateDNA_reg_reg[1]_LDC_i_3_n_0 ),
        .I2(\stateDNA_reg_reg[1]_P_n_0 ),
        .I3(\stateDNA_reg_reg[1]_LDC_n_0 ),
        .I4(\stateDNA_reg_reg[0]_LDC_n_0 ),
        .I5(\stateDNA_reg_reg[0]_P_n_0 ),
        .O(\stateDNA_reg_reg[1]_LDC_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \stateDNA_reg_reg[1]_LDC_i_3 
       (.I0(read_r_reg_i_5_n_0),
        .I1(\cnt_clk_reg[31]_LDC_i_16_n_0 ),
        .I2(\cnt_clk_reg[31]_LDC_i_15_n_0 ),
        .I3(\cnt_clk_reg[31]_LDC_i_14_n_0 ),
        .I4(\cnt_clk_reg[31]_LDC_i_13_n_0 ),
        .I5(read_r_reg_i_8_n_0),
        .O(\stateDNA_reg_reg[1]_LDC_i_3_n_0 ));
  FDPE \stateDNA_reg_reg[1]_P 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(1'b0),
        .PRE(\stateDNA_reg_reg[1]_LDC_i_1_n_0 ),
        .Q(\stateDNA_reg_reg[1]_P_n_0 ));
endmodule

(* CHECK_LICENSE_TYPE = "design_2_ID_FPGA_0_0,ID_FPGA_v1_0,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "ID_FPGA_v1_0,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (s00_axi_aclk,
    s00_axi_aresetn,
    ready,
    outID);
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 s00_axi_aclk CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s00_axi_aclk, ASSOCIATED_RESET s00_axi_aresetn, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_2_s00_axi_aclk_0, INSERT_VIP 0" *) input s00_axi_aclk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 s00_axi_aresetn RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s00_axi_aresetn, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input s00_axi_aresetn;
  output ready;
  output [63:0]outID;

  wire s00_axi_aclk;
  wire s00_axi_aresetn;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ID_FPGA_v1_0 inst
       (.s00_axi_aclk(s00_axi_aclk),
        .s00_axi_aresetn(s00_axi_aresetn));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
