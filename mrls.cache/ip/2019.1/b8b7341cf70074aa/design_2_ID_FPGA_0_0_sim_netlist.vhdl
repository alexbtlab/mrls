-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
-- Date        : Wed Mar 30 15:48:38 2022
-- Host        : alexbtlab-System-Product-Name running 64-bit Ubuntu 20.04.3 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_2_ID_FPGA_0_0_sim_netlist.vhdl
-- Design      : design_2_ID_FPGA_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z020clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ID_FPGA_v1_0 is
  port (
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ID_FPGA_v1_0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ID_FPGA_v1_0 is
  signal READ : STD_LOGIC;
  signal \cnt_clk_reg[0]_C_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[0]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[0]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[0]_LDC_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[0]_P_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[10]_C_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[10]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[10]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[10]_LDC_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[10]_P_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[11]_C_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[11]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[11]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[11]_LDC_i_3_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[11]_LDC_i_3_n_1\ : STD_LOGIC;
  signal \cnt_clk_reg[11]_LDC_i_3_n_2\ : STD_LOGIC;
  signal \cnt_clk_reg[11]_LDC_i_3_n_3\ : STD_LOGIC;
  signal \cnt_clk_reg[11]_LDC_i_4_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[11]_LDC_i_5_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[11]_LDC_i_6_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[11]_LDC_i_7_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[11]_LDC_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[11]_P_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[12]_C_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[12]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[12]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[12]_LDC_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[12]_P_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[13]_C_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[13]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[13]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[13]_LDC_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[13]_P_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[14]_C_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[14]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[14]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[14]_LDC_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[14]_P_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[15]_C_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[15]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[15]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[15]_LDC_i_3_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[15]_LDC_i_3_n_1\ : STD_LOGIC;
  signal \cnt_clk_reg[15]_LDC_i_3_n_2\ : STD_LOGIC;
  signal \cnt_clk_reg[15]_LDC_i_3_n_3\ : STD_LOGIC;
  signal \cnt_clk_reg[15]_LDC_i_4_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[15]_LDC_i_5_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[15]_LDC_i_6_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[15]_LDC_i_7_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[15]_LDC_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[15]_P_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[16]_C_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[16]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[16]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[16]_LDC_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[16]_P_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[17]_C_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[17]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[17]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[17]_LDC_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[17]_P_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[18]_C_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[18]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[18]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[18]_LDC_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[18]_P_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[19]_C_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[19]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[19]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[19]_LDC_i_3_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[19]_LDC_i_3_n_1\ : STD_LOGIC;
  signal \cnt_clk_reg[19]_LDC_i_3_n_2\ : STD_LOGIC;
  signal \cnt_clk_reg[19]_LDC_i_3_n_3\ : STD_LOGIC;
  signal \cnt_clk_reg[19]_LDC_i_4_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[19]_LDC_i_5_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[19]_LDC_i_6_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[19]_LDC_i_7_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[19]_LDC_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[19]_P_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[1]_C_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[1]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[1]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[1]_LDC_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[1]_P_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[20]_C_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[20]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[20]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[20]_LDC_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[20]_P_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[21]_C_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[21]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[21]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[21]_LDC_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[21]_P_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[22]_C_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[22]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[22]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[22]_LDC_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[22]_P_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[23]_C_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[23]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[23]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[23]_LDC_i_3_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[23]_LDC_i_3_n_1\ : STD_LOGIC;
  signal \cnt_clk_reg[23]_LDC_i_3_n_2\ : STD_LOGIC;
  signal \cnt_clk_reg[23]_LDC_i_3_n_3\ : STD_LOGIC;
  signal \cnt_clk_reg[23]_LDC_i_4_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[23]_LDC_i_5_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[23]_LDC_i_6_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[23]_LDC_i_7_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[23]_LDC_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[23]_P_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[24]_C_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[24]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[24]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[24]_LDC_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[24]_P_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[25]_C_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[25]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[25]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[25]_LDC_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[25]_P_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[26]_C_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[26]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[26]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[26]_LDC_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[26]_P_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[27]_C_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[27]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[27]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[27]_LDC_i_3_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[27]_LDC_i_3_n_1\ : STD_LOGIC;
  signal \cnt_clk_reg[27]_LDC_i_3_n_2\ : STD_LOGIC;
  signal \cnt_clk_reg[27]_LDC_i_3_n_3\ : STD_LOGIC;
  signal \cnt_clk_reg[27]_LDC_i_4_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[27]_LDC_i_5_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[27]_LDC_i_6_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[27]_LDC_i_7_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[27]_LDC_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[27]_P_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[28]_C_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[28]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[28]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[28]_LDC_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[28]_P_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[29]_C_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[29]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[29]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[29]_LDC_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[29]_P_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[2]_C_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[2]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[2]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[2]_LDC_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[2]_P_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[30]_C_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[30]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[30]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[30]_LDC_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[30]_P_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[31]_C_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[31]_LDC_i_10_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[31]_LDC_i_11_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[31]_LDC_i_12_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[31]_LDC_i_13_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[31]_LDC_i_14_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[31]_LDC_i_15_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[31]_LDC_i_16_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[31]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[31]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[31]_LDC_i_3_n_1\ : STD_LOGIC;
  signal \cnt_clk_reg[31]_LDC_i_3_n_2\ : STD_LOGIC;
  signal \cnt_clk_reg[31]_LDC_i_3_n_3\ : STD_LOGIC;
  signal \cnt_clk_reg[31]_LDC_i_4_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[31]_LDC_i_5_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[31]_LDC_i_6_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[31]_LDC_i_7_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[31]_LDC_i_8_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[31]_LDC_i_9_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[31]_LDC_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[31]_P_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[3]_C_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[3]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[3]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[3]_LDC_i_3_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[3]_LDC_i_3_n_1\ : STD_LOGIC;
  signal \cnt_clk_reg[3]_LDC_i_3_n_2\ : STD_LOGIC;
  signal \cnt_clk_reg[3]_LDC_i_3_n_3\ : STD_LOGIC;
  signal \cnt_clk_reg[3]_LDC_i_4_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[3]_LDC_i_5_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[3]_LDC_i_6_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[3]_LDC_i_7_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[3]_LDC_i_8_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[3]_LDC_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[3]_P_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[4]_C_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[4]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[4]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[4]_LDC_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[4]_P_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[5]_C_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[5]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[5]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[5]_LDC_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[5]_P_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[6]_C_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[6]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[6]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[6]_LDC_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[6]_P_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[7]_C_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[7]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[7]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[7]_LDC_i_3_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[7]_LDC_i_3_n_1\ : STD_LOGIC;
  signal \cnt_clk_reg[7]_LDC_i_3_n_2\ : STD_LOGIC;
  signal \cnt_clk_reg[7]_LDC_i_3_n_3\ : STD_LOGIC;
  signal \cnt_clk_reg[7]_LDC_i_4_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[7]_LDC_i_5_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[7]_LDC_i_6_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[7]_LDC_i_7_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[7]_LDC_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[7]_P_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[8]_C_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[8]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[8]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[8]_LDC_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[8]_P_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[9]_C_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[9]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[9]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[9]_LDC_n_0\ : STD_LOGIC;
  signal \cnt_clk_reg[9]_P_n_0\ : STD_LOGIC;
  signal data0 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal read_r_reg_i_10_n_0 : STD_LOGIC;
  signal read_r_reg_i_11_n_0 : STD_LOGIC;
  signal read_r_reg_i_12_n_0 : STD_LOGIC;
  signal read_r_reg_i_13_n_0 : STD_LOGIC;
  signal read_r_reg_i_14_n_0 : STD_LOGIC;
  signal read_r_reg_i_15_n_0 : STD_LOGIC;
  signal read_r_reg_i_16_n_0 : STD_LOGIC;
  signal read_r_reg_i_17_n_0 : STD_LOGIC;
  signal read_r_reg_i_1_n_0 : STD_LOGIC;
  signal read_r_reg_i_3_n_0 : STD_LOGIC;
  signal read_r_reg_i_5_n_0 : STD_LOGIC;
  signal read_r_reg_i_6_n_0 : STD_LOGIC;
  signal read_r_reg_i_7_n_0 : STD_LOGIC;
  signal read_r_reg_i_8_n_0 : STD_LOGIC;
  signal sel0 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal stateDNA_reg : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \stateDNA_reg_reg[0]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \stateDNA_reg_reg[0]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \stateDNA_reg_reg[0]_LDC_n_0\ : STD_LOGIC;
  signal \stateDNA_reg_reg[0]_P_n_0\ : STD_LOGIC;
  signal \stateDNA_reg_reg[1]_LDC_i_1_n_0\ : STD_LOGIC;
  signal \stateDNA_reg_reg[1]_LDC_i_2_n_0\ : STD_LOGIC;
  signal \stateDNA_reg_reg[1]_LDC_i_3_n_0\ : STD_LOGIC;
  signal \stateDNA_reg_reg[1]_LDC_n_0\ : STD_LOGIC;
  signal \stateDNA_reg_reg[1]_P_n_0\ : STD_LOGIC;
  signal NLW_DNA_PORT_inst_DOUT_UNCONNECTED : STD_LOGIC;
  signal \NLW_cnt_clk_reg[31]_LDC_i_3_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute BOX_TYPE : string;
  attribute BOX_TYPE of DNA_PORT_inst : label is "PRIMITIVE";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of \cnt_clk_reg[0]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \cnt_clk_reg[10]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \cnt_clk_reg[11]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \cnt_clk_reg[12]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \cnt_clk_reg[13]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \cnt_clk_reg[14]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \cnt_clk_reg[15]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \cnt_clk_reg[16]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \cnt_clk_reg[17]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \cnt_clk_reg[18]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \cnt_clk_reg[19]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \cnt_clk_reg[1]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \cnt_clk_reg[20]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \cnt_clk_reg[21]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \cnt_clk_reg[22]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \cnt_clk_reg[23]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \cnt_clk_reg[24]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \cnt_clk_reg[25]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \cnt_clk_reg[26]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \cnt_clk_reg[27]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \cnt_clk_reg[28]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \cnt_clk_reg[29]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \cnt_clk_reg[2]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \cnt_clk_reg[30]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \cnt_clk_reg[31]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \cnt_clk_reg[3]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \cnt_clk_reg[4]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \cnt_clk_reg[5]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \cnt_clk_reg[6]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \cnt_clk_reg[7]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \cnt_clk_reg[8]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \cnt_clk_reg[9]_LDC\ : label is "LDC";
  attribute OPT_MODIFIED : string;
  attribute OPT_MODIFIED of read_r_reg : label is "MLO";
  attribute XILINX_LEGACY_PRIM of read_r_reg : label is "LD";
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of read_r_reg_i_1 : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of read_r_reg_i_4 : label is "soft_lutpair0";
  attribute XILINX_LEGACY_PRIM of \stateDNA_reg_reg[0]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \stateDNA_reg_reg[1]_LDC\ : label is "LDC";
begin
DNA_PORT_inst: unisim.vcomponents.DNA_PORT
    generic map(
      SIM_DNA_VALUE => X"123456789012345"
    )
        port map (
      CLK => s00_axi_aclk,
      DIN => '0',
      DOUT => NLW_DNA_PORT_inst_DOUT_UNCONNECTED,
      READ => READ,
      SHIFT => '0'
    );
\cnt_clk[0]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[0]_P_n_0\,
      I1 => \cnt_clk_reg[0]_LDC_n_0\,
      I2 => \cnt_clk_reg[0]_C_n_0\,
      O => sel0(0)
    );
\cnt_clk[10]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[10]_P_n_0\,
      I1 => \cnt_clk_reg[10]_LDC_n_0\,
      I2 => \cnt_clk_reg[10]_C_n_0\,
      O => sel0(10)
    );
\cnt_clk[11]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[11]_P_n_0\,
      I1 => \cnt_clk_reg[11]_LDC_n_0\,
      I2 => \cnt_clk_reg[11]_C_n_0\,
      O => sel0(11)
    );
\cnt_clk[12]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[12]_P_n_0\,
      I1 => \cnt_clk_reg[12]_LDC_n_0\,
      I2 => \cnt_clk_reg[12]_C_n_0\,
      O => sel0(12)
    );
\cnt_clk[13]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[13]_P_n_0\,
      I1 => \cnt_clk_reg[13]_LDC_n_0\,
      I2 => \cnt_clk_reg[13]_C_n_0\,
      O => sel0(13)
    );
\cnt_clk[14]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[14]_P_n_0\,
      I1 => \cnt_clk_reg[14]_LDC_n_0\,
      I2 => \cnt_clk_reg[14]_C_n_0\,
      O => sel0(14)
    );
\cnt_clk[15]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[15]_P_n_0\,
      I1 => \cnt_clk_reg[15]_LDC_n_0\,
      I2 => \cnt_clk_reg[15]_C_n_0\,
      O => sel0(15)
    );
\cnt_clk[16]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[16]_P_n_0\,
      I1 => \cnt_clk_reg[16]_LDC_n_0\,
      I2 => \cnt_clk_reg[16]_C_n_0\,
      O => sel0(16)
    );
\cnt_clk[17]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[17]_P_n_0\,
      I1 => \cnt_clk_reg[17]_LDC_n_0\,
      I2 => \cnt_clk_reg[17]_C_n_0\,
      O => sel0(17)
    );
\cnt_clk[18]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[18]_P_n_0\,
      I1 => \cnt_clk_reg[18]_LDC_n_0\,
      I2 => \cnt_clk_reg[18]_C_n_0\,
      O => sel0(18)
    );
\cnt_clk[19]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[19]_P_n_0\,
      I1 => \cnt_clk_reg[19]_LDC_n_0\,
      I2 => \cnt_clk_reg[19]_C_n_0\,
      O => sel0(19)
    );
\cnt_clk[1]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[1]_P_n_0\,
      I1 => \cnt_clk_reg[1]_LDC_n_0\,
      I2 => \cnt_clk_reg[1]_C_n_0\,
      O => sel0(1)
    );
\cnt_clk[20]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[20]_P_n_0\,
      I1 => \cnt_clk_reg[20]_LDC_n_0\,
      I2 => \cnt_clk_reg[20]_C_n_0\,
      O => sel0(20)
    );
\cnt_clk[21]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[21]_P_n_0\,
      I1 => \cnt_clk_reg[21]_LDC_n_0\,
      I2 => \cnt_clk_reg[21]_C_n_0\,
      O => sel0(21)
    );
\cnt_clk[22]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[22]_P_n_0\,
      I1 => \cnt_clk_reg[22]_LDC_n_0\,
      I2 => \cnt_clk_reg[22]_C_n_0\,
      O => sel0(22)
    );
\cnt_clk[23]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[23]_P_n_0\,
      I1 => \cnt_clk_reg[23]_LDC_n_0\,
      I2 => \cnt_clk_reg[23]_C_n_0\,
      O => sel0(23)
    );
\cnt_clk[24]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[24]_P_n_0\,
      I1 => \cnt_clk_reg[24]_LDC_n_0\,
      I2 => \cnt_clk_reg[24]_C_n_0\,
      O => sel0(24)
    );
\cnt_clk[25]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[25]_P_n_0\,
      I1 => \cnt_clk_reg[25]_LDC_n_0\,
      I2 => \cnt_clk_reg[25]_C_n_0\,
      O => sel0(25)
    );
\cnt_clk[26]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[26]_P_n_0\,
      I1 => \cnt_clk_reg[26]_LDC_n_0\,
      I2 => \cnt_clk_reg[26]_C_n_0\,
      O => sel0(26)
    );
\cnt_clk[27]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[27]_P_n_0\,
      I1 => \cnt_clk_reg[27]_LDC_n_0\,
      I2 => \cnt_clk_reg[27]_C_n_0\,
      O => sel0(27)
    );
\cnt_clk[28]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[28]_P_n_0\,
      I1 => \cnt_clk_reg[28]_LDC_n_0\,
      I2 => \cnt_clk_reg[28]_C_n_0\,
      O => sel0(28)
    );
\cnt_clk[29]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[29]_P_n_0\,
      I1 => \cnt_clk_reg[29]_LDC_n_0\,
      I2 => \cnt_clk_reg[29]_C_n_0\,
      O => sel0(29)
    );
\cnt_clk[2]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[2]_P_n_0\,
      I1 => \cnt_clk_reg[2]_LDC_n_0\,
      I2 => \cnt_clk_reg[2]_C_n_0\,
      O => sel0(2)
    );
\cnt_clk[30]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[30]_P_n_0\,
      I1 => \cnt_clk_reg[30]_LDC_n_0\,
      I2 => \cnt_clk_reg[30]_C_n_0\,
      O => sel0(30)
    );
\cnt_clk[31]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[31]_P_n_0\,
      I1 => \cnt_clk_reg[31]_LDC_n_0\,
      I2 => \cnt_clk_reg[31]_C_n_0\,
      O => sel0(31)
    );
\cnt_clk[3]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[3]_P_n_0\,
      I1 => \cnt_clk_reg[3]_LDC_n_0\,
      I2 => \cnt_clk_reg[3]_C_n_0\,
      O => sel0(3)
    );
\cnt_clk[4]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[4]_P_n_0\,
      I1 => \cnt_clk_reg[4]_LDC_n_0\,
      I2 => \cnt_clk_reg[4]_C_n_0\,
      O => sel0(4)
    );
\cnt_clk[5]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[5]_P_n_0\,
      I1 => \cnt_clk_reg[5]_LDC_n_0\,
      I2 => \cnt_clk_reg[5]_C_n_0\,
      O => sel0(5)
    );
\cnt_clk[6]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[6]_P_n_0\,
      I1 => \cnt_clk_reg[6]_LDC_n_0\,
      I2 => \cnt_clk_reg[6]_C_n_0\,
      O => sel0(6)
    );
\cnt_clk[7]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[7]_P_n_0\,
      I1 => \cnt_clk_reg[7]_LDC_n_0\,
      I2 => \cnt_clk_reg[7]_C_n_0\,
      O => sel0(7)
    );
\cnt_clk[8]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[8]_P_n_0\,
      I1 => \cnt_clk_reg[8]_LDC_n_0\,
      I2 => \cnt_clk_reg[8]_C_n_0\,
      O => sel0(8)
    );
\cnt_clk[9]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[9]_P_n_0\,
      I1 => \cnt_clk_reg[9]_LDC_n_0\,
      I2 => \cnt_clk_reg[9]_C_n_0\,
      O => sel0(9)
    );
\cnt_clk_reg[0]_C\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => \cnt_clk_reg[0]_LDC_i_2_n_0\,
      D => sel0(0),
      Q => \cnt_clk_reg[0]_C_n_0\
    );
\cnt_clk_reg[0]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \cnt_clk_reg[0]_LDC_i_2_n_0\,
      D => '1',
      G => \cnt_clk_reg[0]_LDC_i_1_n_0\,
      GE => '1',
      Q => \cnt_clk_reg[0]_LDC_n_0\
    );
\cnt_clk_reg[0]_LDC_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8888888888888088"
    )
        port map (
      I0 => data0(0),
      I1 => s00_axi_aresetn,
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[0]_LDC_i_1_n_0\
    );
\cnt_clk_reg[0]_LDC_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2222222222222A22"
    )
        port map (
      I0 => s00_axi_aresetn,
      I1 => data0(0),
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[0]_LDC_i_2_n_0\
    );
\cnt_clk_reg[0]_P\: unisim.vcomponents.FDPE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sel0(0),
      PRE => \cnt_clk_reg[0]_LDC_i_1_n_0\,
      Q => \cnt_clk_reg[0]_P_n_0\
    );
\cnt_clk_reg[10]_C\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => \cnt_clk_reg[10]_LDC_i_2_n_0\,
      D => sel0(10),
      Q => \cnt_clk_reg[10]_C_n_0\
    );
\cnt_clk_reg[10]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \cnt_clk_reg[10]_LDC_i_2_n_0\,
      D => '1',
      G => \cnt_clk_reg[10]_LDC_i_1_n_0\,
      GE => '1',
      Q => \cnt_clk_reg[10]_LDC_n_0\
    );
\cnt_clk_reg[10]_LDC_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8888888888888088"
    )
        port map (
      I0 => data0(10),
      I1 => s00_axi_aresetn,
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[10]_LDC_i_1_n_0\
    );
\cnt_clk_reg[10]_LDC_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2222222222222A22"
    )
        port map (
      I0 => s00_axi_aresetn,
      I1 => data0(10),
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[10]_LDC_i_2_n_0\
    );
\cnt_clk_reg[10]_P\: unisim.vcomponents.FDPE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sel0(10),
      PRE => \cnt_clk_reg[10]_LDC_i_1_n_0\,
      Q => \cnt_clk_reg[10]_P_n_0\
    );
\cnt_clk_reg[11]_C\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => \cnt_clk_reg[11]_LDC_i_2_n_0\,
      D => sel0(11),
      Q => \cnt_clk_reg[11]_C_n_0\
    );
\cnt_clk_reg[11]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \cnt_clk_reg[11]_LDC_i_2_n_0\,
      D => '1',
      G => \cnt_clk_reg[11]_LDC_i_1_n_0\,
      GE => '1',
      Q => \cnt_clk_reg[11]_LDC_n_0\
    );
\cnt_clk_reg[11]_LDC_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8888888888888088"
    )
        port map (
      I0 => data0(11),
      I1 => s00_axi_aresetn,
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[11]_LDC_i_1_n_0\
    );
\cnt_clk_reg[11]_LDC_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2222222222222A22"
    )
        port map (
      I0 => s00_axi_aresetn,
      I1 => data0(11),
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[11]_LDC_i_2_n_0\
    );
\cnt_clk_reg[11]_LDC_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_clk_reg[7]_LDC_i_3_n_0\,
      CO(3) => \cnt_clk_reg[11]_LDC_i_3_n_0\,
      CO(2) => \cnt_clk_reg[11]_LDC_i_3_n_1\,
      CO(1) => \cnt_clk_reg[11]_LDC_i_3_n_2\,
      CO(0) => \cnt_clk_reg[11]_LDC_i_3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(11 downto 8),
      S(3) => \cnt_clk_reg[11]_LDC_i_4_n_0\,
      S(2) => \cnt_clk_reg[11]_LDC_i_5_n_0\,
      S(1) => \cnt_clk_reg[11]_LDC_i_6_n_0\,
      S(0) => \cnt_clk_reg[11]_LDC_i_7_n_0\
    );
\cnt_clk_reg[11]_LDC_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[11]_P_n_0\,
      I1 => \cnt_clk_reg[11]_LDC_n_0\,
      I2 => \cnt_clk_reg[11]_C_n_0\,
      O => \cnt_clk_reg[11]_LDC_i_4_n_0\
    );
\cnt_clk_reg[11]_LDC_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[10]_P_n_0\,
      I1 => \cnt_clk_reg[10]_LDC_n_0\,
      I2 => \cnt_clk_reg[10]_C_n_0\,
      O => \cnt_clk_reg[11]_LDC_i_5_n_0\
    );
\cnt_clk_reg[11]_LDC_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[9]_P_n_0\,
      I1 => \cnt_clk_reg[9]_LDC_n_0\,
      I2 => \cnt_clk_reg[9]_C_n_0\,
      O => \cnt_clk_reg[11]_LDC_i_6_n_0\
    );
\cnt_clk_reg[11]_LDC_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[8]_P_n_0\,
      I1 => \cnt_clk_reg[8]_LDC_n_0\,
      I2 => \cnt_clk_reg[8]_C_n_0\,
      O => \cnt_clk_reg[11]_LDC_i_7_n_0\
    );
\cnt_clk_reg[11]_P\: unisim.vcomponents.FDPE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sel0(11),
      PRE => \cnt_clk_reg[11]_LDC_i_1_n_0\,
      Q => \cnt_clk_reg[11]_P_n_0\
    );
\cnt_clk_reg[12]_C\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => \cnt_clk_reg[12]_LDC_i_2_n_0\,
      D => sel0(12),
      Q => \cnt_clk_reg[12]_C_n_0\
    );
\cnt_clk_reg[12]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \cnt_clk_reg[12]_LDC_i_2_n_0\,
      D => '1',
      G => \cnt_clk_reg[12]_LDC_i_1_n_0\,
      GE => '1',
      Q => \cnt_clk_reg[12]_LDC_n_0\
    );
\cnt_clk_reg[12]_LDC_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8888888888888088"
    )
        port map (
      I0 => data0(12),
      I1 => s00_axi_aresetn,
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[12]_LDC_i_1_n_0\
    );
\cnt_clk_reg[12]_LDC_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2222222222222A22"
    )
        port map (
      I0 => s00_axi_aresetn,
      I1 => data0(12),
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[12]_LDC_i_2_n_0\
    );
\cnt_clk_reg[12]_P\: unisim.vcomponents.FDPE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sel0(12),
      PRE => \cnt_clk_reg[12]_LDC_i_1_n_0\,
      Q => \cnt_clk_reg[12]_P_n_0\
    );
\cnt_clk_reg[13]_C\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => \cnt_clk_reg[13]_LDC_i_2_n_0\,
      D => sel0(13),
      Q => \cnt_clk_reg[13]_C_n_0\
    );
\cnt_clk_reg[13]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \cnt_clk_reg[13]_LDC_i_2_n_0\,
      D => '1',
      G => \cnt_clk_reg[13]_LDC_i_1_n_0\,
      GE => '1',
      Q => \cnt_clk_reg[13]_LDC_n_0\
    );
\cnt_clk_reg[13]_LDC_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8888888888888088"
    )
        port map (
      I0 => data0(13),
      I1 => s00_axi_aresetn,
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[13]_LDC_i_1_n_0\
    );
\cnt_clk_reg[13]_LDC_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2222222222222A22"
    )
        port map (
      I0 => s00_axi_aresetn,
      I1 => data0(13),
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[13]_LDC_i_2_n_0\
    );
\cnt_clk_reg[13]_P\: unisim.vcomponents.FDPE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sel0(13),
      PRE => \cnt_clk_reg[13]_LDC_i_1_n_0\,
      Q => \cnt_clk_reg[13]_P_n_0\
    );
\cnt_clk_reg[14]_C\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => \cnt_clk_reg[14]_LDC_i_2_n_0\,
      D => sel0(14),
      Q => \cnt_clk_reg[14]_C_n_0\
    );
\cnt_clk_reg[14]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \cnt_clk_reg[14]_LDC_i_2_n_0\,
      D => '1',
      G => \cnt_clk_reg[14]_LDC_i_1_n_0\,
      GE => '1',
      Q => \cnt_clk_reg[14]_LDC_n_0\
    );
\cnt_clk_reg[14]_LDC_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8888888888888088"
    )
        port map (
      I0 => data0(14),
      I1 => s00_axi_aresetn,
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[14]_LDC_i_1_n_0\
    );
\cnt_clk_reg[14]_LDC_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2222222222222A22"
    )
        port map (
      I0 => s00_axi_aresetn,
      I1 => data0(14),
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[14]_LDC_i_2_n_0\
    );
\cnt_clk_reg[14]_P\: unisim.vcomponents.FDPE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sel0(14),
      PRE => \cnt_clk_reg[14]_LDC_i_1_n_0\,
      Q => \cnt_clk_reg[14]_P_n_0\
    );
\cnt_clk_reg[15]_C\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => \cnt_clk_reg[15]_LDC_i_2_n_0\,
      D => sel0(15),
      Q => \cnt_clk_reg[15]_C_n_0\
    );
\cnt_clk_reg[15]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \cnt_clk_reg[15]_LDC_i_2_n_0\,
      D => '1',
      G => \cnt_clk_reg[15]_LDC_i_1_n_0\,
      GE => '1',
      Q => \cnt_clk_reg[15]_LDC_n_0\
    );
\cnt_clk_reg[15]_LDC_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8888888888888088"
    )
        port map (
      I0 => data0(15),
      I1 => s00_axi_aresetn,
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[15]_LDC_i_1_n_0\
    );
\cnt_clk_reg[15]_LDC_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2222222222222A22"
    )
        port map (
      I0 => s00_axi_aresetn,
      I1 => data0(15),
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[15]_LDC_i_2_n_0\
    );
\cnt_clk_reg[15]_LDC_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_clk_reg[11]_LDC_i_3_n_0\,
      CO(3) => \cnt_clk_reg[15]_LDC_i_3_n_0\,
      CO(2) => \cnt_clk_reg[15]_LDC_i_3_n_1\,
      CO(1) => \cnt_clk_reg[15]_LDC_i_3_n_2\,
      CO(0) => \cnt_clk_reg[15]_LDC_i_3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(15 downto 12),
      S(3) => \cnt_clk_reg[15]_LDC_i_4_n_0\,
      S(2) => \cnt_clk_reg[15]_LDC_i_5_n_0\,
      S(1) => \cnt_clk_reg[15]_LDC_i_6_n_0\,
      S(0) => \cnt_clk_reg[15]_LDC_i_7_n_0\
    );
\cnt_clk_reg[15]_LDC_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[15]_P_n_0\,
      I1 => \cnt_clk_reg[15]_LDC_n_0\,
      I2 => \cnt_clk_reg[15]_C_n_0\,
      O => \cnt_clk_reg[15]_LDC_i_4_n_0\
    );
\cnt_clk_reg[15]_LDC_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[14]_P_n_0\,
      I1 => \cnt_clk_reg[14]_LDC_n_0\,
      I2 => \cnt_clk_reg[14]_C_n_0\,
      O => \cnt_clk_reg[15]_LDC_i_5_n_0\
    );
\cnt_clk_reg[15]_LDC_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[13]_P_n_0\,
      I1 => \cnt_clk_reg[13]_LDC_n_0\,
      I2 => \cnt_clk_reg[13]_C_n_0\,
      O => \cnt_clk_reg[15]_LDC_i_6_n_0\
    );
\cnt_clk_reg[15]_LDC_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[12]_P_n_0\,
      I1 => \cnt_clk_reg[12]_LDC_n_0\,
      I2 => \cnt_clk_reg[12]_C_n_0\,
      O => \cnt_clk_reg[15]_LDC_i_7_n_0\
    );
\cnt_clk_reg[15]_P\: unisim.vcomponents.FDPE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sel0(15),
      PRE => \cnt_clk_reg[15]_LDC_i_1_n_0\,
      Q => \cnt_clk_reg[15]_P_n_0\
    );
\cnt_clk_reg[16]_C\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => \cnt_clk_reg[16]_LDC_i_2_n_0\,
      D => sel0(16),
      Q => \cnt_clk_reg[16]_C_n_0\
    );
\cnt_clk_reg[16]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \cnt_clk_reg[16]_LDC_i_2_n_0\,
      D => '1',
      G => \cnt_clk_reg[16]_LDC_i_1_n_0\,
      GE => '1',
      Q => \cnt_clk_reg[16]_LDC_n_0\
    );
\cnt_clk_reg[16]_LDC_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8888888888888088"
    )
        port map (
      I0 => data0(16),
      I1 => s00_axi_aresetn,
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[16]_LDC_i_1_n_0\
    );
\cnt_clk_reg[16]_LDC_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2222222222222A22"
    )
        port map (
      I0 => s00_axi_aresetn,
      I1 => data0(16),
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[16]_LDC_i_2_n_0\
    );
\cnt_clk_reg[16]_P\: unisim.vcomponents.FDPE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sel0(16),
      PRE => \cnt_clk_reg[16]_LDC_i_1_n_0\,
      Q => \cnt_clk_reg[16]_P_n_0\
    );
\cnt_clk_reg[17]_C\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => \cnt_clk_reg[17]_LDC_i_2_n_0\,
      D => sel0(17),
      Q => \cnt_clk_reg[17]_C_n_0\
    );
\cnt_clk_reg[17]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \cnt_clk_reg[17]_LDC_i_2_n_0\,
      D => '1',
      G => \cnt_clk_reg[17]_LDC_i_1_n_0\,
      GE => '1',
      Q => \cnt_clk_reg[17]_LDC_n_0\
    );
\cnt_clk_reg[17]_LDC_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8888888888888088"
    )
        port map (
      I0 => data0(17),
      I1 => s00_axi_aresetn,
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[17]_LDC_i_1_n_0\
    );
\cnt_clk_reg[17]_LDC_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2222222222222A22"
    )
        port map (
      I0 => s00_axi_aresetn,
      I1 => data0(17),
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[17]_LDC_i_2_n_0\
    );
\cnt_clk_reg[17]_P\: unisim.vcomponents.FDPE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sel0(17),
      PRE => \cnt_clk_reg[17]_LDC_i_1_n_0\,
      Q => \cnt_clk_reg[17]_P_n_0\
    );
\cnt_clk_reg[18]_C\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => \cnt_clk_reg[18]_LDC_i_2_n_0\,
      D => sel0(18),
      Q => \cnt_clk_reg[18]_C_n_0\
    );
\cnt_clk_reg[18]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \cnt_clk_reg[18]_LDC_i_2_n_0\,
      D => '1',
      G => \cnt_clk_reg[18]_LDC_i_1_n_0\,
      GE => '1',
      Q => \cnt_clk_reg[18]_LDC_n_0\
    );
\cnt_clk_reg[18]_LDC_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8888888888888088"
    )
        port map (
      I0 => data0(18),
      I1 => s00_axi_aresetn,
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[18]_LDC_i_1_n_0\
    );
\cnt_clk_reg[18]_LDC_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2222222222222A22"
    )
        port map (
      I0 => s00_axi_aresetn,
      I1 => data0(18),
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[18]_LDC_i_2_n_0\
    );
\cnt_clk_reg[18]_P\: unisim.vcomponents.FDPE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sel0(18),
      PRE => \cnt_clk_reg[18]_LDC_i_1_n_0\,
      Q => \cnt_clk_reg[18]_P_n_0\
    );
\cnt_clk_reg[19]_C\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => \cnt_clk_reg[19]_LDC_i_2_n_0\,
      D => sel0(19),
      Q => \cnt_clk_reg[19]_C_n_0\
    );
\cnt_clk_reg[19]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \cnt_clk_reg[19]_LDC_i_2_n_0\,
      D => '1',
      G => \cnt_clk_reg[19]_LDC_i_1_n_0\,
      GE => '1',
      Q => \cnt_clk_reg[19]_LDC_n_0\
    );
\cnt_clk_reg[19]_LDC_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8888888888888088"
    )
        port map (
      I0 => data0(19),
      I1 => s00_axi_aresetn,
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[19]_LDC_i_1_n_0\
    );
\cnt_clk_reg[19]_LDC_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2222222222222A22"
    )
        port map (
      I0 => s00_axi_aresetn,
      I1 => data0(19),
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[19]_LDC_i_2_n_0\
    );
\cnt_clk_reg[19]_LDC_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_clk_reg[15]_LDC_i_3_n_0\,
      CO(3) => \cnt_clk_reg[19]_LDC_i_3_n_0\,
      CO(2) => \cnt_clk_reg[19]_LDC_i_3_n_1\,
      CO(1) => \cnt_clk_reg[19]_LDC_i_3_n_2\,
      CO(0) => \cnt_clk_reg[19]_LDC_i_3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(19 downto 16),
      S(3) => \cnt_clk_reg[19]_LDC_i_4_n_0\,
      S(2) => \cnt_clk_reg[19]_LDC_i_5_n_0\,
      S(1) => \cnt_clk_reg[19]_LDC_i_6_n_0\,
      S(0) => \cnt_clk_reg[19]_LDC_i_7_n_0\
    );
\cnt_clk_reg[19]_LDC_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[19]_P_n_0\,
      I1 => \cnt_clk_reg[19]_LDC_n_0\,
      I2 => \cnt_clk_reg[19]_C_n_0\,
      O => \cnt_clk_reg[19]_LDC_i_4_n_0\
    );
\cnt_clk_reg[19]_LDC_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[18]_P_n_0\,
      I1 => \cnt_clk_reg[18]_LDC_n_0\,
      I2 => \cnt_clk_reg[18]_C_n_0\,
      O => \cnt_clk_reg[19]_LDC_i_5_n_0\
    );
\cnt_clk_reg[19]_LDC_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[17]_P_n_0\,
      I1 => \cnt_clk_reg[17]_LDC_n_0\,
      I2 => \cnt_clk_reg[17]_C_n_0\,
      O => \cnt_clk_reg[19]_LDC_i_6_n_0\
    );
\cnt_clk_reg[19]_LDC_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[16]_P_n_0\,
      I1 => \cnt_clk_reg[16]_LDC_n_0\,
      I2 => \cnt_clk_reg[16]_C_n_0\,
      O => \cnt_clk_reg[19]_LDC_i_7_n_0\
    );
\cnt_clk_reg[19]_P\: unisim.vcomponents.FDPE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sel0(19),
      PRE => \cnt_clk_reg[19]_LDC_i_1_n_0\,
      Q => \cnt_clk_reg[19]_P_n_0\
    );
\cnt_clk_reg[1]_C\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => \cnt_clk_reg[1]_LDC_i_2_n_0\,
      D => sel0(1),
      Q => \cnt_clk_reg[1]_C_n_0\
    );
\cnt_clk_reg[1]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \cnt_clk_reg[1]_LDC_i_2_n_0\,
      D => '1',
      G => \cnt_clk_reg[1]_LDC_i_1_n_0\,
      GE => '1',
      Q => \cnt_clk_reg[1]_LDC_n_0\
    );
\cnt_clk_reg[1]_LDC_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8888888888888088"
    )
        port map (
      I0 => data0(1),
      I1 => s00_axi_aresetn,
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[1]_LDC_i_1_n_0\
    );
\cnt_clk_reg[1]_LDC_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2222222222222A22"
    )
        port map (
      I0 => s00_axi_aresetn,
      I1 => data0(1),
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[1]_LDC_i_2_n_0\
    );
\cnt_clk_reg[1]_P\: unisim.vcomponents.FDPE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sel0(1),
      PRE => \cnt_clk_reg[1]_LDC_i_1_n_0\,
      Q => \cnt_clk_reg[1]_P_n_0\
    );
\cnt_clk_reg[20]_C\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => \cnt_clk_reg[20]_LDC_i_2_n_0\,
      D => sel0(20),
      Q => \cnt_clk_reg[20]_C_n_0\
    );
\cnt_clk_reg[20]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \cnt_clk_reg[20]_LDC_i_2_n_0\,
      D => '1',
      G => \cnt_clk_reg[20]_LDC_i_1_n_0\,
      GE => '1',
      Q => \cnt_clk_reg[20]_LDC_n_0\
    );
\cnt_clk_reg[20]_LDC_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8888888888888088"
    )
        port map (
      I0 => data0(20),
      I1 => s00_axi_aresetn,
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[20]_LDC_i_1_n_0\
    );
\cnt_clk_reg[20]_LDC_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2222222222222A22"
    )
        port map (
      I0 => s00_axi_aresetn,
      I1 => data0(20),
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[20]_LDC_i_2_n_0\
    );
\cnt_clk_reg[20]_P\: unisim.vcomponents.FDPE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sel0(20),
      PRE => \cnt_clk_reg[20]_LDC_i_1_n_0\,
      Q => \cnt_clk_reg[20]_P_n_0\
    );
\cnt_clk_reg[21]_C\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => \cnt_clk_reg[21]_LDC_i_2_n_0\,
      D => sel0(21),
      Q => \cnt_clk_reg[21]_C_n_0\
    );
\cnt_clk_reg[21]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \cnt_clk_reg[21]_LDC_i_2_n_0\,
      D => '1',
      G => \cnt_clk_reg[21]_LDC_i_1_n_0\,
      GE => '1',
      Q => \cnt_clk_reg[21]_LDC_n_0\
    );
\cnt_clk_reg[21]_LDC_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8888888888888088"
    )
        port map (
      I0 => data0(21),
      I1 => s00_axi_aresetn,
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[21]_LDC_i_1_n_0\
    );
\cnt_clk_reg[21]_LDC_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2222222222222A22"
    )
        port map (
      I0 => s00_axi_aresetn,
      I1 => data0(21),
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[21]_LDC_i_2_n_0\
    );
\cnt_clk_reg[21]_P\: unisim.vcomponents.FDPE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sel0(21),
      PRE => \cnt_clk_reg[21]_LDC_i_1_n_0\,
      Q => \cnt_clk_reg[21]_P_n_0\
    );
\cnt_clk_reg[22]_C\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => \cnt_clk_reg[22]_LDC_i_2_n_0\,
      D => sel0(22),
      Q => \cnt_clk_reg[22]_C_n_0\
    );
\cnt_clk_reg[22]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \cnt_clk_reg[22]_LDC_i_2_n_0\,
      D => '1',
      G => \cnt_clk_reg[22]_LDC_i_1_n_0\,
      GE => '1',
      Q => \cnt_clk_reg[22]_LDC_n_0\
    );
\cnt_clk_reg[22]_LDC_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8888888888888088"
    )
        port map (
      I0 => data0(22),
      I1 => s00_axi_aresetn,
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[22]_LDC_i_1_n_0\
    );
\cnt_clk_reg[22]_LDC_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2222222222222A22"
    )
        port map (
      I0 => s00_axi_aresetn,
      I1 => data0(22),
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[22]_LDC_i_2_n_0\
    );
\cnt_clk_reg[22]_P\: unisim.vcomponents.FDPE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sel0(22),
      PRE => \cnt_clk_reg[22]_LDC_i_1_n_0\,
      Q => \cnt_clk_reg[22]_P_n_0\
    );
\cnt_clk_reg[23]_C\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => \cnt_clk_reg[23]_LDC_i_2_n_0\,
      D => sel0(23),
      Q => \cnt_clk_reg[23]_C_n_0\
    );
\cnt_clk_reg[23]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \cnt_clk_reg[23]_LDC_i_2_n_0\,
      D => '1',
      G => \cnt_clk_reg[23]_LDC_i_1_n_0\,
      GE => '1',
      Q => \cnt_clk_reg[23]_LDC_n_0\
    );
\cnt_clk_reg[23]_LDC_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8888888888888088"
    )
        port map (
      I0 => data0(23),
      I1 => s00_axi_aresetn,
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[23]_LDC_i_1_n_0\
    );
\cnt_clk_reg[23]_LDC_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2222222222222A22"
    )
        port map (
      I0 => s00_axi_aresetn,
      I1 => data0(23),
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[23]_LDC_i_2_n_0\
    );
\cnt_clk_reg[23]_LDC_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_clk_reg[19]_LDC_i_3_n_0\,
      CO(3) => \cnt_clk_reg[23]_LDC_i_3_n_0\,
      CO(2) => \cnt_clk_reg[23]_LDC_i_3_n_1\,
      CO(1) => \cnt_clk_reg[23]_LDC_i_3_n_2\,
      CO(0) => \cnt_clk_reg[23]_LDC_i_3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(23 downto 20),
      S(3) => \cnt_clk_reg[23]_LDC_i_4_n_0\,
      S(2) => \cnt_clk_reg[23]_LDC_i_5_n_0\,
      S(1) => \cnt_clk_reg[23]_LDC_i_6_n_0\,
      S(0) => \cnt_clk_reg[23]_LDC_i_7_n_0\
    );
\cnt_clk_reg[23]_LDC_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[23]_P_n_0\,
      I1 => \cnt_clk_reg[23]_LDC_n_0\,
      I2 => \cnt_clk_reg[23]_C_n_0\,
      O => \cnt_clk_reg[23]_LDC_i_4_n_0\
    );
\cnt_clk_reg[23]_LDC_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[22]_P_n_0\,
      I1 => \cnt_clk_reg[22]_LDC_n_0\,
      I2 => \cnt_clk_reg[22]_C_n_0\,
      O => \cnt_clk_reg[23]_LDC_i_5_n_0\
    );
\cnt_clk_reg[23]_LDC_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[21]_P_n_0\,
      I1 => \cnt_clk_reg[21]_LDC_n_0\,
      I2 => \cnt_clk_reg[21]_C_n_0\,
      O => \cnt_clk_reg[23]_LDC_i_6_n_0\
    );
\cnt_clk_reg[23]_LDC_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[20]_P_n_0\,
      I1 => \cnt_clk_reg[20]_LDC_n_0\,
      I2 => \cnt_clk_reg[20]_C_n_0\,
      O => \cnt_clk_reg[23]_LDC_i_7_n_0\
    );
\cnt_clk_reg[23]_P\: unisim.vcomponents.FDPE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sel0(23),
      PRE => \cnt_clk_reg[23]_LDC_i_1_n_0\,
      Q => \cnt_clk_reg[23]_P_n_0\
    );
\cnt_clk_reg[24]_C\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => \cnt_clk_reg[24]_LDC_i_2_n_0\,
      D => sel0(24),
      Q => \cnt_clk_reg[24]_C_n_0\
    );
\cnt_clk_reg[24]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \cnt_clk_reg[24]_LDC_i_2_n_0\,
      D => '1',
      G => \cnt_clk_reg[24]_LDC_i_1_n_0\,
      GE => '1',
      Q => \cnt_clk_reg[24]_LDC_n_0\
    );
\cnt_clk_reg[24]_LDC_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8888888888888088"
    )
        port map (
      I0 => data0(24),
      I1 => s00_axi_aresetn,
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[24]_LDC_i_1_n_0\
    );
\cnt_clk_reg[24]_LDC_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2222222222222A22"
    )
        port map (
      I0 => s00_axi_aresetn,
      I1 => data0(24),
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[24]_LDC_i_2_n_0\
    );
\cnt_clk_reg[24]_P\: unisim.vcomponents.FDPE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sel0(24),
      PRE => \cnt_clk_reg[24]_LDC_i_1_n_0\,
      Q => \cnt_clk_reg[24]_P_n_0\
    );
\cnt_clk_reg[25]_C\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => \cnt_clk_reg[25]_LDC_i_2_n_0\,
      D => sel0(25),
      Q => \cnt_clk_reg[25]_C_n_0\
    );
\cnt_clk_reg[25]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \cnt_clk_reg[25]_LDC_i_2_n_0\,
      D => '1',
      G => \cnt_clk_reg[25]_LDC_i_1_n_0\,
      GE => '1',
      Q => \cnt_clk_reg[25]_LDC_n_0\
    );
\cnt_clk_reg[25]_LDC_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8888888888888088"
    )
        port map (
      I0 => data0(25),
      I1 => s00_axi_aresetn,
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[25]_LDC_i_1_n_0\
    );
\cnt_clk_reg[25]_LDC_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2222222222222A22"
    )
        port map (
      I0 => s00_axi_aresetn,
      I1 => data0(25),
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[25]_LDC_i_2_n_0\
    );
\cnt_clk_reg[25]_P\: unisim.vcomponents.FDPE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sel0(25),
      PRE => \cnt_clk_reg[25]_LDC_i_1_n_0\,
      Q => \cnt_clk_reg[25]_P_n_0\
    );
\cnt_clk_reg[26]_C\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => \cnt_clk_reg[26]_LDC_i_2_n_0\,
      D => sel0(26),
      Q => \cnt_clk_reg[26]_C_n_0\
    );
\cnt_clk_reg[26]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \cnt_clk_reg[26]_LDC_i_2_n_0\,
      D => '1',
      G => \cnt_clk_reg[26]_LDC_i_1_n_0\,
      GE => '1',
      Q => \cnt_clk_reg[26]_LDC_n_0\
    );
\cnt_clk_reg[26]_LDC_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8888888888888088"
    )
        port map (
      I0 => data0(26),
      I1 => s00_axi_aresetn,
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[26]_LDC_i_1_n_0\
    );
\cnt_clk_reg[26]_LDC_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2222222222222A22"
    )
        port map (
      I0 => s00_axi_aresetn,
      I1 => data0(26),
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[26]_LDC_i_2_n_0\
    );
\cnt_clk_reg[26]_P\: unisim.vcomponents.FDPE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sel0(26),
      PRE => \cnt_clk_reg[26]_LDC_i_1_n_0\,
      Q => \cnt_clk_reg[26]_P_n_0\
    );
\cnt_clk_reg[27]_C\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => \cnt_clk_reg[27]_LDC_i_2_n_0\,
      D => sel0(27),
      Q => \cnt_clk_reg[27]_C_n_0\
    );
\cnt_clk_reg[27]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \cnt_clk_reg[27]_LDC_i_2_n_0\,
      D => '1',
      G => \cnt_clk_reg[27]_LDC_i_1_n_0\,
      GE => '1',
      Q => \cnt_clk_reg[27]_LDC_n_0\
    );
\cnt_clk_reg[27]_LDC_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8888888888888088"
    )
        port map (
      I0 => data0(27),
      I1 => s00_axi_aresetn,
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[27]_LDC_i_1_n_0\
    );
\cnt_clk_reg[27]_LDC_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2222222222222A22"
    )
        port map (
      I0 => s00_axi_aresetn,
      I1 => data0(27),
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[27]_LDC_i_2_n_0\
    );
\cnt_clk_reg[27]_LDC_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_clk_reg[23]_LDC_i_3_n_0\,
      CO(3) => \cnt_clk_reg[27]_LDC_i_3_n_0\,
      CO(2) => \cnt_clk_reg[27]_LDC_i_3_n_1\,
      CO(1) => \cnt_clk_reg[27]_LDC_i_3_n_2\,
      CO(0) => \cnt_clk_reg[27]_LDC_i_3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(27 downto 24),
      S(3) => \cnt_clk_reg[27]_LDC_i_4_n_0\,
      S(2) => \cnt_clk_reg[27]_LDC_i_5_n_0\,
      S(1) => \cnt_clk_reg[27]_LDC_i_6_n_0\,
      S(0) => \cnt_clk_reg[27]_LDC_i_7_n_0\
    );
\cnt_clk_reg[27]_LDC_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[27]_P_n_0\,
      I1 => \cnt_clk_reg[27]_LDC_n_0\,
      I2 => \cnt_clk_reg[27]_C_n_0\,
      O => \cnt_clk_reg[27]_LDC_i_4_n_0\
    );
\cnt_clk_reg[27]_LDC_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[26]_P_n_0\,
      I1 => \cnt_clk_reg[26]_LDC_n_0\,
      I2 => \cnt_clk_reg[26]_C_n_0\,
      O => \cnt_clk_reg[27]_LDC_i_5_n_0\
    );
\cnt_clk_reg[27]_LDC_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[25]_P_n_0\,
      I1 => \cnt_clk_reg[25]_LDC_n_0\,
      I2 => \cnt_clk_reg[25]_C_n_0\,
      O => \cnt_clk_reg[27]_LDC_i_6_n_0\
    );
\cnt_clk_reg[27]_LDC_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[24]_P_n_0\,
      I1 => \cnt_clk_reg[24]_LDC_n_0\,
      I2 => \cnt_clk_reg[24]_C_n_0\,
      O => \cnt_clk_reg[27]_LDC_i_7_n_0\
    );
\cnt_clk_reg[27]_P\: unisim.vcomponents.FDPE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sel0(27),
      PRE => \cnt_clk_reg[27]_LDC_i_1_n_0\,
      Q => \cnt_clk_reg[27]_P_n_0\
    );
\cnt_clk_reg[28]_C\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => \cnt_clk_reg[28]_LDC_i_2_n_0\,
      D => sel0(28),
      Q => \cnt_clk_reg[28]_C_n_0\
    );
\cnt_clk_reg[28]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \cnt_clk_reg[28]_LDC_i_2_n_0\,
      D => '1',
      G => \cnt_clk_reg[28]_LDC_i_1_n_0\,
      GE => '1',
      Q => \cnt_clk_reg[28]_LDC_n_0\
    );
\cnt_clk_reg[28]_LDC_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8888888888888088"
    )
        port map (
      I0 => data0(28),
      I1 => s00_axi_aresetn,
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[28]_LDC_i_1_n_0\
    );
\cnt_clk_reg[28]_LDC_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2222222222222A22"
    )
        port map (
      I0 => s00_axi_aresetn,
      I1 => data0(28),
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[28]_LDC_i_2_n_0\
    );
\cnt_clk_reg[28]_P\: unisim.vcomponents.FDPE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sel0(28),
      PRE => \cnt_clk_reg[28]_LDC_i_1_n_0\,
      Q => \cnt_clk_reg[28]_P_n_0\
    );
\cnt_clk_reg[29]_C\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => \cnt_clk_reg[29]_LDC_i_2_n_0\,
      D => sel0(29),
      Q => \cnt_clk_reg[29]_C_n_0\
    );
\cnt_clk_reg[29]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \cnt_clk_reg[29]_LDC_i_2_n_0\,
      D => '1',
      G => \cnt_clk_reg[29]_LDC_i_1_n_0\,
      GE => '1',
      Q => \cnt_clk_reg[29]_LDC_n_0\
    );
\cnt_clk_reg[29]_LDC_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8888888888888088"
    )
        port map (
      I0 => data0(29),
      I1 => s00_axi_aresetn,
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[29]_LDC_i_1_n_0\
    );
\cnt_clk_reg[29]_LDC_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2222222222222A22"
    )
        port map (
      I0 => s00_axi_aresetn,
      I1 => data0(29),
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[29]_LDC_i_2_n_0\
    );
\cnt_clk_reg[29]_P\: unisim.vcomponents.FDPE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sel0(29),
      PRE => \cnt_clk_reg[29]_LDC_i_1_n_0\,
      Q => \cnt_clk_reg[29]_P_n_0\
    );
\cnt_clk_reg[2]_C\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => \cnt_clk_reg[2]_LDC_i_2_n_0\,
      D => sel0(2),
      Q => \cnt_clk_reg[2]_C_n_0\
    );
\cnt_clk_reg[2]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \cnt_clk_reg[2]_LDC_i_2_n_0\,
      D => '1',
      G => \cnt_clk_reg[2]_LDC_i_1_n_0\,
      GE => '1',
      Q => \cnt_clk_reg[2]_LDC_n_0\
    );
\cnt_clk_reg[2]_LDC_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8888888888888088"
    )
        port map (
      I0 => data0(2),
      I1 => s00_axi_aresetn,
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[2]_LDC_i_1_n_0\
    );
\cnt_clk_reg[2]_LDC_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2222222222222A22"
    )
        port map (
      I0 => s00_axi_aresetn,
      I1 => data0(2),
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[2]_LDC_i_2_n_0\
    );
\cnt_clk_reg[2]_P\: unisim.vcomponents.FDPE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sel0(2),
      PRE => \cnt_clk_reg[2]_LDC_i_1_n_0\,
      Q => \cnt_clk_reg[2]_P_n_0\
    );
\cnt_clk_reg[30]_C\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => \cnt_clk_reg[30]_LDC_i_2_n_0\,
      D => sel0(30),
      Q => \cnt_clk_reg[30]_C_n_0\
    );
\cnt_clk_reg[30]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \cnt_clk_reg[30]_LDC_i_2_n_0\,
      D => '1',
      G => \cnt_clk_reg[30]_LDC_i_1_n_0\,
      GE => '1',
      Q => \cnt_clk_reg[30]_LDC_n_0\
    );
\cnt_clk_reg[30]_LDC_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8888888888888088"
    )
        port map (
      I0 => data0(30),
      I1 => s00_axi_aresetn,
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[30]_LDC_i_1_n_0\
    );
\cnt_clk_reg[30]_LDC_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2222222222222A22"
    )
        port map (
      I0 => s00_axi_aresetn,
      I1 => data0(30),
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[30]_LDC_i_2_n_0\
    );
\cnt_clk_reg[30]_P\: unisim.vcomponents.FDPE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sel0(30),
      PRE => \cnt_clk_reg[30]_LDC_i_1_n_0\,
      Q => \cnt_clk_reg[30]_P_n_0\
    );
\cnt_clk_reg[31]_C\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => \cnt_clk_reg[31]_LDC_i_2_n_0\,
      D => sel0(31),
      Q => \cnt_clk_reg[31]_C_n_0\
    );
\cnt_clk_reg[31]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \cnt_clk_reg[31]_LDC_i_2_n_0\,
      D => '1',
      G => \cnt_clk_reg[31]_LDC_i_1_n_0\,
      GE => '1',
      Q => \cnt_clk_reg[31]_LDC_n_0\
    );
\cnt_clk_reg[31]_LDC_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8888888888888088"
    )
        port map (
      I0 => data0(31),
      I1 => s00_axi_aresetn,
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[31]_LDC_i_1_n_0\
    );
\cnt_clk_reg[31]_LDC_i_10\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[28]_P_n_0\,
      I1 => \cnt_clk_reg[28]_LDC_n_0\,
      I2 => \cnt_clk_reg[28]_C_n_0\,
      O => \cnt_clk_reg[31]_LDC_i_10_n_0\
    );
\cnt_clk_reg[31]_LDC_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFEFEA"
    )
        port map (
      I0 => sel0(23),
      I1 => \cnt_clk_reg[22]_P_n_0\,
      I2 => \cnt_clk_reg[22]_LDC_n_0\,
      I3 => \cnt_clk_reg[22]_C_n_0\,
      I4 => sel0(25),
      I5 => sel0(24),
      O => \cnt_clk_reg[31]_LDC_i_11_n_0\
    );
\cnt_clk_reg[31]_LDC_i_12\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFEFEA"
    )
        port map (
      I0 => sel0(27),
      I1 => \cnt_clk_reg[26]_P_n_0\,
      I2 => \cnt_clk_reg[26]_LDC_n_0\,
      I3 => \cnt_clk_reg[26]_C_n_0\,
      I4 => sel0(29),
      I5 => sel0(28),
      O => \cnt_clk_reg[31]_LDC_i_12_n_0\
    );
\cnt_clk_reg[31]_LDC_i_13\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFEFEA"
    )
        port map (
      I0 => sel0(15),
      I1 => \cnt_clk_reg[14]_P_n_0\,
      I2 => \cnt_clk_reg[14]_LDC_n_0\,
      I3 => \cnt_clk_reg[14]_C_n_0\,
      I4 => sel0(17),
      I5 => sel0(16),
      O => \cnt_clk_reg[31]_LDC_i_13_n_0\
    );
\cnt_clk_reg[31]_LDC_i_14\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFEFEA"
    )
        port map (
      I0 => sel0(19),
      I1 => \cnt_clk_reg[18]_P_n_0\,
      I2 => \cnt_clk_reg[18]_LDC_n_0\,
      I3 => \cnt_clk_reg[18]_C_n_0\,
      I4 => sel0(21),
      I5 => sel0(20),
      O => \cnt_clk_reg[31]_LDC_i_14_n_0\
    );
\cnt_clk_reg[31]_LDC_i_15\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFEFEA"
    )
        port map (
      I0 => sel0(11),
      I1 => \cnt_clk_reg[10]_P_n_0\,
      I2 => \cnt_clk_reg[10]_LDC_n_0\,
      I3 => \cnt_clk_reg[10]_C_n_0\,
      I4 => sel0(13),
      I5 => sel0(12),
      O => \cnt_clk_reg[31]_LDC_i_15_n_0\
    );
\cnt_clk_reg[31]_LDC_i_16\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFD"
    )
        port map (
      I0 => sel0(6),
      I1 => sel0(30),
      I2 => sel0(31),
      I3 => sel0(9),
      I4 => sel0(8),
      O => \cnt_clk_reg[31]_LDC_i_16_n_0\
    );
\cnt_clk_reg[31]_LDC_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2222222222222A22"
    )
        port map (
      I0 => s00_axi_aresetn,
      I1 => data0(31),
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[31]_LDC_i_2_n_0\
    );
\cnt_clk_reg[31]_LDC_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_clk_reg[27]_LDC_i_3_n_0\,
      CO(3) => \NLW_cnt_clk_reg[31]_LDC_i_3_CO_UNCONNECTED\(3),
      CO(2) => \cnt_clk_reg[31]_LDC_i_3_n_1\,
      CO(1) => \cnt_clk_reg[31]_LDC_i_3_n_2\,
      CO(0) => \cnt_clk_reg[31]_LDC_i_3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(31 downto 28),
      S(3) => \cnt_clk_reg[31]_LDC_i_7_n_0\,
      S(2) => \cnt_clk_reg[31]_LDC_i_8_n_0\,
      S(1) => \cnt_clk_reg[31]_LDC_i_9_n_0\,
      S(0) => \cnt_clk_reg[31]_LDC_i_10_n_0\
    );
\cnt_clk_reg[31]_LDC_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1D1D1DFFFFFF1DFF"
    )
        port map (
      I0 => \cnt_clk_reg[5]_C_n_0\,
      I1 => \cnt_clk_reg[5]_LDC_n_0\,
      I2 => \cnt_clk_reg[5]_P_n_0\,
      I3 => \cnt_clk_reg[7]_C_n_0\,
      I4 => \cnt_clk_reg[7]_LDC_n_0\,
      I5 => \cnt_clk_reg[7]_P_n_0\,
      O => \cnt_clk_reg[31]_LDC_i_4_n_0\
    );
\cnt_clk_reg[31]_LDC_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"757FFFFFFFFFFFFF"
    )
        port map (
      I0 => sel0(2),
      I1 => \cnt_clk_reg[1]_P_n_0\,
      I2 => \cnt_clk_reg[1]_LDC_n_0\,
      I3 => \cnt_clk_reg[1]_C_n_0\,
      I4 => sel0(4),
      I5 => sel0(3),
      O => \cnt_clk_reg[31]_LDC_i_5_n_0\
    );
\cnt_clk_reg[31]_LDC_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \cnt_clk_reg[31]_LDC_i_11_n_0\,
      I1 => \cnt_clk_reg[31]_LDC_i_12_n_0\,
      I2 => \cnt_clk_reg[31]_LDC_i_13_n_0\,
      I3 => \cnt_clk_reg[31]_LDC_i_14_n_0\,
      I4 => \cnt_clk_reg[31]_LDC_i_15_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_16_n_0\,
      O => \cnt_clk_reg[31]_LDC_i_6_n_0\
    );
\cnt_clk_reg[31]_LDC_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[31]_P_n_0\,
      I1 => \cnt_clk_reg[31]_LDC_n_0\,
      I2 => \cnt_clk_reg[31]_C_n_0\,
      O => \cnt_clk_reg[31]_LDC_i_7_n_0\
    );
\cnt_clk_reg[31]_LDC_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[30]_P_n_0\,
      I1 => \cnt_clk_reg[30]_LDC_n_0\,
      I2 => \cnt_clk_reg[30]_C_n_0\,
      O => \cnt_clk_reg[31]_LDC_i_8_n_0\
    );
\cnt_clk_reg[31]_LDC_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[29]_P_n_0\,
      I1 => \cnt_clk_reg[29]_LDC_n_0\,
      I2 => \cnt_clk_reg[29]_C_n_0\,
      O => \cnt_clk_reg[31]_LDC_i_9_n_0\
    );
\cnt_clk_reg[31]_P\: unisim.vcomponents.FDPE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sel0(31),
      PRE => \cnt_clk_reg[31]_LDC_i_1_n_0\,
      Q => \cnt_clk_reg[31]_P_n_0\
    );
\cnt_clk_reg[3]_C\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => \cnt_clk_reg[3]_LDC_i_2_n_0\,
      D => sel0(3),
      Q => \cnt_clk_reg[3]_C_n_0\
    );
\cnt_clk_reg[3]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \cnt_clk_reg[3]_LDC_i_2_n_0\,
      D => '1',
      G => \cnt_clk_reg[3]_LDC_i_1_n_0\,
      GE => '1',
      Q => \cnt_clk_reg[3]_LDC_n_0\
    );
\cnt_clk_reg[3]_LDC_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8888888888888088"
    )
        port map (
      I0 => data0(3),
      I1 => s00_axi_aresetn,
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[3]_LDC_i_1_n_0\
    );
\cnt_clk_reg[3]_LDC_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2222222222222A22"
    )
        port map (
      I0 => s00_axi_aresetn,
      I1 => data0(3),
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[3]_LDC_i_2_n_0\
    );
\cnt_clk_reg[3]_LDC_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \cnt_clk_reg[3]_LDC_i_3_n_0\,
      CO(2) => \cnt_clk_reg[3]_LDC_i_3_n_1\,
      CO(1) => \cnt_clk_reg[3]_LDC_i_3_n_2\,
      CO(0) => \cnt_clk_reg[3]_LDC_i_3_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \cnt_clk_reg[3]_LDC_i_4_n_0\,
      O(3 downto 0) => data0(3 downto 0),
      S(3) => \cnt_clk_reg[3]_LDC_i_5_n_0\,
      S(2) => \cnt_clk_reg[3]_LDC_i_6_n_0\,
      S(1) => \cnt_clk_reg[3]_LDC_i_7_n_0\,
      S(0) => \cnt_clk_reg[3]_LDC_i_8_n_0\
    );
\cnt_clk_reg[3]_LDC_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[0]_P_n_0\,
      I1 => \cnt_clk_reg[0]_LDC_n_0\,
      I2 => \cnt_clk_reg[0]_C_n_0\,
      O => \cnt_clk_reg[3]_LDC_i_4_n_0\
    );
\cnt_clk_reg[3]_LDC_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[3]_P_n_0\,
      I1 => \cnt_clk_reg[3]_LDC_n_0\,
      I2 => \cnt_clk_reg[3]_C_n_0\,
      O => \cnt_clk_reg[3]_LDC_i_5_n_0\
    );
\cnt_clk_reg[3]_LDC_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[2]_P_n_0\,
      I1 => \cnt_clk_reg[2]_LDC_n_0\,
      I2 => \cnt_clk_reg[2]_C_n_0\,
      O => \cnt_clk_reg[3]_LDC_i_6_n_0\
    );
\cnt_clk_reg[3]_LDC_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[1]_P_n_0\,
      I1 => \cnt_clk_reg[1]_LDC_n_0\,
      I2 => \cnt_clk_reg[1]_C_n_0\,
      O => \cnt_clk_reg[3]_LDC_i_7_n_0\
    );
\cnt_clk_reg[3]_LDC_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"1D"
    )
        port map (
      I0 => \cnt_clk_reg[0]_C_n_0\,
      I1 => \cnt_clk_reg[0]_LDC_n_0\,
      I2 => \cnt_clk_reg[0]_P_n_0\,
      O => \cnt_clk_reg[3]_LDC_i_8_n_0\
    );
\cnt_clk_reg[3]_P\: unisim.vcomponents.FDPE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sel0(3),
      PRE => \cnt_clk_reg[3]_LDC_i_1_n_0\,
      Q => \cnt_clk_reg[3]_P_n_0\
    );
\cnt_clk_reg[4]_C\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => \cnt_clk_reg[4]_LDC_i_2_n_0\,
      D => sel0(4),
      Q => \cnt_clk_reg[4]_C_n_0\
    );
\cnt_clk_reg[4]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \cnt_clk_reg[4]_LDC_i_2_n_0\,
      D => '1',
      G => \cnt_clk_reg[4]_LDC_i_1_n_0\,
      GE => '1',
      Q => \cnt_clk_reg[4]_LDC_n_0\
    );
\cnt_clk_reg[4]_LDC_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8888888888888088"
    )
        port map (
      I0 => data0(4),
      I1 => s00_axi_aresetn,
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[4]_LDC_i_1_n_0\
    );
\cnt_clk_reg[4]_LDC_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2222222222222A22"
    )
        port map (
      I0 => s00_axi_aresetn,
      I1 => data0(4),
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[4]_LDC_i_2_n_0\
    );
\cnt_clk_reg[4]_P\: unisim.vcomponents.FDPE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sel0(4),
      PRE => \cnt_clk_reg[4]_LDC_i_1_n_0\,
      Q => \cnt_clk_reg[4]_P_n_0\
    );
\cnt_clk_reg[5]_C\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => \cnt_clk_reg[5]_LDC_i_2_n_0\,
      D => sel0(5),
      Q => \cnt_clk_reg[5]_C_n_0\
    );
\cnt_clk_reg[5]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \cnt_clk_reg[5]_LDC_i_2_n_0\,
      D => '1',
      G => \cnt_clk_reg[5]_LDC_i_1_n_0\,
      GE => '1',
      Q => \cnt_clk_reg[5]_LDC_n_0\
    );
\cnt_clk_reg[5]_LDC_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8888888888888088"
    )
        port map (
      I0 => data0(5),
      I1 => s00_axi_aresetn,
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[5]_LDC_i_1_n_0\
    );
\cnt_clk_reg[5]_LDC_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2222222222222A22"
    )
        port map (
      I0 => s00_axi_aresetn,
      I1 => data0(5),
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[5]_LDC_i_2_n_0\
    );
\cnt_clk_reg[5]_P\: unisim.vcomponents.FDPE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sel0(5),
      PRE => \cnt_clk_reg[5]_LDC_i_1_n_0\,
      Q => \cnt_clk_reg[5]_P_n_0\
    );
\cnt_clk_reg[6]_C\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => \cnt_clk_reg[6]_LDC_i_2_n_0\,
      D => sel0(6),
      Q => \cnt_clk_reg[6]_C_n_0\
    );
\cnt_clk_reg[6]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \cnt_clk_reg[6]_LDC_i_2_n_0\,
      D => '1',
      G => \cnt_clk_reg[6]_LDC_i_1_n_0\,
      GE => '1',
      Q => \cnt_clk_reg[6]_LDC_n_0\
    );
\cnt_clk_reg[6]_LDC_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8888888888888088"
    )
        port map (
      I0 => data0(6),
      I1 => s00_axi_aresetn,
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[6]_LDC_i_1_n_0\
    );
\cnt_clk_reg[6]_LDC_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2222222222222A22"
    )
        port map (
      I0 => s00_axi_aresetn,
      I1 => data0(6),
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[6]_LDC_i_2_n_0\
    );
\cnt_clk_reg[6]_P\: unisim.vcomponents.FDPE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sel0(6),
      PRE => \cnt_clk_reg[6]_LDC_i_1_n_0\,
      Q => \cnt_clk_reg[6]_P_n_0\
    );
\cnt_clk_reg[7]_C\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => \cnt_clk_reg[7]_LDC_i_2_n_0\,
      D => sel0(7),
      Q => \cnt_clk_reg[7]_C_n_0\
    );
\cnt_clk_reg[7]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \cnt_clk_reg[7]_LDC_i_2_n_0\,
      D => '1',
      G => \cnt_clk_reg[7]_LDC_i_1_n_0\,
      GE => '1',
      Q => \cnt_clk_reg[7]_LDC_n_0\
    );
\cnt_clk_reg[7]_LDC_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8888888888888088"
    )
        port map (
      I0 => data0(7),
      I1 => s00_axi_aresetn,
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[7]_LDC_i_1_n_0\
    );
\cnt_clk_reg[7]_LDC_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2222222222222A22"
    )
        port map (
      I0 => s00_axi_aresetn,
      I1 => data0(7),
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[7]_LDC_i_2_n_0\
    );
\cnt_clk_reg[7]_LDC_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_clk_reg[3]_LDC_i_3_n_0\,
      CO(3) => \cnt_clk_reg[7]_LDC_i_3_n_0\,
      CO(2) => \cnt_clk_reg[7]_LDC_i_3_n_1\,
      CO(1) => \cnt_clk_reg[7]_LDC_i_3_n_2\,
      CO(0) => \cnt_clk_reg[7]_LDC_i_3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(7 downto 4),
      S(3) => \cnt_clk_reg[7]_LDC_i_4_n_0\,
      S(2) => \cnt_clk_reg[7]_LDC_i_5_n_0\,
      S(1) => \cnt_clk_reg[7]_LDC_i_6_n_0\,
      S(0) => \cnt_clk_reg[7]_LDC_i_7_n_0\
    );
\cnt_clk_reg[7]_LDC_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[7]_P_n_0\,
      I1 => \cnt_clk_reg[7]_LDC_n_0\,
      I2 => \cnt_clk_reg[7]_C_n_0\,
      O => \cnt_clk_reg[7]_LDC_i_4_n_0\
    );
\cnt_clk_reg[7]_LDC_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[6]_P_n_0\,
      I1 => \cnt_clk_reg[6]_LDC_n_0\,
      I2 => \cnt_clk_reg[6]_C_n_0\,
      O => \cnt_clk_reg[7]_LDC_i_5_n_0\
    );
\cnt_clk_reg[7]_LDC_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[5]_P_n_0\,
      I1 => \cnt_clk_reg[5]_LDC_n_0\,
      I2 => \cnt_clk_reg[5]_C_n_0\,
      O => \cnt_clk_reg[7]_LDC_i_6_n_0\
    );
\cnt_clk_reg[7]_LDC_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \cnt_clk_reg[4]_P_n_0\,
      I1 => \cnt_clk_reg[4]_LDC_n_0\,
      I2 => \cnt_clk_reg[4]_C_n_0\,
      O => \cnt_clk_reg[7]_LDC_i_7_n_0\
    );
\cnt_clk_reg[7]_P\: unisim.vcomponents.FDPE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sel0(7),
      PRE => \cnt_clk_reg[7]_LDC_i_1_n_0\,
      Q => \cnt_clk_reg[7]_P_n_0\
    );
\cnt_clk_reg[8]_C\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => \cnt_clk_reg[8]_LDC_i_2_n_0\,
      D => sel0(8),
      Q => \cnt_clk_reg[8]_C_n_0\
    );
\cnt_clk_reg[8]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \cnt_clk_reg[8]_LDC_i_2_n_0\,
      D => '1',
      G => \cnt_clk_reg[8]_LDC_i_1_n_0\,
      GE => '1',
      Q => \cnt_clk_reg[8]_LDC_n_0\
    );
\cnt_clk_reg[8]_LDC_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8888888888888088"
    )
        port map (
      I0 => data0(8),
      I1 => s00_axi_aresetn,
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[8]_LDC_i_1_n_0\
    );
\cnt_clk_reg[8]_LDC_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2222222222222A22"
    )
        port map (
      I0 => s00_axi_aresetn,
      I1 => data0(8),
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[8]_LDC_i_2_n_0\
    );
\cnt_clk_reg[8]_P\: unisim.vcomponents.FDPE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sel0(8),
      PRE => \cnt_clk_reg[8]_LDC_i_1_n_0\,
      Q => \cnt_clk_reg[8]_P_n_0\
    );
\cnt_clk_reg[9]_C\: unisim.vcomponents.FDCE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      CLR => \cnt_clk_reg[9]_LDC_i_2_n_0\,
      D => sel0(9),
      Q => \cnt_clk_reg[9]_C_n_0\
    );
\cnt_clk_reg[9]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \cnt_clk_reg[9]_LDC_i_2_n_0\,
      D => '1',
      G => \cnt_clk_reg[9]_LDC_i_1_n_0\,
      GE => '1',
      Q => \cnt_clk_reg[9]_LDC_n_0\
    );
\cnt_clk_reg[9]_LDC_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8888888888888088"
    )
        port map (
      I0 => data0(9),
      I1 => s00_axi_aresetn,
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[9]_LDC_i_1_n_0\
    );
\cnt_clk_reg[9]_LDC_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2222222222222A22"
    )
        port map (
      I0 => s00_axi_aresetn,
      I1 => data0(9),
      I2 => \cnt_clk_reg[31]_LDC_i_4_n_0\,
      I3 => sel0(0),
      I4 => \cnt_clk_reg[31]_LDC_i_5_n_0\,
      I5 => \cnt_clk_reg[31]_LDC_i_6_n_0\,
      O => \cnt_clk_reg[9]_LDC_i_2_n_0\
    );
\cnt_clk_reg[9]_P\: unisim.vcomponents.FDPE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => sel0(9),
      PRE => \cnt_clk_reg[9]_LDC_i_1_n_0\,
      Q => \cnt_clk_reg[9]_P_n_0\
    );
read_r_reg: unisim.vcomponents.LDCE
    generic map(
      INIT => '0',
      IS_G_INVERTED => '1'
    )
        port map (
      CLR => '0',
      D => read_r_reg_i_1_n_0,
      G => read_r_reg_i_3_n_0,
      GE => '1',
      Q => READ
    );
read_r_reg_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \stateDNA_reg_reg[1]_P_n_0\,
      I1 => \stateDNA_reg_reg[1]_LDC_n_0\,
      O => read_r_reg_i_1_n_0
    );
read_r_reg_i_10: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFE2E2E2FFE2"
    )
        port map (
      I0 => \cnt_clk_reg[5]_C_n_0\,
      I1 => \cnt_clk_reg[5]_LDC_n_0\,
      I2 => \cnt_clk_reg[5]_P_n_0\,
      I3 => \cnt_clk_reg[7]_C_n_0\,
      I4 => \cnt_clk_reg[7]_LDC_n_0\,
      I5 => \cnt_clk_reg[7]_P_n_0\,
      O => read_r_reg_i_10_n_0
    );
read_r_reg_i_11: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFE2E2E2FFE2"
    )
        port map (
      I0 => \cnt_clk_reg[8]_C_n_0\,
      I1 => \cnt_clk_reg[8]_LDC_n_0\,
      I2 => \cnt_clk_reg[8]_P_n_0\,
      I3 => \cnt_clk_reg[9]_C_n_0\,
      I4 => \cnt_clk_reg[9]_LDC_n_0\,
      I5 => \cnt_clk_reg[9]_P_n_0\,
      O => read_r_reg_i_11_n_0
    );
read_r_reg_i_12: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFE2E2E2FFE2"
    )
        port map (
      I0 => \cnt_clk_reg[12]_C_n_0\,
      I1 => \cnt_clk_reg[12]_LDC_n_0\,
      I2 => \cnt_clk_reg[12]_P_n_0\,
      I3 => \cnt_clk_reg[13]_C_n_0\,
      I4 => \cnt_clk_reg[13]_LDC_n_0\,
      I5 => \cnt_clk_reg[13]_P_n_0\,
      O => read_r_reg_i_12_n_0
    );
read_r_reg_i_13: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFE2E2E2FFE2"
    )
        port map (
      I0 => \cnt_clk_reg[10]_C_n_0\,
      I1 => \cnt_clk_reg[10]_LDC_n_0\,
      I2 => \cnt_clk_reg[10]_P_n_0\,
      I3 => \cnt_clk_reg[11]_C_n_0\,
      I4 => \cnt_clk_reg[11]_LDC_n_0\,
      I5 => \cnt_clk_reg[11]_P_n_0\,
      O => read_r_reg_i_13_n_0
    );
read_r_reg_i_14: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFE2E2E2FFE2"
    )
        port map (
      I0 => \cnt_clk_reg[20]_C_n_0\,
      I1 => \cnt_clk_reg[20]_LDC_n_0\,
      I2 => \cnt_clk_reg[20]_P_n_0\,
      I3 => \cnt_clk_reg[21]_C_n_0\,
      I4 => \cnt_clk_reg[21]_LDC_n_0\,
      I5 => \cnt_clk_reg[21]_P_n_0\,
      O => read_r_reg_i_14_n_0
    );
read_r_reg_i_15: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFE2E2E2FFE2"
    )
        port map (
      I0 => \cnt_clk_reg[18]_C_n_0\,
      I1 => \cnt_clk_reg[18]_LDC_n_0\,
      I2 => \cnt_clk_reg[18]_P_n_0\,
      I3 => \cnt_clk_reg[19]_C_n_0\,
      I4 => \cnt_clk_reg[19]_LDC_n_0\,
      I5 => \cnt_clk_reg[19]_P_n_0\,
      O => read_r_reg_i_15_n_0
    );
read_r_reg_i_16: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFE2E2E2FFE2"
    )
        port map (
      I0 => \cnt_clk_reg[28]_C_n_0\,
      I1 => \cnt_clk_reg[28]_LDC_n_0\,
      I2 => \cnt_clk_reg[28]_P_n_0\,
      I3 => \cnt_clk_reg[29]_C_n_0\,
      I4 => \cnt_clk_reg[29]_LDC_n_0\,
      I5 => \cnt_clk_reg[29]_P_n_0\,
      O => read_r_reg_i_16_n_0
    );
read_r_reg_i_17: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFE2E2E2FFE2"
    )
        port map (
      I0 => \cnt_clk_reg[26]_C_n_0\,
      I1 => \cnt_clk_reg[26]_LDC_n_0\,
      I2 => \cnt_clk_reg[26]_P_n_0\,
      I3 => \cnt_clk_reg[27]_C_n_0\,
      I4 => \cnt_clk_reg[27]_LDC_n_0\,
      I5 => \cnt_clk_reg[27]_P_n_0\,
      O => read_r_reg_i_17_n_0
    );
read_r_reg_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000001FFFFFFFF"
    )
        port map (
      I0 => stateDNA_reg(1),
      I1 => read_r_reg_i_5_n_0,
      I2 => read_r_reg_i_6_n_0,
      I3 => read_r_reg_i_7_n_0,
      I4 => read_r_reg_i_8_n_0,
      I5 => stateDNA_reg(0),
      O => read_r_reg_i_3_n_0
    );
read_r_reg_i_4: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \stateDNA_reg_reg[1]_LDC_n_0\,
      I1 => \stateDNA_reg_reg[1]_P_n_0\,
      O => stateDNA_reg(1)
    );
read_r_reg_i_5: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => sel0(3),
      I1 => sel0(4),
      I2 => sel0(1),
      I3 => sel0(2),
      I4 => sel0(0),
      I5 => read_r_reg_i_10_n_0,
      O => read_r_reg_i_5_n_0
    );
read_r_reg_i_6: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFEFF"
    )
        port map (
      I0 => read_r_reg_i_11_n_0,
      I1 => sel0(31),
      I2 => sel0(30),
      I3 => sel0(6),
      I4 => read_r_reg_i_12_n_0,
      I5 => read_r_reg_i_13_n_0,
      O => read_r_reg_i_6_n_0
    );
read_r_reg_i_7: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => sel0(16),
      I1 => sel0(17),
      I2 => sel0(14),
      I3 => sel0(15),
      I4 => read_r_reg_i_14_n_0,
      I5 => read_r_reg_i_15_n_0,
      O => read_r_reg_i_7_n_0
    );
read_r_reg_i_8: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => sel0(24),
      I1 => sel0(25),
      I2 => sel0(22),
      I3 => sel0(23),
      I4 => read_r_reg_i_16_n_0,
      I5 => read_r_reg_i_17_n_0,
      O => read_r_reg_i_8_n_0
    );
read_r_reg_i_9: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \stateDNA_reg_reg[0]_LDC_n_0\,
      I1 => \stateDNA_reg_reg[0]_P_n_0\,
      O => stateDNA_reg(0)
    );
\stateDNA_reg_reg[0]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \stateDNA_reg_reg[0]_LDC_i_2_n_0\,
      D => '1',
      G => \stateDNA_reg_reg[0]_LDC_i_1_n_0\,
      GE => '1',
      Q => \stateDNA_reg_reg[0]_LDC_n_0\
    );
\stateDNA_reg_reg[0]_LDC_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => read_r_reg_i_3_n_0,
      I1 => s00_axi_aresetn,
      O => \stateDNA_reg_reg[0]_LDC_i_1_n_0\
    );
\stateDNA_reg_reg[0]_LDC_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => s00_axi_aresetn,
      I1 => read_r_reg_i_3_n_0,
      O => \stateDNA_reg_reg[0]_LDC_i_2_n_0\
    );
\stateDNA_reg_reg[0]_P\: unisim.vcomponents.FDPE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => '0',
      PRE => \stateDNA_reg_reg[0]_LDC_i_1_n_0\,
      Q => \stateDNA_reg_reg[0]_P_n_0\
    );
\stateDNA_reg_reg[1]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => \stateDNA_reg_reg[1]_LDC_i_2_n_0\,
      D => '1',
      G => \stateDNA_reg_reg[1]_LDC_i_1_n_0\,
      GE => '1',
      Q => \stateDNA_reg_reg[1]_LDC_n_0\
    );
\stateDNA_reg_reg[1]_LDC_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2A0000002A808080"
    )
        port map (
      I0 => s00_axi_aresetn,
      I1 => \stateDNA_reg_reg[0]_P_n_0\,
      I2 => \stateDNA_reg_reg[0]_LDC_n_0\,
      I3 => \stateDNA_reg_reg[1]_LDC_n_0\,
      I4 => \stateDNA_reg_reg[1]_P_n_0\,
      I5 => \stateDNA_reg_reg[1]_LDC_i_3_n_0\,
      O => \stateDNA_reg_reg[1]_LDC_i_1_n_0\
    );
\stateDNA_reg_reg[1]_LDC_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A8880AAA0AAA0AAA"
    )
        port map (
      I0 => s00_axi_aresetn,
      I1 => \stateDNA_reg_reg[1]_LDC_i_3_n_0\,
      I2 => \stateDNA_reg_reg[1]_P_n_0\,
      I3 => \stateDNA_reg_reg[1]_LDC_n_0\,
      I4 => \stateDNA_reg_reg[0]_LDC_n_0\,
      I5 => \stateDNA_reg_reg[0]_P_n_0\,
      O => \stateDNA_reg_reg[1]_LDC_i_2_n_0\
    );
\stateDNA_reg_reg[1]_LDC_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => read_r_reg_i_5_n_0,
      I1 => \cnt_clk_reg[31]_LDC_i_16_n_0\,
      I2 => \cnt_clk_reg[31]_LDC_i_15_n_0\,
      I3 => \cnt_clk_reg[31]_LDC_i_14_n_0\,
      I4 => \cnt_clk_reg[31]_LDC_i_13_n_0\,
      I5 => read_r_reg_i_8_n_0,
      O => \stateDNA_reg_reg[1]_LDC_i_3_n_0\
    );
\stateDNA_reg_reg[1]_P\: unisim.vcomponents.FDPE
     port map (
      C => s00_axi_aclk,
      CE => '1',
      D => '0',
      PRE => \stateDNA_reg_reg[1]_LDC_i_1_n_0\,
      Q => \stateDNA_reg_reg[1]_P_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    s00_axi_aclk : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC;
    ready : out STD_LOGIC;
    outID : out STD_LOGIC_VECTOR ( 63 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_2_ID_FPGA_0_0,ID_FPGA_v1_0,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "ID_FPGA_v1_0,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of s00_axi_aclk : signal is "xilinx.com:signal:clock:1.0 s00_axi_aclk CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of s00_axi_aclk : signal is "XIL_INTERFACENAME s00_axi_aclk, ASSOCIATED_RESET s00_axi_aresetn, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_2_s00_axi_aclk_0, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of s00_axi_aresetn : signal is "xilinx.com:signal:reset:1.0 s00_axi_aresetn RST";
  attribute X_INTERFACE_PARAMETER of s00_axi_aresetn : signal is "XIL_INTERFACENAME s00_axi_aresetn, POLARITY ACTIVE_LOW, INSERT_VIP 0";
begin
  ready <= 'Z';
  outID(0) <= 'Z';
  outID(1) <= 'Z';
  outID(2) <= 'Z';
  outID(3) <= 'Z';
  outID(4) <= 'Z';
  outID(5) <= 'Z';
  outID(6) <= 'Z';
  outID(7) <= 'Z';
  outID(8) <= 'Z';
  outID(9) <= 'Z';
  outID(10) <= 'Z';
  outID(11) <= 'Z';
  outID(12) <= 'Z';
  outID(13) <= 'Z';
  outID(14) <= 'Z';
  outID(15) <= 'Z';
  outID(16) <= 'Z';
  outID(17) <= 'Z';
  outID(18) <= 'Z';
  outID(19) <= 'Z';
  outID(20) <= 'Z';
  outID(21) <= 'Z';
  outID(22) <= 'Z';
  outID(23) <= 'Z';
  outID(24) <= 'Z';
  outID(25) <= 'Z';
  outID(26) <= 'Z';
  outID(27) <= 'Z';
  outID(28) <= 'Z';
  outID(29) <= 'Z';
  outID(30) <= 'Z';
  outID(31) <= 'Z';
  outID(32) <= 'Z';
  outID(33) <= 'Z';
  outID(34) <= 'Z';
  outID(35) <= 'Z';
  outID(36) <= 'Z';
  outID(37) <= 'Z';
  outID(38) <= 'Z';
  outID(39) <= 'Z';
  outID(40) <= 'Z';
  outID(41) <= 'Z';
  outID(42) <= 'Z';
  outID(43) <= 'Z';
  outID(44) <= 'Z';
  outID(45) <= 'Z';
  outID(46) <= 'Z';
  outID(47) <= 'Z';
  outID(48) <= 'Z';
  outID(49) <= 'Z';
  outID(50) <= 'Z';
  outID(51) <= 'Z';
  outID(52) <= 'Z';
  outID(53) <= 'Z';
  outID(54) <= 'Z';
  outID(55) <= 'Z';
  outID(56) <= 'Z';
  outID(57) <= 'Z';
  outID(58) <= 'Z';
  outID(59) <= 'Z';
  outID(60) <= 'Z';
  outID(61) <= 'Z';
  outID(62) <= 'Z';
  outID(63) <= 'Z';
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ID_FPGA_v1_0
     port map (
      s00_axi_aclk => s00_axi_aclk,
      s00_axi_aresetn => s00_axi_aresetn
    );
end STRUCTURE;
