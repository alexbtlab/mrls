// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
// Date        : Thu Jun  2 10:36:15 2022
// Host        : alexbtlab-System-Product-Name running 64-bit Ubuntu 20.04.3 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_syncADCData_0_0_sim_netlist.v
// Design      : design_1_syncADCData_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_syncADCData_0_0,syncADCData_v1_0,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "syncADCData_v1_0,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (dco_or_dcoa,
    dco_or_dcob,
    dco_or_ora,
    dco_or_orb,
    reset,
    DCOA,
    DATA_CH1_out,
    DATA_CH2_out,
    clk_10MHz,
    clk_100MHz,
    DATA_CH1_in,
    DATA_CH2_in);
  (* X_INTERFACE_INFO = "user.org:interface:dco_or:1.0 DCO_OR dcoa" *) input dco_or_dcoa;
  (* X_INTERFACE_INFO = "user.org:interface:dco_or:1.0 DCO_OR dcob" *) input dco_or_dcob;
  (* X_INTERFACE_INFO = "user.org:interface:dco_or:1.0 DCO_OR ora" *) input dco_or_ora;
  (* X_INTERFACE_INFO = "user.org:interface:dco_or:1.0 DCO_OR orb" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME DCO_OR, SV_INTERFACE true" *) input dco_or_orb;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 reset RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME reset, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input reset;
  output DCOA;
  output [15:0]DATA_CH1_out;
  output [15:0]DATA_CH2_out;
  input clk_10MHz;
  input clk_100MHz;
  input [15:0]DATA_CH1_in;
  input [15:0]DATA_CH2_in;

  wire [15:0]DATA_CH1_in;
  wire [15:0]DATA_CH1_out;
  wire [15:0]DATA_CH2_in;
  wire [15:0]DATA_CH2_out;
  wire clk_100MHz;
  wire clk_10MHz;
  wire dco_or_dcoa;
  wire reset;

  assign DCOA = dco_or_dcoa;
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_syncADCData_v1_0 inst
       (.DATA_CH1_in(DATA_CH1_in),
        .DATA_CH1_out(DATA_CH1_out),
        .DATA_CH2_in(DATA_CH2_in),
        .DATA_CH2_out(DATA_CH2_out),
        .clk_100MHz(clk_100MHz),
        .clk_10MHz(clk_10MHz),
        .dco_or_dcoa(dco_or_dcoa),
        .reset(reset));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_syncADCData_v1_0
   (DATA_CH1_out,
    DATA_CH2_out,
    reset,
    dco_or_dcoa,
    DATA_CH1_in,
    clk_100MHz,
    clk_10MHz,
    DATA_CH2_in);
  output [15:0]DATA_CH1_out;
  output [15:0]DATA_CH2_out;
  input reset;
  input dco_or_dcoa;
  input [15:0]DATA_CH1_in;
  input clk_100MHz;
  input clk_10MHz;
  input [15:0]DATA_CH2_in;

  wire [15:0]DATA_CH1_in;
  wire [15:0]DATA_CH1_out;
  wire [15:0]DATA_CH1_tmp;
  wire DATA_CH1_tmp_0;
  wire [15:0]DATA_CH2_in;
  wire [15:0]DATA_CH2_out;
  wire [15:0]DATA_CH2_tmp;
  wire clk_100MHz;
  wire clk_10MHz;
  wire \count[3]_i_1_n_0 ;
  wire [3:0]count_reg;
  wire dco_or_dcoa;
  wire p_0_in;
  wire [3:0]p_0_in__0;
  wire reset;

  LUT1 #(
    .INIT(2'h1)) 
    \DATA_CH1_sync[15]_i_1 
       (.I0(reset),
        .O(p_0_in));
  FDRE \DATA_CH1_sync_reg[0] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(DATA_CH1_tmp[0]),
        .Q(DATA_CH1_out[0]),
        .R(p_0_in));
  FDRE \DATA_CH1_sync_reg[10] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(DATA_CH1_tmp[10]),
        .Q(DATA_CH1_out[10]),
        .R(p_0_in));
  FDRE \DATA_CH1_sync_reg[11] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(DATA_CH1_tmp[11]),
        .Q(DATA_CH1_out[11]),
        .R(p_0_in));
  FDRE \DATA_CH1_sync_reg[12] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(DATA_CH1_tmp[12]),
        .Q(DATA_CH1_out[12]),
        .R(p_0_in));
  FDRE \DATA_CH1_sync_reg[13] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(DATA_CH1_tmp[13]),
        .Q(DATA_CH1_out[13]),
        .R(p_0_in));
  FDRE \DATA_CH1_sync_reg[14] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(DATA_CH1_tmp[14]),
        .Q(DATA_CH1_out[14]),
        .R(p_0_in));
  FDRE \DATA_CH1_sync_reg[15] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(DATA_CH1_tmp[15]),
        .Q(DATA_CH1_out[15]),
        .R(p_0_in));
  FDRE \DATA_CH1_sync_reg[1] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(DATA_CH1_tmp[1]),
        .Q(DATA_CH1_out[1]),
        .R(p_0_in));
  FDRE \DATA_CH1_sync_reg[2] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(DATA_CH1_tmp[2]),
        .Q(DATA_CH1_out[2]),
        .R(p_0_in));
  FDRE \DATA_CH1_sync_reg[3] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(DATA_CH1_tmp[3]),
        .Q(DATA_CH1_out[3]),
        .R(p_0_in));
  FDRE \DATA_CH1_sync_reg[4] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(DATA_CH1_tmp[4]),
        .Q(DATA_CH1_out[4]),
        .R(p_0_in));
  FDRE \DATA_CH1_sync_reg[5] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(DATA_CH1_tmp[5]),
        .Q(DATA_CH1_out[5]),
        .R(p_0_in));
  FDRE \DATA_CH1_sync_reg[6] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(DATA_CH1_tmp[6]),
        .Q(DATA_CH1_out[6]),
        .R(p_0_in));
  FDRE \DATA_CH1_sync_reg[7] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(DATA_CH1_tmp[7]),
        .Q(DATA_CH1_out[7]),
        .R(p_0_in));
  FDRE \DATA_CH1_sync_reg[8] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(DATA_CH1_tmp[8]),
        .Q(DATA_CH1_out[8]),
        .R(p_0_in));
  FDRE \DATA_CH1_sync_reg[9] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(DATA_CH1_tmp[9]),
        .Q(DATA_CH1_out[9]),
        .R(p_0_in));
  LUT5 #(
    .INIT(32'h00000020)) 
    \DATA_CH1_tmp[15]_i_1 
       (.I0(dco_or_dcoa),
        .I1(count_reg[0]),
        .I2(count_reg[1]),
        .I3(count_reg[2]),
        .I4(count_reg[3]),
        .O(DATA_CH1_tmp_0));
  FDRE \DATA_CH1_tmp_reg[0] 
       (.C(clk_100MHz),
        .CE(DATA_CH1_tmp_0),
        .D(DATA_CH1_in[0]),
        .Q(DATA_CH1_tmp[0]),
        .R(p_0_in));
  FDRE \DATA_CH1_tmp_reg[10] 
       (.C(clk_100MHz),
        .CE(DATA_CH1_tmp_0),
        .D(DATA_CH1_in[10]),
        .Q(DATA_CH1_tmp[10]),
        .R(p_0_in));
  FDRE \DATA_CH1_tmp_reg[11] 
       (.C(clk_100MHz),
        .CE(DATA_CH1_tmp_0),
        .D(DATA_CH1_in[11]),
        .Q(DATA_CH1_tmp[11]),
        .R(p_0_in));
  FDRE \DATA_CH1_tmp_reg[12] 
       (.C(clk_100MHz),
        .CE(DATA_CH1_tmp_0),
        .D(DATA_CH1_in[12]),
        .Q(DATA_CH1_tmp[12]),
        .R(p_0_in));
  FDRE \DATA_CH1_tmp_reg[13] 
       (.C(clk_100MHz),
        .CE(DATA_CH1_tmp_0),
        .D(DATA_CH1_in[13]),
        .Q(DATA_CH1_tmp[13]),
        .R(p_0_in));
  FDRE \DATA_CH1_tmp_reg[14] 
       (.C(clk_100MHz),
        .CE(DATA_CH1_tmp_0),
        .D(DATA_CH1_in[14]),
        .Q(DATA_CH1_tmp[14]),
        .R(p_0_in));
  FDRE \DATA_CH1_tmp_reg[15] 
       (.C(clk_100MHz),
        .CE(DATA_CH1_tmp_0),
        .D(DATA_CH1_in[15]),
        .Q(DATA_CH1_tmp[15]),
        .R(p_0_in));
  FDRE \DATA_CH1_tmp_reg[1] 
       (.C(clk_100MHz),
        .CE(DATA_CH1_tmp_0),
        .D(DATA_CH1_in[1]),
        .Q(DATA_CH1_tmp[1]),
        .R(p_0_in));
  FDRE \DATA_CH1_tmp_reg[2] 
       (.C(clk_100MHz),
        .CE(DATA_CH1_tmp_0),
        .D(DATA_CH1_in[2]),
        .Q(DATA_CH1_tmp[2]),
        .R(p_0_in));
  FDRE \DATA_CH1_tmp_reg[3] 
       (.C(clk_100MHz),
        .CE(DATA_CH1_tmp_0),
        .D(DATA_CH1_in[3]),
        .Q(DATA_CH1_tmp[3]),
        .R(p_0_in));
  FDRE \DATA_CH1_tmp_reg[4] 
       (.C(clk_100MHz),
        .CE(DATA_CH1_tmp_0),
        .D(DATA_CH1_in[4]),
        .Q(DATA_CH1_tmp[4]),
        .R(p_0_in));
  FDRE \DATA_CH1_tmp_reg[5] 
       (.C(clk_100MHz),
        .CE(DATA_CH1_tmp_0),
        .D(DATA_CH1_in[5]),
        .Q(DATA_CH1_tmp[5]),
        .R(p_0_in));
  FDRE \DATA_CH1_tmp_reg[6] 
       (.C(clk_100MHz),
        .CE(DATA_CH1_tmp_0),
        .D(DATA_CH1_in[6]),
        .Q(DATA_CH1_tmp[6]),
        .R(p_0_in));
  FDRE \DATA_CH1_tmp_reg[7] 
       (.C(clk_100MHz),
        .CE(DATA_CH1_tmp_0),
        .D(DATA_CH1_in[7]),
        .Q(DATA_CH1_tmp[7]),
        .R(p_0_in));
  FDRE \DATA_CH1_tmp_reg[8] 
       (.C(clk_100MHz),
        .CE(DATA_CH1_tmp_0),
        .D(DATA_CH1_in[8]),
        .Q(DATA_CH1_tmp[8]),
        .R(p_0_in));
  FDRE \DATA_CH1_tmp_reg[9] 
       (.C(clk_100MHz),
        .CE(DATA_CH1_tmp_0),
        .D(DATA_CH1_in[9]),
        .Q(DATA_CH1_tmp[9]),
        .R(p_0_in));
  FDRE \DATA_CH2_sync_reg[0] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(DATA_CH2_tmp[0]),
        .Q(DATA_CH2_out[0]),
        .R(p_0_in));
  FDRE \DATA_CH2_sync_reg[10] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(DATA_CH2_tmp[10]),
        .Q(DATA_CH2_out[10]),
        .R(p_0_in));
  FDRE \DATA_CH2_sync_reg[11] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(DATA_CH2_tmp[11]),
        .Q(DATA_CH2_out[11]),
        .R(p_0_in));
  FDRE \DATA_CH2_sync_reg[12] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(DATA_CH2_tmp[12]),
        .Q(DATA_CH2_out[12]),
        .R(p_0_in));
  FDRE \DATA_CH2_sync_reg[13] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(DATA_CH2_tmp[13]),
        .Q(DATA_CH2_out[13]),
        .R(p_0_in));
  FDRE \DATA_CH2_sync_reg[14] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(DATA_CH2_tmp[14]),
        .Q(DATA_CH2_out[14]),
        .R(p_0_in));
  FDRE \DATA_CH2_sync_reg[15] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(DATA_CH2_tmp[15]),
        .Q(DATA_CH2_out[15]),
        .R(p_0_in));
  FDRE \DATA_CH2_sync_reg[1] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(DATA_CH2_tmp[1]),
        .Q(DATA_CH2_out[1]),
        .R(p_0_in));
  FDRE \DATA_CH2_sync_reg[2] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(DATA_CH2_tmp[2]),
        .Q(DATA_CH2_out[2]),
        .R(p_0_in));
  FDRE \DATA_CH2_sync_reg[3] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(DATA_CH2_tmp[3]),
        .Q(DATA_CH2_out[3]),
        .R(p_0_in));
  FDRE \DATA_CH2_sync_reg[4] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(DATA_CH2_tmp[4]),
        .Q(DATA_CH2_out[4]),
        .R(p_0_in));
  FDRE \DATA_CH2_sync_reg[5] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(DATA_CH2_tmp[5]),
        .Q(DATA_CH2_out[5]),
        .R(p_0_in));
  FDRE \DATA_CH2_sync_reg[6] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(DATA_CH2_tmp[6]),
        .Q(DATA_CH2_out[6]),
        .R(p_0_in));
  FDRE \DATA_CH2_sync_reg[7] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(DATA_CH2_tmp[7]),
        .Q(DATA_CH2_out[7]),
        .R(p_0_in));
  FDRE \DATA_CH2_sync_reg[8] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(DATA_CH2_tmp[8]),
        .Q(DATA_CH2_out[8]),
        .R(p_0_in));
  FDRE \DATA_CH2_sync_reg[9] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(DATA_CH2_tmp[9]),
        .Q(DATA_CH2_out[9]),
        .R(p_0_in));
  FDRE \DATA_CH2_tmp_reg[0] 
       (.C(clk_100MHz),
        .CE(DATA_CH1_tmp_0),
        .D(DATA_CH2_in[0]),
        .Q(DATA_CH2_tmp[0]),
        .R(p_0_in));
  FDRE \DATA_CH2_tmp_reg[10] 
       (.C(clk_100MHz),
        .CE(DATA_CH1_tmp_0),
        .D(DATA_CH2_in[10]),
        .Q(DATA_CH2_tmp[10]),
        .R(p_0_in));
  FDRE \DATA_CH2_tmp_reg[11] 
       (.C(clk_100MHz),
        .CE(DATA_CH1_tmp_0),
        .D(DATA_CH2_in[11]),
        .Q(DATA_CH2_tmp[11]),
        .R(p_0_in));
  FDRE \DATA_CH2_tmp_reg[12] 
       (.C(clk_100MHz),
        .CE(DATA_CH1_tmp_0),
        .D(DATA_CH2_in[12]),
        .Q(DATA_CH2_tmp[12]),
        .R(p_0_in));
  FDRE \DATA_CH2_tmp_reg[13] 
       (.C(clk_100MHz),
        .CE(DATA_CH1_tmp_0),
        .D(DATA_CH2_in[13]),
        .Q(DATA_CH2_tmp[13]),
        .R(p_0_in));
  FDRE \DATA_CH2_tmp_reg[14] 
       (.C(clk_100MHz),
        .CE(DATA_CH1_tmp_0),
        .D(DATA_CH2_in[14]),
        .Q(DATA_CH2_tmp[14]),
        .R(p_0_in));
  FDRE \DATA_CH2_tmp_reg[15] 
       (.C(clk_100MHz),
        .CE(DATA_CH1_tmp_0),
        .D(DATA_CH2_in[15]),
        .Q(DATA_CH2_tmp[15]),
        .R(p_0_in));
  FDRE \DATA_CH2_tmp_reg[1] 
       (.C(clk_100MHz),
        .CE(DATA_CH1_tmp_0),
        .D(DATA_CH2_in[1]),
        .Q(DATA_CH2_tmp[1]),
        .R(p_0_in));
  FDRE \DATA_CH2_tmp_reg[2] 
       (.C(clk_100MHz),
        .CE(DATA_CH1_tmp_0),
        .D(DATA_CH2_in[2]),
        .Q(DATA_CH2_tmp[2]),
        .R(p_0_in));
  FDRE \DATA_CH2_tmp_reg[3] 
       (.C(clk_100MHz),
        .CE(DATA_CH1_tmp_0),
        .D(DATA_CH2_in[3]),
        .Q(DATA_CH2_tmp[3]),
        .R(p_0_in));
  FDRE \DATA_CH2_tmp_reg[4] 
       (.C(clk_100MHz),
        .CE(DATA_CH1_tmp_0),
        .D(DATA_CH2_in[4]),
        .Q(DATA_CH2_tmp[4]),
        .R(p_0_in));
  FDRE \DATA_CH2_tmp_reg[5] 
       (.C(clk_100MHz),
        .CE(DATA_CH1_tmp_0),
        .D(DATA_CH2_in[5]),
        .Q(DATA_CH2_tmp[5]),
        .R(p_0_in));
  FDRE \DATA_CH2_tmp_reg[6] 
       (.C(clk_100MHz),
        .CE(DATA_CH1_tmp_0),
        .D(DATA_CH2_in[6]),
        .Q(DATA_CH2_tmp[6]),
        .R(p_0_in));
  FDRE \DATA_CH2_tmp_reg[7] 
       (.C(clk_100MHz),
        .CE(DATA_CH1_tmp_0),
        .D(DATA_CH2_in[7]),
        .Q(DATA_CH2_tmp[7]),
        .R(p_0_in));
  FDRE \DATA_CH2_tmp_reg[8] 
       (.C(clk_100MHz),
        .CE(DATA_CH1_tmp_0),
        .D(DATA_CH2_in[8]),
        .Q(DATA_CH2_tmp[8]),
        .R(p_0_in));
  FDRE \DATA_CH2_tmp_reg[9] 
       (.C(clk_100MHz),
        .CE(DATA_CH1_tmp_0),
        .D(DATA_CH2_in[9]),
        .Q(DATA_CH2_tmp[9]),
        .R(p_0_in));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \count[0]_i_1 
       (.I0(count_reg[0]),
        .O(p_0_in__0[0]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \count[1]_i_1 
       (.I0(count_reg[0]),
        .I1(count_reg[1]),
        .O(p_0_in__0[1]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \count[2]_i_1 
       (.I0(count_reg[0]),
        .I1(count_reg[1]),
        .I2(count_reg[2]),
        .O(p_0_in__0[2]));
  LUT2 #(
    .INIT(4'h7)) 
    \count[3]_i_1 
       (.I0(reset),
        .I1(dco_or_dcoa),
        .O(\count[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \count[3]_i_2 
       (.I0(count_reg[1]),
        .I1(count_reg[0]),
        .I2(count_reg[2]),
        .I3(count_reg[3]),
        .O(p_0_in__0[3]));
  FDRE \count_reg[0] 
       (.C(clk_100MHz),
        .CE(1'b1),
        .D(p_0_in__0[0]),
        .Q(count_reg[0]),
        .R(\count[3]_i_1_n_0 ));
  FDRE \count_reg[1] 
       (.C(clk_100MHz),
        .CE(1'b1),
        .D(p_0_in__0[1]),
        .Q(count_reg[1]),
        .R(\count[3]_i_1_n_0 ));
  FDRE \count_reg[2] 
       (.C(clk_100MHz),
        .CE(1'b1),
        .D(p_0_in__0[2]),
        .Q(count_reg[2]),
        .R(\count[3]_i_1_n_0 ));
  FDRE \count_reg[3] 
       (.C(clk_100MHz),
        .CE(1'b1),
        .D(p_0_in__0[3]),
        .Q(count_reg[3]),
        .R(\count[3]_i_1_n_0 ));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
