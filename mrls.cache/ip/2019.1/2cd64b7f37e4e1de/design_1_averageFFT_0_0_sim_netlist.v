// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
// Date        : Fri Apr 29 17:22:33 2022
// Host        : alexbtlab-System-Product-Name running 64-bit Ubuntu 20.04.3 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_averageFFT_0_0_sim_netlist.v
// Design      : design_1_averageFFT_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_averageFFT_v4_0
   (m00_axis_tdata,
    azimut8,
    m00_axis_tlast,
    m00_axis_tvalid,
    clk_10MHz,
    m00_axis_aclk,
    m00_axis_aresetn,
    azimuth_0,
    allowed_clk,
    s00_axis_tlast,
    s00_axis_tvalid);
  output [15:0]m00_axis_tdata;
  output [15:0]azimut8;
  output m00_axis_tlast;
  output m00_axis_tvalid;
  input clk_10MHz;
  input m00_axis_aclk;
  input m00_axis_aresetn;
  input azimuth_0;
  input allowed_clk;
  input s00_axis_tlast;
  input s00_axis_tvalid;

  wire [15:0]adr;
  wire adr0_carry__0_n_0;
  wire adr0_carry__0_n_1;
  wire adr0_carry__0_n_2;
  wire adr0_carry__0_n_3;
  wire adr0_carry__0_n_4;
  wire adr0_carry__0_n_5;
  wire adr0_carry__0_n_6;
  wire adr0_carry__0_n_7;
  wire adr0_carry__1_n_0;
  wire adr0_carry__1_n_1;
  wire adr0_carry__1_n_2;
  wire adr0_carry__1_n_3;
  wire adr0_carry__1_n_4;
  wire adr0_carry__1_n_5;
  wire adr0_carry__1_n_6;
  wire adr0_carry__1_n_7;
  wire adr0_carry__2_n_2;
  wire adr0_carry__2_n_3;
  wire adr0_carry__2_n_5;
  wire adr0_carry__2_n_6;
  wire adr0_carry__2_n_7;
  wire adr0_carry_n_0;
  wire adr0_carry_n_1;
  wire adr0_carry_n_2;
  wire adr0_carry_n_3;
  wire adr0_carry_n_4;
  wire adr0_carry_n_5;
  wire adr0_carry_n_6;
  wire adr0_carry_n_7;
  wire \adr[15]_i_1_n_0 ;
  wire \adr[15]_i_3_n_0 ;
  wire \adr[15]_i_4_n_0 ;
  wire \adr[15]_i_5_n_0 ;
  wire \adr[15]_i_6_n_0 ;
  wire [15:0]adr_0;
  wire allowed_clk;
  wire [15:0]azimut8;
  wire azimuth_0;
  wire clk_10MHz;
  wire \cnt_high_allowed_clk[0]_i_1_n_0 ;
  wire \cnt_high_allowed_clk[0]_i_3_n_0 ;
  wire [15:0]cnt_high_allowed_clk_reg;
  wire \cnt_high_allowed_clk_reg[0]_i_2_n_0 ;
  wire \cnt_high_allowed_clk_reg[0]_i_2_n_1 ;
  wire \cnt_high_allowed_clk_reg[0]_i_2_n_2 ;
  wire \cnt_high_allowed_clk_reg[0]_i_2_n_3 ;
  wire \cnt_high_allowed_clk_reg[0]_i_2_n_4 ;
  wire \cnt_high_allowed_clk_reg[0]_i_2_n_5 ;
  wire \cnt_high_allowed_clk_reg[0]_i_2_n_6 ;
  wire \cnt_high_allowed_clk_reg[0]_i_2_n_7 ;
  wire \cnt_high_allowed_clk_reg[12]_i_1_n_1 ;
  wire \cnt_high_allowed_clk_reg[12]_i_1_n_2 ;
  wire \cnt_high_allowed_clk_reg[12]_i_1_n_3 ;
  wire \cnt_high_allowed_clk_reg[12]_i_1_n_4 ;
  wire \cnt_high_allowed_clk_reg[12]_i_1_n_5 ;
  wire \cnt_high_allowed_clk_reg[12]_i_1_n_6 ;
  wire \cnt_high_allowed_clk_reg[12]_i_1_n_7 ;
  wire \cnt_high_allowed_clk_reg[4]_i_1_n_0 ;
  wire \cnt_high_allowed_clk_reg[4]_i_1_n_1 ;
  wire \cnt_high_allowed_clk_reg[4]_i_1_n_2 ;
  wire \cnt_high_allowed_clk_reg[4]_i_1_n_3 ;
  wire \cnt_high_allowed_clk_reg[4]_i_1_n_4 ;
  wire \cnt_high_allowed_clk_reg[4]_i_1_n_5 ;
  wire \cnt_high_allowed_clk_reg[4]_i_1_n_6 ;
  wire \cnt_high_allowed_clk_reg[4]_i_1_n_7 ;
  wire \cnt_high_allowed_clk_reg[8]_i_1_n_0 ;
  wire \cnt_high_allowed_clk_reg[8]_i_1_n_1 ;
  wire \cnt_high_allowed_clk_reg[8]_i_1_n_2 ;
  wire \cnt_high_allowed_clk_reg[8]_i_1_n_3 ;
  wire \cnt_high_allowed_clk_reg[8]_i_1_n_4 ;
  wire \cnt_high_allowed_clk_reg[8]_i_1_n_5 ;
  wire \cnt_high_allowed_clk_reg[8]_i_1_n_6 ;
  wire \cnt_high_allowed_clk_reg[8]_i_1_n_7 ;
  wire fft_azimut8_r;
  wire \fft_azimut8_r[15]_i_1_n_0 ;
  wire \fft_azimut8_r[15]_i_4_n_0 ;
  wire \fft_azimut8_r[15]_i_5_n_0 ;
  wire \fft_azimut8_r[15]_i_6_n_0 ;
  wire \fft_azimut8_r[3]_i_2_n_0 ;
  wire \fft_azimut8_r_reg[11]_i_1_n_0 ;
  wire \fft_azimut8_r_reg[11]_i_1_n_1 ;
  wire \fft_azimut8_r_reg[11]_i_1_n_2 ;
  wire \fft_azimut8_r_reg[11]_i_1_n_3 ;
  wire \fft_azimut8_r_reg[11]_i_1_n_4 ;
  wire \fft_azimut8_r_reg[11]_i_1_n_5 ;
  wire \fft_azimut8_r_reg[11]_i_1_n_6 ;
  wire \fft_azimut8_r_reg[11]_i_1_n_7 ;
  wire \fft_azimut8_r_reg[15]_i_3_n_1 ;
  wire \fft_azimut8_r_reg[15]_i_3_n_2 ;
  wire \fft_azimut8_r_reg[15]_i_3_n_3 ;
  wire \fft_azimut8_r_reg[15]_i_3_n_4 ;
  wire \fft_azimut8_r_reg[15]_i_3_n_5 ;
  wire \fft_azimut8_r_reg[15]_i_3_n_6 ;
  wire \fft_azimut8_r_reg[15]_i_3_n_7 ;
  wire \fft_azimut8_r_reg[3]_i_1_n_0 ;
  wire \fft_azimut8_r_reg[3]_i_1_n_1 ;
  wire \fft_azimut8_r_reg[3]_i_1_n_2 ;
  wire \fft_azimut8_r_reg[3]_i_1_n_3 ;
  wire \fft_azimut8_r_reg[3]_i_1_n_4 ;
  wire \fft_azimut8_r_reg[3]_i_1_n_5 ;
  wire \fft_azimut8_r_reg[3]_i_1_n_6 ;
  wire \fft_azimut8_r_reg[3]_i_1_n_7 ;
  wire \fft_azimut8_r_reg[7]_i_1_n_0 ;
  wire \fft_azimut8_r_reg[7]_i_1_n_1 ;
  wire \fft_azimut8_r_reg[7]_i_1_n_2 ;
  wire \fft_azimut8_r_reg[7]_i_1_n_3 ;
  wire \fft_azimut8_r_reg[7]_i_1_n_4 ;
  wire \fft_azimut8_r_reg[7]_i_1_n_5 ;
  wire \fft_azimut8_r_reg[7]_i_1_n_6 ;
  wire \fft_azimut8_r_reg[7]_i_1_n_7 ;
  wire fft_azimut_r;
  wire \fft_azimut_r[0]_i_1_n_0 ;
  wire \fft_azimut_r[1]_i_1_n_0 ;
  wire \fft_azimut_r[2]_i_1_n_0 ;
  wire \fft_azimut_r[3]_i_2_n_0 ;
  wire \fft_azimut_r[3]_i_3_n_0 ;
  wire \fft_azimut_r_reg_n_0_[0] ;
  wire \fft_azimut_r_reg_n_0_[1] ;
  wire \fft_azimut_r_reg_n_0_[2] ;
  wire \fft_azimut_r_reg_n_0_[3] ;
  wire m00_axis_aclk;
  wire m00_axis_aresetn;
  wire [15:0]m00_axis_tdata;
  wire \m00_axis_tdata_r[15]_i_2_n_0 ;
  wire m00_axis_tlast;
  wire m00_axis_tvalid;
  wire [15:15]p_0_in;
  wire [15:0]p_2_in;
  wire s00_axis_tlast;
  wire s00_axis_tvalid;
  wire [3:2]NLW_adr0_carry__2_CO_UNCONNECTED;
  wire [3:3]NLW_adr0_carry__2_O_UNCONNECTED;
  wire [3:3]\NLW_cnt_high_allowed_clk_reg[12]_i_1_CO_UNCONNECTED ;
  wire [3:3]\NLW_fft_azimut8_r_reg[15]_i_3_CO_UNCONNECTED ;

  CARRY4 adr0_carry
       (.CI(1'b0),
        .CO({adr0_carry_n_0,adr0_carry_n_1,adr0_carry_n_2,adr0_carry_n_3}),
        .CYINIT(adr[0]),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({adr0_carry_n_4,adr0_carry_n_5,adr0_carry_n_6,adr0_carry_n_7}),
        .S(adr[4:1]));
  CARRY4 adr0_carry__0
       (.CI(adr0_carry_n_0),
        .CO({adr0_carry__0_n_0,adr0_carry__0_n_1,adr0_carry__0_n_2,adr0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({adr0_carry__0_n_4,adr0_carry__0_n_5,adr0_carry__0_n_6,adr0_carry__0_n_7}),
        .S(adr[8:5]));
  CARRY4 adr0_carry__1
       (.CI(adr0_carry__0_n_0),
        .CO({adr0_carry__1_n_0,adr0_carry__1_n_1,adr0_carry__1_n_2,adr0_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({adr0_carry__1_n_4,adr0_carry__1_n_5,adr0_carry__1_n_6,adr0_carry__1_n_7}),
        .S(adr[12:9]));
  CARRY4 adr0_carry__2
       (.CI(adr0_carry__1_n_0),
        .CO({NLW_adr0_carry__2_CO_UNCONNECTED[3:2],adr0_carry__2_n_2,adr0_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_adr0_carry__2_O_UNCONNECTED[3],adr0_carry__2_n_5,adr0_carry__2_n_6,adr0_carry__2_n_7}),
        .S({1'b0,adr[15:13]}));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \adr[0]_i_1 
       (.I0(adr[0]),
        .O(adr_0[0]));
  LUT5 #(
    .INIT(32'hFFFB0000)) 
    \adr[10]_i_1 
       (.I0(\adr[15]_i_3_n_0 ),
        .I1(\adr[15]_i_4_n_0 ),
        .I2(\adr[15]_i_5_n_0 ),
        .I3(\adr[15]_i_6_n_0 ),
        .I4(adr0_carry__1_n_6),
        .O(adr_0[10]));
  LUT5 #(
    .INIT(32'hFFFB0000)) 
    \adr[11]_i_1 
       (.I0(\adr[15]_i_3_n_0 ),
        .I1(\adr[15]_i_4_n_0 ),
        .I2(\adr[15]_i_5_n_0 ),
        .I3(\adr[15]_i_6_n_0 ),
        .I4(adr0_carry__1_n_5),
        .O(adr_0[11]));
  LUT5 #(
    .INIT(32'hFFFB0000)) 
    \adr[12]_i_1 
       (.I0(\adr[15]_i_3_n_0 ),
        .I1(\adr[15]_i_4_n_0 ),
        .I2(\adr[15]_i_5_n_0 ),
        .I3(\adr[15]_i_6_n_0 ),
        .I4(adr0_carry__1_n_4),
        .O(adr_0[12]));
  LUT5 #(
    .INIT(32'hFFFB0000)) 
    \adr[13]_i_1 
       (.I0(\adr[15]_i_3_n_0 ),
        .I1(\adr[15]_i_4_n_0 ),
        .I2(\adr[15]_i_5_n_0 ),
        .I3(\adr[15]_i_6_n_0 ),
        .I4(adr0_carry__2_n_7),
        .O(adr_0[13]));
  LUT5 #(
    .INIT(32'hFFFB0000)) 
    \adr[14]_i_1 
       (.I0(\adr[15]_i_3_n_0 ),
        .I1(\adr[15]_i_4_n_0 ),
        .I2(\adr[15]_i_5_n_0 ),
        .I3(\adr[15]_i_6_n_0 ),
        .I4(adr0_carry__2_n_6),
        .O(adr_0[14]));
  LUT2 #(
    .INIT(4'h7)) 
    \adr[15]_i_1 
       (.I0(s00_axis_tvalid),
        .I1(m00_axis_aresetn),
        .O(\adr[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFFB0000)) 
    \adr[15]_i_2 
       (.I0(\adr[15]_i_3_n_0 ),
        .I1(\adr[15]_i_4_n_0 ),
        .I2(\adr[15]_i_5_n_0 ),
        .I3(\adr[15]_i_6_n_0 ),
        .I4(adr0_carry__2_n_5),
        .O(adr_0[15]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'h7FFF)) 
    \adr[15]_i_3 
       (.I0(adr[2]),
        .I1(adr[0]),
        .I2(adr[8]),
        .I3(adr[1]),
        .O(\adr[15]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h8000)) 
    \adr[15]_i_4 
       (.I0(adr[6]),
        .I1(adr[5]),
        .I2(adr[7]),
        .I3(adr[3]),
        .O(\adr[15]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hFFFD)) 
    \adr[15]_i_5 
       (.I0(adr[12]),
        .I1(adr[15]),
        .I2(adr[13]),
        .I3(adr[14]),
        .O(\adr[15]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'h7FFF)) 
    \adr[15]_i_6 
       (.I0(adr[9]),
        .I1(adr[4]),
        .I2(adr[10]),
        .I3(adr[11]),
        .O(\adr[15]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'hFFFB0000)) 
    \adr[1]_i_1 
       (.I0(\adr[15]_i_3_n_0 ),
        .I1(\adr[15]_i_4_n_0 ),
        .I2(\adr[15]_i_5_n_0 ),
        .I3(\adr[15]_i_6_n_0 ),
        .I4(adr0_carry_n_7),
        .O(adr_0[1]));
  LUT5 #(
    .INIT(32'hFFFB0000)) 
    \adr[2]_i_1 
       (.I0(\adr[15]_i_3_n_0 ),
        .I1(\adr[15]_i_4_n_0 ),
        .I2(\adr[15]_i_5_n_0 ),
        .I3(\adr[15]_i_6_n_0 ),
        .I4(adr0_carry_n_6),
        .O(adr_0[2]));
  LUT5 #(
    .INIT(32'hFFFB0000)) 
    \adr[3]_i_1 
       (.I0(\adr[15]_i_3_n_0 ),
        .I1(\adr[15]_i_4_n_0 ),
        .I2(\adr[15]_i_5_n_0 ),
        .I3(\adr[15]_i_6_n_0 ),
        .I4(adr0_carry_n_5),
        .O(adr_0[3]));
  LUT5 #(
    .INIT(32'hFFFB0000)) 
    \adr[4]_i_1 
       (.I0(\adr[15]_i_3_n_0 ),
        .I1(\adr[15]_i_4_n_0 ),
        .I2(\adr[15]_i_5_n_0 ),
        .I3(\adr[15]_i_6_n_0 ),
        .I4(adr0_carry_n_4),
        .O(adr_0[4]));
  LUT5 #(
    .INIT(32'hFFFB0000)) 
    \adr[5]_i_1 
       (.I0(\adr[15]_i_3_n_0 ),
        .I1(\adr[15]_i_4_n_0 ),
        .I2(\adr[15]_i_5_n_0 ),
        .I3(\adr[15]_i_6_n_0 ),
        .I4(adr0_carry__0_n_7),
        .O(adr_0[5]));
  LUT5 #(
    .INIT(32'hFFFB0000)) 
    \adr[6]_i_1 
       (.I0(\adr[15]_i_3_n_0 ),
        .I1(\adr[15]_i_4_n_0 ),
        .I2(\adr[15]_i_5_n_0 ),
        .I3(\adr[15]_i_6_n_0 ),
        .I4(adr0_carry__0_n_6),
        .O(adr_0[6]));
  LUT5 #(
    .INIT(32'hFFFB0000)) 
    \adr[7]_i_1 
       (.I0(\adr[15]_i_3_n_0 ),
        .I1(\adr[15]_i_4_n_0 ),
        .I2(\adr[15]_i_5_n_0 ),
        .I3(\adr[15]_i_6_n_0 ),
        .I4(adr0_carry__0_n_5),
        .O(adr_0[7]));
  LUT5 #(
    .INIT(32'hFFFB0000)) 
    \adr[8]_i_1 
       (.I0(\adr[15]_i_3_n_0 ),
        .I1(\adr[15]_i_4_n_0 ),
        .I2(\adr[15]_i_5_n_0 ),
        .I3(\adr[15]_i_6_n_0 ),
        .I4(adr0_carry__0_n_4),
        .O(adr_0[8]));
  LUT5 #(
    .INIT(32'hFFFB0000)) 
    \adr[9]_i_1 
       (.I0(\adr[15]_i_3_n_0 ),
        .I1(\adr[15]_i_4_n_0 ),
        .I2(\adr[15]_i_5_n_0 ),
        .I3(\adr[15]_i_6_n_0 ),
        .I4(adr0_carry__1_n_7),
        .O(adr_0[9]));
  FDRE \adr_reg[0] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(adr_0[0]),
        .Q(adr[0]),
        .R(\adr[15]_i_1_n_0 ));
  FDRE \adr_reg[10] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(adr_0[10]),
        .Q(adr[10]),
        .R(\adr[15]_i_1_n_0 ));
  FDRE \adr_reg[11] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(adr_0[11]),
        .Q(adr[11]),
        .R(\adr[15]_i_1_n_0 ));
  FDRE \adr_reg[12] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(adr_0[12]),
        .Q(adr[12]),
        .R(\adr[15]_i_1_n_0 ));
  FDRE \adr_reg[13] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(adr_0[13]),
        .Q(adr[13]),
        .R(\adr[15]_i_1_n_0 ));
  FDRE \adr_reg[14] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(adr_0[14]),
        .Q(adr[14]),
        .R(\adr[15]_i_1_n_0 ));
  FDRE \adr_reg[15] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(adr_0[15]),
        .Q(adr[15]),
        .R(\adr[15]_i_1_n_0 ));
  FDRE \adr_reg[1] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(adr_0[1]),
        .Q(adr[1]),
        .R(\adr[15]_i_1_n_0 ));
  FDRE \adr_reg[2] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(adr_0[2]),
        .Q(adr[2]),
        .R(\adr[15]_i_1_n_0 ));
  FDRE \adr_reg[3] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(adr_0[3]),
        .Q(adr[3]),
        .R(\adr[15]_i_1_n_0 ));
  FDRE \adr_reg[4] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(adr_0[4]),
        .Q(adr[4]),
        .R(\adr[15]_i_1_n_0 ));
  FDRE \adr_reg[5] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(adr_0[5]),
        .Q(adr[5]),
        .R(\adr[15]_i_1_n_0 ));
  FDRE \adr_reg[6] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(adr_0[6]),
        .Q(adr[6]),
        .R(\adr[15]_i_1_n_0 ));
  FDRE \adr_reg[7] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(adr_0[7]),
        .Q(adr[7]),
        .R(\adr[15]_i_1_n_0 ));
  FDRE \adr_reg[8] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(adr_0[8]),
        .Q(adr[8]),
        .R(\adr[15]_i_1_n_0 ));
  FDRE \adr_reg[9] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(adr_0[9]),
        .Q(adr[9]),
        .R(\adr[15]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'hDF)) 
    \cnt_high_allowed_clk[0]_i_1 
       (.I0(m00_axis_aresetn),
        .I1(azimuth_0),
        .I2(allowed_clk),
        .O(\cnt_high_allowed_clk[0]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \cnt_high_allowed_clk[0]_i_3 
       (.I0(cnt_high_allowed_clk_reg[0]),
        .O(\cnt_high_allowed_clk[0]_i_3_n_0 ));
  FDRE \cnt_high_allowed_clk_reg[0] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_high_allowed_clk_reg[0]_i_2_n_7 ),
        .Q(cnt_high_allowed_clk_reg[0]),
        .R(\cnt_high_allowed_clk[0]_i_1_n_0 ));
  CARRY4 \cnt_high_allowed_clk_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\cnt_high_allowed_clk_reg[0]_i_2_n_0 ,\cnt_high_allowed_clk_reg[0]_i_2_n_1 ,\cnt_high_allowed_clk_reg[0]_i_2_n_2 ,\cnt_high_allowed_clk_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\cnt_high_allowed_clk_reg[0]_i_2_n_4 ,\cnt_high_allowed_clk_reg[0]_i_2_n_5 ,\cnt_high_allowed_clk_reg[0]_i_2_n_6 ,\cnt_high_allowed_clk_reg[0]_i_2_n_7 }),
        .S({cnt_high_allowed_clk_reg[3:1],\cnt_high_allowed_clk[0]_i_3_n_0 }));
  FDRE \cnt_high_allowed_clk_reg[10] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_high_allowed_clk_reg[8]_i_1_n_5 ),
        .Q(cnt_high_allowed_clk_reg[10]),
        .R(\cnt_high_allowed_clk[0]_i_1_n_0 ));
  FDRE \cnt_high_allowed_clk_reg[11] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_high_allowed_clk_reg[8]_i_1_n_4 ),
        .Q(cnt_high_allowed_clk_reg[11]),
        .R(\cnt_high_allowed_clk[0]_i_1_n_0 ));
  FDRE \cnt_high_allowed_clk_reg[12] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_high_allowed_clk_reg[12]_i_1_n_7 ),
        .Q(cnt_high_allowed_clk_reg[12]),
        .R(\cnt_high_allowed_clk[0]_i_1_n_0 ));
  CARRY4 \cnt_high_allowed_clk_reg[12]_i_1 
       (.CI(\cnt_high_allowed_clk_reg[8]_i_1_n_0 ),
        .CO({\NLW_cnt_high_allowed_clk_reg[12]_i_1_CO_UNCONNECTED [3],\cnt_high_allowed_clk_reg[12]_i_1_n_1 ,\cnt_high_allowed_clk_reg[12]_i_1_n_2 ,\cnt_high_allowed_clk_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_high_allowed_clk_reg[12]_i_1_n_4 ,\cnt_high_allowed_clk_reg[12]_i_1_n_5 ,\cnt_high_allowed_clk_reg[12]_i_1_n_6 ,\cnt_high_allowed_clk_reg[12]_i_1_n_7 }),
        .S(cnt_high_allowed_clk_reg[15:12]));
  FDRE \cnt_high_allowed_clk_reg[13] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_high_allowed_clk_reg[12]_i_1_n_6 ),
        .Q(cnt_high_allowed_clk_reg[13]),
        .R(\cnt_high_allowed_clk[0]_i_1_n_0 ));
  FDRE \cnt_high_allowed_clk_reg[14] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_high_allowed_clk_reg[12]_i_1_n_5 ),
        .Q(cnt_high_allowed_clk_reg[14]),
        .R(\cnt_high_allowed_clk[0]_i_1_n_0 ));
  FDRE \cnt_high_allowed_clk_reg[15] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_high_allowed_clk_reg[12]_i_1_n_4 ),
        .Q(cnt_high_allowed_clk_reg[15]),
        .R(\cnt_high_allowed_clk[0]_i_1_n_0 ));
  FDRE \cnt_high_allowed_clk_reg[1] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_high_allowed_clk_reg[0]_i_2_n_6 ),
        .Q(cnt_high_allowed_clk_reg[1]),
        .R(\cnt_high_allowed_clk[0]_i_1_n_0 ));
  FDRE \cnt_high_allowed_clk_reg[2] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_high_allowed_clk_reg[0]_i_2_n_5 ),
        .Q(cnt_high_allowed_clk_reg[2]),
        .R(\cnt_high_allowed_clk[0]_i_1_n_0 ));
  FDRE \cnt_high_allowed_clk_reg[3] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_high_allowed_clk_reg[0]_i_2_n_4 ),
        .Q(cnt_high_allowed_clk_reg[3]),
        .R(\cnt_high_allowed_clk[0]_i_1_n_0 ));
  FDRE \cnt_high_allowed_clk_reg[4] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_high_allowed_clk_reg[4]_i_1_n_7 ),
        .Q(cnt_high_allowed_clk_reg[4]),
        .R(\cnt_high_allowed_clk[0]_i_1_n_0 ));
  CARRY4 \cnt_high_allowed_clk_reg[4]_i_1 
       (.CI(\cnt_high_allowed_clk_reg[0]_i_2_n_0 ),
        .CO({\cnt_high_allowed_clk_reg[4]_i_1_n_0 ,\cnt_high_allowed_clk_reg[4]_i_1_n_1 ,\cnt_high_allowed_clk_reg[4]_i_1_n_2 ,\cnt_high_allowed_clk_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_high_allowed_clk_reg[4]_i_1_n_4 ,\cnt_high_allowed_clk_reg[4]_i_1_n_5 ,\cnt_high_allowed_clk_reg[4]_i_1_n_6 ,\cnt_high_allowed_clk_reg[4]_i_1_n_7 }),
        .S(cnt_high_allowed_clk_reg[7:4]));
  FDRE \cnt_high_allowed_clk_reg[5] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_high_allowed_clk_reg[4]_i_1_n_6 ),
        .Q(cnt_high_allowed_clk_reg[5]),
        .R(\cnt_high_allowed_clk[0]_i_1_n_0 ));
  FDRE \cnt_high_allowed_clk_reg[6] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_high_allowed_clk_reg[4]_i_1_n_5 ),
        .Q(cnt_high_allowed_clk_reg[6]),
        .R(\cnt_high_allowed_clk[0]_i_1_n_0 ));
  FDRE \cnt_high_allowed_clk_reg[7] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_high_allowed_clk_reg[4]_i_1_n_4 ),
        .Q(cnt_high_allowed_clk_reg[7]),
        .R(\cnt_high_allowed_clk[0]_i_1_n_0 ));
  FDRE \cnt_high_allowed_clk_reg[8] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_high_allowed_clk_reg[8]_i_1_n_7 ),
        .Q(cnt_high_allowed_clk_reg[8]),
        .R(\cnt_high_allowed_clk[0]_i_1_n_0 ));
  CARRY4 \cnt_high_allowed_clk_reg[8]_i_1 
       (.CI(\cnt_high_allowed_clk_reg[4]_i_1_n_0 ),
        .CO({\cnt_high_allowed_clk_reg[8]_i_1_n_0 ,\cnt_high_allowed_clk_reg[8]_i_1_n_1 ,\cnt_high_allowed_clk_reg[8]_i_1_n_2 ,\cnt_high_allowed_clk_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_high_allowed_clk_reg[8]_i_1_n_4 ,\cnt_high_allowed_clk_reg[8]_i_1_n_5 ,\cnt_high_allowed_clk_reg[8]_i_1_n_6 ,\cnt_high_allowed_clk_reg[8]_i_1_n_7 }),
        .S(cnt_high_allowed_clk_reg[11:8]));
  FDRE \cnt_high_allowed_clk_reg[9] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_high_allowed_clk_reg[8]_i_1_n_6 ),
        .Q(cnt_high_allowed_clk_reg[9]),
        .R(\cnt_high_allowed_clk[0]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'hB)) 
    \fft_azimut8_r[15]_i_1 
       (.I0(azimuth_0),
        .I1(m00_axis_aresetn),
        .O(\fft_azimut8_r[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000400000000)) 
    \fft_azimut8_r[15]_i_2 
       (.I0(\fft_azimut8_r[15]_i_4_n_0 ),
        .I1(\fft_azimut8_r[15]_i_5_n_0 ),
        .I2(\fft_azimut_r_reg_n_0_[2] ),
        .I3(\fft_azimut_r_reg_n_0_[1] ),
        .I4(\fft_azimut_r_reg_n_0_[0] ),
        .I5(\fft_azimut_r_reg_n_0_[3] ),
        .O(fft_azimut8_r));
  LUT5 #(
    .INIT(32'hFFFFFEFF)) 
    \fft_azimut8_r[15]_i_4 
       (.I0(cnt_high_allowed_clk_reg[2]),
        .I1(cnt_high_allowed_clk_reg[12]),
        .I2(cnt_high_allowed_clk_reg[0]),
        .I3(cnt_high_allowed_clk_reg[8]),
        .I4(\fft_azimut8_r[15]_i_6_n_0 ),
        .O(\fft_azimut8_r[15]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h00000008)) 
    \fft_azimut8_r[15]_i_5 
       (.I0(cnt_high_allowed_clk_reg[5]),
        .I1(cnt_high_allowed_clk_reg[7]),
        .I2(cnt_high_allowed_clk_reg[1]),
        .I3(cnt_high_allowed_clk_reg[15]),
        .I4(\fft_azimut_r[3]_i_3_n_0 ),
        .O(\fft_azimut8_r[15]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'hFFDF)) 
    \fft_azimut8_r[15]_i_6 
       (.I0(cnt_high_allowed_clk_reg[6]),
        .I1(cnt_high_allowed_clk_reg[4]),
        .I2(cnt_high_allowed_clk_reg[3]),
        .I3(cnt_high_allowed_clk_reg[10]),
        .O(\fft_azimut8_r[15]_i_6_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \fft_azimut8_r[3]_i_2 
       (.I0(azimut8[0]),
        .O(\fft_azimut8_r[3]_i_2_n_0 ));
  FDRE \fft_azimut8_r_reg[0] 
       (.C(clk_10MHz),
        .CE(fft_azimut8_r),
        .D(\fft_azimut8_r_reg[3]_i_1_n_7 ),
        .Q(azimut8[0]),
        .R(\fft_azimut8_r[15]_i_1_n_0 ));
  FDRE \fft_azimut8_r_reg[10] 
       (.C(clk_10MHz),
        .CE(fft_azimut8_r),
        .D(\fft_azimut8_r_reg[11]_i_1_n_5 ),
        .Q(azimut8[10]),
        .R(\fft_azimut8_r[15]_i_1_n_0 ));
  FDRE \fft_azimut8_r_reg[11] 
       (.C(clk_10MHz),
        .CE(fft_azimut8_r),
        .D(\fft_azimut8_r_reg[11]_i_1_n_4 ),
        .Q(azimut8[11]),
        .R(\fft_azimut8_r[15]_i_1_n_0 ));
  CARRY4 \fft_azimut8_r_reg[11]_i_1 
       (.CI(\fft_azimut8_r_reg[7]_i_1_n_0 ),
        .CO({\fft_azimut8_r_reg[11]_i_1_n_0 ,\fft_azimut8_r_reg[11]_i_1_n_1 ,\fft_azimut8_r_reg[11]_i_1_n_2 ,\fft_azimut8_r_reg[11]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\fft_azimut8_r_reg[11]_i_1_n_4 ,\fft_azimut8_r_reg[11]_i_1_n_5 ,\fft_azimut8_r_reg[11]_i_1_n_6 ,\fft_azimut8_r_reg[11]_i_1_n_7 }),
        .S(azimut8[11:8]));
  FDRE \fft_azimut8_r_reg[12] 
       (.C(clk_10MHz),
        .CE(fft_azimut8_r),
        .D(\fft_azimut8_r_reg[15]_i_3_n_7 ),
        .Q(azimut8[12]),
        .R(\fft_azimut8_r[15]_i_1_n_0 ));
  FDRE \fft_azimut8_r_reg[13] 
       (.C(clk_10MHz),
        .CE(fft_azimut8_r),
        .D(\fft_azimut8_r_reg[15]_i_3_n_6 ),
        .Q(azimut8[13]),
        .R(\fft_azimut8_r[15]_i_1_n_0 ));
  FDRE \fft_azimut8_r_reg[14] 
       (.C(clk_10MHz),
        .CE(fft_azimut8_r),
        .D(\fft_azimut8_r_reg[15]_i_3_n_5 ),
        .Q(azimut8[14]),
        .R(\fft_azimut8_r[15]_i_1_n_0 ));
  FDRE \fft_azimut8_r_reg[15] 
       (.C(clk_10MHz),
        .CE(fft_azimut8_r),
        .D(\fft_azimut8_r_reg[15]_i_3_n_4 ),
        .Q(azimut8[15]),
        .R(\fft_azimut8_r[15]_i_1_n_0 ));
  CARRY4 \fft_azimut8_r_reg[15]_i_3 
       (.CI(\fft_azimut8_r_reg[11]_i_1_n_0 ),
        .CO({\NLW_fft_azimut8_r_reg[15]_i_3_CO_UNCONNECTED [3],\fft_azimut8_r_reg[15]_i_3_n_1 ,\fft_azimut8_r_reg[15]_i_3_n_2 ,\fft_azimut8_r_reg[15]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\fft_azimut8_r_reg[15]_i_3_n_4 ,\fft_azimut8_r_reg[15]_i_3_n_5 ,\fft_azimut8_r_reg[15]_i_3_n_6 ,\fft_azimut8_r_reg[15]_i_3_n_7 }),
        .S(azimut8[15:12]));
  FDRE \fft_azimut8_r_reg[1] 
       (.C(clk_10MHz),
        .CE(fft_azimut8_r),
        .D(\fft_azimut8_r_reg[3]_i_1_n_6 ),
        .Q(azimut8[1]),
        .R(\fft_azimut8_r[15]_i_1_n_0 ));
  FDRE \fft_azimut8_r_reg[2] 
       (.C(clk_10MHz),
        .CE(fft_azimut8_r),
        .D(\fft_azimut8_r_reg[3]_i_1_n_5 ),
        .Q(azimut8[2]),
        .R(\fft_azimut8_r[15]_i_1_n_0 ));
  FDRE \fft_azimut8_r_reg[3] 
       (.C(clk_10MHz),
        .CE(fft_azimut8_r),
        .D(\fft_azimut8_r_reg[3]_i_1_n_4 ),
        .Q(azimut8[3]),
        .R(\fft_azimut8_r[15]_i_1_n_0 ));
  CARRY4 \fft_azimut8_r_reg[3]_i_1 
       (.CI(1'b0),
        .CO({\fft_azimut8_r_reg[3]_i_1_n_0 ,\fft_azimut8_r_reg[3]_i_1_n_1 ,\fft_azimut8_r_reg[3]_i_1_n_2 ,\fft_azimut8_r_reg[3]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\fft_azimut8_r_reg[3]_i_1_n_4 ,\fft_azimut8_r_reg[3]_i_1_n_5 ,\fft_azimut8_r_reg[3]_i_1_n_6 ,\fft_azimut8_r_reg[3]_i_1_n_7 }),
        .S({azimut8[3:1],\fft_azimut8_r[3]_i_2_n_0 }));
  FDRE \fft_azimut8_r_reg[4] 
       (.C(clk_10MHz),
        .CE(fft_azimut8_r),
        .D(\fft_azimut8_r_reg[7]_i_1_n_7 ),
        .Q(azimut8[4]),
        .R(\fft_azimut8_r[15]_i_1_n_0 ));
  FDRE \fft_azimut8_r_reg[5] 
       (.C(clk_10MHz),
        .CE(fft_azimut8_r),
        .D(\fft_azimut8_r_reg[7]_i_1_n_6 ),
        .Q(azimut8[5]),
        .R(\fft_azimut8_r[15]_i_1_n_0 ));
  FDRE \fft_azimut8_r_reg[6] 
       (.C(clk_10MHz),
        .CE(fft_azimut8_r),
        .D(\fft_azimut8_r_reg[7]_i_1_n_5 ),
        .Q(azimut8[6]),
        .R(\fft_azimut8_r[15]_i_1_n_0 ));
  FDRE \fft_azimut8_r_reg[7] 
       (.C(clk_10MHz),
        .CE(fft_azimut8_r),
        .D(\fft_azimut8_r_reg[7]_i_1_n_4 ),
        .Q(azimut8[7]),
        .R(\fft_azimut8_r[15]_i_1_n_0 ));
  CARRY4 \fft_azimut8_r_reg[7]_i_1 
       (.CI(\fft_azimut8_r_reg[3]_i_1_n_0 ),
        .CO({\fft_azimut8_r_reg[7]_i_1_n_0 ,\fft_azimut8_r_reg[7]_i_1_n_1 ,\fft_azimut8_r_reg[7]_i_1_n_2 ,\fft_azimut8_r_reg[7]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\fft_azimut8_r_reg[7]_i_1_n_4 ,\fft_azimut8_r_reg[7]_i_1_n_5 ,\fft_azimut8_r_reg[7]_i_1_n_6 ,\fft_azimut8_r_reg[7]_i_1_n_7 }),
        .S(azimut8[7:4]));
  FDRE \fft_azimut8_r_reg[8] 
       (.C(clk_10MHz),
        .CE(fft_azimut8_r),
        .D(\fft_azimut8_r_reg[11]_i_1_n_7 ),
        .Q(azimut8[8]),
        .R(\fft_azimut8_r[15]_i_1_n_0 ));
  FDRE \fft_azimut8_r_reg[9] 
       (.C(clk_10MHz),
        .CE(fft_azimut8_r),
        .D(\fft_azimut8_r_reg[11]_i_1_n_6 ),
        .Q(azimut8[9]),
        .R(\fft_azimut8_r[15]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'h00EF)) 
    \fft_azimut_r[0]_i_1 
       (.I0(\fft_azimut_r_reg_n_0_[2] ),
        .I1(\fft_azimut_r_reg_n_0_[1] ),
        .I2(\fft_azimut_r_reg_n_0_[3] ),
        .I3(\fft_azimut_r_reg_n_0_[0] ),
        .O(\fft_azimut_r[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \fft_azimut_r[1]_i_1 
       (.I0(\fft_azimut_r_reg_n_0_[1] ),
        .I1(\fft_azimut_r_reg_n_0_[0] ),
        .O(\fft_azimut_r[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \fft_azimut_r[2]_i_1 
       (.I0(\fft_azimut_r_reg_n_0_[2] ),
        .I1(\fft_azimut_r_reg_n_0_[0] ),
        .I2(\fft_azimut_r_reg_n_0_[1] ),
        .O(\fft_azimut_r[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000001000000)) 
    \fft_azimut_r[3]_i_1 
       (.I0(\fft_azimut_r[3]_i_3_n_0 ),
        .I1(cnt_high_allowed_clk_reg[15]),
        .I2(cnt_high_allowed_clk_reg[1]),
        .I3(cnt_high_allowed_clk_reg[7]),
        .I4(cnt_high_allowed_clk_reg[5]),
        .I5(\fft_azimut8_r[15]_i_4_n_0 ),
        .O(fft_azimut_r));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h7E80)) 
    \fft_azimut_r[3]_i_2 
       (.I0(\fft_azimut_r_reg_n_0_[2] ),
        .I1(\fft_azimut_r_reg_n_0_[0] ),
        .I2(\fft_azimut_r_reg_n_0_[1] ),
        .I3(\fft_azimut_r_reg_n_0_[3] ),
        .O(\fft_azimut_r[3]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hFFFD)) 
    \fft_azimut_r[3]_i_3 
       (.I0(cnt_high_allowed_clk_reg[9]),
        .I1(cnt_high_allowed_clk_reg[13]),
        .I2(cnt_high_allowed_clk_reg[14]),
        .I3(cnt_high_allowed_clk_reg[11]),
        .O(\fft_azimut_r[3]_i_3_n_0 ));
  FDSE \fft_azimut_r_reg[0] 
       (.C(clk_10MHz),
        .CE(fft_azimut_r),
        .D(\fft_azimut_r[0]_i_1_n_0 ),
        .Q(\fft_azimut_r_reg_n_0_[0] ),
        .S(\fft_azimut8_r[15]_i_1_n_0 ));
  FDSE \fft_azimut_r_reg[1] 
       (.C(clk_10MHz),
        .CE(fft_azimut_r),
        .D(\fft_azimut_r[1]_i_1_n_0 ),
        .Q(\fft_azimut_r_reg_n_0_[1] ),
        .S(\fft_azimut8_r[15]_i_1_n_0 ));
  FDSE \fft_azimut_r_reg[2] 
       (.C(clk_10MHz),
        .CE(fft_azimut_r),
        .D(\fft_azimut_r[2]_i_1_n_0 ),
        .Q(\fft_azimut_r_reg_n_0_[2] ),
        .S(\fft_azimut8_r[15]_i_1_n_0 ));
  FDSE \fft_azimut_r_reg[3] 
       (.C(clk_10MHz),
        .CE(fft_azimut_r),
        .D(\fft_azimut_r[3]_i_2_n_0 ),
        .Q(\fft_azimut_r_reg_n_0_[3] ),
        .S(\fft_azimut8_r[15]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \m00_axis_tdata_r[0]_i_1 
       (.I0(adr[0]),
        .I1(m00_axis_aresetn),
        .I2(s00_axis_tvalid),
        .O(p_2_in[0]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \m00_axis_tdata_r[10]_i_1 
       (.I0(adr[10]),
        .I1(m00_axis_aresetn),
        .I2(s00_axis_tvalid),
        .O(p_2_in[10]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \m00_axis_tdata_r[11]_i_1 
       (.I0(adr[11]),
        .I1(m00_axis_aresetn),
        .I2(s00_axis_tvalid),
        .O(p_2_in[11]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \m00_axis_tdata_r[12]_i_1 
       (.I0(adr[12]),
        .I1(m00_axis_aresetn),
        .I2(s00_axis_tvalid),
        .O(p_2_in[12]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \m00_axis_tdata_r[13]_i_1 
       (.I0(adr[13]),
        .I1(m00_axis_aresetn),
        .I2(s00_axis_tvalid),
        .O(p_2_in[13]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \m00_axis_tdata_r[14]_i_1 
       (.I0(adr[14]),
        .I1(m00_axis_aresetn),
        .I2(s00_axis_tvalid),
        .O(p_2_in[14]));
  LUT6 #(
    .INIT(64'hFEFF000000000000)) 
    \m00_axis_tdata_r[15]_i_1 
       (.I0(\fft_azimut_r_reg_n_0_[2] ),
        .I1(\fft_azimut_r_reg_n_0_[1] ),
        .I2(\fft_azimut_r_reg_n_0_[0] ),
        .I3(\fft_azimut_r_reg_n_0_[3] ),
        .I4(m00_axis_aresetn),
        .I5(s00_axis_tvalid),
        .O(p_0_in));
  LUT2 #(
    .INIT(4'hB)) 
    \m00_axis_tdata_r[15]_i_2 
       (.I0(s00_axis_tvalid),
        .I1(m00_axis_aresetn),
        .O(\m00_axis_tdata_r[15]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \m00_axis_tdata_r[15]_i_3 
       (.I0(adr[15]),
        .I1(m00_axis_aresetn),
        .I2(s00_axis_tvalid),
        .O(p_2_in[15]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \m00_axis_tdata_r[1]_i_1 
       (.I0(adr[1]),
        .I1(m00_axis_aresetn),
        .I2(s00_axis_tvalid),
        .O(p_2_in[1]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \m00_axis_tdata_r[2]_i_1 
       (.I0(adr[2]),
        .I1(m00_axis_aresetn),
        .I2(s00_axis_tvalid),
        .O(p_2_in[2]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \m00_axis_tdata_r[3]_i_1 
       (.I0(adr[3]),
        .I1(m00_axis_aresetn),
        .I2(s00_axis_tvalid),
        .O(p_2_in[3]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \m00_axis_tdata_r[4]_i_1 
       (.I0(adr[4]),
        .I1(m00_axis_aresetn),
        .I2(s00_axis_tvalid),
        .O(p_2_in[4]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \m00_axis_tdata_r[5]_i_1 
       (.I0(adr[5]),
        .I1(m00_axis_aresetn),
        .I2(s00_axis_tvalid),
        .O(p_2_in[5]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \m00_axis_tdata_r[6]_i_1 
       (.I0(adr[6]),
        .I1(m00_axis_aresetn),
        .I2(s00_axis_tvalid),
        .O(p_2_in[6]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \m00_axis_tdata_r[7]_i_1 
       (.I0(adr[7]),
        .I1(m00_axis_aresetn),
        .I2(s00_axis_tvalid),
        .O(p_2_in[7]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \m00_axis_tdata_r[8]_i_1 
       (.I0(adr[8]),
        .I1(m00_axis_aresetn),
        .I2(s00_axis_tvalid),
        .O(p_2_in[8]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \m00_axis_tdata_r[9]_i_1 
       (.I0(adr[9]),
        .I1(m00_axis_aresetn),
        .I2(s00_axis_tvalid),
        .O(p_2_in[9]));
  FDRE \m00_axis_tdata_r_reg[0] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[15]_i_2_n_0 ),
        .D(p_2_in[0]),
        .Q(m00_axis_tdata[0]),
        .R(p_0_in));
  FDRE \m00_axis_tdata_r_reg[10] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[15]_i_2_n_0 ),
        .D(p_2_in[10]),
        .Q(m00_axis_tdata[10]),
        .R(p_0_in));
  FDRE \m00_axis_tdata_r_reg[11] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[15]_i_2_n_0 ),
        .D(p_2_in[11]),
        .Q(m00_axis_tdata[11]),
        .R(p_0_in));
  FDRE \m00_axis_tdata_r_reg[12] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[15]_i_2_n_0 ),
        .D(p_2_in[12]),
        .Q(m00_axis_tdata[12]),
        .R(p_0_in));
  FDRE \m00_axis_tdata_r_reg[13] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[15]_i_2_n_0 ),
        .D(p_2_in[13]),
        .Q(m00_axis_tdata[13]),
        .R(p_0_in));
  FDRE \m00_axis_tdata_r_reg[14] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[15]_i_2_n_0 ),
        .D(p_2_in[14]),
        .Q(m00_axis_tdata[14]),
        .R(p_0_in));
  FDRE \m00_axis_tdata_r_reg[15] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[15]_i_2_n_0 ),
        .D(p_2_in[15]),
        .Q(m00_axis_tdata[15]),
        .R(p_0_in));
  FDRE \m00_axis_tdata_r_reg[1] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[15]_i_2_n_0 ),
        .D(p_2_in[1]),
        .Q(m00_axis_tdata[1]),
        .R(p_0_in));
  FDRE \m00_axis_tdata_r_reg[2] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[15]_i_2_n_0 ),
        .D(p_2_in[2]),
        .Q(m00_axis_tdata[2]),
        .R(p_0_in));
  FDRE \m00_axis_tdata_r_reg[3] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[15]_i_2_n_0 ),
        .D(p_2_in[3]),
        .Q(m00_axis_tdata[3]),
        .R(p_0_in));
  FDRE \m00_axis_tdata_r_reg[4] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[15]_i_2_n_0 ),
        .D(p_2_in[4]),
        .Q(m00_axis_tdata[4]),
        .R(p_0_in));
  FDRE \m00_axis_tdata_r_reg[5] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[15]_i_2_n_0 ),
        .D(p_2_in[5]),
        .Q(m00_axis_tdata[5]),
        .R(p_0_in));
  FDRE \m00_axis_tdata_r_reg[6] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[15]_i_2_n_0 ),
        .D(p_2_in[6]),
        .Q(m00_axis_tdata[6]),
        .R(p_0_in));
  FDRE \m00_axis_tdata_r_reg[7] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[15]_i_2_n_0 ),
        .D(p_2_in[7]),
        .Q(m00_axis_tdata[7]),
        .R(p_0_in));
  FDRE \m00_axis_tdata_r_reg[8] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[15]_i_2_n_0 ),
        .D(p_2_in[8]),
        .Q(m00_axis_tdata[8]),
        .R(p_0_in));
  FDRE \m00_axis_tdata_r_reg[9] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[15]_i_2_n_0 ),
        .D(p_2_in[9]),
        .Q(m00_axis_tdata[9]),
        .R(p_0_in));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h00020000)) 
    m00_axis_tlast_INST_0
       (.I0(s00_axis_tlast),
        .I1(\fft_azimut_r_reg_n_0_[2] ),
        .I2(\fft_azimut_r_reg_n_0_[1] ),
        .I3(\fft_azimut_r_reg_n_0_[0] ),
        .I4(\fft_azimut_r_reg_n_0_[3] ),
        .O(m00_axis_tlast));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'h00020000)) 
    m00_axis_tvalid_INST_0
       (.I0(s00_axis_tvalid),
        .I1(\fft_azimut_r_reg_n_0_[2] ),
        .I2(\fft_azimut_r_reg_n_0_[1] ),
        .I3(\fft_azimut_r_reg_n_0_[0] ),
        .I4(\fft_azimut_r_reg_n_0_[3] ),
        .O(m00_axis_tvalid));
endmodule

(* CHECK_LICENSE_TYPE = "design_1_averageFFT_0_0,averageFFT_v4_0,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "averageFFT_v4_0,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (s00_axis_tready,
    s00_axis_tdata,
    s00_axis_tstrb,
    s00_axis_tlast,
    s00_axis_tvalid,
    m00_axis_tvalid,
    m00_axis_tdata,
    m00_axis_tstrb,
    m00_axis_tlast,
    m00_axis_tready,
    allowed_clk,
    azimuth_0,
    m00_axis_aclk,
    clk_10MHz,
    m00_axis_aresetn,
    azimut8);
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s00_axis TREADY" *) output s00_axis_tready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s00_axis TDATA" *) input [63:0]s00_axis_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s00_axis TSTRB" *) input [7:0]s00_axis_tstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s00_axis TLAST" *) input s00_axis_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s00_axis TVALID" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s00_axis, TDATA_NUM_BYTES 8, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef, INSERT_VIP 0" *) input s00_axis_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m00_axis TVALID" *) output m00_axis_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m00_axis TDATA" *) output [31:0]m00_axis_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m00_axis TSTRB" *) output [3:0]m00_axis_tstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m00_axis TLAST" *) output m00_axis_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m00_axis TREADY" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME m00_axis, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef, INSERT_VIP 0" *) input m00_axis_tready;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 allowed_clk CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME allowed_clk, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0" *) input allowed_clk;
  input azimuth_0;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 m00_axis_aclk CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME m00_axis_aclk, ASSOCIATED_BUSIF m00_axis:s00_axis, ASSOCIATED_RESET m00_axis_aresetn, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0" *) input m00_axis_aclk;
  input clk_10MHz;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 m00_axis_aresetn RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME m00_axis_aresetn, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input m00_axis_aresetn;
  output [15:0]azimut8;

  wire \<const0> ;
  wire \<const1> ;
  wire allowed_clk;
  wire [15:0]azimut8;
  wire azimuth_0;
  wire clk_10MHz;
  wire m00_axis_aclk;
  wire m00_axis_aresetn;
  wire [15:0]\^m00_axis_tdata ;
  wire m00_axis_tlast;
  wire m00_axis_tready;
  wire m00_axis_tvalid;
  wire s00_axis_tlast;
  wire s00_axis_tvalid;

  assign m00_axis_tdata[31] = \<const0> ;
  assign m00_axis_tdata[30] = \<const0> ;
  assign m00_axis_tdata[29] = \<const0> ;
  assign m00_axis_tdata[28] = \<const0> ;
  assign m00_axis_tdata[27] = \<const0> ;
  assign m00_axis_tdata[26] = \<const0> ;
  assign m00_axis_tdata[25] = \<const0> ;
  assign m00_axis_tdata[24] = \<const0> ;
  assign m00_axis_tdata[23] = \<const0> ;
  assign m00_axis_tdata[22] = \<const0> ;
  assign m00_axis_tdata[21] = \<const0> ;
  assign m00_axis_tdata[20] = \<const0> ;
  assign m00_axis_tdata[19] = \<const0> ;
  assign m00_axis_tdata[18] = \<const0> ;
  assign m00_axis_tdata[17] = \<const0> ;
  assign m00_axis_tdata[16] = \<const0> ;
  assign m00_axis_tdata[15:0] = \^m00_axis_tdata [15:0];
  assign m00_axis_tstrb[3] = \<const1> ;
  assign m00_axis_tstrb[2] = \<const1> ;
  assign m00_axis_tstrb[1] = \<const1> ;
  assign m00_axis_tstrb[0] = \<const1> ;
  assign s00_axis_tready = m00_axis_tready;
  GND GND
       (.G(\<const0> ));
  VCC VCC
       (.P(\<const1> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_averageFFT_v4_0 inst
       (.allowed_clk(allowed_clk),
        .azimut8(azimut8),
        .azimuth_0(azimuth_0),
        .clk_10MHz(clk_10MHz),
        .m00_axis_aclk(m00_axis_aclk),
        .m00_axis_aresetn(m00_axis_aresetn),
        .m00_axis_tdata(\^m00_axis_tdata ),
        .m00_axis_tlast(m00_axis_tlast),
        .m00_axis_tvalid(m00_axis_tvalid),
        .s00_axis_tlast(s00_axis_tlast),
        .s00_axis_tvalid(s00_axis_tvalid));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
