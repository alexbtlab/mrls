// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
// Date        : Mon Feb 28 19:13:10 2022
// Host        : alexbtlab-System-Product-Name running 64-bit Ubuntu 20.04.3 LTS
// Command     : write_verilog -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_SwitchMicroWavePath_0_0_stub.v
// Design      : design_1_SwitchMicroWavePath_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z020clg400-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "SwitchMicroWavePath_v1_0,Vivado 2019.1" *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix(s00_axi_awaddr, s00_axi_awprot, 
  s00_axi_awvalid, s00_axi_awready, s00_axi_wdata, s00_axi_wstrb, s00_axi_wvalid, 
  s00_axi_wready, s00_axi_bresp, s00_axi_bvalid, s00_axi_bready, s00_axi_araddr, 
  s00_axi_arprot, s00_axi_arvalid, s00_axi_arready, s00_axi_rdata, s00_axi_rresp, 
  s00_axi_rvalid, s00_axi_rready, sw_base_sw_out_if1_a0, sw_base_sw_out_if1_a1, 
  sw_base_sw_out_if2_a0, sw_base_sw_out_if2_a1, sw_1_sw1_if1_ctrl, sw_1_sw1_if2_ctrl, 
  sw_2_sw2_if1_ctrl, sw_2_sw2_if2_ctrl, sw_3_sw3_if1_ctrl, sw_3_sw3_if2_ctrl, rx_amp_amp0, 
  rx_amp_amp1, rx_amp_amp2, low_amp_en, s00_axi_aclk, s00_axi_aresetn)
/* synthesis syn_black_box black_box_pad_pin="s00_axi_awaddr[3:0],s00_axi_awprot[2:0],s00_axi_awvalid,s00_axi_awready,s00_axi_wdata[31:0],s00_axi_wstrb[3:0],s00_axi_wvalid,s00_axi_wready,s00_axi_bresp[1:0],s00_axi_bvalid,s00_axi_bready,s00_axi_araddr[3:0],s00_axi_arprot[2:0],s00_axi_arvalid,s00_axi_arready,s00_axi_rdata[31:0],s00_axi_rresp[1:0],s00_axi_rvalid,s00_axi_rready,sw_base_sw_out_if1_a0,sw_base_sw_out_if1_a1,sw_base_sw_out_if2_a0,sw_base_sw_out_if2_a1,sw_1_sw1_if1_ctrl,sw_1_sw1_if2_ctrl,sw_2_sw2_if1_ctrl,sw_2_sw2_if2_ctrl,sw_3_sw3_if1_ctrl,sw_3_sw3_if2_ctrl,rx_amp_amp0,rx_amp_amp1,rx_amp_amp2,low_amp_en,s00_axi_aclk,s00_axi_aresetn" */;
  input [3:0]s00_axi_awaddr;
  input [2:0]s00_axi_awprot;
  input s00_axi_awvalid;
  output s00_axi_awready;
  input [31:0]s00_axi_wdata;
  input [3:0]s00_axi_wstrb;
  input s00_axi_wvalid;
  output s00_axi_wready;
  output [1:0]s00_axi_bresp;
  output s00_axi_bvalid;
  input s00_axi_bready;
  input [3:0]s00_axi_araddr;
  input [2:0]s00_axi_arprot;
  input s00_axi_arvalid;
  output s00_axi_arready;
  output [31:0]s00_axi_rdata;
  output [1:0]s00_axi_rresp;
  output s00_axi_rvalid;
  input s00_axi_rready;
  output sw_base_sw_out_if1_a0;
  output sw_base_sw_out_if1_a1;
  output sw_base_sw_out_if2_a0;
  output sw_base_sw_out_if2_a1;
  output sw_1_sw1_if1_ctrl;
  output sw_1_sw1_if2_ctrl;
  output sw_2_sw2_if1_ctrl;
  output sw_2_sw2_if2_ctrl;
  output sw_3_sw3_if1_ctrl;
  output sw_3_sw3_if2_ctrl;
  output rx_amp_amp0;
  output rx_amp_amp1;
  output rx_amp_amp2;
  output low_amp_en;
  input s00_axi_aclk;
  input s00_axi_aresetn;
endmodule
