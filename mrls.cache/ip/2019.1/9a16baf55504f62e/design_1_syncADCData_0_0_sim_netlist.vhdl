-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
-- Date        : Tue May 31 12:16:28 2022
-- Host        : alexbtlab-System-Product-Name running 64-bit Ubuntu 20.04.3 LTS
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_syncADCData_0_0_sim_netlist.vhdl
-- Design      : design_1_syncADCData_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z020clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_syncADCData_v1_0 is
  port (
    DATA_CH1_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    DATA_CH2_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    reset : in STD_LOGIC;
    dco_or_dcoa : in STD_LOGIC;
    DATA_CH1_in : in STD_LOGIC_VECTOR ( 15 downto 0 );
    clk_100MHz : in STD_LOGIC;
    clk_10MHz : in STD_LOGIC;
    DATA_CH2_in : in STD_LOGIC_VECTOR ( 15 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_syncADCData_v1_0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_syncADCData_v1_0 is
  signal DATA_CH1_tmp : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal DATA_CH1_tmp_0 : STD_LOGIC;
  signal DATA_CH2_tmp : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \count[3]_i_1_n_0\ : STD_LOGIC;
  signal count_reg : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal p_0_in : STD_LOGIC;
  signal \p_0_in__0\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \count[0]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \count[1]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \count[2]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \count[3]_i_2\ : label is "soft_lutpair0";
begin
\DATA_CH1_sync[15]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => reset,
      O => p_0_in
    );
\DATA_CH1_sync_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => DATA_CH1_tmp(0),
      Q => DATA_CH1_out(0),
      R => p_0_in
    );
\DATA_CH1_sync_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => DATA_CH1_tmp(10),
      Q => DATA_CH1_out(10),
      R => p_0_in
    );
\DATA_CH1_sync_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => DATA_CH1_tmp(11),
      Q => DATA_CH1_out(11),
      R => p_0_in
    );
\DATA_CH1_sync_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => DATA_CH1_tmp(12),
      Q => DATA_CH1_out(12),
      R => p_0_in
    );
\DATA_CH1_sync_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => DATA_CH1_tmp(13),
      Q => DATA_CH1_out(13),
      R => p_0_in
    );
\DATA_CH1_sync_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => DATA_CH1_tmp(14),
      Q => DATA_CH1_out(14),
      R => p_0_in
    );
\DATA_CH1_sync_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => DATA_CH1_tmp(15),
      Q => DATA_CH1_out(15),
      R => p_0_in
    );
\DATA_CH1_sync_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => DATA_CH1_tmp(1),
      Q => DATA_CH1_out(1),
      R => p_0_in
    );
\DATA_CH1_sync_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => DATA_CH1_tmp(2),
      Q => DATA_CH1_out(2),
      R => p_0_in
    );
\DATA_CH1_sync_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => DATA_CH1_tmp(3),
      Q => DATA_CH1_out(3),
      R => p_0_in
    );
\DATA_CH1_sync_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => DATA_CH1_tmp(4),
      Q => DATA_CH1_out(4),
      R => p_0_in
    );
\DATA_CH1_sync_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => DATA_CH1_tmp(5),
      Q => DATA_CH1_out(5),
      R => p_0_in
    );
\DATA_CH1_sync_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => DATA_CH1_tmp(6),
      Q => DATA_CH1_out(6),
      R => p_0_in
    );
\DATA_CH1_sync_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => DATA_CH1_tmp(7),
      Q => DATA_CH1_out(7),
      R => p_0_in
    );
\DATA_CH1_sync_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => DATA_CH1_tmp(8),
      Q => DATA_CH1_out(8),
      R => p_0_in
    );
\DATA_CH1_sync_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => DATA_CH1_tmp(9),
      Q => DATA_CH1_out(9),
      R => p_0_in
    );
\DATA_CH1_tmp[15]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000020"
    )
        port map (
      I0 => dco_or_dcoa,
      I1 => count_reg(0),
      I2 => count_reg(1),
      I3 => count_reg(2),
      I4 => count_reg(3),
      O => DATA_CH1_tmp_0
    );
\DATA_CH1_tmp_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk_100MHz,
      CE => DATA_CH1_tmp_0,
      D => DATA_CH1_in(0),
      Q => DATA_CH1_tmp(0),
      R => p_0_in
    );
\DATA_CH1_tmp_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => clk_100MHz,
      CE => DATA_CH1_tmp_0,
      D => DATA_CH1_in(10),
      Q => DATA_CH1_tmp(10),
      R => p_0_in
    );
\DATA_CH1_tmp_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => clk_100MHz,
      CE => DATA_CH1_tmp_0,
      D => DATA_CH1_in(11),
      Q => DATA_CH1_tmp(11),
      R => p_0_in
    );
\DATA_CH1_tmp_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => clk_100MHz,
      CE => DATA_CH1_tmp_0,
      D => DATA_CH1_in(12),
      Q => DATA_CH1_tmp(12),
      R => p_0_in
    );
\DATA_CH1_tmp_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clk_100MHz,
      CE => DATA_CH1_tmp_0,
      D => DATA_CH1_in(13),
      Q => DATA_CH1_tmp(13),
      R => p_0_in
    );
\DATA_CH1_tmp_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => clk_100MHz,
      CE => DATA_CH1_tmp_0,
      D => DATA_CH1_in(14),
      Q => DATA_CH1_tmp(14),
      R => p_0_in
    );
\DATA_CH1_tmp_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clk_100MHz,
      CE => DATA_CH1_tmp_0,
      D => DATA_CH1_in(15),
      Q => DATA_CH1_tmp(15),
      R => p_0_in
    );
\DATA_CH1_tmp_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk_100MHz,
      CE => DATA_CH1_tmp_0,
      D => DATA_CH1_in(1),
      Q => DATA_CH1_tmp(1),
      R => p_0_in
    );
\DATA_CH1_tmp_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk_100MHz,
      CE => DATA_CH1_tmp_0,
      D => DATA_CH1_in(2),
      Q => DATA_CH1_tmp(2),
      R => p_0_in
    );
\DATA_CH1_tmp_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk_100MHz,
      CE => DATA_CH1_tmp_0,
      D => DATA_CH1_in(3),
      Q => DATA_CH1_tmp(3),
      R => p_0_in
    );
\DATA_CH1_tmp_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk_100MHz,
      CE => DATA_CH1_tmp_0,
      D => DATA_CH1_in(4),
      Q => DATA_CH1_tmp(4),
      R => p_0_in
    );
\DATA_CH1_tmp_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk_100MHz,
      CE => DATA_CH1_tmp_0,
      D => DATA_CH1_in(5),
      Q => DATA_CH1_tmp(5),
      R => p_0_in
    );
\DATA_CH1_tmp_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk_100MHz,
      CE => DATA_CH1_tmp_0,
      D => DATA_CH1_in(6),
      Q => DATA_CH1_tmp(6),
      R => p_0_in
    );
\DATA_CH1_tmp_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk_100MHz,
      CE => DATA_CH1_tmp_0,
      D => DATA_CH1_in(7),
      Q => DATA_CH1_tmp(7),
      R => p_0_in
    );
\DATA_CH1_tmp_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clk_100MHz,
      CE => DATA_CH1_tmp_0,
      D => DATA_CH1_in(8),
      Q => DATA_CH1_tmp(8),
      R => p_0_in
    );
\DATA_CH1_tmp_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clk_100MHz,
      CE => DATA_CH1_tmp_0,
      D => DATA_CH1_in(9),
      Q => DATA_CH1_tmp(9),
      R => p_0_in
    );
\DATA_CH2_sync_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => DATA_CH2_tmp(0),
      Q => DATA_CH2_out(0),
      R => p_0_in
    );
\DATA_CH2_sync_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => DATA_CH2_tmp(10),
      Q => DATA_CH2_out(10),
      R => p_0_in
    );
\DATA_CH2_sync_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => DATA_CH2_tmp(11),
      Q => DATA_CH2_out(11),
      R => p_0_in
    );
\DATA_CH2_sync_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => DATA_CH2_tmp(12),
      Q => DATA_CH2_out(12),
      R => p_0_in
    );
\DATA_CH2_sync_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => DATA_CH2_tmp(13),
      Q => DATA_CH2_out(13),
      R => p_0_in
    );
\DATA_CH2_sync_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => DATA_CH2_tmp(14),
      Q => DATA_CH2_out(14),
      R => p_0_in
    );
\DATA_CH2_sync_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => DATA_CH2_tmp(15),
      Q => DATA_CH2_out(15),
      R => p_0_in
    );
\DATA_CH2_sync_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => DATA_CH2_tmp(1),
      Q => DATA_CH2_out(1),
      R => p_0_in
    );
\DATA_CH2_sync_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => DATA_CH2_tmp(2),
      Q => DATA_CH2_out(2),
      R => p_0_in
    );
\DATA_CH2_sync_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => DATA_CH2_tmp(3),
      Q => DATA_CH2_out(3),
      R => p_0_in
    );
\DATA_CH2_sync_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => DATA_CH2_tmp(4),
      Q => DATA_CH2_out(4),
      R => p_0_in
    );
\DATA_CH2_sync_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => DATA_CH2_tmp(5),
      Q => DATA_CH2_out(5),
      R => p_0_in
    );
\DATA_CH2_sync_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => DATA_CH2_tmp(6),
      Q => DATA_CH2_out(6),
      R => p_0_in
    );
\DATA_CH2_sync_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => DATA_CH2_tmp(7),
      Q => DATA_CH2_out(7),
      R => p_0_in
    );
\DATA_CH2_sync_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => DATA_CH2_tmp(8),
      Q => DATA_CH2_out(8),
      R => p_0_in
    );
\DATA_CH2_sync_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clk_10MHz,
      CE => '1',
      D => DATA_CH2_tmp(9),
      Q => DATA_CH2_out(9),
      R => p_0_in
    );
\DATA_CH2_tmp_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk_100MHz,
      CE => DATA_CH1_tmp_0,
      D => DATA_CH2_in(0),
      Q => DATA_CH2_tmp(0),
      R => p_0_in
    );
\DATA_CH2_tmp_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => clk_100MHz,
      CE => DATA_CH1_tmp_0,
      D => DATA_CH2_in(10),
      Q => DATA_CH2_tmp(10),
      R => p_0_in
    );
\DATA_CH2_tmp_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => clk_100MHz,
      CE => DATA_CH1_tmp_0,
      D => DATA_CH2_in(11),
      Q => DATA_CH2_tmp(11),
      R => p_0_in
    );
\DATA_CH2_tmp_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => clk_100MHz,
      CE => DATA_CH1_tmp_0,
      D => DATA_CH2_in(12),
      Q => DATA_CH2_tmp(12),
      R => p_0_in
    );
\DATA_CH2_tmp_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clk_100MHz,
      CE => DATA_CH1_tmp_0,
      D => DATA_CH2_in(13),
      Q => DATA_CH2_tmp(13),
      R => p_0_in
    );
\DATA_CH2_tmp_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => clk_100MHz,
      CE => DATA_CH1_tmp_0,
      D => DATA_CH2_in(14),
      Q => DATA_CH2_tmp(14),
      R => p_0_in
    );
\DATA_CH2_tmp_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clk_100MHz,
      CE => DATA_CH1_tmp_0,
      D => DATA_CH2_in(15),
      Q => DATA_CH2_tmp(15),
      R => p_0_in
    );
\DATA_CH2_tmp_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk_100MHz,
      CE => DATA_CH1_tmp_0,
      D => DATA_CH2_in(1),
      Q => DATA_CH2_tmp(1),
      R => p_0_in
    );
\DATA_CH2_tmp_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk_100MHz,
      CE => DATA_CH1_tmp_0,
      D => DATA_CH2_in(2),
      Q => DATA_CH2_tmp(2),
      R => p_0_in
    );
\DATA_CH2_tmp_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk_100MHz,
      CE => DATA_CH1_tmp_0,
      D => DATA_CH2_in(3),
      Q => DATA_CH2_tmp(3),
      R => p_0_in
    );
\DATA_CH2_tmp_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk_100MHz,
      CE => DATA_CH1_tmp_0,
      D => DATA_CH2_in(4),
      Q => DATA_CH2_tmp(4),
      R => p_0_in
    );
\DATA_CH2_tmp_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk_100MHz,
      CE => DATA_CH1_tmp_0,
      D => DATA_CH2_in(5),
      Q => DATA_CH2_tmp(5),
      R => p_0_in
    );
\DATA_CH2_tmp_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk_100MHz,
      CE => DATA_CH1_tmp_0,
      D => DATA_CH2_in(6),
      Q => DATA_CH2_tmp(6),
      R => p_0_in
    );
\DATA_CH2_tmp_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk_100MHz,
      CE => DATA_CH1_tmp_0,
      D => DATA_CH2_in(7),
      Q => DATA_CH2_tmp(7),
      R => p_0_in
    );
\DATA_CH2_tmp_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clk_100MHz,
      CE => DATA_CH1_tmp_0,
      D => DATA_CH2_in(8),
      Q => DATA_CH2_tmp(8),
      R => p_0_in
    );
\DATA_CH2_tmp_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clk_100MHz,
      CE => DATA_CH1_tmp_0,
      D => DATA_CH2_in(9),
      Q => DATA_CH2_tmp(9),
      R => p_0_in
    );
\count[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count_reg(0),
      O => \p_0_in__0\(0)
    );
\count[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => count_reg(0),
      I1 => count_reg(1),
      O => \p_0_in__0\(1)
    );
\count[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => count_reg(0),
      I1 => count_reg(1),
      I2 => count_reg(2),
      O => \p_0_in__0\(2)
    );
\count[3]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => reset,
      I1 => dco_or_dcoa,
      O => \count[3]_i_1_n_0\
    );
\count[3]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => count_reg(1),
      I1 => count_reg(0),
      I2 => count_reg(2),
      I3 => count_reg(3),
      O => \p_0_in__0\(3)
    );
\count_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk_100MHz,
      CE => '1',
      D => \p_0_in__0\(0),
      Q => count_reg(0),
      R => \count[3]_i_1_n_0\
    );
\count_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk_100MHz,
      CE => '1',
      D => \p_0_in__0\(1),
      Q => count_reg(1),
      R => \count[3]_i_1_n_0\
    );
\count_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk_100MHz,
      CE => '1',
      D => \p_0_in__0\(2),
      Q => count_reg(2),
      R => \count[3]_i_1_n_0\
    );
\count_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk_100MHz,
      CE => '1',
      D => \p_0_in__0\(3),
      Q => count_reg(3),
      R => \count[3]_i_1_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    dco_or_dcoa : in STD_LOGIC;
    dco_or_dcob : in STD_LOGIC;
    dco_or_ora : in STD_LOGIC;
    dco_or_orb : in STD_LOGIC;
    reset : in STD_LOGIC;
    DATA_CH1_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    DATA_CH2_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    clk_10MHz : in STD_LOGIC;
    clk_100MHz : in STD_LOGIC;
    DATA_CH1_in : in STD_LOGIC_VECTOR ( 15 downto 0 );
    DATA_CH2_in : in STD_LOGIC_VECTOR ( 15 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_1_syncADCData_0_0,syncADCData_v1_0,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "syncADCData_v1_0,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of dco_or_dcoa : signal is "user.org:interface:dco_or:1.0 DCO_OR dcoa";
  attribute X_INTERFACE_INFO of dco_or_dcob : signal is "user.org:interface:dco_or:1.0 DCO_OR dcob";
  attribute X_INTERFACE_INFO of dco_or_ora : signal is "user.org:interface:dco_or:1.0 DCO_OR ora";
  attribute X_INTERFACE_INFO of dco_or_orb : signal is "user.org:interface:dco_or:1.0 DCO_OR orb";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of dco_or_orb : signal is "XIL_INTERFACENAME DCO_OR, SV_INTERFACE true";
  attribute X_INTERFACE_INFO of reset : signal is "xilinx.com:signal:reset:1.0 reset RST";
  attribute X_INTERFACE_PARAMETER of reset : signal is "XIL_INTERFACENAME reset, POLARITY ACTIVE_LOW, INSERT_VIP 0";
begin
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_syncADCData_v1_0
     port map (
      DATA_CH1_in(15 downto 0) => DATA_CH1_in(15 downto 0),
      DATA_CH1_out(15 downto 0) => DATA_CH1_out(15 downto 0),
      DATA_CH2_in(15 downto 0) => DATA_CH2_in(15 downto 0),
      DATA_CH2_out(15 downto 0) => DATA_CH2_out(15 downto 0),
      clk_100MHz => clk_100MHz,
      clk_10MHz => clk_10MHz,
      dco_or_dcoa => dco_or_dcoa,
      reset => reset
    );
end STRUCTURE;
