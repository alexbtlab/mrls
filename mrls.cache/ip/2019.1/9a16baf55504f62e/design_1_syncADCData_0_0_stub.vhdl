-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
-- Date        : Tue May 31 12:16:27 2022
-- Host        : alexbtlab-System-Product-Name running 64-bit Ubuntu 20.04.3 LTS
-- Command     : write_vhdl -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_syncADCData_0_0_stub.vhdl
-- Design      : design_1_syncADCData_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7z020clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  Port ( 
    dco_or_dcoa : in STD_LOGIC;
    dco_or_dcob : in STD_LOGIC;
    dco_or_ora : in STD_LOGIC;
    dco_or_orb : in STD_LOGIC;
    reset : in STD_LOGIC;
    DATA_CH1_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    DATA_CH2_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    clk_10MHz : in STD_LOGIC;
    clk_100MHz : in STD_LOGIC;
    DATA_CH1_in : in STD_LOGIC_VECTOR ( 15 downto 0 );
    DATA_CH2_in : in STD_LOGIC_VECTOR ( 15 downto 0 )
  );

end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture stub of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "dco_or_dcoa,dco_or_dcob,dco_or_ora,dco_or_orb,reset,DATA_CH1_out[15:0],DATA_CH2_out[15:0],clk_10MHz,clk_100MHz,DATA_CH1_in[15:0],DATA_CH2_in[15:0]";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "syncADCData_v1_0,Vivado 2019.1";
begin
end;
