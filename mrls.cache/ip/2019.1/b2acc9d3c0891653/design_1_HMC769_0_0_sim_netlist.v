// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
// Date        : Thu Apr  7 13:38:57 2022
// Host        : alexbtlab-System-Product-Name running 64-bit Ubuntu 20.04.3 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_HMC769_0_0_sim_netlist.v
// Design      : design_1_HMC769_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_HMC769_v5_0
   (spi_pll_cen,
    ATTEN,
    PLL_POW_EN,
    LOW_AMP_EN,
    PAMP_EN,
    s00_axi_wready,
    s00_axi_awready,
    s00_axi_arready,
    s00_axi_rdata,
    pll_trig,
    start_adc_count,
    spi_pll_sck,
    s00_axi_rvalid,
    spi_pll_sen,
    spi_pll_mosi,
    s00_axi_bvalid,
    azimut_0,
    s00_axi_aresetn,
    spi_pll_ld_sdo,
    clk_SPI_PLL,
    clk_10MHz,
    s00_axi_aclk,
    s00_axi_awaddr,
    s00_axi_wdata,
    s00_axi_araddr,
    s00_axi_awvalid,
    s00_axi_wvalid,
    s00_axi_wstrb,
    s00_axi_arvalid,
    s00_axi_bready,
    s00_axi_rready,
    clkDCO_10MHz);
  output spi_pll_cen;
  output [5:0]ATTEN;
  output PLL_POW_EN;
  output LOW_AMP_EN;
  output PAMP_EN;
  output s00_axi_wready;
  output s00_axi_awready;
  output s00_axi_arready;
  output [31:0]s00_axi_rdata;
  output pll_trig;
  output start_adc_count;
  output spi_pll_sck;
  output s00_axi_rvalid;
  output spi_pll_sen;
  output spi_pll_mosi;
  output s00_axi_bvalid;
  input azimut_0;
  input s00_axi_aresetn;
  input spi_pll_ld_sdo;
  input clk_SPI_PLL;
  input clk_10MHz;
  input s00_axi_aclk;
  input [3:0]s00_axi_awaddr;
  input [31:0]s00_axi_wdata;
  input [3:0]s00_axi_araddr;
  input s00_axi_awvalid;
  input s00_axi_wvalid;
  input [3:0]s00_axi_wstrb;
  input s00_axi_arvalid;
  input s00_axi_bready;
  input s00_axi_rready;
  input clkDCO_10MHz;

  wire [5:0]ATTEN;
  wire HMC769_v5_0_S00_AXI_inst_n_1;
  wire HMC769_v5_0_S00_AXI_inst_n_24;
  wire HMC769_v5_0_S00_AXI_inst_n_25;
  wire HMC769_v5_0_S00_AXI_inst_n_55;
  wire HMC769_v5_0_S00_AXI_inst_n_6;
  wire LOW_AMP_EN;
  wire PAMP_EN;
  wire PLL_POW_EN;
  wire azimut_0;
  wire clkDCO_10MHz;
  wire clk_10MHz;
  wire clk_SPI_PLL;
  wire enable_triger_CMD;
  wire [23:1]ip2mb_reg1;
  wire pll_trig;
  wire s00_axi_aclk;
  wire [3:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arready;
  wire s00_axi_arvalid;
  wire [3:0]s00_axi_awaddr;
  wire s00_axi_awready;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire s00_axi_wready;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;
  wire [3:0]sel0;
  wire [1:0]slv_reg0;
  wire [4:4]slv_reg1;
  wire [5:0]slv_reg4;
  wire [3:0]slv_reg5;
  wire [15:0]slv_reg6;
  wire [16:1]slv_reg7;
  wire [2:0]\spi_hmc_mode_RX_inst/cnt_reg ;
  wire [4:0]\spi_hmc_mode_TX_inst/cnt_reg ;
  wire spi_pll_cen;
  wire spi_pll_ld_sdo;
  wire spi_pll_mosi;
  wire spi_pll_sck;
  wire spi_pll_sen;
  wire start_adc_count;
  wire top_PLL_control_i_n_14;
  wire top_PLL_control_i_n_16;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_HMC769_v5_0_S00_AXI HMC769_v5_0_S00_AXI_inst
       (.Q(slv_reg7),
        .SR(HMC769_v5_0_S00_AXI_inst_n_1),
        .\axi_araddr_reg[4]_0 (HMC769_v5_0_S00_AXI_inst_n_55),
        .\axi_araddr_reg[5]_0 ({sel0[3],sel0[0]}),
        .\axi_rdata_reg[0]_0 (top_PLL_control_i_n_16),
        .azimut_0(azimut_0),
        .\cnt_reg[2] (HMC769_v5_0_S00_AXI_inst_n_24),
        .\cnt_reg[3] (HMC769_v5_0_S00_AXI_inst_n_6),
        .enable_triger_CMD(enable_triger_CMD),
        .ip2mb_reg1(ip2mb_reg1),
        .mosi_r_reg(top_PLL_control_i_n_14),
        .mosi_r_reg_0(\spi_hmc_mode_RX_inst/cnt_reg ),
        .out(\spi_hmc_mode_TX_inst/cnt_reg ),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_arready(s00_axi_arready),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr),
        .s00_axi_awready(s00_axi_awready),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bready(s00_axi_bready),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rready(s00_axi_rready),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wready(s00_axi_wready),
        .s00_axi_wstrb(s00_axi_wstrb),
        .s00_axi_wvalid(s00_axi_wvalid),
        .slv_reg0(slv_reg0),
        .\slv_reg1_reg[0]_0 (HMC769_v5_0_S00_AXI_inst_n_25),
        .\slv_reg1_reg[4]_0 (slv_reg1),
        .\slv_reg4_reg[5]_0 (slv_reg4),
        .\slv_reg5_reg[3]_0 (slv_reg5),
        .\slv_reg6_reg[15]_0 (slv_reg6));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_top_PLL_control top_PLL_control_i
       (.ATTEN(ATTEN),
        .D(slv_reg6),
        .LOW_AMP_EN(LOW_AMP_EN),
        .LOW_AMP_EN_reg_reg_0(slv_reg5),
        .PAMP_EN(PAMP_EN),
        .PLL_POW_EN(PLL_POW_EN),
        .SS(HMC769_v5_0_S00_AXI_inst_n_1),
        .\atten_reg_reg[5]_0 (slv_reg4),
        .\axi_rdata_reg[0] ({sel0[3],sel0[0]}),
        .\axi_rdata_reg[0]_0 (HMC769_v5_0_S00_AXI_inst_n_55),
        .azimut_0(azimut_0),
        .clkDCO_10MHz(clkDCO_10MHz),
        .clk_10MHz(clk_10MHz),
        .clk_SPI_PLL(clk_SPI_PLL),
        .\cnt_reg[4] (\spi_hmc_mode_TX_inst/cnt_reg ),
        .\cnt_reg[5] (top_PLL_control_i_n_14),
        .\data_r_reg[0] (top_PLL_control_i_n_16),
        .\data_r_reg[23] (ip2mb_reg1),
        .enable_triger_CMD(enable_triger_CMD),
        .mosi_r_reg(slv_reg1),
        .mosi_r_reg_0(HMC769_v5_0_S00_AXI_inst_n_25),
        .mosi_r_reg_1(HMC769_v5_0_S00_AXI_inst_n_24),
        .mosi_r_reg_2(HMC769_v5_0_S00_AXI_inst_n_6),
        .out(\spi_hmc_mode_RX_inst/cnt_reg ),
        .pll_trig(pll_trig),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_aresetn(s00_axi_aresetn),
        .\shift_front_from_cpu_reg[15]_0 (slv_reg7),
        .slv_reg0(slv_reg0),
        .spi_pll_cen(spi_pll_cen),
        .spi_pll_ld_sdo(spi_pll_ld_sdo),
        .spi_pll_mosi(spi_pll_mosi),
        .spi_pll_sck(spi_pll_sck),
        .spi_pll_sen(spi_pll_sen),
        .start_adc_count(start_adc_count));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_HMC769_v5_0_S00_AXI
   (s00_axi_wready,
    SR,
    s00_axi_awready,
    s00_axi_arready,
    s00_axi_bvalid,
    s00_axi_rvalid,
    \cnt_reg[3] ,
    enable_triger_CMD,
    Q,
    \cnt_reg[2] ,
    \slv_reg1_reg[0]_0 ,
    \slv_reg1_reg[4]_0 ,
    \axi_araddr_reg[5]_0 ,
    \slv_reg6_reg[15]_0 ,
    \slv_reg5_reg[3]_0 ,
    \slv_reg4_reg[5]_0 ,
    \axi_araddr_reg[4]_0 ,
    s00_axi_rdata,
    slv_reg0,
    s00_axi_aclk,
    out,
    mosi_r_reg,
    s00_axi_aresetn,
    mosi_r_reg_0,
    \axi_rdata_reg[0]_0 ,
    ip2mb_reg1,
    azimut_0,
    s00_axi_awvalid,
    s00_axi_wvalid,
    s00_axi_bready,
    s00_axi_arvalid,
    s00_axi_rready,
    s00_axi_awaddr,
    s00_axi_wdata,
    s00_axi_araddr,
    s00_axi_wstrb);
  output s00_axi_wready;
  output [0:0]SR;
  output s00_axi_awready;
  output s00_axi_arready;
  output s00_axi_bvalid;
  output s00_axi_rvalid;
  output \cnt_reg[3] ;
  output enable_triger_CMD;
  output [15:0]Q;
  output \cnt_reg[2] ;
  output \slv_reg1_reg[0]_0 ;
  output [0:0]\slv_reg1_reg[4]_0 ;
  output [1:0]\axi_araddr_reg[5]_0 ;
  output [15:0]\slv_reg6_reg[15]_0 ;
  output [3:0]\slv_reg5_reg[3]_0 ;
  output [5:0]\slv_reg4_reg[5]_0 ;
  output \axi_araddr_reg[4]_0 ;
  output [31:0]s00_axi_rdata;
  output [1:0]slv_reg0;
  input s00_axi_aclk;
  input [4:0]out;
  input mosi_r_reg;
  input s00_axi_aresetn;
  input [2:0]mosi_r_reg_0;
  input \axi_rdata_reg[0]_0 ;
  input [22:0]ip2mb_reg1;
  input azimut_0;
  input s00_axi_awvalid;
  input s00_axi_wvalid;
  input s00_axi_bready;
  input s00_axi_arvalid;
  input s00_axi_rready;
  input [3:0]s00_axi_awaddr;
  input [31:0]s00_axi_wdata;
  input [3:0]s00_axi_araddr;
  input [3:0]s00_axi_wstrb;

  wire [15:0]Q;
  wire [0:0]SR;
  wire aw_en_i_1_n_0;
  wire aw_en_reg_n_0;
  wire \axi_araddr_reg[4]_0 ;
  wire [1:0]\axi_araddr_reg[5]_0 ;
  wire axi_arready0;
  wire axi_awready0;
  wire axi_bvalid_i_1_n_0;
  wire \axi_rdata[0]_i_2_n_0 ;
  wire \axi_rdata[0]_i_3_n_0 ;
  wire \axi_rdata[10]_i_2_n_0 ;
  wire \axi_rdata[10]_i_3_n_0 ;
  wire \axi_rdata[11]_i_2_n_0 ;
  wire \axi_rdata[11]_i_3_n_0 ;
  wire \axi_rdata[12]_i_2_n_0 ;
  wire \axi_rdata[12]_i_3_n_0 ;
  wire \axi_rdata[13]_i_2_n_0 ;
  wire \axi_rdata[13]_i_3_n_0 ;
  wire \axi_rdata[14]_i_2_n_0 ;
  wire \axi_rdata[14]_i_3_n_0 ;
  wire \axi_rdata[15]_i_2_n_0 ;
  wire \axi_rdata[15]_i_3_n_0 ;
  wire \axi_rdata[16]_i_2_n_0 ;
  wire \axi_rdata[16]_i_3_n_0 ;
  wire \axi_rdata[17]_i_2_n_0 ;
  wire \axi_rdata[17]_i_3_n_0 ;
  wire \axi_rdata[18]_i_3_n_0 ;
  wire \axi_rdata[18]_i_4_n_0 ;
  wire \axi_rdata[19]_i_3_n_0 ;
  wire \axi_rdata[19]_i_4_n_0 ;
  wire \axi_rdata[1]_i_2_n_0 ;
  wire \axi_rdata[1]_i_3_n_0 ;
  wire \axi_rdata[1]_i_4_n_0 ;
  wire \axi_rdata[20]_i_3_n_0 ;
  wire \axi_rdata[20]_i_4_n_0 ;
  wire \axi_rdata[21]_i_2_n_0 ;
  wire \axi_rdata[21]_i_3_n_0 ;
  wire \axi_rdata[21]_i_4_n_0 ;
  wire \axi_rdata[22]_i_3_n_0 ;
  wire \axi_rdata[22]_i_4_n_0 ;
  wire \axi_rdata[23]_i_3_n_0 ;
  wire \axi_rdata[23]_i_4_n_0 ;
  wire \axi_rdata[24]_i_2_n_0 ;
  wire \axi_rdata[24]_i_3_n_0 ;
  wire \axi_rdata[25]_i_2_n_0 ;
  wire \axi_rdata[25]_i_3_n_0 ;
  wire \axi_rdata[26]_i_2_n_0 ;
  wire \axi_rdata[26]_i_3_n_0 ;
  wire \axi_rdata[27]_i_2_n_0 ;
  wire \axi_rdata[27]_i_3_n_0 ;
  wire \axi_rdata[28]_i_2_n_0 ;
  wire \axi_rdata[28]_i_3_n_0 ;
  wire \axi_rdata[29]_i_2_n_0 ;
  wire \axi_rdata[29]_i_3_n_0 ;
  wire \axi_rdata[29]_i_4_n_0 ;
  wire \axi_rdata[2]_i_3_n_0 ;
  wire \axi_rdata[2]_i_4_n_0 ;
  wire \axi_rdata[30]_i_2_n_0 ;
  wire \axi_rdata[30]_i_3_n_0 ;
  wire \axi_rdata[31]_i_2_n_0 ;
  wire \axi_rdata[31]_i_3_n_0 ;
  wire \axi_rdata[3]_i_2_n_0 ;
  wire \axi_rdata[3]_i_3_n_0 ;
  wire \axi_rdata[4]_i_2_n_0 ;
  wire \axi_rdata[4]_i_3_n_0 ;
  wire \axi_rdata[5]_i_2_n_0 ;
  wire \axi_rdata[5]_i_3_n_0 ;
  wire \axi_rdata[6]_i_2_n_0 ;
  wire \axi_rdata[6]_i_3_n_0 ;
  wire \axi_rdata[7]_i_2_n_0 ;
  wire \axi_rdata[7]_i_3_n_0 ;
  wire \axi_rdata[8]_i_2_n_0 ;
  wire \axi_rdata[8]_i_3_n_0 ;
  wire \axi_rdata[9]_i_2_n_0 ;
  wire \axi_rdata[9]_i_3_n_0 ;
  wire \axi_rdata_reg[0]_0 ;
  wire \axi_rdata_reg[18]_i_2_n_0 ;
  wire \axi_rdata_reg[19]_i_2_n_0 ;
  wire \axi_rdata_reg[20]_i_2_n_0 ;
  wire \axi_rdata_reg[22]_i_2_n_0 ;
  wire \axi_rdata_reg[23]_i_2_n_0 ;
  wire \axi_rdata_reg[2]_i_2_n_0 ;
  wire axi_rvalid_i_1_n_0;
  wire axi_wready0;
  wire azimut_0;
  wire \cnt_reg[2] ;
  wire \cnt_reg[3] ;
  wire enable_triger_CMD;
  wire [22:0]ip2mb_reg1;
  wire mosi_r_i_15_n_0;
  wire mosi_r_i_16_n_0;
  wire mosi_r_i_17_n_0;
  wire mosi_r_i_18_n_0;
  wire mosi_r_i_19_n_0;
  wire mosi_r_i_20_n_0;
  wire mosi_r_i_7_n_0;
  wire mosi_r_reg;
  wire [2:0]mosi_r_reg_0;
  wire mosi_r_reg_i_10_n_0;
  wire mosi_r_reg_i_11_n_0;
  wire mosi_r_reg_i_12_n_0;
  wire [4:0]out;
  wire [3:0]p_0_in;
  wire [31:7]p_1_in;
  wire [31:0]reg_data_out;
  wire s00_axi_aclk;
  wire [3:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arready;
  wire s00_axi_arvalid;
  wire [3:0]s00_axi_awaddr;
  wire s00_axi_awready;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire s00_axi_wready;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;
  wire [2:1]sel0;
  wire [1:0]slv_reg0;
  wire \slv_reg0[0]_i_1_n_0 ;
  wire \slv_reg0[1]_i_1_n_0 ;
  wire \slv_reg0[1]_i_2_n_0 ;
  wire [31:0]slv_reg1;
  wire \slv_reg1_reg[0]_0 ;
  wire [0:0]\slv_reg1_reg[4]_0 ;
  wire [31:0]slv_reg2;
  wire \slv_reg2[15]_i_1_n_0 ;
  wire \slv_reg2[23]_i_1_n_0 ;
  wire \slv_reg2[31]_i_1_n_0 ;
  wire \slv_reg2[7]_i_1_n_0 ;
  wire [31:0]slv_reg3;
  wire \slv_reg3[15]_i_1_n_0 ;
  wire \slv_reg3[23]_i_1_n_0 ;
  wire \slv_reg3[31]_i_1_n_0 ;
  wire \slv_reg3[7]_i_1_n_0 ;
  wire [31:6]slv_reg4;
  wire \slv_reg4[15]_i_1_n_0 ;
  wire \slv_reg4[23]_i_1_n_0 ;
  wire \slv_reg4[31]_i_1_n_0 ;
  wire \slv_reg4[7]_i_1_n_0 ;
  wire [5:0]\slv_reg4_reg[5]_0 ;
  wire [31:4]slv_reg5;
  wire \slv_reg5[15]_i_1_n_0 ;
  wire \slv_reg5[23]_i_1_n_0 ;
  wire \slv_reg5[31]_i_1_n_0 ;
  wire \slv_reg5[7]_i_1_n_0 ;
  wire [3:0]\slv_reg5_reg[3]_0 ;
  wire [31:16]slv_reg6;
  wire \slv_reg6[15]_i_1_n_0 ;
  wire \slv_reg6[23]_i_1_n_0 ;
  wire \slv_reg6[31]_i_1_n_0 ;
  wire \slv_reg6[7]_i_1_n_0 ;
  wire [15:0]\slv_reg6_reg[15]_0 ;
  wire [31:0]slv_reg7;
  wire \slv_reg7[15]_i_1_n_0 ;
  wire \slv_reg7[23]_i_1_n_0 ;
  wire \slv_reg7[31]_i_1_n_0 ;
  wire \slv_reg7[7]_i_1_n_0 ;
  wire slv_reg_rden__0;
  wire slv_reg_wren__0;

  LUT6 #(
    .INIT(64'hF7FFC4CCC4CCC4CC)) 
    aw_en_i_1
       (.I0(s00_axi_awvalid),
        .I1(aw_en_reg_n_0),
        .I2(s00_axi_awready),
        .I3(s00_axi_wvalid),
        .I4(s00_axi_bready),
        .I5(s00_axi_bvalid),
        .O(aw_en_i_1_n_0));
  FDSE aw_en_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(aw_en_i_1_n_0),
        .Q(aw_en_reg_n_0),
        .S(SR));
  FDRE \axi_araddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(axi_arready0),
        .D(s00_axi_araddr[0]),
        .Q(\axi_araddr_reg[5]_0 [0]),
        .R(SR));
  FDRE \axi_araddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(axi_arready0),
        .D(s00_axi_araddr[1]),
        .Q(sel0[1]),
        .R(SR));
  FDRE \axi_araddr_reg[4] 
       (.C(s00_axi_aclk),
        .CE(axi_arready0),
        .D(s00_axi_araddr[2]),
        .Q(sel0[2]),
        .R(SR));
  FDRE \axi_araddr_reg[5] 
       (.C(s00_axi_aclk),
        .CE(axi_arready0),
        .D(s00_axi_araddr[3]),
        .Q(\axi_araddr_reg[5]_0 [1]),
        .R(SR));
  LUT2 #(
    .INIT(4'h2)) 
    axi_arready_i_1
       (.I0(s00_axi_arvalid),
        .I1(s00_axi_arready),
        .O(axi_arready0));
  FDRE axi_arready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_arready0),
        .Q(s00_axi_arready),
        .R(SR));
  FDRE \axi_awaddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(axi_awready0),
        .D(s00_axi_awaddr[0]),
        .Q(p_0_in[0]),
        .R(SR));
  FDRE \axi_awaddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(axi_awready0),
        .D(s00_axi_awaddr[1]),
        .Q(p_0_in[1]),
        .R(SR));
  FDRE \axi_awaddr_reg[4] 
       (.C(s00_axi_aclk),
        .CE(axi_awready0),
        .D(s00_axi_awaddr[2]),
        .Q(p_0_in[2]),
        .R(SR));
  FDRE \axi_awaddr_reg[5] 
       (.C(s00_axi_aclk),
        .CE(axi_awready0),
        .D(s00_axi_awaddr[3]),
        .Q(p_0_in[3]),
        .R(SR));
  LUT1 #(
    .INIT(2'h1)) 
    axi_awready_i_1
       (.I0(s00_axi_aresetn),
        .O(SR));
  LUT4 #(
    .INIT(16'h2000)) 
    axi_awready_i_2
       (.I0(s00_axi_wvalid),
        .I1(s00_axi_awready),
        .I2(aw_en_reg_n_0),
        .I3(s00_axi_awvalid),
        .O(axi_awready0));
  FDRE axi_awready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_awready0),
        .Q(s00_axi_awready),
        .R(SR));
  LUT6 #(
    .INIT(64'h0000FFFF80008000)) 
    axi_bvalid_i_1
       (.I0(s00_axi_awvalid),
        .I1(s00_axi_wvalid),
        .I2(s00_axi_awready),
        .I3(s00_axi_wready),
        .I4(s00_axi_bready),
        .I5(s00_axi_bvalid),
        .O(axi_bvalid_i_1_n_0));
  FDRE axi_bvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_bvalid_i_1_n_0),
        .Q(s00_axi_bvalid),
        .R(SR));
  LUT5 #(
    .INIT(32'hFFFF4540)) 
    \axi_rdata[0]_i_1 
       (.I0(\axi_araddr_reg[5]_0 [1]),
        .I1(\axi_rdata[0]_i_2_n_0 ),
        .I2(sel0[2]),
        .I3(\axi_rdata[0]_i_3_n_0 ),
        .I4(\axi_rdata_reg[0]_0 ),
        .O(reg_data_out[0]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_2 
       (.I0(slv_reg7[0]),
        .I1(\slv_reg6_reg[15]_0 [0]),
        .I2(sel0[1]),
        .I3(\slv_reg5_reg[3]_0 [0]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(\slv_reg4_reg[5]_0 [0]),
        .O(\axi_rdata[0]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[0]_i_3 
       (.I0(slv_reg3[0]),
        .I1(slv_reg2[0]),
        .I2(sel0[1]),
        .I3(slv_reg1[0]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .O(\axi_rdata[0]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \axi_rdata[0]_i_5 
       (.I0(sel0[2]),
        .I1(sel0[1]),
        .O(\axi_araddr_reg[4]_0 ));
  LUT6 #(
    .INIT(64'h4F444F4F4F444444)) 
    \axi_rdata[10]_i_1 
       (.I0(\axi_rdata[21]_i_4_n_0 ),
        .I1(ip2mb_reg1[9]),
        .I2(\axi_araddr_reg[5]_0 [1]),
        .I3(\axi_rdata[10]_i_2_n_0 ),
        .I4(sel0[2]),
        .I5(\axi_rdata[10]_i_3_n_0 ),
        .O(reg_data_out[10]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_2 
       (.I0(Q[9]),
        .I1(\slv_reg6_reg[15]_0 [10]),
        .I2(sel0[1]),
        .I3(slv_reg5[10]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(slv_reg4[10]),
        .O(\axi_rdata[10]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[10]_i_3 
       (.I0(slv_reg3[10]),
        .I1(slv_reg2[10]),
        .I2(sel0[1]),
        .I3(slv_reg1[10]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .O(\axi_rdata[10]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h4540FFFF45404540)) 
    \axi_rdata[11]_i_1 
       (.I0(\axi_araddr_reg[5]_0 [1]),
        .I1(\axi_rdata[11]_i_2_n_0 ),
        .I2(sel0[2]),
        .I3(\axi_rdata[11]_i_3_n_0 ),
        .I4(\axi_rdata[21]_i_4_n_0 ),
        .I5(ip2mb_reg1[10]),
        .O(reg_data_out[11]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_2 
       (.I0(Q[10]),
        .I1(\slv_reg6_reg[15]_0 [11]),
        .I2(sel0[1]),
        .I3(slv_reg5[11]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(slv_reg4[11]),
        .O(\axi_rdata[11]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[11]_i_3 
       (.I0(slv_reg3[11]),
        .I1(slv_reg2[11]),
        .I2(sel0[1]),
        .I3(slv_reg1[11]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .O(\axi_rdata[11]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h4540FFFF45404540)) 
    \axi_rdata[12]_i_1 
       (.I0(\axi_araddr_reg[5]_0 [1]),
        .I1(\axi_rdata[12]_i_2_n_0 ),
        .I2(sel0[2]),
        .I3(\axi_rdata[12]_i_3_n_0 ),
        .I4(\axi_rdata[21]_i_4_n_0 ),
        .I5(ip2mb_reg1[11]),
        .O(reg_data_out[12]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_2 
       (.I0(Q[11]),
        .I1(\slv_reg6_reg[15]_0 [12]),
        .I2(sel0[1]),
        .I3(slv_reg5[12]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(slv_reg4[12]),
        .O(\axi_rdata[12]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[12]_i_3 
       (.I0(slv_reg3[12]),
        .I1(slv_reg2[12]),
        .I2(sel0[1]),
        .I3(slv_reg1[12]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .O(\axi_rdata[12]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h4F444F4F4F444444)) 
    \axi_rdata[13]_i_1 
       (.I0(\axi_rdata[21]_i_4_n_0 ),
        .I1(ip2mb_reg1[12]),
        .I2(\axi_araddr_reg[5]_0 [1]),
        .I3(\axi_rdata[13]_i_2_n_0 ),
        .I4(sel0[2]),
        .I5(\axi_rdata[13]_i_3_n_0 ),
        .O(reg_data_out[13]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_2 
       (.I0(Q[12]),
        .I1(\slv_reg6_reg[15]_0 [13]),
        .I2(sel0[1]),
        .I3(slv_reg5[13]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(slv_reg4[13]),
        .O(\axi_rdata[13]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[13]_i_3 
       (.I0(slv_reg3[13]),
        .I1(slv_reg2[13]),
        .I2(sel0[1]),
        .I3(slv_reg1[13]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .O(\axi_rdata[13]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h4540FFFF45404540)) 
    \axi_rdata[14]_i_1 
       (.I0(\axi_araddr_reg[5]_0 [1]),
        .I1(\axi_rdata[14]_i_2_n_0 ),
        .I2(sel0[2]),
        .I3(\axi_rdata[14]_i_3_n_0 ),
        .I4(\axi_rdata[21]_i_4_n_0 ),
        .I5(ip2mb_reg1[13]),
        .O(reg_data_out[14]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_2 
       (.I0(Q[13]),
        .I1(\slv_reg6_reg[15]_0 [14]),
        .I2(sel0[1]),
        .I3(slv_reg5[14]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(slv_reg4[14]),
        .O(\axi_rdata[14]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[14]_i_3 
       (.I0(slv_reg3[14]),
        .I1(slv_reg2[14]),
        .I2(sel0[1]),
        .I3(slv_reg1[14]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .O(\axi_rdata[14]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h4540FFFF45404540)) 
    \axi_rdata[15]_i_1 
       (.I0(\axi_araddr_reg[5]_0 [1]),
        .I1(\axi_rdata[15]_i_2_n_0 ),
        .I2(sel0[2]),
        .I3(\axi_rdata[15]_i_3_n_0 ),
        .I4(\axi_rdata[21]_i_4_n_0 ),
        .I5(ip2mb_reg1[14]),
        .O(reg_data_out[15]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_2 
       (.I0(Q[14]),
        .I1(\slv_reg6_reg[15]_0 [15]),
        .I2(sel0[1]),
        .I3(slv_reg5[15]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(slv_reg4[15]),
        .O(\axi_rdata[15]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[15]_i_3 
       (.I0(slv_reg3[15]),
        .I1(slv_reg2[15]),
        .I2(sel0[1]),
        .I3(slv_reg1[15]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .O(\axi_rdata[15]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h4F444F4F4F444444)) 
    \axi_rdata[16]_i_1 
       (.I0(\axi_rdata[21]_i_4_n_0 ),
        .I1(ip2mb_reg1[15]),
        .I2(\axi_araddr_reg[5]_0 [1]),
        .I3(\axi_rdata[16]_i_2_n_0 ),
        .I4(sel0[2]),
        .I5(\axi_rdata[16]_i_3_n_0 ),
        .O(reg_data_out[16]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[16]_i_2 
       (.I0(Q[15]),
        .I1(slv_reg6[16]),
        .I2(sel0[1]),
        .I3(slv_reg5[16]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(slv_reg4[16]),
        .O(\axi_rdata[16]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[16]_i_3 
       (.I0(slv_reg3[16]),
        .I1(slv_reg2[16]),
        .I2(sel0[1]),
        .I3(\axi_araddr_reg[5]_0 [0]),
        .I4(slv_reg1[16]),
        .O(\axi_rdata[16]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h4F444F4F4F444444)) 
    \axi_rdata[17]_i_1 
       (.I0(\axi_rdata[21]_i_4_n_0 ),
        .I1(ip2mb_reg1[16]),
        .I2(\axi_araddr_reg[5]_0 [1]),
        .I3(\axi_rdata[17]_i_2_n_0 ),
        .I4(sel0[2]),
        .I5(\axi_rdata[17]_i_3_n_0 ),
        .O(reg_data_out[17]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[17]_i_2 
       (.I0(slv_reg7[17]),
        .I1(slv_reg6[17]),
        .I2(sel0[1]),
        .I3(slv_reg5[17]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(slv_reg4[17]),
        .O(\axi_rdata[17]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[17]_i_3 
       (.I0(slv_reg3[17]),
        .I1(slv_reg2[17]),
        .I2(sel0[1]),
        .I3(\axi_araddr_reg[5]_0 [0]),
        .I4(slv_reg1[17]),
        .O(\axi_rdata[17]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFEAFFFFFFEA0000)) 
    \axi_rdata[18]_i_1 
       (.I0(sel0[2]),
        .I1(\axi_araddr_reg[5]_0 [0]),
        .I2(ip2mb_reg1[17]),
        .I3(sel0[1]),
        .I4(\axi_araddr_reg[5]_0 [1]),
        .I5(\axi_rdata_reg[18]_i_2_n_0 ),
        .O(reg_data_out[18]));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[18]_i_3 
       (.I0(slv_reg3[18]),
        .I1(slv_reg2[18]),
        .I2(sel0[1]),
        .I3(slv_reg1[18]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .O(\axi_rdata[18]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[18]_i_4 
       (.I0(slv_reg7[18]),
        .I1(slv_reg6[18]),
        .I2(sel0[1]),
        .I3(slv_reg5[18]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(slv_reg4[18]),
        .O(\axi_rdata[18]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFEAFFFFFFEA0000)) 
    \axi_rdata[19]_i_1 
       (.I0(sel0[2]),
        .I1(\axi_araddr_reg[5]_0 [0]),
        .I2(ip2mb_reg1[18]),
        .I3(sel0[1]),
        .I4(\axi_araddr_reg[5]_0 [1]),
        .I5(\axi_rdata_reg[19]_i_2_n_0 ),
        .O(reg_data_out[19]));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[19]_i_3 
       (.I0(slv_reg3[19]),
        .I1(slv_reg2[19]),
        .I2(sel0[1]),
        .I3(slv_reg1[19]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .O(\axi_rdata[19]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_4 
       (.I0(slv_reg7[19]),
        .I1(slv_reg6[19]),
        .I2(sel0[1]),
        .I3(slv_reg5[19]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(slv_reg4[19]),
        .O(\axi_rdata[19]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \axi_rdata[1]_i_1 
       (.I0(\axi_rdata[1]_i_2_n_0 ),
        .I1(\axi_araddr_reg[5]_0 [1]),
        .I2(\axi_rdata[1]_i_3_n_0 ),
        .I3(sel0[2]),
        .I4(\axi_rdata[1]_i_4_n_0 ),
        .O(reg_data_out[1]));
  LUT5 #(
    .INIT(32'hBABBBAAA)) 
    \axi_rdata[1]_i_2 
       (.I0(sel0[1]),
        .I1(sel0[2]),
        .I2(ip2mb_reg1[0]),
        .I3(\axi_araddr_reg[5]_0 [0]),
        .I4(azimut_0),
        .O(\axi_rdata[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_3 
       (.I0(Q[0]),
        .I1(\slv_reg6_reg[15]_0 [1]),
        .I2(sel0[1]),
        .I3(\slv_reg5_reg[3]_0 [1]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(\slv_reg4_reg[5]_0 [1]),
        .O(\axi_rdata[1]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hAFA0C0C0)) 
    \axi_rdata[1]_i_4 
       (.I0(slv_reg3[1]),
        .I1(slv_reg2[1]),
        .I2(sel0[1]),
        .I3(slv_reg1[1]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .O(\axi_rdata[1]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFEAFFFFFFEA0000)) 
    \axi_rdata[20]_i_1 
       (.I0(sel0[2]),
        .I1(\axi_araddr_reg[5]_0 [0]),
        .I2(ip2mb_reg1[19]),
        .I3(sel0[1]),
        .I4(\axi_araddr_reg[5]_0 [1]),
        .I5(\axi_rdata_reg[20]_i_2_n_0 ),
        .O(reg_data_out[20]));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[20]_i_3 
       (.I0(slv_reg3[20]),
        .I1(slv_reg2[20]),
        .I2(sel0[1]),
        .I3(slv_reg1[20]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .O(\axi_rdata[20]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[20]_i_4 
       (.I0(slv_reg7[20]),
        .I1(slv_reg6[20]),
        .I2(sel0[1]),
        .I3(slv_reg5[20]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(slv_reg4[20]),
        .O(\axi_rdata[20]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h4540FFFF45404540)) 
    \axi_rdata[21]_i_1 
       (.I0(\axi_araddr_reg[5]_0 [1]),
        .I1(\axi_rdata[21]_i_2_n_0 ),
        .I2(sel0[2]),
        .I3(\axi_rdata[21]_i_3_n_0 ),
        .I4(\axi_rdata[21]_i_4_n_0 ),
        .I5(ip2mb_reg1[20]),
        .O(reg_data_out[21]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[21]_i_2 
       (.I0(slv_reg7[21]),
        .I1(slv_reg6[21]),
        .I2(sel0[1]),
        .I3(slv_reg5[21]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(slv_reg4[21]),
        .O(\axi_rdata[21]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[21]_i_3 
       (.I0(slv_reg3[21]),
        .I1(slv_reg2[21]),
        .I2(sel0[1]),
        .I3(\axi_araddr_reg[5]_0 [0]),
        .I4(slv_reg1[21]),
        .O(\axi_rdata[21]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'hFFDF)) 
    \axi_rdata[21]_i_4 
       (.I0(\axi_araddr_reg[5]_0 [0]),
        .I1(sel0[1]),
        .I2(\axi_araddr_reg[5]_0 [1]),
        .I3(sel0[2]),
        .O(\axi_rdata[21]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFF8FFFFFFF80000)) 
    \axi_rdata[22]_i_1 
       (.I0(ip2mb_reg1[21]),
        .I1(\axi_araddr_reg[5]_0 [0]),
        .I2(sel0[1]),
        .I3(sel0[2]),
        .I4(\axi_araddr_reg[5]_0 [1]),
        .I5(\axi_rdata_reg[22]_i_2_n_0 ),
        .O(reg_data_out[22]));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[22]_i_3 
       (.I0(slv_reg3[22]),
        .I1(slv_reg2[22]),
        .I2(sel0[1]),
        .I3(slv_reg1[22]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .O(\axi_rdata[22]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[22]_i_4 
       (.I0(slv_reg7[22]),
        .I1(slv_reg6[22]),
        .I2(sel0[1]),
        .I3(slv_reg5[22]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(slv_reg4[22]),
        .O(\axi_rdata[22]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFF8FFFFFFF80000)) 
    \axi_rdata[23]_i_1 
       (.I0(ip2mb_reg1[22]),
        .I1(\axi_araddr_reg[5]_0 [0]),
        .I2(sel0[1]),
        .I3(sel0[2]),
        .I4(\axi_araddr_reg[5]_0 [1]),
        .I5(\axi_rdata_reg[23]_i_2_n_0 ),
        .O(reg_data_out[23]));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[23]_i_3 
       (.I0(slv_reg3[23]),
        .I1(slv_reg2[23]),
        .I2(sel0[1]),
        .I3(slv_reg1[23]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .O(\axi_rdata[23]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_4 
       (.I0(slv_reg7[23]),
        .I1(slv_reg6[23]),
        .I2(sel0[1]),
        .I3(slv_reg5[23]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(slv_reg4[23]),
        .O(\axi_rdata[23]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h00E2)) 
    \axi_rdata[24]_i_1 
       (.I0(\axi_rdata[24]_i_2_n_0 ),
        .I1(sel0[2]),
        .I2(\axi_rdata[24]_i_3_n_0 ),
        .I3(\axi_araddr_reg[5]_0 [1]),
        .O(reg_data_out[24]));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[24]_i_2 
       (.I0(slv_reg3[24]),
        .I1(slv_reg2[24]),
        .I2(sel0[1]),
        .I3(\axi_araddr_reg[5]_0 [0]),
        .I4(slv_reg1[24]),
        .O(\axi_rdata[24]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[24]_i_3 
       (.I0(slv_reg7[24]),
        .I1(slv_reg6[24]),
        .I2(sel0[1]),
        .I3(slv_reg5[24]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(slv_reg4[24]),
        .O(\axi_rdata[24]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h00E2)) 
    \axi_rdata[25]_i_1 
       (.I0(\axi_rdata[25]_i_2_n_0 ),
        .I1(sel0[2]),
        .I2(\axi_rdata[25]_i_3_n_0 ),
        .I3(\axi_araddr_reg[5]_0 [1]),
        .O(reg_data_out[25]));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[25]_i_2 
       (.I0(slv_reg3[25]),
        .I1(slv_reg2[25]),
        .I2(sel0[1]),
        .I3(\axi_araddr_reg[5]_0 [0]),
        .I4(slv_reg1[25]),
        .O(\axi_rdata[25]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[25]_i_3 
       (.I0(slv_reg7[25]),
        .I1(slv_reg6[25]),
        .I2(sel0[1]),
        .I3(slv_reg5[25]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(slv_reg4[25]),
        .O(\axi_rdata[25]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'hFCBBFC88)) 
    \axi_rdata[26]_i_1 
       (.I0(sel0[1]),
        .I1(\axi_araddr_reg[5]_0 [1]),
        .I2(\axi_rdata[26]_i_2_n_0 ),
        .I3(sel0[2]),
        .I4(\axi_rdata[26]_i_3_n_0 ),
        .O(reg_data_out[26]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[26]_i_2 
       (.I0(slv_reg7[26]),
        .I1(slv_reg6[26]),
        .I2(sel0[1]),
        .I3(slv_reg5[26]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(slv_reg4[26]),
        .O(\axi_rdata[26]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[26]_i_3 
       (.I0(slv_reg3[26]),
        .I1(slv_reg2[26]),
        .I2(sel0[1]),
        .I3(slv_reg1[26]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .O(\axi_rdata[26]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hFCBBFC88)) 
    \axi_rdata[27]_i_1 
       (.I0(sel0[1]),
        .I1(\axi_araddr_reg[5]_0 [1]),
        .I2(\axi_rdata[27]_i_2_n_0 ),
        .I3(sel0[2]),
        .I4(\axi_rdata[27]_i_3_n_0 ),
        .O(reg_data_out[27]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[27]_i_2 
       (.I0(slv_reg7[27]),
        .I1(slv_reg6[27]),
        .I2(sel0[1]),
        .I3(slv_reg5[27]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(slv_reg4[27]),
        .O(\axi_rdata[27]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[27]_i_3 
       (.I0(slv_reg3[27]),
        .I1(slv_reg2[27]),
        .I2(sel0[1]),
        .I3(slv_reg1[27]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .O(\axi_rdata[27]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h00E2)) 
    \axi_rdata[28]_i_1 
       (.I0(\axi_rdata[28]_i_2_n_0 ),
        .I1(sel0[2]),
        .I2(\axi_rdata[28]_i_3_n_0 ),
        .I3(\axi_araddr_reg[5]_0 [1]),
        .O(reg_data_out[28]));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[28]_i_2 
       (.I0(slv_reg3[28]),
        .I1(slv_reg2[28]),
        .I2(sel0[1]),
        .I3(\axi_araddr_reg[5]_0 [0]),
        .I4(slv_reg1[28]),
        .O(\axi_rdata[28]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[28]_i_3 
       (.I0(slv_reg7[28]),
        .I1(slv_reg6[28]),
        .I2(sel0[1]),
        .I3(slv_reg5[28]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(slv_reg4[28]),
        .O(\axi_rdata[28]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFBBFFBBFFB8FCB8)) 
    \axi_rdata[29]_i_1 
       (.I0(sel0[1]),
        .I1(\axi_araddr_reg[5]_0 [1]),
        .I2(\axi_rdata[29]_i_2_n_0 ),
        .I3(sel0[2]),
        .I4(\axi_rdata[29]_i_3_n_0 ),
        .I5(\axi_rdata[29]_i_4_n_0 ),
        .O(reg_data_out[29]));
  LUT5 #(
    .INIT(32'h0000B800)) 
    \axi_rdata[29]_i_2 
       (.I0(slv_reg3[29]),
        .I1(\axi_araddr_reg[5]_0 [0]),
        .I2(slv_reg2[29]),
        .I3(sel0[1]),
        .I4(sel0[2]),
        .O(\axi_rdata[29]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[29]_i_3 
       (.I0(slv_reg7[29]),
        .I1(slv_reg6[29]),
        .I2(sel0[1]),
        .I3(slv_reg5[29]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(slv_reg4[29]),
        .O(\axi_rdata[29]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'h1011)) 
    \axi_rdata[29]_i_4 
       (.I0(sel0[1]),
        .I1(sel0[2]),
        .I2(slv_reg1[29]),
        .I3(\axi_araddr_reg[5]_0 [0]),
        .O(\axi_rdata[29]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hBAAAFFFFBAAA0000)) 
    \axi_rdata[2]_i_1 
       (.I0(sel0[2]),
        .I1(sel0[1]),
        .I2(\axi_araddr_reg[5]_0 [0]),
        .I3(ip2mb_reg1[1]),
        .I4(\axi_araddr_reg[5]_0 [1]),
        .I5(\axi_rdata_reg[2]_i_2_n_0 ),
        .O(reg_data_out[2]));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[2]_i_3 
       (.I0(slv_reg3[2]),
        .I1(slv_reg2[2]),
        .I2(sel0[1]),
        .I3(slv_reg1[2]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .O(\axi_rdata[2]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_4 
       (.I0(Q[1]),
        .I1(\slv_reg6_reg[15]_0 [2]),
        .I2(sel0[1]),
        .I3(\slv_reg5_reg[3]_0 [2]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(\slv_reg4_reg[5]_0 [2]),
        .O(\axi_rdata[2]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h00E2)) 
    \axi_rdata[30]_i_1 
       (.I0(\axi_rdata[30]_i_2_n_0 ),
        .I1(sel0[2]),
        .I2(\axi_rdata[30]_i_3_n_0 ),
        .I3(\axi_araddr_reg[5]_0 [1]),
        .O(reg_data_out[30]));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[30]_i_2 
       (.I0(slv_reg3[30]),
        .I1(slv_reg2[30]),
        .I2(sel0[1]),
        .I3(\axi_araddr_reg[5]_0 [0]),
        .I4(slv_reg1[30]),
        .O(\axi_rdata[30]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[30]_i_3 
       (.I0(slv_reg7[30]),
        .I1(slv_reg6[30]),
        .I2(sel0[1]),
        .I3(slv_reg5[30]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(slv_reg4[30]),
        .O(\axi_rdata[30]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hFCBBFC88)) 
    \axi_rdata[31]_i_1 
       (.I0(sel0[1]),
        .I1(\axi_araddr_reg[5]_0 [1]),
        .I2(\axi_rdata[31]_i_2_n_0 ),
        .I3(sel0[2]),
        .I4(\axi_rdata[31]_i_3_n_0 ),
        .O(reg_data_out[31]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[31]_i_2 
       (.I0(slv_reg7[31]),
        .I1(slv_reg6[31]),
        .I2(sel0[1]),
        .I3(slv_reg5[31]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(slv_reg4[31]),
        .O(\axi_rdata[31]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[31]_i_3 
       (.I0(slv_reg3[31]),
        .I1(slv_reg2[31]),
        .I2(sel0[1]),
        .I3(slv_reg1[31]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .O(\axi_rdata[31]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h4F444F4F4F444444)) 
    \axi_rdata[3]_i_1 
       (.I0(\axi_rdata[21]_i_4_n_0 ),
        .I1(ip2mb_reg1[2]),
        .I2(\axi_araddr_reg[5]_0 [1]),
        .I3(\axi_rdata[3]_i_2_n_0 ),
        .I4(sel0[2]),
        .I5(\axi_rdata[3]_i_3_n_0 ),
        .O(reg_data_out[3]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_2 
       (.I0(Q[2]),
        .I1(\slv_reg6_reg[15]_0 [3]),
        .I2(sel0[1]),
        .I3(\slv_reg5_reg[3]_0 [3]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(\slv_reg4_reg[5]_0 [3]),
        .O(\axi_rdata[3]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[3]_i_3 
       (.I0(slv_reg3[3]),
        .I1(slv_reg2[3]),
        .I2(sel0[1]),
        .I3(slv_reg1[3]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .O(\axi_rdata[3]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h4F444F4F4F444444)) 
    \axi_rdata[4]_i_1 
       (.I0(\axi_rdata[21]_i_4_n_0 ),
        .I1(ip2mb_reg1[3]),
        .I2(\axi_araddr_reg[5]_0 [1]),
        .I3(\axi_rdata[4]_i_2_n_0 ),
        .I4(sel0[2]),
        .I5(\axi_rdata[4]_i_3_n_0 ),
        .O(reg_data_out[4]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_2 
       (.I0(Q[3]),
        .I1(\slv_reg6_reg[15]_0 [4]),
        .I2(sel0[1]),
        .I3(slv_reg5[4]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(\slv_reg4_reg[5]_0 [4]),
        .O(\axi_rdata[4]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAFA0C0C0)) 
    \axi_rdata[4]_i_3 
       (.I0(slv_reg3[4]),
        .I1(slv_reg2[4]),
        .I2(sel0[1]),
        .I3(\slv_reg1_reg[4]_0 ),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .O(\axi_rdata[4]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h4540FFFF45404540)) 
    \axi_rdata[5]_i_1 
       (.I0(\axi_araddr_reg[5]_0 [1]),
        .I1(\axi_rdata[5]_i_2_n_0 ),
        .I2(sel0[2]),
        .I3(\axi_rdata[5]_i_3_n_0 ),
        .I4(\axi_rdata[21]_i_4_n_0 ),
        .I5(ip2mb_reg1[4]),
        .O(reg_data_out[5]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_2 
       (.I0(Q[4]),
        .I1(\slv_reg6_reg[15]_0 [5]),
        .I2(sel0[1]),
        .I3(slv_reg5[5]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(\slv_reg4_reg[5]_0 [5]),
        .O(\axi_rdata[5]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[5]_i_3 
       (.I0(slv_reg3[5]),
        .I1(slv_reg2[5]),
        .I2(sel0[1]),
        .I3(slv_reg1[5]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .O(\axi_rdata[5]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h4F444F4F4F444444)) 
    \axi_rdata[6]_i_1 
       (.I0(\axi_rdata[21]_i_4_n_0 ),
        .I1(ip2mb_reg1[5]),
        .I2(\axi_araddr_reg[5]_0 [1]),
        .I3(\axi_rdata[6]_i_2_n_0 ),
        .I4(sel0[2]),
        .I5(\axi_rdata[6]_i_3_n_0 ),
        .O(reg_data_out[6]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_2 
       (.I0(Q[5]),
        .I1(\slv_reg6_reg[15]_0 [6]),
        .I2(sel0[1]),
        .I3(slv_reg5[6]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(slv_reg4[6]),
        .O(\axi_rdata[6]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[6]_i_3 
       (.I0(slv_reg3[6]),
        .I1(slv_reg2[6]),
        .I2(sel0[1]),
        .I3(slv_reg1[6]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .O(\axi_rdata[6]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h4F444F4F4F444444)) 
    \axi_rdata[7]_i_1 
       (.I0(\axi_rdata[21]_i_4_n_0 ),
        .I1(ip2mb_reg1[6]),
        .I2(\axi_araddr_reg[5]_0 [1]),
        .I3(\axi_rdata[7]_i_2_n_0 ),
        .I4(sel0[2]),
        .I5(\axi_rdata[7]_i_3_n_0 ),
        .O(reg_data_out[7]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_2 
       (.I0(Q[6]),
        .I1(\slv_reg6_reg[15]_0 [7]),
        .I2(sel0[1]),
        .I3(slv_reg5[7]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(slv_reg4[7]),
        .O(\axi_rdata[7]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[7]_i_3 
       (.I0(slv_reg3[7]),
        .I1(slv_reg2[7]),
        .I2(sel0[1]),
        .I3(slv_reg1[7]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .O(\axi_rdata[7]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h4F444F4F4F444444)) 
    \axi_rdata[8]_i_1 
       (.I0(\axi_rdata[21]_i_4_n_0 ),
        .I1(ip2mb_reg1[7]),
        .I2(\axi_araddr_reg[5]_0 [1]),
        .I3(\axi_rdata[8]_i_2_n_0 ),
        .I4(sel0[2]),
        .I5(\axi_rdata[8]_i_3_n_0 ),
        .O(reg_data_out[8]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_2 
       (.I0(Q[7]),
        .I1(\slv_reg6_reg[15]_0 [8]),
        .I2(sel0[1]),
        .I3(slv_reg5[8]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(slv_reg4[8]),
        .O(\axi_rdata[8]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[8]_i_3 
       (.I0(slv_reg3[8]),
        .I1(slv_reg2[8]),
        .I2(sel0[1]),
        .I3(\axi_araddr_reg[5]_0 [0]),
        .I4(slv_reg1[8]),
        .O(\axi_rdata[8]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h4F444F4F4F444444)) 
    \axi_rdata[9]_i_1 
       (.I0(\axi_rdata[21]_i_4_n_0 ),
        .I1(ip2mb_reg1[8]),
        .I2(\axi_araddr_reg[5]_0 [1]),
        .I3(\axi_rdata[9]_i_2_n_0 ),
        .I4(sel0[2]),
        .I5(\axi_rdata[9]_i_3_n_0 ),
        .O(reg_data_out[9]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_2 
       (.I0(Q[8]),
        .I1(\slv_reg6_reg[15]_0 [9]),
        .I2(sel0[1]),
        .I3(slv_reg5[9]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(slv_reg4[9]),
        .O(\axi_rdata[9]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[9]_i_3 
       (.I0(slv_reg3[9]),
        .I1(slv_reg2[9]),
        .I2(sel0[1]),
        .I3(slv_reg1[9]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .O(\axi_rdata[9]_i_3_n_0 ));
  FDRE \axi_rdata_reg[0] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[0]),
        .Q(s00_axi_rdata[0]),
        .R(SR));
  FDRE \axi_rdata_reg[10] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[10]),
        .Q(s00_axi_rdata[10]),
        .R(SR));
  FDRE \axi_rdata_reg[11] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[11]),
        .Q(s00_axi_rdata[11]),
        .R(SR));
  FDRE \axi_rdata_reg[12] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[12]),
        .Q(s00_axi_rdata[12]),
        .R(SR));
  FDRE \axi_rdata_reg[13] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[13]),
        .Q(s00_axi_rdata[13]),
        .R(SR));
  FDRE \axi_rdata_reg[14] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[14]),
        .Q(s00_axi_rdata[14]),
        .R(SR));
  FDRE \axi_rdata_reg[15] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[15]),
        .Q(s00_axi_rdata[15]),
        .R(SR));
  FDRE \axi_rdata_reg[16] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[16]),
        .Q(s00_axi_rdata[16]),
        .R(SR));
  FDRE \axi_rdata_reg[17] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[17]),
        .Q(s00_axi_rdata[17]),
        .R(SR));
  FDRE \axi_rdata_reg[18] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[18]),
        .Q(s00_axi_rdata[18]),
        .R(SR));
  MUXF7 \axi_rdata_reg[18]_i_2 
       (.I0(\axi_rdata[18]_i_3_n_0 ),
        .I1(\axi_rdata[18]_i_4_n_0 ),
        .O(\axi_rdata_reg[18]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[19] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[19]),
        .Q(s00_axi_rdata[19]),
        .R(SR));
  MUXF7 \axi_rdata_reg[19]_i_2 
       (.I0(\axi_rdata[19]_i_3_n_0 ),
        .I1(\axi_rdata[19]_i_4_n_0 ),
        .O(\axi_rdata_reg[19]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[1] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[1]),
        .Q(s00_axi_rdata[1]),
        .R(SR));
  FDRE \axi_rdata_reg[20] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[20]),
        .Q(s00_axi_rdata[20]),
        .R(SR));
  MUXF7 \axi_rdata_reg[20]_i_2 
       (.I0(\axi_rdata[20]_i_3_n_0 ),
        .I1(\axi_rdata[20]_i_4_n_0 ),
        .O(\axi_rdata_reg[20]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[21] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[21]),
        .Q(s00_axi_rdata[21]),
        .R(SR));
  FDRE \axi_rdata_reg[22] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[22]),
        .Q(s00_axi_rdata[22]),
        .R(SR));
  MUXF7 \axi_rdata_reg[22]_i_2 
       (.I0(\axi_rdata[22]_i_3_n_0 ),
        .I1(\axi_rdata[22]_i_4_n_0 ),
        .O(\axi_rdata_reg[22]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[23] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[23]),
        .Q(s00_axi_rdata[23]),
        .R(SR));
  MUXF7 \axi_rdata_reg[23]_i_2 
       (.I0(\axi_rdata[23]_i_3_n_0 ),
        .I1(\axi_rdata[23]_i_4_n_0 ),
        .O(\axi_rdata_reg[23]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[24] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[24]),
        .Q(s00_axi_rdata[24]),
        .R(SR));
  FDRE \axi_rdata_reg[25] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[25]),
        .Q(s00_axi_rdata[25]),
        .R(SR));
  FDRE \axi_rdata_reg[26] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[26]),
        .Q(s00_axi_rdata[26]),
        .R(SR));
  FDRE \axi_rdata_reg[27] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[27]),
        .Q(s00_axi_rdata[27]),
        .R(SR));
  FDRE \axi_rdata_reg[28] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[28]),
        .Q(s00_axi_rdata[28]),
        .R(SR));
  FDRE \axi_rdata_reg[29] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[29]),
        .Q(s00_axi_rdata[29]),
        .R(SR));
  FDRE \axi_rdata_reg[2] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[2]),
        .Q(s00_axi_rdata[2]),
        .R(SR));
  MUXF7 \axi_rdata_reg[2]_i_2 
       (.I0(\axi_rdata[2]_i_3_n_0 ),
        .I1(\axi_rdata[2]_i_4_n_0 ),
        .O(\axi_rdata_reg[2]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[30] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[30]),
        .Q(s00_axi_rdata[30]),
        .R(SR));
  FDRE \axi_rdata_reg[31] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[31]),
        .Q(s00_axi_rdata[31]),
        .R(SR));
  FDRE \axi_rdata_reg[3] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[3]),
        .Q(s00_axi_rdata[3]),
        .R(SR));
  FDRE \axi_rdata_reg[4] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[4]),
        .Q(s00_axi_rdata[4]),
        .R(SR));
  FDRE \axi_rdata_reg[5] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[5]),
        .Q(s00_axi_rdata[5]),
        .R(SR));
  FDRE \axi_rdata_reg[6] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[6]),
        .Q(s00_axi_rdata[6]),
        .R(SR));
  FDRE \axi_rdata_reg[7] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[7]),
        .Q(s00_axi_rdata[7]),
        .R(SR));
  FDRE \axi_rdata_reg[8] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[8]),
        .Q(s00_axi_rdata[8]),
        .R(SR));
  FDRE \axi_rdata_reg[9] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[9]),
        .Q(s00_axi_rdata[9]),
        .R(SR));
  LUT4 #(
    .INIT(16'h08F8)) 
    axi_rvalid_i_1
       (.I0(s00_axi_arready),
        .I1(s00_axi_arvalid),
        .I2(s00_axi_rvalid),
        .I3(s00_axi_rready),
        .O(axi_rvalid_i_1_n_0));
  FDRE axi_rvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_rvalid_i_1_n_0),
        .Q(s00_axi_rvalid),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'h0800)) 
    axi_wready_i_1
       (.I0(s00_axi_awvalid),
        .I1(s00_axi_wvalid),
        .I2(s00_axi_wready),
        .I3(aw_en_reg_n_0),
        .O(axi_wready0));
  FDRE axi_wready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_wready0),
        .Q(s00_axi_wready),
        .R(SR));
  LUT2 #(
    .INIT(4'h8)) 
    enable_triger_CMD_i_1
       (.I0(slv_reg7[0]),
        .I1(s00_axi_aresetn),
        .O(enable_triger_CMD));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    mosi_r_i_15
       (.I0(slv_reg3[4]),
        .I1(slv_reg3[5]),
        .I2(out[1]),
        .I3(slv_reg3[6]),
        .I4(out[0]),
        .I5(slv_reg3[7]),
        .O(mosi_r_i_15_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    mosi_r_i_16
       (.I0(slv_reg3[0]),
        .I1(slv_reg3[1]),
        .I2(out[1]),
        .I3(slv_reg3[2]),
        .I4(out[0]),
        .I5(slv_reg3[3]),
        .O(mosi_r_i_16_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    mosi_r_i_17
       (.I0(slv_reg3[12]),
        .I1(slv_reg3[13]),
        .I2(out[1]),
        .I3(slv_reg3[14]),
        .I4(out[0]),
        .I5(slv_reg3[15]),
        .O(mosi_r_i_17_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    mosi_r_i_18
       (.I0(slv_reg3[8]),
        .I1(slv_reg3[9]),
        .I2(out[1]),
        .I3(slv_reg3[10]),
        .I4(out[0]),
        .I5(slv_reg3[11]),
        .O(mosi_r_i_18_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    mosi_r_i_19
       (.I0(slv_reg3[20]),
        .I1(slv_reg3[21]),
        .I2(out[1]),
        .I3(slv_reg3[22]),
        .I4(out[0]),
        .I5(slv_reg3[23]),
        .O(mosi_r_i_19_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    mosi_r_i_20
       (.I0(slv_reg3[16]),
        .I1(slv_reg3[17]),
        .I2(out[1]),
        .I3(slv_reg3[18]),
        .I4(out[0]),
        .I5(slv_reg3[19]),
        .O(mosi_r_i_20_n_0));
  LUT5 #(
    .INIT(32'h88BB8B8B)) 
    mosi_r_i_2__0
       (.I0(mosi_r_i_7_n_0),
        .I1(mosi_r_reg_0[2]),
        .I2(slv_reg2[5]),
        .I3(slv_reg2[4]),
        .I4(mosi_r_reg_0[0]),
        .O(\cnt_reg[2] ));
  LUT6 #(
    .INIT(64'h00000000474700FF)) 
    mosi_r_i_4__0
       (.I0(mosi_r_reg_i_10_n_0),
        .I1(out[3]),
        .I2(mosi_r_reg_i_11_n_0),
        .I3(mosi_r_reg_i_12_n_0),
        .I4(out[4]),
        .I5(mosi_r_reg),
        .O(\cnt_reg[3] ));
  LUT6 #(
    .INIT(64'h505F3030505F3F3F)) 
    mosi_r_i_7
       (.I0(slv_reg2[0]),
        .I1(slv_reg2[1]),
        .I2(mosi_r_reg_0[1]),
        .I3(slv_reg2[2]),
        .I4(mosi_r_reg_0[0]),
        .I5(slv_reg2[3]),
        .O(mosi_r_i_7_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    mosi_r_i_7__0
       (.I0(slv_reg1[0]),
        .I1(slv_reg1[1]),
        .I2(out[1]),
        .I3(slv_reg1[2]),
        .I4(out[0]),
        .I5(slv_reg1[3]),
        .O(\slv_reg1_reg[0]_0 ));
  MUXF7 mosi_r_reg_i_10
       (.I0(mosi_r_i_15_n_0),
        .I1(mosi_r_i_16_n_0),
        .O(mosi_r_reg_i_10_n_0),
        .S(out[2]));
  MUXF7 mosi_r_reg_i_11
       (.I0(mosi_r_i_17_n_0),
        .I1(mosi_r_i_18_n_0),
        .O(mosi_r_reg_i_11_n_0),
        .S(out[2]));
  MUXF7 mosi_r_reg_i_12
       (.I0(mosi_r_i_19_n_0),
        .I1(mosi_r_i_20_n_0),
        .O(mosi_r_reg_i_12_n_0),
        .S(out[2]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \slv_reg0[0]_i_1 
       (.I0(s00_axi_wdata[0]),
        .I1(\slv_reg0[1]_i_2_n_0 ),
        .I2(slv_reg0[0]),
        .O(\slv_reg0[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \slv_reg0[1]_i_1 
       (.I0(s00_axi_wdata[1]),
        .I1(\slv_reg0[1]_i_2_n_0 ),
        .I2(slv_reg0[1]),
        .O(\slv_reg0[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    \slv_reg0[1]_i_2 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[0]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(s00_axi_wstrb[0]),
        .O(\slv_reg0[1]_i_2_n_0 ));
  FDRE \slv_reg0_reg[0] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\slv_reg0[0]_i_1_n_0 ),
        .Q(slv_reg0[0]),
        .R(SR));
  FDRE \slv_reg0_reg[1] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\slv_reg0[1]_i_1_n_0 ),
        .Q(slv_reg0[1]),
        .R(SR));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_reg1[15]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[1]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(p_0_in[0]),
        .O(p_1_in[15]));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_reg1[23]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[2]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(p_0_in[0]),
        .O(p_1_in[23]));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_reg1[31]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[3]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(p_0_in[0]),
        .O(p_1_in[31]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \slv_reg1[31]_i_2 
       (.I0(s00_axi_wready),
        .I1(s00_axi_awready),
        .I2(s00_axi_awvalid),
        .I3(s00_axi_wvalid),
        .O(slv_reg_wren__0));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_reg1[7]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[0]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(p_0_in[0]),
        .O(p_1_in[7]));
  FDRE \slv_reg1_reg[0] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg1[0]),
        .R(SR));
  FDRE \slv_reg1_reg[10] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg1[10]),
        .R(SR));
  FDRE \slv_reg1_reg[11] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg1[11]),
        .R(SR));
  FDRE \slv_reg1_reg[12] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg1[12]),
        .R(SR));
  FDRE \slv_reg1_reg[13] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg1[13]),
        .R(SR));
  FDRE \slv_reg1_reg[14] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg1[14]),
        .R(SR));
  FDRE \slv_reg1_reg[15] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg1[15]),
        .R(SR));
  FDRE \slv_reg1_reg[16] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg1[16]),
        .R(SR));
  FDRE \slv_reg1_reg[17] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg1[17]),
        .R(SR));
  FDRE \slv_reg1_reg[18] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg1[18]),
        .R(SR));
  FDRE \slv_reg1_reg[19] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg1[19]),
        .R(SR));
  FDRE \slv_reg1_reg[1] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg1[1]),
        .R(SR));
  FDRE \slv_reg1_reg[20] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg1[20]),
        .R(SR));
  FDRE \slv_reg1_reg[21] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg1[21]),
        .R(SR));
  FDRE \slv_reg1_reg[22] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg1[22]),
        .R(SR));
  FDRE \slv_reg1_reg[23] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg1[23]),
        .R(SR));
  FDRE \slv_reg1_reg[24] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg1[24]),
        .R(SR));
  FDRE \slv_reg1_reg[25] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg1[25]),
        .R(SR));
  FDRE \slv_reg1_reg[26] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg1[26]),
        .R(SR));
  FDRE \slv_reg1_reg[27] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg1[27]),
        .R(SR));
  FDRE \slv_reg1_reg[28] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg1[28]),
        .R(SR));
  FDRE \slv_reg1_reg[29] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg1[29]),
        .R(SR));
  FDRE \slv_reg1_reg[2] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg1[2]),
        .R(SR));
  FDRE \slv_reg1_reg[30] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg1[30]),
        .R(SR));
  FDRE \slv_reg1_reg[31] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg1[31]),
        .R(SR));
  FDRE \slv_reg1_reg[3] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg1[3]),
        .R(SR));
  FDRE \slv_reg1_reg[4] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[4]),
        .Q(\slv_reg1_reg[4]_0 ),
        .R(SR));
  FDRE \slv_reg1_reg[5] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg1[5]),
        .R(SR));
  FDRE \slv_reg1_reg[6] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg1[6]),
        .R(SR));
  FDRE \slv_reg1_reg[7] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg1[7]),
        .R(SR));
  FDRE \slv_reg1_reg[8] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg1[8]),
        .R(SR));
  FDRE \slv_reg1_reg[9] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg1[9]),
        .R(SR));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_reg2[15]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[1]),
        .I2(p_0_in[3]),
        .I3(p_0_in[0]),
        .I4(p_0_in[2]),
        .I5(p_0_in[1]),
        .O(\slv_reg2[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_reg2[23]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[2]),
        .I2(p_0_in[3]),
        .I3(p_0_in[0]),
        .I4(p_0_in[2]),
        .I5(p_0_in[1]),
        .O(\slv_reg2[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_reg2[31]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[3]),
        .I2(p_0_in[3]),
        .I3(p_0_in[0]),
        .I4(p_0_in[2]),
        .I5(p_0_in[1]),
        .O(\slv_reg2[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_reg2[7]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[0]),
        .I2(p_0_in[3]),
        .I3(p_0_in[0]),
        .I4(p_0_in[2]),
        .I5(p_0_in[1]),
        .O(\slv_reg2[7]_i_1_n_0 ));
  FDRE \slv_reg2_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg2[0]),
        .R(SR));
  FDRE \slv_reg2_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg2[10]),
        .R(SR));
  FDRE \slv_reg2_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg2[11]),
        .R(SR));
  FDRE \slv_reg2_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg2[12]),
        .R(SR));
  FDRE \slv_reg2_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg2[13]),
        .R(SR));
  FDRE \slv_reg2_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg2[14]),
        .R(SR));
  FDRE \slv_reg2_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg2[15]),
        .R(SR));
  FDRE \slv_reg2_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg2[16]),
        .R(SR));
  FDRE \slv_reg2_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg2[17]),
        .R(SR));
  FDRE \slv_reg2_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg2[18]),
        .R(SR));
  FDRE \slv_reg2_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg2[19]),
        .R(SR));
  FDRE \slv_reg2_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg2[1]),
        .R(SR));
  FDRE \slv_reg2_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg2[20]),
        .R(SR));
  FDRE \slv_reg2_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg2[21]),
        .R(SR));
  FDRE \slv_reg2_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg2[22]),
        .R(SR));
  FDRE \slv_reg2_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg2[23]),
        .R(SR));
  FDRE \slv_reg2_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg2[24]),
        .R(SR));
  FDRE \slv_reg2_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg2[25]),
        .R(SR));
  FDRE \slv_reg2_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg2[26]),
        .R(SR));
  FDRE \slv_reg2_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg2[27]),
        .R(SR));
  FDRE \slv_reg2_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg2[28]),
        .R(SR));
  FDRE \slv_reg2_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg2[29]),
        .R(SR));
  FDRE \slv_reg2_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg2[2]),
        .R(SR));
  FDRE \slv_reg2_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg2[30]),
        .R(SR));
  FDRE \slv_reg2_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg2[31]),
        .R(SR));
  FDRE \slv_reg2_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg2[3]),
        .R(SR));
  FDRE \slv_reg2_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg2[4]),
        .R(SR));
  FDRE \slv_reg2_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg2[5]),
        .R(SR));
  FDRE \slv_reg2_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg2[6]),
        .R(SR));
  FDRE \slv_reg2_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg2[7]),
        .R(SR));
  FDRE \slv_reg2_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg2[8]),
        .R(SR));
  FDRE \slv_reg2_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg2[9]),
        .R(SR));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg3[15]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(p_0_in[3]),
        .O(\slv_reg3[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg3[23]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(p_0_in[3]),
        .O(\slv_reg3[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg3[31]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[3]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(p_0_in[3]),
        .O(\slv_reg3[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg3[7]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[0]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(p_0_in[3]),
        .O(\slv_reg3[7]_i_1_n_0 ));
  FDRE \slv_reg3_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg3[0]),
        .R(SR));
  FDRE \slv_reg3_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg3[10]),
        .R(SR));
  FDRE \slv_reg3_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg3[11]),
        .R(SR));
  FDRE \slv_reg3_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg3[12]),
        .R(SR));
  FDRE \slv_reg3_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg3[13]),
        .R(SR));
  FDRE \slv_reg3_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg3[14]),
        .R(SR));
  FDRE \slv_reg3_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg3[15]),
        .R(SR));
  FDRE \slv_reg3_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg3[16]),
        .R(SR));
  FDRE \slv_reg3_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg3[17]),
        .R(SR));
  FDRE \slv_reg3_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg3[18]),
        .R(SR));
  FDRE \slv_reg3_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg3[19]),
        .R(SR));
  FDRE \slv_reg3_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg3[1]),
        .R(SR));
  FDRE \slv_reg3_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg3[20]),
        .R(SR));
  FDRE \slv_reg3_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg3[21]),
        .R(SR));
  FDRE \slv_reg3_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg3[22]),
        .R(SR));
  FDRE \slv_reg3_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg3[23]),
        .R(SR));
  FDRE \slv_reg3_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg3[24]),
        .R(SR));
  FDRE \slv_reg3_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg3[25]),
        .R(SR));
  FDRE \slv_reg3_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg3[26]),
        .R(SR));
  FDRE \slv_reg3_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg3[27]),
        .R(SR));
  FDRE \slv_reg3_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg3[28]),
        .R(SR));
  FDRE \slv_reg3_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg3[29]),
        .R(SR));
  FDRE \slv_reg3_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg3[2]),
        .R(SR));
  FDRE \slv_reg3_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg3[30]),
        .R(SR));
  FDRE \slv_reg3_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg3[31]),
        .R(SR));
  FDRE \slv_reg3_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg3[3]),
        .R(SR));
  FDRE \slv_reg3_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg3[4]),
        .R(SR));
  FDRE \slv_reg3_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg3[5]),
        .R(SR));
  FDRE \slv_reg3_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg3[6]),
        .R(SR));
  FDRE \slv_reg3_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg3[7]),
        .R(SR));
  FDRE \slv_reg3_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg3[8]),
        .R(SR));
  FDRE \slv_reg3_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg3[9]),
        .R(SR));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_reg4[15]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[1]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[0]),
        .I5(p_0_in[2]),
        .O(\slv_reg4[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_reg4[23]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[2]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[0]),
        .I5(p_0_in[2]),
        .O(\slv_reg4[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_reg4[31]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[3]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[0]),
        .I5(p_0_in[2]),
        .O(\slv_reg4[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_reg4[7]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[0]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[0]),
        .I5(p_0_in[2]),
        .O(\slv_reg4[7]_i_1_n_0 ));
  FDRE \slv_reg4_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(\slv_reg4_reg[5]_0 [0]),
        .R(SR));
  FDRE \slv_reg4_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg4[10]),
        .R(SR));
  FDRE \slv_reg4_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg4[11]),
        .R(SR));
  FDRE \slv_reg4_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg4[12]),
        .R(SR));
  FDRE \slv_reg4_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg4[13]),
        .R(SR));
  FDRE \slv_reg4_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg4[14]),
        .R(SR));
  FDRE \slv_reg4_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg4[15]),
        .R(SR));
  FDRE \slv_reg4_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg4[16]),
        .R(SR));
  FDRE \slv_reg4_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg4[17]),
        .R(SR));
  FDRE \slv_reg4_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg4[18]),
        .R(SR));
  FDRE \slv_reg4_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg4[19]),
        .R(SR));
  FDRE \slv_reg4_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(\slv_reg4_reg[5]_0 [1]),
        .R(SR));
  FDRE \slv_reg4_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg4[20]),
        .R(SR));
  FDRE \slv_reg4_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg4[21]),
        .R(SR));
  FDRE \slv_reg4_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg4[22]),
        .R(SR));
  FDRE \slv_reg4_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg4[23]),
        .R(SR));
  FDRE \slv_reg4_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg4[24]),
        .R(SR));
  FDRE \slv_reg4_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg4[25]),
        .R(SR));
  FDRE \slv_reg4_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg4[26]),
        .R(SR));
  FDRE \slv_reg4_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg4[27]),
        .R(SR));
  FDRE \slv_reg4_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg4[28]),
        .R(SR));
  FDRE \slv_reg4_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg4[29]),
        .R(SR));
  FDRE \slv_reg4_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(\slv_reg4_reg[5]_0 [2]),
        .R(SR));
  FDRE \slv_reg4_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg4[30]),
        .R(SR));
  FDRE \slv_reg4_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg4[31]),
        .R(SR));
  FDRE \slv_reg4_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(\slv_reg4_reg[5]_0 [3]),
        .R(SR));
  FDRE \slv_reg4_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(\slv_reg4_reg[5]_0 [4]),
        .R(SR));
  FDRE \slv_reg4_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(\slv_reg4_reg[5]_0 [5]),
        .R(SR));
  FDRE \slv_reg4_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg4[6]),
        .R(SR));
  FDRE \slv_reg4_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg4[7]),
        .R(SR));
  FDRE \slv_reg4_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg4[8]),
        .R(SR));
  FDRE \slv_reg4_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg4[9]),
        .R(SR));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg5[15]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\slv_reg5[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg5[23]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\slv_reg5[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg5[31]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[3]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\slv_reg5[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg5[7]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[0]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\slv_reg5[7]_i_1_n_0 ));
  FDRE \slv_reg5_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(\slv_reg5_reg[3]_0 [0]),
        .R(SR));
  FDRE \slv_reg5_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg5[10]),
        .R(SR));
  FDRE \slv_reg5_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg5[11]),
        .R(SR));
  FDRE \slv_reg5_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg5[12]),
        .R(SR));
  FDRE \slv_reg5_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg5[13]),
        .R(SR));
  FDRE \slv_reg5_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg5[14]),
        .R(SR));
  FDRE \slv_reg5_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg5[15]),
        .R(SR));
  FDRE \slv_reg5_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg5[16]),
        .R(SR));
  FDRE \slv_reg5_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg5[17]),
        .R(SR));
  FDRE \slv_reg5_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg5[18]),
        .R(SR));
  FDRE \slv_reg5_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg5[19]),
        .R(SR));
  FDRE \slv_reg5_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(\slv_reg5_reg[3]_0 [1]),
        .R(SR));
  FDRE \slv_reg5_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg5[20]),
        .R(SR));
  FDRE \slv_reg5_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg5[21]),
        .R(SR));
  FDRE \slv_reg5_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg5[22]),
        .R(SR));
  FDRE \slv_reg5_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg5[23]),
        .R(SR));
  FDRE \slv_reg5_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg5[24]),
        .R(SR));
  FDRE \slv_reg5_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg5[25]),
        .R(SR));
  FDRE \slv_reg5_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg5[26]),
        .R(SR));
  FDRE \slv_reg5_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg5[27]),
        .R(SR));
  FDRE \slv_reg5_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg5[28]),
        .R(SR));
  FDRE \slv_reg5_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg5[29]),
        .R(SR));
  FDRE \slv_reg5_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(\slv_reg5_reg[3]_0 [2]),
        .R(SR));
  FDRE \slv_reg5_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg5[30]),
        .R(SR));
  FDRE \slv_reg5_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg5[31]),
        .R(SR));
  FDRE \slv_reg5_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(\slv_reg5_reg[3]_0 [3]),
        .R(SR));
  FDRE \slv_reg5_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg5[4]),
        .R(SR));
  FDRE \slv_reg5_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg5[5]),
        .R(SR));
  FDRE \slv_reg5_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg5[6]),
        .R(SR));
  FDRE \slv_reg5_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg5[7]),
        .R(SR));
  FDRE \slv_reg5_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg5[8]),
        .R(SR));
  FDRE \slv_reg5_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg5[9]),
        .R(SR));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg6[15]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[1]),
        .I2(p_0_in[2]),
        .I3(p_0_in[1]),
        .I4(p_0_in[0]),
        .I5(p_0_in[3]),
        .O(\slv_reg6[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg6[23]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[2]),
        .I2(p_0_in[2]),
        .I3(p_0_in[1]),
        .I4(p_0_in[0]),
        .I5(p_0_in[3]),
        .O(\slv_reg6[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg6[31]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[1]),
        .I4(p_0_in[0]),
        .I5(p_0_in[3]),
        .O(\slv_reg6[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg6[7]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[0]),
        .I2(p_0_in[2]),
        .I3(p_0_in[1]),
        .I4(p_0_in[0]),
        .I5(p_0_in[3]),
        .O(\slv_reg6[7]_i_1_n_0 ));
  FDRE \slv_reg6_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(\slv_reg6_reg[15]_0 [0]),
        .R(SR));
  FDRE \slv_reg6_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(\slv_reg6_reg[15]_0 [10]),
        .R(SR));
  FDRE \slv_reg6_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(\slv_reg6_reg[15]_0 [11]),
        .R(SR));
  FDRE \slv_reg6_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(\slv_reg6_reg[15]_0 [12]),
        .R(SR));
  FDRE \slv_reg6_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(\slv_reg6_reg[15]_0 [13]),
        .R(SR));
  FDRE \slv_reg6_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(\slv_reg6_reg[15]_0 [14]),
        .R(SR));
  FDRE \slv_reg6_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(\slv_reg6_reg[15]_0 [15]),
        .R(SR));
  FDRE \slv_reg6_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg6[16]),
        .R(SR));
  FDRE \slv_reg6_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg6[17]),
        .R(SR));
  FDRE \slv_reg6_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg6[18]),
        .R(SR));
  FDRE \slv_reg6_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg6[19]),
        .R(SR));
  FDRE \slv_reg6_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(\slv_reg6_reg[15]_0 [1]),
        .R(SR));
  FDRE \slv_reg6_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg6[20]),
        .R(SR));
  FDRE \slv_reg6_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg6[21]),
        .R(SR));
  FDRE \slv_reg6_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg6[22]),
        .R(SR));
  FDRE \slv_reg6_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg6[23]),
        .R(SR));
  FDRE \slv_reg6_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg6[24]),
        .R(SR));
  FDRE \slv_reg6_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg6[25]),
        .R(SR));
  FDRE \slv_reg6_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg6[26]),
        .R(SR));
  FDRE \slv_reg6_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg6[27]),
        .R(SR));
  FDRE \slv_reg6_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg6[28]),
        .R(SR));
  FDRE \slv_reg6_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg6[29]),
        .R(SR));
  FDRE \slv_reg6_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(\slv_reg6_reg[15]_0 [2]),
        .R(SR));
  FDRE \slv_reg6_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg6[30]),
        .R(SR));
  FDRE \slv_reg6_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg6[31]),
        .R(SR));
  FDRE \slv_reg6_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(\slv_reg6_reg[15]_0 [3]),
        .R(SR));
  FDRE \slv_reg6_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(\slv_reg6_reg[15]_0 [4]),
        .R(SR));
  FDRE \slv_reg6_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(\slv_reg6_reg[15]_0 [5]),
        .R(SR));
  FDRE \slv_reg6_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(\slv_reg6_reg[15]_0 [6]),
        .R(SR));
  FDRE \slv_reg6_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(\slv_reg6_reg[15]_0 [7]),
        .R(SR));
  FDRE \slv_reg6_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(\slv_reg6_reg[15]_0 [8]),
        .R(SR));
  FDRE \slv_reg6_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(\slv_reg6_reg[15]_0 [9]),
        .R(SR));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \slv_reg7[15]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[2]),
        .I2(s00_axi_wstrb[1]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\slv_reg7[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \slv_reg7[23]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[2]),
        .I2(s00_axi_wstrb[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\slv_reg7[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \slv_reg7[31]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[2]),
        .I2(s00_axi_wstrb[3]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\slv_reg7[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \slv_reg7[7]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[2]),
        .I2(s00_axi_wstrb[0]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\slv_reg7[7]_i_1_n_0 ));
  FDRE \slv_reg7_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg7[0]),
        .R(SR));
  FDRE \slv_reg7_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(Q[9]),
        .R(SR));
  FDRE \slv_reg7_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(Q[10]),
        .R(SR));
  FDRE \slv_reg7_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(Q[11]),
        .R(SR));
  FDRE \slv_reg7_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(Q[12]),
        .R(SR));
  FDRE \slv_reg7_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(Q[13]),
        .R(SR));
  FDRE \slv_reg7_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(Q[14]),
        .R(SR));
  FDRE \slv_reg7_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(Q[15]),
        .R(SR));
  FDRE \slv_reg7_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg7[17]),
        .R(SR));
  FDRE \slv_reg7_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg7[18]),
        .R(SR));
  FDRE \slv_reg7_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg7[19]),
        .R(SR));
  FDRE \slv_reg7_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(Q[0]),
        .R(SR));
  FDRE \slv_reg7_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg7[20]),
        .R(SR));
  FDRE \slv_reg7_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg7[21]),
        .R(SR));
  FDRE \slv_reg7_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg7[22]),
        .R(SR));
  FDRE \slv_reg7_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg7[23]),
        .R(SR));
  FDRE \slv_reg7_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg7[24]),
        .R(SR));
  FDRE \slv_reg7_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg7[25]),
        .R(SR));
  FDRE \slv_reg7_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg7[26]),
        .R(SR));
  FDRE \slv_reg7_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg7[27]),
        .R(SR));
  FDRE \slv_reg7_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg7[28]),
        .R(SR));
  FDRE \slv_reg7_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg7[29]),
        .R(SR));
  FDRE \slv_reg7_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(Q[1]),
        .R(SR));
  FDRE \slv_reg7_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg7[30]),
        .R(SR));
  FDRE \slv_reg7_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg7[31]),
        .R(SR));
  FDRE \slv_reg7_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(Q[2]),
        .R(SR));
  FDRE \slv_reg7_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(Q[3]),
        .R(SR));
  FDRE \slv_reg7_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(Q[4]),
        .R(SR));
  FDRE \slv_reg7_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(Q[5]),
        .R(SR));
  FDRE \slv_reg7_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(Q[6]),
        .R(SR));
  FDRE \slv_reg7_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(Q[7]),
        .R(SR));
  FDRE \slv_reg7_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(Q[8]),
        .R(SR));
  LUT3 #(
    .INIT(8'h20)) 
    slv_reg_rden
       (.I0(s00_axi_arvalid),
        .I1(s00_axi_rvalid),
        .I2(s00_axi_arready),
        .O(slv_reg_rden__0));
endmodule

(* CHECK_LICENSE_TYPE = "design_1_HMC769_0_0,HMC769_v5_0,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "HMC769_v5_0,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (clk_10MHz,
    clk_SPI_PLL,
    clkDCO_10MHz,
    s00_axi_awaddr,
    s00_axi_awprot,
    s00_axi_awvalid,
    s00_axi_awready,
    s00_axi_wdata,
    s00_axi_wstrb,
    s00_axi_wvalid,
    s00_axi_wready,
    s00_axi_bresp,
    s00_axi_bvalid,
    s00_axi_bready,
    s00_axi_araddr,
    s00_axi_arprot,
    s00_axi_arvalid,
    s00_axi_arready,
    s00_axi_rdata,
    s00_axi_rresp,
    s00_axi_rvalid,
    s00_axi_rready,
    azimut_0,
    spi_pll_sen,
    spi_pll_sck,
    spi_pll_mosi,
    spi_pll_cen,
    spi_pll_ld_sdo,
    pll_trig,
    ATTEN,
    PLL_POW_EN,
    LOW_AMP_EN,
    PAMP_EN,
    start_adc_count,
    s00_axi_aclk,
    s00_axi_aresetn);
  input clk_10MHz;
  input clk_SPI_PLL;
  input clkDCO_10MHz;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWADDR" *) input [5:0]s00_axi_awaddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWPROT" *) input [2:0]s00_axi_awprot;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWVALID" *) input s00_axi_awvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWREADY" *) output s00_axi_awready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI WDATA" *) input [31:0]s00_axi_wdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI WSTRB" *) input [3:0]s00_axi_wstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI WVALID" *) input s00_axi_wvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI WREADY" *) output s00_axi_wready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI BRESP" *) output [1:0]s00_axi_bresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI BVALID" *) output s00_axi_bvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI BREADY" *) input s00_axi_bready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARADDR" *) input [5:0]s00_axi_araddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARPROT" *) input [2:0]s00_axi_arprot;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARVALID" *) input s00_axi_arvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARREADY" *) output s00_axi_arready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI RDATA" *) output [31:0]s00_axi_rdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI RRESP" *) output [1:0]s00_axi_rresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI RVALID" *) output s00_axi_rvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI RREADY" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME S00_AXI, WIZ_DATA_WIDTH 32, WIZ_NUM_REG 4, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 100000000, ID_WIDTH 0, ADDR_WIDTH 6, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 2, NUM_WRITE_OUTSTANDING 2, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0" *) input s00_axi_rready;
  input azimut_0;
  (* X_INTERFACE_INFO = "user.org:interface:spi_pll:1.0 SPI_PLL sen" *) output spi_pll_sen;
  (* X_INTERFACE_INFO = "user.org:interface:spi_pll:1.0 SPI_PLL sck" *) output spi_pll_sck;
  (* X_INTERFACE_INFO = "user.org:interface:spi_pll:1.0 SPI_PLL mosi" *) output spi_pll_mosi;
  (* X_INTERFACE_INFO = "user.org:interface:spi_pll:1.0 SPI_PLL cen" *) output spi_pll_cen;
  (* X_INTERFACE_INFO = "user.org:interface:spi_pll:1.0 SPI_PLL ld_sdo" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME SPI_PLL, SV_INTERFACE true" *) input spi_pll_ld_sdo;
  output pll_trig;
  output [5:0]ATTEN;
  output PLL_POW_EN;
  output LOW_AMP_EN;
  output PAMP_EN;
  output start_adc_count;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 S00_AXI_CLK CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME S00_AXI_CLK, ASSOCIATED_BUSIF S00_AXI, ASSOCIATED_RESET s00_axi_aresetn, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0" *) input s00_axi_aclk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 S00_AXI_RST RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME S00_AXI_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input s00_axi_aresetn;

  wire \<const0> ;
  wire [5:0]ATTEN;
  wire LOW_AMP_EN;
  wire PAMP_EN;
  wire PLL_POW_EN;
  wire azimut_0;
  wire clkDCO_10MHz;
  wire clk_10MHz;
  wire clk_SPI_PLL;
  wire pll_trig;
  wire s00_axi_aclk;
  wire [5:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arready;
  wire s00_axi_arvalid;
  wire [5:0]s00_axi_awaddr;
  wire s00_axi_awready;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire s00_axi_wready;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;
  wire spi_pll_cen;
  wire spi_pll_ld_sdo;
  wire spi_pll_mosi;
  wire spi_pll_sck;
  wire spi_pll_sen;
  wire start_adc_count;

  assign s00_axi_bresp[1] = \<const0> ;
  assign s00_axi_bresp[0] = \<const0> ;
  assign s00_axi_rresp[1] = \<const0> ;
  assign s00_axi_rresp[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_HMC769_v5_0 inst
       (.ATTEN(ATTEN),
        .LOW_AMP_EN(LOW_AMP_EN),
        .PAMP_EN(PAMP_EN),
        .PLL_POW_EN(PLL_POW_EN),
        .azimut_0(azimut_0),
        .clkDCO_10MHz(clkDCO_10MHz),
        .clk_10MHz(clk_10MHz),
        .clk_SPI_PLL(clk_SPI_PLL),
        .pll_trig(pll_trig),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr[5:2]),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_arready(s00_axi_arready),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr[5:2]),
        .s00_axi_awready(s00_axi_awready),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bready(s00_axi_bready),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rready(s00_axi_rready),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wready(s00_axi_wready),
        .s00_axi_wstrb(s00_axi_wstrb),
        .s00_axi_wvalid(s00_axi_wvalid),
        .spi_pll_cen(spi_pll_cen),
        .spi_pll_ld_sdo(spi_pll_ld_sdo),
        .spi_pll_mosi(spi_pll_mosi),
        .spi_pll_sck(spi_pll_sck),
        .spi_pll_sen(spi_pll_sen),
        .start_adc_count(start_adc_count));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_spi_hmc_mode_RX
   (enable_sck,
    pll_mosi_read,
    pll_sen_read,
    out,
    \data_r_reg[0]_0 ,
    \data_r_reg[23]_0 ,
    clk_SPI_PLL,
    START_receive,
    \axi_rdata_reg[0] ,
    \axi_rdata_reg[0]_0 ,
    pll_data_ready_TX,
    mosi_r_reg_0,
    spi_pll_ld_sdo);
  output enable_sck;
  output pll_mosi_read;
  output pll_sen_read;
  output [2:0]out;
  output \data_r_reg[0]_0 ;
  output [22:0]\data_r_reg[23]_0 ;
  input clk_SPI_PLL;
  input START_receive;
  input [1:0]\axi_rdata_reg[0] ;
  input \axi_rdata_reg[0]_0 ;
  input pll_data_ready_TX;
  input mosi_r_reg_0;
  input spi_pll_ld_sdo;

  wire START_receive;
  wire [1:0]\axi_rdata_reg[0] ;
  wire \axi_rdata_reg[0]_0 ;
  wire clk_SPI_PLL;
  wire \cnt[0]_i_1_n_0 ;
  wire \cnt[0]_i_3_n_0 ;
  wire \cnt[0]_i_4_n_0 ;
  wire \cnt[0]_i_5_n_0 ;
  wire \cnt[0]_i_6__0_n_0 ;
  wire \cnt[0]_i_7_n_0 ;
  wire [15:3]cnt_reg;
  wire \cnt_reg[0]_i_2_n_0 ;
  wire \cnt_reg[0]_i_2_n_1 ;
  wire \cnt_reg[0]_i_2_n_2 ;
  wire \cnt_reg[0]_i_2_n_3 ;
  wire \cnt_reg[0]_i_2_n_4 ;
  wire \cnt_reg[0]_i_2_n_5 ;
  wire \cnt_reg[0]_i_2_n_6 ;
  wire \cnt_reg[0]_i_2_n_7 ;
  wire \cnt_reg[12]_i_1_n_1 ;
  wire \cnt_reg[12]_i_1_n_2 ;
  wire \cnt_reg[12]_i_1_n_3 ;
  wire \cnt_reg[12]_i_1_n_4 ;
  wire \cnt_reg[12]_i_1_n_5 ;
  wire \cnt_reg[12]_i_1_n_6 ;
  wire \cnt_reg[12]_i_1_n_7 ;
  wire \cnt_reg[4]_i_1_n_0 ;
  wire \cnt_reg[4]_i_1_n_1 ;
  wire \cnt_reg[4]_i_1_n_2 ;
  wire \cnt_reg[4]_i_1_n_3 ;
  wire \cnt_reg[4]_i_1_n_4 ;
  wire \cnt_reg[4]_i_1_n_5 ;
  wire \cnt_reg[4]_i_1_n_6 ;
  wire \cnt_reg[4]_i_1_n_7 ;
  wire \cnt_reg[8]_i_1_n_0 ;
  wire \cnt_reg[8]_i_1_n_1 ;
  wire \cnt_reg[8]_i_1_n_2 ;
  wire \cnt_reg[8]_i_1_n_3 ;
  wire \cnt_reg[8]_i_1_n_4 ;
  wire \cnt_reg[8]_i_1_n_5 ;
  wire \cnt_reg[8]_i_1_n_6 ;
  wire \cnt_reg[8]_i_1_n_7 ;
  wire cs_r_i_1_n_0;
  wire cs_r_i_2_n_0;
  wire cs_r_i_3_n_0;
  wire cs_r_i_4_n_0;
  wire cs_r_i_5_n_0;
  wire [15:1]data_r1;
  wire \data_r1_inferred__0/i__carry__0_n_0 ;
  wire \data_r1_inferred__0/i__carry__0_n_1 ;
  wire \data_r1_inferred__0/i__carry__0_n_2 ;
  wire \data_r1_inferred__0/i__carry__0_n_3 ;
  wire \data_r1_inferred__0/i__carry__1_n_0 ;
  wire \data_r1_inferred__0/i__carry__1_n_1 ;
  wire \data_r1_inferred__0/i__carry__1_n_2 ;
  wire \data_r1_inferred__0/i__carry__1_n_3 ;
  wire \data_r1_inferred__0/i__carry__2_n_0 ;
  wire \data_r1_inferred__0/i__carry__2_n_2 ;
  wire \data_r1_inferred__0/i__carry__2_n_3 ;
  wire \data_r1_inferred__0/i__carry_n_0 ;
  wire \data_r1_inferred__0/i__carry_n_1 ;
  wire \data_r1_inferred__0/i__carry_n_2 ;
  wire \data_r1_inferred__0/i__carry_n_3 ;
  wire \data_r[0]_i_1_n_0 ;
  wire \data_r[10]_i_1_n_0 ;
  wire \data_r[11]_i_1_n_0 ;
  wire \data_r[12]_i_1_n_0 ;
  wire \data_r[13]_i_1_n_0 ;
  wire \data_r[14]_i_1_n_0 ;
  wire \data_r[14]_i_2_n_0 ;
  wire \data_r[15]_i_1_n_0 ;
  wire \data_r[16]_i_1_n_0 ;
  wire \data_r[17]_i_1_n_0 ;
  wire \data_r[18]_i_1_n_0 ;
  wire \data_r[19]_i_1_n_0 ;
  wire \data_r[1]_i_1_n_0 ;
  wire \data_r[20]_i_1_n_0 ;
  wire \data_r[21]_i_1_n_0 ;
  wire \data_r[22]_i_1_n_0 ;
  wire \data_r[22]_i_2_n_0 ;
  wire \data_r[22]_i_3_n_0 ;
  wire \data_r[23]_i_1_n_0 ;
  wire \data_r[23]_i_2_n_0 ;
  wire \data_r[23]_i_3_n_0 ;
  wire \data_r[23]_i_4_n_0 ;
  wire \data_r[23]_i_5_n_0 ;
  wire \data_r[23]_i_6_n_0 ;
  wire \data_r[2]_i_1_n_0 ;
  wire \data_r[3]_i_1_n_0 ;
  wire \data_r[4]_i_1_n_0 ;
  wire \data_r[5]_i_1_n_0 ;
  wire \data_r[6]_i_1_n_0 ;
  wire \data_r[6]_i_2_n_0 ;
  wire \data_r[7]_i_1_n_0 ;
  wire \data_r[8]_i_1_n_0 ;
  wire \data_r[9]_i_1_n_0 ;
  wire \data_r_reg[0]_0 ;
  wire [22:0]\data_r_reg[23]_0 ;
  wire enable_cnt;
  wire enable_sck;
  wire enable_sck0;
  wire enable_sck_i_2_n_0;
  wire enable_sck_i_3_n_0;
  wire enable_sck_i_4_n_0;
  wire i__carry__0_i_1_n_0;
  wire i__carry__0_i_2__1_n_0;
  wire i__carry__0_i_3__1_n_0;
  wire i__carry__0_i_4_n_0;
  wire i__carry__1_i_1_n_0;
  wire i__carry__1_i_2_n_0;
  wire i__carry__1_i_3_n_0;
  wire i__carry__1_i_4_n_0;
  wire i__carry__2_i_1_n_0;
  wire i__carry__2_i_2_n_0;
  wire i__carry__2_i_3_n_0;
  wire i__carry_i_1__1_n_0;
  wire i__carry_i_2_n_0;
  wire i__carry_i_3__1_n_0;
  wire i__carry_i_4__1_n_0;
  wire i__carry_i_5__0_n_0;
  wire [0:0]ip2mb_reg1;
  wire mosi_r_i_1_n_0;
  wire mosi_r_i_3_n_0;
  wire mosi_r_i_4_n_0;
  wire mosi_r_i_5_n_0;
  wire mosi_r_i_6_n_0;
  wire mosi_r_i_8_n_0;
  wire mosi_r_i_9__0_n_0;
  wire mosi_r_reg_0;
  wire [2:0]out;
  wire pll_data_ready_RX;
  wire pll_data_ready_TX;
  wire pll_mosi_read;
  wire pll_sen_read;
  wire ready_r0;
  wire ready_r_i_1_n_0;
  wire ready_r_i_3_n_0;
  wire ready_r_i_4_n_0;
  wire ready_r_i_5_n_0;
  wire spi_pll_ld_sdo;
  wire start_prev;
  wire start_prev_i_1_n_0;
  wire start_prev_i_3_n_0;
  wire start_prev_i_4__0_n_0;
  wire [3:3]\NLW_cnt_reg[12]_i_1_CO_UNCONNECTED ;
  wire [2:2]\NLW_data_r1_inferred__0/i__carry__2_CO_UNCONNECTED ;
  wire [3:3]\NLW_data_r1_inferred__0/i__carry__2_O_UNCONNECTED ;

  LUT6 #(
    .INIT(64'h8C8C8C8CC0C0C000)) 
    \axi_rdata[0]_i_4 
       (.I0(ip2mb_reg1),
        .I1(\axi_rdata_reg[0] [1]),
        .I2(\axi_rdata_reg[0]_0 ),
        .I3(pll_data_ready_TX),
        .I4(pll_data_ready_RX),
        .I5(\axi_rdata_reg[0] [0]),
        .O(\data_r_reg[0]_0 ));
  LUT6 #(
    .INIT(64'h555555555555555D)) 
    \cnt[0]_i_1 
       (.I0(enable_cnt),
        .I1(\cnt[0]_i_3_n_0 ),
        .I2(cnt_reg[4]),
        .I3(cnt_reg[13]),
        .I4(\cnt[0]_i_4_n_0 ),
        .I5(\cnt[0]_i_5_n_0 ),
        .O(\cnt[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT4 #(
    .INIT(16'h0008)) 
    \cnt[0]_i_3 
       (.I0(cnt_reg[9]),
        .I1(cnt_reg[5]),
        .I2(cnt_reg[14]),
        .I3(cnt_reg[12]),
        .O(\cnt[0]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \cnt[0]_i_4 
       (.I0(cnt_reg[10]),
        .I1(cnt_reg[11]),
        .O(\cnt[0]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hFFEFFFFF)) 
    \cnt[0]_i_5 
       (.I0(\cnt[0]_i_7_n_0 ),
        .I1(out[0]),
        .I2(cnt_reg[6]),
        .I3(cnt_reg[15]),
        .I4(cnt_reg[3]),
        .O(\cnt[0]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \cnt[0]_i_6__0 
       (.I0(out[0]),
        .O(\cnt[0]_i_6__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT4 #(
    .INIT(16'hFFF7)) 
    \cnt[0]_i_7 
       (.I0(cnt_reg[8]),
        .I1(cnt_reg[7]),
        .I2(out[2]),
        .I3(out[1]),
        .O(\cnt[0]_i_7_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[0] 
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(\cnt_reg[0]_i_2_n_7 ),
        .Q(out[0]),
        .R(\cnt[0]_i_1_n_0 ));
  CARRY4 \cnt_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\cnt_reg[0]_i_2_n_0 ,\cnt_reg[0]_i_2_n_1 ,\cnt_reg[0]_i_2_n_2 ,\cnt_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\cnt_reg[0]_i_2_n_4 ,\cnt_reg[0]_i_2_n_5 ,\cnt_reg[0]_i_2_n_6 ,\cnt_reg[0]_i_2_n_7 }),
        .S({cnt_reg[3],out[2:1],\cnt[0]_i_6__0_n_0 }));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[10] 
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(\cnt_reg[8]_i_1_n_5 ),
        .Q(cnt_reg[10]),
        .R(\cnt[0]_i_1_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[11] 
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(\cnt_reg[8]_i_1_n_4 ),
        .Q(cnt_reg[11]),
        .R(\cnt[0]_i_1_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[12] 
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(\cnt_reg[12]_i_1_n_7 ),
        .Q(cnt_reg[12]),
        .R(\cnt[0]_i_1_n_0 ));
  CARRY4 \cnt_reg[12]_i_1 
       (.CI(\cnt_reg[8]_i_1_n_0 ),
        .CO({\NLW_cnt_reg[12]_i_1_CO_UNCONNECTED [3],\cnt_reg[12]_i_1_n_1 ,\cnt_reg[12]_i_1_n_2 ,\cnt_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_reg[12]_i_1_n_4 ,\cnt_reg[12]_i_1_n_5 ,\cnt_reg[12]_i_1_n_6 ,\cnt_reg[12]_i_1_n_7 }),
        .S(cnt_reg[15:12]));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[13] 
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(\cnt_reg[12]_i_1_n_6 ),
        .Q(cnt_reg[13]),
        .R(\cnt[0]_i_1_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[14] 
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(\cnt_reg[12]_i_1_n_5 ),
        .Q(cnt_reg[14]),
        .R(\cnt[0]_i_1_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[15] 
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(\cnt_reg[12]_i_1_n_4 ),
        .Q(cnt_reg[15]),
        .R(\cnt[0]_i_1_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[1] 
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(\cnt_reg[0]_i_2_n_6 ),
        .Q(out[1]),
        .R(\cnt[0]_i_1_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[2] 
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(\cnt_reg[0]_i_2_n_5 ),
        .Q(out[2]),
        .R(\cnt[0]_i_1_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[3] 
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(\cnt_reg[0]_i_2_n_4 ),
        .Q(cnt_reg[3]),
        .R(\cnt[0]_i_1_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[4] 
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(\cnt_reg[4]_i_1_n_7 ),
        .Q(cnt_reg[4]),
        .R(\cnt[0]_i_1_n_0 ));
  CARRY4 \cnt_reg[4]_i_1 
       (.CI(\cnt_reg[0]_i_2_n_0 ),
        .CO({\cnt_reg[4]_i_1_n_0 ,\cnt_reg[4]_i_1_n_1 ,\cnt_reg[4]_i_1_n_2 ,\cnt_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_reg[4]_i_1_n_4 ,\cnt_reg[4]_i_1_n_5 ,\cnt_reg[4]_i_1_n_6 ,\cnt_reg[4]_i_1_n_7 }),
        .S(cnt_reg[7:4]));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[5] 
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(\cnt_reg[4]_i_1_n_6 ),
        .Q(cnt_reg[5]),
        .R(\cnt[0]_i_1_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[6] 
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(\cnt_reg[4]_i_1_n_5 ),
        .Q(cnt_reg[6]),
        .R(\cnt[0]_i_1_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[7] 
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(\cnt_reg[4]_i_1_n_4 ),
        .Q(cnt_reg[7]),
        .R(\cnt[0]_i_1_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[8] 
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(\cnt_reg[8]_i_1_n_7 ),
        .Q(cnt_reg[8]),
        .R(\cnt[0]_i_1_n_0 ));
  CARRY4 \cnt_reg[8]_i_1 
       (.CI(\cnt_reg[4]_i_1_n_0 ),
        .CO({\cnt_reg[8]_i_1_n_0 ,\cnt_reg[8]_i_1_n_1 ,\cnt_reg[8]_i_1_n_2 ,\cnt_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_reg[8]_i_1_n_4 ,\cnt_reg[8]_i_1_n_5 ,\cnt_reg[8]_i_1_n_6 ,\cnt_reg[8]_i_1_n_7 }),
        .S(cnt_reg[11:8]));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[9] 
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(\cnt_reg[8]_i_1_n_6 ),
        .Q(cnt_reg[9]),
        .R(\cnt[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT4 #(
    .INIT(16'h00AE)) 
    cs_r_i_1
       (.I0(pll_sen_read),
        .I1(START_receive),
        .I2(enable_cnt),
        .I3(cs_r_i_2_n_0),
        .O(cs_r_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000002000000)) 
    cs_r_i_2
       (.I0(cs_r_i_3_n_0),
        .I1(cnt_reg[14]),
        .I2(cnt_reg[12]),
        .I3(cnt_reg[5]),
        .I4(out[0]),
        .I5(cs_r_i_4_n_0),
        .O(cs_r_i_2_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    cs_r_i_3
       (.I0(enable_sck_i_3_n_0),
        .I1(cnt_reg[3]),
        .I2(cnt_reg[15]),
        .I3(cnt_reg[4]),
        .I4(cnt_reg[13]),
        .I5(cs_r_i_5_n_0),
        .O(cs_r_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    cs_r_i_4
       (.I0(cnt_reg[11]),
        .I1(cnt_reg[10]),
        .I2(cnt_reg[9]),
        .I3(cnt_reg[8]),
        .O(cs_r_i_4_n_0));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT2 #(
    .INIT(4'hE)) 
    cs_r_i_5
       (.I0(cnt_reg[7]),
        .I1(cnt_reg[6]),
        .O(cs_r_i_5_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    cs_r_reg
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(cs_r_i_1_n_0),
        .Q(pll_sen_read),
        .R(1'b0));
  CARRY4 \data_r1_inferred__0/i__carry 
       (.CI(1'b0),
        .CO({\data_r1_inferred__0/i__carry_n_0 ,\data_r1_inferred__0/i__carry_n_1 ,\data_r1_inferred__0/i__carry_n_2 ,\data_r1_inferred__0/i__carry_n_3 }),
        .CYINIT(i__carry_i_1__1_n_0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data_r1[4:1]),
        .S({i__carry_i_2_n_0,i__carry_i_3__1_n_0,i__carry_i_4__1_n_0,i__carry_i_5__0_n_0}));
  CARRY4 \data_r1_inferred__0/i__carry__0 
       (.CI(\data_r1_inferred__0/i__carry_n_0 ),
        .CO({\data_r1_inferred__0/i__carry__0_n_0 ,\data_r1_inferred__0/i__carry__0_n_1 ,\data_r1_inferred__0/i__carry__0_n_2 ,\data_r1_inferred__0/i__carry__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,i__carry__0_i_1_n_0}),
        .O(data_r1[8:5]),
        .S({i__carry__0_i_2__1_n_0,i__carry__0_i_3__1_n_0,i__carry__0_i_4_n_0,cnt_reg[5]}));
  CARRY4 \data_r1_inferred__0/i__carry__1 
       (.CI(\data_r1_inferred__0/i__carry__0_n_0 ),
        .CO({\data_r1_inferred__0/i__carry__1_n_0 ,\data_r1_inferred__0/i__carry__1_n_1 ,\data_r1_inferred__0/i__carry__1_n_2 ,\data_r1_inferred__0/i__carry__1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data_r1[12:9]),
        .S({i__carry__1_i_1_n_0,i__carry__1_i_2_n_0,i__carry__1_i_3_n_0,i__carry__1_i_4_n_0}));
  CARRY4 \data_r1_inferred__0/i__carry__2 
       (.CI(\data_r1_inferred__0/i__carry__1_n_0 ),
        .CO({\data_r1_inferred__0/i__carry__2_n_0 ,\NLW_data_r1_inferred__0/i__carry__2_CO_UNCONNECTED [2],\data_r1_inferred__0/i__carry__2_n_2 ,\data_r1_inferred__0/i__carry__2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_data_r1_inferred__0/i__carry__2_O_UNCONNECTED [3],data_r1[15:13]}),
        .S({1'b1,i__carry__2_i_1_n_0,i__carry__2_i_2_n_0,i__carry__2_i_3_n_0}));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000002)) 
    \data_r[0]_i_1 
       (.I0(spi_pll_ld_sdo),
        .I1(out[0]),
        .I2(data_r1[1]),
        .I3(data_r1[2]),
        .I4(\data_r[6]_i_2_n_0 ),
        .I5(ip2mb_reg1),
        .O(\data_r[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFEF00000020)) 
    \data_r[10]_i_1 
       (.I0(spi_pll_ld_sdo),
        .I1(out[0]),
        .I2(data_r1[1]),
        .I3(data_r1[2]),
        .I4(\data_r[14]_i_2_n_0 ),
        .I5(\data_r_reg[23]_0 [9]),
        .O(\data_r[10]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFBF00000080)) 
    \data_r[11]_i_1 
       (.I0(spi_pll_ld_sdo),
        .I1(out[0]),
        .I2(data_r1[1]),
        .I3(data_r1[2]),
        .I4(\data_r[14]_i_2_n_0 ),
        .I5(\data_r_reg[23]_0 [10]),
        .O(\data_r[11]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFEFF00000200)) 
    \data_r[12]_i_1 
       (.I0(spi_pll_ld_sdo),
        .I1(out[0]),
        .I2(data_r1[1]),
        .I3(data_r1[2]),
        .I4(\data_r[14]_i_2_n_0 ),
        .I5(\data_r_reg[23]_0 [11]),
        .O(\data_r[12]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFEFFF00002000)) 
    \data_r[13]_i_1 
       (.I0(spi_pll_ld_sdo),
        .I1(data_r1[1]),
        .I2(out[0]),
        .I3(data_r1[2]),
        .I4(\data_r[14]_i_2_n_0 ),
        .I5(\data_r_reg[23]_0 [12]),
        .O(\data_r[13]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFEFFF00002000)) 
    \data_r[14]_i_1 
       (.I0(spi_pll_ld_sdo),
        .I1(out[0]),
        .I2(data_r1[1]),
        .I3(data_r1[2]),
        .I4(\data_r[14]_i_2_n_0 ),
        .I5(\data_r_reg[23]_0 [13]),
        .O(\data_r[14]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFB)) 
    \data_r[14]_i_2 
       (.I0(\data_r[23]_i_6_n_0 ),
        .I1(data_r1[3]),
        .I2(data_r1[4]),
        .I3(\data_r[23]_i_5_n_0 ),
        .I4(\data_r[23]_i_4_n_0 ),
        .I5(\data_r[22]_i_3_n_0 ),
        .O(\data_r[14]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFBF00000080)) 
    \data_r[15]_i_1 
       (.I0(spi_pll_ld_sdo),
        .I1(\data_r[23]_i_3_n_0 ),
        .I2(data_r1[3]),
        .I3(data_r1[4]),
        .I4(\data_r[23]_i_2_n_0 ),
        .I5(\data_r_reg[23]_0 [14]),
        .O(\data_r[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFB00000008)) 
    \data_r[16]_i_1 
       (.I0(spi_pll_ld_sdo),
        .I1(\data_r[22]_i_2_n_0 ),
        .I2(out[0]),
        .I3(data_r1[1]),
        .I4(data_r1[2]),
        .I5(\data_r_reg[23]_0 [15]),
        .O(\data_r[16]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFBFF00000800)) 
    \data_r[17]_i_1 
       (.I0(spi_pll_ld_sdo),
        .I1(\data_r[22]_i_2_n_0 ),
        .I2(data_r1[1]),
        .I3(out[0]),
        .I4(data_r1[2]),
        .I5(\data_r_reg[23]_0 [16]),
        .O(\data_r[17]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFBFF00000800)) 
    \data_r[18]_i_1 
       (.I0(spi_pll_ld_sdo),
        .I1(\data_r[22]_i_2_n_0 ),
        .I2(out[0]),
        .I3(data_r1[1]),
        .I4(data_r1[2]),
        .I5(\data_r_reg[23]_0 [17]),
        .O(\data_r[18]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFBFFF00008000)) 
    \data_r[19]_i_1 
       (.I0(spi_pll_ld_sdo),
        .I1(\data_r[22]_i_2_n_0 ),
        .I2(out[0]),
        .I3(data_r1[1]),
        .I4(data_r1[2]),
        .I5(\data_r_reg[23]_0 [18]),
        .O(\data_r[19]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFEF00000020)) 
    \data_r[1]_i_1 
       (.I0(spi_pll_ld_sdo),
        .I1(data_r1[1]),
        .I2(out[0]),
        .I3(data_r1[2]),
        .I4(\data_r[6]_i_2_n_0 ),
        .I5(\data_r_reg[23]_0 [0]),
        .O(\data_r[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFBFFFF00080000)) 
    \data_r[20]_i_1 
       (.I0(spi_pll_ld_sdo),
        .I1(\data_r[22]_i_2_n_0 ),
        .I2(out[0]),
        .I3(data_r1[1]),
        .I4(data_r1[2]),
        .I5(\data_r_reg[23]_0 [19]),
        .O(\data_r[20]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFBFFFFFF08000000)) 
    \data_r[21]_i_1 
       (.I0(spi_pll_ld_sdo),
        .I1(\data_r[22]_i_2_n_0 ),
        .I2(data_r1[1]),
        .I3(out[0]),
        .I4(data_r1[2]),
        .I5(\data_r_reg[23]_0 [20]),
        .O(\data_r[21]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hEFFFFFFF20000000)) 
    \data_r[22]_i_1 
       (.I0(spi_pll_ld_sdo),
        .I1(out[0]),
        .I2(data_r1[1]),
        .I3(data_r1[2]),
        .I4(\data_r[22]_i_2_n_0 ),
        .I5(\data_r_reg[23]_0 [21]),
        .O(\data_r[22]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000004)) 
    \data_r[22]_i_2 
       (.I0(data_r1[3]),
        .I1(data_r1[4]),
        .I2(\data_r[22]_i_3_n_0 ),
        .I3(\data_r[23]_i_4_n_0 ),
        .I4(\data_r[23]_i_5_n_0 ),
        .I5(\data_r[23]_i_6_n_0 ),
        .O(\data_r[22]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \data_r[22]_i_3 
       (.I0(data_r1[5]),
        .I1(data_r1[14]),
        .I2(data_r1[12]),
        .I3(data_r1[15]),
        .O(\data_r[22]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFEFFFFF00200000)) 
    \data_r[23]_i_1 
       (.I0(spi_pll_ld_sdo),
        .I1(data_r1[3]),
        .I2(data_r1[4]),
        .I3(\data_r[23]_i_2_n_0 ),
        .I4(\data_r[23]_i_3_n_0 ),
        .I5(\data_r_reg[23]_0 [22]),
        .O(\data_r[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \data_r[23]_i_2 
       (.I0(data_r1[15]),
        .I1(data_r1[12]),
        .I2(data_r1[14]),
        .I3(data_r1[5]),
        .I4(\data_r[23]_i_4_n_0 ),
        .I5(\data_r[23]_i_5_n_0 ),
        .O(\data_r[23]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h4000)) 
    \data_r[23]_i_3 
       (.I0(\data_r[23]_i_6_n_0 ),
        .I1(data_r1[2]),
        .I2(out[0]),
        .I3(data_r1[1]),
        .O(\data_r[23]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \data_r[23]_i_4 
       (.I0(data_r1[9]),
        .I1(data_r1[11]),
        .I2(data_r1[6]),
        .I3(data_r1[10]),
        .O(\data_r[23]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hFFFD)) 
    \data_r[23]_i_5 
       (.I0(\data_r1_inferred__0/i__carry__2_n_0 ),
        .I1(data_r1[8]),
        .I2(data_r1[7]),
        .I3(data_r1[13]),
        .O(\data_r[23]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFEFF00FFFF)) 
    \data_r[23]_i_6 
       (.I0(out[0]),
        .I1(out[1]),
        .I2(out[2]),
        .I3(enable_sck_i_2_n_0),
        .I4(mosi_r_i_8_n_0),
        .I5(cnt_reg[5]),
        .O(\data_r[23]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFEF00000020)) 
    \data_r[2]_i_1 
       (.I0(spi_pll_ld_sdo),
        .I1(out[0]),
        .I2(data_r1[1]),
        .I3(data_r1[2]),
        .I4(\data_r[6]_i_2_n_0 ),
        .I5(\data_r_reg[23]_0 [1]),
        .O(\data_r[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFBF00000080)) 
    \data_r[3]_i_1 
       (.I0(spi_pll_ld_sdo),
        .I1(out[0]),
        .I2(data_r1[1]),
        .I3(data_r1[2]),
        .I4(\data_r[6]_i_2_n_0 ),
        .I5(\data_r_reg[23]_0 [2]),
        .O(\data_r[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFEFF00000200)) 
    \data_r[4]_i_1 
       (.I0(spi_pll_ld_sdo),
        .I1(out[0]),
        .I2(data_r1[1]),
        .I3(data_r1[2]),
        .I4(\data_r[6]_i_2_n_0 ),
        .I5(\data_r_reg[23]_0 [3]),
        .O(\data_r[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFEFFF00002000)) 
    \data_r[5]_i_1 
       (.I0(spi_pll_ld_sdo),
        .I1(data_r1[1]),
        .I2(out[0]),
        .I3(data_r1[2]),
        .I4(\data_r[6]_i_2_n_0 ),
        .I5(\data_r_reg[23]_0 [4]),
        .O(\data_r[5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFEFFF00002000)) 
    \data_r[6]_i_1 
       (.I0(spi_pll_ld_sdo),
        .I1(out[0]),
        .I2(data_r1[1]),
        .I3(data_r1[2]),
        .I4(\data_r[6]_i_2_n_0 ),
        .I5(\data_r_reg[23]_0 [5]),
        .O(\data_r[6]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \data_r[6]_i_2 
       (.I0(\data_r[23]_i_6_n_0 ),
        .I1(data_r1[4]),
        .I2(\data_r[23]_i_5_n_0 ),
        .I3(\data_r[23]_i_4_n_0 ),
        .I4(\data_r[22]_i_3_n_0 ),
        .I5(data_r1[3]),
        .O(\data_r[6]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFB00000008)) 
    \data_r[7]_i_1 
       (.I0(spi_pll_ld_sdo),
        .I1(\data_r[23]_i_3_n_0 ),
        .I2(data_r1[4]),
        .I3(\data_r[23]_i_2_n_0 ),
        .I4(data_r1[3]),
        .I5(\data_r_reg[23]_0 [6]),
        .O(\data_r[7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000002)) 
    \data_r[8]_i_1 
       (.I0(spi_pll_ld_sdo),
        .I1(out[0]),
        .I2(data_r1[1]),
        .I3(data_r1[2]),
        .I4(\data_r[14]_i_2_n_0 ),
        .I5(\data_r_reg[23]_0 [7]),
        .O(\data_r[8]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFEF00000020)) 
    \data_r[9]_i_1 
       (.I0(spi_pll_ld_sdo),
        .I1(data_r1[1]),
        .I2(out[0]),
        .I3(data_r1[2]),
        .I4(\data_r[14]_i_2_n_0 ),
        .I5(\data_r_reg[23]_0 [8]),
        .O(\data_r[9]_i_1_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \data_r_reg[0] 
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(\data_r[0]_i_1_n_0 ),
        .Q(ip2mb_reg1),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \data_r_reg[10] 
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(\data_r[10]_i_1_n_0 ),
        .Q(\data_r_reg[23]_0 [9]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \data_r_reg[11] 
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(\data_r[11]_i_1_n_0 ),
        .Q(\data_r_reg[23]_0 [10]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \data_r_reg[12] 
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(\data_r[12]_i_1_n_0 ),
        .Q(\data_r_reg[23]_0 [11]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \data_r_reg[13] 
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(\data_r[13]_i_1_n_0 ),
        .Q(\data_r_reg[23]_0 [12]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \data_r_reg[14] 
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(\data_r[14]_i_1_n_0 ),
        .Q(\data_r_reg[23]_0 [13]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \data_r_reg[15] 
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(\data_r[15]_i_1_n_0 ),
        .Q(\data_r_reg[23]_0 [14]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \data_r_reg[16] 
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(\data_r[16]_i_1_n_0 ),
        .Q(\data_r_reg[23]_0 [15]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \data_r_reg[17] 
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(\data_r[17]_i_1_n_0 ),
        .Q(\data_r_reg[23]_0 [16]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \data_r_reg[18] 
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(\data_r[18]_i_1_n_0 ),
        .Q(\data_r_reg[23]_0 [17]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \data_r_reg[19] 
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(\data_r[19]_i_1_n_0 ),
        .Q(\data_r_reg[23]_0 [18]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \data_r_reg[1] 
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(\data_r[1]_i_1_n_0 ),
        .Q(\data_r_reg[23]_0 [0]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \data_r_reg[20] 
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(\data_r[20]_i_1_n_0 ),
        .Q(\data_r_reg[23]_0 [19]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \data_r_reg[21] 
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(\data_r[21]_i_1_n_0 ),
        .Q(\data_r_reg[23]_0 [20]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \data_r_reg[22] 
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(\data_r[22]_i_1_n_0 ),
        .Q(\data_r_reg[23]_0 [21]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \data_r_reg[23] 
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(\data_r[23]_i_1_n_0 ),
        .Q(\data_r_reg[23]_0 [22]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \data_r_reg[2] 
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(\data_r[2]_i_1_n_0 ),
        .Q(\data_r_reg[23]_0 [1]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \data_r_reg[3] 
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(\data_r[3]_i_1_n_0 ),
        .Q(\data_r_reg[23]_0 [2]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \data_r_reg[4] 
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(\data_r[4]_i_1_n_0 ),
        .Q(\data_r_reg[23]_0 [3]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \data_r_reg[5] 
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(\data_r[5]_i_1_n_0 ),
        .Q(\data_r_reg[23]_0 [4]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \data_r_reg[6] 
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(\data_r[6]_i_1_n_0 ),
        .Q(\data_r_reg[23]_0 [5]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \data_r_reg[7] 
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(\data_r[7]_i_1_n_0 ),
        .Q(\data_r_reg[23]_0 [6]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \data_r_reg[8] 
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(\data_r[8]_i_1_n_0 ),
        .Q(\data_r_reg[23]_0 [7]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \data_r_reg[9] 
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(\data_r[9]_i_1_n_0 ),
        .Q(\data_r_reg[23]_0 [8]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h1111111111111110)) 
    enable_sck_i_1
       (.I0(cnt_reg[5]),
        .I1(enable_sck_i_2_n_0),
        .I2(out[0]),
        .I3(enable_sck_i_3_n_0),
        .I4(cnt_reg[4]),
        .I5(cnt_reg[3]),
        .O(enable_sck0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    enable_sck_i_2
       (.I0(cnt_reg[8]),
        .I1(cnt_reg[9]),
        .I2(\cnt[0]_i_4_n_0 ),
        .I3(cnt_reg[7]),
        .I4(cnt_reg[6]),
        .I5(enable_sck_i_4_n_0),
        .O(enable_sck_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT2 #(
    .INIT(4'hE)) 
    enable_sck_i_3
       (.I0(out[1]),
        .I1(out[2]),
        .O(enable_sck_i_3_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    enable_sck_i_4
       (.I0(cnt_reg[14]),
        .I1(cnt_reg[12]),
        .I2(cnt_reg[15]),
        .I3(cnt_reg[13]),
        .O(enable_sck_i_4_n_0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    enable_sck_reg
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(enable_sck0),
        .Q(enable_sck),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__0_i_1
       (.I0(cnt_reg[5]),
        .O(i__carry__0_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__0_i_2__1
       (.I0(cnt_reg[8]),
        .O(i__carry__0_i_2__1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__0_i_3__1
       (.I0(cnt_reg[7]),
        .O(i__carry__0_i_3__1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__0_i_4
       (.I0(cnt_reg[6]),
        .O(i__carry__0_i_4_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__1_i_1
       (.I0(cnt_reg[12]),
        .O(i__carry__1_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__1_i_2
       (.I0(cnt_reg[11]),
        .O(i__carry__1_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__1_i_3
       (.I0(cnt_reg[10]),
        .O(i__carry__1_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__1_i_4
       (.I0(cnt_reg[9]),
        .O(i__carry__1_i_4_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__2_i_1
       (.I0(cnt_reg[15]),
        .O(i__carry__2_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__2_i_2
       (.I0(cnt_reg[14]),
        .O(i__carry__2_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__2_i_3
       (.I0(cnt_reg[13]),
        .O(i__carry__2_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry_i_1__1
       (.I0(out[0]),
        .O(i__carry_i_1__1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry_i_2
       (.I0(cnt_reg[4]),
        .O(i__carry_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry_i_3__1
       (.I0(cnt_reg[3]),
        .O(i__carry_i_3__1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry_i_4__1
       (.I0(out[2]),
        .O(i__carry_i_4__1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry_i_5__0
       (.I0(out[1]),
        .O(i__carry_i_5__0_n_0));
  LUT6 #(
    .INIT(64'hF5F5F0F3F5F5F0F0)) 
    mosi_r_i_1
       (.I0(mosi_r_reg_0),
        .I1(mosi_r_i_3_n_0),
        .I2(mosi_r_i_4_n_0),
        .I3(mosi_r_i_5_n_0),
        .I4(mosi_r_i_6_n_0),
        .I5(pll_mosi_read),
        .O(mosi_r_i_1_n_0));
  LUT6 #(
    .INIT(64'h0001010144444444)) 
    mosi_r_i_3
       (.I0(enable_sck_i_2_n_0),
        .I1(mosi_r_i_8_n_0),
        .I2(out[2]),
        .I3(out[0]),
        .I4(out[1]),
        .I5(cnt_reg[5]),
        .O(mosi_r_i_3_n_0));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    mosi_r_i_4
       (.I0(mosi_r_i_9__0_n_0),
        .I1(out[2]),
        .I2(cnt_reg[3]),
        .I3(cnt_reg[4]),
        .I4(out[1]),
        .I5(out[0]),
        .O(mosi_r_i_4_n_0));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hF4)) 
    mosi_r_i_5
       (.I0(enable_cnt),
        .I1(START_receive),
        .I2(cs_r_i_2_n_0),
        .O(mosi_r_i_5_n_0));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT5 #(
    .INIT(32'h000000E0)) 
    mosi_r_i_6
       (.I0(out[2]),
        .I1(out[1]),
        .I2(mosi_r_i_9__0_n_0),
        .I3(cnt_reg[4]),
        .I4(cnt_reg[3]),
        .O(mosi_r_i_6_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    mosi_r_i_8
       (.I0(cnt_reg[4]),
        .I1(cnt_reg[3]),
        .O(mosi_r_i_8_n_0));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT2 #(
    .INIT(4'h1)) 
    mosi_r_i_9__0
       (.I0(cnt_reg[5]),
        .I1(enable_sck_i_2_n_0),
        .O(mosi_r_i_9__0_n_0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    mosi_r_reg
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(mosi_r_i_1_n_0),
        .Q(pll_mosi_read),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT5 #(
    .INIT(32'h0000EECE)) 
    ready_r_i_1
       (.I0(pll_data_ready_RX),
        .I1(ready_r0),
        .I2(START_receive),
        .I3(enable_cnt),
        .I4(start_prev),
        .O(ready_r_i_1_n_0));
  LUT6 #(
    .INIT(64'hAAAAAAAA88808888)) 
    ready_r_i_2
       (.I0(ready_r_i_3_n_0),
        .I1(cnt_reg[8]),
        .I2(cnt_reg[7]),
        .I3(cnt_reg[6]),
        .I4(ready_r_i_4_n_0),
        .I5(cnt_reg[9]),
        .O(ready_r0));
  LUT6 #(
    .INIT(64'h0000000000000002)) 
    ready_r_i_3
       (.I0(ready_r_i_5_n_0),
        .I1(\cnt[0]_i_4_n_0 ),
        .I2(cnt_reg[14]),
        .I3(cnt_reg[12]),
        .I4(cnt_reg[15]),
        .I5(cnt_reg[13]),
        .O(ready_r_i_3_n_0));
  LUT6 #(
    .INIT(64'h00001FFFFFFFFFFF)) 
    ready_r_i_4
       (.I0(out[0]),
        .I1(out[1]),
        .I2(cnt_reg[3]),
        .I3(out[2]),
        .I4(cnt_reg[4]),
        .I5(cnt_reg[5]),
        .O(ready_r_i_4_n_0));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    ready_r_i_5
       (.I0(mosi_r_i_8_n_0),
        .I1(cnt_reg[5]),
        .I2(cnt_reg[9]),
        .I3(cnt_reg[6]),
        .I4(cnt_reg[7]),
        .I5(cnt_reg[8]),
        .O(ready_r_i_5_n_0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    ready_r_reg
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(ready_r_i_1_n_0),
        .Q(pll_data_ready_RX),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'h0E)) 
    start_prev_i_1
       (.I0(enable_cnt),
        .I1(START_receive),
        .I2(start_prev),
        .O(start_prev_i_1_n_0));
  LUT6 #(
    .INIT(64'h0002000000000000)) 
    start_prev_i_2
       (.I0(start_prev_i_3_n_0),
        .I1(start_prev_i_4__0_n_0),
        .I2(enable_sck_i_4_n_0),
        .I3(enable_sck_i_3_n_0),
        .I4(cnt_reg[7]),
        .I5(cnt_reg[8]),
        .O(start_prev));
  LUT6 #(
    .INIT(64'h0000000000800000)) 
    start_prev_i_3
       (.I0(cnt_reg[5]),
        .I1(cnt_reg[7]),
        .I2(cnt_reg[6]),
        .I3(cnt_reg[4]),
        .I4(cnt_reg[3]),
        .I5(\cnt[0]_i_4_n_0 ),
        .O(start_prev_i_3_n_0));
  LUT6 #(
    .INIT(64'hF4F4F4F4FFFFF4FF)) 
    start_prev_i_4__0
       (.I0(out[1]),
        .I1(out[0]),
        .I2(out[2]),
        .I3(cnt_reg[9]),
        .I4(cnt_reg[10]),
        .I5(cnt_reg[11]),
        .O(start_prev_i_4__0_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    start_prev_reg
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(start_prev_i_1_n_0),
        .Q(enable_cnt),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_spi_hmc_mode_TX
   (pll_data_ready_TX,
    out,
    \cnt_reg[5]_0 ,
    spi_pll_sck,
    spi_pll_sen,
    spi_pll_mosi,
    clk_SPI_PLL,
    START_send,
    mosi_r_reg_0,
    mosi_r_reg_1,
    enable_sck,
    pll_sen_read,
    pll_mosi_read,
    mosi_r_reg_2);
  output pll_data_ready_TX;
  output [4:0]out;
  output \cnt_reg[5]_0 ;
  output spi_pll_sck;
  output spi_pll_sen;
  output spi_pll_mosi;
  input clk_SPI_PLL;
  input START_send;
  input [0:0]mosi_r_reg_0;
  input mosi_r_reg_1;
  input enable_sck;
  input pll_sen_read;
  input pll_mosi_read;
  input mosi_r_reg_2;

  wire START_send;
  wire clk_SPI_PLL;
  wire \cnt[0]_i_1__0_n_0 ;
  wire \cnt[0]_i_3__0_n_0 ;
  wire \cnt[0]_i_4__0_n_0 ;
  wire \cnt[0]_i_5__0_n_0 ;
  wire \cnt[0]_i_6_n_0 ;
  wire [15:5]cnt_reg;
  wire \cnt_reg[0]_i_2__0_n_0 ;
  wire \cnt_reg[0]_i_2__0_n_1 ;
  wire \cnt_reg[0]_i_2__0_n_2 ;
  wire \cnt_reg[0]_i_2__0_n_3 ;
  wire \cnt_reg[0]_i_2__0_n_4 ;
  wire \cnt_reg[0]_i_2__0_n_5 ;
  wire \cnt_reg[0]_i_2__0_n_6 ;
  wire \cnt_reg[0]_i_2__0_n_7 ;
  wire \cnt_reg[12]_i_1__0_n_1 ;
  wire \cnt_reg[12]_i_1__0_n_2 ;
  wire \cnt_reg[12]_i_1__0_n_3 ;
  wire \cnt_reg[12]_i_1__0_n_4 ;
  wire \cnt_reg[12]_i_1__0_n_5 ;
  wire \cnt_reg[12]_i_1__0_n_6 ;
  wire \cnt_reg[12]_i_1__0_n_7 ;
  wire \cnt_reg[4]_i_1__0_n_0 ;
  wire \cnt_reg[4]_i_1__0_n_1 ;
  wire \cnt_reg[4]_i_1__0_n_2 ;
  wire \cnt_reg[4]_i_1__0_n_3 ;
  wire \cnt_reg[4]_i_1__0_n_4 ;
  wire \cnt_reg[4]_i_1__0_n_5 ;
  wire \cnt_reg[4]_i_1__0_n_6 ;
  wire \cnt_reg[4]_i_1__0_n_7 ;
  wire \cnt_reg[5]_0 ;
  wire \cnt_reg[8]_i_1__0_n_0 ;
  wire \cnt_reg[8]_i_1__0_n_1 ;
  wire \cnt_reg[8]_i_1__0_n_2 ;
  wire \cnt_reg[8]_i_1__0_n_3 ;
  wire \cnt_reg[8]_i_1__0_n_4 ;
  wire \cnt_reg[8]_i_1__0_n_5 ;
  wire \cnt_reg[8]_i_1__0_n_6 ;
  wire \cnt_reg[8]_i_1__0_n_7 ;
  wire cs_r_i_1__0_n_0;
  wire cs_r_i_3__0_n_0;
  wire cs_r_i_4__0_n_0;
  wire cs_r_i_5__0_n_0;
  wire cs_r_i_6_n_0;
  wire cs_r_i_7_n_0;
  wire enable_cnt;
  wire enable_sck;
  wire enable_sck_0;
  wire enable_sck_i_1__0_n_0;
  wire enable_sck_i_2__0_n_0;
  wire enable_sck_i_3__0_n_0;
  wire enable_sck_i_4__0_n_0;
  wire enable_sck_i_5_n_0;
  wire mosi_r_i_13_n_0;
  wire mosi_r_i_14_n_0;
  wire mosi_r_i_1__0_n_0;
  wire mosi_r_i_2_n_0;
  wire mosi_r_i_3__0_n_0;
  wire mosi_r_i_6__0_n_0;
  wire mosi_r_i_8__0_n_0;
  wire mosi_r_i_9_n_0;
  wire [0:0]mosi_r_reg_0;
  wire mosi_r_reg_1;
  wire mosi_r_reg_2;
  wire [4:0]out;
  wire pll_data_ready_TX;
  wire pll_mosi_read;
  wire pll_mosi_write;
  wire pll_sen_read;
  wire pll_sen_write;
  wire ready_tx_r;
  wire ready_tx_r0;
  wire ready_tx_r08_out;
  wire ready_tx_r_i_1_n_0;
  wire ready_tx_r_i_3_n_0;
  wire ready_tx_r_i_4_n_0;
  wire spi_pll_mosi;
  wire spi_pll_sck;
  wire spi_pll_sen;
  wire start_prev_i_1__0_n_0;
  wire start_prev_i_3__0_n_0;
  wire start_prev_i_4_n_0;
  wire [3:3]\NLW_cnt_reg[12]_i_1__0_CO_UNCONNECTED ;

  LUT5 #(
    .INIT(32'h555555D5)) 
    \cnt[0]_i_1__0 
       (.I0(enable_cnt),
        .I1(\cnt[0]_i_3__0_n_0 ),
        .I2(enable_sck_i_3__0_n_0),
        .I3(\cnt[0]_i_4__0_n_0 ),
        .I4(\cnt[0]_i_5__0_n_0 ),
        .O(\cnt[0]_i_1__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT5 #(
    .INIT(32'h00000020)) 
    \cnt[0]_i_3__0 
       (.I0(out[3]),
        .I1(out[4]),
        .I2(cnt_reg[6]),
        .I3(cnt_reg[15]),
        .I4(cnt_reg[12]),
        .O(\cnt[0]_i_3__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \cnt[0]_i_4__0 
       (.I0(cnt_reg[11]),
        .I1(cnt_reg[10]),
        .I2(cnt_reg[14]),
        .I3(cnt_reg[13]),
        .O(\cnt[0]_i_4__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT4 #(
    .INIT(16'h7FFF)) 
    \cnt[0]_i_5__0 
       (.I0(cnt_reg[9]),
        .I1(cnt_reg[5]),
        .I2(cnt_reg[7]),
        .I3(cnt_reg[8]),
        .O(\cnt[0]_i_5__0_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \cnt[0]_i_6 
       (.I0(out[0]),
        .O(\cnt[0]_i_6_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[0] 
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(\cnt_reg[0]_i_2__0_n_7 ),
        .Q(out[0]),
        .R(\cnt[0]_i_1__0_n_0 ));
  CARRY4 \cnt_reg[0]_i_2__0 
       (.CI(1'b0),
        .CO({\cnt_reg[0]_i_2__0_n_0 ,\cnt_reg[0]_i_2__0_n_1 ,\cnt_reg[0]_i_2__0_n_2 ,\cnt_reg[0]_i_2__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\cnt_reg[0]_i_2__0_n_4 ,\cnt_reg[0]_i_2__0_n_5 ,\cnt_reg[0]_i_2__0_n_6 ,\cnt_reg[0]_i_2__0_n_7 }),
        .S({out[3:1],\cnt[0]_i_6_n_0 }));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[10] 
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(\cnt_reg[8]_i_1__0_n_5 ),
        .Q(cnt_reg[10]),
        .R(\cnt[0]_i_1__0_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[11] 
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(\cnt_reg[8]_i_1__0_n_4 ),
        .Q(cnt_reg[11]),
        .R(\cnt[0]_i_1__0_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[12] 
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(\cnt_reg[12]_i_1__0_n_7 ),
        .Q(cnt_reg[12]),
        .R(\cnt[0]_i_1__0_n_0 ));
  CARRY4 \cnt_reg[12]_i_1__0 
       (.CI(\cnt_reg[8]_i_1__0_n_0 ),
        .CO({\NLW_cnt_reg[12]_i_1__0_CO_UNCONNECTED [3],\cnt_reg[12]_i_1__0_n_1 ,\cnt_reg[12]_i_1__0_n_2 ,\cnt_reg[12]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_reg[12]_i_1__0_n_4 ,\cnt_reg[12]_i_1__0_n_5 ,\cnt_reg[12]_i_1__0_n_6 ,\cnt_reg[12]_i_1__0_n_7 }),
        .S(cnt_reg[15:12]));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[13] 
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(\cnt_reg[12]_i_1__0_n_6 ),
        .Q(cnt_reg[13]),
        .R(\cnt[0]_i_1__0_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[14] 
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(\cnt_reg[12]_i_1__0_n_5 ),
        .Q(cnt_reg[14]),
        .R(\cnt[0]_i_1__0_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[15] 
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(\cnt_reg[12]_i_1__0_n_4 ),
        .Q(cnt_reg[15]),
        .R(\cnt[0]_i_1__0_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[1] 
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(\cnt_reg[0]_i_2__0_n_6 ),
        .Q(out[1]),
        .R(\cnt[0]_i_1__0_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[2] 
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(\cnt_reg[0]_i_2__0_n_5 ),
        .Q(out[2]),
        .R(\cnt[0]_i_1__0_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[3] 
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(\cnt_reg[0]_i_2__0_n_4 ),
        .Q(out[3]),
        .R(\cnt[0]_i_1__0_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[4] 
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(\cnt_reg[4]_i_1__0_n_7 ),
        .Q(out[4]),
        .R(\cnt[0]_i_1__0_n_0 ));
  CARRY4 \cnt_reg[4]_i_1__0 
       (.CI(\cnt_reg[0]_i_2__0_n_0 ),
        .CO({\cnt_reg[4]_i_1__0_n_0 ,\cnt_reg[4]_i_1__0_n_1 ,\cnt_reg[4]_i_1__0_n_2 ,\cnt_reg[4]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_reg[4]_i_1__0_n_4 ,\cnt_reg[4]_i_1__0_n_5 ,\cnt_reg[4]_i_1__0_n_6 ,\cnt_reg[4]_i_1__0_n_7 }),
        .S({cnt_reg[7:5],out[4]}));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[5] 
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(\cnt_reg[4]_i_1__0_n_6 ),
        .Q(cnt_reg[5]),
        .R(\cnt[0]_i_1__0_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[6] 
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(\cnt_reg[4]_i_1__0_n_5 ),
        .Q(cnt_reg[6]),
        .R(\cnt[0]_i_1__0_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[7] 
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(\cnt_reg[4]_i_1__0_n_4 ),
        .Q(cnt_reg[7]),
        .R(\cnt[0]_i_1__0_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[8] 
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(\cnt_reg[8]_i_1__0_n_7 ),
        .Q(cnt_reg[8]),
        .R(\cnt[0]_i_1__0_n_0 ));
  CARRY4 \cnt_reg[8]_i_1__0 
       (.CI(\cnt_reg[4]_i_1__0_n_0 ),
        .CO({\cnt_reg[8]_i_1__0_n_0 ,\cnt_reg[8]_i_1__0_n_1 ,\cnt_reg[8]_i_1__0_n_2 ,\cnt_reg[8]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_reg[8]_i_1__0_n_4 ,\cnt_reg[8]_i_1__0_n_5 ,\cnt_reg[8]_i_1__0_n_6 ,\cnt_reg[8]_i_1__0_n_7 }),
        .S(cnt_reg[11:8]));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[9] 
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(\cnt_reg[8]_i_1__0_n_6 ),
        .Q(cnt_reg[9]),
        .R(\cnt[0]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'hEEEEEEEEEEE0EEEE)) 
    cs_r_i_1__0
       (.I0(pll_sen_write),
        .I1(ready_tx_r0),
        .I2(cs_r_i_3__0_n_0),
        .I3(cs_r_i_4__0_n_0),
        .I4(cs_r_i_5__0_n_0),
        .I5(cs_r_i_6_n_0),
        .O(cs_r_i_1__0_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    cs_r_i_2__0
       (.I0(START_send),
        .I1(enable_cnt),
        .O(ready_tx_r0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFFFF)) 
    cs_r_i_3__0
       (.I0(cnt_reg[13]),
        .I1(cnt_reg[14]),
        .I2(cs_r_i_7_n_0),
        .I3(out[4]),
        .I4(cnt_reg[5]),
        .I5(cnt_reg[15]),
        .O(cs_r_i_3__0_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFF2FF)) 
    cs_r_i_4__0
       (.I0(cnt_reg[9]),
        .I1(cnt_reg[10]),
        .I2(cnt_reg[11]),
        .I3(out[0]),
        .I4(out[1]),
        .I5(out[2]),
        .O(cs_r_i_4__0_n_0));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'h8A)) 
    cs_r_i_5__0
       (.I0(cnt_reg[5]),
        .I1(out[4]),
        .I2(out[3]),
        .O(cs_r_i_5__0_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFF2)) 
    cs_r_i_6
       (.I0(cnt_reg[12]),
        .I1(cnt_reg[13]),
        .I2(cnt_reg[14]),
        .I3(cnt_reg[7]),
        .I4(cnt_reg[6]),
        .I5(cnt_reg[8]),
        .O(cs_r_i_6_n_0));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT2 #(
    .INIT(4'hE)) 
    cs_r_i_7
       (.I0(cnt_reg[10]),
        .I1(cnt_reg[11]),
        .O(cs_r_i_7_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    cs_r_reg
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(cs_r_i_1__0_n_0),
        .Q(pll_sen_write),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h00000000AAAAAA2A)) 
    enable_sck_i_1__0
       (.I0(enable_sck_i_2__0_n_0),
        .I1(enable_sck_i_3__0_n_0),
        .I2(enable_sck_i_4__0_n_0),
        .I3(out[4]),
        .I4(out[3]),
        .I5(cnt_reg[5]),
        .O(enable_sck_i_1__0_n_0));
  LUT6 #(
    .INIT(64'h0000000000000100)) 
    enable_sck_i_2__0
       (.I0(cnt_reg[9]),
        .I1(cnt_reg[11]),
        .I2(cnt_reg[10]),
        .I3(enable_sck_i_4__0_n_0),
        .I4(enable_sck_i_5_n_0),
        .I5(cnt_reg[8]),
        .O(enable_sck_i_2__0_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    enable_sck_i_3__0
       (.I0(out[0]),
        .I1(out[2]),
        .I2(out[1]),
        .O(enable_sck_i_3__0_n_0));
  LUT4 #(
    .INIT(16'h0001)) 
    enable_sck_i_4__0
       (.I0(cnt_reg[14]),
        .I1(cnt_reg[13]),
        .I2(cnt_reg[15]),
        .I3(cnt_reg[12]),
        .O(enable_sck_i_4__0_n_0));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT2 #(
    .INIT(4'hE)) 
    enable_sck_i_5
       (.I0(cnt_reg[7]),
        .I1(cnt_reg[6]),
        .O(enable_sck_i_5_n_0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    enable_sck_reg
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(enable_sck_i_1__0_n_0),
        .Q(enable_sck_0),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT2 #(
    .INIT(4'hE)) 
    mosi_r_i_13
       (.I0(out[4]),
        .I1(out[3]),
        .O(mosi_r_i_13_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFDFFFFFFFF)) 
    mosi_r_i_14
       (.I0(cnt_reg[5]),
        .I1(out[4]),
        .I2(out[3]),
        .I3(out[2]),
        .I4(out[1]),
        .I5(out[0]),
        .O(mosi_r_i_14_n_0));
  LUT6 #(
    .INIT(64'hFF0C080C000C080C)) 
    mosi_r_i_1__0
       (.I0(mosi_r_i_2_n_0),
        .I1(mosi_r_i_3__0_n_0),
        .I2(mosi_r_reg_2),
        .I3(\cnt_reg[5]_0 ),
        .I4(mosi_r_i_6__0_n_0),
        .I5(pll_mosi_write),
        .O(mosi_r_i_1__0_n_0));
  LUT6 #(
    .INIT(64'h00000000FF800080)) 
    mosi_r_i_2
       (.I0(out[1]),
        .I1(out[0]),
        .I2(mosi_r_reg_0),
        .I3(out[2]),
        .I4(mosi_r_reg_1),
        .I5(mosi_r_i_8__0_n_0),
        .O(mosi_r_i_2_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFEFFFFFFFF)) 
    mosi_r_i_3__0
       (.I0(mosi_r_i_9_n_0),
        .I1(cnt_reg[9]),
        .I2(cnt_reg[11]),
        .I3(cnt_reg[10]),
        .I4(enable_sck_i_5_n_0),
        .I5(enable_sck_i_4__0_n_0),
        .O(mosi_r_i_3__0_n_0));
  LUT6 #(
    .INIT(64'hBBB9B9B9FFFFFFFF)) 
    mosi_r_i_5__0
       (.I0(cnt_reg[5]),
        .I1(mosi_r_i_13_n_0),
        .I2(out[2]),
        .I3(out[1]),
        .I4(out[0]),
        .I5(enable_sck_i_2__0_n_0),
        .O(\cnt_reg[5]_0 ));
  LUT6 #(
    .INIT(64'h00000000FEFF00FF)) 
    mosi_r_i_6__0
       (.I0(mosi_r_i_13_n_0),
        .I1(cnt_reg[5]),
        .I2(enable_sck_i_3__0_n_0),
        .I3(enable_sck_i_2__0_n_0),
        .I4(mosi_r_i_14_n_0),
        .I5(ready_tx_r0),
        .O(mosi_r_i_6__0_n_0));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT4 #(
    .INIT(16'hFEFF)) 
    mosi_r_i_8__0
       (.I0(out[4]),
        .I1(out[3]),
        .I2(cnt_reg[5]),
        .I3(enable_sck_i_2__0_n_0),
        .O(mosi_r_i_8__0_n_0));
  LUT6 #(
    .INIT(64'hFFFFFEFFFFFFFFFF)) 
    mosi_r_i_9
       (.I0(mosi_r_i_13_n_0),
        .I1(out[2]),
        .I2(cnt_reg[8]),
        .I3(cnt_reg[5]),
        .I4(out[1]),
        .I5(out[0]),
        .O(mosi_r_i_9_n_0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    mosi_r_reg
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(mosi_r_i_1__0_n_0),
        .Q(pll_mosi_write),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT5 #(
    .INIT(32'h0000EECE)) 
    ready_tx_r_i_1
       (.I0(pll_data_ready_TX),
        .I1(ready_tx_r08_out),
        .I2(START_send),
        .I3(enable_cnt),
        .I4(ready_tx_r),
        .O(ready_tx_r_i_1_n_0));
  LUT6 #(
    .INIT(64'hAAA8AAAA88888888)) 
    ready_tx_r_i_2
       (.I0(ready_tx_r_i_3_n_0),
        .I1(cnt_reg[9]),
        .I2(cnt_reg[7]),
        .I3(cnt_reg[6]),
        .I4(ready_tx_r_i_4_n_0),
        .I5(cnt_reg[8]),
        .O(ready_tx_r08_out));
  LUT6 #(
    .INIT(64'h000000000000F700)) 
    ready_tx_r_i_3
       (.I0(cnt_reg[6]),
        .I1(mosi_r_i_13_n_0),
        .I2(\cnt[0]_i_5__0_n_0 ),
        .I3(enable_sck_i_4__0_n_0),
        .I4(cnt_reg[10]),
        .I5(cnt_reg[11]),
        .O(ready_tx_r_i_3_n_0));
  LUT6 #(
    .INIT(64'h555555557F7F7FFF)) 
    ready_tx_r_i_4
       (.I0(cnt_reg[5]),
        .I1(out[3]),
        .I2(out[2]),
        .I3(out[0]),
        .I4(out[1]),
        .I5(out[4]),
        .O(ready_tx_r_i_4_n_0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    ready_tx_r_reg
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(ready_tx_r_i_1_n_0),
        .Q(pll_data_ready_TX),
        .R(1'b0));
  LUT2 #(
    .INIT(4'hE)) 
    spi_pll_mosi_INST_0
       (.I0(pll_mosi_write),
        .I1(pll_mosi_read),
        .O(spi_pll_mosi));
  LUT3 #(
    .INIT(8'hA8)) 
    spi_pll_sck_INST_0
       (.I0(clk_SPI_PLL),
        .I1(enable_sck_0),
        .I2(enable_sck),
        .O(spi_pll_sck));
  LUT2 #(
    .INIT(4'hE)) 
    spi_pll_sen_INST_0
       (.I0(pll_sen_write),
        .I1(pll_sen_read),
        .O(spi_pll_sen));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'h0E)) 
    start_prev_i_1__0
       (.I0(enable_cnt),
        .I1(START_send),
        .I2(ready_tx_r),
        .O(start_prev_i_1__0_n_0));
  LUT6 #(
    .INIT(64'h0000000040000000)) 
    start_prev_i_2__0
       (.I0(start_prev_i_3__0_n_0),
        .I1(start_prev_i_4_n_0),
        .I2(cnt_reg[7]),
        .I3(cnt_reg[8]),
        .I4(enable_sck_i_3__0_n_0),
        .I5(cs_r_i_3__0_n_0),
        .O(ready_tx_r));
  LUT6 #(
    .INIT(64'hF2F2F2F2FFFFF2FF)) 
    start_prev_i_3__0
       (.I0(cnt_reg[12]),
        .I1(cnt_reg[13]),
        .I2(cnt_reg[14]),
        .I3(cnt_reg[9]),
        .I4(cnt_reg[10]),
        .I5(cnt_reg[11]),
        .O(start_prev_i_3__0_n_0));
  LUT6 #(
    .INIT(64'h0000400040404040)) 
    start_prev_i_4
       (.I0(out[2]),
        .I1(cnt_reg[7]),
        .I2(cnt_reg[6]),
        .I3(out[3]),
        .I4(out[4]),
        .I5(cnt_reg[5]),
        .O(start_prev_i_4_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    start_prev_reg
       (.C(clk_SPI_PLL),
        .CE(1'b1),
        .D(start_prev_i_1__0_n_0),
        .Q(enable_cnt),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_top_PLL_control
   (spi_pll_cen,
    PLL_POW_EN,
    LOW_AMP_EN,
    PAMP_EN,
    out,
    pll_trig,
    start_adc_count,
    \cnt_reg[4] ,
    \cnt_reg[5] ,
    spi_pll_sck,
    \data_r_reg[0] ,
    \data_r_reg[23] ,
    spi_pll_sen,
    spi_pll_mosi,
    ATTEN,
    clk_SPI_PLL,
    clk_10MHz,
    enable_triger_CMD,
    s00_axi_aclk,
    s00_axi_aresetn,
    LOW_AMP_EN_reg_reg_0,
    slv_reg0,
    clkDCO_10MHz,
    azimut_0,
    mosi_r_reg,
    mosi_r_reg_0,
    \axi_rdata_reg[0] ,
    \axi_rdata_reg[0]_0 ,
    mosi_r_reg_1,
    mosi_r_reg_2,
    spi_pll_ld_sdo,
    D,
    SS,
    \atten_reg_reg[5]_0 ,
    \shift_front_from_cpu_reg[15]_0 );
  output spi_pll_cen;
  output PLL_POW_EN;
  output LOW_AMP_EN;
  output PAMP_EN;
  output [2:0]out;
  output pll_trig;
  output start_adc_count;
  output [4:0]\cnt_reg[4] ;
  output \cnt_reg[5] ;
  output spi_pll_sck;
  output \data_r_reg[0] ;
  output [22:0]\data_r_reg[23] ;
  output spi_pll_sen;
  output spi_pll_mosi;
  output [5:0]ATTEN;
  input clk_SPI_PLL;
  input clk_10MHz;
  input enable_triger_CMD;
  input s00_axi_aclk;
  input s00_axi_aresetn;
  input [3:0]LOW_AMP_EN_reg_reg_0;
  input [1:0]slv_reg0;
  input clkDCO_10MHz;
  input azimut_0;
  input [0:0]mosi_r_reg;
  input mosi_r_reg_0;
  input [1:0]\axi_rdata_reg[0] ;
  input \axi_rdata_reg[0]_0 ;
  input mosi_r_reg_1;
  input mosi_r_reg_2;
  input spi_pll_ld_sdo;
  input [15:0]D;
  input [0:0]SS;
  input [5:0]\atten_reg_reg[5]_0 ;
  input [15:0]\shift_front_from_cpu_reg[15]_0 ;

  wire [5:0]ATTEN;
  wire [15:0]D;
  wire LOW_AMP_EN;
  wire [3:0]LOW_AMP_EN_reg_reg_0;
  wire PAMP_EN;
  wire PLL_POW_EN;
  wire [0:0]SS;
  wire START_receive;
  wire START_send;
  wire [5:0]\atten_reg_reg[5]_0 ;
  wire [1:0]\axi_rdata_reg[0] ;
  wire \axi_rdata_reg[0]_0 ;
  wire azimut_0;
  wire clkDCO_10MHz;
  wire clk_10MHz;
  wire clk_SPI_PLL;
  wire [4:0]\cnt_reg[4] ;
  wire \cnt_reg[5] ;
  wire \data_r_reg[0] ;
  wire [22:0]\data_r_reg[23] ;
  wire enable_sck;
  wire enable_triger;
  wire enable_triger_CMD;
  wire enable_triger_CMD_reg_n_0;
  wire enable_triger_i_1_n_0;
  wire i__carry__0_i_1__0_n_0;
  wire i__carry__0_i_1__1_n_0;
  wire i__carry__0_i_1__1_n_2;
  wire i__carry__0_i_1__1_n_3;
  wire i__carry__0_i_2__0_n_0;
  wire i__carry__0_i_2_n_0;
  wire i__carry__0_i_3__0_n_0;
  wire i__carry__0_i_3_n_0;
  wire i__carry__0_i_4__0_n_0;
  wire i__carry__0_i_4__1_n_0;
  wire i__carry__0_i_5__0_n_0;
  wire i__carry__0_i_5_n_0;
  wire i__carry__0_i_6__0_n_0;
  wire i__carry__0_i_6_n_0;
  wire i__carry__0_i_7_n_0;
  wire i__carry__0_i_8_n_0;
  wire i__carry_i_10_n_0;
  wire i__carry_i_11_n_0;
  wire i__carry_i_12_n_0;
  wire i__carry_i_13_n_0;
  wire i__carry_i_14_n_0;
  wire i__carry_i_15_n_0;
  wire i__carry_i_16_n_0;
  wire i__carry_i_17_n_0;
  wire i__carry_i_18_n_0;
  wire i__carry_i_19_n_0;
  wire i__carry_i_1__0_n_0;
  wire i__carry_i_1_n_0;
  wire i__carry_i_2__0_n_0;
  wire i__carry_i_2__1_n_0;
  wire i__carry_i_3__0_n_0;
  wire i__carry_i_3_n_0;
  wire i__carry_i_4__0_n_0;
  wire i__carry_i_4_n_0;
  wire i__carry_i_5__1_n_0;
  wire i__carry_i_5__1_n_1;
  wire i__carry_i_5__1_n_2;
  wire i__carry_i_5__1_n_3;
  wire i__carry_i_5_n_0;
  wire i__carry_i_6__0_n_0;
  wire i__carry_i_6__0_n_1;
  wire i__carry_i_6__0_n_2;
  wire i__carry_i_6__0_n_3;
  wire i__carry_i_6_n_0;
  wire i__carry_i_7__0_n_0;
  wire i__carry_i_7__0_n_1;
  wire i__carry_i_7__0_n_2;
  wire i__carry_i_7__0_n_3;
  wire i__carry_i_7_n_0;
  wire i__carry_i_8__0_n_0;
  wire i__carry_i_8_n_0;
  wire i__carry_i_9_n_0;
  wire [0:0]mosi_r_reg;
  wire mosi_r_reg_0;
  wire mosi_r_reg_1;
  wire mosi_r_reg_2;
  wire [2:0]out;
  wire [15:1]p_2_in;
  wire p_4_in;
  wire pll_data_ready_TX;
  wire pll_mosi_read;
  wire pll_sen_read;
  wire pll_trig;
  wire pll_trig_reg2_i_1_n_0;
  wire pll_trig_reg2_i_2_n_0;
  wire pll_trig_reg2_i_3_n_0;
  wire pll_trig_reg2_i_4_n_0;
  wire pll_trig_reg2_i_5_n_0;
  wire pll_trig_reg2_i_6_n_0;
  wire pll_trig_reg2_i_7_n_0;
  wire pll_trig_reg2_i_8_n_0;
  wire s00_axi_aclk;
  wire s00_axi_aresetn;
  wire [15:0]shift_front_from_cpu;
  wire [15:0]\shift_front_from_cpu_reg[15]_0 ;
  wire [1:0]slv_reg0;
  wire spi_pll_cen;
  wire spi_pll_ld_sdo;
  wire spi_pll_mosi;
  wire spi_pll_sck;
  wire spi_pll_sen;
  wire start_adc_count;
  wire start_adc_count_r0;
  wire start_adc_count_r0_carry__0_i_1_n_0;
  wire start_adc_count_r0_carry__0_i_2_n_0;
  wire start_adc_count_r0_carry__0_n_3;
  wire start_adc_count_r0_carry_i_1_n_0;
  wire start_adc_count_r0_carry_i_2_n_0;
  wire start_adc_count_r0_carry_i_3_n_0;
  wire start_adc_count_r0_carry_i_4_n_0;
  wire start_adc_count_r0_carry_n_0;
  wire start_adc_count_r0_carry_n_1;
  wire start_adc_count_r0_carry_n_2;
  wire start_adc_count_r0_carry_n_3;
  wire start_adc_count_r10_in;
  wire \start_adc_count_r1_inferred__0/i__carry__0_n_0 ;
  wire \start_adc_count_r1_inferred__0/i__carry__0_n_1 ;
  wire \start_adc_count_r1_inferred__0/i__carry__0_n_2 ;
  wire \start_adc_count_r1_inferred__0/i__carry__0_n_3 ;
  wire \start_adc_count_r1_inferred__0/i__carry__1_n_2 ;
  wire \start_adc_count_r1_inferred__0/i__carry__1_n_3 ;
  wire \start_adc_count_r1_inferred__0/i__carry_n_0 ;
  wire \start_adc_count_r1_inferred__0/i__carry_n_1 ;
  wire \start_adc_count_r1_inferred__0/i__carry_n_2 ;
  wire \start_adc_count_r1_inferred__0/i__carry_n_3 ;
  wire [15:1]start_adc_count_r2;
  wire start_adc_count_r_i_1_n_0;
  wire start_adc_count_r_i_2_n_0;
  wire start_adc_count_r_i_3_n_0;
  wire start_adc_count_r_i_4_n_0;
  wire start_adc_count_r_i_5_n_0;
  wire [15:0]sweep_val;
  wire [15:0]sweep_val_tmp;
  wire tguard_counter;
  wire \tguard_counter[0]_i_1_n_0 ;
  wire \tguard_counter[15]_i_1_n_0 ;
  wire \tguard_counter[15]_i_4_n_0 ;
  wire \tguard_counter[15]_i_5_n_0 ;
  wire \tguard_counter[15]_i_6_n_0 ;
  wire \tguard_counter[15]_i_7_n_0 ;
  wire \tguard_counter[15]_i_8_n_0 ;
  wire \tguard_counter_reg[12]_i_1_n_0 ;
  wire \tguard_counter_reg[12]_i_1_n_1 ;
  wire \tguard_counter_reg[12]_i_1_n_2 ;
  wire \tguard_counter_reg[12]_i_1_n_3 ;
  wire \tguard_counter_reg[15]_i_3_n_2 ;
  wire \tguard_counter_reg[15]_i_3_n_3 ;
  wire \tguard_counter_reg[4]_i_1_n_0 ;
  wire \tguard_counter_reg[4]_i_1_n_1 ;
  wire \tguard_counter_reg[4]_i_1_n_2 ;
  wire \tguard_counter_reg[4]_i_1_n_3 ;
  wire \tguard_counter_reg[8]_i_1_n_0 ;
  wire \tguard_counter_reg[8]_i_1_n_1 ;
  wire \tguard_counter_reg[8]_i_1_n_2 ;
  wire \tguard_counter_reg[8]_i_1_n_3 ;
  wire \tguard_counter_reg_n_0_[0] ;
  wire \tguard_counter_reg_n_0_[10] ;
  wire \tguard_counter_reg_n_0_[11] ;
  wire \tguard_counter_reg_n_0_[12] ;
  wire \tguard_counter_reg_n_0_[13] ;
  wire \tguard_counter_reg_n_0_[14] ;
  wire \tguard_counter_reg_n_0_[15] ;
  wire \tguard_counter_reg_n_0_[1] ;
  wire \tguard_counter_reg_n_0_[2] ;
  wire \tguard_counter_reg_n_0_[3] ;
  wire \tguard_counter_reg_n_0_[4] ;
  wire \tguard_counter_reg_n_0_[5] ;
  wire \tguard_counter_reg_n_0_[6] ;
  wire \tguard_counter_reg_n_0_[7] ;
  wire \tguard_counter_reg_n_0_[8] ;
  wire \tguard_counter_reg_n_0_[9] ;
  wire tsweep_counter118_in;
  wire \tsweep_counter1_inferred__0/i__carry__0_n_1 ;
  wire \tsweep_counter1_inferred__0/i__carry__0_n_2 ;
  wire \tsweep_counter1_inferred__0/i__carry__0_n_3 ;
  wire \tsweep_counter1_inferred__0/i__carry_n_0 ;
  wire \tsweep_counter1_inferred__0/i__carry_n_1 ;
  wire \tsweep_counter1_inferred__0/i__carry_n_2 ;
  wire \tsweep_counter1_inferred__0/i__carry_n_3 ;
  wire tsweep_counter2;
  wire tsweep_counter2_carry__0_i_1_n_0;
  wire tsweep_counter2_carry__0_i_2_n_0;
  wire tsweep_counter2_carry__0_n_3;
  wire tsweep_counter2_carry_i_1_n_0;
  wire tsweep_counter2_carry_i_2_n_0;
  wire tsweep_counter2_carry_i_3_n_0;
  wire tsweep_counter2_carry_i_4_n_0;
  wire tsweep_counter2_carry_n_0;
  wire tsweep_counter2_carry_n_1;
  wire tsweep_counter2_carry_n_2;
  wire tsweep_counter2_carry_n_3;
  wire \tsweep_counter[0]_i_1_n_0 ;
  wire \tsweep_counter[0]_i_2_n_0 ;
  wire \tsweep_counter[0]_i_5_n_0 ;
  wire \tsweep_counter[0]_i_6_n_0 ;
  wire \tsweep_counter[0]_i_7_n_0 ;
  wire \tsweep_counter[0]_i_8_n_0 ;
  wire \tsweep_counter[12]_i_2_n_0 ;
  wire \tsweep_counter[12]_i_3_n_0 ;
  wire \tsweep_counter[12]_i_4_n_0 ;
  wire \tsweep_counter[12]_i_5_n_0 ;
  wire \tsweep_counter[4]_i_2_n_0 ;
  wire \tsweep_counter[4]_i_3_n_0 ;
  wire \tsweep_counter[4]_i_4_n_0 ;
  wire \tsweep_counter[4]_i_5_n_0 ;
  wire \tsweep_counter[8]_i_2_n_0 ;
  wire \tsweep_counter[8]_i_3_n_0 ;
  wire \tsweep_counter[8]_i_4_n_0 ;
  wire \tsweep_counter[8]_i_5_n_0 ;
  wire [15:0]tsweep_counter_reg;
  wire \tsweep_counter_reg[0]_i_3_n_0 ;
  wire \tsweep_counter_reg[0]_i_3_n_1 ;
  wire \tsweep_counter_reg[0]_i_3_n_2 ;
  wire \tsweep_counter_reg[0]_i_3_n_3 ;
  wire \tsweep_counter_reg[0]_i_3_n_4 ;
  wire \tsweep_counter_reg[0]_i_3_n_5 ;
  wire \tsweep_counter_reg[0]_i_3_n_6 ;
  wire \tsweep_counter_reg[0]_i_3_n_7 ;
  wire \tsweep_counter_reg[12]_i_1_n_1 ;
  wire \tsweep_counter_reg[12]_i_1_n_2 ;
  wire \tsweep_counter_reg[12]_i_1_n_3 ;
  wire \tsweep_counter_reg[12]_i_1_n_4 ;
  wire \tsweep_counter_reg[12]_i_1_n_5 ;
  wire \tsweep_counter_reg[12]_i_1_n_6 ;
  wire \tsweep_counter_reg[12]_i_1_n_7 ;
  wire \tsweep_counter_reg[4]_i_1_n_0 ;
  wire \tsweep_counter_reg[4]_i_1_n_1 ;
  wire \tsweep_counter_reg[4]_i_1_n_2 ;
  wire \tsweep_counter_reg[4]_i_1_n_3 ;
  wire \tsweep_counter_reg[4]_i_1_n_4 ;
  wire \tsweep_counter_reg[4]_i_1_n_5 ;
  wire \tsweep_counter_reg[4]_i_1_n_6 ;
  wire \tsweep_counter_reg[4]_i_1_n_7 ;
  wire \tsweep_counter_reg[8]_i_1_n_0 ;
  wire \tsweep_counter_reg[8]_i_1_n_1 ;
  wire \tsweep_counter_reg[8]_i_1_n_2 ;
  wire \tsweep_counter_reg[8]_i_1_n_3 ;
  wire \tsweep_counter_reg[8]_i_1_n_4 ;
  wire \tsweep_counter_reg[8]_i_1_n_5 ;
  wire \tsweep_counter_reg[8]_i_1_n_6 ;
  wire \tsweep_counter_reg[8]_i_1_n_7 ;
  wire [2:2]NLW_i__carry__0_i_1__1_CO_UNCONNECTED;
  wire [3:3]NLW_i__carry__0_i_1__1_O_UNCONNECTED;
  wire [3:0]NLW_start_adc_count_r0_carry_O_UNCONNECTED;
  wire [3:2]NLW_start_adc_count_r0_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_start_adc_count_r0_carry__0_O_UNCONNECTED;
  wire [3:0]\NLW_start_adc_count_r1_inferred__0/i__carry_O_UNCONNECTED ;
  wire [3:0]\NLW_start_adc_count_r1_inferred__0/i__carry__0_O_UNCONNECTED ;
  wire [3:3]\NLW_start_adc_count_r1_inferred__0/i__carry__1_CO_UNCONNECTED ;
  wire [3:0]\NLW_start_adc_count_r1_inferred__0/i__carry__1_O_UNCONNECTED ;
  wire [3:2]\NLW_tguard_counter_reg[15]_i_3_CO_UNCONNECTED ;
  wire [3:3]\NLW_tguard_counter_reg[15]_i_3_O_UNCONNECTED ;
  wire [3:0]\NLW_tsweep_counter1_inferred__0/i__carry_O_UNCONNECTED ;
  wire [3:0]\NLW_tsweep_counter1_inferred__0/i__carry__0_O_UNCONNECTED ;
  wire [3:0]NLW_tsweep_counter2_carry_O_UNCONNECTED;
  wire [3:2]NLW_tsweep_counter2_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_tsweep_counter2_carry__0_O_UNCONNECTED;
  wire [3:3]\NLW_tsweep_counter_reg[12]_i_1_CO_UNCONNECTED ;

  FDRE LOW_AMP_EN_reg_reg
       (.C(s00_axi_aclk),
        .CE(s00_axi_aresetn),
        .D(LOW_AMP_EN_reg_reg_0[3]),
        .Q(LOW_AMP_EN),
        .R(1'b0));
  FDRE PAMP_EN_reg_reg
       (.C(s00_axi_aclk),
        .CE(s00_axi_aresetn),
        .D(LOW_AMP_EN_reg_reg_0[1]),
        .Q(PAMP_EN),
        .R(1'b0));
  FDRE PLL_POW_EN_reg_reg
       (.C(s00_axi_aclk),
        .CE(s00_axi_aresetn),
        .D(LOW_AMP_EN_reg_reg_0[0]),
        .Q(PLL_POW_EN),
        .R(1'b0));
  FDRE START_receive_reg
       (.C(s00_axi_aclk),
        .CE(s00_axi_aresetn),
        .D(slv_reg0[1]),
        .Q(START_receive),
        .R(1'b0));
  FDRE START_send_reg
       (.C(s00_axi_aclk),
        .CE(s00_axi_aresetn),
        .D(slv_reg0[0]),
        .Q(START_send),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \atten_reg_reg[0] 
       (.C(s00_axi_aclk),
        .CE(s00_axi_aresetn),
        .D(\atten_reg_reg[5]_0 [0]),
        .Q(ATTEN[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \atten_reg_reg[1] 
       (.C(s00_axi_aclk),
        .CE(s00_axi_aresetn),
        .D(\atten_reg_reg[5]_0 [1]),
        .Q(ATTEN[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \atten_reg_reg[2] 
       (.C(s00_axi_aclk),
        .CE(s00_axi_aresetn),
        .D(\atten_reg_reg[5]_0 [2]),
        .Q(ATTEN[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \atten_reg_reg[3] 
       (.C(s00_axi_aclk),
        .CE(s00_axi_aresetn),
        .D(\atten_reg_reg[5]_0 [3]),
        .Q(ATTEN[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \atten_reg_reg[4] 
       (.C(s00_axi_aclk),
        .CE(s00_axi_aresetn),
        .D(\atten_reg_reg[5]_0 [4]),
        .Q(ATTEN[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \atten_reg_reg[5] 
       (.C(s00_axi_aclk),
        .CE(s00_axi_aresetn),
        .D(\atten_reg_reg[5]_0 [5]),
        .Q(ATTEN[5]),
        .R(1'b0));
  FDRE enable_triger_CMD_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(enable_triger_CMD),
        .Q(enable_triger_CMD_reg_n_0),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    enable_triger_i_1
       (.I0(enable_triger_CMD_reg_n_0),
        .I1(azimut_0),
        .I2(enable_triger),
        .O(enable_triger_i_1_n_0));
  FDRE enable_triger_reg
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(enable_triger_i_1_n_0),
        .Q(enable_triger),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h4D44)) 
    i__carry__0_i_1__0
       (.I0(tsweep_counter_reg[15]),
        .I1(sweep_val[15]),
        .I2(tsweep_counter_reg[14]),
        .I3(sweep_val[14]),
        .O(i__carry__0_i_1__0_n_0));
  CARRY4 i__carry__0_i_1__1
       (.CI(i__carry_i_5__1_n_0),
        .CO({i__carry__0_i_1__1_n_0,NLW_i__carry__0_i_1__1_CO_UNCONNECTED[2],i__carry__0_i_1__1_n_2,i__carry__0_i_1__1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,sweep_val[15:13]}),
        .O({NLW_i__carry__0_i_1__1_O_UNCONNECTED[3],start_adc_count_r2[15:13]}),
        .S({1'b1,i__carry__0_i_4__1_n_0,i__carry__0_i_5__0_n_0,i__carry__0_i_6__0_n_0}));
  LUT4 #(
    .INIT(16'h44D4)) 
    i__carry__0_i_2
       (.I0(tsweep_counter_reg[13]),
        .I1(sweep_val[13]),
        .I2(sweep_val[12]),
        .I3(tsweep_counter_reg[12]),
        .O(i__carry__0_i_2_n_0));
  LUT3 #(
    .INIT(8'h82)) 
    i__carry__0_i_2__0
       (.I0(i__carry__0_i_1__1_n_0),
        .I1(start_adc_count_r2[15]),
        .I2(tsweep_counter_reg[15]),
        .O(i__carry__0_i_2__0_n_0));
  LUT4 #(
    .INIT(16'h44D4)) 
    i__carry__0_i_3
       (.I0(tsweep_counter_reg[11]),
        .I1(sweep_val[11]),
        .I2(sweep_val[10]),
        .I3(tsweep_counter_reg[10]),
        .O(i__carry__0_i_3_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    i__carry__0_i_3__0
       (.I0(tsweep_counter_reg[12]),
        .I1(start_adc_count_r2[12]),
        .I2(tsweep_counter_reg[13]),
        .I3(start_adc_count_r2[13]),
        .I4(start_adc_count_r2[14]),
        .I5(tsweep_counter_reg[14]),
        .O(i__carry__0_i_3__0_n_0));
  LUT4 #(
    .INIT(16'h44D4)) 
    i__carry__0_i_4__0
       (.I0(tsweep_counter_reg[9]),
        .I1(sweep_val[9]),
        .I2(sweep_val[8]),
        .I3(tsweep_counter_reg[8]),
        .O(i__carry__0_i_4__0_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__0_i_4__1
       (.I0(sweep_val[15]),
        .O(i__carry__0_i_4__1_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_5
       (.I0(sweep_val[15]),
        .I1(tsweep_counter_reg[15]),
        .I2(sweep_val[14]),
        .I3(tsweep_counter_reg[14]),
        .O(i__carry__0_i_5_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__0_i_5__0
       (.I0(sweep_val[14]),
        .O(i__carry__0_i_5__0_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_6
       (.I0(sweep_val[13]),
        .I1(tsweep_counter_reg[13]),
        .I2(sweep_val[12]),
        .I3(tsweep_counter_reg[12]),
        .O(i__carry__0_i_6_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__0_i_6__0
       (.I0(sweep_val[13]),
        .O(i__carry__0_i_6__0_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_7
       (.I0(sweep_val[11]),
        .I1(tsweep_counter_reg[11]),
        .I2(sweep_val[10]),
        .I3(tsweep_counter_reg[10]),
        .O(i__carry__0_i_7_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_8
       (.I0(sweep_val[9]),
        .I1(tsweep_counter_reg[9]),
        .I2(sweep_val[8]),
        .I3(tsweep_counter_reg[8]),
        .O(i__carry__0_i_8_n_0));
  LUT4 #(
    .INIT(16'h44D4)) 
    i__carry_i_1
       (.I0(tsweep_counter_reg[7]),
        .I1(sweep_val[7]),
        .I2(sweep_val[6]),
        .I3(tsweep_counter_reg[6]),
        .O(i__carry_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry_i_10
       (.I0(sweep_val[10]),
        .O(i__carry_i_10_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry_i_11
       (.I0(sweep_val[9]),
        .O(i__carry_i_11_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry_i_12
       (.I0(sweep_val[8]),
        .O(i__carry_i_12_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry_i_13
       (.I0(sweep_val[7]),
        .O(i__carry_i_13_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry_i_14
       (.I0(sweep_val[6]),
        .O(i__carry_i_14_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry_i_15
       (.I0(sweep_val[5]),
        .O(i__carry_i_15_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry_i_16
       (.I0(sweep_val[4]),
        .O(i__carry_i_16_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry_i_17
       (.I0(sweep_val[3]),
        .O(i__carry_i_17_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry_i_18
       (.I0(sweep_val[2]),
        .O(i__carry_i_18_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry_i_19
       (.I0(sweep_val[1]),
        .O(i__carry_i_19_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    i__carry_i_1__0
       (.I0(tsweep_counter_reg[10]),
        .I1(start_adc_count_r2[10]),
        .I2(tsweep_counter_reg[9]),
        .I3(start_adc_count_r2[9]),
        .I4(start_adc_count_r2[11]),
        .I5(tsweep_counter_reg[11]),
        .O(i__carry_i_1__0_n_0));
  LUT4 #(
    .INIT(16'h44D4)) 
    i__carry_i_2__0
       (.I0(tsweep_counter_reg[5]),
        .I1(sweep_val[5]),
        .I2(sweep_val[4]),
        .I3(tsweep_counter_reg[4]),
        .O(i__carry_i_2__0_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    i__carry_i_2__1
       (.I0(start_adc_count_r2[8]),
        .I1(tsweep_counter_reg[8]),
        .I2(tsweep_counter_reg[6]),
        .I3(start_adc_count_r2[6]),
        .I4(tsweep_counter_reg[7]),
        .I5(start_adc_count_r2[7]),
        .O(i__carry_i_2__1_n_0));
  LUT4 #(
    .INIT(16'h44D4)) 
    i__carry_i_3
       (.I0(tsweep_counter_reg[3]),
        .I1(sweep_val[3]),
        .I2(sweep_val[2]),
        .I3(tsweep_counter_reg[2]),
        .O(i__carry_i_3_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    i__carry_i_3__0
       (.I0(tsweep_counter_reg[5]),
        .I1(start_adc_count_r2[5]),
        .I2(tsweep_counter_reg[3]),
        .I3(start_adc_count_r2[3]),
        .I4(start_adc_count_r2[4]),
        .I5(tsweep_counter_reg[4]),
        .O(i__carry_i_3__0_n_0));
  LUT4 #(
    .INIT(16'h4D44)) 
    i__carry_i_4
       (.I0(tsweep_counter_reg[1]),
        .I1(sweep_val[1]),
        .I2(tsweep_counter_reg[0]),
        .I3(sweep_val[0]),
        .O(i__carry_i_4_n_0));
  LUT6 #(
    .INIT(64'h6006000000006006)) 
    i__carry_i_4__0
       (.I0(sweep_val[0]),
        .I1(tsweep_counter_reg[0]),
        .I2(tsweep_counter_reg[2]),
        .I3(start_adc_count_r2[2]),
        .I4(tsweep_counter_reg[1]),
        .I5(start_adc_count_r2[1]),
        .O(i__carry_i_4__0_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_5
       (.I0(sweep_val[7]),
        .I1(tsweep_counter_reg[7]),
        .I2(sweep_val[6]),
        .I3(tsweep_counter_reg[6]),
        .O(i__carry_i_5_n_0));
  CARRY4 i__carry_i_5__1
       (.CI(i__carry_i_6__0_n_0),
        .CO({i__carry_i_5__1_n_0,i__carry_i_5__1_n_1,i__carry_i_5__1_n_2,i__carry_i_5__1_n_3}),
        .CYINIT(1'b0),
        .DI(sweep_val[12:9]),
        .O(start_adc_count_r2[12:9]),
        .S({i__carry_i_8__0_n_0,i__carry_i_9_n_0,i__carry_i_10_n_0,i__carry_i_11_n_0}));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_6
       (.I0(sweep_val[5]),
        .I1(tsweep_counter_reg[5]),
        .I2(sweep_val[4]),
        .I3(tsweep_counter_reg[4]),
        .O(i__carry_i_6_n_0));
  CARRY4 i__carry_i_6__0
       (.CI(i__carry_i_7__0_n_0),
        .CO({i__carry_i_6__0_n_0,i__carry_i_6__0_n_1,i__carry_i_6__0_n_2,i__carry_i_6__0_n_3}),
        .CYINIT(1'b0),
        .DI(sweep_val[8:5]),
        .O(start_adc_count_r2[8:5]),
        .S({i__carry_i_12_n_0,i__carry_i_13_n_0,i__carry_i_14_n_0,i__carry_i_15_n_0}));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_7
       (.I0(sweep_val[3]),
        .I1(tsweep_counter_reg[3]),
        .I2(sweep_val[2]),
        .I3(tsweep_counter_reg[2]),
        .O(i__carry_i_7_n_0));
  CARRY4 i__carry_i_7__0
       (.CI(1'b0),
        .CO({i__carry_i_7__0_n_0,i__carry_i_7__0_n_1,i__carry_i_7__0_n_2,i__carry_i_7__0_n_3}),
        .CYINIT(sweep_val[0]),
        .DI(sweep_val[4:1]),
        .O(start_adc_count_r2[4:1]),
        .S({i__carry_i_16_n_0,i__carry_i_17_n_0,i__carry_i_18_n_0,i__carry_i_19_n_0}));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_8
       (.I0(sweep_val[1]),
        .I1(tsweep_counter_reg[1]),
        .I2(sweep_val[0]),
        .I3(tsweep_counter_reg[0]),
        .O(i__carry_i_8_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry_i_8__0
       (.I0(sweep_val[12]),
        .O(i__carry_i_8__0_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry_i_9
       (.I0(sweep_val[11]),
        .O(i__carry_i_9_n_0));
  FDRE pll_cen_reg_reg
       (.C(s00_axi_aclk),
        .CE(s00_axi_aresetn),
        .D(LOW_AMP_EN_reg_reg_0[2]),
        .Q(spi_pll_cen),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h00000000FCFE0000)) 
    pll_trig_reg2_i_1
       (.I0(pll_trig),
        .I1(pll_trig_reg2_i_2_n_0),
        .I2(pll_trig_reg2_i_3_n_0),
        .I3(tguard_counter),
        .I4(pll_trig_reg2_i_4_n_0),
        .I5(pll_trig_reg2_i_5_n_0),
        .O(pll_trig_reg2_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT2 #(
    .INIT(4'h2)) 
    pll_trig_reg2_i_2
       (.I0(tsweep_counter2),
        .I1(\tguard_counter[15]_i_4_n_0 ),
        .O(pll_trig_reg2_i_2_n_0));
  LUT6 #(
    .INIT(64'h0000000000000020)) 
    pll_trig_reg2_i_3
       (.I0(start_adc_count_r_i_2_n_0),
        .I1(\tguard_counter_reg_n_0_[7] ),
        .I2(\tguard_counter_reg_n_0_[2] ),
        .I3(pll_trig_reg2_i_6_n_0),
        .I4(pll_trig_reg2_i_7_n_0),
        .I5(pll_trig_reg2_i_8_n_0),
        .O(pll_trig_reg2_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'h40)) 
    pll_trig_reg2_i_4
       (.I0(azimut_0),
        .I1(enable_triger),
        .I2(s00_axi_aresetn),
        .O(pll_trig_reg2_i_4_n_0));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT2 #(
    .INIT(4'h2)) 
    pll_trig_reg2_i_5
       (.I0(tsweep_counter118_in),
        .I1(\tguard_counter[15]_i_4_n_0 ),
        .O(pll_trig_reg2_i_5_n_0));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT2 #(
    .INIT(4'hE)) 
    pll_trig_reg2_i_6
       (.I0(\tguard_counter_reg_n_0_[4] ),
        .I1(\tguard_counter_reg_n_0_[3] ),
        .O(pll_trig_reg2_i_6_n_0));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT4 #(
    .INIT(16'hFFF7)) 
    pll_trig_reg2_i_7
       (.I0(\tguard_counter_reg_n_0_[5] ),
        .I1(\tguard_counter_reg_n_0_[6] ),
        .I2(\tguard_counter_reg_n_0_[1] ),
        .I3(\tguard_counter_reg_n_0_[0] ),
        .O(pll_trig_reg2_i_7_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    pll_trig_reg2_i_8
       (.I0(\tguard_counter_reg_n_0_[15] ),
        .I1(\tguard_counter_reg_n_0_[8] ),
        .I2(\tguard_counter_reg_n_0_[12] ),
        .I3(\tguard_counter_reg_n_0_[11] ),
        .I4(\tguard_counter[15]_i_8_n_0 ),
        .O(pll_trig_reg2_i_8_n_0));
  FDRE pll_trig_reg2_reg
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(pll_trig_reg2_i_1_n_0),
        .Q(pll_trig),
        .R(1'b0));
  FDRE \shift_front_from_cpu_reg[0] 
       (.C(s00_axi_aclk),
        .CE(s00_axi_aresetn),
        .D(\shift_front_from_cpu_reg[15]_0 [0]),
        .Q(shift_front_from_cpu[0]),
        .R(1'b0));
  FDRE \shift_front_from_cpu_reg[10] 
       (.C(s00_axi_aclk),
        .CE(s00_axi_aresetn),
        .D(\shift_front_from_cpu_reg[15]_0 [10]),
        .Q(shift_front_from_cpu[10]),
        .R(1'b0));
  FDRE \shift_front_from_cpu_reg[11] 
       (.C(s00_axi_aclk),
        .CE(s00_axi_aresetn),
        .D(\shift_front_from_cpu_reg[15]_0 [11]),
        .Q(shift_front_from_cpu[11]),
        .R(1'b0));
  FDRE \shift_front_from_cpu_reg[12] 
       (.C(s00_axi_aclk),
        .CE(s00_axi_aresetn),
        .D(\shift_front_from_cpu_reg[15]_0 [12]),
        .Q(shift_front_from_cpu[12]),
        .R(1'b0));
  FDRE \shift_front_from_cpu_reg[13] 
       (.C(s00_axi_aclk),
        .CE(s00_axi_aresetn),
        .D(\shift_front_from_cpu_reg[15]_0 [13]),
        .Q(shift_front_from_cpu[13]),
        .R(1'b0));
  FDRE \shift_front_from_cpu_reg[14] 
       (.C(s00_axi_aclk),
        .CE(s00_axi_aresetn),
        .D(\shift_front_from_cpu_reg[15]_0 [14]),
        .Q(shift_front_from_cpu[14]),
        .R(1'b0));
  FDRE \shift_front_from_cpu_reg[15] 
       (.C(s00_axi_aclk),
        .CE(s00_axi_aresetn),
        .D(\shift_front_from_cpu_reg[15]_0 [15]),
        .Q(shift_front_from_cpu[15]),
        .R(1'b0));
  FDRE \shift_front_from_cpu_reg[1] 
       (.C(s00_axi_aclk),
        .CE(s00_axi_aresetn),
        .D(\shift_front_from_cpu_reg[15]_0 [1]),
        .Q(shift_front_from_cpu[1]),
        .R(1'b0));
  FDRE \shift_front_from_cpu_reg[2] 
       (.C(s00_axi_aclk),
        .CE(s00_axi_aresetn),
        .D(\shift_front_from_cpu_reg[15]_0 [2]),
        .Q(shift_front_from_cpu[2]),
        .R(1'b0));
  FDRE \shift_front_from_cpu_reg[3] 
       (.C(s00_axi_aclk),
        .CE(s00_axi_aresetn),
        .D(\shift_front_from_cpu_reg[15]_0 [3]),
        .Q(shift_front_from_cpu[3]),
        .R(1'b0));
  FDRE \shift_front_from_cpu_reg[4] 
       (.C(s00_axi_aclk),
        .CE(s00_axi_aresetn),
        .D(\shift_front_from_cpu_reg[15]_0 [4]),
        .Q(shift_front_from_cpu[4]),
        .R(1'b0));
  FDRE \shift_front_from_cpu_reg[5] 
       (.C(s00_axi_aclk),
        .CE(s00_axi_aresetn),
        .D(\shift_front_from_cpu_reg[15]_0 [5]),
        .Q(shift_front_from_cpu[5]),
        .R(1'b0));
  FDRE \shift_front_from_cpu_reg[6] 
       (.C(s00_axi_aclk),
        .CE(s00_axi_aresetn),
        .D(\shift_front_from_cpu_reg[15]_0 [6]),
        .Q(shift_front_from_cpu[6]),
        .R(1'b0));
  FDRE \shift_front_from_cpu_reg[7] 
       (.C(s00_axi_aclk),
        .CE(s00_axi_aresetn),
        .D(\shift_front_from_cpu_reg[15]_0 [7]),
        .Q(shift_front_from_cpu[7]),
        .R(1'b0));
  FDRE \shift_front_from_cpu_reg[8] 
       (.C(s00_axi_aclk),
        .CE(s00_axi_aresetn),
        .D(\shift_front_from_cpu_reg[15]_0 [8]),
        .Q(shift_front_from_cpu[8]),
        .R(1'b0));
  FDRE \shift_front_from_cpu_reg[9] 
       (.C(s00_axi_aclk),
        .CE(s00_axi_aresetn),
        .D(\shift_front_from_cpu_reg[15]_0 [9]),
        .Q(shift_front_from_cpu[9]),
        .R(1'b0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_spi_hmc_mode_RX spi_hmc_mode_RX_inst
       (.START_receive(START_receive),
        .\axi_rdata_reg[0] (\axi_rdata_reg[0] ),
        .\axi_rdata_reg[0]_0 (\axi_rdata_reg[0]_0 ),
        .clk_SPI_PLL(clk_SPI_PLL),
        .\data_r_reg[0]_0 (\data_r_reg[0] ),
        .\data_r_reg[23]_0 (\data_r_reg[23] ),
        .enable_sck(enable_sck),
        .mosi_r_reg_0(mosi_r_reg_1),
        .out(out),
        .pll_data_ready_TX(pll_data_ready_TX),
        .pll_mosi_read(pll_mosi_read),
        .pll_sen_read(pll_sen_read),
        .spi_pll_ld_sdo(spi_pll_ld_sdo));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_spi_hmc_mode_TX spi_hmc_mode_TX_inst
       (.START_send(START_send),
        .clk_SPI_PLL(clk_SPI_PLL),
        .\cnt_reg[5]_0 (\cnt_reg[5] ),
        .enable_sck(enable_sck),
        .mosi_r_reg_0(mosi_r_reg),
        .mosi_r_reg_1(mosi_r_reg_0),
        .mosi_r_reg_2(mosi_r_reg_2),
        .out(\cnt_reg[4] ),
        .pll_data_ready_TX(pll_data_ready_TX),
        .pll_mosi_read(pll_mosi_read),
        .pll_sen_read(pll_sen_read),
        .spi_pll_mosi(spi_pll_mosi),
        .spi_pll_sck(spi_pll_sck),
        .spi_pll_sen(spi_pll_sen));
  CARRY4 start_adc_count_r0_carry
       (.CI(1'b0),
        .CO({start_adc_count_r0_carry_n_0,start_adc_count_r0_carry_n_1,start_adc_count_r0_carry_n_2,start_adc_count_r0_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_start_adc_count_r0_carry_O_UNCONNECTED[3:0]),
        .S({start_adc_count_r0_carry_i_1_n_0,start_adc_count_r0_carry_i_2_n_0,start_adc_count_r0_carry_i_3_n_0,start_adc_count_r0_carry_i_4_n_0}));
  CARRY4 start_adc_count_r0_carry__0
       (.CI(start_adc_count_r0_carry_n_0),
        .CO({NLW_start_adc_count_r0_carry__0_CO_UNCONNECTED[3:2],start_adc_count_r0,start_adc_count_r0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_start_adc_count_r0_carry__0_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,start_adc_count_r0_carry__0_i_1_n_0,start_adc_count_r0_carry__0_i_2_n_0}));
  LUT2 #(
    .INIT(4'h9)) 
    start_adc_count_r0_carry__0_i_1
       (.I0(shift_front_from_cpu[15]),
        .I1(tsweep_counter_reg[15]),
        .O(start_adc_count_r0_carry__0_i_1_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    start_adc_count_r0_carry__0_i_2
       (.I0(tsweep_counter_reg[14]),
        .I1(shift_front_from_cpu[14]),
        .I2(tsweep_counter_reg[12]),
        .I3(shift_front_from_cpu[12]),
        .I4(shift_front_from_cpu[13]),
        .I5(tsweep_counter_reg[13]),
        .O(start_adc_count_r0_carry__0_i_2_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    start_adc_count_r0_carry_i_1
       (.I0(tsweep_counter_reg[11]),
        .I1(shift_front_from_cpu[11]),
        .I2(tsweep_counter_reg[9]),
        .I3(shift_front_from_cpu[9]),
        .I4(shift_front_from_cpu[10]),
        .I5(tsweep_counter_reg[10]),
        .O(start_adc_count_r0_carry_i_1_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    start_adc_count_r0_carry_i_2
       (.I0(shift_front_from_cpu[8]),
        .I1(tsweep_counter_reg[8]),
        .I2(tsweep_counter_reg[6]),
        .I3(shift_front_from_cpu[6]),
        .I4(tsweep_counter_reg[7]),
        .I5(shift_front_from_cpu[7]),
        .O(start_adc_count_r0_carry_i_2_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    start_adc_count_r0_carry_i_3
       (.I0(tsweep_counter_reg[3]),
        .I1(shift_front_from_cpu[3]),
        .I2(tsweep_counter_reg[4]),
        .I3(shift_front_from_cpu[4]),
        .I4(shift_front_from_cpu[5]),
        .I5(tsweep_counter_reg[5]),
        .O(start_adc_count_r0_carry_i_3_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    start_adc_count_r0_carry_i_4
       (.I0(tsweep_counter_reg[1]),
        .I1(shift_front_from_cpu[1]),
        .I2(tsweep_counter_reg[0]),
        .I3(shift_front_from_cpu[0]),
        .I4(shift_front_from_cpu[2]),
        .I5(tsweep_counter_reg[2]),
        .O(start_adc_count_r0_carry_i_4_n_0));
  CARRY4 \start_adc_count_r1_inferred__0/i__carry 
       (.CI(1'b0),
        .CO({\start_adc_count_r1_inferred__0/i__carry_n_0 ,\start_adc_count_r1_inferred__0/i__carry_n_1 ,\start_adc_count_r1_inferred__0/i__carry_n_2 ,\start_adc_count_r1_inferred__0/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_start_adc_count_r1_inferred__0/i__carry_O_UNCONNECTED [3:0]),
        .S({i__carry_i_1__0_n_0,i__carry_i_2__1_n_0,i__carry_i_3__0_n_0,i__carry_i_4__0_n_0}));
  CARRY4 \start_adc_count_r1_inferred__0/i__carry__0 
       (.CI(\start_adc_count_r1_inferred__0/i__carry_n_0 ),
        .CO({\start_adc_count_r1_inferred__0/i__carry__0_n_0 ,\start_adc_count_r1_inferred__0/i__carry__0_n_1 ,\start_adc_count_r1_inferred__0/i__carry__0_n_2 ,\start_adc_count_r1_inferred__0/i__carry__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_start_adc_count_r1_inferred__0/i__carry__0_O_UNCONNECTED [3:0]),
        .S({i__carry__0_i_1__1_n_0,i__carry__0_i_1__1_n_0,i__carry__0_i_2__0_n_0,i__carry__0_i_3__0_n_0}));
  CARRY4 \start_adc_count_r1_inferred__0/i__carry__1 
       (.CI(\start_adc_count_r1_inferred__0/i__carry__0_n_0 ),
        .CO({\NLW_start_adc_count_r1_inferred__0/i__carry__1_CO_UNCONNECTED [3],start_adc_count_r10_in,\start_adc_count_r1_inferred__0/i__carry__1_n_2 ,\start_adc_count_r1_inferred__0/i__carry__1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_start_adc_count_r1_inferred__0/i__carry__1_O_UNCONNECTED [3:0]),
        .S({1'b0,i__carry__0_i_1__1_n_0,i__carry__0_i_1__1_n_0,i__carry__0_i_1__1_n_0}));
  LUT5 #(
    .INIT(32'h000E0000)) 
    start_adc_count_r_i_1
       (.I0(start_adc_count),
        .I1(start_adc_count_r0),
        .I2(start_adc_count_r10_in),
        .I3(start_adc_count_r_i_2_n_0),
        .I4(s00_axi_aresetn),
        .O(start_adc_count_r_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    start_adc_count_r_i_2
       (.I0(start_adc_count_r_i_3_n_0),
        .I1(tsweep_counter_reg[3]),
        .I2(tsweep_counter_reg[0]),
        .I3(tsweep_counter_reg[2]),
        .I4(tsweep_counter_reg[1]),
        .I5(start_adc_count_r_i_4_n_0),
        .O(start_adc_count_r_i_2_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    start_adc_count_r_i_3
       (.I0(tsweep_counter_reg[6]),
        .I1(tsweep_counter_reg[5]),
        .I2(tsweep_counter_reg[7]),
        .I3(tsweep_counter_reg[4]),
        .O(start_adc_count_r_i_3_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    start_adc_count_r_i_4
       (.I0(tsweep_counter_reg[8]),
        .I1(tsweep_counter_reg[9]),
        .I2(tsweep_counter_reg[10]),
        .I3(tsweep_counter_reg[11]),
        .I4(start_adc_count_r_i_5_n_0),
        .O(start_adc_count_r_i_4_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    start_adc_count_r_i_5
       (.I0(tsweep_counter_reg[14]),
        .I1(tsweep_counter_reg[13]),
        .I2(tsweep_counter_reg[15]),
        .I3(tsweep_counter_reg[12]),
        .O(start_adc_count_r_i_5_n_0));
  FDRE start_adc_count_r_reg
       (.C(clkDCO_10MHz),
        .CE(1'b1),
        .D(start_adc_count_r_i_1_n_0),
        .Q(start_adc_count),
        .R(1'b0));
  FDRE \sweep_val_reg[0] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sweep_val_tmp[0]),
        .Q(sweep_val[0]),
        .R(SS));
  FDSE \sweep_val_reg[10] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sweep_val_tmp[10]),
        .Q(sweep_val[10]),
        .S(SS));
  FDRE \sweep_val_reg[11] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sweep_val_tmp[11]),
        .Q(sweep_val[11]),
        .R(SS));
  FDRE \sweep_val_reg[12] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sweep_val_tmp[12]),
        .Q(sweep_val[12]),
        .R(SS));
  FDSE \sweep_val_reg[13] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sweep_val_tmp[13]),
        .Q(sweep_val[13]),
        .S(SS));
  FDRE \sweep_val_reg[14] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sweep_val_tmp[14]),
        .Q(sweep_val[14]),
        .R(SS));
  FDRE \sweep_val_reg[15] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sweep_val_tmp[15]),
        .Q(sweep_val[15]),
        .R(SS));
  FDRE \sweep_val_reg[1] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sweep_val_tmp[1]),
        .Q(sweep_val[1]),
        .R(SS));
  FDSE \sweep_val_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sweep_val_tmp[2]),
        .Q(sweep_val[2]),
        .S(SS));
  FDSE \sweep_val_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sweep_val_tmp[3]),
        .Q(sweep_val[3]),
        .S(SS));
  FDRE \sweep_val_reg[4] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sweep_val_tmp[4]),
        .Q(sweep_val[4]),
        .R(SS));
  FDSE \sweep_val_reg[5] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sweep_val_tmp[5]),
        .Q(sweep_val[5]),
        .S(SS));
  FDRE \sweep_val_reg[6] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sweep_val_tmp[6]),
        .Q(sweep_val[6]),
        .R(SS));
  FDSE \sweep_val_reg[7] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sweep_val_tmp[7]),
        .Q(sweep_val[7]),
        .S(SS));
  FDRE \sweep_val_reg[8] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sweep_val_tmp[8]),
        .Q(sweep_val[8]),
        .R(SS));
  FDSE \sweep_val_reg[9] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(sweep_val_tmp[9]),
        .Q(sweep_val[9]),
        .S(SS));
  FDRE \sweep_val_tmp_reg[0] 
       (.C(s00_axi_aclk),
        .CE(s00_axi_aresetn),
        .D(D[0]),
        .Q(sweep_val_tmp[0]),
        .R(1'b0));
  FDRE \sweep_val_tmp_reg[10] 
       (.C(s00_axi_aclk),
        .CE(s00_axi_aresetn),
        .D(D[10]),
        .Q(sweep_val_tmp[10]),
        .R(1'b0));
  FDRE \sweep_val_tmp_reg[11] 
       (.C(s00_axi_aclk),
        .CE(s00_axi_aresetn),
        .D(D[11]),
        .Q(sweep_val_tmp[11]),
        .R(1'b0));
  FDRE \sweep_val_tmp_reg[12] 
       (.C(s00_axi_aclk),
        .CE(s00_axi_aresetn),
        .D(D[12]),
        .Q(sweep_val_tmp[12]),
        .R(1'b0));
  FDRE \sweep_val_tmp_reg[13] 
       (.C(s00_axi_aclk),
        .CE(s00_axi_aresetn),
        .D(D[13]),
        .Q(sweep_val_tmp[13]),
        .R(1'b0));
  FDRE \sweep_val_tmp_reg[14] 
       (.C(s00_axi_aclk),
        .CE(s00_axi_aresetn),
        .D(D[14]),
        .Q(sweep_val_tmp[14]),
        .R(1'b0));
  FDRE \sweep_val_tmp_reg[15] 
       (.C(s00_axi_aclk),
        .CE(s00_axi_aresetn),
        .D(D[15]),
        .Q(sweep_val_tmp[15]),
        .R(1'b0));
  FDRE \sweep_val_tmp_reg[1] 
       (.C(s00_axi_aclk),
        .CE(s00_axi_aresetn),
        .D(D[1]),
        .Q(sweep_val_tmp[1]),
        .R(1'b0));
  FDRE \sweep_val_tmp_reg[2] 
       (.C(s00_axi_aclk),
        .CE(s00_axi_aresetn),
        .D(D[2]),
        .Q(sweep_val_tmp[2]),
        .R(1'b0));
  FDRE \sweep_val_tmp_reg[3] 
       (.C(s00_axi_aclk),
        .CE(s00_axi_aresetn),
        .D(D[3]),
        .Q(sweep_val_tmp[3]),
        .R(1'b0));
  FDRE \sweep_val_tmp_reg[4] 
       (.C(s00_axi_aclk),
        .CE(s00_axi_aresetn),
        .D(D[4]),
        .Q(sweep_val_tmp[4]),
        .R(1'b0));
  FDRE \sweep_val_tmp_reg[5] 
       (.C(s00_axi_aclk),
        .CE(s00_axi_aresetn),
        .D(D[5]),
        .Q(sweep_val_tmp[5]),
        .R(1'b0));
  FDRE \sweep_val_tmp_reg[6] 
       (.C(s00_axi_aclk),
        .CE(s00_axi_aresetn),
        .D(D[6]),
        .Q(sweep_val_tmp[6]),
        .R(1'b0));
  FDRE \sweep_val_tmp_reg[7] 
       (.C(s00_axi_aclk),
        .CE(s00_axi_aresetn),
        .D(D[7]),
        .Q(sweep_val_tmp[7]),
        .R(1'b0));
  FDRE \sweep_val_tmp_reg[8] 
       (.C(s00_axi_aclk),
        .CE(s00_axi_aresetn),
        .D(D[8]),
        .Q(sweep_val_tmp[8]),
        .R(1'b0));
  FDRE \sweep_val_tmp_reg[9] 
       (.C(s00_axi_aclk),
        .CE(s00_axi_aresetn),
        .D(D[9]),
        .Q(sweep_val_tmp[9]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h12FF000012120000)) 
    \tguard_counter[0]_i_1 
       (.I0(\tguard_counter_reg_n_0_[0] ),
        .I1(pll_trig_reg2_i_3_n_0),
        .I2(tguard_counter),
        .I3(tsweep_counter118_in),
        .I4(pll_trig_reg2_i_4_n_0),
        .I5(pll_trig_reg2_i_2_n_0),
        .O(\tguard_counter[0]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hA0F4FFFF)) 
    \tguard_counter[15]_i_1 
       (.I0(\tguard_counter[15]_i_4_n_0 ),
        .I1(tsweep_counter2),
        .I2(pll_trig_reg2_i_3_n_0),
        .I3(tsweep_counter118_in),
        .I4(pll_trig_reg2_i_4_n_0),
        .O(\tguard_counter[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h000000000AAAAAA8)) 
    \tguard_counter[15]_i_2 
       (.I0(start_adc_count_r_i_2_n_0),
        .I1(\tguard_counter[15]_i_5_n_0 ),
        .I2(\tguard_counter_reg_n_0_[5] ),
        .I3(\tguard_counter_reg_n_0_[6] ),
        .I4(\tguard_counter[15]_i_6_n_0 ),
        .I5(\tguard_counter[15]_i_7_n_0 ),
        .O(tguard_counter));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \tguard_counter[15]_i_4 
       (.I0(\tguard_counter[15]_i_7_n_0 ),
        .I1(\tguard_counter_reg_n_0_[1] ),
        .I2(\tguard_counter_reg_n_0_[0] ),
        .I3(\tguard_counter_reg_n_0_[5] ),
        .I4(\tguard_counter_reg_n_0_[6] ),
        .I5(\tguard_counter[15]_i_6_n_0 ),
        .O(\tguard_counter[15]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \tguard_counter[15]_i_5 
       (.I0(\tguard_counter_reg_n_0_[0] ),
        .I1(\tguard_counter_reg_n_0_[1] ),
        .O(\tguard_counter[15]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    \tguard_counter[15]_i_6 
       (.I0(\tguard_counter_reg_n_0_[2] ),
        .I1(\tguard_counter_reg_n_0_[3] ),
        .I2(\tguard_counter_reg_n_0_[4] ),
        .O(\tguard_counter[15]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \tguard_counter[15]_i_7 
       (.I0(\tguard_counter_reg_n_0_[7] ),
        .I1(\tguard_counter[15]_i_8_n_0 ),
        .I2(\tguard_counter_reg_n_0_[11] ),
        .I3(\tguard_counter_reg_n_0_[12] ),
        .I4(\tguard_counter_reg_n_0_[8] ),
        .I5(\tguard_counter_reg_n_0_[15] ),
        .O(\tguard_counter[15]_i_7_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \tguard_counter[15]_i_8 
       (.I0(\tguard_counter_reg_n_0_[10] ),
        .I1(\tguard_counter_reg_n_0_[13] ),
        .I2(\tguard_counter_reg_n_0_[9] ),
        .I3(\tguard_counter_reg_n_0_[14] ),
        .O(\tguard_counter[15]_i_8_n_0 ));
  FDRE \tguard_counter_reg[0] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\tguard_counter[0]_i_1_n_0 ),
        .Q(\tguard_counter_reg_n_0_[0] ),
        .R(1'b0));
  FDRE \tguard_counter_reg[10] 
       (.C(clk_10MHz),
        .CE(tguard_counter),
        .D(p_2_in[10]),
        .Q(\tguard_counter_reg_n_0_[10] ),
        .R(\tguard_counter[15]_i_1_n_0 ));
  FDRE \tguard_counter_reg[11] 
       (.C(clk_10MHz),
        .CE(tguard_counter),
        .D(p_2_in[11]),
        .Q(\tguard_counter_reg_n_0_[11] ),
        .R(\tguard_counter[15]_i_1_n_0 ));
  FDRE \tguard_counter_reg[12] 
       (.C(clk_10MHz),
        .CE(tguard_counter),
        .D(p_2_in[12]),
        .Q(\tguard_counter_reg_n_0_[12] ),
        .R(\tguard_counter[15]_i_1_n_0 ));
  CARRY4 \tguard_counter_reg[12]_i_1 
       (.CI(\tguard_counter_reg[8]_i_1_n_0 ),
        .CO({\tguard_counter_reg[12]_i_1_n_0 ,\tguard_counter_reg[12]_i_1_n_1 ,\tguard_counter_reg[12]_i_1_n_2 ,\tguard_counter_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(p_2_in[12:9]),
        .S({\tguard_counter_reg_n_0_[12] ,\tguard_counter_reg_n_0_[11] ,\tguard_counter_reg_n_0_[10] ,\tguard_counter_reg_n_0_[9] }));
  FDRE \tguard_counter_reg[13] 
       (.C(clk_10MHz),
        .CE(tguard_counter),
        .D(p_2_in[13]),
        .Q(\tguard_counter_reg_n_0_[13] ),
        .R(\tguard_counter[15]_i_1_n_0 ));
  FDRE \tguard_counter_reg[14] 
       (.C(clk_10MHz),
        .CE(tguard_counter),
        .D(p_2_in[14]),
        .Q(\tguard_counter_reg_n_0_[14] ),
        .R(\tguard_counter[15]_i_1_n_0 ));
  FDRE \tguard_counter_reg[15] 
       (.C(clk_10MHz),
        .CE(tguard_counter),
        .D(p_2_in[15]),
        .Q(\tguard_counter_reg_n_0_[15] ),
        .R(\tguard_counter[15]_i_1_n_0 ));
  CARRY4 \tguard_counter_reg[15]_i_3 
       (.CI(\tguard_counter_reg[12]_i_1_n_0 ),
        .CO({\NLW_tguard_counter_reg[15]_i_3_CO_UNCONNECTED [3:2],\tguard_counter_reg[15]_i_3_n_2 ,\tguard_counter_reg[15]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_tguard_counter_reg[15]_i_3_O_UNCONNECTED [3],p_2_in[15:13]}),
        .S({1'b0,\tguard_counter_reg_n_0_[15] ,\tguard_counter_reg_n_0_[14] ,\tguard_counter_reg_n_0_[13] }));
  FDRE \tguard_counter_reg[1] 
       (.C(clk_10MHz),
        .CE(tguard_counter),
        .D(p_2_in[1]),
        .Q(\tguard_counter_reg_n_0_[1] ),
        .R(\tguard_counter[15]_i_1_n_0 ));
  FDRE \tguard_counter_reg[2] 
       (.C(clk_10MHz),
        .CE(tguard_counter),
        .D(p_2_in[2]),
        .Q(\tguard_counter_reg_n_0_[2] ),
        .R(\tguard_counter[15]_i_1_n_0 ));
  FDRE \tguard_counter_reg[3] 
       (.C(clk_10MHz),
        .CE(tguard_counter),
        .D(p_2_in[3]),
        .Q(\tguard_counter_reg_n_0_[3] ),
        .R(\tguard_counter[15]_i_1_n_0 ));
  FDRE \tguard_counter_reg[4] 
       (.C(clk_10MHz),
        .CE(tguard_counter),
        .D(p_2_in[4]),
        .Q(\tguard_counter_reg_n_0_[4] ),
        .R(\tguard_counter[15]_i_1_n_0 ));
  CARRY4 \tguard_counter_reg[4]_i_1 
       (.CI(1'b0),
        .CO({\tguard_counter_reg[4]_i_1_n_0 ,\tguard_counter_reg[4]_i_1_n_1 ,\tguard_counter_reg[4]_i_1_n_2 ,\tguard_counter_reg[4]_i_1_n_3 }),
        .CYINIT(\tguard_counter_reg_n_0_[0] ),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(p_2_in[4:1]),
        .S({\tguard_counter_reg_n_0_[4] ,\tguard_counter_reg_n_0_[3] ,\tguard_counter_reg_n_0_[2] ,\tguard_counter_reg_n_0_[1] }));
  FDRE \tguard_counter_reg[5] 
       (.C(clk_10MHz),
        .CE(tguard_counter),
        .D(p_2_in[5]),
        .Q(\tguard_counter_reg_n_0_[5] ),
        .R(\tguard_counter[15]_i_1_n_0 ));
  FDRE \tguard_counter_reg[6] 
       (.C(clk_10MHz),
        .CE(tguard_counter),
        .D(p_2_in[6]),
        .Q(\tguard_counter_reg_n_0_[6] ),
        .R(\tguard_counter[15]_i_1_n_0 ));
  FDRE \tguard_counter_reg[7] 
       (.C(clk_10MHz),
        .CE(tguard_counter),
        .D(p_2_in[7]),
        .Q(\tguard_counter_reg_n_0_[7] ),
        .R(\tguard_counter[15]_i_1_n_0 ));
  FDRE \tguard_counter_reg[8] 
       (.C(clk_10MHz),
        .CE(tguard_counter),
        .D(p_2_in[8]),
        .Q(\tguard_counter_reg_n_0_[8] ),
        .R(\tguard_counter[15]_i_1_n_0 ));
  CARRY4 \tguard_counter_reg[8]_i_1 
       (.CI(\tguard_counter_reg[4]_i_1_n_0 ),
        .CO({\tguard_counter_reg[8]_i_1_n_0 ,\tguard_counter_reg[8]_i_1_n_1 ,\tguard_counter_reg[8]_i_1_n_2 ,\tguard_counter_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(p_2_in[8:5]),
        .S({\tguard_counter_reg_n_0_[8] ,\tguard_counter_reg_n_0_[7] ,\tguard_counter_reg_n_0_[6] ,\tguard_counter_reg_n_0_[5] }));
  FDRE \tguard_counter_reg[9] 
       (.C(clk_10MHz),
        .CE(tguard_counter),
        .D(p_2_in[9]),
        .Q(\tguard_counter_reg_n_0_[9] ),
        .R(\tguard_counter[15]_i_1_n_0 ));
  CARRY4 \tsweep_counter1_inferred__0/i__carry 
       (.CI(1'b0),
        .CO({\tsweep_counter1_inferred__0/i__carry_n_0 ,\tsweep_counter1_inferred__0/i__carry_n_1 ,\tsweep_counter1_inferred__0/i__carry_n_2 ,\tsweep_counter1_inferred__0/i__carry_n_3 }),
        .CYINIT(1'b0),
        .DI({i__carry_i_1_n_0,i__carry_i_2__0_n_0,i__carry_i_3_n_0,i__carry_i_4_n_0}),
        .O(\NLW_tsweep_counter1_inferred__0/i__carry_O_UNCONNECTED [3:0]),
        .S({i__carry_i_5_n_0,i__carry_i_6_n_0,i__carry_i_7_n_0,i__carry_i_8_n_0}));
  CARRY4 \tsweep_counter1_inferred__0/i__carry__0 
       (.CI(\tsweep_counter1_inferred__0/i__carry_n_0 ),
        .CO({tsweep_counter118_in,\tsweep_counter1_inferred__0/i__carry__0_n_1 ,\tsweep_counter1_inferred__0/i__carry__0_n_2 ,\tsweep_counter1_inferred__0/i__carry__0_n_3 }),
        .CYINIT(1'b0),
        .DI({i__carry__0_i_1__0_n_0,i__carry__0_i_2_n_0,i__carry__0_i_3_n_0,i__carry__0_i_4__0_n_0}),
        .O(\NLW_tsweep_counter1_inferred__0/i__carry__0_O_UNCONNECTED [3:0]),
        .S({i__carry__0_i_5_n_0,i__carry__0_i_6_n_0,i__carry__0_i_7_n_0,i__carry__0_i_8_n_0}));
  CARRY4 tsweep_counter2_carry
       (.CI(1'b0),
        .CO({tsweep_counter2_carry_n_0,tsweep_counter2_carry_n_1,tsweep_counter2_carry_n_2,tsweep_counter2_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_tsweep_counter2_carry_O_UNCONNECTED[3:0]),
        .S({tsweep_counter2_carry_i_1_n_0,tsweep_counter2_carry_i_2_n_0,tsweep_counter2_carry_i_3_n_0,tsweep_counter2_carry_i_4_n_0}));
  CARRY4 tsweep_counter2_carry__0
       (.CI(tsweep_counter2_carry_n_0),
        .CO({NLW_tsweep_counter2_carry__0_CO_UNCONNECTED[3:2],tsweep_counter2,tsweep_counter2_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_tsweep_counter2_carry__0_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,tsweep_counter2_carry__0_i_1_n_0,tsweep_counter2_carry__0_i_2_n_0}));
  LUT2 #(
    .INIT(4'h9)) 
    tsweep_counter2_carry__0_i_1
       (.I0(sweep_val[15]),
        .I1(tsweep_counter_reg[15]),
        .O(tsweep_counter2_carry__0_i_1_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    tsweep_counter2_carry__0_i_2
       (.I0(tsweep_counter_reg[12]),
        .I1(sweep_val[12]),
        .I2(tsweep_counter_reg[13]),
        .I3(sweep_val[13]),
        .I4(sweep_val[14]),
        .I5(tsweep_counter_reg[14]),
        .O(tsweep_counter2_carry__0_i_2_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    tsweep_counter2_carry_i_1
       (.I0(tsweep_counter_reg[10]),
        .I1(sweep_val[10]),
        .I2(tsweep_counter_reg[11]),
        .I3(sweep_val[11]),
        .I4(sweep_val[9]),
        .I5(tsweep_counter_reg[9]),
        .O(tsweep_counter2_carry_i_1_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    tsweep_counter2_carry_i_2
       (.I0(tsweep_counter_reg[6]),
        .I1(sweep_val[6]),
        .I2(tsweep_counter_reg[7]),
        .I3(sweep_val[7]),
        .I4(sweep_val[8]),
        .I5(tsweep_counter_reg[8]),
        .O(tsweep_counter2_carry_i_2_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    tsweep_counter2_carry_i_3
       (.I0(tsweep_counter_reg[4]),
        .I1(sweep_val[4]),
        .I2(tsweep_counter_reg[5]),
        .I3(sweep_val[5]),
        .I4(sweep_val[3]),
        .I5(tsweep_counter_reg[3]),
        .O(tsweep_counter2_carry_i_3_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    tsweep_counter2_carry_i_4
       (.I0(tsweep_counter_reg[0]),
        .I1(sweep_val[0]),
        .I2(tsweep_counter_reg[1]),
        .I3(sweep_val[1]),
        .I4(sweep_val[2]),
        .I5(tsweep_counter_reg[2]),
        .O(tsweep_counter2_carry_i_4_n_0));
  LUT6 #(
    .INIT(64'h7F7F7F7F7F7F7F77)) 
    \tsweep_counter[0]_i_1 
       (.I0(enable_triger),
        .I1(s00_axi_aresetn),
        .I2(\tsweep_counter[0]_i_2_n_0 ),
        .I3(pll_trig_reg2_i_2_n_0),
        .I4(pll_trig_reg2_i_3_n_0),
        .I5(tguard_counter),
        .O(\tsweep_counter[0]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'hBA)) 
    \tsweep_counter[0]_i_2 
       (.I0(azimut_0),
        .I1(\tguard_counter[15]_i_4_n_0 ),
        .I2(tsweep_counter118_in),
        .O(\tsweep_counter[0]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \tsweep_counter[0]_i_4 
       (.I0(azimut_0),
        .O(p_4_in));
  LUT3 #(
    .INIT(8'hB8)) 
    \tsweep_counter[0]_i_5 
       (.I0(sweep_val[3]),
        .I1(azimut_0),
        .I2(tsweep_counter_reg[3]),
        .O(\tsweep_counter[0]_i_5_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \tsweep_counter[0]_i_6 
       (.I0(sweep_val[2]),
        .I1(azimut_0),
        .I2(tsweep_counter_reg[2]),
        .O(\tsweep_counter[0]_i_6_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \tsweep_counter[0]_i_7 
       (.I0(sweep_val[1]),
        .I1(azimut_0),
        .I2(tsweep_counter_reg[1]),
        .O(\tsweep_counter[0]_i_7_n_0 ));
  LUT3 #(
    .INIT(8'hC5)) 
    \tsweep_counter[0]_i_8 
       (.I0(tsweep_counter_reg[0]),
        .I1(sweep_val[0]),
        .I2(azimut_0),
        .O(\tsweep_counter[0]_i_8_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \tsweep_counter[12]_i_2 
       (.I0(sweep_val[15]),
        .I1(azimut_0),
        .I2(tsweep_counter_reg[15]),
        .O(\tsweep_counter[12]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \tsweep_counter[12]_i_3 
       (.I0(sweep_val[14]),
        .I1(azimut_0),
        .I2(tsweep_counter_reg[14]),
        .O(\tsweep_counter[12]_i_3_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \tsweep_counter[12]_i_4 
       (.I0(sweep_val[13]),
        .I1(azimut_0),
        .I2(tsweep_counter_reg[13]),
        .O(\tsweep_counter[12]_i_4_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \tsweep_counter[12]_i_5 
       (.I0(sweep_val[12]),
        .I1(azimut_0),
        .I2(tsweep_counter_reg[12]),
        .O(\tsweep_counter[12]_i_5_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \tsweep_counter[4]_i_2 
       (.I0(sweep_val[7]),
        .I1(azimut_0),
        .I2(tsweep_counter_reg[7]),
        .O(\tsweep_counter[4]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \tsweep_counter[4]_i_3 
       (.I0(sweep_val[6]),
        .I1(azimut_0),
        .I2(tsweep_counter_reg[6]),
        .O(\tsweep_counter[4]_i_3_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \tsweep_counter[4]_i_4 
       (.I0(sweep_val[5]),
        .I1(azimut_0),
        .I2(tsweep_counter_reg[5]),
        .O(\tsweep_counter[4]_i_4_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \tsweep_counter[4]_i_5 
       (.I0(sweep_val[4]),
        .I1(azimut_0),
        .I2(tsweep_counter_reg[4]),
        .O(\tsweep_counter[4]_i_5_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \tsweep_counter[8]_i_2 
       (.I0(sweep_val[11]),
        .I1(azimut_0),
        .I2(tsweep_counter_reg[11]),
        .O(\tsweep_counter[8]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \tsweep_counter[8]_i_3 
       (.I0(sweep_val[10]),
        .I1(azimut_0),
        .I2(tsweep_counter_reg[10]),
        .O(\tsweep_counter[8]_i_3_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \tsweep_counter[8]_i_4 
       (.I0(sweep_val[9]),
        .I1(azimut_0),
        .I2(tsweep_counter_reg[9]),
        .O(\tsweep_counter[8]_i_4_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \tsweep_counter[8]_i_5 
       (.I0(sweep_val[8]),
        .I1(azimut_0),
        .I2(tsweep_counter_reg[8]),
        .O(\tsweep_counter[8]_i_5_n_0 ));
  FDRE \tsweep_counter_reg[0] 
       (.C(clk_10MHz),
        .CE(\tsweep_counter[0]_i_2_n_0 ),
        .D(\tsweep_counter_reg[0]_i_3_n_7 ),
        .Q(tsweep_counter_reg[0]),
        .R(\tsweep_counter[0]_i_1_n_0 ));
  CARRY4 \tsweep_counter_reg[0]_i_3 
       (.CI(1'b0),
        .CO({\tsweep_counter_reg[0]_i_3_n_0 ,\tsweep_counter_reg[0]_i_3_n_1 ,\tsweep_counter_reg[0]_i_3_n_2 ,\tsweep_counter_reg[0]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,p_4_in}),
        .O({\tsweep_counter_reg[0]_i_3_n_4 ,\tsweep_counter_reg[0]_i_3_n_5 ,\tsweep_counter_reg[0]_i_3_n_6 ,\tsweep_counter_reg[0]_i_3_n_7 }),
        .S({\tsweep_counter[0]_i_5_n_0 ,\tsweep_counter[0]_i_6_n_0 ,\tsweep_counter[0]_i_7_n_0 ,\tsweep_counter[0]_i_8_n_0 }));
  FDRE \tsweep_counter_reg[10] 
       (.C(clk_10MHz),
        .CE(\tsweep_counter[0]_i_2_n_0 ),
        .D(\tsweep_counter_reg[8]_i_1_n_5 ),
        .Q(tsweep_counter_reg[10]),
        .R(\tsweep_counter[0]_i_1_n_0 ));
  FDRE \tsweep_counter_reg[11] 
       (.C(clk_10MHz),
        .CE(\tsweep_counter[0]_i_2_n_0 ),
        .D(\tsweep_counter_reg[8]_i_1_n_4 ),
        .Q(tsweep_counter_reg[11]),
        .R(\tsweep_counter[0]_i_1_n_0 ));
  FDRE \tsweep_counter_reg[12] 
       (.C(clk_10MHz),
        .CE(\tsweep_counter[0]_i_2_n_0 ),
        .D(\tsweep_counter_reg[12]_i_1_n_7 ),
        .Q(tsweep_counter_reg[12]),
        .R(\tsweep_counter[0]_i_1_n_0 ));
  CARRY4 \tsweep_counter_reg[12]_i_1 
       (.CI(\tsweep_counter_reg[8]_i_1_n_0 ),
        .CO({\NLW_tsweep_counter_reg[12]_i_1_CO_UNCONNECTED [3],\tsweep_counter_reg[12]_i_1_n_1 ,\tsweep_counter_reg[12]_i_1_n_2 ,\tsweep_counter_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\tsweep_counter_reg[12]_i_1_n_4 ,\tsweep_counter_reg[12]_i_1_n_5 ,\tsweep_counter_reg[12]_i_1_n_6 ,\tsweep_counter_reg[12]_i_1_n_7 }),
        .S({\tsweep_counter[12]_i_2_n_0 ,\tsweep_counter[12]_i_3_n_0 ,\tsweep_counter[12]_i_4_n_0 ,\tsweep_counter[12]_i_5_n_0 }));
  FDRE \tsweep_counter_reg[13] 
       (.C(clk_10MHz),
        .CE(\tsweep_counter[0]_i_2_n_0 ),
        .D(\tsweep_counter_reg[12]_i_1_n_6 ),
        .Q(tsweep_counter_reg[13]),
        .R(\tsweep_counter[0]_i_1_n_0 ));
  FDRE \tsweep_counter_reg[14] 
       (.C(clk_10MHz),
        .CE(\tsweep_counter[0]_i_2_n_0 ),
        .D(\tsweep_counter_reg[12]_i_1_n_5 ),
        .Q(tsweep_counter_reg[14]),
        .R(\tsweep_counter[0]_i_1_n_0 ));
  FDRE \tsweep_counter_reg[15] 
       (.C(clk_10MHz),
        .CE(\tsweep_counter[0]_i_2_n_0 ),
        .D(\tsweep_counter_reg[12]_i_1_n_4 ),
        .Q(tsweep_counter_reg[15]),
        .R(\tsweep_counter[0]_i_1_n_0 ));
  FDRE \tsweep_counter_reg[1] 
       (.C(clk_10MHz),
        .CE(\tsweep_counter[0]_i_2_n_0 ),
        .D(\tsweep_counter_reg[0]_i_3_n_6 ),
        .Q(tsweep_counter_reg[1]),
        .R(\tsweep_counter[0]_i_1_n_0 ));
  FDRE \tsweep_counter_reg[2] 
       (.C(clk_10MHz),
        .CE(\tsweep_counter[0]_i_2_n_0 ),
        .D(\tsweep_counter_reg[0]_i_3_n_5 ),
        .Q(tsweep_counter_reg[2]),
        .R(\tsweep_counter[0]_i_1_n_0 ));
  FDRE \tsweep_counter_reg[3] 
       (.C(clk_10MHz),
        .CE(\tsweep_counter[0]_i_2_n_0 ),
        .D(\tsweep_counter_reg[0]_i_3_n_4 ),
        .Q(tsweep_counter_reg[3]),
        .R(\tsweep_counter[0]_i_1_n_0 ));
  FDRE \tsweep_counter_reg[4] 
       (.C(clk_10MHz),
        .CE(\tsweep_counter[0]_i_2_n_0 ),
        .D(\tsweep_counter_reg[4]_i_1_n_7 ),
        .Q(tsweep_counter_reg[4]),
        .R(\tsweep_counter[0]_i_1_n_0 ));
  CARRY4 \tsweep_counter_reg[4]_i_1 
       (.CI(\tsweep_counter_reg[0]_i_3_n_0 ),
        .CO({\tsweep_counter_reg[4]_i_1_n_0 ,\tsweep_counter_reg[4]_i_1_n_1 ,\tsweep_counter_reg[4]_i_1_n_2 ,\tsweep_counter_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\tsweep_counter_reg[4]_i_1_n_4 ,\tsweep_counter_reg[4]_i_1_n_5 ,\tsweep_counter_reg[4]_i_1_n_6 ,\tsweep_counter_reg[4]_i_1_n_7 }),
        .S({\tsweep_counter[4]_i_2_n_0 ,\tsweep_counter[4]_i_3_n_0 ,\tsweep_counter[4]_i_4_n_0 ,\tsweep_counter[4]_i_5_n_0 }));
  FDRE \tsweep_counter_reg[5] 
       (.C(clk_10MHz),
        .CE(\tsweep_counter[0]_i_2_n_0 ),
        .D(\tsweep_counter_reg[4]_i_1_n_6 ),
        .Q(tsweep_counter_reg[5]),
        .R(\tsweep_counter[0]_i_1_n_0 ));
  FDRE \tsweep_counter_reg[6] 
       (.C(clk_10MHz),
        .CE(\tsweep_counter[0]_i_2_n_0 ),
        .D(\tsweep_counter_reg[4]_i_1_n_5 ),
        .Q(tsweep_counter_reg[6]),
        .R(\tsweep_counter[0]_i_1_n_0 ));
  FDRE \tsweep_counter_reg[7] 
       (.C(clk_10MHz),
        .CE(\tsweep_counter[0]_i_2_n_0 ),
        .D(\tsweep_counter_reg[4]_i_1_n_4 ),
        .Q(tsweep_counter_reg[7]),
        .R(\tsweep_counter[0]_i_1_n_0 ));
  FDRE \tsweep_counter_reg[8] 
       (.C(clk_10MHz),
        .CE(\tsweep_counter[0]_i_2_n_0 ),
        .D(\tsweep_counter_reg[8]_i_1_n_7 ),
        .Q(tsweep_counter_reg[8]),
        .R(\tsweep_counter[0]_i_1_n_0 ));
  CARRY4 \tsweep_counter_reg[8]_i_1 
       (.CI(\tsweep_counter_reg[4]_i_1_n_0 ),
        .CO({\tsweep_counter_reg[8]_i_1_n_0 ,\tsweep_counter_reg[8]_i_1_n_1 ,\tsweep_counter_reg[8]_i_1_n_2 ,\tsweep_counter_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\tsweep_counter_reg[8]_i_1_n_4 ,\tsweep_counter_reg[8]_i_1_n_5 ,\tsweep_counter_reg[8]_i_1_n_6 ,\tsweep_counter_reg[8]_i_1_n_7 }),
        .S({\tsweep_counter[8]_i_2_n_0 ,\tsweep_counter[8]_i_3_n_0 ,\tsweep_counter[8]_i_4_n_0 ,\tsweep_counter[8]_i_5_n_0 }));
  FDRE \tsweep_counter_reg[9] 
       (.C(clk_10MHz),
        .CE(\tsweep_counter[0]_i_2_n_0 ),
        .D(\tsweep_counter_reg[8]_i_1_n_6 ),
        .Q(tsweep_counter_reg[9]),
        .R(\tsweep_counter[0]_i_1_n_0 ));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
